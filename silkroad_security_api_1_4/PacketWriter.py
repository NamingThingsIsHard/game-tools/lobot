﻿import io
from typing import List, BinaryIO

from silkroad_security_api_1_4.BinaryWriter import BinaryWriter


class PacketWriter(BinaryWriter):

	def __init__(self, data=None):
		if not data:
			data = []
		super(PacketWriter, self).__init__(data)
		self.m_ms: BinaryIO = io.BytesIO()
		self.OutStream = self.m_ms

	def get_bytes(self) -> List[int]:
		d = self.data.tolist()
		return d
		# return self.data.bytes()
