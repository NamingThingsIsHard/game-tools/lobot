﻿import unicodedata

from typing import List


def is_control_character(ch: str) -> bool:
	return unicodedata.category(ch)[0] == "C"


def remove_control_characters(s):
	return "".join(ch for ch in s if not is_control_character(ch))


class Utility:

	def hex_dump(self, buffer: List[int], offset: int = 0, count: int = -1) -> str:
		if count == -1:
			count = len(buffer)

		bytes_per_line: int = 16
		output = [""]
		ascii_output = [""]
		length = count
		if length % bytes_per_line != 0:
			length += bytes_per_line - length % bytes_per_line
		for x in range(0, length):
			if x % bytes_per_line == 0:
				if x > 0:
					output.append(f'{"".join(ascii_output)}\\n')
					ascii_output.clear()
				if x != length:
					output.append(format("0:d10   ", str(x)))
			if x < count:
				output.append(format("0:X2 ", str(buffer[offset + x])))
				ch = str(buffer[offset + x])
				if not is_control_character(ch):
					ascii_output.append(format("0", ch))
				else:
					ascii_output.append(".")
			else:
				output.append("   ")
				ascii_output.append(".")

		return "".join(output)
