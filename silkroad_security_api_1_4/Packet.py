﻿import codecs
import threading
from ctypes import *
from typing import List, Optional

from silkroad_security_api_1_4.PacketReader import PacketReader
from silkroad_security_api_1_4.PacketWriter import PacketWriter
from silkroad_security_api_1_4.SeekType import SeekType


class PacketDirection:
	Client = 0
	Server = 1


class Packet:
	def __init__(self, opcode: int, encrypted=False, massive=False, buffer: List[int] = None, offset: int = 0, length: int = 0):
		if encrypted and massive:
			raise Exception("[Packet:Packet] Packets cannot both be massive and encrypted!")

		self.m_opcode = opcode
		self.m_writer: PacketWriter = PacketWriter()
		self.m_reader: Optional[PacketReader] = None
		self.m_reader_bytes = []
		self.m_encrypted = encrypted
		self.m_massive = massive
		self.m_locked = False
		self.m_lock = threading.Lock()

		if buffer:
			if offset > 0 and length > 0:
				self.m_writer.write_offset(buffer, offset, length)
			else:
				self.m_writer.write(buffer)

	@property
	def opcode(self) -> int:
		return self.m_opcode

	@property
	def encrypted(self) -> bool:
		return self.m_encrypted

	@property
	def massive(self) -> bool:
		return self.m_massive

	def __set__(self, value):  # Packet(rhs: Packet):
		with value.m_lock:
			self.m_lock = threading.Lock()

			self.m_opcode = value.m_opcode
			self.m_encrypted = value.m_encrypted
			self.m_massive = value.m_massive

			self.m_locked = value.m_locked
			if not self.m_locked:
				self.m_writer = PacketWriter()
				self.m_reader = None
				self.m_reader_bytes = None
				self.m_writer.write(value.m_writer.get_bytes())
			else:
				self.m_writer = None
				self.m_reader_bytes = value.m_reader_bytes
				self.m_reader = PacketReader(self.m_reader_bytes)

	def __repr__(self):
		data_str = self.m_writer.data.tolist() if self.m_writer else self.m_reader_bytes
		return f"{format(self.m_opcode, '#04x')},encrypted={self.m_encrypted},massive={self.m_massive},buffer={data_str}"

	def __str__(self):
		data_str = self.m_writer.data.tolist() if self.m_writer else self.m_reader_bytes
		return f"{format(self.m_opcode, '#04x')},encrypted={self.m_encrypted},massive={self.m_massive},buffer={data_str}"

	def get_bytes(self) -> List[int]:
		with self.m_lock:
			if self.m_locked:
				return self.m_reader_bytes
			return self.m_writer.get_bytes()

	def lock(self):
		with self.m_lock:
			if not self.m_locked:
				self.m_reader_bytes = self.m_writer.get_bytes()
				self.m_reader = PacketReader(self.m_reader_bytes)
				# self.m_writer.Close() ## Garbage collect here
				self.m_writer = None
				self.m_locked = True

	def seek_read(self, offset: c_long, orgin: SeekType) -> int:  # -> c_long:
		with self.m_lock:
			if not self.m_locked:
				raise Exception("Cannot SeekRead on an unlocked Packet.")
			return self.m_reader.seek(offset)

	def remaining_read(self) -> int:
		with self.m_lock:
			if not self.m_locked:
				raise Exception("Cannot SeekRead on an unlocked Packet.")
			return self.m_reader.size - self.m_reader.offset

	def read_uint8(self) -> int:  # -> c_byte:
		with self.m_lock:
			if not self.m_locked:
				raise Exception("Cannot Read from an unlocked Packet.")
			return self.m_reader.read_byte()

	def read_int8(self) -> int:  # -> c_byte:
		with self.m_lock:
			if not self.m_locked:
				raise Exception("Cannot Read from an unlocked Packet.")
			return self.m_reader.read_int8()

	def read_uint16(self) -> int:  # -> c_uint16:
		with self.m_lock:
			if not self.m_locked:
				raise Exception("Cannot Read from an unlocked Packet.")
			return self.m_reader.read_uint16()

	def read_int16(self) -> int:  # -> c_int16:
		with self.m_lock:
			if not self.m_locked:
				raise Exception("Cannot Read from an unlocked Packet.")
			return self.m_reader.read_int16()

	def read_uint32(self) -> int:  # -> c_uint32:
		with self.m_lock:
			if not self.m_locked:
				raise Exception("Cannot Read from an unlocked Packet.")
			return self.m_reader.read_uint32()

	def read_int32(self) -> int:  # -> c_int32:
		with self.m_lock:
			if not self.m_locked:
				raise Exception("Cannot Read from an unlocked Packet.")
			return self.m_reader.read_int32()

	def read_uint64(self) -> int:  # -> c_uint64:
		with self.m_lock:
			if not self.m_locked:
				raise Exception("Cannot Read from an unlocked Packet.")
			return self.m_reader.read_uint64()

	def read_int64(self) -> int:  # -> c_int64:
		with self.m_lock:
			if not self.m_locked:
				raise Exception("Cannot Read from an unlocked Packet.")
			return self.m_reader.read_int64()

	def read_float(self) -> int:  # -> float:
		with self.m_lock:
			if not self.m_locked:
				raise Exception("Cannot Read from an unlocked Packet.")
			return self.m_reader.read_float()

	def read_single(self) -> float:
		return self.read_float()

	def read_double(self) -> float:
		with self.m_lock:
			if not self.m_locked:
				raise Exception("Cannot Read from an unlocked Packet.")
			return self.m_reader.read_double()

	def read_ascii(self, codepage: str = 'windows-1252') -> str:
		with self.m_lock:
			if not self.m_locked:
				raise Exception("Cannot Read from an unlocked Packet.")
			length = self.m_reader.read_uint16()
			c_bytes: List[int] = self.m_reader.read_bytes(length)
			return bytes(c_bytes).decode(codepage, 'replace')

	def read_unicode(self) -> str:
		with self.m_lock:
			if not self.m_locked:
				raise Exception("Cannot Read from an unlocked Packet.")
			length = self.m_reader.read_uint16()
			c_bytes: List[int] = self.m_reader.read_bytes(length * 2)
			return bytes(c_bytes).decode('utf-16-le')

	def read_uint8_array(self, count: int) -> List[int]:  # -> List[c_byte]:
		with self.m_lock:
			if not self.m_locked:
				raise Exception("Cannot Read from an unlocked Packet.")
			values: List[int] = [0x00] * count
			for x in range(0, count):
				values[x] = self.m_reader.read_byte()
			return values

	def read_int8_array(self, count: int) -> List[int]:  # -> List[c_byte]:
		with self.m_lock:
			if not self.m_locked:
				raise Exception("Cannot Read from an unlocked Packet.")
			values: List[int] = [0x00] * count
			for x in range(0, count):
				values[x] = self.m_reader.read_byte()
			return values

	def read_uint16_array(self, count: int) -> List[int]:  # -> List[c_uint16]:
		with self.m_lock:
			if not self.m_locked:
				raise Exception("Cannot Read from an unlocked Packet.")
			values: List[int] = [0x00] * count
			for x in range(0, count):
				values[x] = self.m_reader.read_uint16()
			return values

	def read_int16_array(self, count: int) -> List[int]:  # -> List[c_int16]:

		with self.m_lock:
			if not self.m_locked:
				raise Exception("Cannot Read from an unlocked Packet.")
			values: List[int] = [0x00] * count
			for x in range(0, count):
				values[x] = self.m_reader.read_int16()

			return values

	def read_uint32_array(self, count: int) -> List[int]:  # -> List[c_uint32]:
		with self.m_lock:
			if not self.m_locked:
				raise Exception("Cannot Read from an unlocked Packet.")
			values: List[int] = [0x00] * count
			for x in range(0, count):
				values[x] = self.m_reader.read_uint32()
			return values

	def read_int32_array(self, count: int) -> List[int]:  # -> List[c_int32]:
		with self.m_lock:
			if not self.m_locked:
				raise Exception("Cannot Read from an unlocked Packet.")
			values: List[int] = [0x00] * count
			for x in range(0, count):
				values[x] = self.m_reader.read_int32()
			return values

	def read_uint64_array(self, count: int) -> List[int]:  # -> List[c_uint64]:
		with self.m_lock:
			if not self.m_locked:
				raise Exception("Cannot Read from an unlocked Packet.")
			values: List[int] = [0x00] * count
			for x in range(0, count):
				values[x] = self.m_reader.read_uint64()
			return values

	def read_int64_array(self, count: int) -> List[int]:  # -> List[c_int64]:
		with self.m_lock:
			if not self.m_locked:
				raise Exception("Cannot Read from an unlocked Packet.")
			values: List[int] = [0x00] * count
			for x in range(0, count):
				values[x] = self.m_reader.read_int64()
			return values

	def read_float_array(self, count: int) -> List[float]:
		with self.m_lock:
			if not self.m_locked:
				raise Exception("Cannot Read from an unlocked Packet.")
			values: List[float] = [0x00] * count
			for x in range(0, count):
				values[x] = self.m_reader.read_float()

			return values

	def read_double_array(self, count: int) -> List[float]:
		with self.m_lock:
			if not self.m_locked:
				raise Exception("Cannot Read from an unlocked Packet.")
			values: List[float] = [0x00] * count
			for x in range(0, count):
				values[x] = self.m_reader.read_double()
			return values

	def read_ascii_array(self, count: int = 1, codepage: str = 'windows-1252') -> List[str]:
		with self.m_lock:
			if not self.m_locked:
				raise Exception("Cannot Read from an unlocked Packet.")
			values: List[str] = [''] * count
			decoder = codecs.getdecoder(codepage)
			for x in range(0, count):
				length = self.m_reader.read_uint16()
				c_bytes: List[int] = self.m_reader.read_bytes(length)
				values[x] = decoder(bytes(c_bytes))[0]
			return values

	def read_unicode_array(self, count: int) -> List[str]:
		with self.m_lock:
			if not self.m_locked:
				raise Exception("Cannot Read from an unlocked Packet.")
			values: List[str] = [''] * count
			decoder = codecs.getdecoder('utf-16-le')
			for x in range(0, count):
				length = self.m_reader.read_uint16()
				c_bytes: List[int] = self.m_reader.read_bytes(length * 2)
				values[x] = decoder(bytes(c_bytes))[0]
			return values

	def seek_write(self, offset: c_long, orgin: SeekType) -> int:  # -> c_long:
		with self.m_lock:
			if self.m_locked:
				raise Exception("Cannot SeekWrite on a locked Packet.")
			return self.m_writer.seek(offset, orgin)

	def write_uint8(self, value):
		with self.m_lock:
			if self.m_locked:
				raise Exception("Cannot Write to a locked Packet.")
			self.m_writer.write_uint8(value & 0xFF)

	def write_int8(self, value):
		with self.m_lock:
			if self.m_locked:
				raise Exception("Cannot Write to a locked Packet.")
			self.m_writer.write_int8((value & 0xFF))

	def write_uint16(self, value):
		with self.m_lock:
			if self.m_locked:
				raise Exception("Cannot Write to a locked Packet.")
			self.m_writer.write_uint16(value & 0xFFFF)

	def write_int16(self, value):
		with self.m_lock:
			if self.m_locked:
				raise Exception("Cannot Write to a locked Packet.")
			self.m_writer.write_int16(value & 0xFFFF)

	def write_uint32(self, value):
		with self.m_lock:
			if self.m_locked:
				raise Exception("Cannot Write to a locked Packet.")
			self.m_writer.write_uint32(value & 0xFFFFFFFF)

	def write_int32(self, value):
		with self.m_lock:
			if self.m_locked:
				raise Exception("Cannot Write to a locked Packet.")
			self.m_writer.write_int32(value & 0xFFFFFFFF)

	def write_uint64(self, value):
		with self.m_lock:
			if self.m_locked:
				raise Exception("Cannot Write to a locked Packet.")
			self.m_writer.write_uint64(value)

	def write_int64(self, value):
		with self.m_lock:
			if self.m_locked:
				raise Exception("Cannot Write to a locked Packet.")
			self.m_writer.write_int64(value)

	def write_float(self, value):
		with self.m_lock:
			if self.m_locked:
				raise Exception("Cannot Write to a locked Packet.")
			self.m_writer.write_float(value)

	def write_double(self, value):
		with self.m_lock:
			if self.m_locked:
				raise Exception("Cannot Write to a locked Packet.")
			self.m_writer.write_double(value)

	def write_ascii(self, value: object, code_page: str = 'utf-7'):  # previously code page 'windows-1252'
		with self.m_lock:
			if self.m_locked:
				raise Exception("Cannot Write to a locked Packet.")
			utf7_value = str(value).encode(code_page)
			self.m_writer.write_uint16(len(utf7_value))
			self.m_writer.write(bytes(utf7_value))

	def write_unicode(self, value):
		with self.m_lock:
			if self.m_locked:
				raise Exception("Cannot Write to a locked Packet.")
			c_bytes: bytes = str(value).encode('utf-16-le')

			self.m_writer.write_uint16(len(str(value)))
			self.m_writer.write(c_bytes)

	def write_uint_array(self, values: List[int], index: int = 0, count: int = 0):
		with self.m_lock:
			if self.m_locked:
				raise Exception("Cannot Write to a locked Packet.")
			if count == 0 and values:
				count = len(values)
			for x in range(0, count):
				self.m_writer.write_uint8(values[x])

	def write_uint16_array(self, values: List[int], index: int = 0, count: int = 0):
		with self.m_lock:
			if self.m_locked:
				raise Exception("Cannot Write to a locked Packet.")
			if count == 0 and values:
				count = len(values)
			for x in range(0, count):
				self.m_writer.write_uint16(values[x])

	def write_int16_array(self, values: List[int], index: int = 0, count: int = 0):
		with self.m_lock:
			if self.m_locked:
				raise Exception("Cannot Write to a locked Packet.")
			if count == 0 and values:
				count = len(values)
			for x in range(0, count):
				self.m_writer.write_int16(values[x])

	def write_uint32_array(self, values: List[int], index: int = 0, count: int = 0):
		with self.m_lock:
			if self.m_locked:
				raise Exception("Cannot Write to a locked Packet.")
			if count == 0 and values:
				count = len(values)
			for x in range(0, count):
				self.m_writer.write_uint32(values[x])

	def write_int32_array(self, values: List[int], index: int = 0, count: int = 0):
		with self.m_lock:
			if self.m_locked:
				raise Exception("Cannot Write to a locked Packet.")
			if count == 0 and values:
				count = len(values)
			for x in range(0, count):
				self.m_writer.write_int32(values[x])

	def write_uint64_array(self, values: List[int], index: int = 0, count: int = 0):
		with self.m_lock:
			if self.m_locked:
				raise Exception("Cannot Write to a locked Packet.")
			if count == 0 and values:
				count = len(values)
			for x in range(0, count):
				self.m_writer.write_uint64(values[x])

	def write_int64_array(self, values: List[int], index: int = 0, count: int = 0):
		with self.m_lock:
			if self.m_locked:
				raise Exception("Cannot Write to a locked Packet.")
			if count == 0 and values:
				count = len(values)
			for x in range(0, count):
				self.m_writer.write_int64(values[x])

	def write_float_array(self, values: List[float], index: int = 0, count: int = 0):
		with self.m_lock:
			if self.m_locked:
				raise Exception("Cannot Write to a locked Packet.")
			if count == 0 and values:
				count = len(values)
			for x in range(0, count):
				self.m_writer.write_float(values[x])

	def write_double_array(self, values: List[float], index: int = 0, count: int = 0):
		with self.m_lock:
			if self.m_locked:
				raise Exception("Cannot Write to a locked Packet.")
			if count == 0 and values:
				count = len(values)
			for x in range(0, count):
				self.m_writer.write_double(values[x])

	def write_ascii_array(self, values: List[str], index: int = 0, count: int = 0):
		with self.m_lock:
			if self.m_locked:
				raise Exception("Cannot Write to a locked Packet.")
			if count == 0 and values:
				count = len(values)
			for x in range(index, count):
				self.m_writer.write_uint16(len(values[x]))
				self.m_writer.write_ascii(values[x])

	def write_unicode_array(self, values: List[bytes], index: int = 0, count: int = 0):
		with self.m_lock:
			if self.m_locked:
				raise Exception("Cannot Write to a locked Packet.")
			if count == 0 and values:
				count = len(values)
			for x in range(index, count):
				self.m_writer.write_uint16(len(values[x]))
				self.m_writer.write_unicode(values[x])
