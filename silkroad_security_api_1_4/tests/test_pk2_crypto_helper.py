import os
import unittest
from io import BytesIO
from pathlib import Path

from lobot_api.pk2.extractors.PK2CryptoHelper import PK2CryptoHelper


class TestPK2CryptoHelper(unittest.TestCase):
	fixtures_path = Path(os.path.dirname(os.path.realpath(__file__))) / "fixtures" / "pk2_data" / "crypto_fixtures"
	skilldata_5000_path = fixtures_path / "skilldata_5000.txt"
	skilldata_5000enc_path = fixtures_path / "skilldata_5000enc.txt"

	file_content_skilldata_5000 = None
	file_content_skilldata_5000enc = None

	@classmethod
	def setUpClass(cls):
		"""
		Setup the skill data input and output objects.
		"""
		with open(cls.skilldata_5000_path, "rb") as skill_data:
			cls.file_content_skilldata_5000 = skill_data.read()

		with open(cls.skilldata_5000enc_path, "rb") as skill_data_enc:
			cls.file_content_skilldata_5000enc = skill_data_enc.read()

	def test_encrypt(self):
		"""
		Test encryption of file.
		"""
		with BytesIO(self.file_content_skilldata_5000) as f:
			encrypted = bytes(PK2CryptoHelper.encrypt(list(f.read())))
			expected = self.file_content_skilldata_5000enc
			self.assertEqual(encrypted, expected)

	def test_decrypt(self):
		"""
		Test decryption of file.
		"""
		with BytesIO(self.file_content_skilldata_5000enc) as f:
			decrypted = PK2CryptoHelper.decrypt(f.read())
			expected = self.file_content_skilldata_5000.decode('utf-16')
			self.assertEqual(decrypted, expected)

	def test_decrypt_unencrypted_file(self):
		"""
		Test decryption of file.
		"""
		with BytesIO(self.file_content_skilldata_5000) as f:
			unchanged = PK2CryptoHelper.decrypt(f.read())
			expected = self.file_content_skilldata_5000.decode('utf-16')
			self.assertEqual(unchanged, expected)
