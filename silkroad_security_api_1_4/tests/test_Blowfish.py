import unittest

from silkroad_security_api_1_4.Blowfish import Blowfish


class TestBlowfish(unittest.TestCase):
	"""Test taken from <https://github.com/jashandeep-sohi/python-blowfish/blob/master/test.py>.
	Little-endian test vectors derived from <https://www.schneier.com/code/vectors.txt>.
	I've simply changed the clear text and cipher text to little-endian byte order hex.
	"""
	test_vectors = (
		# key                 clear text          cipher text
		("0000000000000000", "0000000000000000", "4597F94E78DD9861"),
		("FFFFFFFFFFFFFFFF", "FFFFFFFFFFFFFFFF", "D56F86518ACB5EB8"),
		("3000000000000000", "0000001001000000", "9A6F857DF2633061"),
		("1111111111111111", "1111111111111111", "87DD66249D3C968B"),
		("0123456789ABCDEF", "1111111111111111", "80C3F96196B08122"),
		("1111111111111111", "67452301EFCDAB89", "30C60C7DC71EDAAF"),
		("0000000000000000", "0000000000000000", "4597F94E78DD9861"),
		("FEDCBA9876543210", "67452301EFCDAB89", "0FABCE0A8DA2A0C6"),
		("7CA110454A1A6E57", "D0D6A10142677739", "4582C6592B2805EB"),
		("0131D9619DC1376E", "A84CD55CDA57EF3D", "0BCCB8B1A0090F25"),
		("07A1133E4A0B2686", "38D448027271F606", "77E53017A41DEA8B"),
		("3849674C2602319E", "584B45510A44DF2D", "56785EA2EB5126CF"),
		("04B915BA43FEB5B6", "3044FD42A27F5759", "B18238351A8FCE09"),
		("0113B970FD34F2CE", "085E9B053A14CF51", "88D0F4481899374C"),
		("0170F175468FB5E6", "E0D85607D2614777", "B793214398FC5189"),
		("43297FAD38E373FE", "B81425766A48BF29", "5441F013E51A9DD6"),
		("07A7137045DA2A16", "9011DD3B02283749", "93DAED2E799CD3FF"),
		("04689104C2FD3B2F", "685F95269A60AF35", "39E087D8E3A62D3C"),
		("37D06BB516CB7546", "405E4D163252274F", "4FD0995F6939165B"),
		("1F08260D1AC2465E", "186E056BCA5C9F75", "3B7A054A7B97D324"),
		("584023641ABA6176", "EFD64B0062601709", "C13120458EDAFAE4"),
		("025816164629B007", "00390D48F262E76E", "39AE5575BD879BF5"),
		("49793EBC79B3258F", "C8407543FA3C8F69", "9C5FC55319C09FB4"),
		("4FB05E1515AB73A7", "A0432D0792520777", "FA7B8E7AA3897E93"),
		("49E95D6D4CA229BF", "7755FE022AF11781", "7A5D9CCFB5AD8649"),
		("018310DC409B26D6", "505C9D1DC228F718", "90B2ABD178C78B65"),
		("1C587F1C13924FEF", "283255305A296F6D", "7437CB5501F23ED1"),
		("0101010101010101", "67452301EFCDAB89", "48EC34FAB268B247"),
		("1F1F1F1F0E0E0E0E", "67452301EFCDAB89", "517990A7AE3CEA08"),
		("E0FEE0FEF1FEF1FE", "67452301EFCDAB89", "2D079EC31D63AC9F"),
		("0000000000000000", "FFFFFFFFFFFFFFFF", "E0334901E4F6AFCD"),
		("FFFFFFFFFFFFFFFF", "0000000000000000", "779A1EF2BC491CB7"),
		("0123456789ABCDEF", "0000000000000000", "884659249A365457"),
		("FEDCBA9876543210", "FFFFFFFFFFFFFFFF", "9C5A5C6B5A0A9E5D"),
	)

	@classmethod
	def setUpClass(cls):
		"""
		Setup the test vectors and cipher objects.
		"""
		calculated_vectors = []
		for key, clear_text, cipher_text in cls.test_vectors:
			cipher = Blowfish()
			cipher.initialize(list(bytes.fromhex(key)))
			calculated_vectors.append(
				(
					cipher,
					key,
					clear_text,
					cipher_text
				)
			)
		cls.test_vectors = calculated_vectors

	def test_encode_block(self):
		"""
		Test encryption of blocks.
		"""
		for cipher, key, clear_text, cipher_text in self.test_vectors:
			with self.subTest(key=key, clear_text=clear_text):
				self.assertEqual(
					bytes(cipher.encode(bytes.fromhex(clear_text))),
					bytes.fromhex(cipher_text)
				)

	def test_decode_block(self):
		"""
		Test decryption of blocks.
		"""
		for cipher, key, clear_text, cipher_text in self.test_vectors:
			with self.subTest(key=key, cipher_text=cipher_text):
				self.assertEqual(
					bytes(cipher.decode(bytes.fromhex(cipher_text))),
					bytes.fromhex(clear_text)
				)

	def test_encode_empty(self):
		blowfish = Blowfish()
		empty_block = ""
		with self.subTest("empty_block"):
			self.assertIsNone(blowfish.encode(empty_block))
		with self.subTest("empty_block with length -1"):
			self.assertEqual(blowfish.encode_default(empty_block, length=-1), [])

	def test_encode_bad_length(self):
		blowfish = Blowfish()
		bad_block = "1"
		self.assertRaises(Exception, blowfish.encode, bad_block)

	def test_decode_bad_length(self):
		blowfish = Blowfish()
		bad_block_empty = ""
		bad_block_short = "1"
		bad_block_long = "0123456789"
		self.assertIsNone(blowfish.decode(bad_block_empty))
		self.assertIsNone(blowfish.decode(bad_block_short))
		self.assertIsNone(blowfish.decode(bad_block_long))

	def test_s_bad_number(self):
		blowfish = Blowfish()
		past_limit = [-1, 4]
		for i in past_limit:
			with self.subTest(f"blowfish past limit for s({i})"):
				self.assertRaises(IndexError, blowfish.s, 1, i)
