import unittest
from typing import List, Tuple

from silkroad_security_api_1_4.PacketReader import PacketReader
from silkroad_security_api_1_4.Security import OutputPacketList, OutputPacket


class BaseTestCase(unittest.TestCase):
    def assertOutputPacketsSimilar(
            self,
            left_packets: OutputPacketList,
            right_packets: OutputPacketList,
            read_sequence: List[Tuple[str, str]] = None,
    ):
        """
        Ensure packets we want to send are similar
        """
        self.assertIsInstance(left_packets, list)
        self.assertIsInstance(right_packets, list)

        self.assertEqual(len(left_packets), len(right_packets))
        for i, (c_packet, packet) in enumerate(zip(left_packets, right_packets)):
            with self.subTest(f"Comparing packet {i}"):
                # Get opcode
                self.assertOutputPacketSimilar(c_packet, packet, read_sequence)

    def assertOutputPacketSimilar(
            self,
            left_packet: OutputPacket,
            right_packet: OutputPacket,
            read_sequence: List[Tuple[str, str]] = None
    ):
        """
        Compare some bytes of two packets intended to be sent

        Sometimes the data can be different, so we won't be able to compare it

        :param: read_sequence: Which data types will be read from the PacketReader with their description
                               The description is to help with error messages
        """
        read_sequence = read_sequence or [
            ("data length", "uint16"),
            ("opcode", "uint16"),
            ("security bytes", "uint16"),
        ]

        self.assertIsInstance(left_packet, list)
        self.assertIsInstance(right_packet, list)

        left_packet_reader = PacketReader(left_packet)
        right_packet_reader = PacketReader(right_packet)
        for name, type_name in read_sequence:
            c_packet_item = getattr(left_packet_reader, f"read_{type_name}")()
            packet_item = getattr(right_packet_reader, f"read_{type_name}")()

            self.assertEqual(
                c_packet_item, packet_item,
                msg=f"{name} with {type_name} aren't equal"
            )


if __name__ == '__main__':
    unittest.main()
