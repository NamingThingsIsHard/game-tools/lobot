import unittest

from lobot_api.scripting.Area import Area


class TestArea(unittest.TestCase):
	def test_get_set_x(self):
		test_area = Area(0, 0, 0)
		self.assertEqual(test_area.x, 0)
		test_area.x = 1
		self.assertEqual(test_area.x, 1)

	def test_get_set_y(self):
		test_area = Area(0, 0, 0)
		self.assertEqual(test_area.y, 0)
		test_area.y = 1
		self.assertEqual(test_area.y, 1)

	def test_set_radius(self):
		test_area = Area(0, 0, 0)
		self.assertEqual(test_area.radius, 0)
		test_area.radius = 10
		self.assertEqual(test_area.radius, 10)

	def test_get_x_sector(self):
		test_area = Area(0, 0, 0)
		self.assertEqual(test_area.get_x_sector, 135)

	def test_get_y_sector(self):
		test_area = Area(0, 0, 0)
		self.assertEqual(test_area.get_y_sector, 92)

	def test_area_x_offset(self):
		test_area = Area(0, 0, 0)
		self.assertEqual(test_area.get_x_offset, 0)

	def test_area_y_offset(self):
		test_area = Area(0, 0, 0)
		self.assertEqual(test_area.get_y_offset, 0)

	def test_area_from_string(self):
		area_str = "0,0,0"
		test_area1 = Area(0, 0, 0)
		test_area_from_string = Area.from_string(area_str)
		with self.subTest("from_string"):
			self.assertEqual(test_area1, test_area_from_string)
			self.assertEqual(area_str, str(test_area_from_string))

	def test_reset_adjacency(self):
		test_area = Area(0, 0, 15)
		test_area.reset_adjacency_list()
		self.assertGreaterEqual(len(test_area.adjacency_list), 5)

	def test_area_radius0(self):
		test_area = Area(0, 0, 0)
		self.assertEqual(len(test_area.adjacency_list), 1)

	def test_area_radius15(self):
		test_area = Area(0, 0, 15)
		self.assertGreaterEqual(len(test_area.adjacency_list), 5)

	def test_area_radius50(self):
		test_area = Area(0, 0, 50)
		for k, v in test_area.adjacency_list.items():
			with self.subTest(f"test_area {k}"):
				self.assertTrue(k not in v, "Self-reference of adjacency point found. ")

		with self.subTest(f"test_area entries"):
			self.assertGreaterEqual(len(test_area.adjacency_list), 5)


if __name__ == '__main__':
	unittest.main()
