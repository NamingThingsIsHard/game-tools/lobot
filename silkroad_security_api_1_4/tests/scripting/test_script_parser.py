import os.path
import unittest
from pathlib import Path

from lobot_api.scripting.ScriptExpression import CommandExpression, ExpressionParser


class TestScriptParser(unittest.TestCase):
	all_commands = [
		CommandExpression("go", ["0", "0", "0"]),
		CommandExpression("go", ["0", "0"]),
		CommandExpression("store"),
		CommandExpression("stable"),
		CommandExpression("repair"),
		CommandExpression("potions"),
		CommandExpression("special"),
		CommandExpression("wait"),
		CommandExpression("wait", ["1000"]),
		CommandExpression("teleport", ["reverseRecall"]),
		CommandExpression("quest", ["1000", "1"]),
		CommandExpression("mount"),
		CommandExpression("dismount"),
		CommandExpression("setArea1", ["0", "0", "0"]),
		CommandExpression("setArea2", ["0", "0", "0"])
	]

	def test_all_commands(self):
		length_all_commands = len(self.all_commands)
		directory = Path(os.path.dirname(os.path.realpath(__file__))).parent
		script_path = directory / "fixtures" / "scripts" / "command_all_commands.txt"

		with self.subTest("parse_script_lines"):
			with open(script_path) as f:
				lines = f.read().splitlines()
				result = ExpressionParser.parse_script_lines(lines)
				self.assertEqual(len(result), length_all_commands)

		with self.subTest("parse_script"):
			result = ExpressionParser.parse_script(str(script_path))
			self.assertEqual(len(result), length_all_commands)

		for index, command in enumerate(self.all_commands):
			with self.subTest(f"{command.command_name} {command.arguments}"):
				self.assertEqual(command, self.all_commands[index])  # add assertion here

	def test_parse_empty_file_path(self):
		self.assertIsNone(ExpressionParser.parse_script(""))


if __name__ == '__main__':
	unittest.main()
