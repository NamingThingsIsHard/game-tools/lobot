from lobot_api.Bot import Bot
from lobot_api.globals.settings.TrainingSettings import TrainingAreaSettings
from lobot_api.scripting import ScriptUtils
from lobot_api.scripting.Area import Area
from lobot_api.scripting.Point import Point
from lobot_api.scripting.ScriptExpression import CommandExpression, CommentExpression
from silkroad_security_api_1_4.tests.packet_tests.packet_test import PacketTest


class TestScriptUtils(PacketTest):
	area = Area(0, 0, 50)
	area_str = "0, 0, 50"
	area_str2 = "-1000, -1000, 50"
	point = Point(168, 92, 0, 0, 0)
	point2 = Point.from_absolute_coords(0, 0, 0)
	go1 = CommandExpression('go', ['0', '0'])
	go2 = CommandExpression('go', ['0', '10'])
	stable = CommandExpression("stable")
	comment = CommentExpression("# Test")

	def test_get_distance_go_gommand(self):
		self.assertEqual(ScriptUtils.get_distance_go_gommand(self.go1, self.go2), 10)

	def test_get_char_distance_bot_to_area(self):
		Bot.char_data.position = self.point
		self.assertGreater(ScriptUtils.get_char_distance_bot_to_area(self.area), 100)

	def test_get_distance_to_area(self):
		Bot.char_data.position = self.point
		self.assertGreater(ScriptUtils.get_distance_to_area(self.area, self.point), 100)

	def test_distance_to_training_area1(self):
		Bot.char_data.position = self.point
		TrainingAreaSettings.instance().area1 = self.area_str
		self.assertGreater(ScriptUtils.distance_to_training_area1(), 100)

	def test_distance_to_training_area2(self):
		Bot.char_data.position = self.point
		TrainingAreaSettings.instance().area2 = self.area_str
		self.assertGreater(ScriptUtils.distance_to_training_area2(), 100)

	def test_distance_to_closest_area(self):
		Bot.char_data.position = self.point
		TrainingAreaSettings.instance().area1 = self.area_str
		self.assertGreater(ScriptUtils.distance_to_closest_area(), 100)

	def test_is_char_in_area1(self):
		Bot.char_data.position = self.point
		TrainingAreaSettings.instance().area1 = self.area_str
		self.assertFalse(ScriptUtils.is_char_in_area1())
		Bot.char_data.position = self.point2
		self.assertTrue(ScriptUtils.is_char_in_area1())

	def test_is_char_in_area2(self):
		Bot.char_data.position = self.point
		TrainingAreaSettings.instance().area2 = self.area_str
		self.assertFalse(ScriptUtils.is_char_in_area2())
		Bot.char_data.position = Point.from_absolute_coords(0, 0, 0)
		self.assertTrue(ScriptUtils.is_char_in_area2())

	def test_is_point_in_any_area(self):
		TrainingAreaSettings.instance().area2 = self.area_str
		self.assertFalse(ScriptUtils.is_point_in_any_area(self.point))
		self.assertTrue(ScriptUtils.is_point_in_any_area(self.point2))

	def test_is_position_in_closest_area(self):
		TrainingAreaSettings.instance().area2 = self.area_str
		self.assertFalse(ScriptUtils.is_position_in_closest_area(self.point))
		self.assertTrue(ScriptUtils.is_position_in_closest_area(self.point2))

	def test_closest_area_to_char(self):
		TrainingAreaSettings.instance().area1 = self.area_str
		TrainingAreaSettings.instance().area2 = self.area_str2
		self.assertEqual(ScriptUtils.get_closest_area_to_position(self.point), self.area)

	def test_get_closest_area_to_position(self):
		TrainingAreaSettings.instance().area2 = self.area_str
		self.assertEqual(ScriptUtils.closest_area_to_char(), self.area)

	def test_generate_position_in_area1(self):
		TrainingAreaSettings.instance().area1 = self.area_str
		p = ScriptUtils.generate_position_in_area1()
		self.assertLessEqual(ScriptUtils.get_distance_to_area(self.area, p), self.area.radius)

	def test_generate_position_in_area2(self):
		TrainingAreaSettings.instance().area2 = self.area_str
		p = ScriptUtils.generate_position_in_area2()
		self.assertLessEqual(ScriptUtils.get_distance_to_area(self.area, p), self.area.radius)

	def test_get_walk_position_within_closest_area(self):
		TrainingAreaSettings.instance().area1 = self.area_str
		p = ScriptUtils.get_walk_position_within_closest_area()
		self.assertLessEqual(ScriptUtils.get_distance_to_area(self.area, p), self.area.radius)

	def test_get_x_coord(self):
		self.assertEqual(ScriptUtils.get_x_coord(100, 135), 10)

	def test_get_y_coord(self):
		self.assertEqual(ScriptUtils.get_y_coord(100, 92), 10)

	def test_get_x_coord_origin(self):
		self.assertEqual(ScriptUtils.get_x_coord_origin(1000, 135), 10)

	def test_get_y_coord_origin(self):
		self.assertEqual(ScriptUtils.get_y_coord_origin(1000, 92), 10)

	def test_get_sector_x(self):
		self.assertEqual(ScriptUtils.get_sector_x(0), 135)

	def test_get_sector_y(self):
		self.assertEqual(ScriptUtils.get_sector_y(0), 92)

	def test_get_first_walk_position(self):
		with self.subTest("command expressions"):
			self.assertEqual(ScriptUtils.get_first_walk_position([self.go1, self.go2]), self.point2)
		with self.subTest("no walk points"):
			self.assertRaises(ValueError, ScriptUtils.get_first_walk_position, [])

	def test_get_first_walk_point_index(self):
		with self.subTest("command expressions"):
			self.assertEqual(ScriptUtils.get_first_walk_point_index([self.go1, self.go2]), 0)
		with self.subTest("no walk points"):
			self.assertEqual(ScriptUtils.get_first_walk_point_index([]), -1)

	def test_total_distance(self):
		with self.subTest("commands and comments"):
			self.assertEqual(ScriptUtils.total_distance([self.comment, self.go1, self.go2]), 10)
		with self.subTest("no commands"):
			self.assertEqual(ScriptUtils.total_distance([]), 0)

	def test_index_of_closest_go_command(self):
		with self.subTest("commands and comments"):
			self.assertEqual(ScriptUtils.index_of_closest_go_command(self.point2, [self.go1, self.comment, self.stable, self.go2]), 0)
		with self.subTest("no commands"):
			self.assertEqual(ScriptUtils.index_of_closest_go_command(self.point2, []), -1)

	def test_index_of_closest_point(self):
		self.assertEqual(ScriptUtils.index_of_closest_point(self.point2, [self.point, self.point2]), 1)
