import unittest

from lobot_api.scripting.ScriptExpression import MetaExpression, CommandExpression


class TestCommandExpression(unittest.TestCase):
	valid_test_and_expected_result = [
		{
			"name": "test_valid_lines",
			"test": [
				"go 0,0,0",
				"go 0,0",
				"wait",
				"wait 1000",
				"teleport reverseRecall",
				"quest 1000,1",
				"mount",
				"dismount",
				"setArea1 0,0,0",
				"setArea2 0,0,0"
			],
			"expected": [
				CommandExpression("go", ["0", "0", "0"]),
				CommandExpression("go", ["0", "0"]),
				CommandExpression("wait"),
				CommandExpression("wait", ["1000"]),
				CommandExpression("teleport", ["reverseRecall"]),
				CommandExpression("quest", ["1000", "1"]),
				CommandExpression("mount"),
				CommandExpression("dismount"),
				CommandExpression("setArea1", ["0", "0", "0"]),
				CommandExpression("setArea2", ["0", "0", "0"])
			]
		},
		{
			"name": "test_valid_lines_spaces",
			"test": [
				"go 0 ,0,0",
				"go 0, 0",
				" wait",
				"wait  1000",
				"teleport reverseRecall ",
				"quest 1000,1 ",
				" mount",
				"dismount ",
				"setArea1  0, 0, 0",
				"setArea2 0 , 0 , 0"
			],
			"expected": [
				CommandExpression("go", ["0", "0", "0"]),
				CommandExpression("go", ["0", "0"]),
				CommandExpression("wait"),
				CommandExpression("wait", ["1000"]),
				CommandExpression("teleport", ["reverseRecall"]),
				CommandExpression("quest", ["1000", "1"]),
				CommandExpression("mount"),
				CommandExpression("dismount"),
				CommandExpression("setArea1", ["0", "0", "0"]),
				CommandExpression("setArea2", ["0", "0", "0"])
			]
		},
		{
			"name": "test_valid_lines_comments",
			"test": [
				"go 0,0,0 //one",
				"go 0,0 // two",
				"wait // //",
				"wait 1000 ////",
				"teleport reverseRecall // // another one",
				"quest 1000,1 // all i do is bot",
				"mount // comment //",
				"dismount // oh // baby // a triple",
				"setArea1 0,0,0 // romcomment",
				"setArea2 0,0,0 // // // //"
			],
			"expected": [
				CommandExpression("go", ["0", "0", "0"]),
				CommandExpression("go", ["0", "0"]),
				CommandExpression("wait"),
				CommandExpression("wait", ["1000"]),
				CommandExpression("teleport", ["reverseRecall"]),
				CommandExpression("quest", ["1000", "1"]),
				CommandExpression("mount"),
				CommandExpression("dismount"),
				CommandExpression("setArea1", ["0", "0", "0"]),
				CommandExpression("setArea2", ["0", "0", "0"])
			]
		},
		{
			"name": "test_teleport",
			"test": [
				"teleport 1,1",
				"teleport 0,0",
				"teleport 1000,0",
				"teleport 1000,1",
				"teleport 1,0",
				"teleport reverseRecall",
				"teleport reverseDeath",
				"teleport reverseLocation location",
			],
			"expected": [
				CommandExpression("teleport", ["1", "1"]),
				CommandExpression("teleport", ["0", "0"]),
				CommandExpression("teleport", ["1000", "0"]),
				CommandExpression("teleport", ["1000", "1"]),
				CommandExpression("teleport", ["1", "0"]),
				CommandExpression("teleport", ["reverseRecall"]),
				CommandExpression("teleport", ["reverseDeath"]),
				CommandExpression("teleport", ["reverseLocation", "location"])
			]
		}
	]

	invalid_test = [
		{
			"name": "test_invalid_teleporter",
			"test": [
				"teleport 1,-1",
				"teleport 0,-0",
				"teleport -1000,0",
				"teleport -1000,1",
				"teleport 1,",
				"teleport rreverseRecall",
				"teleport reverseDeathh",
				"teleport reverseLocation locationn",
			]
		},
		{
			"name": "test_invalid_low_bound",
			"test": [
				"StartingTown angan",
				"MonsterLevel -1",
				"MonsterDifficulty -1",
				"ScriptDifficulty -1",
				"NextScript -1",  # MonsterLevel(,MonsterDifficulty)?;
				"NextScript -1,-1",
			]
		},
		{
			"name": "test_valid_lines_comments",
			"test": [
				"go 0,0,-1",
				"go 0,0,",
				"shoop",
				"waitt",
				"wait -1000",
				"teleport teleporter 1000,-1",
				"quest 1000,-1",
				"mmount",
				"dismount 1",
				"setArea1 0,0,-1",
				"setArea2 0,0,-1"
			]
		}
	]

	def test_valid_lines(self):
		for test in self.valid_test_and_expected_result:
			with self.subTest(test["name"]):
				result = [CommandExpression(s) for s in test["test"]]

				for index, r in enumerate(result):
					with self.subTest(test["expected"][index].command_name):
						expected = test["expected"][index]
						self.assertEqual(r, test["expected"][index])
					with self.subTest(f"execute {test['expected'][index].command_name}"):
						self.assertRaises(NotImplementedError, expected.execute)

	def test_str(self):
		expr = MetaExpression("StartingTown Jangan")
		self.assertGreater(len(str(expr)), 1)

	def test_invalid_lines(self):
		for test in self.invalid_test:
			for s in test["test"]:
				with self.subTest(test["name"]):
					self.assertRaises(SyntaxError, MetaExpression, s)


if __name__ == '__main__':
	unittest.main()
