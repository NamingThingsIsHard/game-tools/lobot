import unittest

from lobot_api.scripting.ScriptExpression import CommentExpression


class TestCommentExpression(unittest.TestCase):
	expr = "# test"

	def test_expression_valid(self):
		result = CommentExpression(self.expr)
		self.assertIsInstance(result, CommentExpression)

	def test_expression_invalid(self):
		expr = " test"
		self.assertRaises(SyntaxError, CommentExpression, expr)

	def test_expression_not_equal(self):
		expr = "# test "
		result = CommentExpression(expr)
		expected = CommentExpression(self.expr)
		self.assertNotEqual(result, expected)


if __name__ == '__main__':
	unittest.main()
