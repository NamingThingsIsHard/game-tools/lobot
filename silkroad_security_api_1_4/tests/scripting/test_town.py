import unittest

from lobot_api.scripting import Town
from lobot_api.scripting.Point import Point


class TestMetaExpression(unittest.TestCase):
	point_in_town = [
		Point.from_absolute(6434, 1121),
		Point.from_absolute(3549, 2070),
		Point.from_absolute(113, 47),
		Point.from_absolute(-5183, 2892),
		Point.from_absolute(-10683, 2584),
		Point.from_absolute(-16166, 4),
		Point.from_absolute(-16580, 292),
	]

	point_not_in_town = [
		Point.from_absolute(7434, 1121),
		Point.from_absolute(4549, 2070),
		Point.from_absolute(513, 47),
		Point.from_absolute(-6183, 2892),
		Point.from_absolute(-11683, 2584),
		Point.from_absolute(-17166, 4),
		Point.from_absolute(-17580, 292),
	]

	def test_points_in_town(self):
		for p in self.point_in_town:
			with self.subTest(str(p)):
				self.assertIsNotNone(Town.current_town(p))

	def test_str(self):
		jangan = Town.Towns["jangan"]
		self.assertIn("Jangan", str(jangan))
		self.assertIn("Jangan", jangan.__repr__())

	def test_points_not_in_town(self):
		Town.Towns["test"] = Town.Town("Test", Point.from_absolute(6434, 1121), [], Town.TownScriptJangan)
		for p in self.point_not_in_town:
			with self.subTest(str(p)):
				self.assertIsNone(Town.current_town(p))


if __name__ == '__main__':
	unittest.main()
