import unittest

from lobot_api.scripting.ScriptExpression import MetaExpression


class TestMetaExpression(unittest.TestCase):
	valid_test_and_expected_result = [
		{
			"name": "test_valid_lines",
			"test": [
				"StartingTown Jangan",
				"MonsterLevel 1",
				"MonsterDifficulty 1",
				"ScriptDifficulty 1",
				"NextScript 1",  # MonsterLevel(,MonsterDifficulty)?;
				"NextScript 1,1"  # MonsterLevel(,MonsterDifficulty)?;
			],
			"expected": [
				MetaExpression("StartingTown Jangan"),
				MetaExpression("MonsterLevel 1"),
				MetaExpression("MonsterDifficulty 1"),
				MetaExpression("ScriptDifficulty 1"),
				MetaExpression("NextScript 1"),
				MetaExpression("NextScript 1 1")
			]
		},
		{
			"name": "test_valid_lines_spaces",
			"test": [
				"StartingTown  Jangan",
				"MonsterLevel  1",
				"MonsterDifficulty  1",
				"ScriptDifficulty  1",
				"NextScript  1",  # MonsterLevel(,MonsterDifficulty)?;
				"NextScript  1 , 1"  # MonsterLevel(,MonsterDifficulty)?;
			],
			"expected": [
				MetaExpression("StartingTown Jangan"),
				MetaExpression("MonsterLevel 1"),
				MetaExpression("MonsterDifficulty 1"),
				MetaExpression("ScriptDifficulty 1"),
				MetaExpression("NextScript 1"),
				MetaExpression("NextScript 1 1")
			]
		},
		{
			"name": "test_valid_lines_comments",
			"test": [
				"StartingTown Jangan // one",
				"MonsterLevel 1 //two",
				"MonsterDifficulty 1 //three ",
				"ScriptDifficulty 1 // four //",
				"NextScript 1 // five // //",
				"NextScript 1,1 ////"
			],
			"expected": [
				MetaExpression("StartingTown Jangan"),
				MetaExpression("MonsterLevel 1"),
				MetaExpression("MonsterDifficulty 1"),
				MetaExpression("ScriptDifficulty 1"),
				MetaExpression("NextScript 1"),
				MetaExpression("NextScript 1 1")
			]
		}
	]

	invalid_test = [
		{
			"name": "test_invalid",
			"test": [
				"StartingTownJangan",
				"MonsterLevell 1",
				"Monster Difficulty 1",
				"ScriptDifficult 1",
				"NNextScript 1",  # MonsterLevel(,MonsterDifficulty)?;
				"NextScript 101,11,22,33"
			]
		},
		{
			"name": "test_invalid_low_bound",
			"test": [
				"StartingTown angan",
				"MonsterLevel -1",
				"MonsterDifficulty -1",
				"ScriptDifficulty -1",
				"NextScript -1",  # MonsterLevel(,MonsterDifficulty)?;
				"NextScript -1,-1",
			]
		},
		{
			"name": "test_valid_lines_comments",
			"test": [
				"StartingTown angan",
				"MonsterLevel 101",
				"MonsterDifficulty 11",
				"ScriptDifficulty 11",
				"NextScript 101",  # MonsterLevel(,MonsterDifficulty)?;
				"NextScript 101,11",
			]
		}
	]

	def test_valid_lines(self):
		for test in self.valid_test_and_expected_result:
			with self.subTest(test["name"]):
				result = [MetaExpression(s) for s in test["test"]]

				for index, r in enumerate(result):
					with self.subTest(test["expected"][index].expression_name):
						expected = test["expected"][index]
						self.assertEqual(r, test["expected"][index])
					with self.subTest(f"execute {test['expected'][index].expression_name}"):
						self.assertRaises(NotImplementedError, expected.execute)

	def test_str(self):
		expr = MetaExpression("StartingTown Jangan")
		self.assertGreater(len(str(expr)), 1)

	def test_invalid_lines(self):
		for test in self.invalid_test:
			for s in test["test"]:
				with self.subTest(test["name"]):
					self.assertRaises(SyntaxError, MetaExpression, s)


if __name__ == '__main__':
	unittest.main()
