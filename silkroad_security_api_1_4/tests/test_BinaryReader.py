import array
import struct
import unittest
from typing import Any

from silkroad_security_api_1_4.BinaryReader import BinaryReader
from silkroad_security_api_1_4.SeekType import SeekType


class TestReset(unittest.TestCase):
    """
    Test the reset method
    """

    def setUp(self) -> None:
        self.reader = BinaryReader([])

    def test_bad_data(self):
        self.assertRaises(TypeError, self.reader.reset, None)

    def test_array_array(self):
        input_array = array.array("i", [1, 2, 3, 4, 5, 6])
        self.reader.reset(input_array)
        self.assertEqual(self.reader.data, input_array)
        self.assertEqual(self.reader.offset, 0)

    def test_list(self):
        _list = [1, 2, 3, 4, 5, 6]
        self.reader.reset(_list)

        self.assertIsInstance(self.reader.data, array.array)
        self.assertEqual(self.reader.size, len(_list))
        self.assertEqual(self.reader.offset, 0)

    def test_bytes(self):
        _bytes = bytes([1, 2, 3, 4, 5, 6])
        self.reader.reset(_bytes)

        self.assertIsInstance(self.reader.data, array.array)
        self.assertEqual(self.reader.size, len(_bytes))
        self.assertEqual(self.reader.offset, 0)


class TestSeekMethods(unittest.TestCase):
    """
    Test methods prefixed seek_
    """

    def setUp(self) -> None:
        self.reader = BinaryReader([1, 2, 3, 4, 5, 6])

    def test_seek__begin(self):
        # Move to position 2
        self.reader.seek(2)
        self.assertEqual(self.reader.offset, 2)
        self.reader.seek(2)
        self.assertEqual(self.reader.offset, 2)

    def test_seek__current(self):
        # Move 2 forward
        self.reader.seek(2, SeekType.Current)
        self.assertEqual(self.reader.offset, 2)

        self.reader.seek(2, SeekType.Current)
        self.assertEqual(self.reader.offset, 4)

    def test_seek__end(self):
        # Move 2 backwards from the end
        self.reader.seek(2, SeekType.End)
        self.assertEqual(self.reader.offset, 4)

        self.reader.seek(1, SeekType.End)
        self.assertEqual(self.reader.offset, 5)

    def test_seek___begin_bad_offset(self):
        self.assertRaises(IndexError, self.reader.seek, 10)
        self.assertRaises(IndexError, self.reader.seek, -10)

    def test_seek___current_bad_offset(self):
        self.assertRaises(IndexError, self.reader.seek, -1, SeekType.Current)

    def test_seek___end_bad_offset(self):
        self.assertRaises(IndexError, self.reader.seek, -1, SeekType.End)

    def test_seek__bad_origin(self):
        self.assertRaises(ValueError, self.reader.seek, 1, None)

    def test_set_good_offset(self):
        expected_offset = 3
        self.reader.seek_set(expected_offset)
        self.assertEqual(self.reader.offset, expected_offset)

    def test_set_bad_offset(self):
        self.assertRaises(IndexError, self.reader.seek_set, 10)

    def test_forward_good_offset(self):
        self.reader.seek_forward(3)
        self.assertEqual(self.reader.offset, 3)

    def test_forward_bad_offset(self):
        self.assertRaises(IndexError, self.reader.seek_forward, 10)
        self.assertRaises(IndexError, self.reader.seek_forward, -10)

    def test_backward_good_offset(self):
        self.reader.seek_set(5)
        self.reader.seek_backward(4)
        self.assertEqual(self.reader.offset, 1)

    def test_backward_bad_offset(self):
        self.assertRaises(IndexError, self.reader.seek_backward, 10)
        self.assertRaises(IndexError, self.reader.seek_backward, -10)


class TestReadMethods(unittest.TestCase):

    def _test_read_func(self, reader, type_name: str, expected_read_count: int, expected_value: Any):
        self.assertReadIsGood(reader, type_name, expected_read_count, expected_value)
        self.assertReadIsOutOfBounds(type_name)

    def assertReadIsOutOfBounds(self, type_name: str):
        reader = BinaryReader([])
        self.assertRaises(EOFError, getattr(reader, f"read_{type_name}"))

    def assertReadIsGood(self, reader: BinaryReader, type_name: str, expected_read_count: int, expected_value: Any):
        func = getattr(reader, f"read_{type_name}")

        old_offset = reader.offset
        result = func()
        self.assertEqual(result, expected_value)

        self.assertEqual(reader.offset, old_offset + expected_read_count)

    def test_int8(self):
        reader = BinaryReader([50])
        self._test_read_func(reader, "int8", 1, 50)

    def test_uint8(self):
        reader = BinaryReader([50])
        self._test_read_func(reader, "uint8", 1, 50)

    def test_byte(self):
        reader = BinaryReader([128])
        self._test_read_func(reader, "byte", 1, 128)

    def test_int16(self):
        # Max positive value of signed 16bit int
        reader = BinaryReader([255, 127])
        self._test_read_func(reader, "int16", 2, 2 ** 15 - 1)

    def test_uint16(self):
        # Max value of unsigned 16bit int
        reader = BinaryReader([255, 255])
        self._test_read_func(reader, "uint16", 2, 2 ** 16 - 1)

    def test_int32(self):
        # Max value of signed 32bit int
        reader = BinaryReader([255] * 3 + [127])
        self._test_read_func(reader, "int32", 4, 2 ** 31 - 1)

    def test_uint32(self):
        # Max value of unsigned 32bit int
        reader = BinaryReader([255] * 4)
        self._test_read_func(reader, "uint32", 4, 2 ** 32 - 1)

    def test_int64(self):
        # Max value of signed 64bit int
        reader = BinaryReader([255] * 7 + [127])
        self._test_read_func(reader, "int64", 8, 2 ** 63 - 1)

    def test_uint64(self):
        # Max value of unsigned 64bit int
        reader = BinaryReader([255] * 8)
        self._test_read_func(reader, "uint64", 8, 2 ** 64 - 1)

    def test_float(self):
        value = 19.8391
        reader = BinaryReader(struct.pack("f", value))
        old_offset = reader.offset

        read_value = reader.read_float()
        self.assertAlmostEqual(read_value, value, 4)
        self.assertEqual(reader.offset, old_offset + 4)

        self.assertReadIsOutOfBounds("float")

    def test_double(self):
        value = 19.83914711113333344555555
        reader = BinaryReader(struct.pack("d", value))
        old_offset = reader.offset

        read_value = reader.read_double()
        self.assertAlmostEqual(read_value, value, 23)
        self.assertEqual(reader.offset, old_offset + 8)

        self.assertReadIsOutOfBounds("double")

    def test_char(self):
        reader = BinaryReader(b'9')
        self._test_read_func(reader, "char", 1, b'9')


class TestReadMethodsWithLengthParam(unittest.TestCase):

    def _test_read_func(
            self, reader: BinaryReader, type_name: str, length: int, expected_read_count: int, expected_value: Any
    ):
        self.assertReadIsGood(reader, type_name, length, expected_read_count, expected_value)
        self.assertReadIsOutOfBounds(type_name)

    def assertReadIsOutOfBounds(self, type_name: str):
        reader = BinaryReader([])
        self.assertRaises(EOFError, getattr(reader, f"read_{type_name}"), length=10)

    def assertReadIsGood(
            self, reader: BinaryReader, type_name: str, length: int, expected_read_count: int, expected_value: Any
    ):
        func = getattr(reader, f"read_{type_name}")

        old_offset = reader.offset
        result = func(length)
        self.assertEqual(result, expected_value)

        self.assertEqual(reader.offset, old_offset + expected_read_count)

    def test_bytes(self):
        value = b'hello there'
        reader = BinaryReader(value)
        self._test_read_func(reader, "bytes", len(value), len(value), value)

    def test_ascii(self):
        reader = BinaryReader(b'hello there')
        self._test_read_func(reader, "ascii", len(b'hello there'), len(b'hello there'), 'hello there')

    def test_utf16(self):
        string = "hello there"
        value = string.encode("utf-16le")
        reader = BinaryReader(value)
        self._test_read_func(
            reader,
            "utf16",
            len(string),
            len(string) * 2,  # python uses utf-8, utf-16 uses double as much space
            string
        )

    def test_utf32(self):
        string = "hello there"
        value = string.encode("utf-32le")
        reader = BinaryReader(value)
        self._test_read_func(
            reader,
            "utf32",
            len(string),
            len(string) * 4,  # python uses utf-8, utf-32 uses quadruple the space
            string
        )


if __name__ == '__main__':
    unittest.main()
