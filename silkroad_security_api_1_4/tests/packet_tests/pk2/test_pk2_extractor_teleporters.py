import unittest

from lobot_api.globals.DirectoryGlobal import DirectoryGlobal
from lobot_api.globals.PK2DataGlobal import PK2DataGlobal
from lobot_api.pk2.extractors.PK2ExtractorTeleporters import PK2ExtractorTeleporters
from silkroad_security_api_1_4.tests.fixtures import pk2_data_loader_util


class TestPK2ExtractorTeleporters(unittest.TestCase):
	def setUp(self) -> None:
		PK2DataGlobal.teleporters.clear()

	def test_load_file(self):
		DirectoryGlobal.teleporter_data = lambda: pk2_data_loader_util.get_data_folder_path() / "teleporters.txt"
		try:
			PK2ExtractorTeleporters().load_from_txt_file()
		except Exception as e:
			self.fail(e)


if __name__ == '__main__':
	unittest.main()
