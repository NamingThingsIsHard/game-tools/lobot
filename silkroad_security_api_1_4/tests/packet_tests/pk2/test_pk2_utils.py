import contextlib
import io
import os
import unittest

from lobot_api.Bot import Bot
from lobot_api.globals.DirectoryGlobal import DirectoryGlobal
from lobot_api.globals.PK2DataGlobal import PK2DataGlobal
from lobot_api.pk2.PK2Class import PK2Class
from lobot_api.pk2.PK2Main import PK2Main
from lobot_api.pk2.PK2Utils import PK2Utils
from lobot_api.pk2.extractors.PK2ExtractorItems import PK2ExtractorItems
from lobot_api.pk2.extractors.PK2ExtractorLevelData import PK2ExtractorLevelData
from lobot_api.pk2.extractors.PK2ExtractorMagParams import PK2ExtractorMagParams
from lobot_api.pk2.extractors.PK2ExtractorNPCs import PK2ExtractorNPCs
from lobot_api.pk2.extractors.PK2ExtractorNames import PK2ExtractorNames
from lobot_api.pk2.extractors.PK2ExtractorShops import PK2ExtractorShops
from lobot_api.pk2.extractors.PK2ExtractorSkills import PK2ExtractorSkills
from lobot_api.pk2.extractors.PK2ExtractorTeleporters import PK2ExtractorTeleporters
from lobot_api.pk2.extractors.PK2PartyTitleExtractor import PK2PartyTitleExtractor
from silkroad_security_api_1_4.tests.fixtures import pk2_data_loader_util


class TestPK2Utils(unittest.TestCase):
	data_folder = pk2_data_loader_util.get_data_folder_path() / "data"
	item_data = {}
	level_data = {}
	mag_param_data = {}
	name_data = {}
	npc_data = {}
	shop_data = {}
	skill_data = {}
	teleporter_data = {}

	@classmethod
	def setUpClass(cls) -> None:
		super().setUpClass()
		minimal_pk2_path = pk2_data_loader_util.get_data_folder_path() / "Media.pk2"
		# set these to test directories
		PK2DataGlobal.pk2_media_file_path = lambda: minimal_pk2_path
		DirectoryGlobal.data_folder = lambda: cls.data_folder
		DirectoryGlobal.item_data = lambda: cls.data_folder / "items.txt"
		DirectoryGlobal.level_data = lambda: cls.data_folder / "levels.txt"
		DirectoryGlobal.mag_param_data = lambda: cls.data_folder / "magoptions.txt"
		DirectoryGlobal.name_data = lambda: cls.data_folder / "names.txt"
		DirectoryGlobal.npc_data = lambda: cls.data_folder / "npcs.txt"
		DirectoryGlobal.shop_data = lambda: cls.data_folder / "shops.txt"
		DirectoryGlobal.skill_data = lambda: cls.data_folder / "skills.txt"
		DirectoryGlobal.teleporter_data = lambda: cls.data_folder / "teleporters.txt"

		PK2Main.s_pK2Class = PK2Class(str(minimal_pk2_path))

		cls.item_data = PK2DataGlobal.items
		cls.level_data = PK2DataGlobal.level_data
		cls.mag_param_data = PK2DataGlobal.mag_params
		cls.name_data = PK2DataGlobal.names
		cls.npc_data = PK2DataGlobal.npcs
		cls.shop_data = PK2DataGlobal.shops
		cls.skill_data = PK2DataGlobal.skills
		cls.teleporter_data = PK2DataGlobal.teleporters

		Bot.reset()

	@classmethod
	def tearDownClass(cls) -> None:
		PK2Main.s_pK2Class = None
		for root, dirs, files in os.walk(cls.data_folder):
			for file in files:
				os.remove(os.path.join(root, file))
			for d in dirs:
				os.rmdir(os.path.join(root, d))
			os.rmdir(root)

		PK2DataGlobal.items = cls.item_data
		PK2DataGlobal.level_data = cls.level_data
		PK2DataGlobal.mag_params = cls.mag_param_data
		PK2DataGlobal.names = cls.name_data
		PK2DataGlobal.npcs = cls.npc_data
		PK2DataGlobal.shops = cls.shop_data
		PK2DataGlobal.skills = cls.skill_data
		PK2DataGlobal.teleporters = cls.teleporter_data

	def test_extract_all(self):
		with contextlib.redirect_stdout(io.StringIO()):
			try:
				with self.subTest("setup"):
					PK2Utils.setup(delete_old_info=True)
					PK2Utils.extract_all_information()

				with self.subTest("names"):
					PK2ExtractorNames().extract_and_map_data()  # Has to be parsed first for all others to use
				with self.subTest("levels"):
					PK2ExtractorLevelData().extract_and_map_data()  # Quick and simple
				with self.subTest("blues"):
					PK2ExtractorMagParams().extract_and_map_data()  # Quick and simple
				with self.subTest("party titles"):
					PK2PartyTitleExtractor().extract_and_map_data()  # Quick and simple
				with self.subTest("skills"):
					PK2ExtractorSkills().extract_and_map_data()  # Most time consuming due to encryption
				with self.subTest("items"):
					PK2ExtractorItems().extract_and_map_data()  # Parse before Shops
				with self.subTest("npcs"):
					PK2ExtractorNPCs().extract_and_map_data()  # Parse before Shops (and Teleporters?)
				with self.subTest("teleporters"):
					PK2ExtractorTeleporters().extract_and_map_data()  # So far Independent from NPCs
				with self.subTest("shops"):
					PK2ExtractorShops().extract_and_map_data()  # Depends on Items and (possibly NPCS in some cases?)
			except Exception as e:
				self.fail(e)


if __name__ == '__main__':
	unittest.main()
