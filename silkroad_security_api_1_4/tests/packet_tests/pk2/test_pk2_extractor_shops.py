import unittest

from lobot_api.globals.DirectoryGlobal import DirectoryGlobal
from lobot_api.globals.PK2DataGlobal import PK2DataGlobal
from lobot_api.pk2.extractors.PK2ExtractorShops import PK2ExtractorShops
from silkroad_security_api_1_4.tests.fixtures import pk2_data_loader_util


class TestPK2ExtractorShops(unittest.TestCase):
	def setUp(self) -> None:
		PK2DataGlobal.shops.clear()

	def test_load_file(self):
		DirectoryGlobal.shop_data = lambda: pk2_data_loader_util.get_data_folder_path() / "shops.txt"
		try:
			PK2ExtractorShops().load_from_txt_file()
		except Exception as e:
			self.fail(e)


if __name__ == '__main__':
	unittest.main()
