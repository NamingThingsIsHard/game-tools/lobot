import unittest

from lobot_api.packets.storage.StorageInfoDataHandler import StorageInfoDataHandler
from silkroad_security_api_1_4.tests.packet_tests.packet_test import PacketTest


class TestStorageInfoData(PacketTest):
	tempBotUniqueID = 0xFD0E34

	@classmethod
	def setUpClass(cls) -> None:
		super().setUpClass()
		cls.load_test_fixture(cls.get_fixtures_folder_path() / "inventory" / "0x3049_CharStorageHandler.json")
		cls.handler = StorageInfoDataHandler()
		for setup_packet in cls.class_setup_packets:
			cls.handler.handle(setup_packet)

	def test_loop_all(self):
		self.loop_all()


if __name__ == '__main__':
	unittest.main()
