import unittest

from lobot_api.Bot import Bot
from lobot_api.packets.Handling import Handling
from lobot_api.packets.inventory.ItemMovementType import ItemMovementType
from lobot_api.packets.inventory.MoveItem import MoveItem
from lobot_api.packets.inventory.UseItem import UseItem
from lobot_api.packets.inventory.UseItemType import UseItemType
from lobot_api.structs.CharData import CharData
from lobot_api.structs.Item import Item
from lobot_api.structs.Monster import Monster
from lobot_api.structs.Pet import Pet
from lobot_api.structs.PickupPet import PickupPet
from silkroad_security_api_1_4.tests.packet_tests.packet_test import PacketTest


class TestUseItem(PacketTest):
	protector_store = Monster()

	def setUp(self) -> None:
		super().setUpClass()
		self.load_test_fixture(self.get_fixtures_folder_path() / "inventory" / "0xB04C_UseItem.json")
		self.handler = UseItem.from_empty_constructor()
		for setup_packet in self.class_setup_packets:
			Handling.instance().handle_packet(setup_packet)

		self.populate_items()

	@staticmethod
	def populate_items():
		Bot.char_data.inventory.items[0x28] = Item(4, 1)  # ITEM_ETC_HP_POT_01

	def tearDown(self) -> None:
		Bot.selected_npc = None
		Bot.char_data = CharData()
		Bot.pickup_pet = PickupPet()

	def test_loop_all(self):
		self.loop_all()

	def test_create_request(self):
		use_normal = UseItem(0x10, UseItemType.Normal, 0, 0)
		use_on_pet = UseItem(0x28, UseItemType.UseOnPet, 0, 0)
		use_non_item = UseItem(0x28, UseItemType.UseOnItem, 0, 0x2C)

		with self.subTest("use normal"):
			self.assertIsNotNone(use_normal.create_request())
		with self.subTest("use on pet"):
			self.assertIsNotNone(use_on_pet.create_request())
		with self.subTest("use on item"):
			self.assertIsNotNone(use_non_item.create_request())


if __name__ == '__main__':
	unittest.main()
