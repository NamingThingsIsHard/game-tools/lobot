import unittest

from lobot_api.Bot import Bot
from lobot_api.packets.storage.StorageInfoDataHandler import StorageInfoDataHandler
from lobot_api.packets.storage.StorageInfoGoldHandler import StorageInfoGoldHandler
from silkroad_security_api_1_4.Packet import Packet
from silkroad_security_api_1_4.tests.packet_tests.packet_test import PacketTest


class TestStorageInfoGold(PacketTest):
	packet_gold = Packet(0x3047, False, False, [0x0F, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00])

	@classmethod
	def setUpClass(cls) -> None:
		super().setUpClass()
		cls.handler = StorageInfoGoldHandler()

	@classmethod
	def tearDownClass(cls) -> None:
		super().tearDownClass()
		Bot.storage_gold = 0

	def test_parse_gold(self):
		with self.subTest("parse storage gold"):
			self.assertIsNone(self.handler.handle(self.packet_gold))
		with self.subTest("test correct gold amount parsed"):
			self.assertEqual(Bot.storage_gold, 0x0F)


if __name__ == '__main__':
	unittest.main()
