import unittest

from lobot_api.Bot import Bot
from lobot_api.packets.Handling import Handling
from lobot_api.packets.inventory.ItemMovementType import ItemMovementType
from lobot_api.packets.inventory.MoveItem import MoveItem
from lobot_api.structs.CharData import CharData
from lobot_api.structs.Item import Item
from lobot_api.structs.Monster import Monster
from lobot_api.structs.Pet import Pet
from lobot_api.structs.PickupPet import PickupPet
from silkroad_security_api_1_4.tests.packet_tests.packet_test import PacketTest


class TestMoveItem(PacketTest):
	protector_store = Monster()

	def setUp(self) -> None:
		super().setUpClass()
		self.load_test_fixture(self.get_fixtures_folder_path() / "inventory" / "0xB034_MoveItem.json")
		self.handler = MoveItem.from_empty_constructor()
		for setup_packet in self.class_setup_packets:
			Handling.instance().handle_packet(setup_packet)

		self.protector_store.ref_id = 2004
		self.protector_store.unique_id = 2004
		Bot.selected_npc = self.protector_store

		self.populate_items()

	@staticmethod
	def populate_items():
		pickup_pet_ref_id = 9264  # COS_P_SEOWON|Squirrel
		pickup_pet = Pet()
		pickup_pet.ref_id = pickup_pet_ref_id
		pickup_pet = PickupPet().set_from_pet(pickup_pet)
		Bot.char_data.inventory.avatar_inventory.items[1] = Item(24652, 1)  # 24652|ITEM_MALL_AVATAR_M_PIRATE_2
		Bot.char_data.inventory.avatar_inventory.items[2] = Item(24361, 1)  # 24361|ITEM_MALL_AVATAR_M_PIRATE_ATTACH
		Bot.pet_inventory[0] = Item(61, 50)  # ITEM_ETC_SCROLL_RETURN_01
		Bot.pet_inventory[1] = Item(62, 1)  # ITEM_ETC_AMMO_ARROW_01
		Bot.pet_inventory[2] = Item(62, 500)  # ITEM_ETC_AMMO_ARROW_01
		Bot.pet_inventory[3] = Item(62, 500)  # ITEM_ETC_AMMO_ARROW_01

	def tearDown(self) -> None:
		Bot.selected_npc = None
		Bot.char_data = CharData()
		Bot.pickup_pet = PickupPet()

	def test_loop_all(self):
		self.loop_all()

	def test_commands_all(self):
		from_slot = 1
		to_slot = 2
		amount = 5
		for t in ItemMovementType._member_map_.values():
			with self.subTest(t.name):
				self.assertIsNotNone(MoveItem(t, from_slot, to_slot, amount).create_request())


if __name__ == '__main__':
	unittest.main()
