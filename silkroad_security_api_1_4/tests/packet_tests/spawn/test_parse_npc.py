import unittest

from lobot_api.globals.SpawnListsGlobal import SpawnListsGlobal
from lobot_api.packets.spawn.GroupSpawnBeginHandler import GroupSpawnBeginHandler
from lobot_api.packets.spawn.GroupSpawnHandler import GroupSpawnHandler
from silkroad_security_api_1_4.tests.packet_tests.packet_test import PacketTest

SKIP_MESSAGE = "Requires updated packets from server with current pk2 files"


class TestParsePet(PacketTest):

	@classmethod
	def setUpClass(cls) -> None:
		super().setUpClass()
		cls.load_test_fixture(cls.get_fixtures_folder_path() / "spawn" / "0x3019_ParseNPC.json")
		cls.handler = GroupSpawnHandler()
		for setup_packet in cls.class_setup_packets:
			cls.handler.handle(setup_packet)

	def tearDown(self) -> None:
		SpawnListsGlobal.remove_all_spawn_entries()

	def test_all_spawn_packets(self):
		yangan_east_gate_guards = self.tests["yangan_east_gate_guards"]
		normal_portal = self.tests["normal_portal"]

		with self.subTest(yangan_east_gate_guards.name):
			self.run_packet_test_should_not_fail("yangan_east_gate_guards")
			SpawnListsGlobal.remove_all_spawn_entries()

		with self.subTest(normal_portal.name):
			try:
				normal_portal.packet.lock()
				GroupSpawnBeginHandler.remaining_spawns += 1
				self.handler.handle(normal_portal.packet)
			except Exception as e:
				self.fail(e)
			self.assertEqual(len(SpawnListsGlobal.NPCSpawns), 1)
			SpawnListsGlobal.remove_all_spawn_entries()


if __name__ == '__main__':
	unittest.main()
