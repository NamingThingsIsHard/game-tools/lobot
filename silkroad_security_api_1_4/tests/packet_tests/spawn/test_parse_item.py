import unittest

from lobot_api.globals.SpawnListsGlobal import SpawnListsGlobal
from lobot_api.packets.spawn.GroupSpawnBeginHandler import GroupSpawnBeginHandler
from lobot_api.packets.spawn.GroupSpawnHandler import GroupSpawnHandler
from lobot_api.packets.spawn.SingleDespawnHandler import SingleDespawnHandler
from lobot_api.packets.spawn.SingleSpawnHandler import SingleSpawnHandler
from silkroad_security_api_1_4.Packet import Packet
from silkroad_security_api_1_4.tests.packet_tests.packet_test import PacketTest

SKIP_MESSAGE = "Requires updated packets from server with current pk2 files"


class TestParsePet(PacketTest):

	@classmethod
	def setUpClass(cls) -> None:
		super().setUpClass()
		cls.load_test_fixture(cls.get_fixtures_folder_path() / "spawn" / "0x3019_ParseDrop.json")
		cls.handler = GroupSpawnHandler()
		for setup_packet in cls.class_setup_packets:
			cls.handler.handle(setup_packet)

	def tearDown(self) -> None:
		SpawnListsGlobal.remove_all_spawn_entries()

	def test_all_spawn_packets(self):
		packet = self.tests["spawn_gold"].packet
		packet_copy = Packet(packet.opcode, packet.encrypted, packet.massive, packet.get_bytes())

		spawn_handler = SingleSpawnHandler()
		despawn_handler = SingleDespawnHandler()

		with self.subTest("parse item"):
			try:
				packet_copy.lock()
				GroupSpawnBeginHandler.remaining_spawns += 1
				self.handler.handle(packet_copy)
			except Exception as e:
				self.fail(e)
			# TODO add pet list
			# self.assertEqual(len(SpawnListsGlobal.NPCSpawns), 1)
			SpawnListsGlobal.remove_all_spawn_entries()

		with self.subTest("spawn_gold"):
			self.handler = spawn_handler
			self.run_packet_test_should_not_fail("spawn_gold")
			self.assertEqual(len(SpawnListsGlobal.Drops), 1)
		with self.subTest("despawn_gold"):
			self.handler = despawn_handler
			self.run_packet_test_should_not_fail("despawn_gold")
			self.assertEqual(len(SpawnListsGlobal.Drops), 0)
		SpawnListsGlobal.remove_all_spawn_entries()


if __name__ == '__main__':
	unittest.main()
