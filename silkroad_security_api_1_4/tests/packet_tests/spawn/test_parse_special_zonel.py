import unittest

from lobot_api.globals.SpawnListsGlobal import SpawnListsGlobal
from lobot_api.packets.spawn.GroupSpawnHandler import GroupSpawnHandler
from lobot_api.packets.spawn.SingleSpawnHandler import SingleSpawnHandler
from lobot_api.packets.spawn.SpawnHandler import ParseSpecialZone
from silkroad_security_api_1_4.SeekType import SeekType
from silkroad_security_api_1_4.tests.packet_tests.packet_test import PacketTest

SKIP_MESSAGE = "Requires updated packets from server with current pk2 files"


class TestParseSpecialZone(PacketTest):

	@classmethod
	def setUpClass(cls) -> None:
		super().setUpClass()
		cls.load_test_fixture(cls.get_fixtures_folder_path() / "spawn" / "0x3019_ParseSpecialZone.json")
		cls.handler = GroupSpawnHandler()
		for setup_packet in cls.class_setup_packets:
			cls.handler.handle(setup_packet)

	def tearDown(self) -> None:
		SpawnListsGlobal.remove_all_spawn_entries()

	def test_all_spawn_packets(self):
		parse_portal = ParseSpecialZone()
		single_spawn_handler = SingleSpawnHandler()
		for test_name, test in self.tests.items():
			with self.subTest(test_name):
				try:
					test.packet.lock()
					parse_portal.parse(test.packet, test.packet.read_uint32())
					test.packet.seek_read(0, SeekType.Begin)
					single_spawn_handler.handle(test.packet)
				except Exception as e:
					self.fail(e)
				# self.assertEqual(len(SpawnListsGlobal.AllSpawnEntries), 1)
				SpawnListsGlobal.remove_all_spawn_entries()


if __name__ == '__main__':
	unittest.main()
