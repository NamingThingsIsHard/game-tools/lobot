import unittest

from lobot_api.globals.SpawnListsGlobal import SpawnListsGlobal
from lobot_api.packets.spawn.GroupSpawnBeginHandler import GroupSpawnBeginHandler
from lobot_api.packets.spawn.GroupSpawnHandler import GroupSpawnHandler
from silkroad_security_api_1_4.tests.packet_tests.packet_test import PacketTest

SKIP_MESSAGE = "Requires updated packets from server with current pk2 files"


class TestParsePet(PacketTest):

	@classmethod
	def setUpClass(cls) -> None:
		super().setUpClass()
		cls.load_test_fixture(cls.get_fixtures_folder_path() / "spawn" / "0x3019_ParseMonster.json")
		cls.handler = GroupSpawnHandler()
		for setup_packet in cls.class_setup_packets:
			cls.handler.handle(setup_packet)

	def tearDown(self) -> None:
		SpawnListsGlobal.remove_all_spawn_entries()

	def test_all_spawn_packets(self):
		mang_yang1 = self.tests["mang_yang1"]
		mang_yang2 = self.tests["mang_yang2"]

		with self.subTest(mang_yang1.name):
			self.run_packet_test_should_not_fail("mang_yang1")
			SpawnListsGlobal.remove_all_spawn_entries()

		with self.subTest(mang_yang2.name):
			try:
				mang_yang2.packet.lock()
				GroupSpawnBeginHandler.remaining_spawns += 1
				self.handler.handle(mang_yang2.packet)
			except Exception as e:
				self.fail(e)
			self.assertEqual(len(SpawnListsGlobal.MonsterSpawns), 1)
			SpawnListsGlobal.remove_all_spawn_entries()


if __name__ == '__main__':
	unittest.main()
