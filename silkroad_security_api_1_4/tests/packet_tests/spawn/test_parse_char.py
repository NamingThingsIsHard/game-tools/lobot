import unittest

from lobot_api.globals.SpawnListsGlobal import SpawnListsGlobal
from lobot_api.packets.spawn.GroupSpawnBeginHandler import GroupSpawnBeginHandler
from lobot_api.packets.spawn.GroupSpawnHandler import GroupSpawnHandler
from silkroad_security_api_1_4.tests.packet_tests.packet_test import PacketTest

SKIP_MESSAGE = "Requires updated packets from server with current pk2 files"


class TestParseChar(PacketTest):

	@classmethod
	def setUpClass(cls) -> None:
		super().setUpClass()
		cls.load_test_fixture(cls.get_fixtures_folder_path() / "spawn" / "0x3019_ParseChar.json")
		cls.handler = GroupSpawnHandler()
		for setup_packet in cls.class_setup_packets:
			cls.handler.handle(setup_packet)

	def tearDown(self) -> None:
		SpawnListsGlobal.remove_all_spawn_entries()

	def test_all_spawn_packets(self):
		for test_name, test in self.tests.items():
			with self.subTest(test_name):
				try:
					test.packet.lock()
					GroupSpawnBeginHandler.remaining_spawns += 1
					self.handler.handle(test.packet)
				except Exception as e:
					self.fail(e)
				self.assertEqual(len(SpawnListsGlobal.Players), 1)
				SpawnListsGlobal.remove_all_spawn_entries()


if __name__ == '__main__':
	unittest.main()
