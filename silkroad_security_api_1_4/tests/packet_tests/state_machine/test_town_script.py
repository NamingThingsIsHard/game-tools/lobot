import unittest

from lobot_api.globals.settings.LoopSettings import ShoppingSettings
from lobot_api.packets.char.CharDataHandler import CharDataHandler
from lobot_api.statemachine.townscript.TownScript import TownScript
from silkroad_security_api_1_4.tests.packet_tests.packet_test import PacketTest


class TestShoppingList(PacketTest):

	@classmethod
	def setUpClass(cls) -> None:
		super().setUpClass()
		cls.load_test_fixture(cls.get_fixtures_folder_path() / "script" / "0x3013_TestShoppingList.json")
		cls.handler = CharDataHandler()

	def setUp(self) -> None:
		if self.class_setup_packets:
			for setup_packet in self.class_setup_packets:
				self.handler.handle(setup_packet)
				self.class_setup_packets.clear()

	def test_hp_potion_quantity(self):
		ShoppingSettings.instance().hp_potion_quantity = 100
		self.assertGreater(TownScript.hp_potion_quantity(), 50)

	def test_mp_potion_quantity(self):
		ShoppingSettings.instance().mp_potion_quantity = 100
		self.assertGreater(TownScript.mp_potion_quantity(), 50)

	def test_vigor_potion_quantity(self):
		ShoppingSettings.instance().vigor_potion_quantity = 20
		self.assertGreater(TownScript.vigor_potion_quantity(), 10)

	def test_purification_pill_quantity(self):
		ShoppingSettings.instance().purification_pill_quantity = 10
		self.assertGreater(TownScript.purification_pill_quantity(), 5)

	def test_projectile_quantity(self):
		ShoppingSettings.instance().projectile_quantity = 1000
		self.assertGreater(TownScript.projectile_quantity(), 250)

	def test_speed_pot_quantity(self):
		ShoppingSettings.instance().speed_pot_quantity = 5
		self.assertGreater(TownScript.speed_pot_quantity(), 1)

	def test_return_scroll_quantity(self):
		ShoppingSettings.instance().return_scroll_quantity = 10
		self.assertGreater(TownScript.return_scroll_quantity(), 5)

	def test_transport_quantity(self):
		ShoppingSettings.instance().transport_quantity = 5
		self.assertGreater(TownScript.transport_quantity(), 1)

	def test_zerk_potion_quantity(self):
		ShoppingSettings.instance().zerk_potion_quantity = 1
		self.assertEqual(TownScript.zerk_potion_quantity(), 1)

	def test_pet_hp_potion_quantity(self):
		ShoppingSettings.instance().pet_hp_potion_quantity = 100

		self.assertEqual(TownScript.pet_hp_potion_quantity(), 100)

	def test_pet_pill_quantity(self):
		ShoppingSettings.instance().pet_pill_quantity = 10
		self.assertEqual(TownScript.pet_pill_quantity(), 10)

	def test_hgp_potion_quantity(self):
		ShoppingSettings.instance().hgp_potion_quantity = 5
		self.assertGreaterEqual(TownScript.hgp_potion_quantity(), 1)

	def test_grass_of_life_quantity(self):
		ShoppingSettings.instance().grass_of_life_quantity = 1
		self.assertEqual(TownScript.grass_of_life_quantity(), 1)


if __name__ == '__main__':
	unittest.main()
