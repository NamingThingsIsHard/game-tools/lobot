import contextlib
import io
import os
import sys
import time
import unittest
import warnings
from pathlib import Path
from typing import Callable
from unittest.mock import patch, PropertyMock

from PyQt5.QtWidgets import QApplication

from controller.controller import Controller
from lobot_api.ObjectStatusChangedArgs import ObjectStatusChangedArgs
from lobot_api.connection import proxy
from lobot_api.globals.PK2DataGlobal import PK2DataGlobal
from lobot_api.globals.SpawnListsGlobal import SpawnListsGlobal
from lobot_api.packets.char.BuffArgs import BuffArgs
from lobot_api.packets.char.MovementArgs import MovementArgs
from lobot_api.scripting.Area import Area
from lobot_api.scripting.Point import Point
from lobot_api.scripting.ScriptExpression import ExpressionParser, CommandExpression
from lobot_api.scripting.Town import TownScriptJangan
from lobot_api.statemachine.IState import IStateType
from lobot_api.statemachine.scriptingstates.ScriptState import ScriptState
from lobot_api.statemachine.townscript.Storage import Storage
from lobot_api.statemachine.townscript.TownScript import TownScript
from lobot_api.structs.Monster import Monster
from lobot_api.structs.ObjectStatus import ObjectStatus
from lobot_api.structs.Portal import Portal
from silkroad_security_api_1_4.tests.fixtures import pk2_data_loader_util


q_app = QApplication(sys.argv)  # use to run tests using QTimers


class TestScriptUtils(unittest.TestCase):
	town_script = TownScript()
	area = Area(0, 0, 50)
	point = Point(168, 92, 0, 0, 0)
	point2 = Point.from_absolute_coords(0, 0, 0)

	@classmethod
	def setUpClass(cls) -> None:
		with contextlib.redirect_stderr(io.StringIO()):
			warnings.simplefilter("ignore")
			Controller.thread_pool.start(proxy.main)
			while proxy.joymax is None:
				time.sleep(0.01)  # wait for initialization to prevent errors

	@classmethod
	def tearDownClass(cls) -> None:
		with contextlib.redirect_stderr(io.StringIO()):
			proxy.stop()

	def does_not_raise(self, method: Callable, *args):
		try:
			if not args:
				method()
			else:
				method(*args)
		except Exception as e:
			self.fail(e)

	def test_open_shop(self):
		monster = Monster()
		monster.ref_id = 2013  # NPC_CH_WAREHOUSE_M|Storage-Keeper Wangu
		monster.unique = 2013
		SpawnListsGlobal.NPCSpawns[monster.unique_id] = monster
		storage = Storage()
		with patch('lobot_api.structs.Monster.Monster.long_id', new_callable=PropertyMock) as mocked_long_id:
			mocked_long_id.return_value = 'NPC_CH_WAREHOUSE_M'
			with patch.object(ScriptState.on_executed_shopping_command, "notify") as mocked_notify:
				ScriptState.__open_shop__(storage, IStateType.TOWN_SCRIPT)
				mocked_notify.assert_called()

	def test_current_script_command(self):
		ScriptState._script_ = []
		self.assertEqual(self.town_script.current_script_command, None)

	def test__pause_(self):
		with contextlib.redirect_stdout(io.StringIO()):
			self.does_not_raise(self.town_script._initialize_event_handlers_)
			self.does_not_raise(self.town_script._pause_)

	def test__resume_(self):
		with contextlib.redirect_stdout(io.StringIO()):
			self.town_script._script_ = TownScriptJangan
			self.does_not_raise(self.town_script._resume_)

	def test__handle_executed_move_command_(self):
		with contextlib.redirect_stdout(io.StringIO()):
			self.town_script._script_ = TownScriptJangan
			self.does_not_raise(self.town_script._handle_executed_move_command_, MovementArgs(self.point, self.point2))

	def test__speed_buff_apply_(self):
		self.does_not_raise(self.town_script._use_speed_buff_)

	def test__cast_speed_buff_after_interaction_(self):
		self.does_not_raise(self.town_script._cast_speed_buff_after_interaction_)

	def test__cast_speed_buff_after_invincible_(self):
		self.does_not_raise(self.town_script._cast_speed_buff_after_invincible_, ObjectStatusChangedArgs(ObjectStatus.Normal, ObjectStatus.Stealth))

	def test__speed_buff_setup_(self):
		self.does_not_raise(self.town_script._speed_buff_setup_)

	def test__speed_buff_clean_up_(self):
		self.does_not_raise(self.town_script._speed_buff_clean_up_)

	def test__handle_speed_buff_ended_(self):
		self.does_not_raise(self.town_script._handle_speed_buff_ended_, BuffArgs(1924))

	def test__can_start_training_(self):
		self.does_not_raise(self.town_script._can_start_training_)

	def test__is_index_in_range_(self):
		self.does_not_raise(self.town_script._is_index_in_range_, 0, 5)

	def test__can_transition_to_walkscript_(self):
		self.does_not_raise(self.town_script._can_transition_to_walkscript_)

	def test__transition_to_next_script_(self):
		with contextlib.redirect_stdout(io.StringIO()):
			self.does_not_raise(self.town_script._transition_to_next_script_)

	def test__handle_skipped_command_(self):
		with contextlib.redirect_stdout(io.StringIO()):
			self.town_script._script_ = TownScriptJangan
			self.does_not_raise(self.town_script._initialize_event_handlers_)
			self.does_not_raise(self.town_script._handle_skipped_command_)

	def test__is_movement_script_command_(self):
		move_command = CommandExpression('go', ['10', '10'])
		ScriptState._script_ = [move_command]
		ScriptState.command_index = 0
		point1 = Point.from_absolute(0, 0)
		point2 = Point.from_absolute(10, 10)
		self.does_not_raise(self.town_script._is_movement_script_command_, MovementArgs(0, point1, point2))

	def test_teleport_reverse_call(self):
		teleport = ["2094", "2"]
		teleport_reverse_recall = ["reverseRecall"]

		if not PK2DataGlobal.teleporters:
			pk2_data_loader_util.load_teleporters_from_txt_file()
		portal = Portal()
		portal.ref_id = 2094
		portal.PK2Data = PK2DataGlobal.teleporters.get(2094)
		SpawnListsGlobal.Portals[2094] = portal
		self.does_not_raise(self.town_script._handle_teleport_command_, teleport)
		self.assertRaises(NotImplementedError, self.town_script._handle_teleport_command_, teleport_reverse_recall)

	def test_execute(self):
		import warnings
		warnings.filterwarnings('ignore')

		directory = Path(os.path.dirname(os.path.realpath(__file__))).parent.parent
		script_path = directory / "fixtures" / "scripts" / "command_all_commands.txt"
		script = []

		with open(script_path) as f:
			lines = f.read().splitlines()
			script = ExpressionParser.parse_script_lines(lines)

		with self.subTest("none command"):
			self.assertRaises(ValueError, self.town_script.execute, None)
		with self.subTest("command list"), contextlib.redirect_stdout(io.StringIO()):
			for command in script:
				with self.subTest(command.command_name):
					if command.command_name == 'quest' or str(command) == 'teleport reverseRecall':
						self.assertRaises(NotImplementedError, self.town_script.execute, command)
					else:
						self.does_not_raise(self.town_script.execute, command)
