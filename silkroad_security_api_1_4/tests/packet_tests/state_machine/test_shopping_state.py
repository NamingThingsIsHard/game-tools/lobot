import unittest

from lobot_api.packets.inventory.MoveItem import MoveItem
from lobot_api.statemachine.scriptingstates.ShoppingState import ShoppingState
from silkroad_security_api_1_4.Packet import Packet
from silkroad_security_api_1_4.tests.packet_tests.packet_test import PacketTest


class TestShoppingState(PacketTest):
	@classmethod
	def setUpClass(cls) -> None:
		super().setUpClass()
		cls.load_test_fixture(cls.get_fixtures_folder_path() / "state_machine" / "0xB034_ShoppingState.json")
		cls.handler = MoveItem.from_empty_constructor()
		for setup_packet in cls.class_setup_packets:
			cls.handler.handle(setup_packet)

	def test_fake_buy_item_jangan(self):
		packet_filter_false = Packet(0xB023)
		self.assertFalse(ShoppingState.should_filter(packet_filter_false))


if __name__ == '__main__':
	unittest.main()
