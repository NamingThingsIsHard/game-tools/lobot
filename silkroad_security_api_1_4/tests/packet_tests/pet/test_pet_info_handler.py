import unittest

from lobot_api.Bot import Bot
from lobot_api.globals.SpawnListsGlobal import SpawnListsGlobal
from lobot_api.packets.Handling import Handling
from lobot_api.packets.pet.PetInfoHandler import PetInfoHandler
from lobot_api.packets.spawn.GroupSpawnBeginHandler import GroupSpawnBeginHandler
from lobot_api.packets.spawn.SingleDespawnHandler import SingleDespawnHandler
from lobot_api.packets.spawn.SingleSpawnHandler import SingleSpawnHandler
from lobot_api.statemachine.AutoPotions import AutoPotions
from silkroad_security_api_1_4.tests.packet_tests.packet_test import PacketTest


class TestPetInfoHandler(PacketTest):
	bot_id = 6124
	pet_id = 137594408

	@classmethod
	def setUpClass(cls) -> None:
		super().setUpClass()
		cls.load_test_fixture(cls.get_fixtures_folder_path() / "pet" / "0x30C8_PetInfoHandler.json")
		cls.handler = SingleSpawnHandler()
		for setup_packet in cls.class_setup_packets:
			Handling.instance().handle_packet(setup_packet)

	def tearDown(self) -> None:
		SpawnListsGlobal.remove_all_spawn_entries()

	def test_spawn_then_despawn_pet(self):
		Bot.char_data.unique_id = self.bot_id
		Bot.growth_pet.unique_id = self.pet_id

		with self.subTest("spawn_pet"):
			self.run_packet_test_should_not_fail("spawn_pet")
			AutoPotions.pet_hgp_timer.stop()
		with self.subTest("after_spawn"):
			self.assertEqual(len(SpawnListsGlobal.AllSpawnEntries), 1)
		self.handler = PetInfoHandler()
		with self.subTest("pet_info"):
			self.run_packet_test_should_not_fail("pet_info")
		self.handler = SingleDespawnHandler()
		with self.subTest("despawn_pet"):
			self.run_packet_test_should_not_fail("despawn_pet")
		with self.subTest("after_despawn"):
			self.assertEqual(GroupSpawnBeginHandler.remaining_spawns, 0)


if __name__ == '__main__':
	unittest.main()
