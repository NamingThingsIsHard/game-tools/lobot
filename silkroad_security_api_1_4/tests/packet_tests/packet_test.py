import json
import os
import unittest
from pathlib import Path
from typing import List, Optional, Dict

from lobot_api.packets.IHandler import IHandler
from silkroad_security_api_1_4.Packet import Packet
from silkroad_security_api_1_4.tests.fixtures import pk2_data_loader_util


class PacketTestContainer:
	def __init__(self, name, packet):
		self.name: str = name
		self.packet: Packet = packet

	def __repr__(self):
		return f"{format(self.packet.opcode, '#04x')} - {self.name}"

	def __str__(self):
		return f"{format(self.packet.opcode, '#04x')} - {self.name}"


class PacketTest(unittest.TestCase):
	"""Base class for packet tests which generally require pk2 data.

	Tests are setup by choosing the packet handler and loading fixtures from json dumps.
	If no new testcases are declared in a subclasses, the handler can b looped over all the parsed tests as subTests
	using loop_all().
	"""

	"""Packets to be handled to setup actual tests by reproduce in game values."""
	class_setup_packets: List[Packet] = []
	"""Parsed Packets with a name to lookup"""
	tests: Dict[str, PacketTestContainer] = []
	"""Default IHandler to test packets in "tests" with"""
	handler: Optional[IHandler] = None

	@classmethod
	def setUpClass(cls) -> None:
		"""Load costly pk2 data once"""
		pk2_data_loader_util.load_all_data()

	@classmethod
	def tearDownClass(cls) -> None:
		cls.class_setup_packets = []
		cls.tests = {}
		cls.handler = None

	@staticmethod
	def does_not_have_other_testCases(cls):
		tests = unittest.defaultTestLoader.loadTestsFromTestCase(cls)
		return tests.countTestCases() > 1

	def run_packet_test_should_not_fail(self, name):
		"""
		Runs test for entry with given name from dict "tests".
		Use this method to assert that it does not fail.
		:param name: Test name
		:return: None
		"""
		test = self.tests[name]
		try:
			self.handler.handle(test.packet)
		except Exception as e:
			self.fail(e)

	def loop_all(self):
		"""
		Loops over list "test" making sure they don't raise any error.
		Can be called when no other checks are needed.
		Not named using the convention "test_*" so that test would not show up multiple times in results.
		:return: None
		"""
		for test_name, test in self.tests.items():
			with self.subTest(test_name):
				try:
					self.handler.handle(test.packet)
				except Exception as e:
					self.fail(e)

	@classmethod
	def get_fixtures_folder_path(cls) -> Path:
		path = Path(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))
		return path / "fixtures" / "packets"

	@classmethod
	def parse_packet(cls, packet_str):
		return eval(f"Packet({packet_str})")

	@classmethod
	def parse_packet_test_container(cls, kwargs) -> PacketTestContainer:
		name = kwargs["name"]
		packet = cls.parse_packet(kwargs["packet"])
		return PacketTestContainer(name, packet)

	@classmethod
	def load_test_fixture(cls, file_path) -> Dict:
		"""
		Get information to run tests from json dump
		:param file_path: Path to json dump
		:return: Parsed packet information
		"""
		with open(file_path, "r") as fp:
			json_string = json.load(fp)

			setup_packets = [cls.parse_packet(packet_str) for packet_str in json_string["class_setup_packets"]]
			cls.class_setup_packets = setup_packets

			test_containers = {}
			for test in json_string["tests"]:
				parsed = cls.parse_packet_test_container(test)
				test_containers[parsed.name] = parsed

			cls.tests = test_containers
		return test_containers

