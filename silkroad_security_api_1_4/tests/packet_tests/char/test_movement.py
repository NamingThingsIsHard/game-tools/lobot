import unittest

from lobot_api.Bot import Bot
from lobot_api.packets.char.Movement import Movement
from lobot_api.scripting.Point import Point
from silkroad_security_api_1_4.tests.packet_tests.packet_test import PacketTest


class TestMovement(PacketTest):
	temp_bot_unique_id = 0xFD0E34

	@classmethod
	def setUpClass(cls) -> None:
		super().setUpClass()
		cls.load_test_fixture(cls.get_fixtures_folder_path() / "char" / "0xB021_Movement.json")
		cls.handler = Movement.from_empty_constructor()
		for setup_packet in cls.class_setup_packets:
			cls.handler.handle(setup_packet)

	def test_char_move_hotan_east_bridge1(self):
		Bot.char_data.unique_id = self.temp_bot_unique_id  # set to uniqueID in packet to correctly set new location
		self.run_packet_test_should_not_fail("char_move_hotan_east_bridge1")
		self.assertTrue(Bot.char_data.position.x < 400)
		self.assertTrue(Bot.char_data.position.y < 50)

	def test_char_move_hotan_east_bridge2(self):
		Bot.char_data.unique_id = self.temp_bot_unique_id  # set to uniqueID in packet to correctly set new location
		self.run_packet_test_should_not_fail("char_move_hotan_east_bridge2")
		self.assertTrue(Bot.char_data.position.x < 350)
		self.assertTrue(Bot.char_data.position.y < 50)

	def test_get_move_time(self):
		point1 = Point.from_absolute_coords(0, 0)
		point2 = Point.from_absolute_coords(100, 100)
		self.assertGreater(Movement.get_walk_time(point1, point2), 1)

if __name__ == '__main__':
	unittest.main()
