import unittest

from lobot_api.Bot import Bot
from lobot_api.packets.char.CharDataHandler import CharDataHandler
from silkroad_security_api_1_4.tests.packet_tests.packet_test import PacketTest

SKIP_MESSAGE = "Requires updated packets from server with current pk2 files"


class TestCharDataHandler(PacketTest):

	@classmethod
	def setUpClass(cls) -> None:
		super().setUpClass()
		cls.load_test_fixture(cls.get_fixtures_folder_path() / "char" / "0x3013_CharDataHandler.json")
		cls.handler = CharDataHandler()
		for setup_packet in cls.class_setup_packets:
			cls.handler.handle(setup_packet)

	def test_char_data_no_buffs_no_pets(self):
		test = self.tests["char_data_no_buffs_no_pets"]
		try:
			self.handler.handle(test.packet)
		except Exception as e:
			self.fail(e)

	def test_char_data_pet_summon_scroll(self):
		self.run_packet_test_should_not_fail("char_data_pet_summon_scroll")

	@unittest.skip(SKIP_MESSAGE)
	def test_char_data_pet_summon_scroll_summoned(self):
		self.run_packet_test_should_not_fail("char_data_pet_summon_scroll_summoned")

	@unittest.skip(SKIP_MESSAGE)
	def test_char_data_attribute_stone(self):
		self.run_packet_test_should_not_fail("char_data_attribute_stone")

	@unittest.skip(SKIP_MESSAGE)
	def test_char_data_char_parsed_as_dead(self):
		self.run_packet_test_should_not_fail("char_data_pet_summon_scroll_summoned")
		self.assertTrue(Bot.char_data.is_alive)

	@unittest.skip(SKIP_MESSAGE)
	def test_char_data_alchemy_items(self):
		self.run_packet_test_should_not_fail("char_data_alchemy_items")

	def test_char_data_location(self):
		self.run_packet_test_should_not_fail("char_data_location")
		x_sec = 0x87
		y_sec = 0x5C
		x = 116
		y = 14
		self.assertEqual(x_sec, Bot.char_data.position.x_sector)
		self.assertEqual(y_sec, Bot.char_data.position.y_sector)
		self.assertEqual(x, Bot.char_data.position.x)
		self.assertEqual(y, Bot.char_data.position.y)


# def test_char_data_bow_skill(self):
# 	self.run_packet_test_should_not_fail("char_data_with_skills")
# 	ref_id_anti_devil_bow = 71  # SKILL_CH_BOW_CRITICAL_A_01;;
# 	self.assertTrue(any(s for s in Bot.char_data.skills if s.ref == ref_id_anti_devil_bow))


if __name__ == '__main__':
	unittest.main()
