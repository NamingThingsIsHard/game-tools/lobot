import unittest

from lobot_api.Bot import Bot
from lobot_api.packets.char.CharExpSpUpdate import CharExpSpUpdate
from lobot_api.packets.char.CharLevelUpHandler import CharLevelUpHandler
from silkroad_security_api_1_4.Packet import Packet
from silkroad_security_api_1_4.tests.packet_tests.packet_test import PacketTest


class TestCharExpSpUpdate(PacketTest):
	char_level_up_handler = CharLevelUpHandler()
	char_exp_sp_update = CharExpSpUpdate()

	exp_gain1 = 73530
	bot_lvl1 = 32
	exp_before_lvl_up1 = 1806816
	exp_after_lvl_up1 = 66019

	exp_gain2 = 146610
	bot_lvl2 = 35
	exp_before_lvl_up2 = 2486809
	exp_after_lvl_up2 = 28376

	@classmethod
	def setUpClass(cls) -> None:
		super().setUpClass()
		cls.load_test_fixture(cls.get_fixtures_folder_path() / "char" / "0x3054_CharExpSpUpdate.json")
		cls.handler = cls.char_exp_sp_update
		for setup_packet in cls.class_setup_packets:
			cls.handler.handle(setup_packet)

	def setUp(self) -> None:
		Bot.char_data.level = 1

	def tearDown(self) -> None:
		Bot.char_data.exp_offset = 0
		self.tests["level_up"].packet = Packet(0x3054, False, False, [0x00, 0x00, 0x00, 0x00])

	def test_exp_sp_update(self):
		Bot.char_data.level = self.bot_lvl1 - 1
		Bot.char_data.exp_offset = 0
		self.run_packet_test_should_not_fail("exp_sp_update")
		self.assertEqual(Bot.char_data.exp_offset, self.exp_gain1)

	def test_exp_sp_update_level_up1(self):
		Bot.char_data.level = self.bot_lvl1
		Bot.char_data.exp_offset = self.exp_before_lvl_up1

		# switch handler to level_up
		self.handler = self.char_level_up_handler
		self.run_packet_test_should_not_fail("level_up")
		# switch back
		self.handler = self.char_exp_sp_update

		self.run_packet_test_should_not_fail("exp_sp_update_level_up1")
		self.assertEqual(Bot.char_data.exp_offset, self.exp_after_lvl_up1)
		self.assertEqual(Bot.char_data.level, self.bot_lvl1 + 1)

	def test_exp_sp_update_level_up2(self):
		Bot.char_data.level = self.bot_lvl2
		Bot.char_data.exp_offset = self.exp_before_lvl_up2

		# switch handler to level_up
		self.handler = self.char_level_up_handler
		self.run_packet_test_should_not_fail("level_up")
		# switch back
		self.handler = self.char_exp_sp_update

		self.run_packet_test_should_not_fail("exp_sp_update_level_up2")
		self.assertEqual(Bot.char_data.exp_offset, self.exp_after_lvl_up2)
		self.assertEqual(Bot.char_data.level, self.bot_lvl2 + 1)


if __name__ == '__main__':
	unittest.main()
