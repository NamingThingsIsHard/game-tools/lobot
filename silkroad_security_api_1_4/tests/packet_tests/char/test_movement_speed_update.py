import unittest

from lobot_api.packets.char.CharSpeedUpdateHandler import CharSpeedUpdateHandler
from silkroad_security_api_1_4.tests.packet_tests.packet_test import PacketTest


class TestCharSpeedUpdate(PacketTest):

	@classmethod
	def setUpClass(cls) -> None:
		super().setUpClass()
		cls.load_test_fixture(cls.get_fixtures_folder_path() / "char" / "0x30D0_CharSpeedUpdateHandler.json")
		cls.handler = CharSpeedUpdateHandler()
		for setup_packet in cls.class_setup_packets:
			cls.handler.handle(setup_packet)

	def test_loop_all(self):
		self.loop_all()

if __name__ == '__main__':
	unittest.main()
