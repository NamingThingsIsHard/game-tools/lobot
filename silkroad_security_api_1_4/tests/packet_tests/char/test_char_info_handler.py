import unittest

from lobot_api.packets.char.CharInfoHandler import CharInfoHandler
from silkroad_security_api_1_4.tests.packet_tests.packet_test import PacketTest


class TestCharInfoHandler(PacketTest):

	@classmethod
	def setUpClass(cls) -> None:
		super().setUpClass()
		cls.load_test_fixture(cls.get_fixtures_folder_path() / "char" / "0x303D_CharInfoHandler.json")
		cls.handler = CharInfoHandler()

	def test_loop_all(self):
		self.loop_all()


if __name__ == '__main__':
	unittest.main()
