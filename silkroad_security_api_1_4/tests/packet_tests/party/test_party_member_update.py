import unittest

from lobot_api.Bot import Bot
from lobot_api.packets.party.PartyMemberUpdateHandle import PartyMemberUpdateHandler
from lobot_api.pk2.PK2Item import Country
from lobot_api.scripting.Point import Point
from lobot_api.structs.PartyEntry import PartyEntry
from lobot_api.structs.PartyMember import PartyMember
from silkroad_security_api_1_4.tests.packet_tests.packet_test import PacketTest


class TestPartyMemberUpdate(PacketTest):
	@staticmethod
	def get_party_member() -> PartyMember:
		party_member = PartyMember(20814)
		party_member.avatar = 1924
		party_member.level = 1
		party_member.hp_mp = 0
		party_member.mastery1 = 0
		party_member.mastery2 = 0
		party_member.name = 'LobotTest'
		party_member.guild_name = ''
		party_member.Country = Country.Chinese
		party_member.x_sector = 169
		party_member.y_sector = 98
		party_member.point = Point(169, 98, 100, 0, 50, 120)
		party_member.angle = 120
		return party_member

	@classmethod
	def setUpClass(cls) -> None:
		super().setUpClass()
		cls.load_test_fixture(cls.get_fixtures_folder_path() / "party" / "0x03684_PartyMemberUpdate.json")
		cls.handler = PartyMemberUpdateHandler()
		for setup_packet in cls.class_setup_packets:
			cls.handler.handle(setup_packet)

	def test_player_left(self):
		with self.subTest("party_leave_party"):
			self.run_packet_test_should_not_fail("party_leave_party")
		with self.subTest("party_number"):
			self.assertEqual(Bot.party_current.party_number, 0)
			self.assertEqual(Bot.party_current.member_count, 0)

	def test_add_member(self):
		Bot.party_current = PartyEntry(0)
		with self.subTest("party_add_member"):
			self.run_packet_test_should_not_fail("party_add_member")
		with self.subTest("party_count"):
			self.assertGreater(Bot.party_current.member_count, 0)

	def test_add_member_cave(self):
		Bot.party_current = PartyEntry(0)
		with self.subTest("party_add_member_cave"):
			self.run_packet_test_should_not_fail("party_add_member_cave")
		with self.subTest("party_count"):
			self.assertGreater(Bot.party_current.member_count, 0)

	def test_update_member_position(self):
		Bot.party_current = PartyEntry(0)
		Bot.party_current.members.append(self.get_party_member())
		with self.subTest("party_update_position"):
			self.run_packet_test_should_not_fail("party_update_position")

	def test_remove_member(self):
		Bot.party_current = PartyEntry(0)
		Bot.party_current.members.append(self.get_party_member())
		Bot.party_current.member_count = 1
		with self.subTest("party_player_left"):
			self.run_packet_test_should_not_fail("party_player_left")
		with self.subTest("party_count"):
			self.assertEqual(Bot.party_current.member_count, 0)


if __name__ == '__main__':
	unittest.main()
