import unittest

from lobot_api.Bot import Bot
from lobot_api.packets.party.PartyMatchRefresh import PartyMatchRefresh
from silkroad_security_api_1_4.tests.packet_tests.packet_test import PacketTest

SKIP_MESSAGE = "To be worked upon in https://gitlab.com/NamingThingsIsHard/game-tools/lobot/-/milestones/2"


class TestPartyMatchRefresh(PacketTest):

	@classmethod
	def setUpClass(cls) -> None:
		super().setUpClass()
		cls.load_test_fixture(cls.get_fixtures_folder_path() / "party" / "0x706C_PartyMatchRefresh.json")
		cls.handler = PartyMatchRefresh()
		for setup_packet in cls.class_setup_packets:
			cls.handler.handle(setup_packet)

	def test_packet_single_item(self):
		parties = 5
		with self.subTest("party_match_refresh"):
			self.run_packet_test_should_not_fail("party_match_refresh")
		with self.subTest("party_count"):
			self.assertEqual(len(Bot.party_matching_list), parties)


if __name__ == '__main__':
	unittest.main()
