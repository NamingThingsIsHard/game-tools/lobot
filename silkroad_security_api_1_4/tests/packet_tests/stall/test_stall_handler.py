import unittest

from lobot_api.packets.stall.StallHandler import StallHandler
from lobot_api.packets.stall.StallUpdateType import StallUpdateType
from lobot_api.structs.Item import Item, StallItem
from silkroad_security_api_1_4.Packet import Packet
from silkroad_security_api_1_4.tests.packet_tests.packet_test import PacketTest


class TestStallHandler(PacketTest):
	tempBotUniqueID = 0xFD0E34
	stall_arguments = {
		"update_type": StallUpdateType.AddItem,
		"slot": 5,
		"source_slot": 10,
		"stack_count": 10,
		"price": 1000,
		"flea_market_id_group": 1,
	}
	registered_item_arguments = {
		"slot": 5,
		"source_slot": 10,
		"stack_count": 10,
		"price": 1000,
	}
	responses_setup_packet = Packet(0xB0BA, False, False, [0x01,0x02,0x00, 0x00,0x00,0x00, 0x00, 0x00, 0x00,0x3E, 0x00, 0x00, 0x00,0x01, 0x00,0x1A,0x01, 0x00,0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,0xFF,])
	responses = {
		# StallUpdateType.StallUpdateItem: None,
		StallUpdateType.AddItem: Packet(0xB0BA, False, False, [0x01, 0x01, 0x00, 0x01, 0x00, 0xE8, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00]),
		# StallUpdateType.RemoveItem: None,
		StallUpdateType.FleaMarketMode: Packet(0xB0BA, False, False, [0x01, 0x04, 0x01]),
		StallUpdateType.State: Packet(0xB0BA, False, False, [0x01, 0x05, 0x01, 0x00, 0x00]),
		StallUpdateType.Message: Packet(0xB0BA, False, False, [0x01, 0x06, 0x0B, 0x00, 0x4D, 0x6F, 0x76, 0x69, 0x65, 0x20, 0x51, 0x75, 0x6F, 0x74, 0x65]),
		StallUpdateType.Name: Packet(0xB0BA, False, False, [0x01, 0x07]),
	}

	@classmethod
	def setUpClass(cls) -> None:
		super().setUpClass()
		cls.load_test_fixture(cls.get_fixtures_folder_path() / "stall" / "0xB0BA_StallHandler.json")
		cls.handler = StallHandler(*cls.stall_arguments.values())
		cls.command = cls.handler
		for setup_packet in cls.class_setup_packets:
			cls.handler.handle(setup_packet)

	def test_loop_all(self):
		self.loop_all()

	def test_update_commands(self):
		for key, update_type in StallUpdateType._value2member_map_.items():
			with self.subTest(f"Stall update commands- {update_type}"):
				self.handler.update_type = update_type
				self.assertIsNotNone(self.command.create_request())

	def test_update_responses(self):
		self.assertIsNone(self.handler.handle(self.responses_setup_packet))
		for update_type, packet in self.responses.items():
			with self.subTest(f"Stall update responses - {update_type.name}"):
				self.assertIsNone(self.handler.handle(packet))

	def test_registered_item_methods(self):
		item = Item(62)
		ri1 = StallItem(item, *self.registered_item_arguments.values())
		ri2 = StallItem(item, *self.registered_item_arguments.values())
		ri2.slot = 10
		with self.subTest("StallItem.__repr__()"):
			all([str(value) in ri1.__repr__() for value in self.stall_arguments.values()])
		with self.subTest("StallItem.__str__()"):
			all([str(value) in ri1.__str__() for value in self.stall_arguments.values()])
		with self.subTest("StallItem.__lt__()"):
			self.assertGreater(ri2, ri1)
			self.assertFalse(ri2.__gt__(None))
		with self.subTest("StallItem.__gt__()"):
			self.assertLess(ri1, ri2)
			self.assertFalse(ri1.__lt__(None))


if __name__ == '__main__':
	unittest.main()
