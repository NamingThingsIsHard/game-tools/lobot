import unittest

from lobot_api.packets.community.fortress_info_handler import FortressInfoHandler
from lobot_api.packets.storage.StorageInfoDataHandler import StorageInfoDataHandler
from silkroad_security_api_1_4.tests.packet_tests.packet_test import PacketTest


class TestFortressInfoData(PacketTest):
	tempBotUniqueID = 0xFD0E34

	@classmethod
	def setUpClass(cls) -> None:
		super().setUpClass()
		cls.load_test_fixture(cls.get_fixtures_folder_path() / "community" / "0x385F_FortressInfoHandler.json")
		cls.handler = FortressInfoHandler()
		for setup_packet in cls.class_setup_packets:
			cls.handler.handle(setup_packet)

	def test_loop_all(self):
		self.loop_all()


if __name__ == '__main__':
	unittest.main()
