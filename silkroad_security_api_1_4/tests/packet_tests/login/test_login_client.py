import contextlib
import io
import logging
import unittest

from lobot_api.logger.Logger import NETWORK
from lobot_api.packets.login.LoginClient import LoginClient
from silkroad_security_api_1_4.tests.packet_tests.packet_test import PacketTest


class TestLoginClient(PacketTest):
	@classmethod
	def setUpClass(cls) -> None:
		super().setUpClass()
		cls.load_test_fixture(cls.get_fixtures_folder_path() / "login" / "0x6103_0xA102_LoginClient.json")
		cls.handler = LoginClient.from_empty_constructor()
		for setup_packet in cls.class_setup_packets:
			cls.handler.handle(setup_packet)

	def test_login_client_valid(self):
		self.assertIsNotNone(self.handler.handle(self.tests["login_client_valid"].packet))

	def test_login_client_invalid(self):
		self.assertTrue(self.handler.handle(self.tests["login_client_invalid"].packet))

	def test_login_client_response_valid(self):
		self.assertIsNotNone(self.handler.handle(self.tests["login_client_response_valid"].packet))

	def test_login_client_response_invalid_test(self):
		login_client_error = "NETWORK:root:LoginClient Error A102 - True - False - 02 01"
		with self.assertLogs() as cm:
			self.assertIsNone(self.handler.handle(self.tests["login_client_response_invalid_test"].packet))
			self.assertIn(login_client_error, cm.output)


if __name__ == '__main__':
	unittest.main()
