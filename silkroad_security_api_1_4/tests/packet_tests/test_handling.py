import importlib
import inspect
import os
import re
import sys
import unittest
from pathlib import Path
from typing import List

from lobot_api.packets.IHandler import IHandler
from lobot_api.packets.IRequest import IRequest


class MyTestCase(unittest.TestCase):
	handler_classes: List[IHandler] = []
	request_classes: List[IRequest] = []

	@classmethod
	def setUpClass(cls) -> None:
		cls.setup_class_lists()

	@classmethod
	def tearDownClass(cls) -> None:
		cls.handler_classes = []
		cls.request_classes = []

	@classmethod
	def setup_class_lists(cls):
		"""Populate the list of available handlers and requests.
		Scans folder with all handler or request classes and keeps the ones that aren't interfaces.

		:return: None
		"""
		project_path = Path(os.path.dirname(os.path.realpath(__file__))).parent.parent.parent
		path = Path(project_path) / "lobot_api/packets/"

		for root, dirs, names in os.walk(path):
			for name in filter(lambda n: n.endswith(".py"), names):
				path_str = re.sub(r"[/]", '.', str(Path(os.path.relpath(root, project_path))))
				class_name = path_str + '.' + name.replace('.py', '')
				importlib.import_module(class_name)

		handlers = []
		requests = []
		handler_base_classes = []
		request_base_classes = []

		for class_type in IHandler.__subclasses__():
			handlers.append(class_type)

		for class_type in IRequest.__subclasses__():
			requests.append(class_type)

		# get nested subclasses of IHandler
		while handlers:
			c = handlers.pop(0)
			if c:
				sub = c.__subclasses__()
				if sub:
					handlers.extend(sub)
			handler_base_classes.append(c)

		# get nested subclasses of IHandler
		while requests:
			c = requests.pop(0)
			if c:
				sub = c.__subclasses__()
				if sub:
					requests.extend(sub)
			request_base_classes.append(c)

		while handler_base_classes:
			class_type = handler_base_classes.pop(0)
			try:
				if not inspect.isabstract(class_type):
					instance = class_type.from_empty_constructor()
					cls.handler_classes.append(instance)
			except NotImplementedError:
				sys.stdout.write(f"{class_type.__name__}.handle not implemented.\n")
			except Exception as e:
				sys.stdout.write(f"{class_type.__name__}:{str(e)}\n")

		while request_base_classes:
			class_type = request_base_classes.pop(0)
			try:
				if not inspect.isabstract(class_type):
					instance = class_type.from_empty_constructor()
					cls.request_classes.append(instance)
			except NotImplementedError:
				sys.stdout.write(f"{class_type.__name__}.handle not implemented.\n")
			except Exception as e:
				sys.stdout.write(f"{class_type.__name__}:{str(e)}\n")

	def test_handler_server_opcode_return(self):
		for hf in self.handler_classes:
			with self.subTest(hf.__class__.__name__):
				server_opcode = hf.server_opcode
				self.assertIsInstance(server_opcode, int)

	def test_opcode_return(self):
		for hf in self.request_classes:
			with self.subTest(hf.__class__.__name__):
				opcode = hf.opcode
				self.assertIsInstance(opcode, int)


if __name__ == '__main__':
	unittest.main()
