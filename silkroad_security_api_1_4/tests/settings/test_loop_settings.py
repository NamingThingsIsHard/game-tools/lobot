import unittest
from typing import Callable

from lobot_api.globals.settings.LoopSettings import ShoppingSettings


class TestShoppingSettings(unittest.TestCase):
	def does_not_raise(self, method: Callable, *args):
		try:
			if not args:
				method()
			else:
				method(*args)
		except Exception as e:
			self.fail(e)

	def test_instance(self):
		self.assertIsNotNone(ShoppingSettings.instance())

	def test_hp_potion_index(self):
		self.does_not_raise(ShoppingSettings.instance().__setattr__, "hp_potion_index", "1")
		self.assertEqual(ShoppingSettings.instance().hp_potion_index, 1)

	def test_hp_potion_type(self):
		self.assertEqual(ShoppingSettings.instance().hp_potion_type, 4)

	def test_mp_potion_index(self):
		self.does_not_raise(ShoppingSettings.instance().__setattr__, "mp_potion_index", "1")
		self.assertEqual(ShoppingSettings.instance().mp_potion_index, 1)

	def test_mp_potion_type(self):
		self.assertEqual(ShoppingSettings.instance().mp_potion_type, 11)

	def test_vigor_potion_index(self):
		self.does_not_raise(ShoppingSettings.instance().__setattr__, "vigor_potion_index", "1")
		self.assertEqual(ShoppingSettings.instance().vigor_potion_index, 1)

	def test_vigor_potion_type(self):
		self.assertEqual(ShoppingSettings.instance().vigor_potion_type, 18)

	def test_universal_pill_index(self):
		self.does_not_raise(ShoppingSettings.instance().__setattr__, "universal_pill_index", "1")
		self.assertEqual(ShoppingSettings.instance().universal_pill_index, 1)

	def test_universal_pill_type(self):
		self.assertEqual(ShoppingSettings.instance().universal_pill_type, 55)

	def test_purification_pill_index(self):
		self.does_not_raise(ShoppingSettings.instance().__setattr__, "purification_pill_index", "1")
		self.assertEqual(ShoppingSettings.instance().purification_pill_index, 1)

	def test_purification_pill_type(self):
		self.assertEqual(ShoppingSettings.instance().purification_pill_type, 10374)

	def test_projectile_index(self):
		self.does_not_raise(ShoppingSettings.instance().__setattr__, "projectile_index", "1")
		self.assertEqual(ShoppingSettings.instance().projectile_index, 1)

	def test_projectile_type(self):
		self.assertEqual(ShoppingSettings.instance().projectile_type, 62)

	def test_speed_pot_index(self):
		self.does_not_raise(ShoppingSettings.instance().__setattr__, "speed_pot_index", "1")
		self.assertEqual(ShoppingSettings.instance().speed_pot_index, 1)

	def test_speed_pot_type(self):
		self.assertEqual(ShoppingSettings.instance().speed_pot_type, 7098)

	def test_return_scroll_index(self):
		self.does_not_raise(ShoppingSettings.instance().__setattr__, "return_scroll_index", "1")
		self.assertEqual(ShoppingSettings.instance().return_scroll_index, 1)

	def test_return_scroll_type(self):
		self.assertEqual(ShoppingSettings.instance().return_scroll_type, 61)

	def test_transport_index(self):
		self.does_not_raise(ShoppingSettings.instance().__setattr__, "transport_index", "1")
		self.assertEqual(ShoppingSettings.instance().transport_index, 1)

	def test_transport_type(self):
		self.assertEqual(ShoppingSettings.instance().transport_type, 2137)

	def test_zerk_potion_index(self):
		self.does_not_raise(ShoppingSettings.instance().__setattr__, "zerk_potion_index", "1")
		self.assertEqual(ShoppingSettings.instance().zerk_potion_index, 1)

	def test_zerk_potion_type(self):
		self.assertEqual(ShoppingSettings.instance().zerk_potion_type, 24993)

	def test_hp_potion_quantity(self):
		self.does_not_raise(ShoppingSettings.instance().__setattr__, "hp_potion_quantity", "1")
		self.assertEqual(ShoppingSettings.instance().hp_potion_quantity, 1)

	def test_mp_potion_quantity(self):
		self.does_not_raise(ShoppingSettings.instance().__setattr__, "mp_potion_quantity", "1")
		self.assertEqual(ShoppingSettings.instance().mp_potion_quantity, 1)

	def test_vigor_potion_quantity(self):
		self.does_not_raise(ShoppingSettings.instance().__setattr__, "vigor_potion_quantity", "1")
		self.assertEqual(ShoppingSettings.instance().vigor_potion_quantity, 1)

	def test_universal_pill_quantity(self):
		self.does_not_raise(ShoppingSettings.instance().__setattr__, "universal_pill_quantity", "1")
		self.assertEqual(ShoppingSettings.instance().universal_pill_quantity, 1)

	def test_purification_pill_quantity(self):
		self.does_not_raise(ShoppingSettings.instance().__setattr__, "purification_pill_quantity", "1")
		self.assertEqual(ShoppingSettings.instance().purification_pill_quantity, 1)

	def test_projectile_quantity(self):
		self.does_not_raise(ShoppingSettings.instance().__setattr__, "projectile_quantity", "1")
		self.assertEqual(ShoppingSettings.instance().projectile_quantity, 1)

	def test_speed_pot_quantity(self):
		self.does_not_raise(ShoppingSettings.instance().__setattr__, "speed_pot_quantity", "1")
		self.assertEqual(ShoppingSettings.instance().speed_pot_quantity, 1)

	def test_return_scroll_quantity(self):
		self.does_not_raise(ShoppingSettings.instance().__setattr__, "return_scroll_quantity", "1")
		self.assertEqual(ShoppingSettings.instance().return_scroll_quantity, 1)

	def test_transport_quantity(self):
		self.does_not_raise(ShoppingSettings.instance().__setattr__, "transport_quantity", "1")
		self.assertEqual(ShoppingSettings.instance().transport_quantity, 1)

	def test_zerk_potion_quantity(self):
		self.does_not_raise(ShoppingSettings.instance().__setattr__, "zerk_potion_quantity", "1")
		self.assertEqual(ShoppingSettings.instance().zerk_potion_quantity, 1)

	def test_pet_hp_potion_index(self):
		self.does_not_raise(ShoppingSettings.instance().__setattr__, "pet_hp_potion_index", "1")
		self.assertEqual(ShoppingSettings.instance().pet_hp_potion_index, 1)

	def test_pet_hp_potion_type(self):
		self.assertEqual(ShoppingSettings.instance().pet_hp_potion_type, 2143)

	def test_pet_pill_index(self):
		self.does_not_raise(ShoppingSettings.instance().__setattr__, "pet_pill_index", "1")
		self.assertEqual(ShoppingSettings.instance().pet_pill_index, 1)

	def test_pet_pill_type(self):
		self.assertEqual(ShoppingSettings.instance().pet_pill_type, 9226)

	def test_pethgp_id(self):
		self.assertEqual(ShoppingSettings.instance().pethgp_id, 7553)

	def test_petgrassoflife_id(self):
		self.assertEqual(ShoppingSettings.instance().petgrassoflife_id, 7552)

	def test_pet_hp_potion_quantity(self):
		self.does_not_raise(ShoppingSettings.instance().__setattr__, "pet_hp_potion_quantity", "1")
		self.assertEqual(ShoppingSettings.instance().pet_hp_potion_quantity, 1)

	def test_pet_pill_quantity(self):
		self.does_not_raise(ShoppingSettings.instance().__setattr__, "pet_pill_quantity", "1")
		self.assertEqual(ShoppingSettings.instance().pet_pill_quantity, 1)

	def test_hgp_potion_quantity(self):
		self.does_not_raise(ShoppingSettings.instance().__setattr__, "hgp_potion_quantity", "1")
		self.assertEqual(ShoppingSettings.instance().hgp_potion_quantity, 1)

	def test_grass_of_life_quantity(self):
		self.does_not_raise(ShoppingSettings.instance().__setattr__, "grass_of_life_quantity", "1")
		self.assertEqual(ShoppingSettings.instance().grass_of_life_quantity, 1)


if __name__ == '__main__':
	unittest.main()
