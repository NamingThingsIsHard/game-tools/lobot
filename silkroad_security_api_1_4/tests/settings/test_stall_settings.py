from typing import Callable
from unittest import TestCase

from lobot_api.globals.settings.StallSettings import StallSettings


class TestStallSettings(TestCase):
	def does_not_raise(self, method: Callable, *args):
		try:
			if not args:
				method()
			else:
				method(*args)
		except Exception as e:
			self.fail(e)

	def test_instance(self):
		self.assertIsNotNone(StallSettings.instance())

	def test_auto_add(self):
		self.does_not_raise(StallSettings.instance().__setattr__, "auto_add", "False")
		self.assertEqual(StallSettings.instance().auto_add, False)

	def test_resume_training(self):
		self.does_not_raise(StallSettings.instance().__setattr__, "resume_training", "False")
		self.assertEqual(StallSettings.instance().resume_training, False)
