from typing import Callable
from unittest import TestCase

from lobot_api.globals.settings.PartySettings import PartySettings
from lobot_api.structs.BoolEventArgs import BoolEventArgs
from lobot_api.structs.PartyObjective import PartyObjective
from lobot_api.structs.PartyType import PartyType


class TestBoolEventArgs(TestCase):
	def test_bool_event_args(self):
		result = BoolEventArgs(False)
		self.assertIsNotNone(result)
		self.assertFalse(result.flag)


class TestPartySettings(TestCase):
	def does_not_raise(self, method: Callable, *args):
		try:
			if not args:
				method()
			else:
				method(*args)
		except Exception as e:
			self.fail(e)

	def test_instance(self):
		self.assertIsNotNone(PartySettings.instance())

	def test_objective(self):
		self.does_not_raise(PartySettings.instance().__setattr__, "objective", str(PartyObjective.Thief.value))
		self.assertEqual(PartySettings.instance().objective, PartyObjective.Thief)

	def test_title(self):
		title = "Test Title"
		self.does_not_raise(PartySettings.instance().__setattr__, "title", title)
		self.assertEqual(PartySettings.instance().title, title)

	def test_type(self):
		self.does_not_raise(PartySettings.instance().__setattr__, "type", str(PartyType.OpenShareShare.value))
		self.assertEqual(PartySettings.instance().type, PartyType.OpenShareShare)

	def test_members_can_invite(self):
		self.does_not_raise(PartySettings.instance().__setattr__, "members_can_invite", "False")
		self.assertEqual(PartySettings.instance().members_can_invite, False)

	def test_only_members(self):
		self.does_not_raise(PartySettings.instance().__setattr__, "only_members", "False")
		self.assertEqual(PartySettings.instance().only_members, False)

	def test_auto_invite(self):
		self.does_not_raise(PartySettings.instance().__setattr__, "auto_invite", "False")
		self.assertEqual(PartySettings.instance().auto_invite, False)

	def test_auto_accept_invite(self):
		self.does_not_raise(PartySettings.instance().__setattr__, "auto_accept_invite", "False")
		self.assertEqual(PartySettings.instance().auto_accept_invite, False)

	def test_auto_reform(self):
		self.does_not_raise(PartySettings.instance().__setattr__, "auto_reform", "False")
		self.assertEqual(PartySettings.instance().auto_reform, False)

	def test_auto_join(self):
		self.does_not_raise(PartySettings.instance().__setattr__, "auto_join", "False")
		self.assertEqual(PartySettings.instance().auto_join, False)

	def test_min_level(self):
		self.does_not_raise(PartySettings.instance().__setattr__, "min_level", "1")
		self.assertEqual(PartySettings.instance().min_level, 1)

	def test_max_level(self):
		self.does_not_raise(PartySettings.instance().__setattr__, "max_level", "100")
		self.assertEqual(PartySettings.instance().max_level, 100)

	def test_member_list(self):
		self.does_not_raise(PartySettings.instance().__setattr__, "member_list", "Me,You,Someone")
		self.assertEqual(PartySettings.instance().member_list, ["Me", "You", "Someone"])
