from typing import Callable
from unittest import TestCase

from lobot_api.globals.settings.LoginSettings import LoginSettings


class TestLoginSettings(TestCase):
	def does_not_raise(self, method: Callable, *args):
		try:
			if not args:
				method()
			else:
				method(*args)
		except Exception as e:
			self.fail(e)

	def test_instance(self):
		self.assertIsNotNone(LoginSettings.instance())

	def test_locale(self):
		self.does_not_raise(LoginSettings.instance().__setattr__, "locale", "22")
		self.assertEqual(LoginSettings.instance().locale, 22)

	def test_username(self):
		self.does_not_raise(LoginSettings.instance().__setattr__, "username", "user")
		self.assertEqual(LoginSettings.instance().username, "user")

	def test_password(self):
		self.does_not_raise(LoginSettings.instance().__setattr__, "password", "secure_password")
		self.assertEqual(LoginSettings.instance().password, "secure_password")

	def test_char_name(self):
		self.does_not_raise(LoginSettings.instance().__setattr__, "char_name", "character")
		self.assertEqual(LoginSettings.instance().char_name, "character")

	def test_login_client_directory_path(self):
		self.does_not_raise(LoginSettings.instance().__setattr__, "login_client_directory_path", "empty")
		self.assertEqual(LoginSettings.instance().login_client_directory_path, "empty")

	def test_auto_select(self):
		self.does_not_raise(LoginSettings.instance().__setattr__, "auto_select", "False")
		self.assertEqual(LoginSettings.instance().auto_select, False)

	def test_auto_connect(self):
		self.does_not_raise(LoginSettings.instance().__setattr__, "auto_connect", "False")
		self.assertEqual(LoginSettings.instance().auto_connect, False)

	def test_auto_connect_seconds(self):
		self.does_not_raise(LoginSettings.instance().__setattr__, "auto_connect_seconds", "5")
		self.assertEqual(LoginSettings.instance().auto_connect_seconds, 5)

	def test_action_on_login(self):
		self.does_not_raise(LoginSettings.instance().__setattr__, "action_on_login", "1")
		self.assertEqual(LoginSettings.instance().action_on_login, 1)

	def test_client_or_clientless(self):
		self.does_not_raise(LoginSettings.instance().__setattr__, "client_or_clientless", "False")
		self.assertEqual(LoginSettings.instance().client_or_clientless, False)

	def test_autostart_or_manual(self):
		self.does_not_raise(LoginSettings.instance().__setattr__, "autostart_or_manual", "False")
		self.assertEqual(LoginSettings.instance().autostart_or_manual, False)

	def test_auto_save_settings(self):
		self.does_not_raise(LoginSettings.instance().__setattr__, "auto_save_settings", "False")
		self.assertEqual(LoginSettings.instance().auto_save_settings, False)
