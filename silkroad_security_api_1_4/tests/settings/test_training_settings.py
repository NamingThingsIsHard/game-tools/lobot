import unittest
from typing import Callable

from lobot_api.Bot import Bot
from lobot_api.globals.settings.TrainingSettings import GeneralTrainingSettings, TrainingAreaSettings
from lobot_api.scripting.Area import Area


class TestGeneralTrainingSettings(unittest.TestCase):
	def does_not_raise(self, method: Callable, *args):
		try:
			if not args:
				method()
			else:
				method(*args)
		except Exception as e:
			self.fail(e)

	def test_instance(self):
		self.assertIsNotNone(GeneralTrainingSettings.instance())

	def test_prioritize_weakest(self):
		self.assertIsInstance(GeneralTrainingSettings.instance().prioritize_weakest, bool)

	def test_prioritize_weakest_setter(self):
		self.does_not_raise(GeneralTrainingSettings.instance().__setattr__, "prioritize_weakest", "False")
		self.assertEqual(GeneralTrainingSettings.instance().prioritize_weakest, False)

	def test_switch_on_aggro(self):
		self.assertIsInstance(GeneralTrainingSettings.instance().switch_on_aggro, bool)

	def test_switch_on_aggro_setter(self):
		self.does_not_raise(GeneralTrainingSettings.instance().__setattr__, "switch_on_aggro", "False")
		self.assertEqual(GeneralTrainingSettings.instance().switch_on_aggro, False)

	def test_defend_teammates(self):
		self.assertIsInstance(GeneralTrainingSettings.instance().defend_teammates, bool)

	def test_defend_teammates_setter(self):
		self.does_not_raise(GeneralTrainingSettings.instance().__setattr__, "defend_teammates", "False")
		self.assertEqual(GeneralTrainingSettings.instance().defend_teammates, False)

	def test_defend_pet(self):
		self.assertIsInstance(GeneralTrainingSettings.instance().defend_pet, bool)

	def test_defend_pet_setter(self):
		self.does_not_raise(GeneralTrainingSettings.instance().__setattr__, "defend_pet", "False")
		self.assertEqual(GeneralTrainingSettings.instance().defend_pet, False)

	def test_return_to_center(self):
		self.assertIsInstance(GeneralTrainingSettings.instance().return_to_center, bool)

	def test_return_to_center_setter(self):
		self.does_not_raise(GeneralTrainingSettings.instance().__setattr__, "return_to_center", "False")
		self.assertEqual(GeneralTrainingSettings.instance().return_to_center, False)

	def test_focus_unique(self):
		self.assertIsInstance(GeneralTrainingSettings.instance().focus_unique, bool)

	def test_focus_unique_setter(self):
		self.does_not_raise(GeneralTrainingSettings.instance().__setattr__, "focus_unique", "False")
		self.assertEqual(GeneralTrainingSettings.instance().focus_unique, False)

	def test_auto_attack_event_monster(self):
		self.assertIsInstance(GeneralTrainingSettings.instance().auto_attack_event_monster, bool)

	def test_auto_attack_event_monster_setter(self):
		self.does_not_raise(GeneralTrainingSettings.instance().__setattr__, "auto_attack_event_monster", "False")
		self.assertEqual(GeneralTrainingSettings.instance().auto_attack_event_monster, False)

	def test_warlock_dot_switch(self):
		self.assertIsInstance(GeneralTrainingSettings.instance().warlock_dot_switch, bool)

	def test_warlock_dot_switch_setter(self):
		self.does_not_raise(GeneralTrainingSettings.instance().__setattr__, "warlock_dot_switch", "False")
		self.assertEqual(GeneralTrainingSettings.instance().warlock_dot_switch, False)

	def test_zerk_when_ready(self):
		self.assertIsInstance(GeneralTrainingSettings.instance().zerk_when_ready, bool)

	def test_zerk_when_ready_setter(self):
		self.does_not_raise(GeneralTrainingSettings.instance().__setattr__, "zerk_when_ready", "False")
		self.assertEqual(GeneralTrainingSettings.instance().zerk_when_ready, False)

	def test_zerk_vs_strong(self):
		self.assertIsInstance(GeneralTrainingSettings.instance().zerk_vs_strong, bool)

	def test_zerk_vs_strong_setter(self):
		self.does_not_raise(GeneralTrainingSettings.instance().__setattr__, "zerk_vs_strong", "False")
		self.assertEqual(GeneralTrainingSettings.instance().zerk_vs_strong, False)

	def test_zerk_when_x_attackers(self):
		self.assertIsInstance(GeneralTrainingSettings.instance().zerk_when_x_attackers, bool)

	def test_zerk_when_x_attackers_setter(self):
		self.does_not_raise(GeneralTrainingSettings.instance().__setattr__, "zerk_when_x_attackers", "False")
		self.assertEqual(GeneralTrainingSettings.instance().zerk_when_x_attackers, False)

	def test_x_attackers(self):
		self.assertIsInstance(GeneralTrainingSettings.instance().x_attackers, int)

	def test_x_attackers_setter(self):
		self.does_not_raise(GeneralTrainingSettings.instance().__setattr__, "x_attackers", "1")
		self.assertEqual(GeneralTrainingSettings.instance().x_attackers, 1)

	def test_dont_attack(self):
		self.assertIsInstance(GeneralTrainingSettings.instance().dont_attack, bool)

	def test_dont_attack_setter(self):
		self.does_not_raise(GeneralTrainingSettings.instance().__setattr__, "dont_attack", "False")
		self.assertEqual(GeneralTrainingSettings.instance().dont_attack, False)

	def test_return_dead(self):
		self.assertIsInstance(GeneralTrainingSettings.instance().return_dead, bool)

	def test_return_dead_setter(self):
		self.does_not_raise(GeneralTrainingSettings.instance().__setattr__, "return_dead", "False")
		self.assertEqual(GeneralTrainingSettings.instance().return_dead, False)

	def test_return_dead_x_minutes(self):
		self.assertIsInstance(GeneralTrainingSettings.instance().return_dead_x_minutes, bool)

	def test_return_dead_x_minutes_setter(self):
		self.does_not_raise(GeneralTrainingSettings.instance().__setattr__, "return_dead_x_minutes", "False")
		self.assertEqual(GeneralTrainingSettings.instance().return_dead_x_minutes, False)

	def test_x_minutes(self):
		self.assertIsInstance(GeneralTrainingSettings.instance().x_minutes, int)

	def test_x_minutes_setter(self):
		self.does_not_raise(GeneralTrainingSettings.instance().__setattr__, "x_minutes", "1")
		self.assertEqual(GeneralTrainingSettings.instance().x_minutes, 1)

	def test_return_inventory_full(self):
		self.assertIsInstance(GeneralTrainingSettings.instance().return_inventory_full, bool)

	def test_return_inventory_full_setter(self):
		self.does_not_raise(GeneralTrainingSettings.instance().__setattr__, "return_inventory_full", "False")
		self.assertEqual(GeneralTrainingSettings.instance().return_inventory_full, False)

	def test_return_unique(self):
		self.assertIsInstance(GeneralTrainingSettings.instance().return_unique, bool)

	def test_return_unique_setter(self):
		self.does_not_raise(GeneralTrainingSettings.instance().__setattr__, "return_unique", "False")
		self.assertEqual(GeneralTrainingSettings.instance().return_unique, False)

	def test_return_gm_found(self):
		self.assertIsInstance(GeneralTrainingSettings.instance().return_gm_found, bool)

	def test_return_gm_found_setter(self):
		self.does_not_raise(GeneralTrainingSettings.instance().__setattr__, "return_gm_found", "False")
		self.assertEqual(GeneralTrainingSettings.instance().return_gm_found, False)

	def test_return_no_vigors(self):
		self.assertIsInstance(GeneralTrainingSettings.instance().return_no_vigors, bool)

	def test_return_no_vigors_setter(self):
		self.does_not_raise(GeneralTrainingSettings.instance().__setattr__, "return_no_vigors", "False")
		self.assertEqual(GeneralTrainingSettings.instance().return_no_vigors, False)

	def test_return_no_universal_pills(self):
		self.assertIsInstance(GeneralTrainingSettings.instance().return_no_universal_pills, bool)

	def test_return_no_universal_pills_setter(self):
		self.does_not_raise(GeneralTrainingSettings.instance().__setattr__, "return_no_universal_pills", "False")
		self.assertEqual(GeneralTrainingSettings.instance().return_no_universal_pills, False)

	def test_return_no_special_pills(self):
		self.assertIsInstance(GeneralTrainingSettings.instance().return_no_special_pills, bool)

	def test_return_no_special_pills_setter(self):
		self.does_not_raise(GeneralTrainingSettings.instance().__setattr__, "return_no_special_pills", "False")
		self.assertEqual(GeneralTrainingSettings.instance().return_no_special_pills, False)

	def test_return_low_hp_pots(self):
		self.assertIsInstance(GeneralTrainingSettings.instance().return_low_hp_pots, bool)

	def test_return_low_hp_pots_setter(self):
		self.does_not_raise(GeneralTrainingSettings.instance().__setattr__, "return_low_hp_pots", "False")
		self.assertEqual(GeneralTrainingSettings.instance().return_low_hp_pots, False)

	def test_low_hp_pots(self):
		self.assertIsInstance(GeneralTrainingSettings.instance().low_hp_pots, int)

	def test_low_hp_pots_setter(self):
		self.does_not_raise(GeneralTrainingSettings.instance().__setattr__, "low_hp_pots", "1")
		self.assertEqual(GeneralTrainingSettings.instance().low_hp_pots, 1)

	def test_return_low_mp_pots(self):
		self.assertIsInstance(GeneralTrainingSettings.instance().return_low_mp_pots, bool)

	def test_return_low_mp_pots_setter(self):
		self.does_not_raise(GeneralTrainingSettings.instance().__setattr__, "return_low_mp_pots", "False")
		self.assertEqual(GeneralTrainingSettings.instance().return_low_mp_pots, False)

	def test_low_mp_pots(self):
		self.assertIsInstance(GeneralTrainingSettings.instance().low_mp_pots, int)

	def test_low_mp_pots_setter(self):
		self.does_not_raise(GeneralTrainingSettings.instance().__setattr__, "low_mp_pots", "1")
		self.assertEqual(GeneralTrainingSettings.instance().low_mp_pots, 1)

	def test_return_low_ammo(self):
		self.assertIsInstance(GeneralTrainingSettings.instance().return_low_ammo, bool)

	def test_return_low_ammo_setter(self):
		self.does_not_raise(GeneralTrainingSettings.instance().__setattr__, "return_low_ammo", "False")
		self.assertEqual(GeneralTrainingSettings.instance().return_low_ammo, False)

	def test_low_ammo(self):
		self.assertIsInstance(GeneralTrainingSettings.instance().low_ammo, int)

	def test_low_ammo_setter(self):
		self.does_not_raise(GeneralTrainingSettings.instance().__setattr__, "low_ammo", "1")
		self.assertEqual(GeneralTrainingSettings.instance().low_ammo, 1)

	def test_return_low_weapon_durability(self):
		self.assertIsInstance(GeneralTrainingSettings.instance().return_low_weapon_durability, bool)

	def test_return_low_weapon_durability_setter(self):
		self.does_not_raise(GeneralTrainingSettings.instance().__setattr__, "return_low_weapon_durability", "False")
		self.assertEqual(GeneralTrainingSettings.instance().return_low_weapon_durability, False)

	def test_low_weapon_durability(self):
		self.assertIsInstance(GeneralTrainingSettings.instance().low_weapon_durability, int)

	def test_low_weapon_durability_setter(self):
		self.does_not_raise(GeneralTrainingSettings.instance().__setattr__, "low_weapon_durability", "1")
		self.assertEqual(GeneralTrainingSettings.instance().low_weapon_durability, 1)

	def test_return_x_broken_items(self):
		self.assertIsInstance(GeneralTrainingSettings.instance().return_x_broken_items, bool)

	def test_return_x_broken_items_setter(self):
		self.does_not_raise(GeneralTrainingSettings.instance().__setattr__, "return_x_broken_items", "False")
		self.assertEqual(GeneralTrainingSettings.instance().return_x_broken_items, False)

	def test_x_broken_items(self):
		self.assertIsInstance(GeneralTrainingSettings.instance().x_broken_items, int)

	def test_x_broken_items_setter(self):
		self.does_not_raise(GeneralTrainingSettings.instance().__setattr__, "x_broken_items", "1")
		self.assertEqual(GeneralTrainingSettings.instance().x_broken_items, 1)

	def test_return_quests_finished(self):
		self.assertIsInstance(GeneralTrainingSettings.instance().return_quests_finished, bool)

	def test_return_quests_finished_setter(self):
		self.does_not_raise(GeneralTrainingSettings.instance().__setattr__, "return_quests_finished", "False")
		self.assertEqual(GeneralTrainingSettings.instance().return_quests_finished, False)

	def test_x_quests_finished(self):
		self.assertIsInstance(GeneralTrainingSettings.instance().x_quests_finished, int)

	def test_x_quests_finished_setter(self):
		self.does_not_raise(GeneralTrainingSettings.instance().__setattr__, "x_quests_finished", "1")
		self.assertEqual(GeneralTrainingSettings.instance().x_quests_finished, 1)

	def test_return_criteria(self):
		self.assertIsInstance(GeneralTrainingSettings.instance().return_criteria, dict)

	def test_should_return_to_town(self):

		with self.assertLogs() as cm:
			GeneralTrainingSettings.instance().return_low_hp_pots = "False"
			GeneralTrainingSettings.instance().return_low_mp_pots = "False"
			self.assertFalse(GeneralTrainingSettings.instance().should_return_to_town())

			Bot.reset()
			GeneralTrainingSettings.instance().return_low_hp_pots = "True"
			self.assertTrue(GeneralTrainingSettings.instance().should_return_to_town())

			error_message = "Returning to town:"
			self.assertIn(error_message, cm.output[0])


class TestTrainingAreaSettings(unittest.TestCase):
	area1 = Area(1, 1, 1)
	area2 = Area(2, 2, 2)

	def does_not_raise(self, method: Callable, *args):
		try:
			if not args:
				method()
			else:
				method(*args)
		except Exception as e:
			self.fail(e)

	def test_instance(self):
		self.assertIsNotNone(TrainingAreaSettings.instance())

	def test_area1(self):
		self.assertIsInstance(TrainingAreaSettings.instance().area1, Area)

	def test_area1_setter(self):
		self.does_not_raise(TrainingAreaSettings.instance().__setattr__, "area1", "1,1,1")
		self.assertEqual(TrainingAreaSettings.instance().area1, self.area1)

	def test_area2(self):
		self.assertIsInstance(TrainingAreaSettings.instance().area2, Area)

	def test_area2_setter(self):
		self.does_not_raise(TrainingAreaSettings.instance().__setattr__, "area2", "2,2,2")
		self.assertEqual(TrainingAreaSettings.instance().area2, self.area2)

	def test_use_second_area(self):
		self.assertIsInstance(TrainingAreaSettings.instance().use_second_area, bool)

	def test_use_second_area_setter(self):
		self.does_not_raise(TrainingAreaSettings.instance().__setattr__, "use_second_area", "False")
		self.assertEqual(TrainingAreaSettings.instance().use_second_area, False)

	def test_stay_idle_option(self):
		self.assertIsInstance(TrainingAreaSettings.instance().stay_idle_option, bool)

	def test_stay_idle_option_setter(self):
		self.does_not_raise(TrainingAreaSettings.instance().__setattr__, "stay_idle_option", "False")
		self.assertEqual(TrainingAreaSettings.instance().stay_idle_option, False)


if __name__ == '__main__':
	unittest.main()
