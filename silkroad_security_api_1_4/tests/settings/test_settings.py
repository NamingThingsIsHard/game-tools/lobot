import contextlib
import io
from unittest import TestCase

from lobot_api.globals.settings import Settings


class TestSettings(TestCase):
	def test_load_settings(self):
		with contextlib.redirect_stdout(io.StringIO()):
			Settings.load_settings()

	def test_parse_pk2item(self):
		pk2item_str = "1|ITEM_ETC_GOLD_01|Gold|0|350|3|0|1|0|0|2|1"
		Settings.parse_pk2item(pk2item_str)

	def test_parse_pk2skill(self):
		pk2item_str = "5000|HSKILL_SPEAR_PHYSICAL_0992||1166|1749|5|0|F|1"
		Settings.parse_pk2skill(pk2item_str)

	def test_parse_itemfilter(self):
		item_filter_str = "1101"
		result = Settings.parse_itemfilter(item_filter_str)
		self.assertEqual("1101", str(result))

	def test_parse_degreefilter(self):
		flags = format(0xFF0, '012b')
		result = Settings.parse_degreefilter(flags)
		self.assertEqual(format(0xFF0, '012b'), str(result))
