from typing import Callable
from unittest import TestCase

from silkroad_security_api_1_4.Packet import Packet


class TestPacket(TestCase):
	opcode = 0x7021
	data = [0, 1, 2, 3, 4, 5, 6, 7]
	encrypted = False
	massive = False
	offset = 1
	packet = Packet(opcode, encrypted, massive, data)

	def does_not_raise(self, method: Callable, *args):
		try:
			if not args:
				method()
			else:
				method(*args)
		except Exception as e:
			self.fail(e)

	def test_init(self):
		self.does_not_raise(Packet, [self.opcode, self.encrypted, self.massive, self.data])
		self.does_not_raise(Packet, [self.opcode, self.encrypted, self.massive, self.data, self.offset])
		self.assertRaises(Exception, Packet, self.opcode, True, True, self.data)

	def test_repr(self):
		self.assertTrue(self.packet.__repr__())

	def test_opcode(self):
		self.assertEqual(self.packet.opcode, self.opcode)

	def test_encrypted(self):
		self.assertEqual(self.packet.encrypted, self.encrypted)

	def test_massive(self):
		self.assertEqual(self.packet.massive, self.massive)

	def test_get_bytes(self):
		new_packet = Packet(self.opcode, self.encrypted, self.encrypted, self.data)
		self.does_not_raise(new_packet.get_bytes)
		self.does_not_raise(new_packet.get_bytes)

	def test_set(self):
		new_packet = Packet(self.opcode, self.encrypted, self.encrypted, self.data)
		self.does_not_raise(self.packet.__set__, Packet(self.opcode, self.encrypted, self.encrypted, self.data))
		new_packet.lock()
		self.does_not_raise(self.packet.__set__, new_packet)

	def test_lock(self):
		new_packet = Packet(self.opcode, self.encrypted, self.encrypted, self.data)
		new_packet.lock()
		self.assertTrue(new_packet.m_locked)

	def test_seek_read(self):
		new_packet = Packet(self.opcode, self.encrypted, self.encrypted, self.data)
		self.assertRaises(Exception, new_packet.seek_read, 0, 0)
		new_packet.lock()
		self.does_not_raise(new_packet.seek_read, 0, 0)

	def test_remaining_read(self):
		new_packet = Packet(self.opcode, self.encrypted, self.encrypted, self.data)
		self.assertRaises(Exception, new_packet.remaining_read)
		new_packet.lock()
		self.assertEqual(new_packet.remaining_read(), 8)

	def test_read_uint8(self):
		new_packet = Packet(self.opcode, self.encrypted, self.encrypted, self.data)
		new_packet.lock()
		self.assertEqual(new_packet.read_uint8(), 0)

	def test_read_int8(self):
		new_packet = Packet(self.opcode, self.encrypted, self.encrypted, self.data)
		new_packet.lock()
		self.assertEqual(new_packet.read_int8(), 0)

	def test_read_uint16(self):
		new_packet = Packet(self.opcode, self.encrypted, self.encrypted, self.data)
		new_packet.lock()
		self.assertEqual(new_packet.read_uint16(), 0x100)

	def test_read_int16(self):
		new_packet = Packet(self.opcode, self.encrypted, self.encrypted, self.data)
		new_packet.lock()
		self.assertEqual(new_packet.read_int16(), 0x100)

	def test_read_uint32(self):
		new_packet = Packet(self.opcode, self.encrypted, self.encrypted, self.data)
		new_packet.lock()
		self.assertEqual(new_packet.read_uint32(), 0x3020100)

	def test_read_int32(self):
		new_packet = Packet(self.opcode, self.encrypted, self.encrypted, self.data)
		new_packet.lock()
		self.assertEqual(new_packet.read_int32(), 0x3020100)

	def test_read_uint64(self):
		new_packet = Packet(self.opcode, self.encrypted, self.encrypted, self.data)
		new_packet.lock()
		self.assertEqual(new_packet.read_uint64(), 0x706050403020100)

	def test_read_int64(self):
		new_packet = Packet(self.opcode, self.encrypted, self.encrypted, self.data)
		new_packet.lock()
		self.assertEqual(new_packet.read_int64(), 0x706050403020100)

	def test_read_float(self):
		new_packet = Packet(self.opcode, self.encrypted, self.encrypted, [0, 0, 0, 0])
		new_packet.lock()
		self.assertEqual(new_packet.read_float(), 0)

	def test_read_single(self):
		new_packet = Packet(self.opcode, self.encrypted, self.encrypted, [0, 0, 0, 0])
		new_packet.lock()
		self.assertEqual(new_packet.read_single(), 0.0)

	def test_read_double(self):
		new_packet = Packet(self.opcode, self.encrypted, self.encrypted, [0, 0, 0, 0, 0, 0, 0, 0])
		new_packet.lock()
		self.assertEqual(new_packet.read_double(), 0.0)

	def test_read_ascii(self):
		new_packet = Packet(self.opcode, self.encrypted, self.encrypted, [0x04, 0x00, 0x74, 0x65, 0x73, 0x74])
		new_packet.lock()
		self.assertEqual(new_packet.read_ascii(), "test")

	def test_read_unicode(self):
		new_packet = Packet(self.opcode, self.encrypted, self.encrypted, [0x04, 0x00, 0x74, 0x00, 0x65, 0x00, 0x73, 0x00, 0x74, 0x0])
		new_packet.lock()
		self.assertEqual(new_packet.read_unicode(), "test")

	def test_read_uint8_array(self):
		new_packet = Packet(self.opcode, self.encrypted, self.encrypted, [0, 0, 0, 0, 0, 0, 0, 0])
		new_packet.lock()
		self.assertEqual(new_packet.read_uint8_array(6), [0, 0, 0, 0, 0, 0])

	def test_read_int8_array(self):
		new_packet = Packet(self.opcode, self.encrypted, self.encrypted, [0, 0, 0, 0, 0, 0, 0, 0])
		new_packet.lock()
		self.assertEqual(new_packet.read_int8_array(6), [0, 0, 0, 0, 0, 0])

	def test_read_uint16_array(self):
		new_packet = Packet(self.opcode, self.encrypted, self.encrypted, [0, 0, 0, 0, 0, 0, 0, 0])
		new_packet.lock()
		self.assertEqual(new_packet.read_uint16_array(3), [0, 0, 0])

	def test_read_int16_array(self):
		new_packet = Packet(self.opcode, self.encrypted, self.encrypted, [0, 0, 0, 0, 0, 0, 0, 0])
		new_packet.lock()
		self.assertEqual(new_packet.read_int16_array(3), [0, 0, 0])

	def test_read_uint32_array(self):
		new_packet = Packet(self.opcode, self.encrypted, self.encrypted, [0, 0, 0, 0, 0, 0])
		new_packet.lock()
		self.assertEqual(new_packet.read_uint32_array(1), [0])

	def test_read_int32_array(self):
		new_packet = Packet(self.opcode, self.encrypted, self.encrypted, [0, 0, 0, 0, 0, 0])
		new_packet.lock()
		self.assertEqual(new_packet.read_int32_array(1), [0])

	def test_read_uint64_array(self):
		new_packet = Packet(self.opcode, self.encrypted, self.encrypted, [0, 0, 0, 0, 0, 0, 0, 0, 0, 0])
		new_packet.lock()
		self.assertEqual(new_packet.read_uint64_array(1), [0])

	def test_read_int64_array(self):
		new_packet = Packet(self.opcode, self.encrypted, self.encrypted, [0, 0, 0, 0, 0, 0, 0, 0, 0, 0])
		new_packet.lock()
		self.assertEqual(new_packet.read_int64_array(1), [0])

	def test_read_float_array(self):
		new_packet = Packet(self.opcode, self.encrypted, self.encrypted, [0, 0, 0, 0, 0, 0, 0, 0, 0, 0])
		new_packet.lock()
		self.assertEqual(new_packet.read_float_array(2), [0.0, 0.0])

	def test_read_double_array(self):
		new_packet = Packet(self.opcode, self.encrypted, self.encrypted, [0, 0, 0, 0, 0, 0, 0, 0, 0, 0])
		new_packet.lock()
		self.assertEqual(new_packet.read_double_array(1), [0.0])

	def test_read_ascii_array(self):
		new_packet = Packet(self.opcode, self.encrypted, self.encrypted, [0x04, 0x00, 0x74, 0x65, 0x73, 0x74])
		new_packet.lock()
		self.assertEqual(new_packet.read_ascii_array(), ["test"])

	def test_read_unicode_array(self):
		new_packet = Packet(self.opcode, self.encrypted, self.encrypted, [0x04, 0x00, 0x74, 0x00, 0x65, 0x00, 0x73, 0x00, 0x74, 0x00])
		new_packet.lock()
		self.assertEqual(new_packet.read_unicode_array(1), ["test"])

	def test_seek_write(self):
		new_packet = Packet(self.opcode, self.encrypted, self.encrypted, [0x00])
		self.does_not_raise(new_packet.seek_write, 1, 0)
		new_packet.lock()
		self.assertRaises(Exception, new_packet.seek_write, 1, 0)

	def test_write_uint8(self):
		new_packet = Packet(self.opcode, self.encrypted, self.encrypted, [])
		self.does_not_raise(new_packet.write_uint8, 0)
		new_packet.lock()
		self.assertRaises(Exception, new_packet.write_uint8, 0)
		self.assertEqual(new_packet.read_uint8(), 0)

	def test_write_int8(self):
		new_packet = Packet(self.opcode, self.encrypted, self.encrypted, [])
		self.does_not_raise(new_packet.write_int8, 0xF)
		new_packet.lock()
		self.assertRaises(Exception, new_packet.write_int8, 0)
		self.assertEqual(new_packet.read_int8(), 0xF)

	def test_write_uint16(self):
		new_packet = Packet(self.opcode, self.encrypted, self.encrypted, [])
		self.does_not_raise(new_packet.write_uint16, 0xFFFF)
		new_packet.lock()
		self.assertRaises(Exception, new_packet.write_int16, 0)
		self.assertEqual(new_packet.read_uint16(), 0xFFFF)

	def test_write_int16(self):
		new_packet = Packet(self.opcode, self.encrypted, self.encrypted, [])
		self.does_not_raise(new_packet.write_int16, 0xFFF)
		new_packet.lock()
		self.assertRaises(Exception, new_packet.write_int16, 0)
		self.assertEqual(new_packet.read_int16(), 0xFFF)

	def test_write_uint32(self):
		new_packet = Packet(self.opcode, self.encrypted, self.encrypted, [])
		self.does_not_raise(new_packet.write_uint32, 0xFFFFFFFF)
		new_packet.lock()
		self.assertRaises(Exception, new_packet.write_uint32, 0)
		self.assertEqual(new_packet.read_uint32(), 0xFFFFFFFF)

	def test_write_int32(self):
		new_packet = Packet(self.opcode, self.encrypted, self.encrypted, [])
		self.does_not_raise(new_packet.write_int32, 0xFFFFFFF)
		new_packet.lock()
		self.assertRaises(Exception, new_packet.write_int32, 0)
		self.assertEqual(new_packet.read_int32(), 0xFFFFFFF)

	def test_write_uint64(self):
		new_packet = Packet(self.opcode, self.encrypted, self.encrypted, [])
		self.does_not_raise(new_packet.write_uint64, 0xFFFFFFFFFFFFFFFF)
		new_packet.lock()
		self.assertRaises(Exception, new_packet.write_int64, 0)
		self.assertEqual(new_packet.read_uint64(), 0xFFFFFFFFFFFFFFFF)

	def test_write_int64(self):
		new_packet = Packet(self.opcode, self.encrypted, self.encrypted, [])
		self.does_not_raise(new_packet.write_int64, 0xFFFFFFFFFFFFFFF)
		new_packet.lock()
		self.assertRaises(Exception, new_packet.write_int64, 0)
		self.assertEqual(new_packet.read_int64(), 0xFFFFFFFFFFFFFFF)

	def test_write_float(self):
		new_packet = Packet(self.opcode, self.encrypted, self.encrypted, [])
		self.does_not_raise(new_packet.write_float, 128)
		new_packet.lock()
		self.assertRaises(Exception, new_packet.write_float, 128.128)
		self.assertEqual(new_packet.read_float(), 128)

	def test_write_double(self):
		new_packet = Packet(self.opcode, self.encrypted, self.encrypted, [])
		self.does_not_raise(new_packet.write_double, 256.256)
		new_packet.lock()
		self.assertRaises(Exception, new_packet.write_double, 256.256)
		self.assertEqual(new_packet.read_double(), 256.256)

	def test_write_ascii(self):
		new_packet = Packet(self.opcode, self.encrypted, self.encrypted, [])
		self.does_not_raise(new_packet.write_ascii, "test")
		new_packet.lock()
		self.assertRaises(Exception, new_packet.write_ascii, "test")
		self.assertEqual(new_packet.read_ascii(), "test")

	def test_write_unicode(self):
		new_packet = Packet(self.opcode, self.encrypted, self.encrypted, [])
		self.does_not_raise(new_packet.write_unicode, "test")
		new_packet.lock()
		self.assertRaises(Exception, new_packet.write_unicode, "test")
		self.assertEqual(new_packet.read_unicode(), "test")

	def test_write_uint_array(self):
		new_packet = Packet(self.opcode, self.encrypted, self.encrypted, [])
		self.does_not_raise(new_packet.write_uint_array, [0])
		new_packet.lock()
		self.assertRaises(Exception, new_packet.write_uint_array, [0])
		self.assertEqual(new_packet.read_uint8_array(1), [0])

	def test_write_uint16_array(self):
		new_packet = Packet(self.opcode, self.encrypted, self.encrypted, [])
		self.does_not_raise(new_packet.write_uint16_array, [0xFF])
		new_packet.lock()
		self.assertRaises(Exception, new_packet.read_uint16_array, [0xFF])
		self.assertEqual(new_packet.read_uint16_array(1), [0xFF])

	def test_write_int16_array(self):
		new_packet = Packet(self.opcode, self.encrypted, self.encrypted, [])
		self.does_not_raise(new_packet.write_int16_array, [0xFF])
		new_packet.lock()
		self.assertRaises(Exception, new_packet.read_int16_array, [0xFF])
		self.assertEqual(new_packet.read_int16_array(1), [0xFF])

	def test_write_uint32_array(self):
		new_packet = Packet(self.opcode, self.encrypted, self.encrypted, [])
		self.does_not_raise(new_packet.write_uint32_array, [0xFFFF])
		new_packet.lock()
		self.assertRaises(Exception, new_packet.read_uint32_array, [0xFFFF])
		self.assertEqual(new_packet.read_uint32_array(1), [0xFFFF])

	def test_write_int32_array(self):
		new_packet = Packet(self.opcode, self.encrypted, self.encrypted, [])
		self.does_not_raise(new_packet.write_int32_array, [0xFFFF])
		new_packet.lock()
		self.assertRaises(Exception, new_packet.read_int32_array, [0xFFFF])
		self.assertEqual(new_packet.read_int32_array(1), [0xFFFF])

	def test_write_uint64_array(self):
		new_packet = Packet(self.opcode, self.encrypted, self.encrypted, [])
		self.does_not_raise(new_packet.write_uint64_array, [0xFFFFFFFF])
		new_packet.lock()
		self.assertRaises(Exception, new_packet.read_uint64_array, [0xFFFFFFFF])
		self.assertEqual(new_packet.read_uint64_array(1), [0xFFFFFFFF])

	def test_write_int64_array(self):
		new_packet = Packet(self.opcode, self.encrypted, self.encrypted, [])
		self.does_not_raise(new_packet.write_int64_array, [0xFFFFFFFF])
		new_packet.lock()
		self.assertRaises(Exception, new_packet.read_int64_array, [0xFFFFFFFF])
		self.assertEqual(new_packet.read_int64_array(1), [0xFFFFFFFF])

	def test_write_float_array(self):
		new_packet = Packet(self.opcode, self.encrypted, self.encrypted, [])
		self.does_not_raise(new_packet.write_float_array, [128])
		new_packet.lock()
		self.assertRaises(Exception, new_packet.write_float_array, [128])
		self.assertEqual(new_packet.read_float_array(1), [128])

	def test_write_double_array(self):
		new_packet = Packet(self.opcode, self.encrypted, self.encrypted, [])
		self.does_not_raise(new_packet.write_double_array, [256.256])
		new_packet.lock()
		self.assertRaises(Exception, new_packet.write_double_array, [256.256])
		self.assertEqual(new_packet.read_double_array(1), [256.256])

	def test_write_ascii_array(self):
		new_packet = Packet(self.opcode, self.encrypted, self.encrypted, [])
		self.does_not_raise(new_packet.write_ascii_array, ["test"])
		new_packet.lock()
		self.assertRaises(Exception, new_packet.write_ascii_array, ["test"])
		self.assertEqual(new_packet.read_ascii_array(1), ["test"])

	def test_write_unicode_array(self):
		new_packet = Packet(self.opcode, self.encrypted, self.encrypted, [])
		self.does_not_raise(new_packet.write_unicode_array, ["test"])
		new_packet.lock()
		self.assertRaises(Exception, new_packet.write_unicode_array, ["test"])
		self.assertEqual(new_packet.read_unicode_array(1), ["test"])
