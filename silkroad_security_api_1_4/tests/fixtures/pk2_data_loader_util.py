import os
import re
from pathlib import Path
from typing import Dict, List

from lobot_api.Bot import Bot
from lobot_api.globals.PK2DataGlobal import PK2DataGlobal
from lobot_api.pk2.PK2Item import PK2Item, PK2ItemType
from lobot_api.pk2.PK2MagParam import PK2MagParam
from lobot_api.pk2.PK2NPC import PK2NPC
from lobot_api.pk2.PK2Skill import PK2Skill
from lobot_api.pk2.PK2TeleporterTypes import PK2Teleporter, PK2TeleporterData
from lobot_api.structs.CharData import Skill
from lobot_api.structs.Shop import Shop
from lobot_api.structs.ShopItemEntry import ShopItemEntry

"""Recreate the PK2 extraction process. Use to prepare for tests that require bot data."""


def get_data_folder_path() -> Path:
	return Path(os.path.dirname(os.path.realpath(__file__))) / "pk2_data"


def is_valid(s: List[str], args: int = 2, min_args: int = 2) -> bool:
	return len(s) >= min_args and s[0] and not s[0].isspace() and args >= min_args and not s[0].startswith('//')


def load_items_from_txt_file():
	path = get_data_folder_path() / "items.txt"

	try:
		if not os.path.isfile(path) or os.stat(path).st_size < 128:
			raise BufferError()

		items: Dict[int, PK2Item] = {}
		with open(path, newline=None, encoding='utf-16') as sr:
			for line in sr.readlines():
				item: List[str] = line.strip('\n').split('|')
				if not is_valid(item, len(item), 12) or int(item[0]) in items:
					continue
				items[int(item[0])] = PK2Item(item[0], item[1], item[2], item[3], item[4], item[5], item[6],
				                              item[7],
				                              item[8], item[9], item[10], item[11])
		PK2DataGlobal.items = items
	except Exception as e:
		print("\"data/items.txt\" not found. Attempting to extract PK2-Info.")
		raise e


def load_levels_from_txt_file():
	path = get_data_folder_path() / "levels.txt"

	try:
		if not path or os.stat(path).st_size < 128:
			raise BufferError()

		levels: Dict[int, int] = {}
		with open(path, newline=None, encoding='utf-16') as sr:
			for line in sr.readlines():
				level: List[str] = line.strip('\n').split('|')
				if not is_valid(level, len(level)) or int(level[0]) in levels:
					continue
				levels[int(level[0])] = int(level[1])
		PK2DataGlobal.level_data = levels
	except Exception as e:
		print("\"data/levels.txt\" not found. Attempting to extract PK2-Info.")
		raise e


def load_mag_params_from_txt_file():
	path = get_data_folder_path() / "magoptions.txt"

	try:
		if not path or os.stat(path).st_size < 128:
			raise BufferError()

		mag_params: Dict[int, PK2MagParam] = {}
		with open(path, newline=None, encoding='utf-16') as sr:
			for line in sr:
				mag_param: List[str] = line.strip('\n').split('|')
				if not is_valid(mag_param, len(mag_param), 5) or int(mag_param[0]) in mag_params:
					continue
				mag_params[int(mag_param[0])] = PK2MagParam.get_from_file_entry(
					mag_param[0],
					mag_param[1],
					mag_param[2],
					mag_param[3].split(','),
					mag_param[4].split(',')
				)
		PK2DataGlobal.mag_params = mag_params
	except Exception as e:
		print("\"data/magoptions.txt\" not found. Attempting to extract PK2-Info.")
		raise e


def load_names_from_txt_file():
	path = get_data_folder_path() / "names.txt"

	try:
		if not path or os.stat(path).st_size < 128:
			raise BufferError()

		names: Dict[str, str] = {}
		with open(path, newline=None, encoding='utf-16') as sr:
			for line in sr:
				name: List[str] = line.strip('\n').split('|')
				if not is_valid(name, len(name)) or name[0] in names:
					continue
				names[name[0]] = name[1]
		PK2DataGlobal.names = names
	except Exception as e:
		print("\"data/names.txt\" not found. Attempting to extract PK2-Info.")
		raise e


def load_npcs_from_txt_file():
	path = get_data_folder_path() / "npcs.txt"

	try:
		if not path or os.stat(path).st_size < 128:
			raise BufferError()

		npcs: Dict[int, PK2NPC] = {}
		with open(path, newline=None, encoding='utf-16') as sr:
			for line in sr.readlines():
				npc: List[str] = line.strip('\n').split('|')
				if not is_valid(npc, len(npc), 6) or int(npc[0]) in npcs:
					continue
				npcs[int(npc[0])] = PK2NPC(npc[0], npc[1], npc[2], npc[3], npc[4], npc[5])
		PK2DataGlobal.npcs = npcs
	except Exception as e:
		print("\"data/npcs.txt\" not found. Attempting to extract PK2-Info.")
		raise e


def load_shops_from_txt_file():
	path = get_data_folder_path() / "shops.txt"

	try:
		if not path or os.stat(path).st_size < 128:
			raise BufferError()

		shops: Dict[int, Shop] = {}
		with open(path) as sr:
			for line in sr.readlines():
				shop_str: List[str] = line.strip('\n').split('|')
				shop: List[int] = [int(x) for x in shop_str]
				if not is_valid(shop_str, len(shop_str), 4):
					continue
				result_shop = shops.get(shop[0], None)
				if not result_shop:
					shops[shop[0]] = Shop(shop[0])

				shops[shop[0]].item_entries[shop[1]] = ShopItemEntry(shop[1], shop[2], shop[3], shop[4])
		PK2DataGlobal.shops = shops
	except Exception as e:
		print("\"data/shops.txt\" not found. Attempting to extract PK2-Info.")
		raise e


def load_skills_from_txt_file():
	def name_filter():
		return re.compile(r"^SKILL_((PUNCH)_01|(CH|EU)_(\w+)_BASE_01)")

	def __try_add_autoattack_skills__(id_: str, skill: str):
		filter_skills = name_filter()
		match = filter_skills.match(skill)
		if match:
			s: Skill = Skill(int(id_), 1, True)
			types = []
			if match.group(2) == "PUNCH":
				types.append(PK2ItemType.Other)
			elif match.group(3) == "CH" and match.group(4) == "SWORD":
				types += [PK2ItemType.Blade, PK2ItemType.Sword]
			elif match.group(4) == "SPEAR":
				types += [PK2ItemType.Glaive, PK2ItemType.Spear]
			elif match.group(4) == "BOW":
				types.append(PK2ItemType.Bow)
			elif match.group(3) == "EU" and match.group(4) == "SWORD":
				types.append(PK2ItemType.SwordOnehander)
			elif match.group(4) == "TSWORD":
				types.append(PK2ItemType.SwordTwohander)
			elif match.group(4) == "AXE":
				types.append(PK2ItemType.Axe)
			elif match.group(4) == "CROSSBOW":
				types.append(PK2ItemType.Crossbow)
			elif match.group(4) == "DAGGER":
				types.append(PK2ItemType.Dagger)
			elif match.group(4) == "STAFF":
				types.append(PK2ItemType.MageStaff)
			elif match.group(4) == "WAND_WARLOCK":
				types.append(PK2ItemType.Darkstaff)
			elif match.group(4) == "HARP":
				types.append(PK2ItemType.Harp)
			elif match.group(4) == "WAND_CLERIC":
				types.append(PK2ItemType.ClericStaff)

			if types:
				for t in types:
					Bot.auto_attack_skills[t] = s
				Bot.auto_attack_skills_ids[s.ref_id] = s

	def __add_autoattack_skills_to_skill_list__():
		for k, v in Bot.auto_attack_skills.items():
			if v not in Bot.char_data.skills:
				Bot.char_data.skills.append(v)

	path = get_data_folder_path() / "skills.txt"

	try:
		if not path or os.stat(path).st_size < 128:
			raise BufferError()

		skills: Dict[int, PK2Skill] = {}
		with open(path, newline=None, encoding='utf-16') as sr:
			for line in sr:
				skill: List[str] = line.strip('\n').split('|')
				if not is_valid(skill, len(skill), 9) or int(skill[0]) in skills:
					continue
				skills[int(skill[0])] = PK2Skill(skill[0], skill[1], skill[2], skill[3], skill[4], skill[5],
				                                 skill[6],
				                                 skill[7], skill[8])
				__try_add_autoattack_skills__(skill[0], skill[1])
		PK2DataGlobal.skills = skills
		__add_autoattack_skills_to_skill_list__()
	except Exception as e:
		print("\"data/skillData.txt\" not found. Attempting to extract PK2-Info.")
		raise e


def load_teleporters_from_txt_file():
	def __loadteleporter_link__(teleporterLinkInfo: List[str]) -> List[int]:
		teleporter_link: List[int] = []
		if len(teleporterLinkInfo) < 1:
			return []
		else:
			for s in teleporterLinkInfo:
				teleporter_link.append(int(s))
			return teleporter_link

	def __loadteleporter_data__(teleporter_data_info: List[str]) -> PK2TeleporterData:
		# Add possible teleporterLinks
		return PK2TeleporterData(teleporter_data_info[0],
		                         teleporter_data_info[1],
		                         teleporter_data_info[2],
		                         __loadteleporter_link__(teleporter_data_info[3].split('|'))) \
			if (len(teleporter_data_info) == 4) \
			else PK2TeleporterData("", "", "", [])

	def __load_teleporter__(teleporter_info: List[str]) -> PK2Teleporter:
		if len(teleporter_info) == 4:
			return PK2Teleporter(teleporter_info[0],
			                     teleporter_info[1],
			                     teleporter_info[2],
			                     __loadteleporter_data__(teleporter_info[3].split('|', 3)))
		elif len(teleporter_info) == 3:
			return PK2Teleporter(teleporter_info[0], teleporter_info[1], teleporter_info[2],
			                     PK2TeleporterData("", "", "", []))
		else:
			return PK2Teleporter()

	path = get_data_folder_path() / "teleporters.txt"

	try:
		if not path or os.stat(path).st_size < 128:
			raise BufferError()

		teleporters: Dict[int, PK2Teleporter] = {}
		with open(path, encoding='utf-16') as sr:
			for line in sr.readlines():
				portal: List[str] = line.strip('\n').split('|', 3)

				if not is_valid(portal, len(portal), 3) or int(portal[0]) in teleporters:
					continue

				teleporter: PK2Teleporter = __load_teleporter__(portal)

				if teleporter.ID == 0:
					continue
				teleporters[teleporter.ID] = teleporter
			PK2DataGlobal.teleporters = teleporters
	except Exception as e:
		print("\"data/teleporters.txt\" not found. Attempting to extract PK2-Info.")
		raise e


def load_all_data():
	if not PK2DataGlobal.names:
		print("Loading Names")
		load_names_from_txt_file()

	if not PK2DataGlobal.level_data:
		print("Loading Levels")
		load_levels_from_txt_file()

	if not PK2DataGlobal.mag_params:
		print("Loading Blues")
		load_mag_params_from_txt_file()

	if not PK2DataGlobal.skills:
		print("Loading Skills")
		load_skills_from_txt_file()

	if not PK2DataGlobal.shops:
		print("Loading Shops")
		load_items_from_txt_file()

	if not PK2DataGlobal.npcs:
		print("Loading General NPCs")
		load_npcs_from_txt_file()

	if not PK2DataGlobal.teleporters:
		print("Loading Teleporter NPCs")
		load_teleporters_from_txt_file()

	if not PK2DataGlobal.shops:
		print("Loading Shop NPCs")
		load_shops_from_txt_file()


if __name__ == '__main__':
	load_all_data()
