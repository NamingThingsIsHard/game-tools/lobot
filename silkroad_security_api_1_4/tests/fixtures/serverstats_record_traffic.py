#!/usr/bin/env python3
import socket
import errno
import struct
import sys
from time import sleep

from silkroad_security_api_1_4.BinaryReader import BinaryReader
from silkroad_security_api_1_4.BinaryWriter import BinaryWriter
from silkroad_security_api_1_4.Packet import Packet
from silkroad_security_api_1_4.Security import Security
from silkroad_security_api_1_4.tests.fixtures import network_traffic_util


def handle_packet(security, packet):
	r = BinaryReader(packet['data'])

	if packet['opcode'] == 0x2001:

		server = r.read_ascii(r.read_uint16())

		if server == 'GatewayServer':
			w = BinaryWriter()
			w.write_uint8(18)
			w.write_uint16(9)
			w.write_ascii('SR_Client')
			w.write_uint32(432)
			security.Send(0x6100, w.tolist(), True)

	elif packet['opcode'] == 0xA100:

		security.Send(0x6101, [], True)

	elif packet['opcode'] == 0xA101:

		entry = r.read_uint8()
		while entry == 1:
			r.read_uint8()
			print(r.read_ascii(r.read_uint16()))
			entry = r.read_uint8()

		print('')

		entry = r.read_uint8()
		while entry == 1:
			server_id = r.read_uint16()

			name = r.read_ascii(r.read_uint16())
			capacity = r.read_float()
			state = r.read_uint8()

			print('[%s] %f' % (name, capacity * 100))

			entry = r.read_uint8()


def main():
	security = Security()

	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	s.connect(('gwgt1.joymax.com', 15779))
	s.setblocking(False)

	try:
		while 1:
			try:
				data = s.recv(8192)

				if data is None:
					break

				# Packet received
				security.recv(data)

				# See if there are packets that can be processed
				packet = security.get_packets_to_recv()
				if packet is not None:
					# Iterate each returned packet
					for p in packet:
						print(
							f"s->p:\t{format(p['opcode'], '#04x')},encrypted={p['encrypted']},massive={p['massive']},buffer={p['data']}")
						network_traffic_util.log_from_server(Packet(p['opcode'], p['encrypted'], p['massive'], p['data']))
						# Process the packet
						handle_packet(security, p)

				# See if there are packets to be sent
				packet = security.get_packets_to_send()

				# Send each packet in the list
				if packet is not None:
					for p in packet:
						opcode = struct.unpack('H', bytes(p[2:4]))[0]
						encrypted = struct.unpack('B', bytes(p[1:2]))[0] == 0x80
						print(f"p->s:\t{format(opcode, '#04x')},{','.join([format(b, '02X') for b in p])}")
						network_traffic_util.log_proxy_to_server(Packet(opcode, encrypted, False, p[6:]))

						data = bytes(p)
						while data:
							sent = s.send(data)
							data = data[sent:]
			except socket.error as e:
				if e.errno == errno.EWOULDBLOCK:
					sleep(0.01)
				else:
					raise e
	except KeyboardInterrupt:
		''''''

	# Cleanup
	s.shutdown(socket.SHUT_RDWR)
	s.close()

	network_traffic_util.save_log("clientless_serverstats")
	return 0


if __name__ == '__main__':
	sys.exit(main())
