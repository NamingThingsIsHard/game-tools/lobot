import json
import os
import sys
from datetime import datetime
from pathlib import Path
from typing import Dict

from argparse import ArgumentParser
from lobot_api.globals.DirectoryGlobal import DirectoryGlobal
from silkroad_security_api_1_4.Packet import Packet

""" Network Traffic Util

A small utility to dump traffic between 
client <-> proxy <-> server

Examples:
proxy_record_traffic.py
serverstats_record_traffic.py

Each packet is logged with a number and added to one of four dictionaries for its direction.

The main functionality includes:

	* get_full_log() - return all logged Packets in one dict
	* save_log() - dump logged traffic as json into directory of get_log_file_path(name) with custom name choice
	* load_file(file_path) - Load previously recorded from a file 
"""
packet_index = 0

proxy_to_client = {}
client_to_server = {}
proxy_to_server = {}
server_to_proxy = {}

time = datetime.now()
time_string = f"{time.year}-{time.month}-{time.day}_{time.hour}-{time.minute}.json"
default_file_name = "connection_log-"
default_folder = DirectoryGlobal.cwd() / "test/silkroad_security_api_1_4/fixtures/log"


def get_log_file_name(file_name=default_file_name) -> Path:
	return default_folder / "test/silkroad_security_api_1_4/fixtures/log" / f"{file_name}-{time_string}"


def add_to_log(log: Dict, item: str):
	global packet_index

	log[packet_index] = item
	packet_index += 1


def log_proxy_to_client(p: Packet):
	add_to_log(proxy_to_client, str(p))


def log_client_to_server(p: Packet):
	add_to_log(client_to_server, str(p))


def log_proxy_to_server(p: Packet):
	add_to_log(proxy_to_server, str(p))


def log_from_server(p: Packet):
	add_to_log(server_to_proxy, str(p))


def get_full_log() -> Dict:
	return dict(
		sorted(
			{
				**proxy_to_client,
				**client_to_server,
				**proxy_to_server,
				**server_to_proxy
			}.items()
		)
	)


def save_log(file_name=default_file_name):
	obj = {
		"proxy_to_client": {
			"abbreviation": "p->c",
			"packets": proxy_to_client
		},
		"client_to_server": {
			"abbreviation": "c->p->s",
			"packets": client_to_server
		},
		"proxy_to_server": {
			"abbreviation": "p->s",
			"packets": proxy_to_server
		},
		"server_to_proxy": {
			"abbreviation": "s->p",
			"packets": server_to_proxy
		}
	}

	# Make sure path exists to create_request logfile in
	if not os.path.exists(default_folder):
		os.makedirs(default_folder, exist_ok=True)

		with open(get_log_file_name(file_name), "w+") as fp:
			json.dump(obj, fp, indent='\t')


def load_file(file_path) -> Dict:
	with open(file_path, "r") as fp:
		json_string = json.load(fp)

		parsed_with_classes = {}

		for k, v in json_string.items():
			parsed_with_classes[k] = v
			packets = v["packets"]

			parsed = {}
			for index, p in packets.items():
				p_args = p.split(',', 4)
				evaluated = eval(f"Packet({','.join(p_args)})")
				parsed[int(index)] = evaluated

			parsed_with_classes[k]["packets"] = parsed

	global proxy_to_client, client_to_server, proxy_to_server, server_to_proxy

	proxy_to_server = parsed_with_classes["proxy_to_server"]["packets"]
	proxy_to_client = parsed_with_classes["proxy_to_client"]["packets"]
	client_to_server = parsed_with_classes["client_to_server"]["packets"]
	server_to_proxy = parsed_with_classes["server_to_proxy"]["packets"]

	global packet_index
	packet_index = sum([len(proxy_to_server), len(proxy_to_client), len(client_to_server), len(server_to_proxy)]) - 1
	return parsed_with_classes


if __name__ == "__main__":
	parser = ArgumentParser()
	parser.add_argument(
		"file",
		type=load_file,
		help="file to load. Contains packet output as json"
	)
	try:
		args = parser.parse_args()
	except SystemExit as err:
		if err.code == 2:
			parser.print_help()
		sys.exit(0)
