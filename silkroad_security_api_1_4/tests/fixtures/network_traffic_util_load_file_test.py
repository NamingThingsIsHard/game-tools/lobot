import os
from pathlib import Path

from silkroad_security_api_1_4.tests.fixtures import network_traffic_util

"""network_traffic_util_load_file Test

This file serves as a sanity check for JSON files created using network_traffic_util_load_file.py.
"""

file_path = Path(os.path.dirname(os.path.realpath(__file__))) / "log" / "proxy_client-2021-11-30_15-49.json"

if __name__ == "__main__":
	dicts = network_traffic_util.load_file(file_path)
	ps = dicts["proxy_to_server"]["packets"]
	pc = dicts["proxy_to_client"]["packets"]
	cs = dicts["client_to_server"]["packets"]
	sp = dicts["server_to_proxy"]["packets"]

	# print({**ps, **pc, **cs, **sp})
	list_print_view = [f"{k}:{v}" for k, v in network_traffic_util.get_full_log().items()]
	print(*list_print_view, sep='\n')
