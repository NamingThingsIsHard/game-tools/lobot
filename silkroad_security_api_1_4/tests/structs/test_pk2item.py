import unittest

from lobot_api.pk2.PK2Item import PK2Item, PK2ItemType
from lobot_api.structs.EquipmentSlot import EquipmentSlot


class TestPK2Item(unittest.TestCase):
	type_ = PK2ItemType.GarmentBody
	item = PK2Item(*"1|ITEM_ETC_GOLD_01|Gold|0|3500|3|0|1|0|0|2|1".split('|'))

	def test_get_attack_range(self):
		for t in PK2ItemType._member_map_.values():
			self.assertGreater(t.get_attack_range(), 0)

	def test_get_required_item(self):
		self.assertEqual(PK2ItemType.ShieldEU.get_required_weapon_slot(), EquipmentSlot.Secondary.value)
		self.assertEqual(PK2ItemType.ShieldCH.get_required_weapon_slot(), EquipmentSlot.Secondary.value)
		self.assertEqual(PK2ItemType.Sword.get_required_weapon_slot(), EquipmentSlot.Primary.value)

	def test_is_weapon_with_shield(self):
		trues = [
			PK2ItemType.Blade,
			PK2ItemType.Sword,
			PK2ItemType.SwordOnehander,
			PK2ItemType.Darkstaff,
			PK2ItemType.ClericStaff
		]
		false = PK2ItemType.Glaive

		with self.subTest("with shield"):
			for t in trues:
				self.assertTrue(t.is_weapon_with_shield())

		with self.subTest("without shield"):
			self.assertFalse(false.is_weapon_with_shield())

	def test_is_2_slot_weapon(self):
		trues = [
			PK2ItemType.Glaive,
			PK2ItemType.Spear,
			PK2ItemType.SwordTwohander,
			PK2ItemType.Axe,
			PK2ItemType.MageStaff,
			PK2ItemType.Harp
		]
		false = PK2ItemType.ClericStaff

		with self.subTest("2 slot"):
			for t in trues:
				self.assertTrue(t.is_2_slot_weapon())

		with self.subTest("1 slot"):
			self.assertFalse(false.is_2_slot_weapon())

	def test_is_jade_tablet(self):
		self.assertTrue(PK2ItemType.JadeTabletAir.is_jade_tablet())
		self.assertFalse(PK2ItemType.RubyTabletLife.is_jade_tablet())

	def test_is_ruby_tablet(self):
		self.assertTrue(PK2ItemType.RubyTabletLife.is_ruby_tablet())
		self.assertFalse(PK2ItemType.JadeTabletAir.is_ruby_tablet())

	def test_repr(self):
		self.assertTrue(self.item.__repr__())

	def test_to_type(self):
		self.assertEqual(self.item.to_type(), self.item)

	def test_is_consumable(self):
		self.assertTrue(self.item.is_consumable())

	def test_is_equipment_item(self):
		self.assertFalse(self.item.is_equipment_item())

	def test_is_weapon(self):
		self.assertFalse(self.item.is_weapon())

	def test_is_shield(self):
		self.assertFalse(self.item.is_shield())

	def test_is_clothing(self):
		self.assertFalse(self.item.is_clothing())

	def test_is_accessory(self):
		self.assertFalse(self.item.is_accessory())

	def test_is_durability_item(self):
		self.assertFalse(self.item.is_durability_item())

	def test_get_elixir_type(self):
		self.assertEqual(self.item.get_elixir_type("ITEM_ETC_ARCHEMY_REINFORCE_RECIPE_WEAPON"), PK2ItemType.WeaponElixir)
		self.assertEqual(self.item.get_elixir_type("ITEM_ETC_ARCHEMY_REINFORCE_RECIPE_SHIELD"), PK2ItemType.ShieldElixir)
		self.assertEqual(self.item.get_elixir_type("ITEM_ETC_ARCHEMY_REINFORCE_RECIPE_ARMOR"), PK2ItemType.ProtectorElixir)
		self.assertEqual(self.item.get_elixir_type("ITEM_ETC_ARCHEMY_REINFORCE_RECIPE_ACCESSARY"), PK2ItemType.AccessoryElixir)
		self.assertEqual(self.item.get_elixir_type(""), PK2ItemType.Elixir)

	def test_get_magic_stone_type(self):
		types_ = [
			"ITEM_ETC_ARCHEMY_MAGICSTONE_STR",
			"ITEM_ETC_ARCHEMY_MAGICSTONE_INT",
			"ITEM_ETC_ARCHEMY_MAGICSTONE_DUR",
			"ITEM_ETC_ARCHEMY_MAGICSTONE_HR",
			"ITEM_ETC_ARCHEMY_MAGICSTONE_EVADE_BLOCK",
			"ITEM_ETC_ARCHEMY_MAGICSTONE_EVADE_CRITICAL",
			"ITEM_ETC_ARCHEMY_MAGICSTONE_ER",
			"ITEM_ETC_ARCHEMY_MAGICSTONE_HP",
			"ITEM_ETC_ARCHEMY_MAGICSTONE_MP",
			"ITEM_ETC_ARCHEMY_MAGICSTONE_FROSTBITE",
			"ITEM_ETC_ARCHEMY_MAGICSTONE_ESHOCK",
			"ITEM_ETC_ARCHEMY_MAGICSTONE_BURN",
			"ITEM_ETC_ARCHEMY_MAGICSTONE_POISON",
			"ITEM_ETC_ARCHEMY_MAGICSTONE_ZOMBIE",
			"ITEM_ETC_ARCHEMY_MAGICSTONE_SOLID",
			"ITEM_ETC_ARCHEMY_MAGICSTONE_LUCK",
			"ITEM_ETC_ARCHEMY_MAGICSTONE_ASTRAL",
			"ITEM_ETC_ARCHEMY_MAGICSTONE_ATHANASIA"
		]
		try:
			for t in types_:
				self.item.get_magic_stone_type(t)
			self.item.get_tablet_type("")
		except Exception as e:
			self.fail(e)

	def test_get_attribute_stone_type(self):
		types_ = [
			"ITEM_ETC_ARCHEMY_ATTRSTONE_PA_",
			"ITEM_ETC_ARCHEMY_ATTRSTONE_PASTR",
			"ITEM_ETC_ARCHEMY_ATTRSTONE_MA_",
			"ITEM_ETC_ARCHEMY_ATTRSTONE_MAINT",
			"ITEM_ETC_ARCHEMY_ATTRSTONE_CRITICAL",
			"ITEM_ETC_ARCHEMY_ATTRSTONE_HR",
			"ITEM_ETC_ARCHEMY_ATTRSTONE_PD_",
			"ITEM_ETC_ARCHEMY_ATTRSTONE_PDSTR",
			"ITEM_ETC_ARCHEMY_ATTRSTONE_MD_",
			"ITEM_ETC_ARCHEMY_ATTRSTONE_MDINT",
			"ITEM_ETC_ARCHEMY_ATTRSTONE_ER",
			"ITEM_ETC_ARCHEMY_ATTRSTONE_BR",
			"ITEM_ETC_ARCHEMY_ATTRSTONE_PAR",
			"ITEM_ETC_ARCHEMY_ATTRSTONE_MAR"
		]
		try:
			for t in types_:
				self.item.get_attribute_stone_type(t)
			self.item.get_tablet_type("")
		except Exception as e:
			self.fail(e)

	def test_get_tablet_type(self):
		types_ = [
			"ITEM_ETC_ARCHEMY_MAGICTABLET_STR",
			"ITEM_ETC_ARCHEMY_MAGICTABLET_INT",
			"ITEM_ETC_ARCHEMY_MAGICTABLET_DUR",
			"ITEM_ETC_ARCHEMY_MAGICTABLET_HR",
			"ITEM_ETC_ARCHEMY_MAGICTABLET_EVADE_BLOCK",
			"ITEM_ETC_ARCHEMY_MAGICTABLET_EVADE_CRITICAL",
			"ITEM_ETC_ARCHEMY_MAGICTABLET_ER",
			"ITEM_ETC_ARCHEMY_MAGICTABLET_HP",
			"ITEM_ETC_ARCHEMY_MAGICTABLET_MP",
			"ITEM_ETC_ARCHEMY_MAGICTABLET_FROSTBITE",
			"ITEM_ETC_ARCHEMY_MAGICTABLET_ESHOCK",
			"ITEM_ETC_ARCHEMY_MAGICTABLET_BURN",
			"ITEM_ETC_ARCHEMY_MAGICTABLET_POISON",
			"ITEM_ETC_ARCHEMY_MAGICTABLET_ZOMBIE",
			"item_etc_archemy_magictablet_sol_id",
			"ITEM_ETC_ARCHEMY_MAGICTABLET_LUCK",
			"ITEM_ETC_ARCHEMY_ATTRTABLET_PA_",
			"ITEM_ETC_ARCHEMY_ATTRTABLET_PASTR",
			"ITEM_ETC_ARCHEMY_ATTRTABLET_MA_",
			"ITEM_ETC_ARCHEMY_ATTRTABLET_MAINT",
			"ITEM_ETC_ARCHEMY_ATTRTABLET_CRITICAL",
			"ITEM_ETC_ARCHEMY_ATTRTABLET_HR",
			"ITEM_ETC_ARCHEMY_ATTRTABLET_PD_",
			"ITEM_ETC_ARCHEMY_ATTRTABLET_PDSTR",
			"ITEM_ETC_ARCHEMY_ATTRTABLET_MD_",
			"ITEM_ETC_ARCHEMY_ATTRTABLET_MDINT",
			"ITEM_ETC_ARCHEMY_ATTRTABLET_ER",
			"ITEM_ETC_ARCHEMY_ATTRTABLET_BR",
			"ITEM_ETC_ARCHEMY_ATTRTABLET_PAR",
			"ITEM_ETC_ARCHEMY_ATTRTABLET_MAR"
		]
		try:
			for t in types_:
				self.item.get_tablet_type(t)
			self.item.get_tablet_type("")
		except Exception as e:
			self.fail(e)

	def test_disambiguate_type(self):
		types_ = [
			PK2ItemType.HPPot.value,
			PK2ItemType.MPPot.value,
			PK2ItemType.VigorPot.value,
			PK2ItemType.BuffScroll.value,
			PK2ItemType.Elixir.value,
			PK2ItemType.MagicStone.value,
			PK2ItemType.AttributeStone.value,
			PK2ItemType.AlchemyTablet.value,
			PK2ItemType.MagicStoneMall.value
		]
		long_id = "ITEM_ETC_GOLD_01"
		try:
			for t in types_:
				self.item.disambiguate_type(t, long_id)
			self.item.get_tablet_type("")
		except Exception as e:
			self.fail(e)

	def test_should_pick(self):
		try:
			self.item.should_pick()
		except Exception as e:
			self.fail(e)

	def test_should_store(self):
		try:
			self.item.should_store()
		except Exception as e:
			self.fail(e)

	def test_should_sell(self):
		try:
			self.item.should_sell()
		except Exception as e:
			self.fail(e)

	def test_should_drop(self):
		try:
			self.item.should_drop()
		except Exception as e:
			self.fail(e)

	def test_get_item_type(self):
		with self.subTest("get type gold"):
			self.assertEqual(self.item.get_item_type("3500", self.item.long_id), PK2ItemType.Gold)
		with self.subTest("get type other"):
			self.assertEqual(self.item.get_item_type("0", self.item.long_id), PK2ItemType.Other)

	def test_get_type_filter(self):
		try:
			self.item.get_type_filter()
		except Exception as e:
			self.fail(e)


if __name__ == '__main__':
	unittest.main()
