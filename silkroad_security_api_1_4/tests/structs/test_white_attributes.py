import unittest

from lobot_api.structs.WhiteAttributes import WhiteAttributes, AttributeType


class TestWhiteAttributes(unittest.TestCase):
	weapon_value = 8725251463  # 87 C5 10 08 02 00 00 00
	equipment_value = 370325281  # C9 18 13 16 00 00 00 00
	shield_value = 35687220  # 34 8B 20 02 00 00 00 00
	accessory_value = 370325281  # C9 18 13 16 00 00 00 00
	white = WhiteAttributes(AttributeType.Weapon)
	stats = [0, 1, 2, 3, 4, 5, 6]

	def test_init_stats(self):
		for type_str, type_ in AttributeType._member_map_.items():
			WhiteAttributes(type_, 0, self.stats)

	def test_str(self):
		for type_str, type_ in AttributeType._member_map_.items():
			self.assertNotEqual(str(WhiteAttributes(type_)), "")
			self.assertNotEqual(WhiteAttributes(type_).__repr__(), "")
		self.assertEqual(str(WhiteAttributes(None)), "")
		self.assertEqual(WhiteAttributes(None).__repr__(), "")

	def test_construct_with_value(self):
		with self.subTest("weapon"):
			WhiteAttributes(AttributeType.Weapon, self.weapon_value)
		with self.subTest("equipment"):
			WhiteAttributes(AttributeType.Equipment, self.equipment_value)
		with self.subTest("shield"):
			WhiteAttributes(AttributeType.Shield, self.shield_value)
		with self.subTest("accessory"):
			WhiteAttributes(AttributeType.Accessory, self.accessory_value)

	def test_update_value(self):
		self.white.type = AttributeType.Equipment
		self.white.update_value()
		self.white.type = AttributeType.Shield
		self.white.update_value()
		self.white.type = AttributeType.Accessory
		self.white.update_value()

	def test_value(self):
		self.white.value = 22
		self.assertGreater(self.white.value, 0)

	def test_type(self):
		self.white.type = AttributeType.Accessory
		self.assertEqual(self.white.type, AttributeType.Accessory)

	def test_durability(self):
		self.white.durability = 22
		self.assertGreater(self.white.durability, 0)

		with self.assertRaises(ValueError):
			self.white.durability = 101

	def test_durability_percentage(self):
		self.white.durability_percentage = 22
		self.assertGreater(self.white.durability_percentage, 0)

		with self.assertRaises(ValueError):
			self.white.durability_percentage = 101

	def test_hit_ratio(self):

		self.white.hit_ratio = 22
		self.assertGreater(self.white.hit_ratio, 0)

		with self.assertRaises(ValueError):
			self.white.hit_ratio = 101

	def test_hit_ratio_percentage(self):

		self.white.hit_ratio_percentage = 22
		self.assertGreater(self.white.hit_ratio_percentage, 0)

		with self.assertRaises(ValueError):
			self.white.hit_ratio_percentage = 101

	def test_parry_ratio(self):

		self.white.parry_ratio = 22
		self.assertGreater(self.white.parry_ratio, 0)

		with self.assertRaises(ValueError):
			self.white.parry_ratio = 101

	def test_parry_ratio_percentage(self):

		self.white.parry_ratio_percentage = 22
		self.assertGreater(self.white.parry_ratio_percentage, 0)

		with self.assertRaises(ValueError):
			self.white.parry_ratio_percentage = 101

	def test_block_ratio(self):

		self.white.block_ratio = 22
		self.assertGreater(self.white.block_ratio, 0)

		with self.assertRaises(ValueError):
			self.white.block_ratio = 101

	def test_block_ratio_percentage(self):

		self.white.block_ratio_percentage = 22
		self.assertGreater(self.white.block_ratio_percentage, 0)

		with self.assertRaises(ValueError):
			self.white.block_ratio_percentage = 101

	def test_critical_ratio(self):

		self.white.critical_ratio = 22
		self.assertGreater(self.white.critical_ratio, 0)

		with self.assertRaises(ValueError):
			self.white.critical_ratio = 101

	def test_critical_ratio_percentage(self):

		self.white.critical_ratio_percentage = 22
		self.assertGreater(self.white.critical_ratio_percentage, 0)

		with self.assertRaises(ValueError):
			self.white.critical_ratio_percentage = 101

	def test_phy_reinforce(self):

		self.white.phy_reinforce = 22
		self.assertGreaterEqual(self.white.phy_reinforce, 0)

		with self.assertRaises(ValueError):
			self.white.phy_reinforce = 101

	def test_phy_reinforce_percentage(self):

		self.white.phy_reinforce_percentage = 22
		self.assertGreater(self.white.phy_reinforce_percentage, 0)

		with self.assertRaises(ValueError):
			self.white.phy_reinforce_percentage = 101

	def test_mag_reinforce(self):

		self.white.mag_reinforce = 22
		self.assertGreaterEqual(self.white.mag_reinforce, 0)

		with self.assertRaises(ValueError):
			self.white.mag_reinforce = 101

	def test_mag_reinforce_percentage(self):

		self.white.mag_reinforce_percentage = 22
		self.assertGreater(self.white.mag_reinforce_percentage, 0)

		with self.assertRaises(ValueError):
			self.white.mag_reinforce_percentage = 101

	def test_phy_attack(self):

		self.white.phy_attack = 22
		self.assertGreater(self.white.phy_attack, 0)

		with self.assertRaises(ValueError):
			self.white.phy_attack = 101

	def test_phy_attack_percentage(self):

		self.white.phy_attack_percentage = 22
		self.assertGreater(self.white.phy_attack_percentage, 0)

		with self.assertRaises(ValueError):
			self.white.phy_attack_percentage = 101

	def test_mag_attack(self):

		self.white.mag_attack = 22
		self.assertGreaterEqual(self.white.mag_attack, 0)

		with self.assertRaises(ValueError):
			self.white.mag_attack = 101

	def test_mag_attack_percentage(self):

		self.white.mag_attack_percentage = 22
		self.assertGreaterEqual(self.white.mag_attack_percentage, 0)

		with self.assertRaises(ValueError):
			self.white.mag_attack_percentage = 101

	def test_phy_defense(self):

		self.white.phy_defense = 22
		self.assertGreater(self.white.phy_defense, 0)

		with self.assertRaises(ValueError):
			self.white.phy_defense = 101

	def test_phy_defense_percentage(self):

		self.white.phy_defense_percentage = 22
		self.assertGreater(self.white.phy_defense_percentage, 0)

		with self.assertRaises(ValueError):
			self.white.phy_defense_percentage = 101

	def test_mag_defense(self):

		self.white.mag_defense = 22
		self.assertGreater(self.white.mag_defense, 0)

		with self.assertRaises(ValueError):
			self.white.mag_defense = 101

	def test_mag_defense_percentage(self):

		self.white.mag_defense_percentage = 22
		self.assertGreater(self.white.mag_defense_percentage, 0)

		with self.assertRaises(ValueError):
			self.white.mag_defense_percentage = 101

	def test_phy_absorb(self):

		self.white.phy_absorb = 22
		self.assertGreater(self.white.phy_absorb, 0)

		with self.assertRaises(ValueError):
			self.white.phy_absorb = 101

	def test_phy_absorb_percentage(self):

		self.white.phy_absorb_percentage = 22
		self.assertGreater(self.white.phy_absorb_percentage, 0)

		with self.assertRaises(ValueError):
			self.white.phy_absorb_percentage = 101

	def test_mag_absorb(self):

		self.white.mag_absorb = 22
		self.assertGreater(self.white.mag_absorb, 0)

		with self.assertRaises(ValueError):
			self.white.mag_absorb = 101

	def test_mag_absorb_percentage(self):

		self.white.mag_absorb_percentage = 22
		self.assertGreater(self.white.mag_absorb_percentage, 0)

		with self.assertRaises(ValueError):
			self.white.mag_absorb_percentage = 101


if __name__ == '__main__':
	unittest.main()
