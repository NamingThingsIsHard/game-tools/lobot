﻿import threading
from ctypes import *

from typing import List


class TransferBuffer:

	def __init__(self, buffer: List[c_byte] = None, assign: bool = False, buffer_size: int = 0, offset: int = 0,
	             size: int = 0):

		if assign and buffer:
			self.m_buffer = buffer
		elif buffer:
			self.m_buffer[:] = buffer
		else:
			self.m_buffer = [0x00] * buffer_size

		self.m_offset = offset
		self.m_size = size
		self.m_lock = threading.Lock()

	@property
	def buffer(self) -> List[c_byte]:
		return self.m_buffer

	@buffer.setter
	def buffer(self, value):
		with self.m_lock:
			self.m_buffer = value

	@property
	def offset(self) -> int:
		return self.m_offset

	@offset.setter
	def offset(self, value):
		with self.m_lock:
			self.m_offset = value

	@property
	def size(self) -> int:
		return self.m_size

	@size.setter
	def size(self, value):
		with self.m_lock:
			self.m_size = value

	def __repr__(self):
		return f"size({self.m_size})offset({self.m_offset}) - {self.m_buffer if self.m_buffer else '[]'}"

	def __str__(self):
		return f"size({self.m_size})offset({self.m_offset}) - {self.m_buffer if self.m_buffer else '[]'}"

# def __init__(self, TransferBuffer rhs):
# {
#    lock (rhs.m_lock)
#    {
#        m_buffer = new c_byte[len(rhs.m_buffer)]
#        System.Buffer.block_copy(rhs.m_buffer, 0, m_buffer, 0, len(m_buffer))
#        m_offset = rhs.m_offset
#        m_size = rhs.m_size
#        m_lock = object()
#    }
# }
