#!/usr/bin/env python3
import array
import struct

from silkroad_security_api_1_4.SeekType import SeekType


class BinaryWriter:
	data: array.array = array.array('B')
	index = 0
	size = 0

	def __init__(self, data=None):
		if not data:
			data = []
		self.reset(data)

	def __repr__(self):
		return f"size({self.size})index({self.index}){self.data}"

	def __str__(self):
		return f"size({self.size})index({self.index}){self.data}"

	def flush(self):
		"""
		Write remaining bytes.
		:return: None
		"""
		if self.size > self.index:
			pass
		else:
			buffer = self.data[self.index:self.size]
			if buffer:
				self.write(buffer)

	def reset(self, data=None):
		if type(data) == array.array:
			self.data = data
		elif type(data) == list:
			self.data = array.array('B', data)
		elif not data:
			self.data = array.array('B')
		else:
			raise Exception('incorrect data type was used to reset the class')

		self.size = self.data.__len__()
		self.seek_end()

	def tostring(self):
		return str(self.data)

	def tolist(self):
		return self.data.tolist()

	def tofile(self, f):
		return self.data.tofile(f)

	def toarray(self):
		return self.data

	def seek(self, offset, origin: SeekType = SeekType.Begin):
		if origin == SeekType.Begin:
			self.seek_set(offset)
		elif origin == SeekType.Current:
			self.seek_forward(offset)
		else:
			self.seek_backward(offset)
		return self.index

	def seek_forward(self, count):
		if self.index + count > self.size:
			raise Exception('index would be past end of stream')
		self.index += count

	def seek_backward(self, count):
		if self.index - count < 0:
			raise Exception('index would be < 0 if seeked further back')
		self.index -= count

	def seek_set(self, index):
		if index > self.size or index < 0:
			raise Exception('invalid index')
		self.index = index

	def seek_end(self):
		self.index = self.size

	def write_int8(self, val):
		self.__append(struct.pack('b', val))

	def write_uint8(self, val):
		self.__append(struct.pack('B', val))

	def write_int16(self, val):
		self.__append(struct.pack('h', val))

	def write_uint16(self, val):
		self.__append(struct.pack('H', val))

	def write_int32(self, val):
		self.__append(struct.pack('i', val))

	def write_uint32(self, val):
		self.__append(struct.pack('I', val))

	def write_int64(self, val):
		self.__append(struct.pack('q', val))

	def write_uint64(self, val):
		self.__append(struct.pack('Q', val))

	def write_float(self, val):
		self.__append(struct.pack('f', val))

	def write_double(self, val):
		self.__append(struct.pack('d', val))

	def write_char(self, val):
		self.__append(struct.pack('c', val))

	def write_ascii(self, val):
		self.__append(bytes(val, 'ascii', 'replace'))

	def write_unicode(self, val):
		self.write_utf16(val)

	def write_utf8(self, val):
		self.__append(bytes(val, 'utf-8'))

	def write_utf16(self, val):
		self.__append(val.encode('utf-16le'))

	def write_utf32(self, val):
		self.__append(val.encode('utf-32le'))

	def write(self, val):
		self.__append(val)

	def write_offset(self, val, offset, count):
		"""
		Writes a region of a byte array to the current stream.
		:param val: A byte array containing the data to write.
		:param offset: The index of the first byte to read from val and to write to the stream.
		:param count: The number of bytes to read from buffer and to write to the stream.
		:return: None
		"""
		length = len(val)
		if 0 <= offset < length and not offset + count > length:
			self.data.fromlist(val[offset:offset + count])

	# TODO: optimize to extend() or use +
	def __append(self, packed):
		for x in packed:
			self.data.insert(self.index, x)
			self.index += 1
			self.size += 1
