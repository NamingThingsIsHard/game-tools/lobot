from enum import Enum


class SeekType(Enum):
	"""Specifies the position in a stream to use for seeking."""
	Begin = 0
	Current = 1
	End = 2
