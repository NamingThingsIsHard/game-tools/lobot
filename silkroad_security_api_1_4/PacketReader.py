﻿from ctypes import *
from typing import List

from silkroad_security_api_1_4.BinaryReader import BinaryReader


class PacketReader(BinaryReader):

	def __init__(self, input_: List[c_byte], offset: int = 0):  # , count: int = 0):
		# super(PacketReader, self).__init__(input, offset, count, False)
		super(PacketReader, self).__init__(input_, offset)
		self.m_input: List[c_byte] = input_
		self.size = len(self.m_input)
