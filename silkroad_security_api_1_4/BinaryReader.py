#!/usr/bin/env python3
# from silkroad_security_api_1_4 import BinaryWriter
import array
import struct
from typing import Union

from silkroad_security_api_1_4.SeekType import SeekType


class BinaryReader(object):
	offset: int = 0
	data: array.array
	size: int = 0

	def __init__(self, data, offset=0):
		self.reset(data, offset)

	def __repr__(self):
		return f"size({self.size})offset({self.offset}){self.data}"

	def __str__(self):
		return f"size({self.size})offset({self.offset}){self.data}"

	def reset(self, data: Union[array.array, list, bytes], offset: int = 0):
		if type(data) == array.array:
			self.data = data
		elif isinstance(data, (list, bytes)):
			self.data = array.array('B', data)
		else:
			raise TypeError('incorrect data type was used to reset the class', type(data))

		self.size = self.data.__len__()
		self.seek_set(offset)

	def bytes_left(self):
		return self.size - self.offset

	def seek(self, offset, origin: SeekType = SeekType.Begin):
		if origin == SeekType.Begin:
			self.seek_set(offset)
		elif origin == SeekType.Current:
			self.seek_forward(offset)
		elif origin == SeekType.End:
			self.seek_set(self.size - offset)
		else:
			raise ValueError("Unknown SeekType passed", origin)

	def seek_forward(self, count):
		self.seek_set(self.offset + count)

	def seek_backward(self, count):
		self.seek_set(self.offset - count)

	def seek_set(self, offset):
		if offset > self.size or offset < 0:
			raise IndexError('invalid offset', offset, self.size)
		self.offset = offset

	def read_int8(self):
		if self.offset + 1 > self.size:
			raise EOFError()
		unpacked = struct.unpack_from('b', self.data, self.offset)[0]
		self.offset += 1
		return unpacked

	def read_uint8(self):
		if self.offset + 1 > self.size:
			raise EOFError()
		unpacked = struct.unpack_from('B', self.data, self.offset)[0]
		self.offset += 1
		return unpacked

	def read_byte(self):
		return self.read_uint8()

	def read_int16(self):
		if self.offset + 2 > self.size:
			raise EOFError()
		unpacked = struct.unpack_from('h', self.data, self.offset)[0]
		self.offset += 2
		return unpacked

	def read_uint16(self):
		if self.offset + 2 > self.size:
			raise EOFError()
		unpacked = struct.unpack_from('H', self.data, self.offset)[0]
		self.offset += 2
		return unpacked

	def read_int32(self):
		if self.offset + 4 > self.size:
			raise EOFError()
		unpacked = struct.unpack_from('i', self.data, self.offset)[0]
		self.offset += 4
		return unpacked

	def read_uint32(self):
		if self.offset + 4 > self.size:
			raise EOFError()
		unpacked = struct.unpack_from('I', self.data, self.offset)[0]
		self.offset += 4
		return unpacked

	def read_int64(self):
		if self.offset + 8 > self.size:
			raise EOFError()
		unpacked = struct.unpack_from('q', self.data, self.offset)[0]
		self.offset += 8
		return unpacked

	def read_uint64(self):
		if self.offset + 8 > self.size:
			raise EOFError()
		unpacked = struct.unpack_from('Q', self.data, self.offset)[0]
		self.offset += 8
		return unpacked

	def read_float(self):
		if self.offset + 4 > self.size:
			raise EOFError()
		unpacked = struct.unpack_from('f', self.data, self.offset)[0]
		self.offset += 4
		return unpacked

	def read_double(self):
		if self.offset + 8 > self.size:
			raise EOFError()
		unpacked = struct.unpack_from('d', self.data, self.offset)[0]
		self.offset += 8
		return unpacked

	def read_char(self):
		if self.offset + 1 > self.size:
			raise EOFError()
		unpacked = struct.unpack_from('c', self.data, self.offset)[0]
		self.offset += 1
		return unpacked

	def read_bytes(self, length):
		if self.offset + length > self.size:
			raise EOFError()
		b = struct.unpack_from(str(length) + 's', self.data, self.offset)[0]
		self.offset += length
		return b

	def read_ascii(self, length = 0):
		if length == 0:
			length = self.read_uint16()

		if self.offset + length > self.size:
			raise EOFError()
		s = struct.unpack_from(str(length) + 's', self.data, self.offset)[0]
		self.offset += length
		return s.decode('ascii', 'replace')

	'''
	def read_utf8(self, length):
		if self.offset + length > self.size:
			raise EOFError()
		s = struct.unpack_from(str(length) + 's', self.data, self.offset)[0]
		self.offset += length
		return s.decode('utf-8')
	'''

	def read_utf16(self, length):
		length *= 2
		if self.offset + length > self.size:
			raise EOFError()
		s = struct.unpack_from(str(length) + 's', self.data, self.offset)[0]
		self.offset += length
		return s.decode('utf-16le')

	def read_utf32(self, length):
		length *= 4
		if self.offset + length > self.size:
			raise EOFError()
		s = struct.unpack_from(str(length) + 's', self.data, self.offset)[0]
		self.offset += length
		return s.decode('utf-32le')

# if __name__ == '__main__':
# 	w = BinaryWriter()
# 	w.write_uint8(4)
# 	w.write_ascii('test')
# 	w.write_uint32(1337)
# 	w.write_int64(-1337313371337)
# 	w.write_float(1337.31337)
# 	w.write([0x75, 0x00, 0x74, 0x00, 0x66, 0x00, 0x31, 0x00, 0x36, 0x00, 0x20, 0x00, 0x77, 0x00, 0x6f, 0x00, 0x72, 0x00,
# 	         0x6b, 0x00, 0x73, 0x00])
#
# 	hex_str = str(w)
# 	hex_list = w.tolist()
# 	hex_array = w.toarray()
#
# 	print('String: %s' % hex_string)
# 	print('List: %s' % hex_list)
# 	print('Array: %s\n' % hex_array)
#
# 	''''''''''''''''''''''''''''''''''''''''''''''''''''''
#
# 	r = BinaryReader(hex_array)
# 	length = r.read_uint8()
# 	str = r.read_ascii(length)
# 	integer = r.read_uint32()
# 	integer_64 = r.read_int64()
# 	float = r.read_float()
# 	utf16 = r.read_utf16(11)
#
# 	print('Extracted length: \'%d\'' % length)
# 	print('Extractedstr: \'%s\'' %str)
# 	print('Extracted number: \'%s\'' % integer)
# 	print('Extracted signed 64-bit integer: \'%s\'' % integer_64)
# 	print('Extracted float: \'%s\'' % float)
# 	print('UTF-16: \'%s\'' % utf16)
# 	print('Bytes left: \'%s\'' % r.bytes_left())
