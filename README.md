# Lobot

Light, obtuse bot for SRO private servers using the VSRO 1.188 files.

The main goal is to make a platform independent bot using PyQt5.

## Requirements

**General requirements**:
- [Python3 (~3.8)](https://www.python.org/downloads/)
- [Silkroad private server files](https://www.elitepvpers.com/forum/private-sro-advertising/), preferably derived from **version 1.188**
- Silkroad loader of choice. edxSilkroadLoader is recommended
  - [Official link](https://www.elitepvpers.com/forum/sro-hacks-bots-cheats-exploits/685912-edxsilkroadloader5-beta-testing-3.html#post8781756)
  - Direct download: [edxSilkroadLoader5_0_3d.zip](https://gitlab.com/NamingThingsIsHard/game-tools/lobot/-/wikis/uploads/50abd63ba794f6c49b16068800f5d37c/edxSilkroadLoader5_0_3d.zip)

**Operation system dependent**:
- **Linux**: Any version with working [wine](https://wiki.winehq.org/Download) runtime
    , e.g. [Lutris](https://lutris.net/)
- **Windows**: WinXP or later
- **Mac**: TBD Has not been tested

## Getting started:

:information_source: [Please visit the Wiki for a primer](https://gitlab.com/NamingThingsIsHard/game-tools/lobot/-/wikis/Getting-Started) :information_source:

### Features
#### Check the [Changelog](https://gitlab.com/NamingThingsIsHard/game-tools/lobot/-/blob/develop/CHANGELOG.md) for a detailed list.
As the project name suggests, only the most essential features are planned. Once those work, enhancements and optimizations will be added at leisure.
The list of planned features is as follows:
#### Setup
- [x] **PK2 Extraction**: ~~Reading info from PK2 file~~ 
- [x] **Settings**: ~~issue 18~~
#### Login
- [x] **Client**: ~~(issue #3)~~
- [ ] ~~**Loader**: (issue #13)~~ Postponed indefinitely
- [x] ~~**Clientless**: (issue #11)~~ Simple login: Private servers don't like clientless anymore
#### Scripting
- [x] **Townscript**: ~~(issues #4, #6)~~
- [x] **Walkscript**: ~~(issue #7)~~
#### Training
- [x] **Autopotions**: ~~(issue #8)~~
- [x] **Item management**: ~~TODO (issue #5)~~
- [ ] **Training**: TODO (issue #9)
- [x] **Party**: ~~(issue #14)~~

