class ObjectStatusChangedArgs:
	def __init__(self, previous_status, current_status):
		self.previous_status = previous_status
		self.current_status = current_status
