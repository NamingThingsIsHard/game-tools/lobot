﻿from pathlib import Path
from typing import Dict, List

from lobot_api.globals.settings.LoginSettings import LoginSettings
from lobot_api.pk2.PK2Item import PK2Item
from lobot_api.pk2.PK2MagParam import PK2MagParam
from lobot_api.pk2.PK2NPC import PK2NPC
from lobot_api.pk2.PK2Skill import PK2Skill
from lobot_api.pk2.PK2TeleporterTypes import PK2Teleporter
from lobot_api.structs.Shop import Shop


class PK2DataGlobal:
	names: Dict[str, str] = {}
	items: Dict[int, PK2Item] = {}
	level_data: Dict[int, int] = {}
	mag_params: Dict[int, PK2MagParam] = {}
	npcs: Dict[int, PK2NPC] = {}
	shops: Dict[int, Shop] = {}  # instead of uint because itemmall numbers are negative (e.g. -268435455)
	skills: Dict[int, PK2Skill] = {}
	teleporters: Dict[int, PK2Teleporter] = {}

	# NavigationSegments: Dict[c_uint, NavMeshSegment]

	@staticmethod
	def pk2_media_file_path(): return Path(LoginSettings.instance().login_client_directory_path) / "Media.pk2"

	@staticmethod
	def pk2_data_file_path(): return Path(LoginSettings.instance().login_client_directory_path) / "Data.pk2"

	@staticmethod
	def pet_types_attack() -> List[str]:
		return [
			"COS_P_BEAR",
			"COS_P_FOX",
			"COS_P_PENGUIN",
			"COS_P_WOLF_WHITE_SMALL",
			"COS_P_WOLF_WHITE",
			"COS_P_WOLF",
			"COS_P_JINN",
			"COS_P_KANGAROO",
			"COS_P_RAVEN"
		]

	@staticmethod
	def pet_types_pickup() -> List[str]:
		return [
			"COS_P_SPOT_RABBIT",
			"COS_P_RABBIT",
			"cos_p_ggl_idER",
			"COS_P_MYOWON",
			"COS_P_SEOWON",
			"COS_P_RACCOONDOG",
			"COS_P_CAT",
			"COS_P_BROWNIE",
			"COS_P_PINKPIG",
			"COS_P_GOLDPIG",
			"COS_P_WINTER_SNOWMAN"
		]
