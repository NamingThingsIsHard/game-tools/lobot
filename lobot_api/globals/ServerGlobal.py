from ctypes import c_uint32
from typing import List, Dict

# from lobot_api.structs.Silkroad import ListedCharacter, Server
from lobot_api.connection.Server import Server


class ServerGlobal:
	server: List[Server]
	# listedCharacters: List[ListedCharacter]
	activeConversations: Dict[c_uint32, str]
