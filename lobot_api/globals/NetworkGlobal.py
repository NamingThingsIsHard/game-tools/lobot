from ctypes import c_ushort

from PyQt5.QtCore import pyqtSignal, pyqtProperty, QObject


class NetworkGlobal(QObject):
	__instance__ = None
	sig_clientdir = pyqtSignal(str)
	sig_serverGWIP = pyqtSignal(str)
	sig_serverGWPort = pyqtSignal(str)
	sig_serverDivision = pyqtSignal(str)

	PROXY_ADDRESS: str = "127.0.0.1"
	""" The port used to connect the client to the proxy. """

	@staticmethod
	def instance():
		if NetworkGlobal.__instance__ is None:
			NetworkGlobal.__instance__ = NetworkGlobal()
		return NetworkGlobal.__instance__

	def __init__(self):
		super().__init__()
		self.clientDir = ''
		self._serverDiv = ''
		self._gatewayIP = ''
		self._gatewayPort = 0

	@pyqtProperty('QString', notify=sig_clientdir)
	def client_directory(self):
		""" The Directory containing the sro_client.exe file. """
		return self.clientDir

	@client_directory.setter
	def client_directory(self, value):
		self.clientDir = value

	""" The full path to the sro_client.exe file. """
	ClientFilePath: str = ''
	"""The default port used to connect the client to the proxy"""
	ProxyGatewayPort: c_ushort = 15779
	""" The port used to connect the proxy to the game server. """
	ProxyAgentPort: c_ushort = 20001
	BotPort: c_ushort = 20002
	""" The gateway division (login server number) to connect to the login server. """

	@pyqtProperty('QString', notify=sig_serverDivision)
	def gateway_server_division(self):
		""" The remote IP address (Joymax port) used to connect to the login server. """
		return self._serverDiv

	@gateway_server_division.setter
	def gateway_server_division(self, value):
		if self._serverDiv != value:
			self._serverDiv = value
			self.sig_serverDivision.emit(value)

	@pyqtProperty('QString', notify=sig_serverGWIP)
	def gateway_ip(self):
		""" The remote IP address (Joymax IP) used to connect to the game server. """
		return self._gatewayIP

	@gateway_ip.setter
	def gateway_ip(self, value):
		if self._gatewayIP != value:
			self._gatewayIP = value
			self.sig_serverGWIP.emit(value)

	@pyqtProperty('QString', notify=sig_serverGWPort)
	def gateway_port(self) -> int:
		""" The remote port (Joymax port) used to connect to the game server. """
		return int(self._gatewayPort)

	@gateway_port.setter
	def gateway_port(self, value):
		if self._gatewayPort != value:
			self._gatewayPort = value
			self.sig_serverGWPort.emit(str(value))

	agent_IP: str = ''
	""" The remote port (Joymax port) used to connect to the login server. """
	agent_port: c_ushort = 15884
