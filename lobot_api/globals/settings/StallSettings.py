from typing import List, Dict

from lobot_api.globals.settings.Settings import settings
from lobot_api.structs.Item import StallItem


class StallSettings:
	__instance__ = None

	@staticmethod
	def instance():
		if StallSettings.__instance__ is None:
			StallSettings.__instance__ = StallSettings()
		return StallSettings.__instance__

	MAX_STALL_ITEMS = 10
	default_name: str = "[Lobot]'s store"
	default_message: str = "Welcome to [Lobot]'s store"

	stall_name: str = "Lobot's stall."
	stall_message: str = "welcome to Lobot's stall."
	stall_time: int = 0
	is_open: bool = False

	@property
	def auto_add(self) -> bool: return settings['Stall']['AutoAdd'] == 'True'

	@auto_add.setter
	def auto_add(self, value):
		settings['Stall']['AutoAdd'] = value

	@property
	def resume_training(self) -> bool: return settings['Stall']['ResumeTraining'] == 'True'

	@resume_training.setter
	def resume_training(self, value):
		settings['Stall']['ResumeTraining'] = value

	stall_items: Dict[int, StallItem] = {}
	stall_log: List[str] = []
# event EventHandlerToggledAutoAdd = Event()
#
# event EventHandlerToggledResumeTraining = Event()
# @staticmethod	def on_toggled_AutoAdd(e: BoolEventArgs):
# @staticmethod	def on_toggled_ToggledResumeTraining(e: BoolEventArgs):
