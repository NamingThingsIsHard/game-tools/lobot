from lobot_api.binding import LobotProperty
from lobot_api.globals.settings.Settings import settings


class ItemFilterSettings:
	DegreesAll = LobotProperty(settings.get, ['Filter', 'DegreesAll'], settings.set, ['Filter', 'DegreesAll'])
	DegreesEU = LobotProperty(settings.get, ['Filter', 'DegreesEU'], settings.set, ['Filter', 'DegreesEU'])
	DegreesCH = LobotProperty(settings.get, ['Filter', 'DegreesCH'], settings.set, ['Filter', 'DegreesCH'])

	FreeItems = LobotProperty(settings.getboolean, ['Filter', 'PickFreeItems'], settings.set, ['Filter', 'PickFreeItems'])
	OwnItems = LobotProperty(settings.getboolean, ['Filter', 'PickOwnItems'], settings.set, ['Filter', 'PickOwnItems'])

	WeaponElixir = LobotProperty(settings.get, ['Filter', 'WeaponElixir'], settings.set, ['Filter', 'WeaponElixir'])
	ShieldElixir = LobotProperty(settings.get, ['Filter', 'ShieldElixir'], settings.set, ['Filter', 'ShieldElixir'])
	ProtectorElixir = LobotProperty(settings.get, ['Filter', 'ProtectorElixir'], settings.set,
	                                ['Filter', 'ProtectorElixir'])
	AccessoryElixir = LobotProperty(settings.get, ['Filter', 'AccessoryElixir'], settings.set,
	                                ['Filter', 'AccessoryElixir'])
	AlchemyMaterial = LobotProperty(settings.get, ['Filter', 'AlchemyMaterial'], settings.set,
	                                ['Filter', 'AlchemyMaterial'])

	HPPots = LobotProperty(settings.get, ['Filter', 'HPPots'], settings.set, ['Filter', 'HPPots'])
	MPPots = LobotProperty(settings.get, ['Filter', 'MPPots'], settings.set, ['Filter', 'MPPots'])
	VigorPots = LobotProperty(settings.get, ['Filter', 'VigorPots'], settings.set, ['Filter', 'VigorPots'])
	Grains = LobotProperty(settings.get, ['Filter', 'Grains'], settings.set, ['Filter', 'Grains'])
	UniversalPills = LobotProperty(settings.get, ['Filter', 'UniversalPills'], settings.set,
	                               ['Filter', 'UniversalPills'])
	PurificationPills = LobotProperty(settings.get, ['Filter', 'PurificationPills'], settings.set,
	                                  ['Filter', 'PurificationPills'])
	Arrows = LobotProperty(settings.get, ['Filter', 'Arrows'], settings.set, ['Filter', 'Arrows'])
	Bolts = LobotProperty(settings.get, ['Filter', 'Bolts'], settings.set, ['Filter', 'Bolts'])
	ReturnScrolls = LobotProperty(settings.get, ['Filter', 'ReturnScrolls'], settings.set, ['Filter', 'ReturnScrolls'])
	COSItems = LobotProperty(settings.get, ['Filter', 'COSItems'], settings.set, ['Filter', 'COSItems'])

	QuestItems = LobotProperty(settings.getboolean, ['Filter', 'QuestItems'], settings.set, ['Filter', 'QuestItems'])
	# EventItems = LobotProperty(settings.getboolean, ['Filter', 'QuestItems'], settings.set, ['Filter', 'EventItems'])
	Gold = LobotProperty(settings.getboolean, ['Filter', 'Gold'], settings.set, ['Filter', 'Gold'])

	Head = LobotProperty(settings.get, ['Filter', 'Head'], settings.set, ['Filter', 'Head'])

	Chest = LobotProperty(settings.get, ['Filter', 'Chest'], settings.set, ['Filter', 'Chest'])

	Hose = LobotProperty(settings.get, ['Filter', 'Hose'], settings.set, ['Filter', 'Hose'])
	Shoulder = LobotProperty(settings.get, ['Filter', 'Shoulder'], settings.set, ['Filter', 'Shoulder'])

	Glove = LobotProperty(settings.get, ['Filter', 'Glove'], settings.set, ['Filter', 'Glove'])

	Boots = LobotProperty(settings.get, ['Filter', 'Boots'], settings.set, ['Filter', 'Boots'])

	Necklace = LobotProperty(settings.get, ['Filter', 'Necklace'], settings.set, ['Filter', 'Necklace'])

	Earrings = LobotProperty(settings.get, ['Filter', 'Earrings'], settings.set, ['Filter', 'Earrings'])

	Ring = LobotProperty(settings.get, ['Filter', 'Ring'], settings.set, ['Filter', 'Ring'])

	Shield = LobotProperty(settings.get, ['Filter', 'Shield'], settings.set, ['Filter', 'Shield'])
	Sword = LobotProperty(settings.get, ['Filter', 'Sword'], settings.set, ['Filter', 'Sword'])
	Blade = LobotProperty(settings.get, ['Filter', 'Blade'], settings.set, ['Filter', 'Blade'])

	Spear = LobotProperty(settings.get, ['Filter', 'Spear'], settings.set, ['Filter', 'Spear'])
	Glaive = LobotProperty(settings.get, ['Filter', 'Glaive'], settings.set, ['Filter', 'Glaive'])

	Bow = LobotProperty(settings.get, ['Filter', 'Bow'], settings.set, ['Filter', 'Bow'])

	EUHead = LobotProperty(settings.get, ['Filter', 'EUHead'], settings.set, ['Filter', 'EUHead'])

	EUChest = LobotProperty(settings.get, ['Filter', 'EUChest'], settings.set, ['Filter', 'EUChest'])
	EUHose = LobotProperty(settings.get, ['Filter', 'EUHose'], settings.set, ['Filter', 'EUHose'])

	EUShoulder = LobotProperty(settings.get, ['Filter', 'EUShoulder'], settings.set, ['Filter', 'EUShoulder'])
	EUGloves = LobotProperty(settings.get, ['Filter', 'EUGloves'], settings.set, ['Filter', 'EUGloves'])
	EUBoots = LobotProperty(settings.get, ['Filter', 'EUBoots'], settings.set, ['Filter', 'EUBoots'])

	EUNecklace = LobotProperty(settings.get, ['Filter', 'EUNecklace'], settings.set, ['Filter', 'EUNecklace'])

	EUEarrings = LobotProperty(settings.get, ['Filter', 'EUEarrings'], settings.set, ['Filter', 'EUEarrings'])

	EURing = LobotProperty(settings.get, ['Filter', 'EURing'], settings.set, ['Filter', 'EURing'])

	EUShield = LobotProperty(settings.get, ['Filter', 'EUShield'], settings.set, ['Filter', 'EUShield'])
	EUSword1H = LobotProperty(settings.get, ['Filter', 'EUSword1H'], settings.set, ['Filter', 'EUSword1H'])
	EUSword2H = LobotProperty(settings.get, ['Filter', 'EUSword2H'], settings.set, ['Filter', 'EUSword2H'])

	EUAxe = LobotProperty(settings.get, ['Filter', 'EUAxe'], settings.set, ['Filter', 'EUAxe'])

	EUCrossbow = LobotProperty(settings.get, ['Filter', 'EUCrossbow'], settings.set, ['Filter', 'EUCrossbow'])

	EUDagger = LobotProperty(settings.get, ['Filter', 'EUDagger'], settings.set, ['Filter', 'EUDagger'])
	EUWarlockStaff = LobotProperty(settings.get, ['Filter', 'EUWarlockStaff'], settings.set,
	                               ['Filter', 'EUWarlockStaff'])

	EUWizardStaff = LobotProperty(settings.get, ['Filter', 'EUWizardStaff'], settings.set, ['Filter', 'EUWizardStaff'])

	EUHarp = LobotProperty(settings.get, ['Filter', 'EUHarp'], settings.set, ['Filter', 'EUHarp'])

	EUClericStaff = LobotProperty(settings.get, ['Filter', 'EUClericStaff'], settings.set, ['Filter', 'EUClericStaff'])