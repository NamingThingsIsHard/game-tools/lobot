from typing import List, Optional

from lobot_api.binding import LobotProperty
from lobot_api.globals.settings.Settings import settings
from lobot_api.structs.CharData import Skill


class SkillSettings:
	__instance__ = None

	@staticmethod
	def instance():
		if SkillSettings.__instance__ is None:
			SkillSettings.__instance__ = SkillSettings()
		return SkillSettings.__instance__

	Imbue: Optional[Skill] = None
	Skills: List[Skill] = []

	MonsterTypes: List[Skill] = []
	InitiationSkills: List[Skill] = []
	GeneralSkills: List[Skill] = []
	FinisherSkills: List[Skill] = []
	BuffSkillsWeak: List[Skill] = []
	BuffSkillsStrong: List[Skill] = []
	MembersToBuff: List[Skill] = []
	MemberBuffs: List[Skill] = []

	skill_list = LobotProperty(settings.get, ['Skill', 'SkillList'], settings.set, ['Skill', 'SkillList'])
	buff_skills_weak = LobotProperty(settings.get, ['Skill', 'BuffSkillsWeak'], settings.set, ['Skill', 'BuffSkillsWeak'])
	buff_skills_strong = LobotProperty(settings.get, ['Skill', 'BuffSkillsStrong'], settings.set, ['Skill', 'BuffSkillsStrong'])
	general_list = LobotProperty(settings.get, ['Skill', 'General'], settings.set, ['Skill', 'General'])
	initiations_list = LobotProperty(settings.get, ['Skill', 'Initiations'], settings.set, ['Skill', 'Initiations'])
	finishers_list = LobotProperty(settings.get, ['Skill', 'Finishers'], settings.set, ['Skill', 'Finishers'])

	imbue = LobotProperty(settings.getint, ['Skill', 'Imbue'], settings.set, ['Skill', 'Imbue'])

	speed_buff = LobotProperty(settings.getint, ['Skill', 'SpeedBuff'], settings.set, ['Skill', 'SpeedBuff'])
	heal_spell = LobotProperty(settings.getint, ['Skill', 'HealSpell'], settings.set, ['Skill', 'HealSpell'])
	mana_spell = LobotProperty(settings.getint, ['Skill', 'ManaSpell'], settings.set, ['Skill', 'ManaSpell'])
	res_spell = LobotProperty(settings.getint, ['Skill', 'ResSpell'], settings.set, ['Skill', 'ResSpell'])
	cycle_skills = LobotProperty(settings.getboolean, ['Skill', 'CycleSkills'], settings.set, ['Skill', 'CycleSkills'])
	speed_buff_or_pot = LobotProperty(settings.getboolean, ['Skill', 'SpeedBuffOrPotion'], settings.set, ['Skill', 'SpeedBuffOrPotion'])
	buff_all_or_members = LobotProperty(settings.getboolean, ['Skill', 'BuffAllOrMembers'], settings.set, ['Skill', 'BuffAllOrMembers'])
	use_heal = LobotProperty(settings.getboolean, ['Skill', 'UseHeal'], settings.set, ['Skill', 'UseHeal'])
	heal_percentage = LobotProperty(settings.getint, ['Skill', 'UseHealPercentage'], settings.set, ['Skill', 'UseHealPercentage'])
	use_mp_spell = LobotProperty(settings.getboolean, ['Skill', 'UseMPSpell'], settings.set, ['Skill', 'UseMPSpell'])
	mp_percentage = LobotProperty(settings.getint, ['Skill', 'UseMPSpellPercentage'], settings.set, ['Skill', 'UseMPSpellPercentage'])
	use_res = LobotProperty(settings.getboolean, ['Skill', 'UseRes'], settings.set, ['Skill', 'UseRes'])

""" Handle updates to Lists in order to save settings file.
# This should be called after the function to prevent the lists' event handlers from being reset.
"""
# #        @staticmethod
# 		def initialize_handlers()
#
"""Serialize changes to skill list"""
#            Bot.char_data.skills.CollectionChanged += UpdateSkillList
"""Reset imbue, speed, heal, mana and res skills"""
#            Bot.char_data.CharNameChanged += HandleParsedCharData
"""initialize Selected skills to prevent None reference exceptions"""
#            SelectedInitiationSkills = InitiationSkills[0]
#            SelectedGeneralSkills = GeneralSkills[0]
#            SelectedFinisherSkills = FinisherSkills[0]
#            SelectedBuffs = BuffSkills[0]
#            SelectedInitiationSkills.CollectionChanged += UpdateInitiationList
#            SelectedGeneralSkills.CollectionChanged += UpdateGeneralList
#            SelectedFinisherSkills.CollectionChanged += UpdateFinisherList
#            SelectedBuffs.CollectionChanged += UpdateBuffList
#            MemberBuffs.CollectionChanged += UpdateMemberBuffList
#
