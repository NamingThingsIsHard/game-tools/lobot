import logging
from typing import List, Dict, Callable

from lobot_api.Bot import Bot
from lobot_api.Event import Event
from lobot_api.binding import LobotProperty
from lobot_api.globals import ConsumablesGlobal
from lobot_api.globals.SpawnListsGlobal import SpawnListsGlobal
from lobot_api.globals.settings.Settings import settings
from lobot_api.logger.Logger import BOTTING
from lobot_api.pk2.PK2Item import PK2ItemType
from lobot_api.scripting.Area import Area
from lobot_api.structs.EquipmentSlot import EquipmentSlot
from lobot_api.structs.Monster import MonsterType


class GeneralTrainingSettings:
	__instance__ = None

	@staticmethod
	def instance():
		if GeneralTrainingSettings.__instance__ is None:
			GeneralTrainingSettings.__instance__ = GeneralTrainingSettings()
		return GeneralTrainingSettings.__instance__

	@property
	def prioritize_weakest(self):
		return settings['Training']['AttackPrioritizeWeakest'] == 'True'

	@prioritize_weakest.setter
	def prioritize_weakest(self, value):
		settings['Training']['AttackPrioritizeWeakest'] = value

	@property
	def switch_on_aggro(self):
		return settings['Training']['AttackSwitchIfPlayerAttacks'] == 'True'

	@switch_on_aggro.setter
	def switch_on_aggro(self, value):
		settings['Training']['AttackSwitchIfPlayerAttacks'] = value

	@property
	def defend_teammates(self):
		return settings['Training']['AttackDefendTeam'] == 'True'

	@defend_teammates.setter
	def defend_teammates(self, value):
		settings['Training']['AttackDefendTeam'] = value

	@property
	def defend_pet(self):
		return settings['Training']['AttackDefendTeam'] == 'True'

	@defend_pet.setter
	def defend_pet(self, value):
		settings['Training']['AttackDefendTeam'] = value

	@property
	def return_to_center(self):
		return settings['Training']['AttackDefendTeam'] == 'True'

	@return_to_center.setter
	def return_to_center(self, value):
		settings['Training']['AttackDefendTeam'] = value

	@property
	def focus_unique(self):
		return settings['Training']['AttackFocusUnique'] == 'True'

	@focus_unique.setter
	def focus_unique(self, value):
		settings['Training']['AttackFocusUnique'] = value

	@property
	def auto_attack_event_monster(self):
		return settings['Training']['AttackAutoAttackEvent'] == 'True'

	@auto_attack_event_monster.setter
	def auto_attack_event_monster(self, value):
		settings['Training']['AttackAutoAttackEvent'] = value

	@property
	def warlock_dot_switch(self):
		return settings['Training']['warlock_dot_switch'] == 'True'

	@warlock_dot_switch.setter
	def warlock_dot_switch(self, value):
		settings['Training']['warlock_dot_switch'] = value

	@property
	def zerk_when_ready(self):
		return settings['Training']['ZerkUseWhenReady'] == 'True'

	@zerk_when_ready.setter
	def zerk_when_ready(self, value):
		settings['Training']['ZerkUseWhenReady'] = value

	@property
	def zerk_vs_strong(self):
		return settings['Training']['ZerkVSStrong'] == 'True'

	@zerk_vs_strong.setter
	def zerk_vs_strong(self, value):
		settings['Training']['ZerkVSStrong'] = value

	@property
	def zerk_when_x_attackers(self):
		return settings['Training']['ZerkAttackersMoreThanX'] == 'True'

	@zerk_when_x_attackers.setter
	def zerk_when_x_attackers(self, value):
		settings['Training']['ZerkAttackersMoreThanX'] = value

	@property
	def x_attackers(self):
		return int(settings['Training']['ZerkAttackersX'])

	@x_attackers.setter
	def x_attackers(self, value):
		settings['Training']['ZerkAttackersX'] = value

	IgnoreList: List[MonsterType]
	IgnoreListSpecial: Dict[MonsterType, bool]

	@property
	def dont_attack(self):
		return settings['Training']['IgnoreRemainIdle'] == 'True'

	@dont_attack.setter
	def dont_attack(self, value):
		settings['Training']['IgnoreRemainIdle'] = value

	@property
	def return_dead(self):
		return settings['Training']['ReturnDead'] == 'True'

	@return_dead.setter
	def return_dead(self, value):
		settings['Training']['ReturnDead'] = value

	@property
	def return_dead_x_minutes(self):
		return settings['Training']['ReturnDeadMinutesMoreThan'] == 'True'

	@return_dead_x_minutes.setter
	def return_dead_x_minutes(self, value):
		settings['Training']['ReturnDeadMinutesMoreThan'] = value

	@property
	def x_minutes(self):
		return int(settings['Training']['x_minutes'])

	@x_minutes.setter
	def x_minutes(self, value):
		settings['Training']['x_minutes'] = value

	@property
	def return_inventory_full(self):
		return settings['Training']['ReturnFull'] == 'True'

	@return_inventory_full.setter
	def return_inventory_full(self, value):
		settings['Training']['ReturnFull'] = value

	@property
	def return_unique(self):
		return settings['Training']['ReturnUniqueFound'] == 'True'

	@return_unique.setter
	def return_unique(self, value):
		settings['Training']['ReturnUniqueFound'] = value

	@property
	def return_gm_found(self):
		return settings['Training']['return_gm_found'] == 'True'

	@return_gm_found.setter
	def return_gm_found(self, value):
		settings['Training']['return_gm_found'] = value

	@property
	def return_no_vigors(self):
		return settings['Training']['return_no_vigors'] == 'True'

	@return_no_vigors.setter
	def return_no_vigors(self, value):
		settings['Training']['return_no_vigors'] = value

	@property
	def return_no_universal_pills(self):
		return settings['Training']['return_no_universal_pills'] == 'True'

	@return_no_universal_pills.setter
	def return_no_universal_pills(self, value):
		settings['Training']['return_no_universal_pills'] = value

	@property
	def return_no_special_pills(self):
		return settings['Training']['return_no_special_pills'] == 'True'

	@return_no_special_pills.setter
	def return_no_special_pills(self, value):
		settings['Training']['return_no_special_pills'] = value

	@property
	def return_low_hp_pots(self):
		return settings['Training']['return_low_hp_pots'] == 'True'

	@return_low_hp_pots.setter
	def return_low_hp_pots(self, value):
		settings['Training']['return_low_hp_pots'] = value

	@property
	def low_hp_pots(self):
		return int(settings['Training']['ReturnHPPotsX'])

	@low_hp_pots.setter
	def low_hp_pots(self, value):
		settings['Training']['ReturnHPPotsX'] = value

	@property
	def return_low_mp_pots(self):
		return settings['Training']['return_low_mp_pots'] == 'True'

	@return_low_mp_pots.setter
	def return_low_mp_pots(self, value):
		settings['Training']['return_low_mp_pots'] = value

	@property
	def low_mp_pots(self):
		return int(settings['Training']['ReturnMpPotsX'])

	@low_mp_pots.setter
	def low_mp_pots(self, value):
		settings['Training']['ReturnMpPotsX'] = value

	@property
	def return_low_ammo(self):
		return settings['Training']['return_low_ammo'] == 'True'

	@return_low_ammo.setter
	def return_low_ammo(self, value):
		settings['Training']['return_low_ammo'] = value

	@property
	def low_ammo(self):
		return int(settings['Training']['ReturnAmmoX'])

	@low_ammo.setter
	def low_ammo(self, value):
		settings['Training']['ReturnAmmoX'] = value

	@property
	def return_low_weapon_durability(self):
		return settings['Training']['ReturnLowWeaponDur'] == 'True'

	@return_low_weapon_durability.setter
	def return_low_weapon_durability(self, value):
		settings['Training']['ReturnLowWeaponDur'] = value

	@property
	def low_weapon_durability(self):
		return int(settings['Training']['ReturnWeaponDurX'])

	@low_weapon_durability.setter
	def low_weapon_durability(self, value):
		settings['Training']['ReturnWeaponDurX'] = value

	@property
	def return_x_broken_items(self):
		return settings['Training']['ReturnBrokenItems'] == 'True'

	@return_x_broken_items.setter
	def return_x_broken_items(self, value):
		settings['Training']['ReturnBrokenItems'] = value

	@property
	def x_broken_items(self):
		return int(settings['Training']['ReturnBrokenItemsX'])

	@x_broken_items.setter
	def x_broken_items(self, value):
		settings['Training']['ReturnBrokenItemsX'] = value

	@property
	def return_quests_finished(self):
		return settings['Training']['ReturnQuestsDone'] == 'True'

	@return_quests_finished.setter
	def return_quests_finished(self, value):
		settings['Training']['ReturnQuestsDone'] = value

	@property
	def x_quests_finished(self):
		return int(settings['Training']['ReturnQuestsDoneX'])

	@x_quests_finished.setter
	def x_quests_finished(self, value):
		settings['Training']['ReturnQuestsDoneX'] = value

	def __dead__(self):
		return Bot.char_data.is_alive is False and (
				self.return_dead or self.return_dead_x_minutes and self.x_minutes < 1)

	def __inventory_full__(self):
		return self.return_inventory_full and Bot.character_inventory_is_full()

	def __pet_inventory_full__(self):
		return self.return_inventory_full\
		       and Bot.pickup_pet and Bot.pickup_pet.unique_id != 0\
		       and len(Bot.pet_inventory) is Bot.pet_inventory_size

	def __unique__(self):
		return self.return_unique and any(
			value.type is MonsterType.Unique for key, value in SpawnListsGlobal.MonsterSpawns)

	def __gm_found__(self):
		return self.return_gm_found and any(value.GM_flag == 0x1 for key, value in SpawnListsGlobal.Players)

	def __no_vigors__(self):
		return self.return_no_vigors and all(
			value.type is not PK2ItemType.VigorPot or value.type is not PK2ItemType.VigorGrain for key, value in
			Bot.char_data.inventory.items.items())

	def __no_universal_pills__(self):
		return self.return_no_universal_pills and all(
			value.type is not PK2ItemType.UniversalPill for key, value in Bot.char_data.inventory.items.items())

	def __no_special_pills__(self):
		return self.return_no_special_pills and all(
			value.type is not PK2ItemType.PurificationPill for key, value in Bot.char_data.inventory.items.items())

	def __low_hp_pots_f__(self):
		return self.return_low_hp_pots and sum(
			value.stack_count for key, value in ConsumablesGlobal.hp_pots()) <= self.low_hp_pots

	def __low_mp_pots_f__(self):
		return self.return_low_mp_pots and sum(
			value.stack_count for key, value in ConsumablesGlobal.mp_pots()) <= self.low_mp_pots

	def __low_ammo_f__(self):
		return self.return_low_ammo and sum(
			value.stack_count for key, value in ConsumablesGlobal.ammo()) <= self.low_ammo

	def __low_weapon_durability_f__(self):
		return self.return_low_weapon_durability and Bot.char_data.inventory.equipment.get(EquipmentSlot.Primary,
		                                                                                   None) and Bot.char_data.inventory.items.get(
			EquipmentSlot.Primary.value).durability <= self.low_weapon_durability

	def __x_broken_items_f__(self):
		is_set = self.return_x_broken_items
		min_broken = self.x_broken_items
		x_items_broken = 0
		for key, value in Bot.char_data.inventory.equipment.items():
			if value.pk2_item.is_durability_item():
				if value.durability < 1:
					x_items_broken += 1

		return is_set and x_items_broken >= min_broken

	def __x_quests_finished_f__(self):
		return False  # TODO /*return_quests_finished*/

	@property
	def return_criteria(self) -> Dict[Callable[[None], bool], str]:
		"""
		The criteria used to choose whether to go return to town.
		:return: Dictionary of criteria to check.
		"""
		return {
			self.__dead__: "Character is dead",
			self.__inventory_full__: "inventory is full",
			self.__pet_inventory_full__: "Pet inventory is full",
			self.__unique__: "Unique Spawned",
			self.__gm_found__: "GM spawned",
			self.__no_vigors__: "No vigor potions left",
			self.__no_universal_pills__: "No universal pills left",
			self.__no_special_pills__: "No special pills left",
			self.__low_hp_pots_f__: "Few HP pots left",
			self.__low_mp_pots_f__: "Few MP pots left",
			self.__low_ammo_f__: "Low ammo",
			self.__low_weapon_durability_f__: "Low weapon durability",
			self.__x_broken_items_f__: f"{self.x_broken_items} broken items",
			self.__x_quests_finished_f__: f"{self.x_quests_finished} quests finished"
		}

	@staticmethod
	def should_return_to_town() -> bool:
		for key, value in GeneralTrainingSettings.__instance__.return_criteria.items():
			if key():
				logging.log(BOTTING, f"Returning to town: {value}")
				return True

		return False


class TrainingAreaSettings:
	__instance__ = None
	on_area_changed = Event()
	_area1: Area = None
	_area2: Area = None

	@staticmethod
	def instance():
		if TrainingAreaSettings.__instance__ is None:
			TrainingAreaSettings.__instance__ = TrainingAreaSettings()
		return TrainingAreaSettings.__instance__

	@property
	def area1(self) -> Area:
		s = settings['Training']['TrainingArea1']
		if not self._area1 or str(self._area1) != s:
			self._area1 = Area.from_string(s)
		return self._area1

	@area1.setter
	def area1(self, value):
		settings['Training']['TrainingArea1'] = value
		self.on_area_changed.notify()

	@property
	def area2(self) -> Area:
		s = settings['Training']['TrainingArea2']
		if not self._area2 or str(self._area2) != s:
			self._area2 = Area.from_string(s)
		return self._area2

	@area2.setter
	def area2(self, value):
		settings['Training']['TrainingArea2'] = value
		self.on_area_changed.notify()

	@property
	def use_second_area(self):
		return settings['Training']['TrainingUseArea2'] == 'True'

	@use_second_area.setter
	def use_second_area(self, value):
		settings['Training']['TrainingUseArea2'] = value

	@property
	def stay_idle_option(self):
		return settings['Training']['TrainingStayIdleOption'] == 'True'

	@stay_idle_option.setter
	def stay_idle_option(self, value):
		settings['Training']['TrainingStayIdleOption'] = value


class PlayerSettings:
	potion_delay = LobotProperty(settings.getint, ['Training', 'AutoPotionDelay'], settings.set,
	                             ['Training', 'AutoPotionDelay'])
	hp_use_pot = LobotProperty(settings.getboolean, ['Training', 'AutoPotionsLowHP'], settings.set,
	                           ['Training', 'AutoPotionsLowHP'])
	hp_pot_percent = LobotProperty(settings.getint, ['Training', 'AutoPotionsLowHPX'], settings.set,
	                               ['Training', 'AutoPotionsLowHPX'])
	hp_use_vigor = LobotProperty(settings.getboolean, ['Training', 'AutoPotionsLowVigorHP'], settings.set,
	                             ['Training', 'AutoPotionsLowVigorHP'])
	hp_vigor_percent = LobotProperty(settings.getint, ['Training', 'AutoPotionsLowVigorHPX'], settings.set,
	                                 ['Training', 'AutoPotionsLowVigorHPX'])
	mp_use_pot = LobotProperty(settings.getboolean, ['Training', 'AutoPotionsLowMP'], settings.set,
	                           ['Training', 'AutoPotionsLowMP'])
	mp_pot_percent = LobotProperty(settings.getint, ['Training', 'AutoPotionsLowMPX'], settings.set,
	                               ['Training', 'AutoPotionsLowMPX'])
	mp_use_vigor = LobotProperty(settings.getboolean, ['Training', 'AutoPotionsLowVigorMP'], settings.set,
	                             ['Training', 'AutoPotionsLowVigorMP'])
	prefer_grains = LobotProperty(settings.getboolean, ['Training', 'AutoPotionsPreferGrains'], settings.set,
	                              ['Training', 'AutoPotionsPreferGrains'])
	mp_vigor_percent = LobotProperty(settings.getint, ['Training', 'AutoPotionsLowVigorMPX'], settings.set,
	                                 ['Training', 'AutoPotionsLowVigorMPX'])
	bad_status_cure = LobotProperty(settings.getboolean, ['Training', 'AutoPotionCurseCure'],
	                                settings.set, ['Training', 'AutoPotionCurseCure'])
	bad_status_pill_or_spell = LobotProperty(settings.getboolean, ['Training', 'AutoPotionBadStatusUsePillOrSpell'],
	                                         settings.set, ['Training', 'AutoPotionBadStatusUsePillOrSpell'])
	bad_status_spell = LobotProperty(settings.getint, ['Training', 'AutoPotionBadStatusSpell'], settings.set,
	                                 ['Training', 'AutoPotionBadStatusSpell'])
	curse_cure = LobotProperty(settings.getboolean, ['Training', 'AutoPotionCurseCure'], settings.set,
	                           ['Training', 'AutoPotionCurseCure'])
	curse_pill_or_spell = LobotProperty(settings.getboolean, ['Training', 'AutoPotionCurseUsePillOrSpell'], settings.set,
	                                    ['Training', 'AutoPotionCurseUsePillOrSpell'])
	curse_spell = LobotProperty(settings.getint, ['Training', 'AutoPotionCurseSpell'], settings.set,
	                            ['Training', 'AutoPotionCurseSpell'])


# static
# PlayerSettings()
#
# VigorTimer.elapsed += on_elapsed_time
# HPPotionTimer.elapsed += on_elapsed_time
# MPPotionTimer.elapsed += on_elapsed_time
# UniversalPillTimer.elapsed += on_elapsed_time
# PurificationTimer.elapsed += on_elapsed_time


# @staticmethod	def on_elapsed_time(object source, e): #: elapsedEventArgs):
class PetSettings:
	# PotionTimer: threading.Timer = threading.Timer(1000)
	# PillTimer: threading.Timer = threading.Timer(1000)
	# HGPTimer: threading.Timer = threading.Timer(1000)
	revive_pet = LobotProperty(settings.getboolean, ['Training', 'RevivePet'], settings.set, ['Training', 'RevivePet'])
	hp_use_pot = LobotProperty(settings.getboolean, ['Training', 'AutoPotionsLowPetHP'], settings.set,
	                           ['Training', 'AutoPotionsLowPetHP'])
	hp_pot_percent = LobotProperty(settings.getint, ['Training', 'AutoPotionsLowPetHPX'], settings.set,
	                               ['Training', 'AutoPotionsLowPetHPX'])
	hgp_use_pot = LobotProperty(settings.getboolean, ['Training', 'AutoPotionsLowPetHGP'], settings.set,
	                            ['Training', 'AutoPotionsLowPetHGP'])
	hgp_pot_percent = LobotProperty(settings.getint, ['Training', 'AutoPotionsLowPetHGPX'], settings.set,
	                                ['Training', 'AutoPotionsLowPetHGPX'])
	CureBadStatus = LobotProperty(settings.getboolean, ['Training', 'AutoPotionsPetBadStatus'], settings.set,
	                              ['Training', 'AutoPotionsPetBadStatus'])


# static
# PetSettings()
#
# PotionTimer.elapsed += on_elapsed_time
# PillTimer.elapsed += on_elapsed_time
# HGPTimer.elapsed += on_elapsed_time


# @staticmethod	def on_elapsed_time(object source, e): #: elapsedEventArgs):
class COSSettings:
	# PotionDelay: threading.Timer = threading.Timer(1000)
	HPUsePot = LobotProperty(settings.getboolean, ['Training', 'AutoPotionsLowTransportHP'], settings.set,
	                         ['Training', 'AutoPotionsLowTransportHP'])
	HPPotPercent = LobotProperty(settings.getint, ['Training', 'AutoPotionsLowTransportHPX'], settings.set,
	                             ['Training', 'AutoPotionsLowTransportHPX'])
	CureBadStatus = LobotProperty(settings.getboolean, ['Training', 'AutoPotionsTransportBadStatus'], settings.set,
	                              ['Training', 'AutoPotionsTransportBadStatus'])

# static
# COSSettings()
#
# PotionDelay.elapsed += on_elapsed_time

# @staticmethod	def on_elapsed_time(object source, e): #: elapsedEventArgs):
