import logging
from configparser import ConfigParser
from pathlib import Path

from lobot_api.globals.DirectoryGlobal import DirectoryGlobal
# from lobot_api.structs.CharData import Skill, Buff
# from lobot_api.structs.Item import Item
# from lobot_api.structs.Monster import MonsterType
from lobot_api.logger.Logger import SETUP


def load_settings():
	"""Call function to initialize from file.
	Use this function after wiring up UI elements to settings."""
	DirectoryGlobal.settings_folder().mkdir(parents=True, exist_ok=True)
	found = settings.read(DirectoryGlobal.settings_data())
	if not found:
		logging.log(SETUP, f"No settings file.")


def parse_pk2item(s: str):
	from lobot_api.pk2.PK2Item import PK2Item
	args = s.split("|", 12)
	return PK2Item(args[0], args[1], args[2], args[3], args[4],
	               args[5], args[6], args[7], args[8], args[9],
	               args[10], args[11])


def parse_pk2skill(s: str):
	from lobot_api.pk2.PK2Skill import PK2Skill
	args = s.split("|", 9)
	return PK2Skill.PK2Skill_type_str(args[0], args[1], args[2], args[3], args[4],
	                args[5], args[6], args[7], args[8])


def parse_itemfilter(s: str):
	from lobot_api.structs.ItemFilter import ItemFilter
	args = [i == '1' or i == 'True' for i in s]
	return ItemFilter(args[0], args[1], args[2], args[3])


def parse_degreefilter(s: str):
	from lobot_api.structs.DegreeFilter import DegreeFilter
	args = int(s, 2)
	return DegreeFilter(args)


settings = ConfigParser(
	converters={
		'pk2item': parse_pk2item,
		'pk2skill': parse_pk2skill,
		'itemfilter': parse_itemfilter,
		'degreefilter': parse_degreefilter
	}
)

# initialize with defaults first then try and load saved values
settings['Login'] = {
	'UserName': '',
	'Password': '',
	'Character': '',
	'Autosave': 'True',
	'ClientPath': '',
	'DisplayCaptcha': '',
	'AutoStart': 'True',
	'AutoConnectSeconds': '10',
	'TypeClientOrClientless': 'True',
	'Autoreconnect': 'True',
	'ActionOnLogin': '0',
	'AutoSelect': '1',
}
settings['Town'] = {
	'HPPotsIndex': '0',
	'HPPotsQt': '0',
	'MPPotsIndex': '0',
	'MPPotsQt': '0',
	'UniPillsIndex': '0',
	'UniPillsQt': '0',
	'PurificationIndex': '0',
	'PurificationQt': '0',
	'AmmoIndex': '0',
	'AmmoQt': '0',
	'SpeedDrugIndex': '0',
	'SpeedDrugQt': '0',
	'return_scroll_index': '0',
	'ReturnScrollQt': '0',
	'HorseIndex': '0',
	'HorseQt': '0',
	'PetHPPotsIndex': '0',
	'PetHPPotsQt': '0',
	'PetPillsIndex': '0',
	'PetPillsQt': '0',
	'PetHGPQt': '0',
	'PetGrassOfLifeQt': '0',
	'VigorsIndex': '0',
	'VigorsQt': '0',
	'ZerkPotsIndex': '0',
	'ZerkPotsQt': '0'
}
settings['Script'] = {
	'TrainingScript': '',
	'QuestScript': '',
	'UseQuestScript': '0',
	'UseSpeedScroll': '0',
	'UseSpeedBuffOrPotion': '0',
	'SpeedBuff': '0',
	'SpeedDrug': '0',
	'UseReverseReturn': '0',
	'ReverseReturnOption': '0',
	'SwitchToShield': '0',
	'RideHorse': '0',
	'UseReturnScroll': '0',
	'RecordWalk': '1',
	'RecordSkills': '0',
	'RecordQuests': '0'
}
settings['Filter'] = {
	'PickOwnItems': 'True',
	'PickFreeItems': 'True',
	'Gold': 'False',
	# 'EventItems': 'True',
	'QuestItems': 'True',
	'WeaponElixir': '1100',
	'ShieldElixir': '1100',
	'ProtectorElixir': '1100',
	'AccessoryElixir': '1100',
	'AlchemyMaterial': '0010',
	'Grains': '1000',
	'Arrows': '0000',
	'Bolts': '0000',
	'Head': '1010',
	'Chest': '1010',
	'Hose': '1010',
	'Shoulder': '1010',
	'Glove': '1010',
	'Boots': '1010',
	'Necklace': '1010',
	'Earrings': '1010',
	'Ring': '1010',
	'EUHead': '1010',
	'EUChest': '1010',
	'EUHose': '1010',
	'EUShoulder': '1010',
	'EUGloves': '1010',
	'EUBoots': '1010',
	'EUNecklace': '1010',
	'EUEarrings': '1010',
	'EURing': '1010',
	'EUCrossbow': '1010',
	'EUClericStaff': '1010',
	'JadeStrength': '1010',
	'JadeIntelligence': '1010',
	'JadeMaster': '1010',
	'JadeStrikes': '1010',
	'JadeDiscipline': '1010',
	'JadePenetration': '1010',
	'JadeDodging': '1010',
	'JadeStamina': '1010',
	'JadeMagic': '1010',
	'JadeFogs': '1010',
	'JadeAir': '1010',
	'JadeFire': '1010',
	'JadeImmunity': '1010',
	'JadeRevival': '1010',
	'JadeSteady': '1010',
	'JadeLuck': '1010',
	'JadeTabletStrength': '1010',
	'JadeTabletIntelligence': '1010',
	'JadeTabletMaster': '1010',
	'JadeTabletStrikes': '1010',
	'JadeTabletDiscipline': '1010',
	'JadeTabletPenetration': '1010',
	'JadeTabletDodging': '1010',
	'JadeTabletStamina': '1010',
	'JadeTabletMagic': '1010',
	'JadeTabletFogs': '1010',
	'JadeTabletAir': '1010',
	'JadeTabletFire': '1010',
	'JadeTabletImmunity': '1010',
	'JadeTabletRevival': '1010',
	'JadeTabletSteady': '1010',
	'JadeTabletLuck': '1010',
	'RubyCourage': '1010',
	'RubyWarriors': '1010',
	'RubyPhilosophy': '1010',
	'RubyMeditation': '1010',
	'RubyChallenge': '1010',
	'RubyFocus': '1010',
	'RubyFlesh': '1010',
	'RubyLife': '1010',
	'RubyMind': '1010',
	'RubySpirit': '1010',
	'RubyDodging': '1010',
	'RubyAgility': '1010',
	'RubyTraining': '1010',
	'RubyPrayer': '1010',
	'RubyTabletCourage': '1010',
	'RubyTabletWarriors': '1010',
	'RubyTabletPhilosophy': '1010',
	'RubyTabletMeditation': '1010',
	'RubyTabletChallenge': '1010',
	'RubyTabletFocus': '1010',
	'RubyTabletFlesh': '1010',
	'RubyTabletLife': '1010',
	'RubyTabletMind': '1010',
	'RubyTabletSpirit': '1010',
	'RubyTabletDodging': '1010',
	'RubyTabletAgility': '1010',
	'RubyTabletTraining': '1010',
	'RubyTabletPrayer': '1010',
	'HPPots': '1000',
	'MPPots': '1000',
	'VigorPots': '1000',
	'UniversalPills': '1000',
	'PurificationPills': '1000',
	'ReturnScrolls': '1000',
	'COSItems': '1000',
	'Shield': '1010',
	'Sword': '1010',
	'Blade': '1010',
	'Spear': '1010',
	'Glaive': '1010',
	'Bow': '1010',
	'EUShield': '1010',
	'EUSword1H': '1010',
	'EUSword2H': '1010',
	'EUAxe': '1010',
	'EUDagger': '1010',
	'EUWarlockStaff': '1010',
	'EUWizardStaff': '1010',
	'EUHarp': '1010',
	'DegreesAll': '111000000000',
	'DegreesCH': '111000000000',
	'DegreesEU': '111000000000',
	'DegreesAlchemy': '111000000000'
}
settings['Training'] = {
	'AttackPrioritizeWeakest': 'True',
	'AttackSwitchIfPlayerAttacks': '',
	'AttackDefendTeam': '',
	'AttackFocusUnique': '',
	'AttackAutoAttackEvent': '',
	'warlock_dot_switch': 'True',
	'ZerkUnique': '',
	'ZerkGiant': 'True',
	'ZerkElite': 'True',
	'ZerkChampion': 'True',
	'ZerkPartyGiant': 'True',
	'ZerkPartyChampion': 'True',
	'ZerkPartyGeneral': 'True',
	'ZerkStrong': '',
	'ZerkQuest': '',
	'ZerkEvent': '',
	'ZerkUseWhenReady': '',
	'ZerkVSStrong': 'True',
	'ZerkAttackersMoreThanX': 'True',
	'ZerkAttackersX': '5',
	'IgnoreUnique': 'False',
	'IgnoreElite': 'False',
	'IgnoreGiant': 'False',
	'IgnoreChampion': 'False',
	'IgnoreGeneral': 'False',
	'IgnorePartyGiant': 'False',
	'IgnorePartyChampion': 'False',
	'IgnorePartyGeneral': 'False',
	'IgnoreStrong': 'False',
	'IgnoreQuest': 'False',
	'IgnoreEvent': 'False',
	'IgnoreDimensionPillar': 'False',
	'IgnoreCaveTrap': 'False',
	'IgnoreSnowSlave': 'False',
	'IgnoreEnvy': 'False',
	'IgnoreDontAttack': 'False',
	'IgnoreRemainIdle': 'False',
	'ReturnDead': 'True',
	'ReturnDeadMinutesMoreThan': 'False',
	'x_minutes': '2',
	'ReturnFull': 'True',
	'ReturnUniqueFound': 'False',
	'return_gm_found': 'False',
	'return_no_vigors': 'False',
	'return_no_universal_pills': 'False',
	'return_no_special_pills': 'False',
	'return_low_hp_pots': 'True',
	'ReturnHPPotsX': '5',
	'return_low_mp_pots': 'True',
	'ReturnMpPotsX': '5',
	'return_low_ammo': 'False',
	'ReturnAmmoX': '20',
	'ReturnLowWeaponDur': 'True',
	'ReturnWeaponDurX': '5',
	'ReturnBrokenItems': 'True',
	'ReturnBrokenItemsX': '2',
	'ReturnQuestsDone': 'False',
	'ReturnQuestsDoneX': '0',
	'TrainingUseArea2': 'True',
	'TrainingStayIdleOption': 'False',
	'AutoPotionsPreferGrains': 'True',
	'AutoPotionsLowHP': 'True',
	'AutoPotionsLowHPX': '50',
	'AutoPotionsLowVigorHP': 'True',
	'AutoPotionsLowVigorHPX': '20',
	'AutoPotionsLowMP': 'True',
	'AutoPotionsLowMPX': '30',
	'AutoPotionsLowVigorMP': 'False',
	'AutoPotionsLowVigorMPX': '10',
	'AutoPotionsLowPetHP': 'True',
	'AutoPotionsLowPetHPX': '50',
	'AutoPotionsLowPetHGP': 'True',
	'AutoPotionsLowPetHGPX': '20',
	'AutoPotionsPetBadStatus': 'True',
	'AutoPotionsLowTransportHP': 'True',
	'AutoPotionsLowTransportHPX': '50',
	'AutoPotionsTransportBadStatus': 'True',
	'RevivePet': 'True',
	'AutoPotionDelay': '1000',
	'AutoPotionBadStatusCure': 'True',
	'AutoPotionBadStatusUsePillOrSpell': 'False',
	'AutoPotionBadStatusSpell': '0',
	'AutoPotionCurseCure': 'True',
	'AutoPotionCurseUsePillOrSpell': 'False',
	'AutoPotionCurseSpell': '0',
	'TrainingArea1': '0,0,50',
	'TrainingArea2': '0,50,50'
}
settings['Skill'] = {
	'Imbue': '0',
	'SpeedBuff': '0',
	'HealSpell': '0',
	'ManaSpell': '0',
	'ResSpell': '0',
	'CycleSkills': 'False',
	'UseHeal': 'False',
	'UseHealPercentage': '50',
	'UseMPSpell': 'False',
	'UseMPSpellPercentage': '25',
	'UseRes': 'False',
	'SpeedBuffOrPotion': 'False',
	'BuffAllOrMembers': 'False',
	'SkillList': '',  # List[Skill],
	'BuffSkillsWeak': '',  # Dict[MonsterType,Buff],
	'BuffSkillsStrong': '',  # Dict[MonsterType,Buff],
	'MembersToBuff': '',  # Dict[str,Skill],
	'MembersBuffs': '',  # {{}},
	'Initiations': '',  # Dict[MonsterType,Skill],
	'General': '',  # Dict[MonsterType,Skill],
	'Finishers': '',  # Dict[MonsterType,Skill],
}
settings['Party'] = {
	'Title': '',  # LTP
	'Objective': '0',
	'MinLevel': '0',
	'MaxLevel': '80',
	'Type': '4',  # LTP
	'AutoInvite': 'False',
	'AutoJoin': 'False',
	'AutoAcceptInvite': 'True',
	'AutoReform': 'True',
	'MembersCanInvite': 'True',
	'OnlyMembers': 'True',
	'MemberList': ''
}
settings['Stall'] = {
	'ResumeTraining': '',
	'AutoAdd': ''
}
settings['Logging'] = {
	'Debug': '',
	'Information': '',
	'Warning': '',
	'Error': '',
	'Botting': '',
	'Script': '',
	'PK2': '',
	'Network': '',
	'Handler': '',
	'All': ''
}
settings['General'] = {
	# 'Login': '',
	# 'Town': '',
	# 'Script': '',
	# 'Filter': '',
	# 'Training': '',
	# 'Skills': '',
	# 'Party': '',
	# 'Stall': '',
	# 'Logging': '',
	'UpgradeRequired': 'False',
}

Path(DirectoryGlobal.settings_folder()).mkdir(parents=True, exist_ok=True)
found = settings.read(DirectoryGlobal.settings_data())
if not found:
	logging.log(SETUP, f"Init settings file.")
