from typing import List

from PyQt5.QtCore import QObject, pyqtProperty, pyqtSignal

from lobot_api.connection.Shard import Shard
from lobot_api.globals.settings.Settings import settings


class LoginSettings(QObject):
	__instance__ = None
	sig_locale = pyqtSignal(str)
	sig_username = pyqtSignal(str)
	sig_password = pyqtSignal(str)
	sig_charName = pyqtSignal(str)
	sig_loginClientDirectoryPath = pyqtSignal(str)
	sig_autoconnect = pyqtSignal(bool)
	sig_autoconnectX = pyqtSignal(int)
	sig_autoSelect = pyqtSignal(bool)
	sig_actionOnLogin = pyqtSignal(bool)
	sig_clientOrClientless = pyqtSignal(bool)
	sig_autoSaveSettings = pyqtSignal(bool)

	@staticmethod
	def instance():
		if LoginSettings.__instance__ is None:
			LoginSettings.__instance__ = LoginSettings()
		return LoginSettings.__instance__

	def __init__(self):
		super().__init__()
		self.can_login: bool = False
		self.can_select_char: bool = False
		self.session_id = 0x00
		self._locale_ = 22  # 0x16
		self.version = 0xE4  # version 228
		self.shard_id = 0x40
		self.shard_list: List[Shard] = []
		self.characters_available: List[str] = []

	@pyqtProperty('QString', notify=sig_locale)
	def locale(self) -> int:
		return int(self._locale_)

	@locale.setter
	def locale(self, value):
		if self._locale_ != value:
			self.sig_locale.emit(str(value))
			self._locale_ = value

	@pyqtProperty('QString', notify=sig_username)
	def username(self):
		return settings['Login']['UserName']

	@username.setter
	def username(self, value):
		settings['Login']['UserName'] = value

	@pyqtProperty('QString')
	def password(self):
		return settings['Login']['Password']

	@password.setter
	def password(self, value):
		settings['Login']['Password'] = value

	@pyqtProperty('QString', notify=sig_charName)
	def char_name(self):
		return settings['Login']['Character']

	@char_name.setter
	def char_name(self, value):
		if settings['Login']['Character'] != value:
			self.sig_charName.emit(value)
			settings['Login']['Character'] = value

	@pyqtProperty('QString', notify=sig_loginClientDirectoryPath)
	def login_client_directory_path(self):
		return settings['Login']['ClientPath']

	@login_client_directory_path.setter
	def login_client_directory_path(self, value):
		settings['Login']['ClientPath'] = value  # LoginClientPath

	@pyqtProperty('bool', notify=sig_autoSelect)
	def auto_select(self) -> bool:
		return settings['Login']['AutoSelect'] == 'True'

	@auto_select.setter
	def auto_select(self, value):
		settings['Login']['AutoSelect'] = str(value)

	@pyqtProperty('bool', notify=sig_autoconnect)
	def auto_connect(self) -> bool:
		return settings['Login']['Autoreconnect'] == 'True'

	@auto_connect.setter
	def auto_connect(self, value):
		settings['Login']['Autoreconnect'] = str(value)

	@pyqtProperty('QString', notify=sig_autoconnectX)
	def auto_connect_seconds(self) -> int:
		return int(settings['Login']['AutoConnectSeconds'])

	@auto_connect_seconds.setter
	def auto_connect_seconds(self, value):
		settings['Login']['AutoConnectSeconds'] = value

	@pyqtProperty('int', notify=sig_actionOnLogin)
	def action_on_login(self) -> int:
		return int(settings['Login']['ActionOnLogin'])

	@action_on_login.setter
	def action_on_login(self, value):
		settings['Login']['ActionOnLogin'] = str(value)

	@pyqtProperty('bool', notify=sig_clientOrClientless)
	def client_or_clientless(self) -> bool:
		return settings['Login']['TypeClientOrClientless'] == 'True'

	@client_or_clientless.setter
	def client_or_clientless(self, value):
		self.sig_clientOrClientless.emit(value == "True")
		settings['Login']['TypeClientOrClientless'] = str(value)

	@pyqtProperty('bool')
	def autostart_or_manual(self) -> bool:
		return settings['Login']['AutoStart'] == 'True'

	@autostart_or_manual.setter
	def autostart_or_manual(self, value):
		settings['Login']['AutoStart'] = value

	@pyqtProperty('bool')
	def auto_save_settings(self) -> bool:
		return settings['Login']['AutosaveSettings'] == 'True'

	@auto_save_settings.setter
	def auto_save_settings(self, value):
		settings['Login']['AutosaveSettings'] = value
