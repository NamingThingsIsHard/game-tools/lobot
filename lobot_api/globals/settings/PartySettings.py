﻿from ctypes import c_byte
from typing import Dict

from PyQt5.QtCore import pyqtSignal, pyqtProperty, QObject

from lobot_api.globals.settings.Settings import settings
from lobot_api.structs.PartyObjective import PartyObjective
from lobot_api.structs.PartyType import PartyType


class PartySettings(QObject):
	__instance__ = None
	sig_title = pyqtSignal(str)
	sig_type = pyqtSignal(int)
	sig_objective = pyqtSignal(int)
	sig_only_members = pyqtSignal(bool)
	sig_members_can_invite = pyqtSignal(bool)
	sig_auto_invite = pyqtSignal(bool)
	sig_auto_accept_invite = pyqtSignal(bool)
	sig_auto_reform = pyqtSignal(bool)
	sig_min_level = pyqtSignal(int)
	sig_max_level = pyqtSignal(int)
	sig_auto_join = pyqtSignal(bool)
	sig_memberlist = pyqtSignal(list)

	default_party_entries: Dict[PartyObjective, str] = {
		PartyObjective.Hunting: "For open hunting in Silkroad.",
		PartyObjective.Quest: "When will the quests end on the Silkroad?",
		PartyObjective.Thief: "No need for blood or tears for thieves! Robbery is the only way to go!",
		PartyObjective.TradeHunter: "Until the day the Silkroad is all covered with money!"
	}

	@staticmethod
	def instance():
		if PartySettings.__instance__ is None:
			PartySettings.__instance__ = PartySettings()
		return PartySettings.__instance__

	@property  # ('int', notify=sig_objective)
	def objective(self) -> PartyObjective:
		return PartyObjective(int(settings['Party']['Objective']))

	@objective.setter
	def objective(self, value):
		if value != int(settings['Party']['Objective']):
			self.sig_objective.emit(value)
			settings['Party']['Objective'] = str(value)

	@pyqtProperty('QString', notify=sig_title)
	def title(self):
		return settings['Party']['Title'] \
			if settings['Party']['Title'] \
			else self.default_party_entries[self.objective]

	@title.setter
	def title(self, value):
		if value != settings['Party']['Title']:
			self.sig_title.emit(value)
			settings['Party']['Title'] = value

	@pyqtProperty('bool', notify=sig_type)
	def type(self) -> PartyType:
		return PartyType(int(settings['Party']['Type']))

	@type.setter
	def type(self, value):
		if value != int(settings['Party']['Type']):
			self.sig_type.emit(value)
			settings['Party']['Type'] = str(value)

	@pyqtProperty('bool', notify=sig_members_can_invite)
	def members_can_invite(self) -> bool:
		return settings['Party']['MembersCanInvite'] == 'True'

	@members_can_invite.setter
	def members_can_invite(self, value):
		if (settings['Party']['MembersCanInvite'] == 'True') != value:
			self.sig_members_can_invite.emit(bool(value))
			settings['Party']['MembersCanInvite'] = str(value)

	@pyqtProperty('bool', notify=sig_only_members)
	def only_members(self) -> bool:
		return settings['Party']['OnlyMembers'] == 'True'

	@only_members.setter
	def only_members(self, value):
		if (settings['Party']['OnlyMembers'] == 'True') != value:
			self.sig_only_members.emit(bool(value))
			settings['Party']['OnlyMembers'] = str(value)

	@pyqtProperty('bool', notify=sig_auto_invite)
	def auto_invite(self) -> bool:
		return settings['Party']['AutoInvite'] == 'True'

	@auto_invite.setter
	def auto_invite(self, value):
		if (settings['Party']['AutoInvite'] == 'True') != value:
			self.sig_auto_invite.emit(bool(value))
			settings['Party']['AutoInvite'] = str(value)

	@pyqtProperty('bool', notify=sig_auto_accept_invite)
	def auto_accept_invite(self) -> bool:
		return settings['Party']['AutoAcceptInvite'] == 'True'

	@auto_accept_invite.setter
	def auto_accept_invite(self, value):
		if (settings['Party']['AutoAcceptInvite'] == 'True') != value:
			self.sig_auto_accept_invite.emit(bool(value))
			settings['Party']['AutoAcceptInvite'] = str(value)

	@pyqtProperty('bool', notify=sig_auto_reform)
	def auto_reform(self) -> bool:
		return settings['Party']['AutoReform'] == 'True'

	@auto_reform.setter
	def auto_reform(self, value):
		if (settings['Party']['AutoReform'] == 'True') != value:
			settings['Party']['AutoReform'] = str(value)
			self.sig_auto_reform.emit(bool(value))

	@pyqtProperty('bool', notify=sig_auto_join)
	def auto_join(self) -> bool:
		return settings['Party']['AutoJoin'] == 'True'

	@auto_join.setter
	def auto_join(self, value):
		if (settings['Party']['AutoJoin'] == 'True') != value:
			settings['Party']['AutoJoin'] = str(value)
			self.sig_auto_join.emit(bool(value))

	@pyqtProperty('int', notify=sig_min_level)
	def min_level(self):
		return int(settings['Party']['MinLevel'])

	@min_level.setter
	def min_level(self, value):
		if int(settings['Party']['MinLevel']) != value:
			self.sig_min_level.emit(int(value))
			settings['Party']['MinLevel'] = str(value)

	@pyqtProperty('int', notify=sig_max_level)
	def max_level(self) -> c_byte:
		return int(settings['Party']['MaxLevel'])

	@max_level.setter
	def max_level(self, value):
		if int(settings['Party']['MaxLevel']) != value:
			self.sig_max_level.emit(int(value))
			settings['Party']['MaxLevel'] = str(value)

	@pyqtProperty('QVariant', notify=sig_memberlist)
	def member_list(self):
		return settings['Party']['MemberList'].split(',')

	@member_list.setter
	def member_list(self, value: str):
		if settings['Party']['MemberList'] != value:
			self.sig_memberlist.emit(value.split(","))
			settings['Party']['MemberList'] = value

# on_toggled_MembersCanInvite: void(e: BoolEventArgs)
# on_toggled_AutoAcceptInvite: void(e: BoolEventArgs)
# on_toggled_AutoReform: void(e: BoolEventArgs)
# on_toggled_toggledAutoJoin: void(e: BoolEventArgs)
