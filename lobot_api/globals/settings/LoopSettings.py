from typing import List

from PyQt5.QtCore import pyqtSignal, QObject

from lobot_api.binding import LobotProperty
from lobot_api.globals.settings.Settings import settings


class ScriptSettings:
	RecordedScript: List[str] = []

	TrainingScriptPath = LobotProperty(settings.get, ['Script', 'TrainingScript'], settings.set, ['Script', 'TrainingScript'])
	QuestScriptPath = LobotProperty(settings.get, ['Script', 'QuestScript'], settings.set, ['Script', 'QuestScript'])
	UseQuestScript = LobotProperty(settings.getboolean, ['Script', 'UseQuestScript'], settings.set, ['Script', 'UseQuestScript'])

	SpeedBuffOrPotion = LobotProperty(settings.getboolean, ['Script', 'UseSpeedBuffOrPotion'], settings.set, ['Script', 'UseSpeedBuffOrPotion'])
	speed_buff_id = LobotProperty(settings.getint, ['Script', 'SpeedBuff'], settings.set, ['Script', 'SpeedBuff'])
	speed_potion_id = LobotProperty(settings.getint, ['Script', 'SpeedDrug'], settings.set, ['Script', 'SpeedDrug'])
	UseSpeedScroll = LobotProperty(settings.getboolean, ['Script', 'UseSpeedScroll'], settings.set, ['Script', 'UseSpeedScroll'])
	UseReverseReturn = LobotProperty(settings.getboolean, ['Script', 'UseReturnScroll'], settings.set, ['Script', 'UseReturnScroll'])
	ReverseReturnOption = LobotProperty(settings.getint, ['Script', 'ReverseReturnOption'], settings.set, ['Script', 'ReverseReturnOption'])
	SwitchToShield = LobotProperty(settings.getboolean, ['Script', 'SwitchToShield'], settings.set, ['Script', 'SwitchToShield'])
	RideHorse = LobotProperty(settings.getboolean, ['Script', 'RideHorse'], settings.set, ['Script', 'RideHorse'])
	UseReturnScroll = LobotProperty(settings.getboolean, ['Script', 'UseReturnScroll'], settings.set, ['Script', 'UseReturnScroll'])
	RecordWalk = LobotProperty(settings.getboolean, ['Script', 'RecordWalk'], settings.set, ['Script', 'RecordWalk'])
	RecordSkills = LobotProperty(settings.getboolean, ['Script', 'RecordSkills'], settings.set, ['Script', 'RecordSkills'])
	RecordQuests = LobotProperty(settings.getboolean, ['Script', 'RecordQuests'], settings.set, ['Script', 'RecordQuests'])


class ShoppingSettings(QObject):
	__instance__ = None

	@staticmethod
	def instance():
		if ShoppingSettings.__instance__ is None:
			ShoppingSettings.__instance__ = ShoppingSettings()
		return ShoppingSettings.__instance__

	sig_HPPotionIndex = pyqtSignal(int)
	sig_HPPotion = pyqtSignal(int)
	sig_MPPotionIndex = pyqtSignal(int)
	sig_MPPotion = pyqtSignal(int)
	sig_VigorPotionIndex = pyqtSignal(int)
	sig_VigorPotion = pyqtSignal(int)
	sig_UniversalPillIndex = pyqtSignal(int)
	sig_UniversalPill = pyqtSignal(int)
	sig_SpecialPillIndex = pyqtSignal(int)
	sig_SpecialPill = pyqtSignal(int)

	sig_ProjectileIndex = pyqtSignal(int)
	sig_Projectile = pyqtSignal(int)
	sig_SpeedDrugIndex = pyqtSignal(int)
	sig_SpeedDrug = pyqtSignal(int)
	sig_ReturnScrollIndex = pyqtSignal(int)
	sig_ReturnScroll = pyqtSignal(int)
	sig_TransportIndex = pyqtSignal(int)
	sig_Transport = pyqtSignal(int)
	sig_ZerkPotsIndex = pyqtSignal(int)
	sig_ZerkPots = pyqtSignal(int)

	sig_PetHPPotionIndex = pyqtSignal(int)
	sig_PetHPPotion = pyqtSignal(int)
	sig_PetPillIndex = pyqtSignal(int)
	sig_PetPill = pyqtSignal(int)
	sig_PetHGP = pyqtSignal(int)
	sig_PetGrassOfLife = pyqtSignal(int)

	@property
	def hp_potion_index(self):
		return int(settings['Town']['HPPotsIndex'])

	@hp_potion_index.setter
	def hp_potion_index(self, value):
		if value != settings['Town']['HPPotsIndex']:
			settings['Town']['HPPotsIndex'] = str(value)
			self.sig_HPPotionIndex.emit(value)

	@property
	def hp_potion_type(self) -> int:
		return 4

	@property
	def mp_potion_index(self):
		return int(settings['Town']['MPPotsIndex'])

	@mp_potion_index.setter
	def mp_potion_index(self, value):
		if value != settings['Town']['MPPotsIndex']:
			settings['Town']['MPPotsIndex'] = str(value)
			self.sig_MPPotionIndex.emit(value)

	@property
	def mp_potion_type(self) -> int:
		return 11

	@property
	def vigor_potion_index(self):
		return int(settings['Town']['VigorsIndex'])

	@vigor_potion_index.setter
	def vigor_potion_index(self, value):
		if value != settings['Town']['VigorsIndex']:
			settings['Town']['VigorsIndex'] = str(value)
			self.sig_VigorPotionIndex.emit(value)

	@property
	def vigor_potion_type(self) -> int:
		return 18

	@property
	def universal_pill_index(self):
		return int(settings['Town']['UniPillsIndex'])

	@universal_pill_index.setter
	def universal_pill_index(self, value):
		if value != settings['Town']['UniPillsIndex']:
			settings['Town']['UniPillsIndex'] = str(value)
			self.sig_UniversalPillIndex.emit(value)

	@property
	def universal_pill_type(self) -> int:
		return 55

	@property
	def purification_pill_index(self):
		return int(settings['Town']['PurificationIndex'])

	@purification_pill_index.setter
	def purification_pill_index(self, value):
		if value != settings['Town']['PurificationIndex']:
			settings['Town']['PurificationIndex'] = str(value)
			self.sig_SpecialPillIndex.emit(value)

	@property
	def purification_pill_type(self) -> int:
		return 10374

	@property
	def projectile_index(self) -> int:
		return int(settings['Town']['AmmoIndex'])

	@projectile_index.setter
	def projectile_index(self, value):
		if value != settings['Town']['AmmoIndex']:
			settings['Town']['AmmoIndex'] = str(value)
			self.sig_ProjectileIndex.emit(value)

	@property
	def projectile_type(self) -> int:
		return 62

	@property
	def speed_pot_index(self) -> int:
		return int(settings['Town']['SpeedDrugIndex'])

	@speed_pot_index.setter
	def speed_pot_index(self, value):
		if value != settings['Town']['SpeedDrugIndex']:
			settings['Town']['SpeedDrugIndex'] = str(value)
			self.sig_SpeedDrugIndex.emit(value)

	@property
	def speed_pot_type(self) -> int:
		return 7098

	@property
	def return_scroll_index(self) -> int:
		return int(settings['Town']['return_scroll_index'])

	@return_scroll_index.setter
	def return_scroll_index(self, value):
		if value != settings['Town']['return_scroll_index']:
			settings['Town']['return_scroll_index'] = str(value)
			self.sig_ReturnScrollIndex.emit(value)

	@property
	def return_scroll_type(self) -> int:
		return 61

	@property
	def transport_index(self) -> int:
		return int(settings['Town']['HorseIndex'])

	@transport_index.setter
	def transport_index(self, value):
		if value != settings['Town']['HorseIndex']:
			settings['Town']['HorseIndex'] = str(value)
			self.sig_TransportIndex.emit(value)

	@property
	def transport_type(self) -> int:
		return 2137

	@property
	def zerk_potion_index(self) -> int:
		return int(settings['Town']['ZerkPotsIndex'])

	@zerk_potion_index.setter
	def zerk_potion_index(self, value):
		if value != settings['Town']['ZerkPotsIndex']:
			settings['Town']['ZerkPotsIndex'] = str(value)
			self.sig_ZerkPotsIndex.emit(value)

	@property
	def zerk_potion_type(self) -> int:
		return 24993

	@property
	def hp_potion_quantity(self) -> int:
		return int(settings['Town']['HPPotsQt'])

	@hp_potion_quantity.setter
	def hp_potion_quantity(self, value):
		if value != settings['Town']['HPPotsQt']:
			settings['Town']['HPPotsQt'] = str(value)
			self.sig_HPPotion.emit(value)

	@property
	def mp_potion_quantity(self) -> int:
		return int(settings['Town']['MPPotsQt'])

	@mp_potion_quantity.setter
	def mp_potion_quantity(self, value):
		if value != settings['Town']['MPPotsQt']:
			settings['Town']['MPPotsQt'] = str(value)
			self.sig_MPPotion.emit(value)

	@property
	def vigor_potion_quantity(self) -> int:
		return int(settings['Town']['VigorsQt'])

	@vigor_potion_quantity.setter
	def vigor_potion_quantity(self, value):
		if value != settings['Town']['VigorsQt']:
			settings['Town']['VigorsQt'] = str(value)
			self.sig_VigorPotion.emit(value)

	@property
	def universal_pill_quantity(self) -> int:
		return int(settings['Town']['UniPillsQt'])

	@universal_pill_quantity.setter
	def universal_pill_quantity(self, value):
		if value != settings['Town']['UniPillsQt']:
			settings['Town']['UniPillsQt'] = str(value)
			self.sig_UniversalPill.emit(value)

	@property
	def purification_pill_quantity(self) -> int:
		return int(settings['Town']['PurificationQt'])

	@purification_pill_quantity.setter
	def purification_pill_quantity(self, value):
		if value != settings['Town']['PurificationQt']:
			settings['Town']['PurificationQt'] = str(value)
			self.sig_SpecialPill.emit(value)

	@property
	def projectile_quantity(self) -> int:
		return int(settings['Town']['AmmoQt'])

	@projectile_quantity.setter
	def projectile_quantity(self, value):
		if value != settings['Town']['AmmoQt']:
			settings['Town']['AmmoQt'] = str(value)
			self.sig_Projectile.emit(value)

	@property
	def speed_pot_quantity(self) -> int:
		return int(settings['Town']['SpeedDrugQt'])

	@speed_pot_quantity.setter
	def speed_pot_quantity(self, value):
		if value != settings['Town']['SpeedDrugQt']:
			settings['Town']['SpeedDrugQt'] = str(value)
			self.sig_SpeedDrug.emit(value)

	@property
	def return_scroll_quantity(self) -> int:
		return int(settings['Town']['ReturnScrollQt'])

	@return_scroll_quantity.setter
	def return_scroll_quantity(self, value):
		if value != settings['Town']['ReturnScrollQt']:
			settings['Town']['ReturnScrollQt'] = str(value)
			self.sig_ReturnScroll.emit(value)

	@property
	def transport_quantity(self) -> int:
		return int(settings['Town']['HorseQt'])

	@transport_quantity.setter
	def transport_quantity(self, value):
		if value != settings['Town']['HorseQt']:
			settings['Town']['HorseQt'] = str(value)
			self.sig_Transport.emit(value)

	@property
	def zerk_potion_quantity(self) -> int:
		return int(settings['Town']['ZerkPotsQt'])

	@zerk_potion_quantity.setter
	def zerk_potion_quantity(self, value):
		if value != settings['Town']['ZerkPotsQt']:
			settings['Town']['ZerkPotsQt'] = str(value)
			self.sig_ZerkPots.emit(value)

	@property
	def pet_hp_potion_index(self):
		return int(settings['Town']['PetHPPotsIndex'])

	@pet_hp_potion_index.setter
	def pet_hp_potion_index(self, value):
		if value != settings['Town']['PetHPPotsIndex']:
			settings['Town']['PetHPPotsIndex'] = str(value)
			self.sig_PetHPPotionIndex.emit(value)

	@property
	def pet_hp_potion_type(self) -> int:
		return 2143

	@property
	def pet_pill_index(self) -> int:
		return int(settings['Town']['PetPillsIndex'])

	@pet_pill_index.setter
	def pet_pill_index(self, value):
		if value != settings['Town']['PetPillsIndex']:
			settings['Town']['PetPillsIndex'] = str(value)
			self.sig_PetPillIndex.emit(value)

	@property
	def pet_pill_type(self) -> int:
		return 9226

	@property
	def pethgp_id(self) -> int:
		return 7553

	@property
	def petgrassoflife_id(self) -> int:
		return 7552

	@property
	def pet_hp_potion_quantity(self) -> int:
		return int(settings['Town']['PetHPPotsQt'])

	@pet_hp_potion_quantity.setter
	def pet_hp_potion_quantity(self, value):
		if value != settings['Town']['PetHPPotsQt']:
			settings['Town']['PetHPPotsQt'] = str(value)
			self.sig_PetHPPotion.emit(value)

	@property
	def pet_pill_quantity(self) -> int:
		return int(settings['Town']['PetPillsQt'])

	@pet_pill_quantity.setter
	def pet_pill_quantity(self, value):
		if value != settings['Town']['PetPillsQt']:
			settings['Town']['PetPillsQt'] = str(value)
			self.sig_PetPill.emit(value)

	@property
	def hgp_potion_quantity(self) -> int:
		return int(settings['Town']['PetHGPQt'])

	@hgp_potion_quantity.setter
	def hgp_potion_quantity(self, value):
		if value != settings['Town']['PetHGPQt']:
			settings['Town']['PetHGPQt'] = str(value)
			self.sig_PetHGP.emit(value)

	@property
	def grass_of_life_quantity(self) -> int:
		return int(settings['Town']['PetGrassOfLifeQt'])

	@grass_of_life_quantity.setter
	def grass_of_life_quantity(self, value):
		if value != settings['Town']['PetGrassOfLifeQt']:
			settings['Town']['PetGrassOfLifeQt'] = str(value)
			self.sig_PetGrassOfLife.emit(value)
