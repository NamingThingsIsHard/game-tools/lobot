from typing import List

from lobot_api.packets.alchemy.AlchemyArgs import AlchemyArgs
from lobot_api.structs.Item import Equipment


class AlchemySettings:
	SelectedItem: Equipment
	Log: List[str] = []
	PlusGoal: int = 1
	AutoBuyLuckyPowder: bool = False
	AutoSwitchToLuckyAvatarDress = False
	AutoGetElixirsFromStorage = False
	AutoGetMaterialsFromStorage = False

	def handle_reinforce(self, e: AlchemyArgs):
		out_put: str = ''
		if e.success:
			if e.opt_level == AlchemySettings.PlusGoal:
				out_put = f"Enhancement Goal {e.opt_level} reached "
			else:
				out_put = f"Success!  {e.item_string}"
		else:
			out_put = f"Failure: {e.item_string}"
		# logging.log(ALCHEMY, output)
		AlchemySettings.Log.append(out_put)
