from enum import Enum
from typing import Dict, List, Tuple, Optional

from lobot_api.Bot import Bot
from lobot_api.structs.CharData import Skill, CharData
from lobot_api.structs.ISpawnObject import ISpawnObject
from lobot_api.structs.Item import PetScroll, PetStatus
from lobot_api.structs.ItemDrop import ItemDrop
from lobot_api.structs.Monster import Monster
from lobot_api.structs.Pet import Pet
from lobot_api.structs.Portal import Portal


class SpawnList(Enum):  # : int
	none = -1
	Drops = 0
	MonsterSpawns = 1
	NPCs = 2
	Portals = 3
	Players = 4
	Pets = 5


class SpawnListsGlobal:
	AllSpawnEntries: Dict[int, SpawnList] = {}
	Drops: Dict[int, ItemDrop] = {}
	SpawnBuffs: Dict[int, int] = {}
	# NPCs
	Players: Dict[int, CharData] = {}  # observable
	MonsterSpawns: Dict[int, Monster] = {}  # observable
	NPCSpawns: Dict[int, Monster] = {}  # observable
	Pets: Dict[int, Pet] = {}
	Portals: Dict[int, Portal] = {}  # observable
	Skills: List[Skill] = []  # observable

	@staticmethod
	def remove_all_spawn_entries():
		keys: List[int] = list(SpawnListsGlobal.AllSpawnEntries.keys())
		for key in keys:
			SpawnListsGlobal.remove_from_bot_list(key, SpawnListsGlobal.AllSpawnEntries[key])

	@staticmethod
	def __access_bot_list__(unique_id: int, bot_list: SpawnList) -> Tuple[bool, ISpawnObject]:
		""" ISpawnType: out spawn"""
		# switch (list)
		if bot_list == SpawnList.none:
			spawn = None
		elif bot_list == SpawnList.Drops:
			spawn = SpawnListsGlobal.Drops.get(unique_id, None)
		elif bot_list == SpawnList.MonsterSpawns:
			spawn = SpawnListsGlobal.MonsterSpawns.get(unique_id, None)
		elif bot_list == SpawnList.NPCs:
			spawn = SpawnListsGlobal.NPCSpawns.get(unique_id, None)
		elif bot_list == SpawnList.Portals:
			spawn = SpawnListsGlobal.Portals.get(unique_id, None)
		elif bot_list == SpawnList.Players:
			spawn = SpawnListsGlobal.Players.get(unique_id, None)
		elif bot_list == SpawnList.Pets:
			spawn = SpawnListsGlobal.Players.get(unique_id, None)
		else:
			spawn = None

		return spawn is not None, spawn

	@staticmethod
	def try_get_spawn_object(unique_id: int) -> Tuple[bool, Optional[ISpawnObject]]:
		"""out ISpawnType spawn"""
		list_type = SpawnListsGlobal.AllSpawnEntries.get(unique_id, None)
		if list_type:
			is_out, result_spawn = SpawnListsGlobal.__access_bot_list__(unique_id, list_type)
			return is_out, result_spawn  # sometimes despawn during AccessBotList[str] can cause a None pointer to return as True
		else:
			return False, None

	@staticmethod
	def remove_from_bot_list(object_id: int, list_number: SpawnList):
		# switch (listNumber)
		if list_number == SpawnList.none:
			return
		elif list_number == SpawnList.Drops:
			SpawnListsGlobal.Drops.pop(object_id)
			SpawnListsGlobal.AllSpawnEntries.pop(object_id)
		elif list_number == SpawnList.MonsterSpawns:
			SpawnListsGlobal.MonsterSpawns.pop(object_id)
			SpawnListsGlobal.AllSpawnEntries.pop(object_id)
		elif list_number == SpawnList.NPCs:
			SpawnListsGlobal.NPCSpawns.pop(object_id)
			SpawnListsGlobal.AllSpawnEntries.pop(object_id)
		elif list_number == SpawnList.Portals:
			SpawnListsGlobal.Portals.pop(object_id)
			SpawnListsGlobal.AllSpawnEntries.pop(object_id)
		elif list_number == SpawnList.Players:
			SpawnListsGlobal.Players.pop(object_id)
			SpawnListsGlobal.AllSpawnEntries.pop(object_id)
		elif list_number == SpawnList.Pets:
			if Bot.pickup_pet.unique_id == object_id:
				Bot.pickup_pet.unique_id = 0
			elif Bot.growth_pet.unique_id == object_id:  # Make# set to "Unsummon" before removing pet
				value = Bot.char_data.inventory.items.get(Bot.growth_pet.ScrollSlot, None)
				if isinstance(value, PetScroll) and value.status is not PetStatus.Summoned:
					Bot.growth_pet.set_from_pet(Pet())
			elif Bot.transport and Bot.transport.unique_id == object_id:
				Bot.transport.unique_id = 0
			SpawnListsGlobal.Pets.pop(object_id)
			SpawnListsGlobal.AllSpawnEntries.pop(object_id)
		else:
			pass
