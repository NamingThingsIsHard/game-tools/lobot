from ctypes import c_byte
from typing import Tuple, List

from lobot_api.Bot import Bot
from lobot_api.globals.settings.LoopSettings import ShoppingSettings
from lobot_api.pk2.PK2Item import PK2ItemType
from lobot_api.structs.Item import Item


def hp_pots() -> List[Tuple[c_byte, Item]]:
	return [(key, value) for key, value in Bot.char_data.inventory.items.items() if
	        value.type == PK2ItemType.HPPot]


def hp_grains() -> List[Tuple[c_byte, Item]]:
	return [(key, value) for key, value in Bot.char_data.inventory.items.items() if
	        value.type == PK2ItemType.HPGrain and "HP_SPOTION" in value.long_id]


def mp_pots() -> List[Tuple[c_byte, Item]]:
	return [(key, value) for key, value in Bot.char_data.inventory.items.items() if
	        value.type == PK2ItemType.MPPot]


def mp_grains() -> List[Tuple[c_byte, Item]]:
	return [(key, value) for key, value in Bot.char_data.inventory.items.items() if
	        value.type == PK2ItemType.MPGrain in value.long_id]


def vigors() -> List[Tuple[c_byte, Item]]:
	return [(key, value) for key, value in Bot.char_data.inventory.items.items() if
	        value.type == PK2ItemType.VigorPot]


def vigor_grains() -> List[Tuple[c_byte, Item]]:
	return [(key, value) for key, value in Bot.char_data.inventory.items.items() if
	        value.type == PK2ItemType.VigorGrain in value.long_id]


def universal_pills() -> List[Tuple[c_byte, Item]]:
	return [(key, value) for key, value in Bot.char_data.inventory.items.items() if
	        value.type == PK2ItemType.UniversalPill]


def special_pills() -> List[Tuple[c_byte, Item]]:
	return [(key, value) for key, value in Bot.char_data.inventory.items.items() if
	        value.type == PK2ItemType.PurificationPill]


def ammo() -> List[Tuple[int, Item]]:
	pk2_type = PK2ItemType.Arrow if ShoppingSettings.ProjectileType == 62 else PK2ItemType.Bolt
	return [value.type is pk2_type for key, value in
	        {**Bot.char_data.inventory.equipment, **Bot.char_data.inventory.items}]


def speed_pots() -> List[Tuple[c_byte, Item]]:
	return [(key, value) for key, value in Bot.char_data.inventory.items.items() if
	        value.type == PK2ItemType.BuffScroll in value.long_id]


def return_scrolls() -> List[Tuple[c_byte, Item]]:
	return [(key, value) for key, value in Bot.char_data.inventory.items.items() if
	        value.type == PK2ItemType.ReturnScroll]


def vehicles() -> List[Tuple[c_byte, Item]]:
	return [(key, value) for key, value in Bot.char_data.inventory.items.items() if
	        value.type == PK2ItemType.PetVehicle]


def pet_hp_pots() -> List[Tuple[c_byte, Item]]:
	return [(key, value) for key, value in Bot.char_data.inventory.items.items() if
	        value.type == PK2ItemType.PetHPPotion]


def pet_pills() -> List[Tuple[c_byte, Item]]:
	return [(key, value) for key, value in Bot.char_data.inventory.items.items() if
	        value.type == PK2ItemType.PetPill]


def HGPs() -> List[Tuple[c_byte, Item]]:
	return [(key, value) for key, value in Bot.char_data.inventory.items.items() if
	        value.type == PK2ItemType.HGPPotion]


def grass_of_life() -> List[Tuple[c_byte, Item]]:
	return [(key, value) for key, value in Bot.char_data.inventory.items.items() if
	        value.type == PK2ItemType.GrassOfLife]
