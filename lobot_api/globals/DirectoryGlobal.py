﻿import os
from pathlib import Path


class DirectoryGlobal:
	@staticmethod
	def cwd() -> Path: return Path(os.path.dirname(os.path.realpath(__file__))).parent.parent

	@staticmethod
	def data_folder() -> Path: return DirectoryGlobal.cwd() / "data"

	@staticmethod
	def map_folder() -> Path: return DirectoryGlobal.data_folder() / "map"

	@staticmethod
	def nav_mesh_folder() -> Path: return DirectoryGlobal.data_folder() / "navMesh"

	@staticmethod
	def settings_folder() -> Path: return DirectoryGlobal.data_folder() / "settings"

	@staticmethod
	def log_folder() -> Path: return DirectoryGlobal.data_folder() / "log"

	@staticmethod
	def script_folder() -> Path: return DirectoryGlobal.data_folder() / "scripts"

	@staticmethod
	def log_data() -> Path: return DirectoryGlobal.log_folder() / "log.txt"

	@staticmethod
	def name_data() -> Path: return DirectoryGlobal.data_folder() / "names.txt"

	@staticmethod
	def item_data() -> Path: return DirectoryGlobal.data_folder() / "items.txt"

	@staticmethod
	def level_data() -> Path: return DirectoryGlobal.data_folder() / "levels.txt"

	@staticmethod
	def mag_param_data() -> Path: return DirectoryGlobal.data_folder() / "magoptions.txt"

	@staticmethod
	def npc_data() -> Path: return DirectoryGlobal.data_folder() / "npcs.txt"

	@staticmethod
	def shop_data() -> Path: return DirectoryGlobal.data_folder() / "shops.txt"

	@staticmethod
	def skill_data() -> Path: return DirectoryGlobal.data_folder() / "skills.txt"

	@staticmethod
	def teleporter_data() -> Path: return DirectoryGlobal.data_folder() / "teleporters.txt"

	@staticmethod
	def settings_data() -> Path: return DirectoryGlobal.settings_folder() / "settings.ini"
