import ctypes


class iovec(ctypes.Structure):
	"""
	The input/output vector for reading and writing virtual memory in GNU/Linux using process_vm_readv and process_vm_writev.
	"""
	_fields_ = [("iov_base", ctypes.c_void_p), ("iov_len", ctypes.c_int)]
