"""  Credits to Vitalka for this awesome loader"""
import ctypes
import os
import signal
import struct
import subprocess
import sys
from ctypes import *
from ctypes import cdll
from mmap import mmap
from pathlib import Path
from typing import List

import psutil

from lobot_api.connection.iovec import iovec

os_is = sys.platform.startswith


class Loader:
	__instance__ = None
	dll: CDLL

	wine_file_path = ''
	k32_file_path = ''
	ws2_32_file_path = ''
	libwine_path = ''
	libwine = None
	libc = None
	get_errno_loc = None

	BaseAddress: c_uint32 = 0x400000
	# region Patches
	StartingMessageText: str = "Welcome to Lobot"
	VersionText: str = "\t\tVersion: Alpha Stage"
	HexColorArray: List[c_byte] = [0x00, 0xDD, 0x00, 0x00]
	JMP: List[c_byte] = [0xEB]
	RETN: List[c_byte] = [0xC3]
	NOPNOP: List[c_byte] = [0x90, 0x90]
	LanguageTab: List[c_byte] = [0xBF, 0x08, 0x00, 0x00, 0x00, 0x90]
	SeedPatch: List[c_byte] = [0xB9, 0x33, 0x00, 0x00, 0x00, 0x90, 0x90, 0x90, 0x90, 0x90]
	PUSH = 0x68
	# BytePattern
	RedirectIPAddressPattern: List[c_byte] = [0x89, 0x86, 0x2C, 0x01, 0x00, 0x00, 0x8B, 0x17, 0x89, 0x56, 0x50,
	                                          0x8B, 0x47, 0x04, 0x89, 0x46, 0x54, 0x8B, 0x4F, 0x08, 0x89, 0x4E,
	                                          0x58, 0x8B, 0x57, 0x0C, 0x89, 0x56, 0x5C, 0x5E, 0xB8, 0x01, 0x00,
	                                          0x00, 0x00, 0x5D, 0xC3]
	SeedPatchPattern: List[c_byte] = [0x8B, 0x4C, 0x24, 0x04, 0x81, 0xE1, 0xFF, 0xFF, 0xFF, 0x7F]
	NudePatchPattern: List[c_byte] = [0x8B, 0x84, 0xEE, 0x1C, 0x01, 0x00, 0x00, 0x3B, 0x44, 0x24, 0x14]
	ZoomhackPattern: List[c_byte] = [0xDF, 0xE0, 0xF6, 0xC4, 0x41, 0x7A, 0x08, 0xD9, 0x9E]
	MulticlientPattern: List[c_byte] = [0x6A, 0x06, 0x8D, 0x44, 0x24, 0x48, 0x50, 0x8B, 0xCF]
	CallForwardPattern: List[c_byte] = [0x56, 0x8B, 0xF1, 0x0F, 0xB7, 0x86, 0x3E, 0x10, 0x00, 0x00, 0x57, 0x66,
	                                    0x8B, 0x7C, 0x24, 0x10, 0x0F, 0xB7, 0xCF, 0x8D, 0x14, 0x01, 0x3B, 0x96,
	                                    0x4C, 0x10, 0x00, 0x00]
	MultiClientErrorStringPattern: List[c_byte] = "½ÇÅ©·Îµå°¡ ÀÌ¹Ì ½ÇÇà Áß ÀÔ´Ï´Ù.".encode("utf-16")  # encoding default
	SwearFilterStringPattern: List[c_byte] = "UIIT_MSG_CHATWND_MESSAGE_FILTER".encode("utf-16")
	ServerStatusFULLStringPattern: List[c_byte] = "UIO_STT_SERVER_MAX_FULL".encode("utf-16")
	ChangeVersionStringPattern: List[c_byte] = "Ver %d.%03d".encode("utf-16")
	StartingMSGStringPattern: List[c_byte] = "UIIT_STT_STARTING_MSG".encode("utf-16")
	AlreadyProgramExeStringPattern: List[
		c_byte] = "//////////////////////////////////////////////////////////////////".encode("ascii")
	NoGameGuardStringPattern: List[c_byte] = r"config\n_protect.dat".encode("utf_16")
	TextDataNameStringPattern: List[c_byte] = r"%stextdata\textdataname.txt".encode("ascii")
	EnglishStringPattern: List[c_byte] = "English".encode("ascii")
	RussiaStringPattern: List[c_byte] = "Russia".encode("ascii")

	def __init__(self):
		self.ByteArray: c_uint32 = 0x0
		# Addresses
		self.AlreadyProgramExe: c_uint32 = 0
		self.RedirectIPAddress: c_uint32 = 0
		self.MultiClientAddress: c_uint32 = 0
		self.CallForwardAddress: c_uint32 = 0
		self.MultiClientError: c_uint32 = 0
		# NudePatch: c_uint32 = 0
		self.SwearFilter1: c_uint32 = 0
		self.SwearFilter2: c_uint32 = 0
		self.SwearFilter3: c_uint32 = 0
		self.SwearFilter4: c_uint32 = 0
		self.EnglishPatch: c_uint32 = 0
		self.RussianHack: c_uint32 = 0
		self.Zoomhack: c_uint32 = 0
		self.SeedPatchAdress: c_uint32 = 0
		self.TextDataName: c_uint32 = 0
		self.StartingMSG: c_uint32 = 0
		self.ChangeVersion: c_uint32 = 0
		# Import
		dll = None
		if os_is("win_32"):
			Loader.dll = ctypes.windll("kernel32.dll")
			self.LoadLibrary = dll.LoadLibrary
			self.OpenProcess = dll.OpenProcess
			self.ReadProcessMemory = dll.ReadProcessMemory
			self.WriteProcessMemory = dll.WriteProcessMemory
			self.VirtualAllocEx = dll.VirtualAllocEx
			self.CreateMutex = dll.CreateMutex
			self.GetModuleHandle = dll.GetModuleHandle
			self.GetProcAddress = dll.GetProcAddress
			self.WritePrivateProfileString = dll.WritePrivateProfileString
			self.GetPrivateProfileString = dll.GetPrivateProfileString
		else:  # os_is("linux"):
			# dll = ctypes.CDLL("libc.so.6")
			Loader.libwine = cdll.load_library(Loader.libwine_path)
			Loader.dll = cdll.load_library(Loader.k32_file_path)
			Loader.ws_32 = ctypes.CDLL(Loader.ws2_32_file_path)
			Loader.libc = CDLL("libc.so.6", use_errno=True)
			# Loader.get_errno_loc = Loader.libc.errno
			# Loader.get_errno_loc = Loader.libc.__errno_location
			# Loader.get_errno_loc.restype = POINTER(c_int)

			self.OpenProcess = subprocess.Popen
			self.ReadProcessMemory = Loader.dll.Toolhelp32ReadProcessMemory
			self.WriteProcessMemory = Loader.libc.process_vm_writev  # TODO
			self.VirtualAllocEx = mmap
			# self.CreateMutex = dll.CreateMutex
			self.GetModuleHandle = Loader.dll.GetHandleContext  # dll.GetModuleHandle TODO
			self.GetProcAddress = Loader.dll.GetProcAddress

	# LoadLibrary(self, dllToLoad: str) -> POINTER:#static extern
	# OpenProcess: Callable[dwDesiredAccess:c_uint32, bInheritHandle: int, dwProcessId: int) -> POINTER:#static extern
	# ReadProcessMemory-> c_uint32: #static extern
	# WriteProcessMemory:-> c_uint32: #static extern
	# VirtualAllocEx -> c_uint32: #static extern
	# CreateMutex: Callable[[lpMutexAttributes: POINTER, bInitialOwner: bool, lpName: str],POINTER] #static extern
	# GetModuleHandle(self, lpModuleName: str) -> POINTER: #static extern
	# GetProcAddress: Callable[-> c_uint32: #static extern
	# WritePrivateProfileString(self)-> c_uint32: #static extern
	# GetPrLibrary(self, dllToLoad: str) -> POINTER:#static extern
	# OpenProcess: Callable[[c_uint32, int, int], POINTER]  # static extern
	# ReadProcessMemory: Callable[[POINTER, c_uint32, c_uint32, c_uint32, c_uint32], c_uint32]  # static extern
	# WriteProcessMemory: Callable[[POINTER, c_uint32, List[c_byte], int, c_uint32], c_uint32]  # static extern
	# VirtualAllocEx: [[POINTER, POINTER, int, c_uint32, c_uint32], c_uint32]  # static extern
	# CreateMutex: Callable[[POINTER, bool, str], POINTER]  # static extern
	# GetModuleHandle: Callable[[str], POINTER]  # static extern
	# GetProcAddress: Callable[[POINTER], c_uint32]  # static extern
	# WritePrivateProfileString: [[str, str, str, str], c_uint32]  # static extern
	# GetPrivateProfileString: Callable[[str, str, str, str, int, str], c_uint32]  # static extern

	@staticmethod
	def instance():
		"""Singleton constructor."""
		if Loader.__instance__ is None:
			Loader.__instance__ = Loader()
		return Loader.__instance__

	def start_client(self, path: str, port: c_ushort, locale: c_byte):

		FileArray: List[c_byte] = open(Path(path) / "sro_client.exe", "rb").read()
		# region FIND ADDRESSES
		# AlreadyProgramExeSearch
		self.AlreadyProgramExe = 4194302
		self.SeedPatchAdress = 5009534
		self.StartingMSG = 4194350
		self.ChangeVersion = 4194326
		self.MultiClientAddress = 5041155
		self.CallForwardAddress = 4902512
		self.MultiClientError = 4194318
		self.Zoomhack = 4194309
		self.SwearFilter1 = 4194324
		self.SwearFilter2 = 4194418
		self.SwearFilter3 = 4194674
		self.SwearFilter4 = 4194944
		self.RedirectIPAddress = 4917412
		# self.AlreadyProgramExe = self.__findstring_pattern__(self.AlreadyProgramExeStringPattern, FileArray, self.BaseAddress,
		#                                               self.PUSH, 1) - 2
		# # SeedPatchSearch
		# self.SeedPatchAdress = self.BaseAddress + self.__find_pattern__(self.SeedPatchPattern, FileArray, 1)
		# # ReplaceText
		# self.StartingMSG = self.__findstring_pattern__(self.StartingMSGStringPattern, FileArray, self.BaseAddress, self.PUSH,
		#                                         1) + 24
		# self.ChangeVersion = self.__findstring_pattern__(self.ChangeVersionStringPattern, FileArray, self.BaseAddress,
		#                                           self.PUSH, 1)
		# # MulticlientSearch
		# self.MultiClientAddress = self.BaseAddress + self.__find_pattern__(self.MulticlientPattern, FileArray, 1) + 9
		# # CallForwardSearch
		# self.CallForwardAddress = self.BaseAddress + self.__find_pattern__(self.CallForwardPattern, FileArray, 1)
		# # MultiClientErrorSearch
		# self.MultiClientError = self.__findstring_pattern__(self.MultiClientErrorStringPattern, FileArray, self.BaseAddress,
		#                                              self.PUSH, 1) - 8
		# # if checkBox_NudePatch.Checked:
		# #    #NudePatchSearch
		# #    NudePatch = self.BaseAddress + FindPattern(NudePatchPattern, FileArray, 1) + 11
		# # ZoomhackSearch
		# self.Zoomhack = self.BaseAddress + self.__find_pattern__(self.ZoomhackPattern, FileArray, 2) + 5
		# # SwearFilterSearch
		# self.SwearFilter1 = self.__findstring_pattern__(self.SwearFilterStringPattern, FileArray, self.BaseAddress, self.PUSH,
		#                                          1) - 2
		# self.SwearFilter2 = self.__findstring_pattern__(self.SwearFilterStringPattern, FileArray, self.BaseAddress, self.PUSH,
		#                                          2) - 2
		# self.SwearFilter3 = self.__findstring_pattern__(self.SwearFilterStringPattern, FileArray, self.BaseAddress, self.PUSH,
		#                                          3) - 2
		# self.SwearFilter4 = self.__findstring_pattern__(self.SwearFilterStringPattern, FileArray, self.BaseAddress, self.PUSH,
		#                                          4) - 2
		# self.RedirectIPAddress = self.BaseAddress + self.__find_pattern__(self.RedirectIPAddressPattern, FileArray, 1) - 50

		self.__start_loader__(path, port, locale)

	def __start_loader__(self, path: str, port: c_ushort, locale: c_byte):
		def set_pdeathsig(sig=signal.SIGTERM):
			"""
			Use to make subprocess terminate in GNU/Linux after bot is closed.
			:param sig: Death signal to send
			:return: function to be started
			"""

			def callable():
				return Loader.libc.prctl(1, sig)

			return callable

		# sro_mutex = self.create_mutex(None, False, "Silkroad Online Launcher")
		# ready_mutex = self.create_mutex(None, False, "Ready")

		args = []
		if os_is("win_32"):
			args = [Path(path) / "sro_client.exe", "0/" + str(locale) + " 0 0"]
			with subprocess.Popen(args, preexec_fn=set_pdeathsig(signal.SIGTERM)) as wine_proc:
				# suspend for edits
				wine_proc.send_signal(signal.SIGSTOP)
				self.__Quickpatches__(wine_proc.pid)
				self.__redirecti_p__(wine_proc.pid, port)
				self.__multi_client__(wine_proc.pid)
				self.__startingtextms_g__(wine_proc.pid, self.StartingMessageText, self.HexColorArray)
				# resume after edits
				wine_proc.send_signal(signal.SIGCONT)

		# wine_proc = subprocess.Popen(args)
		# SroProcessHandle: POINTER = self.open_process((0x000F0000 | 0x0010000000100000 | 0xFFF), 0, wine_proc.pid)
		# self.__Quickpatches__(SroProcessHandle)
		# self.__redirecti_p__(SroProcessHandle, port)
		# self.__multi_client__(SroProcessHandle)
		# self.__startingtextms_g__(SroProcessHandle, self.StartingMessageText, self.HexColorArray)
		else:  # os_is("linux"):
			# include winepath for linux
			# args = [LoginSettings.instance().winepath + "\\sro_client.exe", "0/" + locale + " 0 0"]
			args = ["/home/pr/.local/share/lutris/runners/wine/lutris-4.20-x86_64/bin/wine64",
			        "/home/pr/Documents/Games/SRO_Asatru/sro_client.exe", "0/" + str(locale) + " 0 0"]
			# args = [f"{Loader.wine_file_path} /home/pr/Documents/Games/SRO_Asatru/silkroad.exe", "0/" + locale + " 0 0"]
			wine_proc = subprocess.Popen(args, stdout=subprocess.PIPE)

			# monitor process names and stop once sro_client.exe is launched
			while True:
				for proc in psutil.process_iter(['pid', 'name', 'username']):
					if proc._name().lower() == "sro_client.exe":  # and proc.ppid() == wine_proc.pid:
						# wait for sro_client.exe then suspend for edits
						proc.send_signal(signal.SIGSTOP)
						self.__Quickpatches__(proc.pid)
						self.__redirecti_p__(proc.pid, port)
						self.__multi_client__(proc.pid)
						self.__startingtextms_g__(proc.pid, self.StartingMessageText, self.HexColorArray)
						# resume after edits
						proc.send_signal(signal.SIGCONT)
						break

	def virtual_alloc_ex_crossplatform(self, process_handle, base_address, size, allocation_type, protect):
		"""
		The adapter function to enable using the same arguments in the platform specific function for changing a region of memory.
		The accepted arguments are windows oriented for the VirtualAllocEx function and are converted for the GNU/Linux mmap if necessary.
		Reserves, commits, or changes the state of a region of memory within the virtual address space of a specified process.
		The function initializes the memory it allocates to zero.

		:param process_handle: The handle to a process. The function allocates memory within the virtual address space of this process.
		:param base_address: The pointer that specifies a desired starting address for the region of pages that you want to allocate.
		:param size: The size of the region of memory to allocate, in bytes.
		:param allocation_type: The type of memory allocation.
		:param protect: The memory protection for the region of pages to be allocated. If the pages are being committed, you can specify any one of the memory protection constants.
		:return: If the function succeeds, the return value is the base address of the allocated region of pages.
		If the function fails, the return value is None.
		"""
		if os_is("win_32"):
			self.VirtualAllocEx(process_handle, base_address, size, allocation_type, protect)
			return 1
		else:
			pass

	def write_process_memory_crossplatform(self, process_handle, base_address, buffer_ptr, buffer_size,
	                                       bytes_written_ref):
		"""
		The adapter function to enable using the same arguments in the platform specific function for writing memory.
		The accepted arguments are windows oriented for the WriteProcessMemory function and are converted for the GNU/Linux libc.process_vm_writev if necessary.

		:param process_handle: A handle to the process memory to be modified. The handle must have PROCESS_VM_WRITE and PROCESS_VM_OPERATION access to the process.
		:param base_address: A pointer to the base address in the specified process to which data is written. Before data transfer occurs, the system verifies that all data in the base address and memory of the specified size is accessible for write access, and if it is not accessible, the function fails.
		:param buffer_size: The number of bytes to be written to the specified process.
		:param buffer_ptr: A pointer to the buffer that contains data to be written in the address space of the specified process.
		:param bytes_written_ref: A pointer to a variable that receives the number of bytes transferred into the specified process. This parameter is optional. If lpNumberOfBytesWritten is NULL, the parameter is ignored.
		:return: If the function succeeds, the return value is nonzero.
		If the function fails, the return value == 0 (zero). To get extended error information, call GetLastError. The function fails if the requested write operation crosses into an area of the process that is inaccessible.
		"""
		try:
			if os_is("win_32"):
				result = self.VirtualAllocEx(process_handle, base_address, buffer_ptr, buffer_size, bytes_written_ref)
				return result
			else:
				data = ctypes.create_string_buffer(buffer_size)
				data.raw = bytes(buffer_ptr)
				local_iovec = iovec(ctypes.cast(data, ctypes.c_void_p), buffer_size)
				remote_iovec = iovec(ctypes.c_void_p(base_address), buffer_size)
				Loader.libc.process_vm_writev.argtypes = [c_int, POINTER(iovec), c_ulong, POINTER(iovec), c_ulong,
				                                          c_ulong]
				# bytes_transferred = Loader.libc.process_vm_readv(process_handle, ctypes.byref(local_iovec), 1, ctypes.byref(remote_iovec), 1, 0)
				bytes_transferred = Loader.libc.process_vm_writev(process_handle, ctypes.byref(local_iovec), 1,
				                                                  ctypes.byref(remote_iovec), 1, 0)
				if bytes_transferred == -1:
					# e = Loader.get_errno_loc.get
					e = ctypes.get_errno()
					print(f"error: {os.strerror(e)}")
				return 1 if bytes_transferred > 0 else -1
		except Exception as e:
			return 0

	def __Quickpatches__(self, SroProcessHandle: POINTER):
		# Already Program Exe
		self.write_process_memory_crossplatform(SroProcessHandle, self.AlreadyProgramExe, self.JMP, len(self.JMP),
		                                        self.ByteArray)
		# Multiclient Error MessageBox
		self.write_process_memory_crossplatform(SroProcessHandle, self.MultiClientError, self.JMP, len(self.JMP),
		                                        self.ByteArray)
		# Nude Patch
		# WriteProcessMemory(SroProcessHandle, NudePatch, NOPNOP, len(NOPNOP), ByteArray)
		# Swear Filter
		self.write_process_memory_crossplatform(SroProcessHandle, self.SwearFilter1, self.JMP, len(self.JMP),
		                                        self.ByteArray)
		self.write_process_memory_crossplatform(SroProcessHandle, self.SwearFilter2, self.JMP, len(self.JMP),
		                                        self.ByteArray)
		self.write_process_memory_crossplatform(SroProcessHandle, self.SwearFilter3, self.JMP, len(self.JMP),
		                                        self.ByteArray)
		self.write_process_memory_crossplatform(SroProcessHandle, self.SwearFilter4, self.JMP, len(self.JMP),
		                                        self.ByteArray)
		# Zoomhack
		self.write_process_memory_crossplatform(SroProcessHandle, self.Zoomhack, self.JMP, len(self.JMP),
		                                        self.ByteArray)
		# English Patch
		self.write_process_memory_crossplatform(SroProcessHandle, self.EnglishPatch, self.NOPNOP, len(self.NOPNOP),
		                                        self.ByteArray)
		self.write_process_memory_crossplatform(SroProcessHandle, self.RussianHack, self.JMP, len(self.JMP),
		                                        self.ByteArray)
		self.write_process_memory_crossplatform(SroProcessHandle, self.TextDataName, self.LanguageTab,
		                                        len(self.LanguageTab),
		                                        self.ByteArray)
		# Seed Patch
		self.write_process_memory_crossplatform(SroProcessHandle, self.SeedPatchAdress, self.SeedPatch,
		                                        len(self.SeedPatch),
		                                        self.ByteArray)

	def __redirecti_p__(self, SroProcessHandle: POINTER, RedirectPort: c_ushort):
		RedirectIPCodeCave: c_uint32 = self.VirtualAllocEx(SroProcessHandle, None, 27, 0x1000, 0x4)
		SockAddrStruct: c_uint32 = self.VirtualAllocEx(SroProcessHandle, None, 8, 0x1000, 0x4)
		WS2Connect: c_uint32 = self.GetProcAddress(self.GetModuleHandle("WS2_32.dll"), "connect")
		WS2Array: List[c_byte] = struct.pack('s', WS2Connect - RedirectIPCodeCave - 26)
		SockAddr: List[c_byte] = struct.pack('s', SockAddrStruct)
		CallRedirectIp: List[c_byte] = struct.pack('s', RedirectIPCodeCave - self.RedirectIPAddress - 5)
		Port: List[c_byte] = struct.pack('s', RedirectPort)
		IP1: List[c_byte] = struct.pack('s', 127)
		IP2: List[c_byte] = struct.pack('s', 0)
		IP3: List[c_byte] = struct.pack('s', 0)
		IP4: List[c_byte] = struct.pack('s', 1)

		Connection: List[c_byte] = [0x02, 0x00, Port[1], Port[0], IP1[0], IP2[0], IP3[0], IP4[0]]
		CallAddress: List[c_byte] = [0xE8, CallRedirectIp[0], CallRedirectIp[1], CallRedirectIp[2], CallRedirectIp[3]]

		RedirectCode: List[c_byte] = [0x50,  # PUSH EAX
		                              0x66, 0x8B, 0x47, 0x02,
		                              # MOV AX,WORD PTR DS:                                          0x66, 0x3D, 0x3D, 0xA3,# CMP AX,0A33D
		                              0x75, 0x05,  # JNZ SHORT xxxxxxxx
		                              0xBF, SockAddr[0], SockAddr[1], SockAddr[2], SockAddr[3],  # MOV EDI,xxxxxxxx
		                              0x58,  # POP EAX
		                              0x6A, 0x10,  # PUSH 10
		                              0x57,  # PUSH EDI
		                              0x51,  # PUSH ECX
		                              0xE8, WS2Array[0], WS2Array[1], WS2Array[2], WS2Array[3],  # CALL WS2_32.connect
		                              0xC3  # RETN
		                              ]
		self.write_process_memory_crossplatform(SroProcessHandle, RedirectIPCodeCave, RedirectCode, len(RedirectCode),
		                                        self.ByteArray)
		self.write_process_memory_crossplatform(SroProcessHandle, SockAddrStruct, Connection, len(Connection),
		                                        self.ByteArray)
		self.write_process_memory_crossplatform(SroProcessHandle, self.RedirectIPAddress, CallAddress, len(CallAddress),
		                                        self.ByteArray)

	def __multi_client__(self, SroProcessHandle: POINTER):
		MultiClientCodeCave: c_uint32 = self.VirtualAllocEx(SroProcessHandle, None, 45, 0x1000, 0x4)
		MACCodeCave: c_uint32 = self.VirtualAllocEx(SroProcessHandle, None, 4, 0x1000, 0x4)
		GTC: c_uint32 = self.GetProcAddress(self.GetModuleHandle("kernel32.dll"), "GetTickCount")
		CallBack: List[c_byte] = struct.pack('s', MultiClientCodeCave + 41)
		CALLForward: List[c_byte] = struct.pack('s', self.CallForwardAddress - MultiClientCodeCave - 34)
		MACAddress: List[c_byte] = struct.pack('s', MACCodeCave)
		GTCAddress: List[c_byte] = struct.pack('s', GTC - MultiClientCodeCave - 18)

		MultiClientArray: List[c_byte] = struct.pack('s', MultiClientCodeCave - self.MultiClientAddress - 5)
		MultiClientCodeArray: List[c_byte] = [0xE8, MultiClientArray[0], MultiClientArray[1], MultiClientArray[2],
		                                      MultiClientArray[3]]

		MultiClientCode: List[c_byte] = [0x8F, 0x05, CallBack[0], CallBack[1], CallBack[2], CallBack[3],
		                                 # POP DWORD PTR DS:[xxxxxxxx]
		                                 0xA3, MACAddress[0], MACAddress[1], MACAddress[2], MACAddress[3],
		                                 # MOV DWORD PTR DS:[xxxxxxxx],EAX
		                                 0x60,  # PUSHAD
		                                 0x9C,  # PUSHFD
		                                 0xE8, GTCAddress[0], GTCAddress[1], GTCAddress[2], GTCAddress[3],
		                                 # Call KERNEL32.gettickcount
		                                 0x8B, 0x0D, MACAddress[0], MACAddress[1], MACAddress[2], MACAddress[3],
		                                 # MOV ECX,DWORD PTR DS:[xxxxxxxx]
		                                 0x89, 0x41, 0x02,  # MOV DWORD PTR DS:[ECX+2],EAX
		                                 0x9D,  # POPFD
		                                 0x61,  # POPAD
		                                 0xE8, CALLForward[0], CALLForward[1], CALLForward[2], CALLForward[3],
		                                 # CALL xxxxxxxx
		                                 0xFF, 0x35, CallBack[0], CallBack[1], CallBack[2], CallBack[3],
		                                 # PUSH DWORD PTR DS:[xxxxxxxx]
		                                 0xC3  # RETN
		                                 ]

		self.write_process_memory_crossplatform(SroProcessHandle, MultiClientCodeCave, MultiClientCode,
		                                        len(MultiClientCode),
		                                        self.ByteArray)
		self.write_process_memory_crossplatform(SroProcessHandle, self.MultiClientAddress, MultiClientCodeArray,
		                                        len(MultiClientCodeArray), self.ByteArray)

	def __startingtextms_g__(self, SroProcessHandle: POINTER, StartingText: str, HexColor: List[c_byte]):

		ChangeVersionString: str = self.VersionText
		StartingMSGStringCodeCave: c_uint32 = self.VirtualAllocEx(SroProcessHandle, None, len(StartingText), 0x1000,
		                                                          0x4)
		ChangeVersionStringCodeCave: c_uint32 = self.VirtualAllocEx(SroProcessHandle, None, len(StartingText), 0x1000,
		                                                            0x4)
		StartingMSGByteArray: List[c_byte] = bytes.decode(StartingText, "utf-7")
		ChangeVersionByteArray: List[c_byte] = bytes.decode(ChangeVersionString, "utf-7")
		CallStartingMSG: List[c_byte] = struct.pack('s', self.StartingMSGStringCodeCave)
		CallChangeVersion: List[c_byte] = struct.pack('s', ChangeVersionStringCodeCave)
		StartingMSGCodeArray: List[c_byte] = [0xB8, CallStartingMSG[0], CallStartingMSG[1], CallStartingMSG[2],
		                                      CallStartingMSG[3]]
		ChangeVersionCodeArray: List[c_byte] = [0x68, CallChangeVersion[0], CallChangeVersion[1], CallChangeVersion[2],
		                                        CallChangeVersion[3]]

		self.write_process_memory_crossplatform(SroProcessHandle, ChangeVersionStringCodeCave, ChangeVersionByteArray,
		                                        len(ChangeVersionByteArray), self.ByteArray)
		self.write_process_memory_crossplatform(SroProcessHandle, self.ChangeVersion, ChangeVersionCodeArray,
		                                        len(ChangeVersionCodeArray), self.ByteArray)
		# WriteProcessMemory(SroProcessHandle, self.ChangeVersion - 59, HexColor, len(HexColor), ByteArray)
		self.write_process_memory_crossplatform(SroProcessHandle, self.StartingMSGStringCodeCave,
		                                        self.StartingMSGByteArray,
		                                        len(self.StartingMSGByteArray), self.ByteArray)
		self.write_process_memory_crossplatform(SroProcessHandle, self.StartingMSG, self.StartingMSGCodeArray,
		                                        len(self.StartingMSGCodeArray), self.ByteArray)
		self.write_process_memory_crossplatform(SroProcessHandle, self.StartingMSG + 9, HexColor, len(HexColor),
		                                        self.ByteArray)

	def __find_pattern__(self, Pattern: List[c_byte], FileByteArray: List[c_byte], Result: c_uint32) -> c_uint32:
		MyPosition: c_uint32 = 0
		ResultCounter: c_uint32 = 0
		for PositionFileByteArray in range(0, len(FileByteArray) - len(Pattern)):
			found: bool = True
			for PositionPattern in range(0, len(Pattern)):
				if FileByteArray[PositionFileByteArray + PositionPattern] != Pattern[PositionPattern]:
					found = False
					break
			if found:
				ResultCounter += 1
				if Result is ResultCounter:
					MyPosition = PositionFileByteArray
					break
		return MyPosition

	def __findstring_pattern__(self, StringByteArray: List[c_byte], FileArray: List[c_byte], BaseAddress: c_uint32,
	                          StringWorker: c_byte, Result: c_uint32) -> c_uint32:

		MyPosition: c_uint32 = 0
		StringWorkerAddress: List[c_byte] = [StringWorker, 0x00, 0x00, 0x00, 0x00]
		StringAddress: List[c_byte] = [0x00] * 4
		StringAddress = (BaseAddress + self.__find_pattern__(StringByteArray, FileArray, 1)).to_bytes(4,
		                                                                                             'little')  # struct.pack('s', )
		StringWorkerAddress[0] = StringAddress[0]
		StringWorkerAddress[1] = StringAddress[1]
		StringWorkerAddress[2] = StringAddress[2]
		StringWorkerAddress[3] = StringAddress[3]
		MyPosition = self.BaseAddress + self.__find_pattern__(StringWorkerAddress, FileArray, Result)
		return MyPosition
