from enum import Enum

from silkroad_security_api_1_4.Packet import Packet


class ShardStatus(Enum):
	Check = 0
	Online = 1


class Shard:
	def __init__(self, packet: Packet):
		self.ID = packet.read_uint16()
		self.name = packet.read_ascii()
		self.players = packet.read_uint16()
		self.capacity = packet.read_uint16()
		self.status = ShardStatus(packet.read_int8())
		self.global_operation_id = packet.read_uint8()

	def __str__(self):
		return f"{self.ID} - {self.name},{self.status}"
