#!/usr/bin/env python3
import logging
import os
import select
import socket
import sys
import threading
from datetime import datetime
from time import sleep
from typing import Callable, Optional, Dict, List, Tuple

from PyQt5.QtCore import QTimer
from silkroad_security_api_1_4.Security import Security

from lobot_api.Bot import Bot
from lobot_api.BotStatus import BotStatus
from lobot_api.Event import Event
from lobot_api.connection.Shard import Shard, ShardStatus
from lobot_api.globals.NetworkGlobal import NetworkGlobal
from lobot_api.globals.settings.LoginSettings import LoginSettings
from lobot_api.logger.Logger import NETWORK, HANDLER
from lobot_api.packets.Handling import Handling
from lobot_api.packets.IRequest import IRequest
from lobot_api.packets.login.CharSelectExecuteAction import CharSelectionExecuteAction, CharacterSelectionAction
from lobot_api.packets.login.LoginClient import LoginClient
from lobot_api.packets.login.Logout import Logout
from lobot_api.packets.login.Logout import LogoutMode
from silkroad_security_api_1_4.BinaryReader import BinaryReader
from silkroad_security_api_1_4.BinaryWriter import BinaryWriter
from silkroad_security_api_1_4.Packet import Packet, PacketDirection

# Replace with your IP address
bind_ip = ''
bind_port = 15779
"""First point to start search for open port."""
bind_port_default = 15779

# gateway_host = NetworkGlobal.instance().GatewayIP  # 'login.asatru-online.eu'
# gateway_port = NetworkGlobal.instance().gateway_port  # 25779

agent_connect = False
agent_host = ''
agent_port = 0

joymax = None
silkroad = None

"""Packet used to keep the connection alive"""
packet_ping = Packet(0x2002)
"""Timer for sending the 2002 ping packet"""
last_packet: datetime = datetime.now()


"""The delegate used to handle incoming packets from the gateway or agent server."""
handle_clientless_delegate: Optional[Callable] = None

"""determines whether loop stays open"""
is_running = True
"""Should switch between client and clientless"""
should_switch_client = False
"""Clientless flag"""
is_clientless = False
"""Waiting to close flag for switching between client and clientless"""
is_client_waiting_for_finish = False

on_client_sent_teleport_packet = Event()
on_client_sent_skill_update_packet = Event()
server_packet_replacement_hooks: Dict[int, Callable[[Packet], List[Tuple[PacketDirection, Packet]]]] = {}

lock = threading.Lock()

def add_server_hook(opcode: int, callback: Callable[[Packet], List[Tuple[PacketDirection, Packet]]]):
	server_packet_replacement_hooks[opcode] = callback


def remove_server_hook(opcode):
	server_packet_replacement_hooks.pop(opcode)


class Silkroad(object):
	s = None
	listen_s = None
	security = None

	def __init__(self):
		pass

	def connect(self, host, port):
		self.close()
		self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		self.s.connect((host, port))
		self.s.setblocking(False)
		self.security = Security()

	def listen(self, host, port):
		self.close()

		if self.listen_s is not None:
			self.listen_s.close()

		self.listen_s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		self.listen_s.bind((host, port))
		self.listen_s.setblocking(False)
		self.listen_s.listen(1)

	def accept(self):
		if self.listen_s is not None:
			self.s, address = self.listen_s.accept()

			self.s.setblocking(False)
			self.security = Security()
			self.security.generate_handshake(True, True, True)

	def recv(self, size=8192):
		try:
			data = self.s.recv(size)
			if data is not None:
				self.security.recv(data)
		except Exception as ex:
			logging.log(NETWORK, *sys.exc_info())
			logging.log(NETWORK, str(ex.__traceback__))
			self.close()

	def send(self, data):
		try:
			while data:
				sent = self.s.send(data)
				data = data[sent:]
		except:
			self.close()

	def send_delayed_packet(self, packet: Packet, delay: int):
		"""
		Run function after QTimer to send delayed packet.
		:param packet: Command instance to create packet.
		:param delay: Delay in milliseconds.
		:return:
		"""
		QTimer.singleShot(delay, lambda: self.send_packet(packet))

	def send_packet(self, packet: Packet):
		"""The main function for sending bot-generated packets to the server.

		:param packet: Packet with information
		:return: None
		"""
		if packet and self.security:
			self.security.send(packet)

	def close(self):
		if self.s is not None:
			try:
				self.s.shutdown(socket.SHUT_RDWR)
				self.s.close()

				self.s = None
				self.security = None
			except:
				pass


def send_login():
	# [C -> S][6102][22 bytes][Enc]
	# 16 #Local
	# 06 00 #UsernameLength
	# 64 61 78 74 65 72 #Username
	# 09 00 #PasswordLenght
	# 31 33 62 69 74 74 65 32 34 #Password
	# 40 00  #shard_id

	packet: Packet = Packet(0x6102, True)
	packet.write_uint8(LoginSettings.instance().locale)
	packet.write_ascii(LoginSettings.instance().username)
	packet.write_ascii(LoginSettings.instance().password)
	packet.write_uint16(LoginSettings.instance().shard_id)
	packet.lock()

	joymax.send_packet(packet)


def send_logout(logout_mode: LogoutMode = LogoutMode.Exit):
	p = Logout(logout_mode)
	if joymax:
		joymax.send_packet(p.create_request())


def send_captcha(captcha: str):
	packet: Packet = Packet(0x6323)
	packet.write_ascii(captcha)
	packet.lock()
	if joymax:
		joymax.send_packet(packet)


def update_ping():
	global last_packet
	now = datetime.now()
	delta = now - last_packet
	last_packet = now
	if joymax and delta.total_seconds() > 5:
		joymax.send_packet(packet_ping)


def handle_clientless_gateway(joymax, p: Packet):
	global agent_connect, agent_host, agent_port, handle_clientless_delegate

	if not should_switch_client:  # Clientless connection ps should end up here
		# Keep alive
		if p.opcode == 0x2002:
			joymax.send_packet(packet_ping)
		# Request Patches
		elif p.opcode == 0x2001:
			# Generate the Patchverification
			response: Packet = Packet(0x6100, True)
			response.write_uint8(LoginSettings.instance().locale)
			response.write_ascii("SR_Client")  # ServiceName
			response.write_uint32(LoginSettings.instance().version)
			joymax.send_packet(response)
		# Request the Serverlist.
		elif p.opcode == 0xA100:
			errorCode = p.read_uint8()
			if errorCode == 1:  # Success
				response: Packet = Packet(0x6101, True)
				joymax.send_packet(response)
			else:
				sys.stdout.write("There is an update or you are using an invalid silkroad version.")
		# Reconnect to AgentServer on successful login
		elif p.opcode == 0xA102:
			result = p.read_uint8()
			if result == 1:
				LoginSettings.instance().session_id = p.read_uint32()
				NetworkGlobal.instance().gateway_ip = p.read_ascii()
				NetworkGlobal.instance().agent_port = p.read_uint16()
				## do:AgentServerConnect = True
				# agent_connect = True
				agent_host = NetworkGlobal.instance().gateway_ip
				agent_port = NetworkGlobal.instance().agent_port
				handle_clientless_delegate = handle_clientless_agent
				joymax.connect(agent_host, agent_port)
		# ServerList
		elif p.opcode == 0xA101:
			# GlobalServer
			next_global_server: bool = p.read_uint8() == 0x01
			# do:
			while next_global_server:
				p.read_uint8()  # globaloperation_id
				p.read_ascii()  # GlobalOperationName
				next_global_server = p.read_uint8() == 0x01

			# ShardList
			LoginSettings.instance().shard_list = []
			next_shard: bool = p.read_uint8() == 0x01
			# do:
			while next_shard:
				s = Shard(p)
				LoginSettings.instance().shard_list.append(s)
				next_shard = p.read_uint8() == 0x01

			shard = next(s for s in LoginSettings.instance().shard_list if s.status == ShardStatus.Online)
			if shard:
				LoginSettings.instance().shard_id = shard.ID
			Bot.set_status(BotStatus.Login)
		# ImageCode Challenge
		elif p.opcode == 0x2322:
			pass
		# captcha: var = CaptchaProcessor.BytesToImage(p.get_bytes())
		# CaptchaProcessor.BytesToImage(CaptchaProcessor.CurrentCaptcha.compressedData)
		# Program.main.picCaptcha.Image = Bitmap.from_file(Environment.CurrentDirectory + "\\captcha.bmp")
		# Program.main.groupImageCode.Enabled = True
		# ImageCode Response
		elif p.opcode == 0xA323:
			result = p.read_uint8()
			if result != 1:
				pass
	# Program.main.groupImageCode.Enabled = True
	return False
	# Else? Else would be clientless and# switching client, you could implement Gateway Clientless->Client function, but thats kinda useless.


def handle_clientless_agent(joymax, p: Packet):
	global agent_connect, agent_host, agent_port, handle_clientless_delegate

	if not should_switch_client:  # Normal Clientless connection should end up here
		# Login
		if p.opcode == 0x6005:
			response: Packet = Packet(0x6103, True)
			response.write_uint32(LoginSettings.instance().session_id)
			response.write_ascii(LoginSettings.instance().username)
			response.write_ascii(LoginSettings.instance().password)
			response.write_uint8(LoginSettings.instance().locale)
			MAC = os.urandom(6)  # uuid.getnode().to_bytes(6, byteorder='big')
			response.write_uint_array(list(MAC))
			response.lock()
			joymax.send_packet(response)
		elif p.opcode == 0xA103:
			success = p.read_uint8()
			if success == 1:
				response: Packet = Packet(0x7007)
				response.write_uint8(2)
				response.lock()
				joymax.send_packet(response)
			else:
				pass
		# !!!  CrossThreading !!!
		# TODO enable login
		# Program.main.groupLogin.Enabled = True
		# CharacterSelection
		elif p.opcode == 0xB007:
			CharSelectionExecuteAction(CharacterSelectionAction.CheckName)._parse_packet_(p)
			Bot.set_status(BotStatus.CharacterSelect)
			# type_value = p.read_uint8()
			# if type_value == 2:
			# 	sucess = p.read_uint8()
			# 	if sucess == 1:
			# 		# Enable character selection
			# 		# Program.main.groupCharacterSelection.Enabled = True
			# 		LoginSettings.instance().characterList = []
			# 		characterCount = p.read_uint8()
			# 		for i in range(0, characterCount):
		#              LoginSettings.instance().characterList.append(Character(p))
		# !!!  CrossThreading !!!
		# Program.main.cbCharacter.Items.clear()
		# Program.main.cbCharacter.Items.add_range(LoginSettings.instance().characterList.to_array())
		# Ingame
		elif p.opcode == 0x3020:
			response: Packet = Packet(0x3012)
			joymax.send_packet(response)
			response2: Packet = Packet(0x750E)  # CLIENT_REQUEST_WEATHER
			joymax.send_packet(response2)
		elif p.opcode == 0x34A6:  # End CharacterData
			pass
		# !!!  CrossThreading !!!
		# Program.main.groupClientlessClient.Enabled = True
		# Program.main.lblStatus.Text = "Clientless connection established. Please use a ReturnScroll"
		elif p.opcode == 0xB04C:
			sucess = p.read_uint8()
			if sucess == 1:
				pass
			# !!!  CrossThreading !!!
			# Program.main.lblStatus.Text = "Waiting for Return Scroll"
			# Program.main.ReturnScrollStarted()
			else:
				pass
	# !!!  CrossThreading !!!
	# Program.main.lblStatus.Text = "ReturnScroll faild, please check Slot (NO INSTANT-RETURN-SCROLLS SIPPORTED YET)"
	else:  # For SwitchingService
		pass
	# Ingame
	# Teleport Request
	# if p.Opcode == 0x30D2:
	# 	# Wait for Client
	# 	# !!!  Create Callback here, because this would freeze the thread resulting in connection lost when taking too long!!!
	# 	while self.__isclientwaitingfor_data__ is False and self.__isclientwatingfor_finish__ is False:
	# 		threading.Thread.Sleep(1)
	# 	# Client is ready
	# 	if self.__isclientwaitingfor_data__:
	# 		# Accept Teleport
	# 		respone: Packet = Packet(0x34B6)
	# 		self.__agent_socket__.send(respone)
	# 		self.__isclientwatingfor_finish__ = True
	# # !!!  CrossThreading!!!
	# # Program.main.lblStatus.Text = "Waiting for Teleport to finish"
	# # Incoming CharacterData
	# if p.Opcode == 0x34A5:
	# 	if self.__isclientwatingfor_finish__:
	# 		self.__shouldswitch_client__ = False
	# 		self.__is_clientless__ = False


def handle_packet_joymax(joymax, silkroad, packet):
	# print('Joymax\n%s' % packet)
	global agent_connect, agent_host, agent_port, handle_clientless_delegate, is_clientless

	if is_clientless:
		packet.lock()
		handle_clientless_delegate(joymax, packet)
	# if agent_connect:
	# 	return handle_clientless_gateway(joymax, p)
	# else:
	# 	return handle_clientless_agent(joymax, p)
	else:
		r = BinaryReader(packet.get_bytes())
		if packet.opcode == 0xA102:

			if r.read_uint8() == 1:
				agent_connect = True

				login_id = r.read_uint32()

				agent_host = r.read_ascii(r.read_uint16())
				agent_port = r.read_uint16()

				w = BinaryWriter()
				w.write_uint8(1)
				w.write_uint32(login_id)
				w.write_uint16(len(bind_ip))
				w.write_ascii(bind_ip)
				w.write_uint16(bind_port)
				silkroad.security.send(Packet(0xA102, True, False, w.tolist()))

				return False

	return True


def handle_packet_silkroad(joymax, silkroad, packet):
	# print('Silkroad\n%s' % packet)

	if packet.opcode == 0x2001:
		return False
	elif packet.opcode == 0x6103:
		packet.lock()
		is_spoofed, result = LoginClient.try_spoof_client_login_packet(packet)
		if is_spoofed:
			joymax.send_packet(result)
			return False
	elif packet.opcode == 0x705A:
		on_client_sent_teleport_packet.notify(packet)
	elif packet.opcode == 0x70A1:
		on_client_sent_skill_update_packet.notify(packet)
	return True


def lobot_handle_incoming_packet(packet):
	"""	Lobot handling function for incoming server packets.
	Updates bot state and calls hook functions if registered.
	:param packet: Server-side packet
	:return: Response packet
	"""
	global joymax, silkroad

	response: IRequest = Handling.instance().handle_packet(packet)
	if response and joymax:
		joymax.security.send(response.create_request())
	if packet.opcode in server_packet_replacement_hooks:
		packets = server_packet_replacement_hooks[packet.opcode](packet)
		for direction, replaced_packet in packets:
			# send replacements
			if direction is PacketDirection.Client and silkroad:
				silkroad.security.send(replaced_packet)
				logging.log(HANDLER, str(replaced_packet))
			elif joymax:
				joymax.security.send(replaced_packet)
				logging.log(HANDLER, str(replaced_packet))
		# only send original packet if not replaced
		return None if packets else packet
	return packet


def get_next_open_port():
	""" Look for next open port to start listening for connection.
	Uses bind_port_default as starting point and increments """
	global bind_port
	testPort = bind_port_default
	validPort: bool = False
	sock: socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)  # , ProtocolType.Tcp)
	# get next free port
	while True:
		try:
			sock.bind((bind_ip, testPort))
			bind_port = testPort
			validPort = True
		except Exception as e:
			testPort += 1
		if validPort:
			break
	sock.close()
	sock = None
	return bind_port


def connect_clientless():
	"""
	Establish clientless connection
	:return:
	"""
	global is_clientless
	is_clientless = True  # FLAG CLIENTLESS
	joymax.connect(NetworkGlobal.instance().gateway_ip, NetworkGlobal.instance().gateway_port)


def disconnect():
	"""Simple logout. Closes both connections to client and server enabling relogging."""
	global is_clientless
	try:
		if silkroad:
			silkroad.close()
		if joymax:
			joymax.close()
		is_clientless = False
	except:
		pass


def stop():
	with lock:
		"""Close connections and stop proxy loop."""
		global is_running, silkroad
		is_running = False
		disconnect()
		silkroad.listen_s.close()


def main():
	global joymax, silkroad, handle_clientless_delegate, on_extraction_changed

	joymax = Silkroad()
	silkroad = Silkroad()

	handle_clientless_delegate = handle_clientless_gateway

	get_next_open_port()
	silkroad.listen(bind_ip, bind_port)
	print(f"Listening for connection on {bind_ip}:{bind_port}")

	try:
		while is_running:
			sockets = []
			with lock:
				if silkroad.listen_s is not None:
					sockets.append(silkroad.listen_s)
				if silkroad.s is not None:
					sockets.append(silkroad.s)
				if joymax.s is not None:
					sockets.append(joymax.s)

			ready_to_read, ready_to_write, in_error = select.select(
				sockets,
				sockets,
				sockets,
				0.01
			)

			# Accept
			if silkroad.listen_s in ready_to_read:
				silkroad.accept()

				global agent_connect
				if agent_connect:
					agent_connect = False
					joymax.connect(agent_host, agent_port)
					print(f"connect ag: {agent_host}:{agent_port}")
				else:
					joymax.connect(NetworkGlobal.instance().gateway_ip, NetworkGlobal.instance().gateway_port)
					print(f"connect gw: {NetworkGlobal.instance().gateway_ip}:{NetworkGlobal.instance().gateway_port}")
			# Joymax
			if joymax.s in ready_to_read:
				# print("server recv")
				joymax.recv()

				packets = joymax.security.get_packets_to_recv()
				if packets:
					for p in packets:
						# print(f"s->p:\t{p}")
						if handle_packet_joymax(joymax, silkroad, p):
							packet = lobot_handle_incoming_packet(p)
							if is_clientless:
								update_ping()
							elif packet and silkroad.security:
								silkroad.security.send(packet)

			if joymax.s in ready_to_write:
				# print("server TransferOutgoing")
				packets = joymax.security.get_packets_to_send()
				if packets:
					for p in packets:
						# opcode = struct.unpack('H', bytes(p[2:4]))[0]
						# print(f"p->s:\t{format(opcode, '#04x')},{','.join([format(b, '02X') for b in p])}")
						joymax.send(bytes(p))

			# Silkroad
			if silkroad.s in ready_to_read:
				# print("client recv")
				silkroad.recv()

				packets = silkroad.security.get_packets_to_recv()
				if packets:
					for p in packets:
						# print(f"c->p->s:\t{p}")
						if handle_packet_silkroad(joymax, silkroad, p):
							joymax.security.send(p)

			if silkroad.s in ready_to_write:
				# print("client TransferOutgoing")
				packets = silkroad.security.get_packets_to_send()
				if packets:
					for p in packets:
						# opcode = struct.unpack('H', bytes(p[2:4]))[0]
						# print(f"p->c:\t{format(opcode, '#04x')},{','.join([format(b, '02X') for b in p])}")
						silkroad.send(bytes(p))

			# Errors
			if joymax.s in in_error:
				joymax.close()

			if silkroad.s in in_error:
				silkroad.close()

			sleep(0.01)
	except KeyboardInterrupt:
		print("Keyboard interrupt")
	except Exception as e:
		logging.log(NETWORK, "proxy error\n", exc_info=sys.exc_info())
		pass

	print("Ended Proxy")
	return 0


if __name__ == '__main__':
	sys.exit(main())
