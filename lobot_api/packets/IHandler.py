from abc import ABC, abstractmethod
from typing import Optional

from lobot_api.packets.IRequest import IRequest
from silkroad_security_api_1_4.Packet import Packet


class IHandler(ABC):
	@property
	@abstractmethod
	def server_opcode(self) -> int:
		"""
		The unique code linked to the server response.
		It determines the packet type sent by the server.
		"""
		pass

	@classmethod
	@abstractmethod
	def _parse_packet_(cls, p: Packet) -> Optional[IRequest]:
		pass

	@classmethod
	def handle(cls, p: Packet) -> IRequest:
		"""Command to handle server responses.
		The received packet is evaluated and a command can be sent as a reply.
		"""
		p.lock()
		return cls._parse_packet_(p)

	@classmethod
	def from_empty_constructor(cls):
		"""
		Constructor used to create_request handler instance for server_opcode and handle function.
		Used in Handling module.
		:return: class instance
		"""
		obj = cls.__new__(cls)  # Does not call __init__
		super(cls, obj).__init__()  # Don't forget to call any polymorphic base class initializers
		return obj
