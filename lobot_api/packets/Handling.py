﻿import importlib
import inspect
import logging
import os
import re
import sys
from pathlib import Path
from typing import Optional

from lobot_api.globals.DirectoryGlobal import DirectoryGlobal
from lobot_api.logger.Logger import HANDLER
from lobot_api.packets.IHandler import IHandler
from lobot_api.packets.IRequest import IRequest
from silkroad_security_api_1_4.Packet import Packet


class Handling:
	"""The class in charge of handling packets.
	Packets to be handled are enqueued then processed by a separate task. This task accesses a dictionary of handling functions which were collected from all available packet handlers using reflection."""

	__instance__ = None

	@staticmethod
	def instance():
		"""The singleton pattern initialization function for handling packets.
		The important part is populating the list of handling functions"""
		if Handling.__instance__ is None:
			Handling.init_handling_functions()
			Handling.__instance__ = Handling()
		return Handling.__instance__

	""" The dictionary mapping opcodes to the according handling functions."""
	handling_functions = {}

	@staticmethod
	def init_handling_functions():
		"""Populate the list of available handlers.
		Scans folder with all handler classes and keeps the ones that aren't interfaces.

		:return: None
		"""
		project_path = DirectoryGlobal.cwd()
		path = Path(project_path) / "lobot_api/packets/"

		for root, dirs, names in os.walk(path):
			for name in filter(lambda n: n.endswith(".py"), names):
				path_str = re.sub(r"[/]", '.', str(Path(os.path.relpath(root, project_path))))
				class_name = path_str + '.' + name.replace('.py', '')
				importlib.import_module(class_name)

		to_check = []
		all_my_base_classes = []
		sub = IHandler.__subclasses__()

		for class_type in IHandler.__subclasses__():
			to_check.append(class_type)

		# get nested subclasses of IHandler
		while to_check:
			c = to_check.pop(0)
			if c:
				sub = c.__subclasses__()
				if sub:
					to_check.extend(sub)
			all_my_base_classes.append(c)

		while all_my_base_classes:
			class_type = all_my_base_classes.pop(0)
			try:
				if not inspect.isabstract(class_type):
					instance = class_type.from_empty_constructor()
					Handling.handling_functions[instance.server_opcode] = instance.handle
			except NotImplementedError as nie:
				logging.log(HANDLER, f"{class_type.__name__}.handle not implemented.\n")
			except Exception as e:
				logging.log(HANDLER, f"{class_type.__name__}:{str(e)}")
				# raise

	@staticmethod
	def is_handshake_packet(opcode: int):
		return opcode == 0x5000 or opcode == 0x9000 or opcode == 0x2001

	# Packet Server -> Client
	def handle_packet(self, p: Packet) -> Optional[IRequest]:
		try:
			if not Handling.is_handshake_packet(p.opcode):
				p.lock()
				handling_function = Handling.handling_functions.get(p.opcode, None)
				if handling_function:
					handling_function(p)
					p.m_reader.seek_set(0)
					return None
				return None
		except Exception as ex:
			logging.log(HANDLER, str(p), exc_info=sys.exc_info())
			return None
