from lobot_api.globals.SpawnListsGlobal import SpawnList
from lobot_api.structs.ISpawnObject import ISpawnObject


class SpawnArgs:
	def __init__(self, spawn: ISpawnObject, spawn_list: SpawnList):
		if spawn is None:
			raise ValueError(ISpawnObject.__name__)
		self.spawn = spawn
		self.spawn_list = spawn_list
