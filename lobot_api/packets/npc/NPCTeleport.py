from typing import Optional

from lobot_api.Bot import Bot
from lobot_api.BotStatus import BotStatus
from lobot_api.Event import Event
from lobot_api.packets.ExchangePacket import ExchangePacket
from lobot_api.packets.IRequest import IRequest
from silkroad_security_api_1_4.Packet import Packet


# teleport from jangan to donwang
# # 0D 00 00 00                                       teleporter_id
# 02                                                action 2 teleport
# 02 00 00 00                                       option // 2 = donwang?
# # 02                                                option 2 teleport
# 01 00                                             unkn
# # 01                                                successFlag?
# teleport from donwang to jangan
# # 11 00 00 00                                       teleporter_id
# 02                                                action 2 teleport
# 01 00 00 00                                       option // 1 = jangan?
# # 02                                                option 2 teleport
# 01 00                                             unkn
# # 01                                                successFlag?
# teleport from jangan to hotan
# # 0D 00 00 00                                       teleporter_id
# 02                                                action 2 teleport
# 05 00 00 00                                       option // 5 = hotan ?
# # 02                                                action 2 teleport.
# 01 00                                             ................
# # 01                                                ................
class NPCTeleport(ExchangePacket):
	on_teleport_used = Event()
	on_teleport_error = Event()

	@property
	def opcode(self) -> int:
		return 0x705A

	@property
	def server_opcode(self) -> int:
		return 0xB05A

	def __init__(self, npc_id, teleport_option: int):
		self.npc_id = npc_id
		self.teleport_option = teleport_option

	def _add_payload_(self) -> Packet:
		p: Packet = Packet(self.opcode)
		p.write_uint32(self.npc_id)
		p.write_uint8(0x02)
		p.write_uint32(self.teleport_option)
		return p

	# # 47 00 00 00                                       unique npc id ?
	# 05                                                teleport type ?
	# 02                                                option move to last recall point
	# # 02                                                ................
	# 01 00                                             01 teleport request received
	# # 02                                                ................
	# 20 1C                                             error: Cannot because lvl over 20 -- UIIT_MSG_NEWBIE_RETURN_LV20_OVER
	# # 47 00 00 00                                       G...............
	# 02                                                ................
	# 2B 00 00 00                                       +...............
	# # 02                                                ................
	# 01 00                                             ................
	# # 01                                                ................
	@classmethod
	def _parse_packet_(cls, p: Packet) -> Optional[IRequest]:
		success = p.read_uint8()
		if success == 0x01:
			Bot.set_status(BotStatus.Loading)
			cls.on_teleport_used.notify()
		elif success == 0x02:  # various codes
			code = p.read_uint16()
			if code == 0x01:
				pass
			# success --> teleport request received
			else:
				cls.on_teleport_error.notify()
		# error message --  UIIT_MSG_NEWBIE_RETURN_LV20_OVER
		return None
