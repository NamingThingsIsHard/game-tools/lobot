from typing import Optional

from lobot_api.Event import Event
from lobot_api.packets.ExchangePacket import ExchangePacket
from lobot_api.packets.IRequest import IRequest
from silkroad_security_api_1_4.Packet import Packet


# Send repair command
# 2E 02 00 00                                       ................
# 02                                                ................
# Receive response
# 01                                                success
class NPCRepair(ExchangePacket):
	Repair = 0x02

	@property
	def opcode(self) -> int: return 0x703E

	@property
	def server_opcode(self) -> int: return 0xB03E

	on_items_repaired = Event()

	def __init__(self, npc_id):
		self.npc_id = npc_id

	def _add_payload_(self) -> Packet:
		p: Packet = Packet(self.opcode)
		p.write_uint32(self.npc_id)
		p.write_uint8(self.Repair)
		return p

	@classmethod
	def _parse_packet_(cls, p: Packet) -> Optional[IRequest]:
		success = p.read_uint8()
		cls.on_items_repaired.notify()
		return None
