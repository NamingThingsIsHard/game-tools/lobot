from ctypes import *
from typing import Optional

from lobot_api.packets.ExchangePacket import ExchangePacket
from lobot_api.packets.IRequest import IRequest
from silkroad_security_api_1_4.Packet import Packet


# # 1B 03 00 00                                       ................
# 00                                                ................
class NPCOpenStorage(ExchangePacket):
	@property
	def opcode(self) -> int: return 0x703C

	@property
	def server_opcode(self) -> int: return 0xB03C

	def __init__(self, unique_id: c_uint32):
		self.npc_id = unique_id

	def _add_payload_(self) -> Packet:
		p: Packet = Packet(self.opcode)
		p.write_uint32(self.npc_id)
		p.write_uint8(0)
		return p

	@classmethod
	def _parse_packet_(cls, p: Packet) -> Optional[IRequest]:
		return None
