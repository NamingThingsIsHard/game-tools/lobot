from lobot_api.globals.SpawnListsGlobal import SpawnList


class DespawnArgs:

	def __init__(self, unique_id, spawn_list: SpawnList):
		self.unique_id = unique_id
		self.list_type = spawn_list
