from abc import abstractmethod

from lobot_api.Event import Event
from silkroad_security_api_1_4.Packet import Packet


class AbstractSpawnParseStrategy:
	on_parsed = Event()

	@abstractmethod
	def parse(self, p: Packet, ref_id):
		pass
