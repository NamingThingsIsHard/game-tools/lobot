from lobot_api.packets.IRequest import IRequest
from silkroad_security_api_1_4.Packet import Packet


class NPCTeleportCancel(IRequest):
	@property
	def opcode(self) -> int: return 0x705B

	def _add_payload_(self) -> Packet:
		return Packet(self.opcode)
