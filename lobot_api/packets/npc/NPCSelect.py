from typing import Optional

from lobot_api.Bot import Bot
from lobot_api.Event import Event
from lobot_api.globals.SpawnListsGlobal import SpawnListsGlobal
from lobot_api.packets.ExchangePacket import ExchangePacket
from lobot_api.packets.IRequest import IRequest
from lobot_api.structs.Monster import Monster
from silkroad_security_api_1_4.Packet import Packet


# # 9E 01 00 00                                       npc_id
# # 01                                                successFlag
# 9E 01 00 00                                       npc_id
# 00                                                ................
# 02                                                ................
# 01                                                ................
# 02                                                ................
# 00                                                ................


class NPCSelect(ExchangePacket):

	on_selected_npc_changed = Event()

	@property
	def opcode(self) -> int:
		return 0x7045

	@property
	def server_opcode(self) -> int:
		return 0xB045

	def __init__(self, npc_id: int):
		self.npc_id = npc_id

	def _add_payload_(self) -> Packet:
		p: Packet = Packet(self.opcode, False)
		p.write_uint32(self.npc_id)
		return p

	@classmethod
	def _parse_packet_(cls, p: Packet) -> Optional[IRequest]:
		success = p.read_uint8()
		if success == 0x01:
			unique_id = p.read_uint32()
			found, spawn = SpawnListsGlobal.try_get_spawn_object(unique_id)
			if found:
				Bot.selected_npc = spawn
			else:
				Bot.selected_npc = Monster()
			cls.on_selected_npc_changed.notify()
		else:
			pass
		# Error
		return None
