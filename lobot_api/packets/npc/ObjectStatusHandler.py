import logging
from typing import Optional

from lobot_api.Bot import Bot
from lobot_api.Event import Event
from lobot_api.globals.SpawnListsGlobal import SpawnListsGlobal
from lobot_api.logger.Logger import BOTTING, NETWORK
from lobot_api.packets.IHandler import IHandler
from lobot_api.packets.IRequest import IRequest
from lobot_api.packets.char.CharStopMovementArgs import CharStopMovementArgs
from lobot_api.packets.npc.DespawnArgs import DespawnArgs
from lobot_api.structs.ObjectStatus import ObjectStatus
from silkroad_security_api_1_4.Packet import Packet


# # 70 62 BE 01                                       unique_id
# 08                                                type (8 = combat)
# 01                                                inCombat flag (1 = yes)
# # 65 67 01 00                                       unique_id
# 00                                                type (0 = dead)
# 02                                                deathFlag (2 = dead)
# # 10 4C 97 00                                       unique_id
# 01                                                type (1 = speed)
# 02                                                2: speed_type = walk
# # 10 4C 97 00                                       unique_id
# 01                                                type (1 = speed)
# 03                                                3: speed_type = run
# # 70 62 BE 01                                       unique_id
# 08                                                type (8 = combat)
# 00                                                inCombat flag (0 = no)
# Invincible after teleport
# # CF 3D A7 00                                       unique_id
# 04                                                4: type = charStatus
# 02                                                status = invincible
# Normal, after invincible or zerk times out
# # CF 3D A7 00
# 04                                                ................
# 00                                                ................


class ObjectStatusHandler(IHandler):
	on_death_registered = Event()
	on_combat_mode_changed = Event()
	on_scroll_mode_change = Event()
	on_resurrect = Event()
	on_zerk_changed = Event()

	@property
	def server_opcode(self) -> int:
		return 0x30BF

	@classmethod
	def _parse_packet_(cls, p: Packet) -> Optional[IRequest]:
		try:
			unique_id = p.read_uint32()
			type_value = p.read_uint8()
			status = p.read_uint8()
			# switch (type)
			if type_value == 0:
				if status == 0x02:  # DEAD
					spawn = SpawnListsGlobal.MonsterSpawns.get(unique_id, None)
					if spawn:
						spawn.is_alive = False
					spawn_list = SpawnListsGlobal.AllSpawnEntries[unique_id]
					if spawn_list:
						cls.on_death_registered.notify(DespawnArgs(unique_id, spawn_list))
				elif status == 0x01:  # ALIVE
					# TODO might be relevant for resurrection
					cls.on_resurrect.notify(CharStopMovementArgs(unique_id))
			elif type_value == 1:  # Speed
				if unique_id == Bot.char_data.unique_id:
					Bot.char_data.motion_state = status
					if status == 2 or status == 3:
						movement_type = 0xFF & (status - 2)  # 2 = walk, 3 = run
						Bot.char_data.movement_type = movement_type  # Switch between walking and running
			elif type_value == 4:
				if Bot.char_data.unique_id == unique_id:
					Bot.char_data.status = ObjectStatus(status)
					cls.on_zerk_changed.notify(status)
				elif status == 0x07:  # /* and Main.DetectInvisible*/:
					found, spawn = SpawnListsGlobal.try_get_spawn_object(unique_id)
					if found:
						logging.log(logging.INFO, f"Invisible Spawn detected: {spawn.name}")
					else:
						logging.log(logging.INFO, "Invisible Spawn detected out of range")
			elif type_value == 7:
				# PINK / RED NAME
				if Bot.char_data.unique_id == unique_id and type_value == 0x07:
					# switch (status)
					if status == 0x00:  # NORMAL
						pass
					elif status == 0x01:  # PINK
						logging.log(logging.WARNING, "Pink name detected!")
					elif status == 0x02:  # RED
						logging.log(BOTTING, "RED NAME DETECTED!")
					else:
						pass
			elif type_value == 8:  # change combat mode
				Bot.char_data.in_combat = status
				cls.on_combat_mode_changed.notify()
			elif type_value == 0xB:  # teleporting status
				Bot.char_data.scroll_mode = status
				cls.on_scroll_mode_change.notify()
			else:
				pass
		except Exception as e:
			logging.log(NETWORK, f"ObjectStatus error {str(p)}")
		return None
