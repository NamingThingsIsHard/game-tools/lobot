from ctypes import *
from typing import Optional

from lobot_api.Bot import Bot
from lobot_api.Event import Event
from lobot_api.packets.ExchangePacket import ExchangePacket
from lobot_api.packets.IRequest import IRequest
from lobot_api.structs.BoolEventArgs import BoolEventArgs
from silkroad_security_api_1_4.Packet import Packet


# start interaction
# 9E 01 00 00                                       npc_id
# 01                                                interaction_type 1 buySell
# Start interaction confirm
# 01                                                success_flag 1
# 01                                                interaction_type 1 buySell
# 02 01 00 00                                       toggle open/close deal with npc
# 02 01 00 00                                       Menu Choice
# 01                                                1 (buy)
# 01                                                successFlag


class NPCCloseDialogue(ExchangePacket):
	on_dialogue_closed = Event()

	@property
	def opcode(self) -> int:
		return 0x704B

	@property
	def server_opcode(self) -> int:
		return 0xB04B

	def __init__(self, npc_id: c_uint32):
		self.npc_id = npc_id

	def _add_payload_(self) -> Packet:
		p: Packet = Packet(self.opcode)
		p.write_uint32(self.npc_id)
		return p

	@classmethod
	def _parse_packet_(cls, p: Packet) -> Optional[IRequest]:
		success_flag = p.read_uint8()
		# switch (successFlag)
		if success_flag == 0x01:  # success
			pass
		else:
			pass
		Bot.is_interacting = success_flag != 0x01
		cls.on_dialogue_closed.notify()
		return None
