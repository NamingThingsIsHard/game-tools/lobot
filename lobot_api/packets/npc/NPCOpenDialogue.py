import logging
from typing import Optional

from lobot_api.Bot import Bot
from lobot_api.Event import Event
from lobot_api.logger.Logger import TOWN
from lobot_api.packets.ExchangePacket import ExchangePacket
from lobot_api.packets.IRequest import IRequest
from lobot_api.packets.npc.NPCInteractionType import NPCInteractionType
from silkroad_security_api_1_4.Packet import Packet


# 02 01 00 00                                       toggle open/close deal with npc
# 02 01 00 00                                       Menu Choice
# 01                                                1 (buy)
# 01                                                successFlag
# 01                                                successFlag1
# 01                                                successFlag2
class NPCOpenDialogue(ExchangePacket):
	on_interaction_started = Event()
	on_interaction_error = Event()

	@property
	def opcode(self) -> int:
		return 0x7046

	@property
	def server_opcode(self) -> int:
		return 0xB046

	def __init__(self, npc_id: int, interaction_option: NPCInteractionType):
		self.npc_id = npc_id
		self.InteractionOption = interaction_option

	def _add_payload_(self) -> Packet:
		p: Packet = Packet(self.opcode)
		p.write_uint32(self.npc_id)
		p.write_uint8(self.InteractionOption.value)
		return p

	@classmethod
	def log_npc_open_dialogue_error(cls, error: int):
		error_text = "User is on other business" if error == 0x1C0B else "Unknown"
		logging.log(TOWN, f"Error opening NPC dialogue: {error} -- {error_text}")

	@classmethod
	def _parse_packet_(cls, p: Packet) -> Optional[IRequest]:
		success = p.read_uint8()
		if success != 1:
			error = p.read_uint16()
			cls.log_npc_open_dialogue_error(error)
			cls.on_interaction_error.notify(error)
			return None

		type_value: NPCInteractionType = NPCInteractionType(p.read_uint8())
		# switch (type)# todo
		if type_value == NPCInteractionType.BuySell:
			pass
		elif type_value == NPCInteractionType.Talk:
			pass
		elif type_value == NPCInteractionType.Store:
			pass
		elif type_value == NPCInteractionType.PurchaseTradeGoods:
			pass
		elif type_value == NPCInteractionType.ParticipateInMagicPop:
			pass
		elif type_value == NPCInteractionType.GrantMagicOption:
			pass
		else:
			pass
		Bot.is_interacting = success == 0x01
		cls.on_interaction_started.notify()
		return None
