from ctypes import *
from typing import Optional

from lobot_api.packets.ExchangePacket import ExchangePacket
from lobot_api.packets.IRequest import IRequest
from silkroad_security_api_1_4.Packet import Packet


# # 0D 00 00 00                                       ................
# # 01                                                ................
class NPCDesignateRecall(ExchangePacket):
	@property
	def opcode(self) -> int: return 0x7059

	@property
	def server_opcode(self) -> int: return 0xB059

	def __init__(self, npc_id: c_uint32):
		self.npc_id = npc_id

	def _add_payload_(self) -> Packet:
		p: Packet = Packet(self.opcode)
		p.write_uint32(self.npc_id)
		return p

	@classmethod
	def _parse_packet_(cls, p: Packet) -> Optional[IRequest]:
		success = p.read_uint8()
		return None
