from enum import Enum


class NPCInteractionType(Enum):  # byte
	BuySell = 0x01
	Talk = 0x02
	Store = 0x03
	PurchaseTradeGoods = 0x0C  # 12
	ParticipateInMagicPop = 0x11  # 17
	GrantMagicOption = 32  # Smith option to enhance AvatarItems
