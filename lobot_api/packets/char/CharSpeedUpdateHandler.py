from typing import Optional

from lobot_api.Bot import Bot
from lobot_api.packets.IHandler import IHandler
from lobot_api.packets.IRequest import IRequest
from silkroad_security_api_1_4.Packet import Packet


class CharSpeedUpdateHandler(IHandler):
	@property
	def server_opcode(self) -> int: return 0x30D0

	# # 18 DF 61 00                                       unique_id
	# 9A 99 99 41                                       walkspeed
	# 01 00 70 42                                       runspeed
	@classmethod
	def _parse_packet_(cls, p: Packet) -> Optional[IRequest]:
		if p.read_uint32() == Bot.char_data.unique_id:
			Bot.char_data.walk_speed = p.read_single()
			Bot.char_data.run_speed = p.read_single()
		return None
