from lobot_api.packets.IRequest import IRequest


class ObjectAction(IRequest):
	@property
	def opcode(self) -> int: return 0x7074
