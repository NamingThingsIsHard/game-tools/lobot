from ctypes import *

from lobot_api.packets.char.ObjectAction import ObjectAction
from silkroad_security_api_1_4.Packet import Packet


# # 01                                                action phase flag // 1 = start 2 = cancel/complete
# 02                                                type ? 
# 01                                                amount?
# 1E D4 AA 01                                       uniqueitem_id
# # 01                                                initiation confirmation
# 01                                                flag
# # 02                                                action phase flag // 1 = start 2 = cancel/complete
# 00                                                flag
class PickupItem(ObjectAction):
	def __init__(self, unique_id: c_uint32):
		self.uniqueitem_id = unique_id

	def _add_payload_(self) -> Packet:
		p: Packet = Packet(self.opcode, False)
		p.write_uint8(0x01)
		p.write_uint8(0x02)  # object action type ?
		p.write_uint8(0x01)
		p.write_uint32(self.uniqueitem_id)
		return p
