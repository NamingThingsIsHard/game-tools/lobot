# # B3 D7 79 01                                       unique_id
# 04 00                                             unknw
# 02                                                type
# 60 0D 00 00                                       newValue
# # 99 CF 74 00                                       unique_id
# 01 00                                             unknw
# 05                                                type
# 31 16 00 00                                       1...............
# 00 00 00 00                                       ................
# slumber
# # 2A 04 E6 00                                       *...............
# # 2A 04 E6 00                                       *...............
# 00 01                                             ................
# 05                                                ................
# 39 06 00 00                                       9...............
# 40 00 00 00                                       @...............
# 04                                                ................
# # 2A 04 E6 00                                       *...............
# 00 01                                             ................
# 04                                                ................
# 40 00 00 00                                       @...............
# 04                                                ................
from typing import Optional

from lobot_api.Bot import Bot
from lobot_api.Event import Event
from lobot_api.globals.SpawnListsGlobal import SpawnListsGlobal, SpawnList
from lobot_api.packets.IHandler import IHandler
from lobot_api.packets.IRequest import IRequest
from lobot_api.packets.autopotion.HPMPUpdateType import HPMPUpdateType
from lobot_api.packets.autopotion.StatusUpdateArgs import StatusUpdateArgs
from lobot_api.statemachine.AutoPotions import AutoPotions
from lobot_api.structs import DebuffStatus
from lobot_api.structs.BadStatus import BadStatus
from lobot_api.structs.DebuffStatus import DebuffStatus
from silkroad_security_api_1_4.Packet import Packet


class HPMPUpdateHandler(IHandler):
	# # Use a bitwise "xor" on this and the status uint --> (0x4000 ^ status)
	# The result is either
	# Debuff > 0x4040
	# Normal is c_uint32(BadStatus.Stun
	# BadStatus < c_uint32(BadStatus.Stun
	# e.g.
	# Trap    0x1004000
	# Panic   0x204000
	# Bleed   0x4800
	# Stun    0x4000
	# Sleep   0x4040
	# Poison  0x4010
	# Normal  0x0000
	# Zombie  0x20
	on_char_updated = Event()
	on_monster_died = Event()
	on_monster_updated = Event()

	on_stunned = Event()
	on_other_updated = Event()

	bad_status_limit = 0x40  # Number to use for debuff + bad status check
	debuff_limit = 0x4000  # Number to use for debuff + bad status check
	bad_status_debuff_limit = 0x4040  # Number to use for debuff + bad status check

	@property
	def server_opcode(self) -> int:
		return 0x3057

	@staticmethod
	def __get_debuff_status__(status_flags: int) -> DebuffStatus:
		if status_flags & BadStatus.Stun.value == BadStatus.Stun.value:
			HPMPUpdateHandler.on_stunned.notify()

		if status_flags > HPMPUpdateHandler.bad_status_debuff_limit:
			return DebuffStatus.BadStatusDebuff
		elif status_flags >= HPMPUpdateHandler.bad_status_limit:
			return DebuffStatus.Debuff
		elif status_flags > 0:
			return DebuffStatus.BadStatus
		else:
			return DebuffStatus.Normal

	@classmethod
	def __update_char_hp_mp__(cls, p: Packet):
		update_type: HPMPUpdateType = HPMPUpdateType(p.read_uint8())
		status: int
		# switch (updateType)
		if update_type == HPMPUpdateType.hp:
			Bot.char_data.current_hp = p.read_uint32()
			AutoPotions.try_use_hp_pot()
			AutoPotions.try_use_vigor()
		elif update_type == HPMPUpdateType.mp:
			Bot.char_data.current_mp = p.read_uint32()
			AutoPotions.try_use_mp_pot()
			AutoPotions.try_use_vigor()
		elif update_type == HPMPUpdateType.hp_mp:
			Bot.char_data.current_hp = p.read_uint32()
			Bot.char_data.current_mp = p.read_uint32()
			AutoPotions.try_use_hp_pot()
			AutoPotions.try_use_mp_pot()
			AutoPotions.try_use_vigor()
		elif update_type == HPMPUpdateType.bad_status:
			# 0 = stop, 1 = fire (?), 2 = ice, 3 = freeze, 4 = electricity, 8 = fire, 16 = poison
			Bot.char_data.debuff_flags = p.read_uint32()
			Bot.char_data.debuff_status = HPMPUpdateHandler.__get_debuff_status__(Bot.char_data.debuff_flags)
			AutoPotions.try_cure_bad_status_or_cure_debuff()
		elif update_type == HPMPUpdateType.hp_bad_status:
			Bot.char_data.current_hp = p.read_uint32()
			Bot.char_data.debuff_flags = p.read_uint32()
			Bot.char_data.debuff_status = HPMPUpdateHandler.__get_debuff_status__(Bot.char_data.debuff_flags)
			AutoPotions.try_use_hp_pot()
			AutoPotions.try_use_vigor()
			AutoPotions.try_cure_bad_status_or_cure_debuff()
		elif update_type == HPMPUpdateType.mp_bad_status:
			Bot.char_data.current_mp = p.read_uint32()
			Bot.char_data.debuff_flags = p.read_uint32()
			Bot.char_data.debuff_status = HPMPUpdateHandler.__get_debuff_status__(Bot.char_data.debuff_flags)
			AutoPotions.try_use_mp_pot()
			AutoPotions.try_use_vigor()
			AutoPotions.try_cure_bad_status_or_cure_debuff()
		elif update_type == HPMPUpdateType.hp_mp_bad_status:
			Bot.char_data.currentMP = p.read_uint32()
			Bot.char_data.currentMP = p.read_uint32()
			status = p.read_uint32()
			Bot.char_data.DebuffState = HPMPUpdateHandler.__get_debuff_status__(status)
			AutoPotions.try_use_hp_pot()
			AutoPotions.try_use_mp_pot()
			AutoPotions.try_use_vigor()
			AutoPotions.try_cure_bad_status_or_cure_debuff()
		else:
			pass

		cls.on_char_updated.notify(StatusUpdateArgs(
			Bot.char_data.unique_id, update_type,
			Bot.char_data.debuff_status, Bot.get_status() == 0)
		)

	@classmethod
	def __update_pet_hp__(cls, p: Packet, object_id):
		update_type: HPMPUpdateType = HPMPUpdateType(p.read_uint8())
		if object_id == Bot.transport.unique_id:
			if update_type.value & 1 == HPMPUpdateType.hp.value:
				Bot.transport.current_hp = p.read_uint32()
				AutoPotions.try_use_transport_hp_pot(object_id)
		elif object_id == Bot.growth_pet.unique_id:
			if update_type.value & 1 == HPMPUpdateType.hp.value:
				Bot.growth_pet.current_hp = p.read_uint32()
				AutoPotions.try_use_pet_hp_pot(object_id)
			elif update_type.value & 4 == HPMPUpdateType.hp.value:  # Attack Mode
				Bot.growth_pet.BadStatus = p.read_uint8()
				AutoPotions.try_cure_pet_bad_status()

	@classmethod
	def __update_monster_hp__(cls, p: Packet, object_id):
		update_type: HPMPUpdateType = HPMPUpdateType(p.read_uint8())

		found, spawn = SpawnListsGlobal.try_get_spawn_object(object_id)
		if update_type.value & 1 == HPMPUpdateType.hp.value and spawn:
			spawn.current_hp = p.read_uint32()
			cls.on_monster_updated.notify(StatusUpdateArgs(object_id, update_type))
			if spawn.current_hp == 0:
				spawn.is_alive = False
				cls.on_monster_died.notify(StatusUpdateArgs(object_id, update_type))

	@classmethod
	def _parse_packet_(cls, p: Packet) -> Optional[IRequest]:
		object_id = p.read_uint32()
		p.read_uint16()  # update_source -- Unknown typeFlag effect
		if Bot.char_data.unique_id == object_id:
			cls.__update_char_hp_mp__(p)
		elif object_id in SpawnListsGlobal.AllSpawnEntries:
			bot_list = SpawnListsGlobal.AllSpawnEntries.get(object_id, None)
			# switch (botLists)
			if bot_list == SpawnList.MonsterSpawns:
				cls.__update_monster_hp__(p, object_id)
			elif bot_list == SpawnList.NPCs:
				pass
			elif bot_list == SpawnList.Portals:
				pass
			elif bot_list == SpawnList.Players:
				# TODO
				pass
			elif bot_list == SpawnList.Pets:
				cls.__update_pet_hp__(p, object_id)
			else:
				pass
		return None
