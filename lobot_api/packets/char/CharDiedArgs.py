from ctypes import c_uint32


class CharDiedArgs:

	def __init__(self, unique_id: c_uint32):
		self.unique_id = unique_id
