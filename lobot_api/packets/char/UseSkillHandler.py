import logging
from typing import Optional

from PyQt5.QtCore import QTimer, QThreadPool

from lobot_api.Bot import Bot
from lobot_api.Event import Event
from lobot_api.globals.SpawnListsGlobal import SpawnListsGlobal
from lobot_api.logger.Logger import SCRIPT
from lobot_api.packets.IRequest import IRequest
from lobot_api.packets.char.MonsterAggroArgs import MonsterAggroArgs
from lobot_api.packets.char.SkillArgs import SkillArgs
from lobot_api.packets.char.SkillErrorArgs import SkillErrorArgs
from lobot_api.packets.char.SkillErrorType import SkillErrorType
from lobot_api.packets.char.SkillHandler import SkillHandler
from lobot_api.structs.CharData import Skill
from lobot_api.structs.GrowthPet import GrowthPet
from lobot_api.structs.Monster import Monster
from silkroad_security_api_1_4.Packet import Packet


# Start using skill
# # 01                                                successFlag // 1 = success 
# 02 30                                             unkn 12290
# 47 00 00 00                                       skill_id 71 antidevil bow
# 0B 2C 10 00                                       caster_id
# 2A BB 00 00                                       unkn 106790
# 26 A1 01 00                                       target_id
# 00                                                instant response
# 
# Damage skill
# # 01                                                successFlag // 1 = success 
# 02 30                                             skilltype
# 32 0E 00 00                                       skill_id
# 7F 82 32 01                                       attacker_id
# 6C 95 05 00                                       unknw
# 9D 86 78 01                                       target_id
# 01                                                ................
# 02                                                ................
# 01                                                ................
# 9D 86 78 01                                       target_id
# 00                                                ................
# 01 0A 00 00                                       ................
# 00 00 00 00                                       ................
# 02                                                ................
# -----Errors
# ì¤í¬ ê´ë ¨							0								
# 1	UIIT_SKILL_USE_FAIL_NOTENOUGHMP MPê° ë¶ì¡±í´ì ì¬ì©í  ì ììµëë¤.    0	0	0	0	0	0	Cannot use due to insufficient MP	0	0	0	0	0	0	0	0	
# 1	UIIT_SKILL_USE_FAIL_NOTENOUGHHP HPê° ë¶ì¡±í´ì ì¬ì©í  ì ììµëë¤.	0	0	0	0	0	0	Cannot use due to insufficient HP	0	0	0	0	0	0	0	0	
# 1	UIIT_SKILL_USE_FAIL_TIMEDELAY_COOL_TIME ì¤í¬ ì¬ ì¬ì© ìê°ì´ ì§ëì§ ìììµëë¤.  0	0	0	0	0	0	Still have time to reuse the skill. 0	0	0	0	0	0	0	0	
# 1	UIIT_SKILL_USE_FAIL_WRONGTARGET ì¬ë°ë¥¸ íê²ì´ ìëëë¤.	0	0	0	0	0	0	Invalid target	0	0	0	0	0	0	0	0
# 1	UIIT_SKILL_USE_FAIL_NONESELECTETARGET íê²ì´ ì íëì§ ìììµëë¤.	0	0	0	0	0	0	The target is not selected.	0	0	0	0	0	0	0	0
# 1	UIIT_SKILL_USE_FAIL_OVERLAP ì¤ë³µë  ì ìë ìì´íì´ë ì¤í¬ì´ ì´ë¯¸ ì¬ì©ëê³  ììµëë¤.    0	0	0	0	0	0	The selected item cannot be used to# gether or the skill is already in use.   0	0	0	0	0	0	0	0	
# 1	UIIT_SKILL_USE_FAIL_CHAR_DEAD ìºë¦­í°ê° ì£½ì ìíìì í´ë¹ ìì´íì ì¬ì©í  ì ììµëë¤.    0	0	0	0	0	0	Cannot the use selected item while dead.    0	0	0	0	0	0	0	0	
# 1	UIIT_SKILL_USE_FAIL_WRONGWEAPON ì¤í¬ì ì¸ ì ìë ë¬´ê¸°ë¥¼ ì°©ì©íê³  ììµëë¤.	0	0	0	0	0	0	You wear equipment which prohibits you from using the selected skill.   0	0	0	0	0	0	0	0	
# 1	UIIT_SKILL_USE_FAIL_RUNOUT_AMMO íì´ì´ ë¶ì¡±í©ëë¤.  0	0	0	0	0	0	Insufficient arrows.	0	0	0	0	0	0	0	0	
# 1	UIIT_SKILL_USE_FAIL_RUNOUT_AMMO_VOLT ë³¼í¸ê° ë¶ì¡±í©ëë¤.  0	0	0	0	0	0	Insufficient bolts.	0	0	0	0	0	0	0	0	
# 1	UIIT_SKILL_USE_FAIL_BROKEN_WEAPON ë¬´ê¸°ê° ê³ ì¥ëì ê³µê²©ì í  ì ììµëë¤.  0	0	0	0	0	0	Cannot attack because the weapon is broken  0	0	0	0	0	0	0	0	
# 1	UIIT_SKILL_USE_FAIL_PATH_INTERRUPTED ì¥ì ë¬¼ì´ ìì´ ê³µê²©ì í  ì ììµëë¤.   0	0	0	0	0	0	Cannot attack due to an obstacle.	0	0	0	0	0	0	0	0	
# 1	UIIT_SKILL_USE_FAIL_DUPLICATE ìì  í¹ì ìëë°©ì´ ì§ì/ìì ëë ¨/ì´ì¸ì ìíì¼ ëë ì¬ì©í  ì ììµëë¤. ëí ì´ìì ìºë¦­í°ìê²ë ì¬ì©í  ì ììµëë¤.	0	0	0	0	0	0	Cannot be used when you or the opponent is under job or free battle or murderer state.	0	0	0	0	0	0	0	0	
# 1	UIIT_SKILL_USE_FAIL_SEALED íì¬ ì¬ì©í  ì ìë ì¤í¬ìëë¤.	0	0	0	0	0	0	Unable to use that skill at this moment 0	0	0	0	0	0	0	0	
# Fail to cast fire shield  becuase shield not equipped
# # 02                                                failure
# 0D 30                                             failure code 0x300D -> wrong weapon
# Ammo not equipped
# # 02                                                failure
# 0E 30                                             failure code 0x300E -> Insufficient bolts UIIT_SKILL_USE_FAIL_RUNOUT_AMMO_VOLT
class UseSkillHandler(SkillHandler):
	on_drew_monster_aggro = Event()
	on_drew_aggro_pet = Event()
	on_skillcast_error = Event()
	on_skill_cast = Event()

	@property
	def server_opcode(self) -> int:
		return 0xB070

	@classmethod
	def enable_skill(cls, delay: int, skill: Skill):
		def enable_skill():
			skill.skill_enabled = 0x01

		QTimer.singleShot(delay, enable_skill)

	@classmethod
	def __register_bot_skill_cast__(cls, skill_id):
		skill: Skill = (Bot.auto_attack_skills_ids[skill_id] if skill_id in Bot.auto_attack_skills_ids else
			next((s for s in Bot.char_data.skills if s.ref_id == skill_id), None))
		if skill is not None:
			# TODO automatically add chain attacks to skill list to prevent None being returned
			skill.skill_enabled = 0x0
			cls.on_skill_cast.notify(SkillArgs(skill))
			QThreadPool.globalInstance().start(lambda: cls.enable_skill(skill.cooldown, skill))  # reset after cooldown

	@classmethod
	def __register_spawn_skill_cast__(cls, attacker_id, target_id):
		found, attacker = SpawnListsGlobal.try_get_spawn_object(attacker_id)
		if found:
			if isinstance(attacker, Monster):  # Handle monster type attacking
				attacker.target = target_id
				if Bot.char_data.unique_id == target_id:
					cls.on_drew_monster_aggro.notify(MonsterAggroArgs(attacker))
				elif Bot.growth_pet.unique_id == target_id:
					cls.on_drew_aggro_pet.notify(MonsterAggroArgs(attacker))
			elif attacker.unique_id == Bot.char_data.unique_id:
				# Handle Player skill cast
				pass
			elif isinstance(attacker, GrowthPet):
				pass
			# Handle Player skill cast
			else:
				pass
				# Unknown unparsed monster ?
				logging.log(SCRIPT, f"Non-Monster-Attacker!  ID {format(attacker_id, '02X')}")

	@classmethod
	def _parse_packet_(cls, p: Packet) -> Optional[IRequest]:
		success = p.read_uint8()
		if success == 0x01:
			set_flag = p.read_uint16()  # 02 30
			# TODO figure out when to ignore type cast
			# if # set_flag != 0x3002:
			# {
			#    return None
			# }
			skill_id = p.read_uint32()
			attacker_id = p.read_uint32()
			unknown = p.read_uint32()
			target_id = p.read_uint32()

			if attacker_id == Bot.char_data.unique_id:  # # set skill on cooldown
				cls.__register_bot_skill_cast__(skill_id)
			elif target_id == Bot.char_data.unique_id:  # Handle being the target
				cls.__register_spawn_skill_cast__(attacker_id, target_id)
				cls.__parse_damage__(p)
		else:
			error_type = p.read_uint8()
			# switch (errorType)
			if error_type == 0x05:
				cls.on_skillcast_error.notify(SkillErrorArgs(SkillErrorType.SkillOnCooldown))
			elif error_type == 0x06:
				cls.on_skillcast_error.notify(SkillErrorArgs(SkillErrorType.InvalidTarget))
			elif error_type == 0x10:  # can't attack due to obstacle
				cls.on_skillcast_error.notify(SkillErrorArgs(SkillErrorType.Obstacle))
			elif error_type == 0x0D:
				sub_type = p.read_uint8()
				if sub_type == 0x30:  # cannot cast skill due to wrong weapon
					pass
				cls.on_skillcast_error.notify(SkillErrorArgs(SkillErrorType.WrongWeapon))
			# TODO change to right weapon
			elif error_type == 0x0E:  # Insufficient Bolts
				cls.on_skillcast_error.notify(SkillErrorArgs(SkillErrorType.NoBolts))
				pass
			else:
				pass
		# error
		return None
