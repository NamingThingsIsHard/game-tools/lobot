from typing import Optional

from lobot_api.Bot import Bot
from lobot_api.Event import Event
from lobot_api.globals.SpawnListsGlobal import SpawnListsGlobal
from lobot_api.packets.IHandler import IHandler
from lobot_api.packets.IRequest import IRequest
from lobot_api.packets.char.BuffArgs import BuffArgs
from lobot_api.pk2.PK2Skill import PK2SkillType
from lobot_api.statemachine.AutoPotions import AutoPotions
from lobot_api.structs.CharData import Buff
from silkroad_security_api_1_4.Packet import Packet


# Add buff fire shield pheonix
# # 54 18 11 00                                       caster_id
# 7C 00 00 00                                       buff_id 127 -> fire shield pheonix
# CC C2 04 00                                       buff unique_id
# target
# # 61 36 A4 00                                       a6..............
# successful slumber on monster
# # 61 36 A4 00                                       targetunique_id
# 70 2B 00 00                                       skill ref_id ->
# 00 00 00 00                                       ................
# unsuccessful slumber
# # 61 36 A4 00                                       targetunique_id
# 70 2B 00 00                                       skill ref_id
# 00 00 00 00                                       ................
# warlockdot SKILL_EU_WARLOCK_DOTA_POISON
# # 47 58 A4 00                                       unique_id target monster kokuro
# D9 29 00 00                                       10713 SKILL_EU_WARLOCK_DOTA_POISON
# 3E 44 00 00                                       unkn
# 60 09 00 00                                       unkn
# dot end ?
# # 01                                                number of buffs ended
# 12 45 00 00                                       unkn


class BuffAddHandler(IHandler):
	on_buff_added = Event()

	@property
	def server_opcode(self) -> int:
		return 0xB0BD

	@classmethod
	def _parse_packet_(cls, p: Packet) -> Optional[IRequest]:
		target_id = p.read_uint32()
		buff: Buff = Buff(p.read_uint32())
		buff.target_id = target_id
		buff.unique_id = p.read_uint32()
		# 0 = resist or debuff (which is handled in 3057)
		if buff.unique_id != 0:
			if target_id == Bot.char_data.unique_id:
				if buff.unique_id not in Bot.char_data.active_buffs:
					Bot.char_data.active_buff_count += 1
					Bot.char_data.active_buffs[buff.unique_id] = buff
				if buff.type is PK2SkillType.Debuff:
					AutoPotions.try_cure_debuff()
				cls.on_buff_added.notify(BuffArgs(buff.unique_id, buff.ref_id, target_id))
			elif buff.unique_id in SpawnListsGlobal.MonsterSpawns:
				spawn = SpawnListsGlobal.MonsterSpawns[buff.unique_id]
				spawn.buffs[buff.unique_id] = buff
				SpawnListsGlobal.SpawnBuffs[buff.unique_id] = spawn.unique_id
		return None
