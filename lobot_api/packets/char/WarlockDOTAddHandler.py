from typing import Optional

from lobot_api.Event import Event
from lobot_api.globals.SpawnListsGlobal import SpawnListsGlobal
from lobot_api.packets.IHandler import IHandler
from lobot_api.packets.IRequest import IRequest
# C9 34 DD 00                                       target_id
# 02 2A 00 00                                       skill_id
# 03 01 02 00                                       uniquebuff_id1
# C9 34 DD 00                                       target_id
# 00 00                                             unkn
# C9 34 DD 00                                       target_id
# 02 2A 00 00                                       skill_id
# 2B D6 02 00                                       uniquebuff_id2
# 60 09 00 00                                       unkn
# 01                                                success
# 03 01 02 00                                       uniquebuff_id1
# 01                                                success
# 2B D6 02 00                                       uniquebuff_id2
from lobot_api.packets.char.BuffArgs import BuffArgs
from lobot_api.structs.CharData import Buff
from lobot_api.structs.Monster import Monster
from silkroad_security_api_1_4.Packet import Packet


class WarlockDOTAddHandler(IHandler):
	on_dot_added = Event()

	@property
	def server_opcode(self) -> int:
		return 0xB0BE

	@classmethod
	def _parse_packet_(cls, p: Packet) -> Optional[IRequest]:
		skill_id = p.read_uint32()
		unique_skill_id = p.read_uint32()
		target_id = p.read_uint32()
		found, spawn = SpawnListsGlobal.try_get_spawn_object(target_id)
		if found:
			if isinstance(spawn, Monster):
				b: Buff = Buff(skill_id)
				b.unique_id = unique_skill_id
				spawn.buffs[b.unique_id] = b
				SpawnListsGlobal.SpawnBuffs[unique_skill_id] = spawn.unique_id
				cls.on_dot_added.notify(BuffArgs(b.unique_id, b.ref_id, target_id))
		return None
