from typing import Optional

from lobot_api.Bot import Bot
from lobot_api.packets.ExchangePacket import ExchangePacket
from lobot_api.packets.IRequest import IRequest
from silkroad_security_api_1_4.Packet import Packet


# # 78 1B direction angle
# # 12 B7 16 01                                       unique_id
# 78 1B angle
class RotateCharacter(ExchangePacket):
	@property
	def opcode(self) -> int: return 0x7024

	@property
	def server_opcode(self) -> int: return 0xB024

	def __init__(self, new_angle):
		self.new_angle = new_angle

	@classmethod
	def _parse_packet_(cls, p: Packet) -> Optional[IRequest]:
		unique_id = p.read_uint32()
		if unique_id == Bot.char_data.unique_id:
			new_angle = p.read_int16() * 360 / 65536
			Bot.char_data.angle = new_angle
		return None

	def _add_payload_(self) -> Packet:
		p: Packet = Packet(self.opcode)
		converted_angle = 0xFFFF & (self.new_angle * 65536 // 360)
		p.write_uint16(converted_angle)
		return p
