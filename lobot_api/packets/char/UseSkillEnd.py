from ctypes import *
from typing import Optional

from lobot_api.packets.IRequest import IRequest
from lobot_api.packets.char.SkillHandler import SkillHandler
from silkroad_security_api_1_4.Packet import Packet


# Target self
# # 01                                                success
# C3 1B 7F 01                                       own ID
# Start casting heal ghost hand lvl 3
# # 01                                                success
# 02 30                                             skilltype
# 17 06 00 00                                       skillref_id
# C3 1B 7F 01                                       caster id(self)
# C4 DD 02 00                                       unknw
# C3 1B 7F 01                                       target_id(self)
# 00                                                instantResponse
# End casting
# # 01                                                success
# C4 DD 02 00                                       unknown
# C3 1B 7F 01                                       target
# 00                                                hasDamage
class UseSkillEnd(SkillHandler):
	@property
	def server_opcode(self) -> int:
		return 0xB071

	@classmethod
	def _parse_packet_(cls, p: Packet) -> Optional[IRequest]:
		is_success = p.read_uint8()
		if is_success == 1:
			caster_id = p.read_uint32()
			target_id = p.read_uint32()
			cls.__parse_damage__(p)
		elif is_success == 2:
			error_code = p.read_uint16()
			cast_world_id = p.read_uint32()
		return None
