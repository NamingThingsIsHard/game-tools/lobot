from typing import Optional

from lobot_api.packets.IHandler import IHandler
from lobot_api.packets.IRequest import IRequest
from silkroad_security_api_1_4.Packet import Packet


# # 3D 3F 93 06                                       unique_id
# 10                                                unkn
class PickupAnimation(IHandler):
	@property
	def server_opcode(self) -> int: return 0x3036

	@classmethod
	def _parse_packet_(cls, p: Packet) -> Optional[IRequest]:
		unique_id = p.read_uint32()
		unkn = p.read_uint8()
		return None
