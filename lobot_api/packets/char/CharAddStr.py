from lobot_api.packets.IRequest import IRequest
from silkroad_security_api_1_4.Packet import Packet


class CharAddStr(IRequest):
	@property
	def opcode(self) -> int: return 0x7050

	def _add_payload_(self) -> Packet:
		return Packet(self.opcode)
