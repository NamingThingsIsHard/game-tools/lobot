from lobot_api.packets.IRequest import IRequest
from silkroad_security_api_1_4.Packet import Packet


class CharAddInt(IRequest):
	@property
	def opcode(self) -> int: return 0x7051

	def _add_payload_(self) -> Packet:
		return Packet(self.opcode)
