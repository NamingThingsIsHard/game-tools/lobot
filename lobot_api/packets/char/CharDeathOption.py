from enum import Enum


class CharDeathOption(Enum):  # byte
	SpecifiedSpot = 1
	CurrentSpot = 2
