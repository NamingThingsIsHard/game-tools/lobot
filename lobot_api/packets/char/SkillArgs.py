from lobot_api.structs.CharData import Skill


class SkillArgs:

	def __init__(self, skill: Skill = None):
		if not Skill:
			raise ValueError(Skill.__name__)
		self.skill = skill
