from ctypes import *
from typing import Optional

from lobot_api.Bot import Bot
from lobot_api.Event import Event
from lobot_api.packets.ExchangePacket import ExchangePacket
from lobot_api.packets.IRequest import IRequest
# level up antidevil bow missile level 1
# # 47 00 00 00                                       skill_id 71
# # 01                                                flag success?
# 47 00 00 00                                       skill_id 71 SKILL_CH_BOW_CRITICAL_A_01
from lobot_api.structs.CharData import Skill
from silkroad_security_api_1_4.Packet import Packet


class CharSkillUpdateLearn(ExchangePacket):
	on_packet = Event()

	@property
	def opcode(self) -> int:
		return 0x70A1

	@property
	def server_opcode(self) -> int:
		return 0xB0A1

	def __init__(self, skill_id: c_uint32):
		self.skill_id = skill_id

	@classmethod
	def _parse_packet_(cls, p: Packet) -> Optional[IRequest]:
		p.read_uint8()  # masteryLearnType success?
		skill: Skill = Skill(p.read_uint32())
		existing_skill: Skill = next((s for s in Bot.char_data.skills if s.name == skill.name), None)
		# add skill or replace existing skill
		if existing_skill is None:
			Bot.char_data.skills.append(skill)
		else:
			index: int = Bot.char_data.skills.index(existing_skill)
			if index > -1:
				Bot.char_data.skills[index].ref_id = skill.ref_id
		CharSkillUpdateLearn.on_packet.notify(skill)
		return None

	def _add_payload_(self) -> Packet:
		p: Packet = Packet(self.opcode)
		p.write_uint32(self.skill_id)
		return p
