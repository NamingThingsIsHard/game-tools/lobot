from typing import Optional

from lobot_api.packets.IHandler import IHandler
from lobot_api.packets.IRequest import IRequest
from silkroad_security_api_1_4.Packet import Packet


# # DC 0A                                             2780
# 16                                                22
# 17                                                23
class SpawnUNKNPacket(IHandler):
	@property
	def server_opcode(self) -> int: return 0x3122

	@classmethod
	def _parse_packet_(cls, p: Packet) -> Optional[IRequest]:
		unkn0 = p.read_uint16()
		unkn1 = p.read_uint8()
		unkn2 = p.read_uint8()
		return None
