from typing import Optional

from lobot_api.Bot import Bot
from lobot_api.packets.IHandler import IHandler
from lobot_api.packets.IRequest import IRequest
from lobot_api.structs.EquipmentSlot import EquipmentSlot
from silkroad_security_api_1_4.Packet import Packet


# EF 00                                             quantity 239
class UpdateAmmo(IHandler):
	@property
	def server_opcode(self) -> int: return 0x3201

	@classmethod
	def _parse_packet_(cls, p: Packet) -> Optional[IRequest]:
		Bot.char_data.inventory.equipment[EquipmentSlot.Secondary.value].stack_count = p.read_uint16()
		return None
