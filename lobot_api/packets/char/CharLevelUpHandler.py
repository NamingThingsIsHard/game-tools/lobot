from typing import Optional

from lobot_api.Bot import Bot
from lobot_api.globals.PK2DataGlobal import PK2DataGlobal
from lobot_api.globals.SpawnListsGlobal import SpawnListsGlobal
from lobot_api.packets.IHandler import IHandler
from lobot_api.packets.IRequest import IRequest
from lobot_api.structs.CharData import CharData
from silkroad_security_api_1_4.Packet import Packet


class CharLevelUpHandler(IHandler):
	@property
	def server_opcode(self) -> int:
		return 0x3054

	@classmethod
	def _parse_packet_(cls, p: Packet) -> Optional[IRequest]:
		id_ = p.read_uint32()
		if id_ == Bot.char_data.unique_id:
			curr_level_max_exp = PK2DataGlobal.level_data.get(Bot.char_data.level, None)
			if curr_level_max_exp:  # reset exp for level
				Bot.char_data.exp_offset -= curr_level_max_exp
			Bot.char_data.level += 1
			Bot.char_data.remain_stat_point += 3
			# Take multi level levelups into account
			# update level until exp no longer exceeds required exp for current level
			# do while:
			value = PK2DataGlobal.level_data.get(Bot.char_data.level, None)
			while value and value < Bot.char_data.exp_offset:
				Bot.char_data.level += 1
				Bot.char_data.remain_stat_point += 3
				value = PK2DataGlobal.level_data.get(Bot.char_data.level, None)

			return None
		elif id_ == Bot.growth_pet.unique_id:
			curr_level_max_exp = PK2DataGlobal.level_data.get(Bot.growth_pet.level, None)  # reset exp for level
			if curr_level_max_exp:
				Bot.growth_pet.exp -= curr_level_max_exp
			Bot.growth_pet.level += 1
			# Take multi level levelups into account
			# update level until exp no longer exceeds required exp for current level
			# do while :
			value = PK2DataGlobal.level_data.get(Bot.growth_pet.level, None)
			while value and value < Bot.growth_pet.exp:
				Bot.growth_pet.level += 1
				value = PK2DataGlobal.level_data.get(Bot.growth_pet.level, None)
			return None
		player: CharData = SpawnListsGlobal.Players.get(id_, None)
		if player is not None and player.unique_id != 0:
			SpawnListsGlobal.Players[id_].level += 1
		return None
