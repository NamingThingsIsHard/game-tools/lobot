from typing import Optional

from lobot_api.packets.IHandler import IHandler
from lobot_api.packets.IRequest import IRequest
from silkroad_security_api_1_4.Packet import Packet


# # 8B E4 74 01                                       temporary_id
# B4 01                                             unkn0
# 08                                                unkn1
# 19                                                unkn2
class teleporttemporary_id(IHandler):
	@property
	def server_opcode(self) -> int: return 0x3020

	@classmethod
	def _parse_packet_(cls, p: Packet) -> Optional[IRequest]:
		temp_id = p.read_uint32()
		unkn0 = p.read_uint16()
		unkn1 = p.read_uint8()
		unkn2 = p.read_uint8()
		return None
