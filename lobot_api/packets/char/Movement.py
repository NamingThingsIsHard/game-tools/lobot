from enum import Enum
from typing import Optional, Dict, Any

from PyQt5.QtCore import QTimer

from lobot_api.Bot import Bot
from lobot_api.Event import Event
from lobot_api.globals.SpawnListsGlobal import SpawnListsGlobal
from lobot_api.packets.ExchangePacket import ExchangePacket
from lobot_api.packets.IRequest import IRequest
from lobot_api.packets.char.CharStopMovementArgs import CharStopMovementArgs
from lobot_api.packets.char.MovementArgs import MovementArgs
from lobot_api.scripting.Point import Point
from silkroad_security_api_1_4.Packet import Packet


class AngleAction(Enum):
	Obsolete = 0  # GO_BACKWARDS or SPIN?
	GoForward = 1


# # from	x 6674	y 1343
# to    x 6673	y 1314
# 01                                                groundclick? yes
# A9 62                                             x_sector(169) y_sector(98)
# B3 05                                             x 1459
# 00 00                                             z 0
# 5D 06                                             y 1629
# A2 27 66 02                                       unique_id
# 01                                                groundclick? yes
# A9 62                                             x_sector(169) y_sector(98)
# B3 05                                             x 1459
# 00 00                                             z 0
# 5D 06                                             y 1629
# 01                                                hasOrigin? yes
# A9 62                                             x_sector(169) y_sector(98)
# 6C 39                                             x 14700
# D1 AE E1 C0                                       z 44753	angle 3297 * 360/65536 = 18
# C4 4A y 19140 (# switched place with angle might be server specific)
# x = ((Xsector - 135) * 192 + (Xoff# set / 100))
# y = ((Ysector - 92) * 192 + (Yoff# set / 100))
# destination
# x = ((169 - 135) * 192 + (1459 / 10)) = 6673
# y = ((98 - 92) * 192 + (1629 / 10)) = 1314
# origin
# x = ((169 - 135) * 192 + (14700 / 100)) = 6675
# y = ((98 - 92) * 192 + (19140 / 100)) = 1343
class Movement(ExchangePacket):
	on_botmovement_registered = Event()
	on_spawnmovement_registered = Event()

	""" Task to be executed after delay or cancelled when next move is started."""
	__tasks__: Dict[int, Any] = {}

	@property
	def opcode(self) -> int:
		return 0x7021

	@property
	def server_opcode(self) -> int:
		return 0xB021

	# # The move command constructor using coordinates.
	def __init__(self, to_point: Point, from_point: Point = None):
		self.to_point: Point = to_point
		self.from_point: Point = from_point

	# 01                                                flag
	# 87 5D                                             xsector 135 ysector 93
	# 99 04                                             x 1177 (to be interpreted as 117.7 for final result of 117.7 )
	# 92 00                                             z
	# 46 02                                             582 (to be interpreted as 58.2 for final result of 250.2 )
	#
	# build packet for 117,250
	#
	# xsector = x / 192 + 135 	= 117 / 192 + 135 	= 135
	# ysector = y / 192 + 92  	= 250 / 192 + 92 	= 93
	#
	# x = (xsector - 135) * 192 + xPos / 10 = (135 - 135) * 192 + xPos / 10 =
	# y = (ysector - 92) * 192 + yPos / 10  = (93 - 92) * 192 + yPos / 10   =
	def _add_payload_(self) -> Packet:
		if not Bot.char_data:
			return Packet(0)
		position = self.to_point
		p: Packet
		packet = Packet(self.opcode, True)
		packet.write_uint8(0x01)
		packet.write_uint8(position.x_sector)
		packet.write_uint8(position.y_sector)
		if Bot.char_data.is_in_cave:
			# regionX = Convert.to_byte(Bot.char.CaveFloor) // will use normal regionX for now
			region_y = 0x80
			x_offset = 0xFFFF & position.x_offset
			y_offset = 0xFFFF & position.x_offset
		else:
			x_offset = position.x_offset
			y_offset = position.y_offset
		packet.write_uint16(x_offset)
		packet.write_uint16(0)
		packet.write_uint16(y_offset)
		return packet

	@classmethod
	def _parse_packet_(cls, p: Packet) -> Optional[IRequest]:
		origin = None
		destination = Point()
		x_sector = y_sector = x_sector_origin = y_sector_origin = 0x00
		x = y = z = x_origin = y_origin = z_origin = 0
		angle = 0
		unique_id = p.read_uint32()
		is_ground_click = p.read_uint8()
		if is_ground_click == 0x01:
			x_sector = p.read_uint8()
			y_sector = p.read_uint8()
			if Bot.char_data.is_in_cave:
				x = p.read_int32()
				z = p.read_int32()
				y = p.read_int32()
			else:
				x = p.read_int16()
				z = p.read_int16()
				y = p.read_int16()
			destination = Point(x_sector, y_sector, x, z, y)
		else:
			p.read_uint8()  # Angle action
			angle = (p.read_int16() * 360) / 65536
		has_origin = p.read_uint8()
		if has_origin == 0x01:
			x_sector_origin = p.read_uint8()
			y_sector_origin = p.read_uint8()
			x_origin = p.read_int16() / 10
			angle = p.read_int16() * 360 / 65536  # angle == difference from previous position
			z_origin = p.read_int16() / 10
			y_origin = p.read_int16() / 10  # position of y_origin might be switched with angle in some versions
			origin = Point(x_sector_origin, y_sector_origin, x_origin, z_origin, y_origin, angle)
		else:
			origin = destination
		cls.handle_movement_registered(MovementArgs(unique_id, destination, origin))
		return None

	@staticmethod
	def get_walk_time(point1: Point, point2: Point) -> int:
		"""
		Get walk time from point1 to point2 in milliseconds.
		:param point1: origin
		:param point2: destination
		:return: walk time in milliseconds
		"""
		if not point2:
			return 0
		distance: float = point1.distance_point(point2)
		speed: float = Bot.transport.run_speed \
			if Bot.char_data.transport_flag and Bot.transport.run_speed > 0 \
			else Bot.char_data.current_speed
		return int(distance / (speed * 0.1)) * 1000

	@staticmethod
	def handle_char_stop_movement(e: CharStopMovementArgs):
		if e.unique_id in Movement.__tasks__:
			Movement.__tasks__[e.unique_id].stop()

	@staticmethod
	def delayed_update(e: MovementArgs):
		"""
		Wait for calculated delay before updating a gameObject's position.
		Delay depends on movement speed.
		Update can be cancelled if another movement starts before current one ends.
		:param e: Movement args
		:return:
		"""
		def update():
			if e.unique_id == Bot.char_data.unique_id:
				Bot.char_data.position = e.destination
			else:
				found_spawn, spawn = SpawnListsGlobal.try_get_spawn_object(e.unique_id)
				if found_spawn:
					spawn.position = e.destination

		if e.unique_id in Movement.__tasks__:
			Movement.__tasks__.pop(e.unique_id)

		if e.unique_id in Movement.__tasks__:
			Movement.__tasks__[e.unique_id].stop()  # TODO change items from lambda to Cancellable for this

		delay = Movement.get_walk_time(e.destination, e.origin)
		Movement.__tasks__[e.unique_id] = lambda: QTimer.singleShot(delay, update)
		Movement.__tasks__[e.unique_id]()

	@staticmethod
	def handle_movement_registered(e: MovementArgs):
		if e.unique_id == Bot.char_data.unique_id or Bot.char_data.transport_flag == 0x01 and Bot.char_data.transport_id == e.unique_id:
			if e.origin:
				Bot.char_data.position = e.origin
			if e.destination != Bot.char_data.position:  # No Origin point given in B021 packet
				# queue task to be run after delay
				Movement.delayed_update(e)
				Movement.on_botmovement_registered.notify(MovementArgs(e.unique_id, e.destination, e.origin))
		else:
			found, spawn = SpawnListsGlobal.try_get_spawn_object(e.unique_id)
			if found and e.destination != spawn.position:  # No Origin point given in B021 packet
				# queue task to be run after delay
				Movement.delayed_update(e)
				spawn.position = e.destination  # FIXME remove delay to more accurately target monsters
				Movement.on_spawnmovement_registered.notify(e)

# The command to move a player character or unit.
#
# Opcode 7021 structure
# 01
# A7 - X section# 62 - Y section# 3B 01 - X coord--> shows the X * 10 (e.g. 85 means 8.5)
# 28 00 - Z coord--> ""
# 3B 07 - Y coord--> ""
# 
# Confirming the move from Client Server sentdown
# 
# Skyclick
# # 00                                                0 flag, means no ground click
# 01                                                identifier, 01 is your own character
# AF 40                                             angle  x * 360 / 65536 ( for AF 40 -> 16559 * 360/ 65535 = 90)
# 
# opcode B021
# 
# 9B 89 00 00 Object ID# 01 Has destination(00 if sky click)# A7 X Section# Section 62 Y# 1C 02 X coord# 00 00 Z coord# C3 00 Y coord# 00 No source# 
# Can determine the ID of the character based on 2 packets
# Can determine the position of the character, or object based on the B021
# xPos: c_uint32 = 0
# yPos: c_uint32 = 0
# if X > 0 and Y > 0:
# {
# xPos = c_uint32 ((X % 192) * 10)
# yPos = c_uint32 ((Y % 192) * 10)
# }
# else:
# {
# if(X0)
# {
#  xPos = c_uint32 ((192 + (X % 192)) * 10)
#  yPos = c_uint32 ((Y % 192) * 10)
# }
# else:
# {
#  if X > 0 and Y< 0:
#  {
#   xPos = c_uint32 ((X % 192) * 10)
#   yPos = c_uint32 ((192 + (Y % 192)) * 10)
#  }
# }
# }
# x_sector = 0xFF & ((X - (int)(xPos / 10)) / 192 + 135)
# y_sector = 0xFF & ((Y - (int)(yPos / 10)) / 192 + 92)
# This is True until the opcode 1041
#
