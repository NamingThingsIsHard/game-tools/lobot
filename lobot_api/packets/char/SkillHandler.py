import logging
import sys
from abc import ABC

from PyQt5.QtCore import QTimer, QThreadPool

from lobot_api.Bot import Bot
from lobot_api.Event import Event
from lobot_api.globals.SpawnListsGlobal import SpawnListsGlobal
from lobot_api.logger.Logger import NETWORK
from lobot_api.packets.IHandler import IHandler
from lobot_api.packets.char.MovementArgs import MovementArgs
from lobot_api.scripting.Point import Point
# # 01                                                success
# 02 30                                             type
# 87 02 00 00                                       skill_id
# C3 1B 7F 01                                       caster_id
# 7E AF 02 00                                       skillunique_id
# AA 21 A2 00                                       target_id
# 00                                                instantResponse?
# # 01                                                success
# 7E AF 02 00                                       skillunique_id
# AA 21 A2 00                                       target_id
# 01                                                hasDamage
# 01                                                hitCount
# 01                                                targetCount
# AA 21 A2 00                                       target_id
# 00                                                skill effect (bitmask separated kibibyte values)
# 01 FA 04 00                                       critStatus 0xFF & ( damage (3 bytes including 1 byte next line)
# 00 00 00 00                                       damage (1 byte), unkn, unkn, unkn
# Ghost Walk Phantom
# # 01                                                successFlag // 1 = success
# 02 30                                             unkn 12290
# 08 05 00 00                                       skill_id 1288 Ghost-Walk Phantom 3 SKILL_CH_LIGHTNING_GYEONGGONG
# 31 27 86 00                                       caster_id
# AE 7F 00 00                                       unkn
# 00 00 00 00                                       target_id 0 is none
# 08                                                type = 8 = movement
# 8C 5C xsector y_sector
# 9E 02 00 00 E8 FF FF FF F5 06 00 00               x 2    z 4    y 4
from lobot_api.structs.ICombatType import ICombatType
from silkroad_security_api_1_4.Packet import Packet


class SkillHandler(IHandler, ABC):
	on_teleport_skill_used = Event()
	on_knocked_back_or_down = Event()

	KNOCK_BACK_DELAY = 1500
	STAND_UP_DELAY = 3500

	"""Flag to reset wait for knockback to wear off."""
	knockbacks = 0

	@classmethod
	def delayed_set_unset_flag(cls, kb_or_kd: bool, target_id: int, delay: int):
		def unset_kd():
			Bot.char_data.is_knocked_down = False
			cls.on_knocked_back_or_down.notify(False)

		def unset_kb():
			SkillHandler.knockbacks -= 1
			if SkillHandler.knockbacks == 0:  # > 0 means KB was reset before timer ended -> don't send event yet
				cls.on_knocked_back_or_down.notify(False)

		if target_id == Bot.char_data.unique_id:
			if kb_or_kd:
				cls.on_knocked_back_or_down.notify(True)
				Bot.char_data.is_knocked_down = True
				QTimer.singleShot(delay, unset_kd)
			else:
				cls.on_knocked_back_or_down.notify(True)
				SkillHandler.knockbacks += 1
				QTimer.singleShot(delay, unset_kb)
		else:
			found, spawn = SpawnListsGlobal.try_get_spawn_object(target_id)
			if found and isinstance(spawn, ICombatType):
				# no need for event, just check kd flag or continue attacking for kb
				pass

	@classmethod
	def set_knock_down(cls, target_id: int):
		def set_kd():
			found, spawn = SpawnListsGlobal.try_get_spawn_object(target_id)
			if found and isinstance(spawn, ICombatType):
				spawn.is_knocked_down = False

		QTimer.singleShot(SkillHandler.STAND_UP_DELAY, set_kd)

	@classmethod
	def __parse_damage__(cls, p: Packet):
		try:
			has_damage = p.read_uint8()
			if has_damage == 1:
				hit_count = p.read_uint8()
				target_count = p.read_uint8()

				for i in range(0, target_count):
					target_id = p.read_uint32()
					for ii in range(0, hit_count):
						effects = p.read_uint8()  # bitmask --> ATTACK_STATE_KNOCKBACK = 0x01,    ATTACK_STATE_BLOCK = 0x02,    ATTACK_STATE_POSITION = 0x04,   abort ? --> 0x08   ATTACK_STATE_DEAD = 0x80
						if (effects & 0x0A) != 0x00:  # if ATTACK_STATE_BLOCK or abort --> no more data
							continue
						crit_status = p.read_uint8()  # bitmask --> DAMAGE_STATE_NORMAL = 0x01,    DAMAGE_STATE_CRIT = 0x02,    DAMAGE_STATE_STATUS = 0x10
						dmg = p.read_uint32()
						unk4 = p.read_uint8()
						unk5 = p.read_uint8()
						unk6 = p.read_uint8()
						if effects in [0x04, 0x05, 0x09]:  # knockdown or knockback TODO check if this works
							if effects == 0x04:  # kd
								QThreadPool.globalInstance().start(lambda: cls.delayed_set_unset_flag(True, target_id, SkillHandler.STAND_UP_DELAY))
							else:  # kb
								QThreadPool.globalInstance().start(lambda: cls.delayed_set_unset_flag(False, target_id, SkillHandler.KNOCK_BACK_DELAY))

							x_sector = p.read_uint8()
							y_sector = p.read_uint8()
							x: float = p.read_single()
							z: float = p.read_single()
							y: float = p.read_single()
							position = Point(x_sector, y_sector, x, z, y)
							if target_id == Bot.char_data.unique_id:
								Bot.char_data.position = position
							else:
								found, spawn = SpawnListsGlobal.try_get_spawn_object(target_id)
								if found:
									spawn.position = position
			elif has_damage == 8:  # Teleport Movement
				x_sector = p.read_uint8()
				y_sector = p.read_uint8()
				x = p.read_uint32()
				z = p.read_uint32()
				y = p.read_uint32()
				Bot.char_data.position = Point(x_sector, y_sector, x, z, y)
				cls.on_teleport_skill_used.notify(MovementArgs(Bot.char_data.unique_id, Bot.char_data.position))
		except SystemError:
			sys.stdout.write(f"SkillHandler error: {format(p.opcode, '02X')}\n")
			logging.log(NETWORK, f"SkillHandler error {format(p.opcode, '02X')} - {str(p.encrypted)} - {str(p.massive)} - {' '.join([format(x, '02X') for x in p.get_bytes()])}")
