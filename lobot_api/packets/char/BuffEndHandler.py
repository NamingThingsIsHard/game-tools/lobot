from typing import Optional

from lobot_api.Bot import Bot
from lobot_api.Event import Event
from lobot_api.globals.SpawnListsGlobal import SpawnListsGlobal
from lobot_api.packets.IHandler import IHandler
from lobot_api.packets.IRequest import IRequest
from lobot_api.packets.char.BuffArgs import BuffArgs
from silkroad_security_api_1_4.Packet import Packet


# Cast Skill river fire force imbue
# # 01                                                success
# 00 30                                             unkn 0x 30 // x =  buff
# 7C 00 00 00                                       buff_id
# 54 18 11 00                                       unkn1
# BC C1 04 00
# 00 00 00 00                                       target?
# 00                                                end flag
# # 54 18 11 00                                       unkn1
# 7C 00 00 00                                       buff_id
# CC C2 04 00                                       buff unique_id
# Buff end -> imbue
# # 01                                                count ? number of Buffs ended
# CC C2 04 00                                       buff unique_id
# Fail to cast fire shield  becuase shield not equipped
# # 02                                                failure
# 0D 30                                             failure code 0x300D -> wrong weapon?


class BuffEndHandler(IHandler):
	on_buff_ended = Event()

	@property
	def server_opcode(self) -> int:
		return 0xB072

	@classmethod
	def _parse_packet_(cls, p: Packet) -> Optional[IRequest]:
		count = p.read_uint8()
		skill_id = p.read_uint32()
		for i in range(0, count):
			monster_id = SpawnListsGlobal.SpawnBuffs.get(skill_id, None)
			if monster_id:
				if monster_id in SpawnListsGlobal.MonsterSpawns:
					spawn = SpawnListsGlobal.MonsterSpawns[monster_id]
					if skill_id in spawn.buffs:
						spawn.buffs.pop(skill_id)
				SpawnListsGlobal.SpawnBuffs.pop(skill_id)
			else:
				if skill_id in Bot.char_data.active_buffs:
					b = Bot.char_data.active_buffs.pop(skill_id)
					cls.on_buff_ended.notify(BuffArgs(b.unique_id, b.ref_id))
		return None
