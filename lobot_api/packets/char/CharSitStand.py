from lobot_api.packets.IRequest import IRequest
from silkroad_security_api_1_4.Packet import Packet


class CharSitStand(IRequest):
	@property
	def opcode(self) -> int: return 0x704F

	def _add_payload_(self) -> Packet:
		p: Packet = Packet(self.opcode)
		p.write_uint8(0x04)
		return p
