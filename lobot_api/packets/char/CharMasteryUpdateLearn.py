from ctypes import *
from typing import Optional

from lobot_api.Bot import Bot
from lobot_api.packets.ExchangePacket import ExchangePacket
from lobot_api.packets.IRequest import IRequest
from lobot_api.structs.CharData import Mastery
from silkroad_security_api_1_4.Packet import Packet


# level up fire mastery to level 5
# # 13 01 00 00                                       mastery_id
# 01                                                masteryLearnType
# # 01                                                masteryLearnType
# 13 01 00 00                                       mastery_id
# 05                                                mastery level


class CharMasteryUpdateLearn(ExchangePacket):
	@property
	def server_opcode(self) -> int:
		return 0xB0A2

	@property
	def opcode(self) -> int:
		return 0x70A2

	def __init__(self, mastery_id: c_uint32, mastery_learn_type: c_byte):
		self.mastery_id = mastery_id
		self.LearnType = mastery_learn_type

	@classmethod
	def _parse_packet_(cls, p: Packet) -> Optional[IRequest]:
		p.read_uint8()  # masteryLearnType success?
		mastery_id = p.read_uint32()
		mastery_level = p.read_uint8()

		mastery: Mastery = next((m for m in Bot.char_data.masteries if m.ID == mastery_id), None)
		if mastery:
			mastery.level = mastery_level
		else:
			m: Mastery = Mastery()
			m.ID = mastery_id
			m.level = mastery_level
			Bot.char_data.masteries.append(mastery_id, m)
		return None

	def _add_payload_(self) -> Packet:
		p: Packet = Packet(self.opcode)
		p.write_uint32(self.mastery_id)
		p.write_uint8(self.LearnType)
		return p
