from ctypes import *
from typing import Optional

from lobot_api.Bot import Bot
from lobot_api.globals.PK2DataGlobal import PK2DataGlobal
from lobot_api.packets.IHandler import IHandler
from lobot_api.packets.IRequest import IRequest
from silkroad_security_api_1_4.Packet import Packet


# Inpanic server
# sp exp update
# # 26 A1 01 00                                       &...............
# 98 08 00 00 00 00 00 00                           ................
# 98 08 00 00 00 00 00 00                           ................
# 00                                                ................
# # 97 1F F1 00                                       unknown unique_id?
# 8C 0A 00 00 00 00 00 00                           expAfter expBefore
# 40 1A 00 00 00 00 00 00                           spAfter spBefore
# 00                                                unknown flag
# 0C 00                                             unknown 12 
# level up from 5 to 7
# # 0B 0B 02 03                                       unique_id
# # D7 7D E9 02                                       unkn
# 00 32 00 00 00 00 00 00                           expAfter expBefore
# 62 2A 00 00 00 00 00 00                           spAfter spBefore
# 00                                                leveldown flag?
# 06 00                                             last level?
# level up from 7 to 13 18797 exp
# # 6A 4E 02 03                                       unique_id
# # C2 9C E9 02                                       unkn
# 77 72 02 00 00 00 00 00                           expAfter expBefore
# B0 7B 01 00 00 00 00 00                           spAfter spBefore
# 00                                                leveldown flag?
# 12 00                                             level: last = 12 ?
# 12|47940
class CharExpSpUpdate(IHandler):
	@property
	def server_opcode(self) -> int:
		return 0x3056

	@classmethod
	def _parse_packet_(cls, p: Packet) -> Optional[IRequest]:
		monster_id = p.read_uint32()  # monster that died
		final_exp = p.read_uint32() - p.read_uint32()
		final_sp_exp = p.read_uint32() - p.read_uint32()  ## get single sp point
		new_exp = Bot.char_data.exp_offset + final_exp
		new_offset = c_uint32(new_exp - PK2DataGlobal.level_data[Bot.char_data.level])
		current_max_exp = PK2DataGlobal.level_data.get(Bot.char_data.level, None)
		if current_max_exp:
			if new_exp < 0:  # Exp decrease and level decrease , e.g. due to death
				Bot.char_data.exp_offset += new_exp
				Bot.char_data.level -= 1
			else:
				Bot.char_data.exp_offset = new_exp
				# take multiple level ups into account
				while Bot.char_data.exp_offset > current_max_exp:
					next_level_max_exp = PK2DataGlobal.level_data.get(Bot.char_data.level + 1, None)
					if next_level_max_exp:
						Bot.char_data.level += 1
						Bot.char_data.remain_stat_point += 3
						Bot.char_data.exp_offset -= current_max_exp
						current_max_exp = next_level_max_exp
					else:
						pass  # exit loop if max level exp is somehow less than current exp
		return None
