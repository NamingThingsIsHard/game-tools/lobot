from typing import Optional

from lobot_api.scripting.Point import Point


class MovementArgs:
	def __init__(self, unique_id, destination: Point = Point(), origin: Optional[Point] = None):
		self.unique_id = unique_id
		self.destination = destination
		self.origin = origin
