from lobot_api.structs import Monster


class MonsterAggroArgs:

	def __init__(self, monster: Monster):
		self.monster = monster
