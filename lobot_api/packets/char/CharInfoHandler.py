from typing import Optional

from lobot_api.Bot import Bot
from lobot_api.Event import Event
from lobot_api.packets.IHandler import IHandler
from lobot_api.packets.IRequest import IRequest
from silkroad_security_api_1_4.Packet import Packet


# # //Opcode: 0x303D
# Name: SERVER_AGENT_CHARACTER_STATS
# Description:
# Encryption: False
# Massive: False
# public const ushort SERVER_AGENT_CHARACTER_STATS = 0x303D
# 	4	uint	PhyAtkMin
# 	4	uint	PhyAtkMax
# 	4	uint	MagAtkMin
# 	4	uint	MagAtkMax
# 	2	ushort	PhyDef
# 	2	ushort	MagDef
# 	2	ushort	HitRate
# 	2	ushort	ParryRate
# 	4	uint	max_hp
# 	4	uint	max_mp
# 	2	ushort	STR
# 	2	ushort	INT
class CharInfoHandler(IHandler):
	on_parsed_info = Event()  # After teleporting and parsing this packet, the character can walk

	@property
	def server_opcode(self) -> int: return 0x303D

	@classmethod
	def _parse_packet_(cls, p: Packet) -> Optional[IRequest]:
		Bot.char_info.PhyAtkMin = p.read_uint32()
		Bot.char_info.PhyAtkMax = p.read_uint32()
		Bot.char_info.MagAtkMin = p.read_uint32()
		Bot.char_info.MagAtkMax = p.read_uint32()
		Bot.char_info.PhyDef = p.read_uint16()
		Bot.char_info.MagDef = p.read_uint16()
		Bot.char_info.HitRate = p.read_uint16()
		Bot.char_info.ParryRate = p.read_uint16()
		Bot.char_info.max_hp = p.read_uint32()
		Bot.char_info.max_mp = p.read_uint32()
		Bot.char_info.STR = p.read_uint16()
		Bot.char_info.INT = p.read_uint16()
		cls.on_parsed_info.notify()
		return None

