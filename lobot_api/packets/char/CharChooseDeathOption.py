from lobot_api.packets.IRequest import IRequest
from lobot_api.packets.char.CharDeathOption import CharDeathOption
from silkroad_security_api_1_4.Packet import Packet


# # 01                                                optionFlag 1= revive specified spot
# # 02                                                2: optionFlag = revive current spot
class CharChooseDeathOption(IRequest):
	@property
	def opcode(self) -> int: return 0x3053

	def __init__(self, option: CharDeathOption):
		self.Option = option

	def _add_payload_(self) -> Packet:
		p: Packet = Packet(self.opcode)
		p.write_uint8(self.Option)
		return p
