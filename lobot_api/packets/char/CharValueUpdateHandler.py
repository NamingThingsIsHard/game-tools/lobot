from enum import Enum
from typing import Optional

from lobot_api.Bot import Bot
from lobot_api.Event import Event
from lobot_api.packets.IHandler import IHandler
from lobot_api.packets.IRequest import IRequest
from silkroad_security_api_1_4.Packet import Packet


# 01                                                update_type 1
# CC 01 00 00 00 00 00 00                           amount
# 00                                                unknown
class CharUpdateType(Enum):  # : byte
	Gold = 1
	SP = 2
	# EXP = 3, ?
	Zerk = 4


class CharValueUpdateHandler(IHandler):
	on_gold_updated = Event()
	on_sp_updated = Event()
	on_zerk_updated = Event()


	@property
	def server_opcode(self) -> int:
		return 0x304E

	@classmethod
	def _parse_packet_(cls, p: Packet) -> Optional[IRequest]:
		# switch (CharUpdateType(p.read_uint8()))
		t = CharUpdateType(p.read_uint8())
		if t == CharUpdateType.Gold:
			Bot.char_data.remain_gold = p.read_uint64()
			cls.on_gold_updated.notify(Bot.char_data.remain_gold)
		# p.read_uint8() unknown
		elif t == CharUpdateType.SP:
			Bot.char_data._sp = p.read_uint32()
			cls.on_sp_updated.notify(Bot.char_data._sp)
		elif t == CharUpdateType.Zerk:
			Bot.char_data.remain_hwan_count = p.read_uint8()
			cls.on_zerk_updated.notify()
		else:
			pass
		return None
