from lobot_api.packets.char.ObjectAction import ObjectAction
from lobot_api.packets.char.ObjectActionType import ObjectActionType
from silkroad_security_api_1_4.Packet import Packet


# Cast anti devil bow
# # 01                                                action phase flag // 1 = start 2 = cancel/complete
# 04                                                type
# 47 00 00 00                                       skill_ref_id
# 01                                                targeted ?
# F3 17 A5 01                                       target_id
# # 01                                                confirmation flag
# 01                                                secondary cast when in range ?
# Start autoattack
# # 01                                                action phase flag // 1 = start 2 = cancel/complete
# 01                                                type ? attack
# 01                                                targeted flag?
# F3 17 A5 01                                       uniqueId
# # 01                                                confirmation flag
# 01                                                secondary cast when in range ?
class UseSkill(ObjectAction):

	def __init__(self, ref_id, type_value: ObjectActionType, target_id=0):
		self.skill_ref_id = ref_id
		self.type = type_value
		self.target_id = target_id

	def _add_payload_(self) -> Packet:
		p: Packet = Packet(self.opcode, False)
		p.write_uint8(0x01)
		# switch (cls.type())
		if self.type == ObjectActionType.AutoAttack:
			p.write_uint8(0x01)
			p.write_uint8(0x01)  # targeted ?
			p.write_uint32(self.target_id)
		elif self.type == ObjectActionType.UseSkill:
			p.write_uint8(0x04)  # object action type ?
			p.write_uint32(self.skill_ref_id)
			if self.target_id != 0x00:
				p.write_uint8(0x01)
				p.write_uint32(self.target_id)
			else:
				p.write_uint8(0x00)
		else:
			pass
		return p
