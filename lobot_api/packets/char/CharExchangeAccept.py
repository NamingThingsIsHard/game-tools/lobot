from lobot_api.packets.IRequest import IRequest
from silkroad_security_api_1_4.Packet import Packet


class CharExchangeAccept(IRequest):
	@property
	def opcode(self) -> int: return 0x7074

	def _add_payload_(self) -> Packet:
		raise NotImplementedError()
