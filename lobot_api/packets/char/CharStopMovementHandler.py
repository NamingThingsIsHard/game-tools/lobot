from typing import Optional

from lobot_api.Bot import Bot
from lobot_api.Event import Event
from lobot_api.packets.IHandler import IHandler
from lobot_api.packets.IRequest import IRequest
from lobot_api.packets.char.CharStopMovementArgs import CharStopMovementArgs
from lobot_api.scripting.Point import Point
from silkroad_security_api_1_4.Packet import Packet


class CharStopMovementHandler(IHandler):
	""" The packet signalling that the character has (been) stopped.
	This includes: Collision with environment, picking up items and any action that requires walking to stop.
	83 5C 5D 05                                       unique_id
	8A 5A                                             x_sector, y_sector
	3E 73 86 42 4F 6F 46 40 0C 5C EF 44 4B 35         x 4f, z 4f, y 4f, angle
	"""
	on_char_stop_movement = Event()

	@property
	def server_opcode(self) -> int: return 0xB023

	@classmethod
	def _parse_packet_(cls, p: Packet) -> Optional[IRequest]:
		unique_id = p.read_uint32()
		if unique_id == Bot.char_data.unique_id:
			x_sector = p.read_uint8()
			y_sector = p.read_uint8()
			x: float = p.read_single()
			z: float = p.read_single()
			y: float = p.read_single()
			Bot.char_data.angle = p.read_uint16() * 360 / 65536
			Bot.char_data.position = Point(x_sector, y_sector, x, z, y)
		cls.on_char_stop_movement.notify(CharStopMovementArgs(unique_id))
		return None
