from typing import Optional

from lobot_api.Bot import Bot
from lobot_api.BotStatus import BotStatus
from lobot_api.Event import Event
from lobot_api.packets.IHandler import IHandler
from lobot_api.packets.IRequest import IRequest
from silkroad_security_api_1_4.Packet import Packet


class CharDeathHandler(IHandler):
	on_char_died = Event()

	@property
	def server_opcode(self) -> int:
		return 0x3011

	# [S -> C] [3011]
	# 04        deathFlag 4
	@classmethod
	def _parse_packet_(cls, p: Packet) -> Optional[IRequest]:
		# switch (p.read_uint8())
		if p.read_uint8() == 0x04:
			Bot.char_data.is_alive = False
			Bot.set_status(BotStatus.Idle)
			cls.on_char_died.notify()
		else:
			pass
		return None
