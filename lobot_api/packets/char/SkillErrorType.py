from enum import Enum


class SkillErrorType(Enum):  # byte
	Obstacle = 0x10
	SkillOnCooldown = 0x05
	InvalidTarget = 0x06
	WrongWeapon = 0x0D
	NoBolts = 0x0E
