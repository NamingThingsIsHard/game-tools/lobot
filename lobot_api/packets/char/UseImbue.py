from lobot_api.packets.char.ObjectAction import ObjectAction
from silkroad_security_api_1_4.Packet import Packet


# cast fire imbue
# # 01                                                action phase flag // 1 = start 2 = cancel/complete
# 04                                                type ? buff
# 7C 00 00 00                                       ref_id
# 00                                                targetted ?
# # 01                                                confirmation flag
# 01                                                secondary cast when in range ?
class UseImbue(ObjectAction):
	def __init__(self, ref_id):
		self.imbue_ref_id = ref_id

	def _add_payload_(self) -> Packet:
		p: Packet = Packet(self.opcode, False)
		p.write_uint8(0x01)
		p.write_uint8(0x04)  # object action type ?
		p.write_uint32(self.imbue_ref_id)
		p.write_uint8(0x00)
		return p
