from lobot_api.packets.char.ObjectAction import ObjectAction
from silkroad_security_api_1_4.Packet import Packet


# Start- then cancel autoattack on big-eyed ghost
# # 01                                                action phase flag // 1 = start 2 = cancel/complete
# 01                                                type ? attack
# 01                                                targetted flag?
# F3 17 A5 01                                       uniqueId
# # 01                                                confirmation flag
# 01                                                secondary cast when in range ?
# # 02                                                cancellation flag
# # 02                                                cancellation flag
# 00                                                unknown flag
# # 02                                                ................
# 00                                                ................
class ObjectActionCancel(ObjectAction):
	def _add_payload_(self) -> Packet:
		return Packet(self.opcode, False, False, [0x02])
