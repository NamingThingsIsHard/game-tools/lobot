from lobot_api.packets.char.SkillErrorType import SkillErrorType


class SkillErrorArgs:

	def __init__(self, type_value: SkillErrorType):
		self.type = type_value
