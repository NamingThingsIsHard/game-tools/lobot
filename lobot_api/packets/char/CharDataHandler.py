from typing import Optional

from lobot_api.Bot import Bot
from lobot_api.BotStatus import BotStatus
from lobot_api.Event import Event
from lobot_api.globals.SpawnListsGlobal import SpawnListsGlobal
from lobot_api.packets.IHandler import IHandler
from lobot_api.packets.IRequest import IRequest
from lobot_api.packets.char.MovementArgs import MovementArgs
from lobot_api.packets.inventory import ItemParser
from lobot_api.pk2.PK2Item import Country
from lobot_api.statemachine.AutoPotions import AutoPotions
from lobot_api.structs import EquipmentSlot
from lobot_api.structs.CharData import Mastery, Skill, Quest, Objective, Buff, Hotkey, BlockedPlayer
from lobot_api.structs.DebuffStatus import DebuffStatus
from lobot_api.structs.Item import Item
from lobot_api.structs.ObjectStatus import ObjectStatus
from silkroad_security_api_1_4.Packet import Packet


class CharDataHandler(IHandler):
	on_packet = Event()

	@property
	def server_opcode(self) -> int:
		return 0x3013

	@classmethod
	def _parse_packet_(cls, p: Packet) -> Optional[IRequest]:
		p.lock()
		Bot.char_data.server_time = p.read_uint32()
		Bot.char_data.ref_id = p.read_uint32()
		Bot.char_data.country = Country.Chinese if Bot.char_data.ref_id <= Country.MAX_CHINESE.value else Country.Euro
		Bot.char_data.scale = p.read_uint8()
		Bot.char_data.level = p.read_uint8()
		Bot.char_data.max_level = p.read_uint8()
		Bot.char_data.exp_offset = p.read_uint64()
		Bot.char_data.SExpOffset = p.read_uint32()
		Bot.char_data.remain_gold = p.read_uint64()
		Bot.char_data.sp = p.read_uint32()
		Bot.char_data.remain_stat_point = p.read_uint16()
		Bot.char_data.remain_hwan_count = p.read_uint8()
		Bot.char_data.gathered_exp_point = p.read_uint32()
		Bot.char_data.current_hp = p.read_uint32()
		Bot.char_data.current_mp = p.read_uint32()
		Bot.char_data.AutoInverstExp = p.read_uint8()  # (1 = Beginner Icon, 2 = Helpful, 3 = Beginner&Helpful)
		Bot.char_data.daily_pk = p.read_uint8()
		Bot.char_data.total_pk = p.read_uint16()
		Bot.char_data.PKPenaltyPoint = p.read_uint32()
		Bot.char_data.HwanLevel = p.read_uint8()
		Bot.char_data.free_pvp = p.read_uint8()  # 0 = None, 1 = Red, 2 = Gray, 3 = Blue, 4 = White, 5 = Gold
		Bot.char_data.inventory.size = p.read_uint8()
		Bot.char_data.inventory.item_count = p.read_uint8()
		# set inventory
		Bot.char_data.inventory.equipment.clear()
		Bot.char_data.inventory.items.clear()
		for i in range(0, Bot.char_data.inventory.item_count):
			item: Item = ItemParser.parse_item(p)
			if item.slot < EquipmentSlot.MAX_EQUIP_SLOTS:
				Bot.char_data.inventory.equipment[item.slot] = item
			else:
				Bot.char_data.inventory.items[item.slot] = item

		Bot.char_data.inventory.avatar_inventory.size = p.read_uint8()
		Bot.char_data.inventory.avatar_inventory.item_count = p.read_uint8()
		for i in range(0, Bot.char_data.inventory.avatar_inventory.item_count):
			item: Item = ItemParser.parse_item(p)
			Bot.char_data.inventory.avatar_inventory.items[item.slot] = item

		Bot.char_data.has_mask = p.read_uint8()  # -> Check for != 0 (MaskFlag?)
		Bot.char_data.mastery_flag = p.read_uint8()
		while Bot.char_data.mastery_flag == 1:
			m: Mastery = Mastery()
			m.ID = p.read_uint32()
			m.level = p.read_uint8()
			Bot.char_data.masteries.append(m)
			Bot.char_data.mastery_flag = p.read_uint8()  # (0 = done, 1 = Mastery)

		end_byte = p.read_uint8()  # Mastery end byte
		Bot.char_data.skill_flag = p.read_uint8()

		while Bot.char_data.skill_flag == 1:
			s: Skill = Skill(p.read_uint32())
			s.skill_enabled = p.read_uint8()
			# Add distinct skills
			if s in Bot.char_data.skills:
				# replace old version of skill if found
				existing_skill: Skill = next((skill for skill in Bot.char_data.skills if skill.name == s.name), None)
				if existing_skill:
					index = Bot.char_data.skills.index(existing_skill)
					Bot.char_data.skills[index] = s

			else:
				Bot.char_data.skills.append(s)
			Bot.char_data.skill_flag = p.read_uint8()  # (0 = done, 1 = Skill)
		# sorted(Bot.char_data.skills, key=operator.attrgetter('Name'))
		# TODO unknown
		# unknown = p.read_uint8()

		Bot.char_data.completed_quest_count = p.read_uint16()
		for i in range(0, Bot.char_data.completed_quest_count):
			Bot.char_data.completed_quests.append(p.read_uint32())

		Bot.char_data.active_quest_count = p.read_uint8()
		for i in range(0, Bot.char_data.active_quest_count):
			q: Quest = Quest()
			q.ref_id = p.read_uint32()
			q.AchievementCount = p.read_uint8()  # (Repetition Amount = Bit and Completetion Amount = Bit)
			q.RequiresSharePt = p.read_uint8()  # -> Check for != 0
			q.type = p.read_uint8()  # (8 = , 24 = , 28 = , 88 = )
			if q.type == 28:
				remaining_time = p.read_uint32()

			q.Status = p.read_uint8()  # (1 = Untouched, 7 = Started, 8 = Complete)
			if q.type != 8:
				q.objective_count = p.read_uint8()
				for objectiveindex in range(0, q.objective_count):
					o: Objective = Objective()
					o.ID = p.read_uint8()
					o.Status = p.read_uint8()  # (00 = done, 01 = incomplete)
					# o.nameLength = p.read_uint16()
					o.name = p.read_ascii()
					o.task_count = p.read_uint8()

					for k in range(0, o.task_count):
						o.Tasks.append(p.read_uint32())  # (=> Killed monsters Collected items)
					q.Objectives.append(o)
			if q.type == 88:
				q.task_count = p.read_uint8()
				for j in range(0, q.task_count):
					q.taskrefobj_id.append(p.read_uint32())  # (=> NPCs to deliver to,# get reward)
			Bot.char_data.active_quests.append(q)
		Bot.char_data.unk05 = p.read_uint8()  # -> Check for != 0
		Bot.char_data.collection_book_count = p.read_uint32()  # -> Check for != 0
		for i in range(0, Bot.char_data.collection_book_count):
			theme_index = p.read_uint32()
			theme_started_datetime = p.read_uint32()  # SROTimeStamp
			theme_pages = p.read_uint32()
		Bot.char_data.unique_id = p.read_uint32()
		x_sector = p.read_uint8()
		y_sector = p.read_uint8()  # (=> XSec YSec)
		x_offset = p.read_single()
		z_offset = p.read_single()
		y_offset = p.read_single()
		angle = p.read_uint16() * 360 / 65536
		has_destination = p.read_uint8()
		Bot.char_data.movement_type = p.read_uint8()  # (0 = Walking, 1 = Running)
		if Bot.char_data.has_destination == 1:
			x_sector = p.read_uint8()
			y_sector = p.read_uint8()
			if not Bot.char_data.is_in_cave:  # In world
				x_offset = p.read_uint16()
				z_offset = p.read_uint16()
				y_offset = p.read_uint16()
			else:  # In cave
				x_offset = p.read_uint32()
				z_offset = p.read_uint32()
				y_offset = p.read_uint32()
			Bot.char_data.destination.set_from_pointPoint(x_sector, y_sector, x_offset, z_offset, y_offset, angle)
		else:
			Bot.char_data.sky_click_flag = p.read_uint8()  # (1 = Sky-/ArrowKey-walking)
			Bot.char_data.position.angle = p.read_uint16() * 360 / 65536
		Bot.char_data.position.set_from_values(x_sector, y_sector, x_offset, z_offset, y_offset, angle)
		Bot.char_data.on_moved.notify(MovementArgs(Bot.char_data.unique_id, Bot.char_data.position, Bot.char_data.position))

		Bot.char_data.is_alive = p.read_uint8() != 0x02  # now anything != 2 seems to be alive --- OLD(1 = Alive, 2 = Dead)
		Bot.char_data.debuff_flags = p.read_uint8()  # -> Check for != 0
		Bot.char_data.debuff_status = DebuffStatus(Bot.char_data.debuff_flags)  # -> Check for != 0
		Bot.char_data.motion_state = p.read_uint8()  # (0 = None, 2 = Walking, 3 = Running, 4 = Sitting)
		Bot.char_data.status = ObjectStatus(p.read_uint8())  # (0 = None,2 = ??@GrowthPet, 3 = Invincible, 4 = Invisible)
		Bot.char_data.walk_speed = p.read_single()
		Bot.char_data.run_speed = p.read_single()
		Bot.char_data.hwan_speed = p.read_single()
		Bot.char_data.active_buff_count = p.read_uint8()

		for i in range(0, Bot.char_data.active_buff_count):
			b: Buff = Buff(p.read_uint32())
			b.buff_duration = p.read_uint32()
			# b.RefSkillParam = p.read_uint32()
			if b.pk2_skill.is_transferable_buff():  # b.RefSkillParam == 1701213281) // -> atfe -> "auto transfer effect" like Recovery Division:
				b.is_creator = p.read_uint8()
		Bot.char_data.name = p.read_ascii()
		Bot.char_data.job_name = p.read_ascii()
		Bot.char_data.job_type = p.read_uint8()  # (0 = None, 1 = Trader, 2 = Thief, 3 = Hunter)
		Bot.char_data.job_level = p.read_uint8()
		Bot.char_data.job_exp = p.read_uint32()
		Bot.char_data.job_contribution = p.read_uint32()
		Bot.char_data.job_reward = p.read_uint32()
		Bot.char_data.PVP_state = p.read_uint8()  # -> Check for != 0	(According to Spawn structure => MurderFlag 0 = White, 1 = Purple, 2 = Red)
		Bot.char_data.transport_flag = p.read_uint8()  # -> Check for != 0	(According to Spawn structure => RideFlag or AttackFlag?)
		Bot.char_data.in_combat = p.read_uint8()  # -> Check for != 0	(According to Spawn structure => EquipmentCountdown?)
		if Bot.char_data.transport_flag == 0x01:
			Bot.char_data.transport_id = p.read_uint32()
		Bot.char_data.PVP_flag = p.read_uint8()  # Flag(0 = Red Side, 1 = Blue Side, 255 = Disable(None), 34 = Enable)
		Bot.char_data.guide_flag = p.read_uint64()
		Bot.char_data.account_id = p.read_uint32()  # (=> gameaccount_id) j_id (Joymax ID)
		Bot.char_data.GM_flag = p.read_uint8()
		Bot.char_data.activation_flag = p.read_uint8()  # ConfigType:0 --> (0 = Not activated, 7 = activated)
		Bot.char_data.hot_key_count = p.read_uint8()

		for i in range(0, Bot.char_data.hot_key_count):
			h: Hotkey = Hotkey()
			h.slotSeq = p.read_uint8()
			h.slotType = p.read_uint8()  # (37 = COS Command, 70 = InventoryItem, 71 = EquipedItem, 73 = Skill, 74 = Action, 78 = EquipedAvatar)
			h.Data = p.read_uint32()
			Bot.char_data.hot_keys.append(h)
		Bot.char_data.hp_slot = p.read_uint8()  # (FValue  10 + Slot)
		Bot.char_data.hp_value = p.read_uint8()  # (Enabled = 128 + Value)
		Bot.char_data.mp_slot = p.read_uint8()  # (FValue  10 + Slot)
		Bot.char_data.mp_value = p.read_uint8()  # (Enabled = 128 + Value)
		Bot.char_data.universal_slot = p.read_uint8()  # (Enabled = 128 + Value)
		Bot.char_data.universal_value = p.read_uint8()  # (Enabled = 128,0 = Disabled)
		Bot.char_data.potion_delay = p.read_uint8()  # (Enabled = 128 + Value)
		Bot.char_data.blocked_play_count = p.read_uint8()

		for i in range(0, Bot.char_data.blocked_play_count):
			b: BlockedPlayer = BlockedPlayer()
			# b.target_name_length = p.read_uint16()
			b.target_name = p.read_ascii()
			Bot.char_data.blocked_players.append(b)
		Bot.char_data.unk13 = p.read_uint32()
		Bot.char_data.unk14 = p.read_uint8()
		Bot.set_status(BotStatus.Idle)

		Bot.char_data.active_buffs.clear()
		SpawnListsGlobal.remove_all_spawn_entries()
		CharDataHandler.on_packet.notify()
		AutoPotions.handle_char_data_parsed(Bot.char_data.country)
		return None
