from ctypes import *

from lobot_api.packets.char.ObjectAction import ObjectAction
from silkroad_security_api_1_4.Packet import Packet


# # 01                                                action phase flag // 1 = start 2 = cancel/complete
# 03                                                type
# 01                                                targetted?
# 1C 80 A8 01                                       target_id
# # 01                                                ................
# 01                                                ................
# # 02                                                stop action
# # 02                                                stop confirm
# 00                                                flag
# # 02                                                ................
# 00                                                ................
class TraceChar(ObjectAction):
	def __init__(self, target_id: c_uint32):
		self.target_id = target_id

	def _add_payload_(self) -> Packet:
		p: Packet = Packet(self.opcode)
		p.write_uint8(0x01)
		p.write_uint8(0x03)
		p.write_uint8(0x01)
		p.write_uint32(self.target_id)
		return p
