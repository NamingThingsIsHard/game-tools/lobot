from typing import *

from lobot_api.Event import Event
from lobot_api.globals.settings.StallSettings import StallSettings
from lobot_api.packets.ExchangePacket import ExchangePacket
from lobot_api.packets.IRequest import IRequest
from lobot_api.packets.inventory import ItemParser
from lobot_api.packets.stall.StallUpdateType import StallUpdateType
from lobot_api.structs.Item import Item, StallItem
from silkroad_security_api_1_4.Packet import Packet


class StallHandler(ExchangePacket):
	DEFAULT_MESSAGE = "Welcome to my Stall!"
	DEFAULT_NAME = "My Stall"
	on_stallitem_created = Event()
	on_stallitem_removed = Event()

	@property
	def opcode(self) -> int:
		return 0x70BA

	@property
	def server_opcode(self) -> int:
		return 0xB0BA

	def __init__(
			self,
			update_type: StallUpdateType,
			slot: int,
			source_slot: int,
			stack_count: int,
			price: int,
			flea_market_id_group: int,
			is_open=True,
			stall_network_result: int = 0x01,
			message: str = DEFAULT_MESSAGE,
			name=DEFAULT_NAME,
			unk_ushort0=0x00
	):
		self.update_type = update_type
		self.slot = slot
		self.source_slot = source_slot
		self.stack_count = stack_count
		self.price = price
		self.flea_market_id_group = flea_market_id_group
		self.flea_market_mode = flea_market_id_group
		self.is_open = is_open
		self.stall_network_result = stall_network_result
		self.message = message
		self.name = name
		self.unk_ushort0 = unk_ushort0

	def _add_payload_(self) -> Packet:
		p = Packet(self.opcode)
		p.write_uint8(self.update_type)
		if self.update_type == StallUpdateType.UpdateItem:
			p.write_uint8(self.slot)  # within Stall (0-9)
			p.write_uint16(self.stack_count)
			p.write_uint64(self.price)
			p.write_uint16(self.unk_ushort0)
		elif self.update_type == StallUpdateType.AddItem:
			p.write_uint8(self.slot)  # within Stall (0-9)
			p.write_uint8(self.source_slot)  # from ownerInventory
			p.write_uint16(self.stack_count)
			p.write_uint64(self.price)
			p.write_uint32(self.flea_market_id_group)
			p.write_uint16(self.unk_ushort0)
		elif self.update_type == StallUpdateType.RemoveItem:
			p.write_uint8(self.slot)  # within Stall (0-9)
			p.write_uint16(self.unk_ushort0)
		elif self.update_type == StallUpdateType.FleaMarketMode:
			p.write_uint8(
				self.flea_market_mode)  # no noticable effects 1 and 2 responded with success, everything > 3 with errorCode 0x3C2B
		elif self.update_type == StallUpdateType.State:
			p.write_uint8(self.is_open)
			p.write_uint16(
				self.stall_network_result)  # 01 00 = Registering of stall items at stall network is successful
		elif self.update_type == StallUpdateType.Message:
			p.write_ascii(self.message)
		elif self.update_type == StallUpdateType.Name:
			p.write_ascii(self.name)
		return p

	@classmethod
	def _parse_packet_(cls, p: Packet) -> Optional[IRequest]:
		result = p.read_uint8()
		update_type = p.read_uint8()
		if update_type == StallUpdateType.UpdateItem.value:
			slot = p.read_uint8()  # within Stall (0-9)
			StallSettings.stall_items[slot].slot = slot
			StallSettings.stall_items[slot].stack_count = p.read_uint16()
			StallSettings.stall_items[slot].price = p.read_uint64()
			error_code = p.read_uint16()
		elif update_type in [StallUpdateType.AddItem.value, StallUpdateType.RemoveItem.value]:
			# Seems to refresh entire list of items in stall
			StallSettings.stall_items.clear()
			error_code = p.read_uint16()
			while True:
				slot = p.read_uint8()  # within Stall (0-9)
				if slot == 0xFF:
					break

				item: Item = ItemParser.parse_stall_item(p)  # depends on TypeIDs
				item.slot = slot
				source_slot = p.read_uint8()  # from OwnerInventory
				stack_count = p.read_uint16()  # sale stack count
				price = p.read_uint64()  # sale price
				StallSettings.stall_items[slot] = StallItem(item, slot, source_slot, stack_count, price)
		elif update_type == StallUpdateType.FleaMarketMode.value:
			flea_market_mode = p.read_uint8()  # no noticable effects 1 and 2 responded with success, everything > 3 with errorCode 0x3C2B
		elif update_type == StallUpdateType.State.value:
			StallSettings.is_open = p.read_uint8() == 0x01
			stall_network_result = p.read_uint16()  # 01 00 = Registering of stall items at stall network is successful
		elif update_type == StallUpdateType.Message.value:
			StallSettings.message = p.read_ascii()
		elif update_type == StallUpdateType.Name.value:
			# via 0x30BB - SERVER_AGENT_ENTITY_UPDATE_STALL_NAME
			pass
		return None
	# Previous items for comparison
	# old_items: List[StallItem] = StallSettings.StallItems
	# # set stall
	# StallSettings.StallItems.clear()
	# for i in range(0, StallSettings.MAX_STALL_ITEMS):
	# 	new_item: Item = ItemParser.parse_item(p)
	#
	# 	if new_item.slot == 0xFF:
	# 		break
	#
	# 	sale_price = p.read_uint64()
	# 	stall_item: StallItem = StallItem(new_item, new_item.slot, sale_price)
	# 	bisect.insort_left(stall_item, StallSettings.StallItems)

	# All removed items - in old but not in new
	# for StallItem stallItem in oldItems.Where(i => not i in StallSettings.StallItems):
	# on_stallitem_removed.notify(StallItemArgs(stallItem))

	# All added items - in but not in old
	# for stallItem in filter(i in oldItems for i in StallSettings.StallItems):
	# on_stallitem_created.notify(StallItemArgs(stallItem))
