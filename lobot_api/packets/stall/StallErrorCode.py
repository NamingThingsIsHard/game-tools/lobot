from ctypes import c_uint32
from enum import IntEnum


class StallErrorCode(IntEnum):  # :c_uint32
	"""007B9C50. 3D 083C0000 CMP EAX,3C08 """
	#	Switch(# cases 3..3C55)

	# Invalid command
	uiit_stt_err_common_inval_id_OPERATION: c_uint32 = 0x5
	# ## set the value only from 1 to 1 billion gold.#
	uiit_msg_fleamarket_err_inval_id_PRICE: c_uint32 = 0x3C08
	# Cannot open a shop whithout the registered goods.
	UIIT_MSG_FLEAMARKET_ERR_NOTHING_TO_SELL: c_uint32 = 0x3C0C
	# Closed the shop.
	UIIT_MSG_FLEAMARKET_ERR_MARKET_CLOSED: c_uint32 = 0x3C0E
	# Cannot buy due to insufficient gold
	UIIT_MSG_FLEAMARKET_ERR_NOT_ENOUGH_GOLD: c_uint32 = 0x3C11
	# UIIT_MSG_FLEAMARKET_ERR_NOT_ENOUGH_GOLD = 0x3C52,
	# Cannot buy due to insufficient space in your inventory.
	UIIT_MSG_FLEAMARKET_ERR_INVENTORY_FULL: c_uint32 = 0x3C12
	# UIIT_MSG_FLEAMARKET_ERR_INVENTORY_FULL = 0x3C53,
	# The shop is closed.
	UIIT_MSG_FLEAMARKET_ERR_HOST_LEFT: c_uint32 = 0x3C15
	# You are in the status of being unable to open a shop.
	UIIT_MSG_FLEAMARKET_ERR_IM_BUSY: c_uint32 = 0x3C16
	# The shop is closed.
	UIIT_MSG_FLEAMARKET_ERR_CLOSED_BY_HOST: c_uint32 = 0x3C17
	# The member limit(8) has been reached.
	UIIT_MSG_FLEAMARKET_ERR_MARKET_FULL: c_uint32 = 0x3C18
	# The shop is under construction.
	uiit_msg_fleamarket_err_inval_id_HOST_STATE: c_uint32 = 0x3C2B
	# Banned by the owner from the shop. This function is not yet supported.
	UIIT_MSG_FLEAMARKET_ERR_CUSTOMER_BANNED: c_uint32 = 0x3C2C
	# Cannot open the shop while boarding the vehicle to move or transport.
	uiit_msg_fleamarket_err_cant_open_market_in_r_idESTATE: c_uint32 = 0x3C34
	# Invalid letters are included. Invalid name for the shop.
	UIIT_MSG_FLEAMARKET_ERR_NOT_ALLOWED_FMARKETNAME: c_uint32 = 0x3C38
	# Cannot open a shop during murderer status.
	UIIT_MSG_FLEAMARKET_ERR_CANT_OPEN_MARKET_MURDERER: c_uint32 = 0x3C39
	# Cannot open a stall under job status.
	UIIT_MSG_FREEMARKET_NOTUSE_JOB: c_uint32 = 0x3C3B
	# Registraion of item on stall network has failed.
	UIIT_MSG_WARENETWORK_ERROR_FAIL: c_uint32 = 0x3C41
# UIIT_MSG_WARENETWORK_ERROR_FAIL = 0x3C42,
# UIIT_MSG_WARENETWORK_ERROR_FAIL = 0x3C43,
