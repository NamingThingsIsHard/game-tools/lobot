from lobot_api.packets.stall.StallUpdateType import StallUpdateType


class StallUpdateArgs:

	def __init__(self, update_type: StallUpdateType):
		self.UpdateType = update_type
