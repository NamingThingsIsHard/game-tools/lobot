from enum import Enum


class StallAction(Enum):  # byte
	Leave = 1
	Enter = 2
	Buy = 3
