from typing import Optional

from lobot_api.Event import Event
from lobot_api.packets.ExchangePacket import ExchangePacket
from lobot_api.packets.IRequest import IRequest
from lobot_api.packets.party.PartyJoinArgs import PartyJoinArgs
from silkroad_security_api_1_4.Packet import Packet


# 29 00 00 00                                       partyNumber
# 4E 51 00 00                                       partymember_id
# 74 04 00 00                                       unknown
# 00 00 00 00                                       unknown
# 00 00 00 00                                       unknown
# 04                                                unknown
# FF                                                unknown
# 4E 51 00 00                                       party_id
# 09 00                                             nameLength
# 4C 6F 62 6F 74 54 65 73 74                        LobotTest.......
# 84 07 00 00                                       player avatar
# 01                                                level
# AA                                                unknown
# A9 62                                             x_sector y_sector
# A5 05                                             x
# 00 00                                             z
# 24 02                                             y
# 01 00 01 00                                       unknown
# 00 00                                             guildname length
# 04                                                unknown
# 00 00 00 00                                       mastery0
# 00 00 00 00                                       mastery1
# # 29 00 00 00                                       )...............
# 4E 51 00 00                                       NQ..............
# 01                                                ................
# # 01                                                ................
# 2E 51 00 00                                       .Q..............


class PartyMatchJoinHandler(ExchangePacket):
	on_packet = Event()

	@property
	def opcode(self) -> int: return 0x706D

	@property
	def server_opcode(self) -> int: return 0xB06D

	on_join_request = Event()

	def __init__(self, number=0):
		self.party_number = number

	def _add_payload_(self) -> Packet:
		p = Packet(self.opcode)
		p.write_uint32(self.party_number)
		p.lock()
		return p

	@classmethod
	def _parse_packet_(cls, p: Packet) -> Optional[IRequest]:
		party_number = p.read_uint32()
		member_id = p.read_uint32()
		cls.on_join_request.notify(PartyJoinArgs(party_number, member_id))
		cls.on_packet.notify([])
		return None
