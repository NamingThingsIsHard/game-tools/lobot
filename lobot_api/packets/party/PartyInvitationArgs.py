from ctypes import c_uint32

from lobot_api.structs.PartyType import PartyType


class PartyInvitationArgs:

	def __init__(self, inviter_id: c_uint32, party_type: PartyType):
		self.inviter_id = inviter_id
		self.PartyType = party_type
