from lobot_api.Bot import Bot
from lobot_api.packets.IRequest import IRequest
from lobot_api.structs.PartyEntry import PartyEntry
from silkroad_security_api_1_4.Packet import Packet


#
# # 01                                                ................
# 0B 00                                             ................


class PartyLeave(IRequest):
	# on_LeftParty = Event()

	@property
	def opcode(self) -> int: return 0x7061

	def _add_payload_(self) -> Packet:
		Bot.party_current = PartyEntry(0)
		# on_left_party(PartyChangedArgs(PartyChangeType.Update))
		return Packet(self.opcode)
# def __on_left_party__(e: PartyChangedArgs):
#    handler: EventHandler = LeftParty
#    if handler is not None:
#        handler(None, e)
