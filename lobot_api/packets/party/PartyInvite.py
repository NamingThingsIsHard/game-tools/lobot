from ctypes import *
from typing import Optional

from lobot_api.Bot import Bot
from lobot_api.packets.ExchangePacket import ExchangePacket
from lobot_api.packets.IRequest import IRequest
from lobot_api.structs.PartyType import PartyType
from silkroad_security_api_1_4.Packet import Packet


# # Invite to party
# exp/item autoshare
# # BF 29 BF 01                                       invitedplayer_id
# 07                                                party_type
# # 02                                                invitationResponse 2 decline
# 0C 2C unknown 716
# BF 29 BF 01                                       invitedplayer_id
# 07                                                party_type
# # 01                                                invitationResponse 1 accept
# 2E 51 00 00                                       unknown


class PartyInvite(ExchangePacket):
	@property
	def opcode(self) -> int:
		return 0x7060

	@property
	def server_opcode(self) -> int:
		return 0xB060

	def __init__(self, player_id: c_uint32, type: PartyType):
		self.invitedplayer_id = player_id
		self.type = type

	def _add_payload_(self) -> Packet:
		p: Packet = Packet(self.opcode, False)
		p.write_uint32(self.invitedplayer_id)
		p.write_uint8(self.type)
		return p

	# Invite to party
	# exp/item autoshare
	# # BF 29 BF 01                                       invitedplayer_id
	# 07                                                party_type
	# # 02                                                invitationResponse 2 decline
	# 0C 2C                                             unknown 716
	# BF 29 BF 01                                       invitedplayer_id
	# 07                                                party_type
	# # 01                                                invitationResponse 1 accept
	# 2E 51 00 00                                       partyleader_id
	@classmethod
	def _parse_packet_(cls, p: Packet) -> Optional[IRequest]:
		if p.read_uint8() == 0x01:
			Bot.party_current.leader_id = p.read_uint32()
		else:
			pass
		# declined invite next person ?

		return None
