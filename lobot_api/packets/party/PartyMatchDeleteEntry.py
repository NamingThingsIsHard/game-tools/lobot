from typing import Optional

from lobot_api.Bot import Bot
from lobot_api.packets.ExchangePacket import ExchangePacket
from lobot_api.packets.IRequest import IRequest
from lobot_api.structs.PartyEntry import PartyEntry
from silkroad_security_api_1_4.Packet import Packet


# 77 04 00 00                                       party_id
# # 01                                                successFlag
# 77 04 00 00                                       party_id


class PartyMatchDeleteEntry(ExchangePacket):
	# public static event EventHandler on_partymatch_entry_deleted = Event()

	@property
	def opcode(self) -> int:
		return 0x706B

	@property
	def server_opcode(self) -> int:
		return 0xB06B

	def _add_payload_(self) -> Packet:
		if Bot.party_current.party_number != 0:
			p: Packet = Packet(self.opcode, False)
			p.write_uint32(Bot.party_current.party_number)
			return p
		else:
			return None

	@classmethod
	def _parse_packet_(cls, p: Packet) -> Optional[IRequest]:
		if p.read_uint8() == 0x01:
			Bot.party_current = PartyEntry(0)
		# cls.on_partymatch_entry_deleted(PartyChangedArgs(PartyChangeType.Update))
		return None
