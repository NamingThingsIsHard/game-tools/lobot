from ctypes import *

from lobot_api.packets.IRequest import IRequest
from silkroad_security_api_1_4.Packet import Packet


# 4E 51 00 00                                       member_id
class PartyBanMember(IRequest):

	@property
	def opcode(self) -> int: return 0x7063

	def __init__(self, member_id: c_uint32):
		self.member_id = member_id

	def _add_payload_(self) -> Packet:
		p: Packet = Packet(self.opcode, False)
		p.write_uint32(self.member_id)
		return p
