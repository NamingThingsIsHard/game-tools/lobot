from typing import Optional

from lobot_api.Bot import Bot
from lobot_api.packets.IHandler import IHandler
from lobot_api.packets.IRequest import IRequest
from silkroad_security_api_1_4.Packet import Packet


class PartyInvitationJoinACKHandler(IHandler):
	# # Invitation
	# # 02                                                invitationFlag 2
	# D6 8E CB 01                                       inviter_id
	# 07                                                party_type exp/item/autoshare
	# # 02                                                inviteResponse 2 decline
	# 0C 2C unknown
	# # 02                                                inviteResponse 2 decline
	# 0C 2C unknown
	# Invitation
	# # 02                                                invitationFlag 2
	# D6 8E CB 01                                       inviter_id
	# 07                                                party_type
	# # 01                                                acceptFlag1
	# 01                                                acceptFlag2
	# # 01                                                confirmInvitationFlag
	# 4E 51 00 00                                       partymember_id
	@property
	def server_opcode(self) -> int:
		return 0xB067

	@classmethod
	def _parse_packet_(cls, p: Packet) -> Optional[IRequest]:
		if p.read_uint8() == 0x01:
			Bot.party_member_id = p.read_uint32()
		else:
			pass  # declined invite next person ?
		return None
