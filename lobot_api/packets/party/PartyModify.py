from typing import Optional

from lobot_api.Bot import Bot
from lobot_api.packets.ExchangePacket import ExchangePacket
from lobot_api.packets.IRequest import IRequest
from lobot_api.structs.PartyEntry import PartyEntry
from lobot_api.structs.PartyObjective import PartyObjective
from lobot_api.structs.PartyType import PartyType
from silkroad_security_api_1_4.Packet import Packet


# # Quest
# level 5-9
# exp autoshare item autoshare
# # 53 04 00 00                                       partyNumber 1107
# 00 00 00 00                                       0: partyleader_id = self
# 07                                                party_type
# 01                                                partyObjective
# 05                                                levelMin 5
# 09                                                levelMax 9
# 29 00                                             partyNameLength
# 57 68 65 6E 20 77 69 6C 6C 20 74 68 65 20 71 75   When.will.the.qu
# 65 73 74 73 20 65 6E 64 20 6F 6E 20 74 68 65 20   ests.end.on.the.
# 53 69 6C 6B 72 6F 61 64 3F                        Silkroad?.......


class PartyModify(ExchangePacket):

	@property
	def opcode(self) -> int: return 0x706A

	@property
	def server_opcode(self) -> int: return 0xB06A

	def __init__(self, party: PartyEntry):
		self.party = party

	def _add_payload_(self) -> Packet:
		p: Packet = Packet(self.opcode, False)
		p.write_uint32(self.party.party_number)
		p.write_uint32(0)
		p.write_uint8(self.party.type)
		p.write_uint8(self.party.objective)
		p.write_uint8(self.party.min_level)
		p.write_uint8(self.party.max_level)
		p.write_uint16(len(self.party.title))
		p.write_ascii(self.party.title)
		return p

	# Update party
	# Quest
	# level 5-9
	# exp autoshare item autoshare
	# # 53 04 00 00                                       partyNumber 1107
	# 00 00 00 00                                       unknown0
	# 07                                                party_type
	# 01                                                partyObjective
	# 05                                                levelMin 5
	# 09                                                levelMax 9
	# 29 00                                             partyNameLength
	# 57 68 65 6E 20 77 69 6C 6C 20 74 68 65 20 71 75   When.will.the.qu
	# 65 73 74 73 20 65 6E 64 20 6F 6E 20 74 68 65 20   ests.end.on.the.
	# 53 69 6C 6B 72 6F 61 64 3F                        Silkroad?.......
	# # 01                                                successFlag?
	# 53 04 00 00                                       partyNumber
	# 00 00 00 00                                       unknown
	# 07                                                party_type
	# 01                   				                objective
	# 05                                                levelMin
	# 09                                                levelMax
	# 29 00                                             partyNameLength
	# 57 68 65 6E 20 77 69 6C 6C 20 74 68 65 20 71 75   When.will.the.qu
	# 65 73 74 73 20 65 6E 64 20 6F 6E 20 74 68 65 20   ests.end.on.the.
	# 53 69 6C 6B 72 6F 61 64 3F                        Silkroad?.......
	@classmethod
	def _parse_packet_(cls, p: Packet) -> Optional[IRequest]:
		p.read_uint8()  # successFlag
		Bot.party_current.party_number = p.read_uint32()
		Bot.party_current.leader_id = p.read_uint32()
		Bot.party_current.type = PartyType(p.read_uint8())
		Bot.party_current.objective = PartyObjective(p.read_uint8())
		Bot.party_current.min_level = p.read_uint8()
		Bot.party_current.max_level = p.read_uint8()
		Bot.party_current.title = p.read_ascii()
		# Bot.PartyMatchingList.remove_all(entry: => entry.PartyNumber is party.PartyNumber)
		# Bot.PartyMatchingList.append(party)
		return None
