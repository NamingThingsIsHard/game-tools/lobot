from typing import Optional

from lobot_api.Bot import Bot
from lobot_api.Event import Event
from lobot_api.packets.IHandler import IHandler
from lobot_api.packets.IRequest import IRequest
from lobot_api.scripting.Point import Point
from lobot_api.structs.PartyEntry import PartyEntry
from lobot_api.structs.PartyMember import PartyMember
from lobot_api.structs.PartyType import PartyType
from silkroad_security_api_1_4.Packet import Packet


class PartyUpdateHandler(IHandler):
	on_packet = Event()

	# FF                                                255: pkflag = none
	# BA 00 00 00                                       partynumber
	# 2E 51 00 00                                       leader_id
	# 07                                                party_type
	# 01                                                partymembers
	# FF                                                unknownflag 255
	# 2E 51 00 00                                       ID
	# 05 00                                             namelength
	# 4C 6F 62 6F 74                                    Lobot...........
	# 73 07 00 00                                       charAvatar 1907
	# 06                                                level 6
	# AA unknown
	# A9 62                                             x_sector y_sector
	# B6 05                                             x
	# 00 00                                             z
	# A3 06                                             y
	# 01 00 01 00                                       angle
	# 00 00                                             guildnameLength
	# 04                                                ................
	# 03 01 00 00                                       mastery pacheon 259
	# 13 01 00 00                                       mastery fire 275
	@property
	def server_opcode(self) -> int: return 0x3065

	@classmethod
	def _parse_packet_(cls, p: Packet) -> Optional[IRequest]:
		unkn0 = p.read_uint8()  # unknown 255
		Bot.party_current = PartyEntry(p.read_uint32())
		Bot.party_current.leader_id = p.read_uint32()
		Bot.party_current.type = PartyType(p.read_uint8())
		Bot.party_current.member_count = p.read_uint8()

		for i in range(Bot.party_current.member_count):
			unkn1 = p.read_uint8()  # unknown 255
			member = PartyMember(p.read_uint32())
			member.name = p.read_ascii()
			member.avatar = p.read_uint32()
			member.level = p.read_uint8()
			unkn2 = p.read_uint8()  # unknown 170
			member.x_sector = p.read_uint8()
			member.y_sector = p.read_uint8()
			point = Point(member.x_sector, member.y_sector, p.read_uint16(), p.read_uint16(), p.read_uint16())
			member.angle = p.read_single()
			member.guild_name = p.read_ascii()  # leader guild name
			unkn3 = p.read_uint8()  # unknown 4
			member.mastery1 = p.read_uint32()  # mastery 1
			member.mastery2 = p.read_uint32()  # mastery 2
			Bot.party_current.members.append(member)
		PartyUpdateHandler.on_packet.notify([str(m) for m in Bot.party_current.members])
		return None
