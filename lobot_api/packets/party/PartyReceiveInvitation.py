from ctypes import *
from typing import Optional

from lobot_api.packets.ExchangePacket import ExchangePacket
from lobot_api.packets.IRequest import IRequest
from lobot_api.packets.party.PartyInvitationResponseType import PartyInvitationResponseType
from silkroad_security_api_1_4.Packet import Packet


# # Invitation
# # 02                                                invitationFlag 2
# D6 8E CB 01                                       inviter_id
# 07                                                party_type exp/item/autoshare
# # 02                                                inviteResponse 2 decline
# 0C 2C unknown
# # 02                                                inviteResponse 2 decline
# 0C 2C unknown
# Invitation
# # 02                                                invitationFlag 2
# D6 8E CB 01                                       inviter_id
# 07                                                party_type
# # 01                                                acceptFlag1
# 01                                                acceptFlag2
# # 01                                                confirmInvitationFlag
# 4E 51 00 00                                       partymember_id
class PartyReceiveInvitation(ExchangePacket):
	DeclineByte: c_ushort = 11276  # 0C 2C

	@property
	def opcode(self) -> int:
		return 0x3080

	@property
	def server_opcode(self) -> int:
		return 0x3080

	# public static event EventHandlerReceivedInvitation = Event()

	# def _on_packet_received_(e: PartyInvitationArgs) -> void:
	#    EventHandlerhandler = ReceivedInvitation
	#    if handler is not None:
	#        handler(this, e)
	def __init__(self, response: PartyInvitationResponseType):
		self.response = response

	def _add_payload_(self) -> Packet:
		p: Packet = Packet(self.opcode, False)
		if self.response == PartyInvitationResponseType.Accept:
			p.write_uint8(self.response)
			p.write_uint8(self.response)
		else:
			p.write_uint8(0x02)
			p.write_uint8(PartyReceiveInvitation.DeclineByte)
		return p

	# Invitation
	# # 02                                                invitationFlag 2
	# D6 8E CB 01                                       inviter_id
	# 07                                                party_type exp/item/autoshare
	# # 02                                                inviteResponse 2 decline
	# 0C 2C unknown
	# # 02                                                inviteResponse 2 decline
	# 0C 2C unknown
	# Invitation
	# # 02                                                invitationFlag 2
	# D6 8E CB 01                                       inviter_id
	# 07                                                party_type
	# # 01                                                acceptFlag1
	# 01                                                acceptFlag2
	@classmethod
	def _parse_packet_(cls, p: Packet) -> Optional[IRequest]:
		if p.read_uint8() == 0x02:  # flag invited
			inviter_id = p.read_uint32()
			type = p.read_uint8()
		# args: EventArgs = EventArgs()
		# on_packet_received.notify(PartyInvitationArgs(inviter_id, (PartyType)type))
		else:
			pass  # todo find other values
		return None
