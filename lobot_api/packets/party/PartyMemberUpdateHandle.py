from ctypes import *
from typing import Optional

from lobot_api.Bot import Bot
from lobot_api.Event import Event
from lobot_api.packets.IHandler import IHandler
from lobot_api.packets.IRequest import IRequest
# 02                                                flag add
# FF pk2flag none
# 4E 51 00 00                                       member_id
# 09 00                                             nameLength
# 4C 6F 62 6F 74 54 65 73 74                        LobotTest.......
# 84 07 00 00                                       char avatar
# 01                                                level 1
# AA                                                unknown positionFlag?
# A9 62                                             x_sector y_sector
# 69 05                                             x
# 00 00                                             z
# 54 06                                             y
# 01 00 01 00                                       angle
# 00 00                                             guild name
# 04                                                unknown
# 00 00 00 00                                       mastery none
# 00 00 00 00                                       mastery none
# update position
# # 06                                                updateflag 6 move
# 4E 51 00 00                                       member_id
# 20                                                unknownflag
# A9 62                                             x_sector y_sector
# 6B 05                                             x
# 00 00                                             z
# 3E 06                                             y
# 01 00 01 00                                       angle
# leave Party
# # 01                                                ................
# 0B 00                                             ................
from lobot_api.scripting.Point import Point
from lobot_api.structs.PartyEntry import PartyEntry
from lobot_api.structs.PartyMember import PartyMember
from silkroad_security_api_1_4.Packet import Packet


class PartyMemberUpdateHandler(IHandler):
	on_kicked = Event()
	on_left = Event()
	on_update = Event()
	on_moved = Event()
	KickedFlag: c_ushort = 0x0B  # 0B 00

	# public static event EventHandlerPartyMemberUpdated = Event()

	@property
	def server_opcode(self) -> int:
		return 0x03684

	@classmethod
	def __leave_party__(cls, p: Packet):
		flag = p.read_uint16()
		if flag == PartyMemberUpdateHandler.KickedFlag:
			Bot.party_current = PartyEntry(0)

	@classmethod
	def __add_member__(cls, p: Packet):
		p.read_uint8()  # pk2 flag
		member: PartyMember = PartyMember(p.read_uint32())
		member.name = p.read_ascii()
		member.avatar = p.read_uint32()
		member.level = p.read_uint8()
		hpmp = p.read_uint8()  # hp first 4 bits, mp last 4 bits
		member.hp = 0xF0 & hpmp
		member.mp = 0x0F & hpmp
		member.x_sector = p.read_uint8()
		member.y_sector = p.read_uint8()
		if member.y_sector == 0x80:
			member.point = Point(member.x_sector, member.y_sector, p.read_int32(), p.read_int32(), p.read_int32())
		else:
			member.point = Point(p.read_int16(), p.read_int16(), p.read_int16())
		member.angle = p.read_single()
		member.guild_name = p.read_ascii()
		unkn3 = p.read_uint8()  # unknown 4
		member.mastery1 = p.read_uint32()  # mastery 1
		member.mastery2 = p.read_uint32()  # mastery 2
		Bot.party_current.members.append(member)
		Bot.party_current.member_count += 1

	@classmethod
	def __player_left__(cls, p: Packet):
		id_ = p.read_uint32()
		member = next((m for m in Bot.party_current.members if m.member_id == id_), None)
		if member:
			Bot.party_current.members.remove(member)
			Bot.party_current.member_count -= 1

	@classmethod
	def __update_member_position__(cls, p: Packet):
		# p.read_uint8()  # pk2 flag
		member_id = p.read_uint32()

		unkn1 = p.read_uint8()  # unknown 32
		x_sector = p.read_uint8()
		y_sector = p.read_uint8()
		x = p.read_uint16()
		z = p.read_uint16()
		y = p.read_uint16()
		angle: float = p.read_single()

		member = next((m for m in Bot.party_current.members if m.member_id == member_id), None)
		if member:
			member.x_sector = x_sector
			member.y_sector = y_sector
			member.point = Point(x_sector, y_sector, x, z, y, angle)

	@classmethod
	def _parse_packet_(cls, p: Packet) -> Optional[IRequest]:
		update_flag = p.read_uint8()  # update flag 1 = disbanded, 2 = add member, 6 = update member movement
		# switch (update_flag)
		if update_flag == 1:  # leave
			cls.__leave_party__(p)
			PartyMemberUpdateHandler.on_kicked.notify([])
		elif update_flag == 2:
			cls.__add_member__(p)
			PartyMemberUpdateHandler.on_update.notify([])
		elif update_flag == 3:
			cls.__player_left__(p)
			PartyMemberUpdateHandler.on_left.notify([])
		elif update_flag == 6:
			cls.__update_member_position__(p)
			PartyMemberUpdateHandler.on_moved.notify([])
		else:
			pass
		return None
# def __on_partymember_updated__(e: PartyChangedArgs):
#    EventHandlerhandler = PartyMemberUpdated
#    # if handler is not None:
#    #    handler(None, e)
