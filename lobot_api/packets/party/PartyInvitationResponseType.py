from enum import Enum


class PartyInvitationResponseType(Enum):  # byte
	Accept = 0x01
	Decline = 0x02
