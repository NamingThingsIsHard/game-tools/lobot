from ctypes import *

from lobot_api.packets.IRequest import IRequest
from silkroad_security_api_1_4.Packet import Packet


# 29 00 00 00                                       )...............
# 4E 51 00 00                                       NQ..............
# 74 04 00 00                                       t...............
# 00 00 00 00                                       ................
# 00 00 00 00                                       ................
# 04                                                ................
# FF                                                ................
# 4E 51 00 00                                       NQ..............
# 09 00                                             ................
# 4C 6F 62 6F 74 54 65 73 74                        LobotTest.......
# 84 07 00 00                                       ................
# 01                                                ................
# AA                                                ................
# A9 62                                             .b..............
# A5 05                                             ................
# 00 00                                             ................
# 24 02                                             $...............
# 01 00 01 00                                       ................
# 00 00                                             ................
# 04                                                ................
# 00 00 00 00                                       ................
# 00 00 00 00                                       ................
# # 29 00 00 00                                       )...............
# 4E 51 00 00                                       NQ..............
# 01                                                ................
# # 01                                                ................
# 2E 51 00 00                                       .Q..............
class PartyMatchAcceptJoin(IRequest):
	@property
	def opcode(self) -> int: return 0x306E

	def __init__(self, partyNumber: c_uint32, partymember_id: c_uint32):
		self.PartyNumber = partyNumber
		self.partymember_id = partymember_id

	def _add_payload_(self) -> Packet:
		p: Packet = Packet(self.opcode, False)
		p.write_uint32(self.PartyNumber)
		p.write_uint32(self.partymember_id)
		p.write_uint8(1)  # 0 decline 1 accept
		return p
