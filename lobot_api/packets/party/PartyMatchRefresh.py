from typing import Optional

from lobot_api.Bot import Bot
from lobot_api.Event import Event
from lobot_api.packets.ExchangePacket import ExchangePacket
from lobot_api.packets.IRequest import IRequest
from lobot_api.structs.PartyEntry import PartyEntry
from lobot_api.structs.PartyObjective import PartyObjective
from lobot_api.structs.PartyType import PartyType
from silkroad_security_api_1_4.Packet import Packet

"""Request an updated party match list from the server."""


# Receives response 0xB06C.
# # 00                                                flag
# # 01                                                flag
# 00                                                ................
# 00                                                ................
# 00                                                ................
class PartyMatchRefresh(ExchangePacket):
	on_packet = Event()

	@property
	def opcode(self) -> int:
		return 0x706C

	@property
	def server_opcode(self) -> int:
		return 0xB06C

	# Request party match list
	# 00                                                ................
	# Receive response
	# 01                                                flag
	# 01                                                totalPages
	# 00                                                currentPage
	# 05                                                partieson_current_page(5)
	# 23 4F 00 00                                       partyNumber(20259)
	# 5D 73 63 5A                                       unknownFlag(ID: partyLeader) ?
	# 08 00                                             leaderNameLength
	# 4B 6F 52 6F 5A 61 4B 69                           partyLeaderName(KoRoZaKi)
	# 00                                                race (0 is chinese)
	# 02                                                members(2)
	# 05                                                party_type(canInvite, itemfree, expshare) ?
	# 00                                                purpose(hunting LTP)
	# 01                                                minLevel(1)
	# 50                                                maxLevel(80)
	# 08 00                                             titleLength
	# 76 65 6D 20 61 6D 6F 72                           partyTitle(vem.amor)
	def _add_payload_(self) -> Packet:
		return Packet(self.opcode, True, False, [0x00])

	@classmethod
	def _parse_packet_(cls, p: Packet) -> Optional[IRequest]:
		# Refresh party matching list
		Bot.party_matching_list.clear()
		p.read_uint8()  # unknown flag page?
		p.read_uint8()  # unknown flag
		p.read_uint8()  # unknown flag
		count = p.read_uint8()
		for i in range(0, count):
			party: PartyEntry = PartyEntry(p.read_uint32())
			party.leader_id = p.read_uint32()
			party.leader = p.read_ascii()
			party.Race = p.read_uint8()
			party.member_count = p.read_uint8()
			party.type = PartyType(p.read_uint8())
			party.objective = PartyObjective(p.read_uint8())
			party.min_level = p.read_uint8()
			party.max_level = p.read_uint8()
			party.title = p.read_ascii()
			if not party in Bot.party_matching_list:
				Bot.party_matching_list.append(party)
		cls.on_packet.notify([str(c) for c in Bot.party_matching_list])
		return None
