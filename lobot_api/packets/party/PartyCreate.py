from typing import Optional

from lobot_api.Bot import Bot
from lobot_api.packets.ExchangePacket import ExchangePacket
from lobot_api.packets.IRequest import IRequest
from lobot_api.packets.party.PartyMatchRefresh import PartyMatchRefresh
from lobot_api.structs.PartyEntry import PartyEntry
from lobot_api.structs.PartyObjective import PartyObjective
from lobot_api.structs.PartyType import PartyType
from silkroad_security_api_1_4.Packet import Packet


# # Create party
# hunting
# level 5-9
# exp autoshare item autoshare
# # 00 00 00 00                                       unknown0
# 00 00 00 00                                       unknown1
# 07                                                party_type
# 00                                                purpose hunting
# 05                                                levelMin 5
# 09                                                levelMax 9
# 1B 00                                             partyNameLength
# 57 65 6C 63 6F 6D 65 20 74 6F 20 49 6E 50 61 6E   Welcome.to.InPan
# 69 63 2D 53 69 6C 6B 72 6F 61 64                  ic-Silkroad.....


class PartyCreate(ExchangePacket):
	@property
	def opcode(self) -> int:
		return 0x7069

	@property
	def server_opcode(self) -> int:
		return 0xB069

	def __init__(self, party: PartyEntry = PartyEntry(0)):
		self.Party = party

	def _add_payload_(self) -> Packet:
		p: Packet = Packet(self.opcode, True)
		p.write_uint32(0x00)  # unknown0
		p.write_uint32(0x00)  # unknown1
		p.write_uint8(self.Party.type.value)
		p.write_uint8(self.Party.objective.value)
		p.write_uint8(self.Party.min_level)
		p.write_uint8(self.Party.max_level)
		p.write_ascii(self.Party.title)
		return p

	# Create party
	# hunting
	# level 5-9
	# exp autoshare item autoshare
	# # 00 00 00 00                                       unknown0
	# 00 00 00 00                                       unknown1
	# 07                                                party_type
	# 00                                                objective hunting
	# 05                                                levelMin 5
	# 09                                                levelMax 9
	# 1B 00                                             partyNameLength
	# 57 65 6C 63 6F 6D 65 20 74 6F 20 49 6E 50 61 6E   Welcome.to.InPan
	# 69 63 2D 53 69 6C 6B 72 6F 61 64                  ic-Silkroad.....
	# # 01                                                unknownFlag success?
	# 53 04 00 00                                       partyNumber 1107
	# 00 00 00 00                                       leader_id 0 is self
	# 07                                                party_type
	# 00                                                objective hunting
	# 05                                                levelMin 5
	# 09                                                levelMax 9
	# 1B 00                                             partyNameLength
	# 57 65 6C 63 6F 6D 65 20 74 6F 20 49 6E 50 61 6E   Welcome.to.InPan
	# 69 63 2D 53 69 6C 6B 72 6F 61 64                  ic-Silkroad.....
	@classmethod
	def _parse_packet_(cls, p: Packet) -> Optional[IRequest]:
		# Refresh party matching list
		success = p.read_uint8()  # successFlag
		if success == 0x01:
			Bot.party_current.party_number = p.read_uint32()
			Bot.party_current.leader_id = p.read_uint32()
			Bot.party_current.type = PartyType(p.read_uint8())
			Bot.party_current.objective = PartyObjective(p.read_uint8())
			Bot.party_current.min_level = p.read_uint8()
			Bot.party_current.max_level = p.read_uint8()
			Bot.party_current.title = p.read_ascii()
			return PartyMatchRefresh()
		else:
			return None
