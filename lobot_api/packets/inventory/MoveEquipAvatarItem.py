from lobot_api.Bot import Bot
from lobot_api.Event import Event
from lobot_api.packets.inventory.AbstractItemMoveStrategy import AbstractItemMoveStrategy
from lobot_api.packets.inventory.IItemMoveHandleStrategy import IItemMoveHandleStrategy
from lobot_api.packets.inventory.ItemMoveParams import ItemMoveParams
from lobot_api.packets.inventory.ItemMovementType import ItemMovementType
from silkroad_security_api_1_4.Packet import Packet


# equip avatar dress
# # 24                                                movement_type
# 23                                                from_slot
# 01                                                to_slot
# # 01                                                successFlag
# 24                                                movement_type
# 24                                                from
# 01                                                to_slot
# 00 00                                             amount
# 00                                                flag
class MoveEquipAvatarItem(AbstractItemMoveStrategy, IItemMoveHandleStrategy):
	on_equipped = Event()

	@classmethod
	def type(cls) -> ItemMovementType: return ItemMovementType.EquipAvatarItem

	@classmethod
	def handle_move(cls, p: ItemMoveParams):
		Bot.char_data.inventory.avatar_inventory.items[p.to_slot] = Bot.char_data.inventory.items[p.from_slot]
		Bot.char_data.inventory.items.pop(p.from_slot)
		cls.on_equipped.notify(ItemMoveParams(p.from_slot, p.to_slot))

	@classmethod
	def move_item(cls, p: ItemMoveParams) -> Packet:
		p.packet.write_uint8(cls.type().value)
		p.packet.write_uint8(p.from_slot)
		p.packet.write_uint8(p.to_slot)
		return p.packet
