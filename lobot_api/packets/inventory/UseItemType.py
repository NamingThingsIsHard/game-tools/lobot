from enum import Enum


class UseItemType(Enum):
	Normal = 1
	UseOnPet = 2
	UseOnItem = 3
