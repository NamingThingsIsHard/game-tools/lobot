from lobot_api.Bot import Bot
from lobot_api.Event import Event
from lobot_api.packets.inventory.AbstractItemMoveStrategy import AbstractItemMoveStrategy
from lobot_api.packets.inventory.IItemMoveHandleStrategy import IItemMoveHandleStrategy
from lobot_api.packets.inventory.ItemMoveArgs import ItemMoveArgs
from lobot_api.packets.inventory.ItemMoveParams import ItemMoveParams
from lobot_api.packets.inventory.ItemMovementType import ItemMovementType
from silkroad_security_api_1_4.Packet import Packet


# move 4 mp recovery herbs from inventory slot 36 to storage slot index 1
# # 02                                                2: moveOption = inventory to storage
# 24                                                from_slot
# 01                                                to_slot (result can vary based on vacancy)
# B1 01 00 00                                       selected_npc
# # 01                                                successFlag
# 02                                                moveOption
# 24                                                from_slot
# 01                                                to_slot
class MoveInventoryToStorage(AbstractItemMoveStrategy, IItemMoveHandleStrategy):
	@classmethod
	def type(cls) -> ItemMovementType: return ItemMovementType.InventoryToStorage

	on_item_stored = Event()

	@classmethod
	def handle_move(cls, p: ItemMoveParams):
		super()._move_(Bot.char_data.inventory.items, p.from_slot, p.to_slot, Bot.storage)
		cls.on_item_stored.notify(ItemMoveArgs(p.from_slot, p.to_slot, cls.type()))

	@classmethod
	def move_item(cls, p: ItemMoveParams) -> Packet:
		p.packet.write_uint8(cls.type().value)
		p.packet.write_uint8(p.from_slot)
		p.packet.write_uint8(p.to_slot)
		p.packet.write_uint32(Bot.selected_npc.unique_id)
		return p.packet
