﻿from typing import NamedTuple

from silkroad_security_api_1_4.Packet import Packet


class ItemMoveParams(NamedTuple):
	from_slot: int = 0
	to_slot: int = 0
	amount: int = 0
	packet: Packet = None
