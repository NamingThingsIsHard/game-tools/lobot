from typing import Optional

from lobot_api.Bot import Bot
from lobot_api.packets.IHandler import IHandler
from lobot_api.packets.IRequest import IRequest
from silkroad_security_api_1_4.Packet import Packet


# update size
# # 01                                                success flag
# 37                                                size 55
class InventoryUpdateSizeHandler(IHandler):
	@property
	def server_opcode(self) -> int: return 0x3092

	@classmethod
	def _parse_packet_(cls, p: Packet) -> Optional[IRequest]:
		success = p.read_uint8()
		if success == 0x1:
			Bot.char_data.inventory.size = p.read_uint8()
		return None
