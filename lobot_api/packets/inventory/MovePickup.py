from lobot_api.Bot import Bot
from lobot_api.Event import Event
from lobot_api.packets.inventory.AbstractItemMoveStrategy import AbstractItemMoveStrategy
from lobot_api.packets.inventory.IItemMoveHandleStrategy import IItemMoveHandleStrategy
from lobot_api.packets.inventory.ItemMoveArgs import ItemMoveArgs
from lobot_api.packets.inventory.ItemMoveParams import ItemMoveParams
from lobot_api.packets.inventory.ItemMovementType import ItemMovementType
from lobot_api.structs.Item import Item
from silkroad_security_api_1_4.Packet import Packet


# pickup sassi charm
# # 01                                                successFlag
# 06                                                movement_type (6 = add to inventory after pickup)
# 26                                                slot
# 00 00 00 00                                       unknownFlag (owner? => 0 is self)
# BF 1B 00 00                                       ref_id (7103 sassi charm)
# 01 00                                             amount
# pickup sosun copper bow +7
# # 01                                                successFlag
# 06                                                movement_type
# 27                                                slot
# 00 00 00 00                                       unknownFlag (owner?)
# 40 10 00 00                                       ref_id
# 07                                                enhancement +7
# 00 00 00 00 00 00 00 00                           attributes
# 28 23 00 00                                       durability
# 02                                                blue (str 4 4: int)
# 44 00 00 00 04 00 00 00                           68 4
# 4A 00 00 00 04 00 00 00                           74 4
# 01                                                ................
# 00                                                ................
# 02                                                ................
# 00                                                ................
# pickup gold
# target gold
# # 01                                                ................
# 02                                                ................
# 01                                                ................
# 80 70 88 01                                       .p..............
# # 01                                                successFlag
# 01                                                ................
# # 01                                                successFlag
# 06                                                movement_type pickup
# FE                                                254: slot = gold
# 19 00 00 00                                       amount 25

class MovePickup(AbstractItemMoveStrategy, IItemMoveHandleStrategy):
	GOLDFLAG = 254

	@classmethod
	def type(cls) -> ItemMovementType: return ItemMovementType.PickUp

	on_item_picked_up = Event()

	@classmethod
	def handle_move(cls, p: ItemMoveParams):
		item: Item = Item()
		item.slot = p.packet.read_uint8()
		if item.slot is MovePickup.GOLDFLAG:
			amount = p.packet.read_uint32()
			Bot.char_data.remain_gold += amount
			return
		# ref item
		cls._parse_new_item_(item, p, Bot.char_data.inventory.items)
		cls.on_item_picked_up.notify(ItemMoveArgs(p.from_slot, item.slot, cls.type()))

	@classmethod
	def move_item(cls, p: ItemMoveParams) -> Packet:
		pass
		# Pickup is taken care of by the interaction opcodes 7074/B074
		return None
