from abc import abstractmethod, ABC

from lobot_api.packets.inventory.ItemMoveParams import ItemMoveParams


class IItemMoveHandleStrategy(ABC):
	@abstractmethod
	def handle_move(self, p: ItemMoveParams):
		pass
