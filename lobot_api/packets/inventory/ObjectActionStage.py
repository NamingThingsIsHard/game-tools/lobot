from enum import Enum


class ObjectActionStage(Enum):  # byte
	Start = 1
	End = 2
