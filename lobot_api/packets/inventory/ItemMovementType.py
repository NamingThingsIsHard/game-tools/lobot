﻿from enum import Enum


class ItemMovementType(Enum):
	InventoryToInventory = 0x00
	StorageToStorage = 0x01
	InventoryToStorage = 0x02
	StorageToInventory = 0x03
	PickUp = 0x06
	Drop = 0x07
	Buy = 0x08
	Sell = 0x09
	DropGold = 0x0A
	AwardItem = 0x0E
	RemoveItem = 0x0F
	PetToPet = 0x10
	PetPickup = 0x11
	PetToInventory = 0x1A
	InventoryToPet = 0x1B
	PetPickupGold = 0x1C
	BuyItemMall = 0x18
	BuyBack = 0x22
	UnequipAvatarItem = 0x23
	EquipAvatarItem = 0x24
