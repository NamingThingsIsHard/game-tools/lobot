import copy
import logging
import sys
from typing import Optional

from lobot_api.Bot import Bot
from lobot_api.globals.PK2DataGlobal import PK2DataGlobal
from lobot_api.logger.Logger import SCRIPT, PK2
from lobot_api.packets.inventory.AbstractItemMoveStrategy import AbstractItemMoveStrategy, List
from lobot_api.packets.inventory.IItemMoveHandleStrategy import IItemMoveHandleStrategy
from lobot_api.packets.inventory.ItemMoveParams import ItemMoveParams
from lobot_api.packets.inventory.ItemMovementType import ItemMovementType
from lobot_api.structs.Item import Item
from lobot_api.structs.Shop import Shop
from silkroad_security_api_1_4.Packet import Packet


# skin change for 50 silk
# [S -> C][B034]
# 01                                                success
# 18                                                type=BuyFromItemMall
# 1B 04                                             unknown = 1051
# 02                                                tab(expendables)
# 00                                                subtab(special)
# 06                                                slot(6)
# 01                                                stack
# 17                                                to_slot
# 01 00                                             amount
# 00 00 00 00                                       recipient=0=self
#
# clock of reincarnation for pets 48 silk
# [S -> C][B034]
# 01                                                success
# 18                                                type=BuyFromItemMall
# 1B 04                                             unknown =
# 03                                                tab(pet)
# 04                                                subtab(pet items)
# 00                                                slot
# 01                                                stack
# 18                                                to_slot
# 01 00                                             amount
# 00 00 00 00                                       recipient=0=self


class MoveBuyItemMall(AbstractItemMoveStrategy, IItemMoveHandleStrategy):
	@classmethod
	def type(cls) -> ItemMovementType:
		return ItemMovementType.Buy

	# on_item_bought = Event()

	# def __on_item_bought__(e: ItemMoveArgs):
	#    EventHandlerhandler = ItemBought
	#    if handler is not None:
	#        handler(None, e)
	@staticmethod
	def find_item(selected_npc, tab, slot) -> Optional[Item]:
		shop: Shop = PK2DataGlobal.shops.get(int(selected_npc), None)
		if shop:
			item_id = next(
				(i for i in shop.item_entries.values() if i.tab_number == tab and i.slot_number == slot), None).item_id
			if item_id != 0x00:
				item: PK2.PK2item = PK2DataGlobal.items[item_id]
				return Item(item)
			else:
				sys.stdout.write(
						f"Item tab {tab} slot {slot} not found in npc {shop.pk2_entry.name} - {format(shop.npc_id, '02X')}\n")
				logging.log(SCRIPT, f"Item tab {tab} slot {slot} not found in npc {shop.pk2_entry.name} - {format(shop.npc_id, '02X')}")
				return Item()
		else:
			sys.stdout.write(f"Shop {selected_npc} not found\n")
			logging.log(SCRIPT, f"Shop {selected_npc} not found")
			return None

	def handle_move(self, p: ItemMoveParams):
		unknown = p.packet.read_uint16()
		unknown_type = p.packet.read_uint8()
		tab = p.packet.read_uint8()
		slot = p.packet.read_uint8()
		quantity = p.packet.read_uint8()

		result = MoveBuyItemMall.find_item(Bot.selected_npc.ref_id, tab, slot)
		item: Item = result if result is not None else Item()  # not found should give dummy item to prevent crash

		to_slot: List[int] = [0x00] * quantity
		for i in range(0, quantity):
			to_slot.append(p.packet.read_uint8())
		item.stack_count = p.packet.read_uint8()
		for i in to_slot:
			recipient_id = p.packet.read_uint32()  # unknown ? self?
			if recipient_id == 0x0:
				c = copy.deepcopy(item)
				c.slot = i
				Bot.char_data.inventory.items[i] = c
		# on_item_bought(ItemMoveArgs(p.from_slot, p.to_slot, cls.type()))

	@classmethod
	def move_item(cls, p: ItemMoveParams) -> Packet:
		super().move_item(p)
		p.packet.write_uint32(Bot.selected_npc.unique_id)
		return p.packet
