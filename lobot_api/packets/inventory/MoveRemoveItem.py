# remove last hp herb after use
# # 01                                                success
# 0F                                                movement_type 15 -> remove
# 2A                                                slot 42
# 02                                                hasNextItem? 2 -> no because deleted
from lobot_api.Bot import Bot
from lobot_api.Event import Event
from lobot_api.packets.inventory.AbstractItemMoveStrategy import AbstractItemMoveStrategy
from lobot_api.packets.inventory.IItemMoveHandleStrategy import IItemMoveHandleStrategy
from lobot_api.packets.inventory.ItemMoveArgs import ItemMoveArgs
from lobot_api.packets.inventory.ItemMoveParams import ItemMoveParams
from lobot_api.packets.inventory.ItemMovementType import ItemMovementType


class MoveRemoveItem(AbstractItemMoveStrategy, IItemMoveHandleStrategy):
	on_item_removed = Event()

	@classmethod
	def type(cls) -> ItemMovementType:
		return ItemMovementType.RemoveItem

	@classmethod
	def handle_move(cls, p: ItemMoveParams):
		slot = p.packet.read_uint8()
		if slot in Bot.char_data.inventory.items.keys():
			Bot.char_data.inventory.items.pop(slot)
		remove_type = p.packet.read_uint8()
		if remove_type == 2:
			pass
			# 2 consumed after use
		elif remove_type == 4:
			pass
			# alchemy (like removing elixir or lucky powder after use)
		cls.on_item_removed.notify(ItemMoveArgs(slot, slot, ItemMovementType.RemoveItem))
