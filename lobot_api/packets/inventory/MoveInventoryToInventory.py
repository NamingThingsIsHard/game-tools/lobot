# Command -># switch
# 00                                                0: movement_type = inventory
# 0D                                                currentSlot 13
# 1D                                                nextSlot 29
# E8 03                                             amount 1000
# Response -> sucessful# switch
# 01                                                successFlag
# 00                                                movement_type
# 0D                                                from_slot 13
# 1D                                                to_slot 29
# E8 03                                             amount 1000
# 00                                                successFlag
from lobot_api.Bot import Bot
from lobot_api.Event import Event
from lobot_api.packets.inventory.AbstractItemMoveStrategy import AbstractItemMoveStrategy
from lobot_api.packets.inventory.IItemMoveHandleStrategy import IItemMoveHandleStrategy
from lobot_api.packets.inventory.ItemMoveArgs import ItemMoveArgs
from lobot_api.packets.inventory.ItemMoveParams import ItemMoveParams
from lobot_api.packets.inventory.ItemMovementType import ItemMovementType
from lobot_api.structs import EquipmentSlot
from lobot_api.structs.Item import Item


class MoveInventoryToInventory(AbstractItemMoveStrategy, IItemMoveHandleStrategy):
	on_item_split = Event()
	on_item_stacked = Event()
	on_item_switched = Event()
	on_item_moved = Event()

	@classmethod
	def type(cls) -> ItemMovementType: return ItemMovementType.InventoryToInventory

	@classmethod
	def set_containers(cls, from_slot, to_slot):
		"""
		Set origin and target container depending on origin and target slot for item.
		:param from_slot:
		:param to_slot:
		:return: Tuple of origin and target container
		"""
		def equipment_or_items(slot):
			return (Bot.char_data.inventory.equipment
			        if slot < EquipmentSlot.MAX_EQUIP_SLOTS else Bot.char_data.inventory.items)

		origin_container = equipment_or_items(from_slot)
		target_container = equipment_or_items(to_slot)
		return origin_container, target_container

	@classmethod
	def choose_move_method(cls, p: ItemMoveParams):
		from_item: Item = Bot.char_data.inventory.get_slot(p.from_slot)
		if from_item:
			to_item: Item = Bot.char_data.inventory.get_slot(p.to_slot)
			if to_item:
				if from_item.ref_id == to_item.ref_id:
					super()._stack_(Bot.char_data.inventory.items, from_item, to_item, p.amount)
					MoveInventoryToInventory.on_item_stacked.notify(
						ItemMoveArgs(p.from_slot, p.to_slot, cls.type()))
				else:
					origin_container, target_container = cls.set_containers(p.from_slot, p.to_slot)
					super()._switch_(origin_container, p.from_slot, p.to_slot, target_container)
					MoveInventoryToInventory.on_item_switched.notify(
						ItemMoveArgs(p.from_slot, p.to_slot, cls.type()))
			else:
				if 0 < p.amount < Bot.char_data.inventory.items[p.from_slot].stack_count:
					super()._split_(Bot.char_data.inventory.items, p.from_slot, p.to_slot, p.amount)
					MoveInventoryToInventory.on_item_split.notify(ItemMoveArgs(p.from_slot, p.to_slot, cls.type()))
				else:
					origin_container, target_container = cls.set_containers(p.from_slot, p.to_slot)
					super()._move_(origin_container, p.from_slot, p.to_slot, target_container)
					MoveInventoryToInventory.on_item_moved.notify(ItemMoveArgs(p.from_slot, p.to_slot, cls.type()))

	@classmethod
	def handle_move(cls, p: ItemMoveParams):
		cls.choose_move_method(p)

		# success e.g. switching single slot weapon with bow and arrows --> move arrows
		success_flag = p.packet.read_uint8()
		if success_flag == 0x01:
			flag_type = p.packet.read_uint8()
			if flag_type == cls.type().value:
				new_params = ItemMoveParams(p.packet.read_uint8(), p.packet.read_uint8(), p.packet.read_uint16(), p.packet)
				cls.choose_move_method(new_params)
