from ctypes import *

from lobot_api.Bot import Bot
from lobot_api.packets.IRequest import IRequest
from silkroad_security_api_1_4.Packet import Packet


# Buyback
# # 02 01 00 00                                       npc_id
# 00                                                from_slot(BuybackList)
# gold update
# # 01                                                successFlag
# CC 0D 00 00 00 00 00 00                           finalAmount 3532
# 01                                                endFlag
# # 01                                                successFlag
# 22                                                movement_type = 34 buyback
# 24                                                to_slot
# 00                                                from_slot(BuybackList)
# 01 00                                             amount
class ItemBuyBack(IRequest):
	@property
	def opcode(self) -> int: return 0x7168

	def __init__(self, from_slot: c_byte):
		self.from_slot = from_slot

	def _add_payload_(self) -> Packet:
		p: Packet = Packet(self.opcode, False)
		p.write_uint32(Bot.selected_npc.unique_id)
		p.write_uint8(self.from_slot)
		return p
