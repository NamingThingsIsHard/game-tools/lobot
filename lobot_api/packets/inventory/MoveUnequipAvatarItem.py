from lobot_api.Bot import Bot
from lobot_api.Event import Event
from lobot_api.packets.inventory.AbstractItemMoveStrategy import AbstractItemMoveStrategy
from lobot_api.packets.inventory.IItemMoveHandleStrategy import IItemMoveHandleStrategy
from lobot_api.packets.inventory.ItemMoveArgs import ItemMoveArgs
from lobot_api.packets.inventory.ItemMoveParams import ItemMoveParams
from lobot_api.packets.inventory.ItemMovementType import ItemMovementType
from silkroad_security_api_1_4.Packet import Packet


# unequip
# # 23                                                movement_type
# 01                                                from_slot
# 27                                                to_slot
# # 01                                                ................
# 23                                                #...............
# 01                                                ................
# 24                                                $...............
# 00 00                                             ................
# 00                                                ................
class MoveUnequipAvatarItem(AbstractItemMoveStrategy, IItemMoveHandleStrategy):
	on_unequipped = Event()

	@classmethod
	def type(cls) -> ItemMovementType: return ItemMovementType.UnequipAvatarItem

	@classmethod
	def handle_move(cls, p: ItemMoveParams):
		Bot.char_data.inventory.items[p.to_slot] = Bot.char_data.inventory.avatar_inventory.items[p.from_slot]
		Bot.char_data.inventory.avatar_inventory.items.pop(p.from_slot)
		cls.on_unequipped.notify(ItemMoveArgs(p.from_slot, p.to_slot, cls.type()))

		success = p.packet.read_uint8()

		# remove accessory if chest piece was moved
		if success == 0x01:
			move_type = p.packet.read_uint8()
			if move_type == cls.type().value:
				from_slot = p.packet.read_uint8()
				to_slot = p.packet.read_uint8()
				Bot.char_data.inventory.items[to_slot] = Bot.char_data.inventory.avatar_inventory.items[from_slot]
				Bot.char_data.inventory.avatar_inventory.items.pop(from_slot)
				cls.on_unequipped.notify(ItemMoveArgs(from_slot, to_slot, cls.type()))

	@classmethod
	def move_item(cls, p: ItemMoveParams) -> Packet:
		p.packet.write_uint8(cls.type().value)
		p.packet.write_uint8(p.from_slot)
		p.packet.write_uint8(p.to_slot)
		return p.packet
