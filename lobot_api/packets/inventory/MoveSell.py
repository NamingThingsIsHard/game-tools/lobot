from lobot_api.Bot import Bot
from lobot_api.Event import Event
from lobot_api.packets.inventory.AbstractItemMoveStrategy import AbstractItemMoveStrategy
from lobot_api.packets.inventory.IItemMoveHandleStrategy import IItemMoveHandleStrategy
from lobot_api.packets.inventory.ItemMoveArgs import ItemMoveArgs
from lobot_api.packets.inventory.ItemMoveParams import ItemMoveParams
from lobot_api.packets.inventory.ItemMovementType import ItemMovementType
from lobot_api.structs.Item import Item
from silkroad_security_api_1_4.Packet import Packet


class MoveSell(AbstractItemMoveStrategy, IItemMoveHandleStrategy):
	MAX_BUYBACK_ITEMS: int = 5
	on_sold = Event()

	@classmethod
	def type(cls) -> ItemMovementType:
		return ItemMovementType.Sell

	# # 09                                                moveOption
	# 26                                                from_slot
	# 01 00                                             amount
	# 9E 01 00 00                                       npc_id
	# # 01                                                successFlag
	# 09                                                9: moveOption = sell
	# 26                                                from_slot
	# 01 00                                             amount
	# 9E 01 00 00                                       npc_id
	# 01                                                amount of items in buyback list
	@classmethod
	def handle_move(cls, p: ItemMoveParams):
		from_item: Item = Bot.char_data.inventory.items.get(p.from_slot, None)
		if from_item:
			if p.amount == from_item.stack_count:
				Bot.char_data.inventory.items.pop(from_item.slot)
			else:
				Bot.char_data.inventory.items[from_item.slot].stack_count -= p.amount
			previous_buy_back_count: int = len(Bot.buy_back_list)
			buy_back_count = p.packet.read_uint8()  # amount of items in buyback list
			if previous_buy_back_count == MoveSell.MAX_BUYBACK_ITEMS:
				Bot.buy_back_list.pop(0)
			Bot.buy_back_list.append(from_item)
			cls.on_sold.notify(ItemMoveArgs(p.from_slot, p.to_slot, cls.type()))

	@classmethod
	def move_item(cls, p: ItemMoveParams) -> Packet:
		p.packet.write_uint8(cls.type().value)
		p.packet.write_uint8(p.from_slot)
		p.packet.write_uint16(p.amount)
		p.packet.write_uint32(Bot.selected_npc.unique_id)
		return p.packet
