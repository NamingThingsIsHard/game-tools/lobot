from typing import Optional

from lobot_api.Event import Event
from lobot_api.packets.ExchangePacket import ExchangePacket
from lobot_api.packets.IRequest import IRequest
from lobot_api.packets.inventory.ItemMoveParams import ItemMoveParams
from lobot_api.packets.inventory.ItemMovementType import ItemMovementType
from lobot_api.packets.inventory.MoveAwardItem import MoveAwardItem
from lobot_api.packets.inventory.MoveBuy import MoveBuy
from lobot_api.packets.inventory.MoveBuyBack import MoveBuyBack
from lobot_api.packets.inventory.MoveBuyItemMall import MoveBuyItemMall
from lobot_api.packets.inventory.MoveDrop import MoveDrop
from lobot_api.packets.inventory.MoveDropGold import MoveDropGold
from lobot_api.packets.inventory.MoveEquipAvatarItem import MoveEquipAvatarItem
from lobot_api.packets.inventory.MoveInventoryToInventory import MoveInventoryToInventory
from lobot_api.packets.inventory.MoveInventoryToPet import MoveInventoryToPet
from lobot_api.packets.inventory.MoveInventoryToStorage import MoveInventoryToStorage
from lobot_api.packets.inventory.MoveItemError import MoveItemError
from lobot_api.packets.inventory.MoveItemErrorArgs import MoveItemErrorArgs
from lobot_api.packets.inventory.MovePetPickup import MovePetPickup
from lobot_api.packets.inventory.MovePetToInventory import MovePetToInventory
from lobot_api.packets.inventory.MovePetToPet import MovePetToPet
from lobot_api.packets.inventory.MovePickup import MovePickup
from lobot_api.packets.inventory.MoveRemoveItem import MoveRemoveItem
from lobot_api.packets.inventory.MoveSell import MoveSell
from lobot_api.packets.inventory.MoveStorageToInventory import MoveStorageToInventory
from lobot_api.packets.inventory.MoveStorageToStorage import MoveStorageToStorage
from lobot_api.packets.inventory.MoveUnequipAvatarItem import MoveUnequipAvatarItem
from silkroad_security_api_1_4.Packet import Packet


# move to storage
# # 01                                                movementOption
# 05                                                from_slot
# 02                                                to_slot
# 04 00                                             amount
# B1 01 00 00                                       selected_npc_id
class MoveItem(ExchangePacket):
	on_item_move_error = Event()

	# private static Comparison{ return i1.Item1 - i2.Item1 })
	@property
	def opcode(self) -> int:
		return 0x7034

	@property
	def server_opcode(self) -> int:
		return 0xB034

	def __init__(self, movement_option: ItemMovementType, from_slot, to_slot, amount=0, unique_npc_id=0):
		self.movement_option = movement_option
		self.from_slot = from_slot
		self.to_slot = to_slot
		self.amount = amount
		self.selected_npc_id = unique_npc_id

	def _add_payload_(self) -> Packet:
		p: Packet = Packet(self.opcode, True)
		parameters: ItemMoveParams = ItemMoveParams(self.from_slot, self.to_slot, self.amount, p)
		# add npc_id if movement has to do with another NPC
		# switch (MovementOption)
		if self.movement_option == ItemMovementType.InventoryToInventory:
			return MoveInventoryToInventory().move_item(parameters)
		elif self.movement_option == ItemMovementType.StorageToStorage:
			return MoveStorageToStorage().move_item(parameters)
		elif self.movement_option == ItemMovementType.InventoryToStorage:
			return MoveInventoryToStorage().move_item(parameters)
		elif self.movement_option == ItemMovementType.PickUp:
			# Pickup command is taken care of by the interaction opcodes 7074/B074
			pass
		elif self.movement_option == ItemMovementType.Drop:
			return MoveDrop().move_item(parameters)
		elif self.movement_option == ItemMovementType.StorageToInventory:
			return MoveStorageToInventory().move_item(parameters)
		elif self.movement_option == ItemMovementType.DropGold:
			return MoveDropGold().move_item(parameters)
		elif self.movement_option == ItemMovementType.Buy:
			return MoveBuy().move_item(parameters)
		elif self.movement_option == ItemMovementType.BuyItemMall:
			return MoveBuyItemMall().move_item(parameters)
		elif self.movement_option == ItemMovementType.Sell:
			return MoveSell().move_item(parameters)
		elif self.movement_option == ItemMovementType.PetToPet:
			return MovePetToPet.move_item(parameters)
		elif self.movement_option == ItemMovementType.PetPickup:
			return MovePetPickup.move_item(parameters)
		elif self.movement_option == ItemMovementType.PetToInventory:
			return MovePetToInventory.move_item(parameters)
		elif self.movement_option == ItemMovementType.InventoryToPet:
			return MoveInventoryToPet.move_item(parameters)
		elif self.movement_option == ItemMovementType.PetPickupGold:
			pass
		elif self.movement_option == ItemMovementType.BuyBack:
			return MoveBuyBack().move_item(parameters)
		elif self.movement_option == ItemMovementType.UnequipAvatarItem:
			return MoveUnequipAvatarItem().move_item(parameters)
		elif self.movement_option == ItemMovementType.EquipAvatarItem:
			return MoveEquipAvatarItem().move_item(parameters)
		else:
			pass
		return p

	@classmethod
	def _parse_packet_(cls, p: Packet) -> Optional[IRequest]:
		success_flag = p.read_uint8()
		if success_flag == 0x01:
			type_value: ItemMovementType = ItemMovementType(p.read_uint8())
			# switch ((ItemMovementType)type)
			if type_value == ItemMovementType.InventoryToInventory:
				# Loop takes item# switch into account (# switch bow+arrow with shield)
				MoveInventoryToInventory.handle_move(ItemMoveParams(p.read_uint8(), p.read_uint8(), p.read_uint16(), p))
			elif type_value == ItemMovementType.StorageToStorage:
				MoveStorageToStorage.handle_move(ItemMoveParams(p.read_uint8(), p.read_uint8(), p.read_uint16()))
			elif type_value == ItemMovementType.InventoryToStorage:
				MoveInventoryToStorage.handle_move(ItemMoveParams(p.read_uint8(), p.read_uint8()))
			elif type_value == ItemMovementType.StorageToInventory:
				MoveStorageToInventory.handle_move(ItemMoveParams(p.read_uint8(), p.read_uint8()))
			elif type_value == ItemMovementType.PickUp:
				MovePickup.handle_move(ItemMoveParams(0, 0, 0, p))
			elif type_value == ItemMovementType.Drop:
				MoveDrop.handle_move(ItemMoveParams(p.read_uint8()))
			elif type_value == ItemMovementType.Buy:
				MoveBuy.handle_move(ItemMoveParams(0, 0, 0, p))
			elif type_value == ItemMovementType.Sell:
				MoveSell.handle_move(ItemMoveParams(p.read_uint8(), 0, p.read_uint16(), p))
			elif type_value == ItemMovementType.DropGold:
				MoveDropGold.handle_move(ItemMoveParams(0, 0, 0, p))
			elif type_value == ItemMovementType.AwardItem:
				MoveAwardItem.handle_move(ItemMoveParams(0, 0, 0, p))
			elif type_value == ItemMovementType.RemoveItem:
				MoveRemoveItem.handle_move(ItemMoveParams(0, 0, 0, p))
			elif type_value == ItemMovementType.PetToPet:
				MovePetToPet.handle_move(ItemMoveParams(0, 0, 0, p))
			elif type_value == ItemMovementType.PetPickup:
				MovePetPickup.handle_move(ItemMoveParams(0, 0, 0, p))
			elif type_value == ItemMovementType.PetToInventory:
				MovePetToInventory.handle_move(ItemMoveParams(0, 0, 0, p))
			elif type_value == ItemMovementType.InventoryToPet:
				MoveInventoryToPet.handle_move(ItemMoveParams(0, 0, 0, p))
			elif type_value == ItemMovementType.PetPickupGold:
				pass
			elif type_value == ItemMovementType.BuyBack:
				MoveBuyBack.handle_move(ItemMoveParams(0, 0, 0, p))
			elif type_value == ItemMovementType.UnequipAvatarItem:
				MoveUnequipAvatarItem.handle_move(ItemMoveParams(p.read_uint8(), p.read_uint8(), p.read_uint16(), p))
			elif type_value == ItemMovementType.EquipAvatarItem:
				MoveEquipAvatarItem.handle_move(ItemMoveParams(p.read_uint8(), p.read_uint8()))
			else:
				pass
		elif success_flag == 0x02:
			error = p.read_uint16()  # error code
			cls.on_item_move_error.notify(MoveItemErrorArgs(MoveItemError(error)))
		return None
