from lobot_api.structs.Item import Item


class ItemUseArgs:
	def __init__(self, slot, item: Item = None):
		self.slot = slot
		self.Item = item
