from lobot_api.Bot import Bot
from lobot_api.Event import Event
from lobot_api.packets.inventory.AbstractItemMoveStrategy import AbstractItemMoveStrategy
from lobot_api.packets.inventory.IItemMoveHandleStrategy import IItemMoveHandleStrategy
from lobot_api.packets.inventory.ItemMoveArgs import ItemMoveArgs
from lobot_api.packets.inventory.ItemMoveParams import ItemMoveParams
from lobot_api.packets.inventory.ItemMovementType import ItemMovementType
# move item from storage slot 5 to slot 2
# # 01                                                movementOption
# 05                                                from_slot
# 02                                                to_slot
# 04 00                                             amount
# B1 01 00 00                                       selected_npc_id
# # 01                                                successFlag
# 01                                                movementOption
# 05                                                from_slot
# 02                                                to_slot
# 04 00                                             amount
from lobot_api.structs.Item import Item
from silkroad_security_api_1_4.Packet import Packet


class MoveStorageToStorage(AbstractItemMoveStrategy, IItemMoveHandleStrategy):
	on_item_moved = Event()

	@classmethod
	def type(cls) -> ItemMovementType:
		return ItemMovementType.StorageToStorage

	@classmethod
	def handle_move(cls, p: ItemMoveParams):
		from_item: Item = Bot.storage.get(p.from_slot, None)
		if p.from_slot in Bot.storage:
			to_item: Item = Bot.storage.get(p.to_slot, None)
			if to_item:
				if from_item.ref_id == to_item.ref_id:
					super()._stack_(Bot.storage, from_item, to_item, p.amount)
				else:
					super()._switch_(Bot.storage, p.from_slot, p.to_slot)
			else:
				if p.amount < Bot.storage[p.from_slot].stack_count:
					super()._split_(Bot.storage, p.from_slot, p.to_slot, p.amount)
				else:
					super()._move_(Bot.storage, p.from_slot, p.to_slot)
		cls.on_item_moved.notify(ItemMoveArgs(p.from_slot, p.to_slot, cls.type()))

	@classmethod
	def move_item(cls, p: ItemMoveParams) -> Packet:
		super().move_item(p)
		p.packet.write_uint32(Bot.selected_npc.unique_id)
		return p.packet
