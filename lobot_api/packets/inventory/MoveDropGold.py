from lobot_api.Bot import Bot
from lobot_api.packets.inventory.AbstractItemMoveStrategy import AbstractItemMoveStrategy
from lobot_api.packets.inventory.IItemMoveHandleStrategy import IItemMoveHandleStrategy
from lobot_api.packets.inventory.ItemMoveParams import ItemMoveParams
from lobot_api.packets.inventory.ItemMovementType import ItemMovementType
from silkroad_security_api_1_4.Packet import Packet


# # 0A                                                movement_type
# 19 00 00 00 00 00 00 00                           amount 25
# # 01                                                successFlag
# 0A                                                10: ItemMovementType = drop gold
# 19 00 00 00 00 00 00 00                           amount 25
class MoveDropGold(AbstractItemMoveStrategy, IItemMoveHandleStrategy):
	@classmethod
	def type(cls) -> ItemMovementType: return ItemMovementType.DropGold

	@classmethod
	def handle_move(cls, p: ItemMoveParams):
		Bot.char_data.remain_gold -= p.packet.read_uint64()

	@classmethod
	def move_item(cls, p: ItemMoveParams) -> Packet:
		p.packet.write_uint8(cls.type().value)
		p.packet.write_uint64(p.amount)
		return p.packet
