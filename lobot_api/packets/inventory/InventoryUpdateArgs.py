﻿class InventoryUpdateArgs:
	def __init__(self, item_slot, update_type, update_info = 0):
		self.item_slot = item_slot
		self.update_type = update_type
		self.new_status = update_info
