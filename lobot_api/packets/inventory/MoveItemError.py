from enum import Enum


class MoveItemError(Enum):  # c_short
	CannotFind = 0x0003
	# Target does not match slot
	InvalidTarget = 0x0005
	# Character is not yet interacting with shop NPC
	ShopNotOpen = 0x0D
	# Item cannot be picked because enough items have been collected for the quest.
	QuestFinished = 0x0074
	# No space left in inventory
	InventoryFull = 0x1807
	# Currently casting skill, so switching is not possible
	ItemLockedCasting = 0x1834
	# Cannot Equip the selected item. Smoetimes happens after selling multiple items in townscript. Teleport does not help
	CannotEquipSelectedItem = 0x180E
	NoItemToRepair = 0x181D
