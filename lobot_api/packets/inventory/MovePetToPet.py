from lobot_api.Bot import Bot
from lobot_api.Event import Event
from lobot_api.packets.inventory.AbstractItemMoveStrategy import AbstractItemMoveStrategy
from lobot_api.packets.inventory.IItemMoveHandleStrategy import IItemMoveHandleStrategy
from lobot_api.packets.inventory.ItemMoveArgs import ItemMoveArgs
from lobot_api.packets.inventory.ItemMoveParams import ItemMoveParams
from lobot_api.packets.inventory.ItemMovementType import ItemMovementType
from lobot_api.structs.Item import Item
from silkroad_security_api_1_4.Packet import Packet


class MovePetToPet(AbstractItemMoveStrategy, IItemMoveHandleStrategy):
	@classmethod
	def type(cls) -> ItemMovementType:
		return ItemMovementType.PetToPet

	on_item_stacked = Event()
	on_item_switched = Event()
	on_item_moved = Event()

	@classmethod
	def handle_move(cls, p: ItemMoveParams):
		pet_id = p.packet.read_uint32()
		from_slot = p.packet.read_uint8()
		to_slot = p.packet.read_uint8()
		amount = p.packet.read_uint16()

		from_item: Item = Bot.pet_inventory.get(from_slot, None)
		if from_item:
			to_item: Item = Bot.pet_inventory.get(to_slot, None)
			if to_item:
				if from_item.ref_id == to_item.ref_id:
					super()._stack_(Bot.pet_inventory, from_item, to_item, amount)
					cls.on_item_stacked.notify(ItemMoveArgs(from_slot, to_slot, cls.type()))
				else:
					super()._switch_(Bot.pet_inventory, from_slot, to_slot)
					cls.on_item_switched.notify(ItemMoveArgs(to_slot, from_slot, cls.type()))
			else:
				if amount < Bot.pet_inventory[from_slot].stack_count:
					super()._split_(Bot.pet_inventory, from_slot, to_slot, amount)
				else:
					super()._move_(Bot.pet_inventory, from_slot, to_slot)
					cls.on_item_moved.notify(ItemMoveArgs(from_slot, to_slot, cls.type()))

	@classmethod
	def move_item(cls, p: ItemMoveParams) -> Packet:
		super().move_item(p)
		p.packet.write_uint32(Bot.selected_npc.unique_id)
		return p.packet
