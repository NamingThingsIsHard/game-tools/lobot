﻿from abc import ABC, abstractmethod
from typing import *

from lobot_api.Bot import Bot
from lobot_api.globals.PK2DataGlobal import PK2DataGlobal
from lobot_api.packets.inventory.ItemMoveParams import ItemMoveParams
from lobot_api.packets.inventory.ItemMovementType import ItemMovementType
from lobot_api.pk2.PK2Item import PK2ItemType
from lobot_api.pk2.PK2Utils import PK2Utils
from lobot_api.structs.Item import Equipment, Item, PetScroll, PetStatus, AbilityPet, ItemExchangeCoupon, \
	MagicCube, MagParam
from lobot_api.structs.WhiteAttributes import WhiteAttributes
from silkroad_security_api_1_4.Packet import Packet


class AbstractItemMoveStrategy(ABC):

	@classmethod
	@abstractmethod
	def type(cls) -> ItemMovementType:
		pass

	# move in inventory
	# [C -> S][7034]
	# 00                flag
	# 0D                slot: from = 13
	# 15                slot: to = 21
	# 14 00             amount = 20
	@classmethod
	def move_item(cls, p: ItemMoveParams) -> Packet:
		p.packet.write_uint8(cls.type().value)
		p.packet.write_uint8(p.from_slot)
		p.packet.write_uint8(p.to_slot)
		p.packet.write_uint16(p.amount)
		return p.packet

	@staticmethod
	def _move_(origin_container: Dict[int, Item], from_slot, to_slot, target_container: Dict[int, Item] = None):
		"""
		Move item from item_container to target_container.
		If no target_container is given, item is moved within item_container.
		:param origin_container: 
		:param from_slot:
		:param to_slot:
		:param target_container:
		:return: 
		"""
		if target_container is None:
			target_container = origin_container
		target_container[to_slot] = origin_container[from_slot]
		target_container[to_slot].slot = to_slot
		origin_container.pop(from_slot)

	@staticmethod
	def _switch_(origin_container: Dict[int, Item], from_slot, to_slot, target_container: Dict[int, Item] = None):
		if target_container is None:
			target_container = origin_container

		# clear slots and keep references to switch items
		from_item = origin_container.pop(from_slot)
		to_item = target_container.pop(to_slot)

		# switch
		origin_container[from_slot] = to_item
		target_container[to_slot] = from_item

		# update slot numbers
		from_item.slot = to_slot
		to_item.slot = from_slot

	@staticmethod
	def _split_(item_container: Dict[int, Item], from_slot, to_slot, amount):
		item_container[from_slot].stack_count -= amount
		item_container[to_slot] = item_container[from_slot]
		item_container[to_slot].stack_count = amount
		item_container[to_slot].slot = to_slot

	@staticmethod
	def _stack_(item_container: Dict[int, Item], from_item: Item, to_item: Item, amount):
		if from_item.ref_id in PK2DataGlobal.items:
			item = PK2DataGlobal.items[from_item.ref_id]
			# switch stacks if to_item already maxed out
			if to_item.stack_count == item.max_stacks:
				to_item.stack_count = amount
				from_item.stack_count = item.max_stacks
			# stack onto target item
			# and reduce on initial item
			elif item.max_stacks >= amount + to_item.stack_count:
				to_item.stack_count += amount
				from_item.stack_count -= amount
			else:
				to_item.stack_count = item.max_stacks
				from_item.stack_count = item.max_stacks - amount

			# set from object, to trigger collectionchange
			item_container[to_item.slot] = to_item

			# remove from_item if all stacks were transferred or set to from_item
			if from_item.stack_count == 0:
				item_container.pop(from_item.slot)
			else:
				item_container[from_item.slot] = from_item

	# ref item: Item
	@staticmethod
	def _parse_new_item_(item: Item, p: ItemMoveParams, item_container=Bot.char_data.inventory.items):
		unknown = p.packet.read_uint32()
		item.ref_id = p.packet.read_uint32()

		if not PK2DataGlobal.items:
			PK2Utils.extract_all_information()

		if item.ref_id in PK2DataGlobal.items:
			value = PK2DataGlobal.items[item.ref_id]
			if value.is_equipment_item():
				e: Equipment = Equipment(item)
				e.opt_level = p.packet.read_uint8()
				e.attributes = WhiteAttributes(e.get_attribute_type(), p.packet.read_uint64())
				e.durability = p.packet.read_uint32()  # (=> Durability)
				e.mag_param_num = p.packet.read_uint8()  # (=> Blue, Red)

				for j in range(0, e.mag_param_num):
					e.blues.append(MagParam(p.packet.read_uint32(), p.packet.read_uint32()))

				opt_type = p.packet.read_uint8()  # (1 => Socket)
				opt_count = p.packet.read_uint8()

				for i in range(0, opt_count):
					e.item_option.slot = p.packet.read_uint8()
					e.item_option.ID = p.packet.read_uint32()
					e.item_option.NParam1 = p.packet.read_uint32()  # (=> Reference to Socket)
				opt_type = p.packet.read_uint8()  # (2 => Advanced elixir)
				opt_count = p.packet.read_uint8()

				for i in range(0, opt_count):
					e.item_option.slot = p.packet.read_uint8()
					e.item_option.ID = p.packet.read_uint32()
					e.item_option.OptValue = p.packet.read_uint32()  # (=> "Advanced elixir in effect [+OptValue]")
				# Notice for Advanced Elixir modding.
				# Mutiple adv elixirs only possible with db edit. nOptValue of last nSlot will be shown as elixir in effect but total Plus value is correct
				# You also have to fix error when "Buy back" from npc
				### Stored procedure Error(-1): {?=CALL _Bind_Option_Manager (3, 174627, 1, 0, 0, 0, 0, 0, 0)} ## D:\WORK2005\Source\SilkroadOnline\Server\SR_GameServer\AsyncQuery_Storage.cpp AQ_StorageUpdater:DoWork_AddBindingOption 1366
				# Storage Operation Failed!!! [OperationType: 34, ErrorCode: 174627]
				# Query: {CALL _STRG_RESTORE_SOLDITEM_ITEM_MAGIC (174628, ?, ?, 34, 6696,13, 137,8,0,137, 1,4294967506,0,0,0,0,0,0,0,0,0,0,0,199970734269)}
				# AQ Failed! Log out!! [AQType: 1]
				item_container[item.slot] = e
			else:
				# switch (value.type)  #from Reference
				if value.type == PK2ItemType.AttributeStone \
						or value.type == PK2ItemType.MagicStone:
					item.stack_count = p.packet.read_uint16()
					# item.AttributeAssimilationProbability = p.Packet.read_uint8() # -- probability not exposed in all silkroad versions
					item_container[item.slot] = item

				elif value.type == PK2ItemType.GrowthPet:
					pet: PetScroll = PetScroll(item)
					pet.status = PetStatus(p.packet.read_uint8())  # (1 = Unsumonned, 2 = Summoned, 3 = Alive, 4 = Dead)
					pet.unique_id = p.packet.read_uint32()
					# pet.nameLength = p.Packet.read_uint16()
					pet.name = p.packet.read_ascii()
					pet.unk02 = p.packet.read_uint8()  # -> Check for != 0
					Bot.char_data.inventory.append(item.slot, pet)
				elif value.type == PK2ItemType.AbilityPet:
					ability_pet: AbilityPet = AbilityPet(item)
					ability_pet.status = PetStatus(
						p.packet.read_uint8())  # (1 = Unsumonned, 2 = Summoned, 3 = Alive, 4 = Dead)
					if ability_pet.status != PetStatus.Unsummoned:
						ability_pet.unique_id = p.packet.read_uint32()
						# ability_pet.nameLength = p.Packet.read_uint16()
						ability_pet.name = p.packet.read_ascii()
						ability_pet.SecondsToRentEndTime = p.packet.read_uint32()
						ability_pet.unk02 = p.packet.read_uint8()  # -> Check for != 0
					Bot.char_data.inventory.append(item.slot, ability_pet)
				elif value.type == PK2ItemType.ItemExchangeCoupon:
					item.stack_count = p.packet.read_uint16()
					mag_param_num = p.packet.read_uint8()

					for i in range(0, mag_param_num):
						iec: ItemExchangeCoupon = ItemExchangeCoupon(item)
						iec.mag_param_values.append(p.packet.read_uint64())
					# 1. MagParam => couponref_id [fixed]
					# 2. MagParam => CouponItemAmount [fixed]
					# When Coupon holds Scrolls or similar, these 2 MagParams above are used.
					# When Coupon holds Equipment, 8 MagParams are used
					# They are defined in database by "[BIIV]<M:str,1,3><M:int,1,3><O:3>"
					# As MagParams we get those 2 above and
					# 01 4D 01 72 74 73 00 00                           MagParam3 - .M.rts..........	(=> There is our str, don't ask me why its reversed)
					# 03 00 00 00 00 00 00 00                           MagParam4 - MagParam.Value		(=> Amount of +STR)
					# 01 4D 01 74 6E 69 00 00                           MagParam5 - .M.tni..........	(=> There is our int, don't ask me why its reversed)
					# 03 00 00 00 00 00 00 00                           MagParam6 - MagParam.Value		(=>	Amount of +INT)
					# 01 4F 00 00 00 00 00 00                           MagParam7 - .O..............	(=> There is our O for opt_level)
					# 03 00 00 00 00 00 00 00                           MagParam8 - opt_level			(=> Amount of +Overall)
					item_container[item.slot] = item
				elif value.type == PK2ItemType.MagicCube:
					magic_cube: MagicCube = MagicCube(item)
					magic_cube.stored_item_count = p.packet.read_uint32()
					item_container[item.slot] = item
				else:
					item.stack_count = p.packet.read_uint16()
					item_container[item.slot] = item
		else:
			item_container[item.slot] = item
