import copy
import logging
# Buy 2 cotton wristlets from protector trader jangan (Miss Jang)
# # 08                                                movement_type 8 buy
# 02                                                itemTab 2
# 0C                                                item_slot 12
# 02 00                                             quantity
# 5D 00 00 00                                       npc_id
# # 01                                                successFlag
# 08                                                movement_type 8 buy
# 02                                                itemTab 2
# 0C                                                itemTab 12
# 02                                                quantity 2 (for unstackable items like Equipment)
# 26                                                to_slot
# 27                                                to_slot
# 02 00                                             stackAmount 2
# 00 00 00 00                                       recipientnpc_id ? 0 => self
# 00 00 00 00                                       recipientnpc_id ? 0 => self
# Buy herb from jangan herbalist (Yangyun)
# 0x7034, CLIENT_ITEM_MOVE
# 08                                                movement_type 8 buy
# 00                                                itemTab 0
# 01                                                item_slot 1
# 03 00                                             amount 3
# 02 01 00 00                                       npc_id
# # 01                                                flag
# 08                                                movement_type 8 buy
# 00                                                itemTab 0
# 01                                                item_slot 1
# 01                                                quantity
# 25                                                to_slot
# 03 00                                             stackAmount 3
# 00 00 00 00                                       recipientnpc_id ? 0 => self
from typing import Optional, Tuple, Union

from lobot_api.Bot import Bot
from lobot_api.BotStatus import BotStatus
from lobot_api.Event import Event
from lobot_api.globals.PK2DataGlobal import PK2DataGlobal
from lobot_api.logger.Logger import SCRIPT, PK2, HANDLER
from lobot_api.packets.inventory.AbstractItemMoveStrategy import AbstractItemMoveStrategy, List
from lobot_api.packets.inventory.FakeItemSuccess import FakeItemSuccess
from lobot_api.packets.inventory.IItemMoveHandleStrategy import IItemMoveHandleStrategy
from lobot_api.packets.inventory.ItemMoveArgs import ItemMoveArgs
from lobot_api.packets.inventory.ItemMoveParams import ItemMoveParams
from lobot_api.packets.inventory.ItemMovementType import ItemMovementType
from lobot_api.packets.inventory.ObjectActionStage import ObjectActionStage
from lobot_api.structs.Item import Item, Equipment, PetScroll
from lobot_api.structs.Shop import Shop
from silkroad_security_api_1_4.Packet import Packet, PacketDirection


class MoveBuy(AbstractItemMoveStrategy, IItemMoveHandleStrategy):
	@classmethod
	def type(cls) -> ItemMovementType:
		return ItemMovementType.Buy

	on_item_bought = Event()

	@staticmethod
	def should_be_faked(p: Packet) -> bool:
		"""Determine whether B034 response packet after buying should be spoofed for client
		to ensure that it accepts the bought/sold item"""
		if p.opcode == 0xB034 and Bot.get_status() is BotStatus.Botting:
			p.lock()
			item_move_bytes: List[int] = p.get_bytes()
			return (item_move_bytes[0] == 0x01
			        and item_move_bytes[1] == ItemMovementType.Buy.value
			        or item_move_bytes[1] == ItemMovementType.BuyBack.value)
				# or item_move_bytes[1] == ItemMovementType.Sell.value)  # <-- not so sure about selling
		return False

	@staticmethod
	def fake_item_movement(p: Packet) -> List[Tuple[PacketDirection, Packet]]:
		packet_copy: Packet = Packet(p.opcode, p.encrypted, p.massive, p.get_bytes())
		packet_copy.lock()
		logging.log(HANDLER, str(p))
		start: Packet = FakeItemSuccess(ObjectActionStage.Start).create_request()
		end: Packet = FakeItemSuccess(ObjectActionStage.End).create_request()
		fake_item_packets: List[Tuple[PacketDirection, Packet]] = []
		success = packet_copy.read_uint8()  # 0x01 success
		type_value = packet_copy.read_uint8()  # 0x08 for buying
		item_type = ItemMovementType.PickUp.value
		tab = packet_copy.read_uint8()
		slot = packet_copy.read_uint8()
		stacks = packet_copy.read_uint8()
		item: Item = MoveBuy.find_item(Bot.selected_npc.ref_id, tab, slot)
		fake_item_packets = []
		# Make sure item is registered in PK2 database
		if item is not None:
			to_slots: List[int] = [0x00] * stacks
			for i in range(0, stacks):
				to_slots[i] = packet_copy.read_uint8()
			amount = packet_copy.read_uint16()
			# For unstackable items, # set amount to 1 for each stack
			if stacks > 1:
				amount = 0x01
			for i in range(0, stacks):
				fake: Packet = Packet(p.opcode, p.encrypted, p.massive)
				fake.write_uint8(0x01)  # success
				fake.write_uint8(item_type)  # type_value: pickup
				fake.write_uint8(to_slots[i])
				fake.write_uint32(0x00)  # user flag
				fake.write_uint32(item.ref_id)
				# TODO: Get item stats straight from pk2
				if isinstance(item, Equipment):
					fake.write_uint8(0x00)  # Enhancement +
					fake.write_uint64(0x00)  # Stats 64: int
					fake.write_uint32(item.durability)
					fake.write_uint8(0x00)  # Blues amount
					fake.write_uint8(1)
					fake.write_uint8(2)
					fake.write_uint8(3)
					fake.write_uint8(4)
				elif isinstance(item, PetScroll):
					fake.write_uint8(0x03)  # Alive
					fake.write_uint32(item.ref_id)  # 1LV wolf (no other pets in shop)
					fake.write_uint16(0x00)  # Name Length (NoName)
					fake.write_uint8(0x00)  # Unknown
				else:
					fake.write_uint16(amount)
				fake_item_packets.append((PacketDirection.Client, fake))
			return fake_item_packets
		else:
			return []

	@staticmethod
	def try_fake_packet(p) -> List[Tuple[PacketDirection, Packet]]:
		if MoveBuy.should_be_faked(p):
			return MoveBuy.fake_item_movement(p)
		else:
			return []

	@staticmethod
	def find_item(selected_npc, tab, slot) -> Optional[Union[Item, Equipment]]:
		shop: Shop = PK2DataGlobal.shops.get(int(selected_npc), None)
		if shop:
			item_id = next(
				(key for key, value in shop.item_entries.items() if value.tab_number == tab and value.slot_number == slot), 0)
			if item_id != 0 and item_id in PK2DataGlobal.items:
				pk2_item: PK2.PK2item = PK2DataGlobal.items[item_id]
				return Equipment(Item(pk2_item.ID)) if pk2_item.is_equipment_item() else Item(pk2_item.ID)
			else:
				logging.log(SCRIPT, f"Item tab {tab} slot {slot} not found in npc {shop.pk2_entry.name} - {hex(shop.npc_id)}")
				return None
		else:
			logging.log(SCRIPT, f"Shop {selected_npc} not found")
			return None

	@classmethod
	def handle_move(cls, p: ItemMoveParams):
		tab = p.packet.read_uint8()
		slot = p.packet.read_uint8()
		result = MoveBuy.find_item(Bot.selected_npc.ref_id, tab, slot)
		item: Item = result if result is not None else Item()  # not found should give dummy item to prevent crash
		quantity = p.packet.read_uint8()
		to_slot: List[int] = []
		for i in range(0, quantity):
			to_slot.append(p.packet.read_uint8())
		item.stack_count = p.packet.read_uint8()
		for i in to_slot:
			recipient_id = p.packet.read_uint32()  # unknown ? self?
			if recipient_id == 0x0:
				c = copy.deepcopy(item)
				c.slot = i
				Bot.char_data.inventory.items[i] = c
		Bot.char_data.inventory.items = {k: v for k, v in sorted(list(Bot.char_data.inventory.items.items()))}
		cls.on_item_bought.notify(ItemMoveArgs(p.from_slot, p.to_slot, cls.type()))

	@classmethod
	def move_item(cls, p: ItemMoveParams) -> Packet:
		super().move_item(p)
		p.packet.write_uint32(Bot.selected_npc.unique_id)
		return p.packet
