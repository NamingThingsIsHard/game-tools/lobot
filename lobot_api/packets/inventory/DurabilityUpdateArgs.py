from lobot_api.structs.Item import Item


class DurabilityUpdateArgs:
	def __init__(self, i: Item):
		if i is None:
			raise ValueError(Item.__name__)
		self.UpdatedItem = i
