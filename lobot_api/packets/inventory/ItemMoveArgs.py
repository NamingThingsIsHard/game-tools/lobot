﻿from lobot_api.packets.inventory.ItemMovementType import ItemMovementType


class ItemMoveArgs:
	def __init__(self, from_slot: int, to_slot: int, type_value: ItemMovementType):
		self.from_slot = from_slot
		self.to_slot = to_slot
		self.type = type_value
