from lobot_api.Bot import Bot
from lobot_api.Event import Event
from lobot_api.packets.inventory.AbstractItemMoveStrategy import AbstractItemMoveStrategy
from lobot_api.packets.inventory.IItemMoveHandleStrategy import IItemMoveHandleStrategy
from lobot_api.packets.inventory.ItemMoveArgs import ItemMoveArgs
from lobot_api.packets.inventory.ItemMoveParams import ItemMoveParams
from lobot_api.packets.inventory.ItemMovementType import ItemMovementType
from silkroad_security_api_1_4.Packet import Packet


class MovePetToInventory(AbstractItemMoveStrategy, IItemMoveHandleStrategy):
	@classmethod
	def type(cls) -> ItemMovementType: return ItemMovementType.PetToInventory

	on_item_moved = Event()

	@classmethod
	def handle_move(cls, p: ItemMoveParams):
		pet_id = p.packet.read_uint32()
		from_slot = p.packet.read_uint8()
		to_slot = p.packet.read_uint8()
		Bot.char_data.inventory.items[to_slot] = Bot.pet_inventory[from_slot]
		Bot.char_data.inventory.items[to_slot].slot = to_slot
		Bot.pet_inventory.pop(from_slot)

		cls.on_item_moved.notify(ItemMoveArgs(from_slot, to_slot, cls.type()))

	@classmethod
	def move_item(cls, p: ItemMoveParams) -> Packet:
		p.packet.write_uint8(cls.type().value)
		p.packet.write_uint8(p.from_slot)
		p.packet.write_uint8(p.to_slot)
		p.packet.write_uint32(Bot.pickup_pet.ref_id)
		return p.packet
