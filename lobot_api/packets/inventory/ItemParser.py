from lobot_api.globals.PK2DataGlobal import PK2DataGlobal
from lobot_api.pk2.PK2Item import PK2ItemType
from lobot_api.pk2.PK2Utils import PK2Utils
from lobot_api.structs.Item import Item, Equipment, PetScroll, PetStatus, AbilityPet, ItemExchangeCoupon, MagicCube, \
	MagParam
from lobot_api.structs.WhiteAttributes import WhiteAttributes
from silkroad_security_api_1_4.Packet import Packet


def get_rent_type_info(p: Packet, item: Item):
	"""
	read rent type info for item
	:param p:
	:param item:
	:return:
	"""
	# switch (item.RentType)
	if item.rent_type == 1:
		item.rent.can_delete = p.read_uint16()  # (adds "Will be deleted when time period is over" to item)
		item.rent.period_begin_time = p.read_uint32()
		item.rent.period_end_time = p.read_uint32()
	elif item.rent_type == 2:
		item.rent.can_delete = p.read_uint16()  # (adds "Will be deleted when time period is over" to item)
		item.rent.can_recharge = p.read_uint16()  # (adds "Able to extend" to item)
		item.rent.meter_rate_time = p.read_uint32()
	elif item.rent_type == 3:
		item.rent.can_delete = p.read_uint16()  # (adds "Will be deleted when time period is over" to item)
		item.rent.period_begin_time = p.read_uint32()
		item.rent.period_end_time = p.read_uint32()
		item.rent.can_recharge = p.read_uint16()  # (adds "Able to extend" to item)
		item.rent.packing_time = p.read_uint32()

	item.ref_id = p.read_uint32()
	if not PK2DataGlobal.items:
		PK2Utils.extract_all_information()


def parse_item(p: Packet) -> Item:
	"""
	Takes packet and extracts item from it.
	:param p: Packet to parse
	:return: Item entity
	"""
	item: Item = Item()
	item.slot = p.read_uint8()
	item.rent_type = p.read_uint32()
	# switch (item.RentType)
	if item.rent_type != 0:
		get_rent_type_info(p, item)

	item.ref_id = p.read_uint32()
	if not PK2DataGlobal.items:
		PK2Utils.extract_all_information()

	value = PK2DataGlobal.items.get(item.ref_id, None)
	if value:
		# value_type: str = format(value.type.value, 'X')
		if value.is_equipment_item():  # Equipment
			e: Equipment = Equipment(item)
			e.opt_level = p.read_uint8()
			e.Attributes = WhiteAttributes(e.get_attribute_type(), p.read_uint64())
			e.durability = p.read_uint32()  # (=> Durability)
			e.mag_param_num = p.read_uint8()  # (=> Blue, Red)

			for j in range(0, e.mag_param_num):
				e.blues.append(MagParam(p.read_uint32(), p.read_uint32()))

			opt_type = p.read_uint8()  # (1 => Socket)
			opt_count = p.read_uint8()
			for i in range(0, opt_count):
				e.item_option.slot = p.read_uint8()
				e.item_option.ID = p.read_uint32()
				e.item_option.NParam1 = p.read_uint32()  # (=> Reference to Socket)

			opt_type = p.read_uint8()  # (2 => Advanced elixir)
			opt_count = p.read_uint8()
			for i in range(0, opt_count):
				e.item_option.slot = p.read_uint8()
				e.item_option.ID = p.read_uint32()
				e.item_option.OptValue = p.read_uint32()  # (=> "Advanced elixir in effect [+OptValue]")
			# Notice for Advanced Elixir modding.
			# Mutiple adv elixirs only possible with db edit. nOptValue of last nSlot will be shown as elixir in effect but total Plus value is correct
			# You also have to fix error when "Buy back" from NPC
			# ## Stored procedure Error(-1): {?=CALL _Bind_Option_Manager (3, 174627, 1, 0, 0, 0, 0, 0, 0)} ## D:\WORK2005\Source\SilkroadOnline\Server\SR_GameServer\AsyncQuery_Storage.cpp AQ_StorageUpdater:DoWork_AddBindingOption 1366
			# Storage Operation Failed!!! # Query: {CALL _STRG_RESTORE_SOLDITEM_ITEM_MAGIC (174628, ?, ?, 34, 6696,13, 137,8,0,137, 1,4294967506,0,0,0,0,0,0,0,0,0,0,0,199970734269)}
			# AQ Failednot  Log out!! [AQType: 1]
			return e
		elif value.type.is_attribute_stone() or value.type.is_magic_stone() or value.type is PK2ItemType.MagicStoneMall:
			item.stack_count = p.read_uint16()
			# enhancement blues dont have assimilation probability
			if item.type is not PK2ItemType.MagicStoneMall and item.type is not PK2ItemType.MagicStoneSteady and item.type is not PK2ItemType.MagicStoneLuck:
				item.attribute_assimilation_probability = p.read_uint8()  # -- probability not exposed in all silkroad versions
			return item
		elif value.type is PK2ItemType.GrowthPet:
			pet: PetScroll = PetScroll(item)
			pet.status = PetStatus(p.read_uint8())  # (1 = Unsummoned, 2 = Summoned, 3 = Alive, 4 = Dead)
			if pet.status is not PetStatus.Unsummoned:
				pet.unique_id = p.read_uint32()
				pet.name = p.read_ascii()
				pet.unk02 = p.read_uint8()  # -> Check for != 0
				if pet.unk02 != 0:
					p.read_uint8()
					p.read_single()  # Walk speed ?
					p.read_single()  # Run speed ?
					p.read_single()  # Zerk Speed ?
					p.read_uint8()

			return pet
		elif value.type is PK2ItemType.AbilityPet:
			ability_pet: AbilityPet = AbilityPet(item)
			ability_pet.status = PetStatus(p.read_uint8())  # (1 = Unsumonned, 2 = Summoned, 3 = Alive, 4 = Dead)
			if ability_pet.status is not PetStatus.Unsummoned:
				ability_pet.unique_id = p.read_uint32()
				# ability_pet.nameLength = p.read_uint16()
				ability_pet.name = p.read_ascii()
				ability_pet.SecondsToRentEndTime = p.read_uint32()
				ability_pet.unk02 = p.read_uint8()  # -> Check for != 0
				if ability_pet.unk02 != 0x00:
					rent_flag = p.read_uint8()
					if rent_flag != 0:
						rent_00 = p.read_uint32()
						rent_01 = p.read_uint32()  # inventory time left
						rent_02 = p.read_uint32()
						rent_end = p.read_uint8()  # rent_flag end
			return ability_pet
		elif value.type is PK2ItemType.ItemExchangeCoupon:
			item.stack_count = p.read_uint16()
			mag_param_num = p.read_uint8()
			for i in range(0, mag_param_num):
				iec: ItemExchangeCoupon = ItemExchangeCoupon(item)
				iec.mag_param_values.append(p.read_uint64())
			return item
		elif value.type is PK2ItemType.MagicCube:
			magic_cube: MagicCube = MagicCube(item)
			magic_cube.stored_item_count = p.read_uint32()
			return magic_cube
		else:
			item.stack_count = p.read_uint16()
			return item
	else:
		return item


def parse_stall_item(p: Packet) -> Item:
	"""
	Takes packet and extracts stall item from it.
	:param p: Packet to parse
	:return: Item entity
	"""
	item: Item = Item()
	item.rent_type = p.read_uint32()
	if item.rent_type != 0:
		get_rent_type_info(p, item)

	item.ref_id = p.read_uint32()

	if not PK2DataGlobal.items:
		PK2Utils.extract_all_information()

	item.stack_count = p.read_uint16()

	return item
