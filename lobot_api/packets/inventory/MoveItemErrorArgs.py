from lobot_api.packets.inventory.MoveItemError import MoveItemError


class MoveItemErrorArgs:
	def __init__(self, error_types: MoveItemError):
		self.error_type = error_types
