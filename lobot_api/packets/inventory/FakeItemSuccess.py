from lobot_api.packets.IRequest import IRequest
from lobot_api.packets.inventory.ObjectActionStage import ObjectActionStage
from silkroad_security_api_1_4.Packet import Packet


# # This packet is used to update the bot inventory for townscript item buys.
# The clientdoes not expect any Item changes (B034), so this packet is sent to the client to tell it that there is an incoming change.
# This allows simulating an item being picked up
# 01                                                ................
# 02                                                ................
# 01                                                ................
# D6 3A F8 01                                       .:..............
# Fake item pickup starts here
# # 01                                                stage: object = 1 starting
# 01                                                success 1
# This packet can be sent to the client to update the inventory
# # 01                                                ................
# 06                                                ................
# 32                                                2...............
# 00 00 00 00                                       ................
# 0D 00 00 00                                       ................
# 20 00                                             ................
# # 02                                                stage: object = 2 ending
# 00                                                ................
class FakeItemSuccess(IRequest):
	__Stage__: ObjectActionStage

	@property
	def opcode(self) -> int:
		return 0x7034

	def __init__(self, stage: ObjectActionStage = ObjectActionStage.Start):
		self.Stage = stage

	def _add_payload_(self) -> Packet:
		p: Packet = Packet(self.opcode)
		if self.Stage is ObjectActionStage.Start:
			p.write_uint8(0x01)
			p.write_uint8(0x01)
		else:
			p.write_uint8(0x02)
			p.write_uint8(0x00)
		return p
