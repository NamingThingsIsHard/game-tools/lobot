from typing import Optional

from lobot_api.Bot import Bot
from lobot_api.Event import Event
from lobot_api.packets.IHandler import IHandler
from lobot_api.packets.IRequest import IRequest
from lobot_api.packets.inventory.InventoryUpdateArgs import InventoryUpdateArgs
from lobot_api.structs.Item import PetStatus, PetScroll
from silkroad_security_api_1_4.Packet import Packet


# update inventory stats after disjoint alchemy material
# # 29                                                updateSlot 42 (slot with void rondo)
# 08                                                update_type 8 (update?)
# A1 00                                             newstack_count 161
# update pet scroll to alive after using grass of life
# # 18                                                useSlot
# EC 30                                             type (grass of life)
# 16                                                targetSlot
# # 01                                                success
# 18                                                useSlot
# 01 00                                             amount
# EC 30                                             type (grass of life)
# # 16                                                petscroll slot
# 40                                                update type (pet ?)
# 03                                                status alive


class UpdateInventory(IHandler):
	on_inventory_updated = Event()
	on_petscroll_updated = Event()

	@property
	def server_opcode(self) -> int:
		return 0x3040

	@classmethod
	def _parse_packet_(cls, p: Packet) -> Optional[IRequest]:
		slot_to_update = p.read_uint8()
		update_type = p.read_uint8()  # 8 => change stacks?
		e: InventoryUpdateArgs = InventoryUpdateArgs(slot_to_update, update_type)
		# switch (updateType)
		if update_type == 8:
			Bot.char_data.inventory.items[slot_to_update].stack_count = p.read_uint16()
		elif update_type == 0x40:
			item = Bot.char_data.inventory.items.get(slot_to_update, None)
			if item and isinstance(item, PetScroll):
				update_status = p.read_uint8()
				item.status = PetStatus(update_status)  # 1 = Unsummoned, 2 = Summoned, 3 = Alive, 4 = Dead
				e.new_status = update_status
				cls.on_petscroll_updated.notify(e)
		else:
			cls.on_inventory_updated.notify(e)
		return None
