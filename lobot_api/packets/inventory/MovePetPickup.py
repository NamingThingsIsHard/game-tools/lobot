from lobot_api.Bot import Bot
from lobot_api.Event import Event
from lobot_api.packets.inventory.AbstractItemMoveStrategy import AbstractItemMoveStrategy
from lobot_api.packets.inventory.IItemMoveHandleStrategy import IItemMoveHandleStrategy
from lobot_api.packets.inventory.ItemMoveArgs import ItemMoveArgs
from lobot_api.packets.inventory.ItemMoveParams import ItemMoveParams
from lobot_api.packets.inventory.ItemMovementType import ItemMovementType
from lobot_api.structs.Item import Item
from silkroad_security_api_1_4.Packet import Packet


class MovePetPickup(AbstractItemMoveStrategy, IItemMoveHandleStrategy):
	@classmethod
	def type(cls) -> ItemMovementType:
		return ItemMovementType.PetToPet

	on_item_pickedUp = Event()

	@classmethod
	def handle_move(cls, p: ItemMoveParams):
		item = Item()
		pet_id = p.packet.read_uint32()
		item.slot = p.packet.read_uint8()

		cls._parse_new_item_(item, p, Bot.pet_inventory)
		cls.on_item_pickedUp.notify(ItemMoveArgs(p.from_slot, item.slot, cls.type()))

	@classmethod
	def move_item(cls, p: ItemMoveParams) -> Packet:
		super().move_item(p)
		p.packet.write_uint32(Bot.selected_npc.unique_id)
		return p.packet
