from lobot_api.Bot import Bot
from lobot_api.Event import Event
from lobot_api.packets.inventory.AbstractItemMoveStrategy import AbstractItemMoveStrategy
from lobot_api.packets.inventory.IItemMoveHandleStrategy import IItemMoveHandleStrategy
from lobot_api.packets.inventory.ItemMoveArgs import ItemMoveArgs
from lobot_api.packets.inventory.ItemMoveParams import ItemMoveParams
from lobot_api.packets.inventory.ItemMovementType import ItemMovementType
from silkroad_security_api_1_4.Packet import Packet


# move 4 mp recovery herbs from storage slot index 1 to inventory slot 35
# # 03                                                3: moveOption = storage to inventory
# 01                                                from_slot
# 23                                                to_slot(selected by client, can end up on different slot if occupied)
# B1 01 00 00                                       selected_npc_id
# # 01                                                successFlag
# 03                                                moveOption
# 01                                                from_slot
# 24                                                to_slot(slot 35 was occupied)
class MoveStorageToInventory(AbstractItemMoveStrategy, IItemMoveHandleStrategy):
	on_moved = Event()

	@classmethod
	def type(cls) -> ItemMovementType: return ItemMovementType.StorageToInventory

	@classmethod
	def handle_move(cls, p: ItemMoveParams):
		super()._move_(Bot.storage, p.from_slot, p.to_slot, Bot.char_data.inventory.items)
		cls.on_moved.notify(ItemMoveArgs(p.from_slot, p.to_slot, cls.type()))

	@classmethod
	def move_item(cls, p: ItemMoveParams) -> Packet:
		p.packet.write_uint8(cls.type().value)
		p.packet.write_uint8(p.from_slot)
		p.packet.write_uint8(p.to_slot)
		p.packet.write_uint32(Bot.selected_npc.unique_id)
		return p.packet
