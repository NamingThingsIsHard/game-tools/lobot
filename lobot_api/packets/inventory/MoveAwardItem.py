# item move
# # 01                                                success
# 0E                                                movement_type 14 -> item create_request spawn
# 27                                                target
# 00                                                from_slot?
# 00 00 00 00                                       unknownFlag(owner? => 0 is self)
# 68 5E 00 00                                       reward item 24168 (Beginner scroll of experience(150%))
# 03 00                                             amount 3
from lobot_api.packets.inventory.AbstractItemMoveStrategy import AbstractItemMoveStrategy
from lobot_api.packets.inventory.IItemMoveHandleStrategy import IItemMoveHandleStrategy
from lobot_api.packets.inventory.ItemMoveParams import ItemMoveParams
from lobot_api.packets.inventory.ItemMovementType import ItemMovementType
from lobot_api.structs.Item import Item


class MoveAwardItem(AbstractItemMoveStrategy, IItemMoveHandleStrategy):
	@classmethod
	def type(cls) -> ItemMovementType: return ItemMovementType.AwardItem

	@classmethod
	def handle_move(cls, p: ItemMoveParams):
		item: Item = Item()
		item.slot = p.packet.read_uint8()
		from_slot = p.packet.read_uint8()
		super()._parse_new_item_(item, p)
