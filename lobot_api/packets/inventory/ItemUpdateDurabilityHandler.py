from typing import Optional

from lobot_api.Bot import Bot
from lobot_api.packets.IHandler import IHandler
from lobot_api.packets.IRequest import IRequest
from lobot_api.structs.Item import Item
from silkroad_security_api_1_4.Packet import Packet


# 03                                                slot = 3 Gauntlet
# 2F 00 00 00                                       durability = 47
class ItemUpdateDurabilityHandler(IHandler):
	# on_durability_updated = Event()
	# @staticmethod
	# def __on_durability_updated__(e: DurabilityUpdateArgs):
	# EventHandlerhandler = DurabilityUpdated
	# if handler is not None:
	#    handler(None, e)
	@property
	def server_opcode(self) -> int: return 0x3052

	@classmethod
	def _parse_packet_(cls, p: Packet) -> Optional[IRequest]:
		slot = p.read_uint8()
		value: Item = Bot.char_data.inventory.items.get(slot, None)
		if value:
			pass
		# on_durability_updated.notify(DurabilityUpdateArgs(value))
		return None
