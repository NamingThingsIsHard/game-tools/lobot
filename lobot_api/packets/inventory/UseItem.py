from typing import *

from lobot_api.Bot import Bot
from lobot_api.Event import Event
from lobot_api.packets.ExchangePacket import ExchangePacket
from lobot_api.packets.IRequest import IRequest
from lobot_api.packets.inventory.ItemUseArgs import ItemUseArgs
from lobot_api.packets.inventory.UseItemType import UseItemType
# # Use X-Large HP Potion in item_slot 1
# # 0D                                                item_slot(13)
# EC 08                                             unknown(2284) https://www.elitepvpers.com/forum/sro-pserver-guides-releases/4396959-0x704c-item-type-calculation-vsro.html
# # 01                                                successFlag
# 0D                                                item_slot(13)
# E7 03                                             stackAmount(999)
# EC 08                                             unknown(2284)
# using mppot
# # 2A                                                *...............
# EC 10                                             mppot type ec 236 main type & 10 item type
# # 01                                                ................
# 2A*...............
# 08 00                                             ................
# EC 10                                             ................
# hp pot
# # 28                                                (...............
# EC 08                                             hppot type ec 236 main type & 08 item type
# # 01                                                ................
# 28                                                (...............
# 0E 00                                             ................
# EC 08                                             ................
# vigour
# # 2C                                                ,...............
# EC 18                                             vigour type ec 236 main type & 18 item type
# # 01                                                ................
# 2C                                                ,...............
# 15 00                                             ................
# EC 18                                             ................
# universal pill
# # 2B                                                +...............
# 6C 31                                             universal pill type 6c & 31
# # 01                                                ................
# 2B                                                +...............
# 36 00                                             6...............
# 6C 31                                             universal pill type 6c & 31
from lobot_api.structs.Item import Item
from silkroad_security_api_1_4.Packet import Packet


class UseItem(ExchangePacket):
	@property
	def opcode(self) -> int:
		return 0x704C

	@property
	def server_opcode(self) -> int:
		return 0xB04C

	on_item_used = Event()

	def __init__(self, item_slot=0, type_value: UseItemType = UseItemType.Normal, target_id=0, target_slot=0):
		self.item_slot = item_slot
		self.type = type_value
		self.target_id = target_id
		self.target_slot = target_slot

	@staticmethod
	def __calculate_item_type__(cash_item: int, type_value: int) -> int:
		"""Calculate the item-use-type using the item's cashItem/Item mall value, the value 3, and the item's3-hex pk2Itemtype.
		E.g. for an hp pot the value would be 2284, using \n
		1	4	ITEM_ETC_HP_POTION_01	HP ?? ??	xxx	SN_ITEM_ETC_HP_POTION_01	SN_ITEM_ETC_HP_POTION_01_TT_DESC	0	0	3	3	1	1	180000	3	0	1	1	1	255	3	1	0	0	1	0	60	0	0	0	1	21	-1	0	-1	0	-1	0	-1	0	-1	0	0	0	0	0	0	0	100	0	0	0	xxx	item\etc\drop_ch_bag.bsr	item\etc\hp_potion_01.ddj	xxx	xxx	1000	2	0	0	1	0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0	0	0	0	0	0	0	0	0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	120	HP???	0	HP???(%)	0	MP???	0	MP???(%)	-1	xxx	-1	xxx	-1	xxx	-1	xxx	-1	xxx	-1	xxx	-1	xxx	-1	xxx	-1	xxx	-1	xxx	-1	xxx	-1	xxx	-1	xxx	-1	xxx	-1	xxx	-1	xxx	0	0

		the important part starts after the 7th column:	0	0	3	3	1	1 \n
		with 0(cashItem)    0(Bionic)    3(type1--isItem (always 3))    3(type2--{1 = equip 2 = pets 3 = etc}) 1(type3--itemtype)  1(type4--itemsubtype)

		result should be EC 08 -> 8EC ->  2284 \n
		CashItem(0) + type1(3*4) + type2(3*32) + type3(1*128) + type4(1* 2048) \n
		= 0 + 12 + 96 + 128 + 2048 \n
		= 2248"""
		type1 = 3
		type2 = (type_value & 0xF000) >> 12
		type3 = (type_value & 0xF00) >> 8
		type4 = type_value & 0xF
		# item_type = CashItem + (type_id1 * 4) + (type_id2 * 32) + (type_id3 * 128) + (type_id4 * 2048)
		return 0xFFFF & cash_item + (type1 * 4) + (type2 * 32) + (type3 * 128) + (type4 * 2048)

	def _add_payload_(self) -> Packet:
		p: Packet = Packet(self.opcode, True)
		p.write_uint8(self.item_slot)
		item: Item = Bot.char_data.inventory.items.get(self.item_slot, None)
		if item:
			item_value = item.type.value & 0xFFFF  # Mask out possible MSB custom type bytes
			item_use_id = UseItem.__calculate_item_type__(1 if item.is_mall_item else 0, item_value)
			p.write_uint16(item_use_id)

		if self.type == UseItemType.Normal:
			pass
		elif self.type == UseItemType.UseOnPet:
			if Bot.transport.unique_id == self.target_id or Bot.growth_pet.unique_id == self.target_id:
				p.write_uint32(self.target_id)
		elif Type == UseItemType.UseOnItem:
			value: Item = Bot.char_data.inventory.items.get(self.target_slot, None)
			if value:
				p.write_uint8(self.target_slot)
		return p
		# 01                                                successFlag
		# 26                                                slot
		# 00 00                                             newAmount
		# EC 08                                             itemUseType

	@classmethod
	def _parse_packet_(cls, p: Packet) -> Optional[IRequest]:
		success = p.read_uint8()
		if success == 0x01:
			slot = p.read_uint8()
			new_amount = p.read_uint16()
			# Reduce stack count of consumables (ITEM_ETC) or remove them entirely from the inventory, if their stack count reaches 0.
			item = Bot.char_data.inventory.items.get(slot, None)
			if item and item.pk2_item.is_consumable():
				item.stack_count = new_amount
				if item.stack_count < 1:
					Bot.char_data.inventory.items.pop(item.slot)
			cls.on_item_used.notify(ItemUseArgs(slot, item))
		return None
