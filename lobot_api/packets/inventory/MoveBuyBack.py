from lobot_api.Bot import Bot
from lobot_api.packets.inventory.AbstractItemMoveStrategy import AbstractItemMoveStrategy
from lobot_api.packets.inventory.IItemMoveHandleStrategy import IItemMoveHandleStrategy
from lobot_api.packets.inventory.ItemMoveParams import ItemMoveParams
from lobot_api.packets.inventory.ItemMovementType import ItemMovementType
from silkroad_security_api_1_4.Packet import Packet


# Buyback
# # 02 01 00 00                                       npc_id
# 00                                                from_slot(BuybackList)
# gold update
# # 01                                                successFlag
# CC 0D 00 00 00 00 00 00                           finalAmount 3532
# 01                                                endFlag
# # 01                                                successFlag
# 22                                                movement_type = 34 buyback
# 24                                                to_slot
# 00                                                from_slot(BuybackList)
# 01 00                                             amount
class MoveBuyBack(AbstractItemMoveStrategy, IItemMoveHandleStrategy):
	@classmethod
	def type(cls) -> ItemMovementType: return ItemMovementType.BuyBack

	@classmethod
	def handle_move(cls, p: ItemMoveParams):
		to_slot = p.packet.read_uint8()
		from_slot = p.packet.read_uint8()
		Bot.char_data.inventory.items[to_slot] = Bot.buy_back_list[from_slot]
		Bot.buy_back_list.pop(from_slot)

	@classmethod
	def move_item(cls, p: ItemMoveParams) -> Packet:
		return p.packet
