from lobot_api.Bot import Bot
from lobot_api.Event import Event
from lobot_api.packets.inventory.AbstractItemMoveStrategy import AbstractItemMoveStrategy
from lobot_api.packets.inventory.IItemMoveHandleStrategy import IItemMoveHandleStrategy
from lobot_api.packets.inventory.ItemMoveArgs import ItemMoveArgs
from lobot_api.packets.inventory.ItemMoveParams import ItemMoveParams
from lobot_api.packets.inventory.ItemMovementType import ItemMovementType
from silkroad_security_api_1_4.Packet import Packet


# drop ume copper ring
# # 07                                                movement_type
# 24                                                from_slot
# # 01                                                successFlag
# 07                                                movement_type
# 24                                                from_slot
class MoveDrop(AbstractItemMoveStrategy, IItemMoveHandleStrategy):
	on_dropped = Event()

	@classmethod
	def type(cls) -> ItemMovementType: return ItemMovementType.Drop

	@classmethod
	def handle_move(cls, p: ItemMoveParams):
		Bot.char_data.inventory.items.pop(p.from_slot)
		cls.on_dropped.notify(ItemMoveArgs(p.from_slot, p.to_slot, cls.type()))

	@classmethod
	def move_item(cls, p: ItemMoveParams) -> Packet:
		p.packet.write_uint8(cls.type().value)
		p.packet.write_uint8(p.to_slot)
		return p.packet
