from enum import Enum
from typing import Optional

from lobot_api.Bot import Bot
from lobot_api.connection import proxy
from lobot_api.packets.ExchangePacket import ExchangePacket
from lobot_api.packets.IRequest import IRequest
from lobot_api.scripting.Point import Point
from silkroad_security_api_1_4.Packet import Packet


class PetActionType(Enum):  # byte
	Move = 0
	Pickup = 1


class PetAction(ExchangePacket):
	@property
	def server_opcode(self) -> int:
		return 0xB0C5

	@property
	def opcode(self) -> int:
		return 0x70C5

	def __init__(self, pet_action=PetActionType.Pickup, unique_id: int = 0, x: int = 0, y: int = 0):
		self.action_type = pet_action
		self.target_id = 0
		self.to_point = None

		if pet_action is PetActionType.Pickup:
			self.target_id = unique_id
		else:
			self.to_point: Point = Point.from_absolute(x, y)

	def __add_pickup_payload__(self) -> Packet:
		p: Packet = Packet(self.opcode)
		p.write_int32(Bot.pickup_pet.unique_id)
		p.write_uint8(8)
		p.write_uint32(self.target_id)
		return p

	def _add_move_payload_(self) -> Packet:
		if Bot.char_data is None or proxy.joymax is None:
			return None
		position = self.to_point
		packet = Packet(self.opcode)
		packet.write_uint32(Bot.char_data.transport_id)
		packet.write_uint8(1)
		packet.write_uint8(1)
		packet.write_uint8(position.x_sector)
		packet.write_uint8(position.y_sector)
		if Bot.char_data.is_in_cave:
			# regionX = Convert.to_byte(Bot.char.CaveFloor) // will use normal regionX for now
			region_y = 0x80
			x_pos = position.x_offset
			y_pos = position.y_offset
		else:
			x_pos = 0xFFFF & position.x_offset
			y_pos = 0xFFFF & position.y_offset
		packet.write_uint16(x_pos)
		packet.write_uint16(0)
		packet.write_uint16(y_pos)
		return packet

	def _add_payload_(self) -> Packet:
		if self.action_type == PetActionType.Move:
			return self._add_move_payload_()
		elif self.action_type == PetActionType.Pickup:
			return self.__add_pickup_payload__()

	@classmethod
	def _parse_packet_(cls, p: Packet) -> Optional[IRequest]:
		return None
