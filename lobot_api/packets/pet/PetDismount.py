from lobot_api.Bot import Bot
from lobot_api.packets.IRequest import IRequest
from silkroad_security_api_1_4.Packet import Packet


# # 00                                                ridingFlag
# 25 BC 6A 00                                       pet_id
class PetDismount(IRequest):
	@property
	def opcode(self) -> int: return 0x70CB

	def _add_payload_(self) -> Packet:
		p: Packet = Packet(self.opcode, False)
		p.write_uint8(0)
		p.write_uint32(Bot.char_data.transport_id)
		return p
