from ctypes import *
from typing import Optional

from lobot_api.Bot import Bot
from lobot_api.packets.ExchangePacket import ExchangePacket
from lobot_api.packets.IRequest import IRequest
from silkroad_security_api_1_4.Packet import Packet


class PetUpdateSettings(ExchangePacket):
	def __init__(self, unique_id: c_uint32, type_value: c_byte, value: c_uint32):
		self.unique_id = unique_id
		self.update_type = type_value
		self.value = value

	@property
	def server_opcode(self) -> int:
		return 0xB420

	@property
	def opcode(self) -> int:
		return 0x7420

	def _add_payload_(self) -> Packet:
		p: Packet = Packet(self.opcode)
		p.write_uint32(self.unique_id)
		p.write_uint8(self.update_type)
		p.write_uint32(self.value)
		return p

	@classmethod
	def _parse_packet_(cls, p: Packet) -> Optional[IRequest]:
		success = p.read_uint8()
		if success == 0x01:
			unique_id = p.read_uint32()
			if Bot.growth_pet.unique_id == unique_id:
				type_value = p.read_uint8()
				if type_value == 0x01:  # Attack/Defense update
					Bot.growth_pet.attack_status = p.read_uint32() == 0x01
			if Bot.pickup_pet.unique_id == unique_id:
				type_value = p.read_uint8()
				if type_value == 0x02:
					Bot.pickup_pet.update_settings(p.read_uint32())
		return None
