# from typing import Optional
#
# from lobot_api.Bot import Bot
# from lobot_api.Event import Event
# from lobot_api.packets.IRequest import IRequest
# from lobot_api.packets.pet.PetInfoHandler import PetInfoHandler
# from silkroad_security_api_1_4.Packet import Packet
#
#
# class PetUpdateBeginHandler(PetInfoHandler):
# 	"""
# 	Packet sent before PetUpdateHandler packet is sent.
# 	"""
#
# 	on_updated = Event()
#
# 	@property
# 	def server_opcode(self) -> int:
# 		return 0x325C
#
# 	@classmethod
# 	def _parse_packet_(cls, p: Packet) -> Optional[IRequest]:
# 		unique_id = p.read_uint32()
# 		if Bot.pickup_pet.unique_id != unique_id:
# 			return
#
# 		p.read_uint32()  # unknown
# 		p.read_uint32()  # unknown
# 		success = p.read_uint8()
#
# 		if success != 1:
# 			return
#
# 		cls.on_updated.notify(unique_id)
#
# 		return None
