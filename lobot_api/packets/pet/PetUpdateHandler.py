from typing import Optional

from lobot_api.Bot import Bot
from lobot_api.Event import Event
from lobot_api.globals.PK2DataGlobal import PK2DataGlobal
from lobot_api.globals.SpawnListsGlobal import SpawnList
from lobot_api.packets.IHandler import IHandler
from lobot_api.packets.IRequest import IRequest
from lobot_api.packets.inventory import ItemParser
from lobot_api.packets.npc.DespawnArgs import DespawnArgs
from lobot_api.pk2.PK2NPC import PK2NPC
from lobot_api.statemachine.AutoPotions import AutoPotions
from silkroad_security_api_1_4.Packet import Packet


# # FC 23 7F 07                                       unique_id
# 03                                                type gain exp
# 34 25 01 00 00 00 00 00                           76050 exp
# E5 EB DC 06                                       owner_id = character
# # FC 23 7F 07                                       unique_id
# 04                                                type HGP update
# 0F 27                                             hgp 9999
# # FC 23 7F 07                                       unique_id
# 07                                                type model update
# E7 17 00 00                                       model number 6119


class PetUpdateHandler(IHandler):
	on_spawned = Event()
	on_hgp_changed = Event()
	on_despawned = Event()

	@property
	def server_opcode(self) -> int:
		return 0x30C9

	@classmethod
	def _parse_packet_(cls, p: Packet) -> Optional[IRequest]:
		unique_id = p.read_uint32()
		if Bot.growth_pet.unique_id == unique_id:
			type_value = p.read_uint8()
			if type_value == 0x01:  # Unsummon: HGP stop countingdown
				cls.on_despawned.notify(DespawnArgs(unique_id, SpawnList.Pets))
				AutoPotions.handle_hgp_despawn()
			if type_value == 0x02:
				cls.on_spawned.notify()
				AutoPotions.handle_hgp_spawn()
			elif type_value == 0x03:  # gain exp
				exp = p.read_uint64()
				unknown = p.read_uint32()  # does not seem to be owner_id
				Bot.growth_pet.exp += exp
				current_max_exp = PK2DataGlobal.level_data.get(Bot.growth_pet.level, None)
				if current_max_exp:
					# take multiple level ups into account
					while Bot.growth_pet.exp > current_max_exp:
						next_level_max_exp = PK2DataGlobal.level_data.get(Bot.growth_pet.level + 1, None)
						if next_level_max_exp:
							Bot.growth_pet.level += 1
							Bot.growth_pet.exp -= current_max_exp
							current_max_exp = next_level_max_exp
						else:
							break  # exit loop if max level exp is somehow less than current exp
			elif type_value == 0x04:  # HGP update
				Bot.growth_pet.hgp = p.read_uint16()
				# cls.on_hgp_changed.notify(BoolEventArgs(False))
				AutoPotions.try_use_hgp_pot(Bot.growth_pet.unique_id)
			elif type == 0x07:  # ref_id update for Model and Max HP after leveling up
				Bot.char_data.ref_id = p.read_uint32()
				pk2_pet: PK2NPC = PK2DataGlobal.npcs.get(Bot.char_data.ref_id, None)
				if pk2_pet:
					Bot.char_data.max_hp = pk2_pet.HP
					Bot.char_data.level = 0xFF & pk2_pet.level
			return None
		elif Bot.pickup_pet.unique_id == unique_id:
			option = p.read_uint8()
			if option == 0x01:  # Unsummon
				cls.on_despawned.notify(DespawnArgs(unique_id, SpawnList.Pets))
			elif option == 2:  # update_inventory
				Bot.pet_inventory_size = p.read_uint8()
				count_new_items = p.read_uint8()

				for i in range(0, count_new_items):
					item = ItemParser.parse_item(p)
					Bot.pet_inventory[item.slot] = item
			return None
