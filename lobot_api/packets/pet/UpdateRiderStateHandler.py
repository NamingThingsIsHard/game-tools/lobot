from typing import Optional

from lobot_api.Bot import Bot
from lobot_api.Event import Event
from lobot_api.globals.SpawnListsGlobal import SpawnListsGlobal
from lobot_api.packets.IHandler import IHandler
from lobot_api.packets.IRequest import IRequest
from silkroad_security_api_1_4.Packet import Packet


# cos info
# # 5B B5 D7 01                                       pet ID
# 8F 08 00 00                                       ref_id (red horse)
# D7 03 00 00                                       maxhp
# D7 03 00 00                                       current hp
# 00                                                flag
# update riderstate --> spawn horse
# # 01                                                riderIsSelf ? 
# E1 90 D7 01                                       rider_id -> check if self
# 01                                                ridingState
# 5B B5 D7 01                                       vehicle_id
# update riderstate --> despawn horse
# # 01                                                riderIsSelf?
# FD BB 6A 00                                       rider_id
# 00                                                rideFlag
# 25 BC 6A 00                                       pet_id
class UpdateRiderStateHandler(IHandler):
	on_mount_or_dismount = Event()

	@property
	def server_opcode(self) -> int:
		return 0xB0CB

	@classmethod
	def _parse_packet_(cls, p: Packet) -> Optional[IRequest]:
		is_self = p.read_uint8()
		if is_self == 0x01:
			rider_id = p.read_uint32()
			if rider_id == Bot.char_data.unique_id:
				Bot.char_data.transport_flag = p.read_uint8()
				Bot.char_data.transport_id = p.read_uint32()
				if Bot.char_data.transport_flag == 0:
					Bot.char_data.transport_id = 0
					Bot.transport.unique_id = 0
				else:
					Bot.transport.unique_id = Bot.char_data.transport_id
					pet = SpawnListsGlobal.Pets.get(Bot.char_data.transport_id, None)
					if pet:
						Bot.transport.set_from_pet(pet)

				cls.on_mount_or_dismount.notify()
		return None
