from ctypes import *

from lobot_api.packets.IRequest import IRequest
from silkroad_security_api_1_4.Packet import Packet


# # D8 47 79 07                                       unique_id
class PetUnsummon(IRequest):

	def __init__(self, unique_id: c_uint32):
		self.unique_id = unique_id

	@property
	def opcode(self) -> int: return 0x7116

	def _add_payload_(self) -> Packet:
		p: Packet = Packet(self.opcode)
		p.write_uint32(self.unique_id)
		return p
