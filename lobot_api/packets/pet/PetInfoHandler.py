import logging
from typing import Optional

from lobot_api.Bot import Bot
from lobot_api.globals.PK2DataGlobal import PK2DataGlobal
from lobot_api.logger.Logger import HANDLER
from lobot_api.packets.IHandler import IHandler
from lobot_api.packets.IRequest import IRequest
from lobot_api.packets.inventory import ItemParser
from lobot_api.pk2.PK2NPC import PK2NPCType
from lobot_api.structs.Item import Item
from silkroad_security_api_1_4.Packet import Packet


class PetInfoHandler(IHandler):
	@property
	def server_opcode(self) -> int:
		return 0x30C8

	@classmethod
	def _parse_packet_(cls, p: Packet) -> Optional[IRequest]:
		unique_id = p.read_uint32()
		pet_ref_id = p.read_uint32()
		pet = PK2DataGlobal.npcs.get(pet_ref_id, 0)
		if pet:
			if pet.type is PK2NPCType.PetPickup:
				Bot.pickup_pet.unique_id = unique_id
				Bot.pet_inventory.clear()
				p.read_uint64()  # Unknown
				p.read_uint32()  # Unknown
				Bot.pickup_pet.custom_name = p.read_ascii()
				Bot.pet_inventory_size = p.read_uint8()
				item_count = p.read_uint8()
				for i in range(0, item_count):
					item: Item = ItemParser.parse_item(p)  # ref p
					Bot.pet_inventory[item.slot] = item
			elif pet.type is PK2NPCType.PetGrowth:
				Bot.growth_pet.unique_id = unique_id  # ref_id is used for for pet model and max hp update on lvl up
				Bot.growth_pet.ref_id = pet_ref_id  # ref_id is used for for pet model and max hp update on lvl up
				Bot.growth_pet.current_hp = p.read_uint32()
				max_hp = int(p.read_uint32())
				exp = p.read_uint64()
				Bot.growth_pet.level = p.read_uint8()
				Bot.growth_pet.exp = exp  # update here to get EXPpercentage
				if max_hp > 0:
					Bot.growth_pet.max_hp = max_hp
				# pk2_pet: PK2NPC = PK2DataGlobal.npcs.get(Bot.growth_pet.ref_id, None)
				if Bot.growth_pet.ref_id in PK2DataGlobal.npcs:
					Bot.growth_pet.max_hp = PK2DataGlobal.npcs[Bot.growth_pet.ref_id].HP
				Bot.growth_pet.hgp = p.read_uint16()
				Bot.growth_pet.attack_status = p.read_uint32() == 0x01
				Bot.growth_pet.custom_name = p.read_ascii()
				unkn0 = p.read_uint8()  # Unknown
				Bot.growth_pet.owner_id = p.read_uint32()
				Bot.growth_pet.ScrollSlot = p.read_uint8()  # Unknown
			elif pet.type in [PK2NPCType.PetTransport, PK2NPCType.PetRide]:
				Bot.char_data.transport_id = unique_id
				Bot.transport.current_hp = p.read_uint32()
				Bot.transport.max_hp = int(p.read_uint32())
		else:
			logging.log(HANDLER, str.format(f"Unknown pet found: {str(p)}"))
		return None
