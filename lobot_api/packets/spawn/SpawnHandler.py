import logging
import sys
from abc import ABC
from typing import List

from lobot_api.Bot import Bot
from lobot_api.Event import Event
from lobot_api.globals.PK2DataGlobal import PK2DataGlobal
from lobot_api.globals.SpawnListsGlobal import SpawnListsGlobal, SpawnList
from lobot_api.logger.Logger import HANDLER
from lobot_api.packets.IHandler import IHandler
from lobot_api.packets.npc.AbstractSpawnParseStrategy import AbstractSpawnParseStrategy
from lobot_api.packets.npc.DespawnArgs import DespawnArgs
from lobot_api.packets.npc.SpawnArgs import SpawnArgs
from lobot_api.packets.spawn.GroupSpawnBeginHandler import GroupSpawnBeginHandler
from lobot_api.pk2.PK2Item import PK2ItemType
from lobot_api.pk2.PK2NPC import PK2NPCType
from lobot_api.pk2.PK2TeleporterTypes import PK2Teleporter
from lobot_api.scripting.Point import Point
from lobot_api.statemachine.AutoPotions import AutoPotions
from lobot_api.structs.CharData import Buff
from lobot_api.structs.CharData import CharData
from lobot_api.structs.DebuffStatus import DebuffStatus
from lobot_api.structs.GrowthPet import GrowthPet
from lobot_api.structs.Item import Equipment, Item
from lobot_api.structs.ItemDrop import ItemDrop
from lobot_api.structs.Monster import Monster, MonsterType
from lobot_api.structs.ObjectStatus import ObjectStatus
from lobot_api.structs.Pet import Pet
from lobot_api.structs.PickupPet import PickupPet
from lobot_api.structs.Portal import Portal
from lobot_api.structs.TransportPet import TransportPet
from silkroad_security_api_1_4.Packet import Packet


class ParseChar(AbstractSpawnParseStrategy):
	on_parsed = Event()

	def parse(self, p: Packet, ref_id):
		try:
			player: CharData = CharData()
			inventory_contains_job_equipment = 0
			player.scale = p.read_uint8()  # Volume/Height
			player.level = p.read_uint8()  # Rank
			p.read_uint8()  # Icons
			p.read_uint8()  # Unknown
			p.read_uint8()  # Max Slots
			player.inventory.item_count = p.read_uint8()
			for i in range(0, player.inventory.item_count):
				equip: Equipment = Equipment(Item())
				equip.ref_id = p.read_uint32()
				equip.slot = i
				item_long_id: str = PK2DataGlobal.items[equip.ref_id].long_id
				if equip.pk2_item.is_equipment_item():  # re.match(r'^ITEM_(([0-9A-Z]+|ROC|FORT)_)?(EU|CH).+', item_long_id):
					equip.opt_level = p.read_uint8()  # Item Plus
				if "_TRADE_TRADER_" in item_long_id or "_TRADE_HUNTER_" in item_long_id or "_TRADE_THIEF_" in item_long_id:
					inventory_contains_job_equipment = 1
				player.inventory.equipment[equip.slot] = equip
			player.inventory.avatar_inventory.size = p.read_uint8()  # Max Avatars Slot
			player.inventory.avatar_inventory.item_count = p.read_uint8()
			for i in range(0, player.inventory.avatar_inventory.item_count):
				p.read_uint32()  # Avatar ID
				p.read_uint8()  # Avatar Plus
			mask: int = p.read_uint8()
			if mask == 1:
				mask_id = p.read_uint32()
				masklong_id: str = PK2DataGlobal.npcs[mask_id].long_id
				if masklong_id.startswith("CHAR"):
					p.read_uint8()
					count = p.read_uint8()
					for i in range(0, count):
						p.read_uint32()
			player.unique_id = p.read_uint32()
			x_sector = p.read_uint8()
			y_sector = p.read_uint8()
			x_offset = p.read_single()
			z_offset = p.read_single()
			y_offset = p.read_single()
			angle = p.read_uint16()
			player.position = Point(x_sector, y_sector, x_offset, z_offset, y_offset, angle)
			move = p.read_uint8()  # Moving
			player.movement_type = p.read_uint8()  # Running
			if move == 1:
				player.destination.x_sector = p.read_uint8()
				player.destination.y_sector = p.read_uint8()
				if player.destination.y_sector == 0x80:
					player.destination.x_offset = 0xFFFF & (p.read_uint16() - p.read_uint16())
					player.destination.z = 0xFFFF & (p.read_uint16() - p.read_uint16())
					player.destination.y_offset = 0xFFFF & (p.read_uint16() - p.read_uint16())
				else:
					player.destination.x_offset = p.read_uint16()
					player.destination.z = p.read_uint16()
					player.destination.y_offset = p.read_uint16()
			else:
				player.sky_click_flag = p.read_uint8()  # 0 = spinning, 1 = skyclick - No Destination
				player.angle = p.read_uint16()  # Angle
			# p.read_uint8() // unknown from old sro version
			player.is_alive = p.read_uint8() == 0x01  # Alive
			player.debuff_status = DebuffStatus(p.read_uint8())  # -> Check for is not 0
			player.motion_state = p.read_uint8()  # (0 = None, 2 = Walking, 3 = Running, 4 = Sitting)
			player.status = ObjectStatus(p.read_uint8())  # (0 = None,2 = ??@GrowthPet, 3 = Invincible, 4 = Invisible)
			player.walk_speed = p.read_single()
			player.run_speed = p.read_single()
			player.hwan_speed = p.read_single()
			player.active_buffs = p.read_uint8()  # Buffs count
			for a in range(0, player.active_buffs):
				skill_id = p.read_uint32()
				skill_long_id: str = PK2DataGlobal.skills[skill_id].long_id
				p.read_uint32()  # Temp ID
				if skill_long_id.startswith("SKILL_EU_CLERIC_RECOVERYA_GROUP") or skill_long_id.startswith(
						"SKILL_EU_BARD_BATTLAA_GUARD") or skill_long_id.startswith(
					"SKILL_EU_BARD_DANCEA") or skill_long_id.startswith("SKILL_EU_BARD_SPEEDUPA_HITRATE"):
					p.read_uint8()
			player.name = p.read_ascii()
			player.job_type = p.read_uint8()  # (0 = None, 1 = Trader, 2 = Thief, 3 = Hunter)
			player.job_level = p.read_uint8()  # Job level
			player.PVP_state = p.read_uint8()
			player.transport_flag = p.read_uint8()
			player.in_combat = p.read_uint8()
			if player.transport_flag == 1:
				player.transport_id = p.read_uint32()
			player.scroll_mode = p.read_uint8()  # 0 = None, 1 = Return Scroll, 2 = Bandit Return Scroll
			player.interaction_mode = p.read_uint8()  # 0 = None 2 = P2P, 4 = P2N_TALK, 6 = OPNMKT_DEAL
			p.read_uint8()
			player.GuildName = p.read_ascii()
			if inventory_contains_job_equipment == 0:
				player.guild_id = p.read_uint32()
				player.guild_grant_name = p.read_ascii()
				player.guild_last_crest = p.read_uint32()
				player.union_id = p.read_uint32()
				player.union_last_crest = p.read_uint32()
				player.guild_is_friendly = p.read_uint8()
				player.guild_siege_authority = p.read_uint8()
			if player.interaction_mode == 4:  # Stall
				stall_name: str = p.read_ascii()  # old version p.read_ascii('12000') // Stall Name UTF-32 bit
				stall_ref_id = p.read_uint32()  # Unknown
			equipment_cooldown = p.read_uint8()  # 00
			player.PVP_flag = p.read_uint8()  # and PK Flag (0xFF)
			SpawnHandler.add_to_botlist(player.unique_id, player, SpawnList.Players)
			self.on_parsed.notify(SpawnArgs(player, SpawnList.Players))
		except SystemError as ex:
			logging.log(HANDLER, "Error parsing Character", exc_info=sys.exc_info())
			raise


class ParseNPC(AbstractSpawnParseStrategy):
	def parse(self, p: Packet, ref_id):
		object_id = p.read_uint32()
		type_npc: str = PK2DataGlobal.npcs[ref_id].long_id
		x_sec = p.read_uint8()
		y_sec = p.read_uint8()
		x_coord: float = p.read_single()
		z_coord: float = p.read_single()
		y_coord: float = p.read_single()
		p.read_uint16()  # Position
		move = p.read_uint8()  # Moving
		p.read_uint8()  # Running
		if move == 1:
			x_sec = p.read_uint8()
			y_sec = p.read_uint8()
			if y_sec == 0x80:
				x_coord = p.read_uint16() - p.read_uint16()
				p.read_uint16()
				p.read_uint16()
				y_coord = p.read_uint16() - p.read_uint16()
			else:
				x_coord = p.read_uint16()
				p.read_uint16()
				y_coord = p.read_uint16()
		else:
			p.read_uint8()  # Unknown
			p.read_uint16()  # Unknown
		p.read_uint8()  # Alive
		# p.read_uint8()
		p.read_uint8()  # Unknown
		p.read_uint8()  # Unknown
		p.read_uint8()  # Zerk Active
		p.read_single()  # Walk Speed
		p.read_single()  # Run Speed
		p.read_single()  # Zerk Speed
		p.read_uint8()  # buffs
		talk_flag = p.read_uint8()
		if talk_flag != 0:
			talk_options_count = p.read_uint8()
			for i in range(0, talk_options_count):
				p.read_uint8()
		m: Monster = Monster()
		m.unique_id = object_id
		m.ref_id = ref_id
		m.current_hp = PK2DataGlobal.npcs[ref_id].HP
		m.position = Point(x_sec, y_sec, x_coord, z_coord, y_coord)
		SpawnHandler.add_to_botlist(m.unique_id, m, SpawnList.NPCs)


# OnParsed(SpawnArgs(m, SpawnList.NPCs))


class ParseMonster(AbstractSpawnParseStrategy):
	on_parsed = Event()

	def parse(self, p: Packet, ref_id):
		m: Monster = Monster()
		m.ref_id = ref_id
		m.unique_id = p.read_uint32()  # unique ID
		x_sector = p.read_uint8()
		y_sector = p.read_uint8()
		x = p.read_single()
		z = p.read_single()  # z
		y = p.read_single()
		p.read_uint16()  # Angle
		moving = p.read_uint8()  # Moving
		p.read_uint8()  # Running
		if moving == 1:
			x_sector = p.read_uint8()
			y_sector = p.read_uint8()
			if y_sector == 0x80:
				x = p.read_uint16() - p.read_uint16()
				z = p.read_float()  # z
				y = p.read_uint16() - p.read_uint16()
			else:
				x = p.read_uint16()
				z = p.read_uint16()  # z
				y = p.read_uint16()
		else:
			p.read_uint8()  # 0: MovementSource = Spinning, 1 = Sky-/Key-walking
			p.read_uint16()  # MovementAngle
		m.is_alive = p.read_uint8() == 1  # Alive
		p.read_uint8()  # Unknownvalue.long_id
		p.read_uint8()  # Motion state 0 = None, 2 = Walking, 3 = Running, 4 = Sitting
		p.read_uint8()  # Status, Zerk Active
		p.read_single()  # Walk Speed
		p.read_single()  # Run Speed
		p.read_single()  # Zerk Speed
		buffs = p.read_uint8()  # Buffs Count On Mobs
		for i in range(0, buffs):
			buff: Buff = Buff(p.read_uint32())
			m.buffs[buff.unique_id] = buff  # Skill ID
			p.read_uint32()  # Skill Duration
			if buff.pk2_skill.is_transferable_buff():  # buff.RefSkillParam == 1701213281:
				# 1701213281 -> atfe -> "auto transfer effect" like Recovery Division
				buff.is_creator = p.read_uint8()

		p.read_uint8()  # Talk flag
		p.read_uint8()  # Rarity
		p.read_uint8()  # Appearance
		type_monster = p.read_uint8()
		m.type = MonsterType(type_monster)
		if m.is_alive:
			m.ref_id = ref_id
			# Monsters.Distance.append(dist)
			npc = PK2DataGlobal.npcs.get(m.ref_id, None)
			if npc:
				m.max_hp = npc.HP
				m.current_hp = m.max_hp
			m.position = Point(x_sector, y_sector, x, z, y)
			SpawnHandler.add_to_botlist(m.unique_id, m, SpawnList.MonsterSpawns)
		self.on_parsed.notify(SpawnArgs(m, SpawnList.MonsterSpawns))


class ParsePet(AbstractSpawnParseStrategy):
	def parse(self, p: Packet, ref_id):
		pet: Pet = Pet()
		pet.ref_id = ref_id
		pet.unique_id = p.read_uint32()  # PET ID
		x_sec = p.read_uint8()
		y_sec = p.read_uint8()
		x_coord: float = p.read_single()
		z_coord: float = p.read_single()
		y_coord: float = p.read_single()
		angle = p.read_uint16()  # Position
		pet.position = Point(x_sec, y_sec, x_coord, z_coord, y_coord)
		move = p.read_uint8()  # Moving
		p.read_uint8()  # Running
		if move == 1:
			x_sec = p.read_uint8()
			y_sec = p.read_uint8()
			if y_sec == 0x80:
				x_coord = p.read_uint16() - p.read_uint16()
				z_coord = p.read_float()
				y_coord = p.read_uint16() - p.read_uint16()
			else:
				x_coord = p.read_uint16()
				z_coord = p.read_uint16()
				y_coord = p.read_uint16()
		else:
			p.read_uint8()  # Unknown
			p.read_uint16()  # Unknown
		# p.read_uint8() spare from: c_byte old version
		pet.is_alive = p.read_uint8() == 1
		p.read_uint8()
		p.read_uint8()
		p.read_uint8()
		p.read_single()  # Walk speed
		pet.run_speed = p.read_single()  # Run speed
		p.read_single()  # Zerk Speed
		# Pets.Speed.append(speed)

		buff_count = p.read_uint8()  #
		for i in range(0, buff_count):
			buff: Buff = Buff(p.read_uint32())
			buff.buff_duration = p.read_uint32()  # Buff.Duration
			if buff.pk2_skill.is_transferable_buff():  # buff.RefSkillParam == 1701213281:
				# 1701213281 -> atfe -> "auto transfer effect" like Recovery Division
				buff.is_creator = p.read_uint8()

		talk_flag = p.read_uint8()  # Buffcount
		if talk_flag == 2:
			talk_option_count = p.read_uint8()
			talk_options: List[int] = []
			for i in range(0, talk_option_count):
				talk_options.append(p.read_uint8())
		# TODO simplify parsing and pet types
		type_pet: PK2NPCType = PK2DataGlobal.npcs[ref_id].type
		if type_pet is PK2NPCType.PetRide:
			# Do Nothing
			pass
		elif type_pet is PK2NPCType.PetPickup:
			pet.custom_name = p.read_ascii()  # Pet Name
			pet.owner_name = p.read_ascii()  # Owner Name
			p.read_uint8()  # Unknown ( Always ?? 4 )
			pet.owner_id = p.read_uint32()  # Looks like Pet Owner ID
			if pet.owner_id == Bot.char_data.unique_id:
				Bot.pickup_pet = PickupPet(pet)
				self.on_parsed.notify(SpawnArgs(pet, SpawnList.Pets))
		elif type_pet is PK2NPCType.PetGrowth:
			pet.custom_name = p.read_ascii()  # Pet Name
			pet.owner_name = p.read_ascii()  # Owner Name
			job_type = p.read_uint8()  # Unknown ( Always ?? 4 )
			if type_pet not in [PK2NPCType.PetPickup, PK2NPCType.PetPickupStart]:
				murder_flag = p.read_uint8()  # 0 = White, 1 = Purple, 2 = Red
			if pet.pk2_entry.type.value in [PK2NPCType.PetGuildCH, PK2NPCType.PetGuildEU]:
				p.read_uint32()  # owner.ref_id
			pet.owner_id = p.read_uint32()  # owner.ref_id
			# Use old data if pet is being relocated by# getting stuck
			if pet.unique_id == Bot.growth_pet.unique_id or pet.owner_id == Bot.char_data.unique_id:
				Bot.growth_pet.set_from_pet(pet)
				AutoPotions.pet_hgp_timer.start()
				pet = GrowthPet(pet)
		elif type_pet is PK2NPCType.PetTransport or type_pet is PK2NPCType.PetTransportMall:  # "COS_T" in type:
			pet.owner_name = p.read_ascii()  # Owner Name
			job_type = p.read_uint8()
			if type_pet not in [PK2NPCType.PetPickup, PK2NPCType.PetPickupStart]:
				murder_flag = p.read_uint8()  # 0 = White, 1 = Purple, 2 = Red
			if type_pet is PK2NPCType.PetGuildCH or type_pet is PK2NPCType.PetGuildEU:
				owner_ref_id = p.read_uint32()
			pet.owner_id = p.read_uint32()
			if pet.owner_id == Bot.char_data.unique_id:
				Bot.transport.set_from_pet(pet)
			pet = TransportPet(pet)
		# elif type.startswith("TRADE_COS_QUEST_TRADE"): // TODO item with type does not exist on some servers
		# {
		#    pet.OwnerName = p.read_ascii('utf_16') p.read_ascii('1200') //Owner Name
		#    p.read_uint16() //Unknown
		#    pet.owner_id = p.read_uint32() //Owner ID
		# }
		else:
			pet.owner_name = p.read_ascii('utf_16')  # p.read_ascii('1200')  # Owner Name
			p.read_uint8()
			p.read_uint8()
			p.read_uint32()
		pet.position = Point(x_sec, y_sec, x_coord, z_coord, y_coord)
		SpawnHandler.add_to_botlist(pet.unique_id, pet, SpawnList.Pets)


class ParseItem(AbstractSpawnParseStrategy):
	on_parsed = Event()

	def parse(self, p: Packet, ref_id):
		drop: ItemDrop = ItemDrop()
		drop.ref_id = ref_id
		drop.name = PK2DataGlobal.items[ref_id].name
		long_id: str = PK2DataGlobal.items[ref_id].long_id
		if drop.pk2_item.type == PK2ItemType.Gold:  # long_id.startswith("ITEM_ETC_GOLD"):
			drop.amount = p.read_uint32()  # Amount
		elif drop.pk2_item.type in [PK2ItemType.ItemTrade, PK2ItemType.ItemQuest]:  # long_id.startswith("ITEM_QSP") or long_id.startswith("ITEM_ETC_E090825") or long_id.startswith("ITEM_QNO") or long_id == "ITEM_TRADE_SPECIAL_BOX":
			drop.owner_name = p.read_ascii()  # Name
		if drop.pk2_item.is_equipment_item():  # long_id.startswith("ITEM_CH") or long_id.startswith("ITEM_EU"):
			drop.plus = p.read_uint8()  # Plus
		drop.unique_id = p.read_uint32()  # ID
		x_sector = p.read_uint8()
		y_sector = p.read_uint8()
		x: float = p.read_single()
		z: float = p.read_single()  # Z
		y: float = p.read_single()
		p.read_uint16()  # Angle
		has_owner = p.read_uint8()
		if has_owner == 1:
			drop.owner_id = p.read_uint32()  # FF FF FF FF is free for all
			# if drop.owner_id == 0xFFFFFFFF:  # Owner ID
			# 	pass  # TODO add to pending pickup list MainData.Drops.DropWaiting = True
		drop.rarity = p.read_uint8()  # Item Type -> 0 = normal , 1 = blue, 2= sox
		if p.opcode == 0x3015:
			drop_source = p.read_uint8()  # 5 = monster ?, 6 = seems to be self/char
			dropper_unique_id = p.read_uint32()
		drop.position = Point(x_sector, y_sector, x, z, y)
		SpawnHandler.add_to_botlist(drop.unique_id, drop, SpawnList.Drops)
		self.on_parsed.notify(SpawnArgs(drop, SpawnList.Drops))


class ParsePortal(AbstractSpawnParseStrategy):
	def parse(self, p: Packet, ref_id):
		portal: Portal = Portal()
		portal.ref_id = ref_id
		data: PK2Teleporter = PK2DataGlobal.teleporters.get(ref_id, None)
		if data:
			portal.PK2Data = data
		portal.unique_id = p.read_uint32()
		x_sector = p.read_uint8()  # x_sector
		y_sector = p.read_uint8()  # y_sector
		x: float = p.read_single()  # x
		z: float = p.read_single()  # z
		y: float = p.read_single()  # y
		p.read_uint16()  # angle
		p.read_uint8()  # unk0
		unk1 = p.read_uint8()  # unk1
		p.read_uint8()  # unk2
		unk3 = p.read_uint8()  # unk3
		if unk3 == 1:  # Normal teleporter
			p.read_uint32()  # unkUInt0
			p.read_uint32()  # unkUInt1
		elif unk3 == 6:  # Dimension hole
			# p.read_uint16() // OwnerNameLength
			p.read_ascii()  # OwnerName
			p.read_uint32()  # ownerunique_id
		if unk1 == 1:  # Store event zone default
			p.read_uint32()  # unkUInt2
			p.read_uint8()  # unkn4
		portal.position = Point(x_sector, y_sector, x, z, y)
		SpawnHandler.add_to_botlist(portal.unique_id, portal, SpawnList.Portals)
		self.on_parsed.notify(SpawnArgs(portal, SpawnList.Portals))


class ParseOther(AbstractSpawnParseStrategy):
	def parse(self, p: Packet, ref_id):
		long_id: str = PK2DataGlobal.npcs[ref_id].long_id
		if long_id == "INS_QUEST_TELEPORT":
			m: Monster = Monster()
			m.ref_id = ref_id
			m.unique_id = p.read_uint32()  # MOB ID
			p.read_uint8()
			p.read_uint8()
			p.read_single()
			p.read_single()
			p.read_single()
			p.read_uint16()  # Angle
			p.read_uint8()  # Unknown
			p.read_uint8()  # Unknown
			p.read_uint16()  # Unknown
			p.read_ascii()
			p.read_uint32()
			self.on_parsed.notify(SpawnArgs(m, SpawnList.none))
			SpawnHandler.add_to_botlist(m.unique_id, m, SpawnList.Portals)


# Cast Harmony Therapy circle
# FF FF FF FF                                       uint.MaxValue -> Buffcircle
# 54 00                                             unkUShort0 84
# 38 06 00 00                                       ref_id 1592
# 4C B4 AD 00                                       unique_id 11383884
# 8A 5C x_sector y_sector
# 52 87 34 41 A3 2D 9D 41 50 DC 3B 44 00 00         x 4  z 4  y 4  angle 2
# 01                                                unkn
class ParseSpecialZone(AbstractSpawnParseStrategy):
	def parse(self, p: Packet, ref_id):
		p.read_uint16()  # unknUShort
		ref_id = p.read_uint32()
		unique_id = p.read_uint32()
		x_sector = p.read_uint8()
		y_sector = p.read_uint8()
		x_offset: float = p.read_single()
		z_offset: float = p.read_single()
		y_offset: float = p.read_single()
		angle = p.read_uint16()
		if p.opcode == 0x3015:
			unkn = p.read_uint8()


class SpawnHandler(IHandler, ABC):
	"""The abstract parent class to( the GroupSpawn- and SingleSpawnHandler class.)"""
	on_spawned = Event()
	on_despawned = Event()

	@staticmethod
	def add_to_botlist(unique_id, o: object, list_number: SpawnList):
		# switch (listNumber)
		if list_number == SpawnList.none:
			return
		elif list_number == SpawnList.Drops:
			if isinstance(o, ItemDrop):
				SpawnListsGlobal.AllSpawnEntries[unique_id] = list_number
				SpawnListsGlobal.Drops[unique_id] = o
		elif list_number == SpawnList.MonsterSpawns:
			if isinstance(o, Monster):
				SpawnListsGlobal.AllSpawnEntries[unique_id] = list_number
				SpawnListsGlobal.MonsterSpawns[unique_id] = o
		elif list_number == SpawnList.NPCs:
			if isinstance(o, Monster):
				SpawnListsGlobal.AllSpawnEntries[unique_id] = list_number
				SpawnListsGlobal.NPCSpawns[unique_id] = o
		elif list_number == SpawnList.Portals:
			if isinstance(o, Portal):
				SpawnListsGlobal.AllSpawnEntries[unique_id] = list_number
				SpawnListsGlobal.Portals[unique_id] = o
		elif list_number == SpawnList.Players:
			if isinstance(o, CharData):
				SpawnListsGlobal.AllSpawnEntries[unique_id] = list_number
				SpawnListsGlobal.Players[unique_id] = o
		elif list_number == SpawnList.Pets:
			if isinstance(o, Pet):
				SpawnListsGlobal.AllSpawnEntries[unique_id] = list_number
				SpawnListsGlobal.Pets[unique_id] = o
		SpawnHandler.on_spawned.notify(unique_id, o, list_number)

	@staticmethod
	def _reduce_remaining_spawns_counter_():
		# Reduce remaining spawns/despawns in 3017 handler
		if GroupSpawnBeginHandler.remaining_spawns > 0:
			GroupSpawnBeginHandler.remaining_spawns -= 1
		else:
			GroupSpawnBeginHandler.remaining_spawns = 0

	@staticmethod
	def _spawn_(p: Packet, error_index: int):
		ref_id = p.read_uint32()
		type_spawn: str = ''
		if ref_id in PK2DataGlobal.npcs:
			type_spawn = PK2DataGlobal.npcs[ref_id].long_id
		elif ref_id in PK2DataGlobal.items:
			type_spawn = PK2DataGlobal.items[ref_id].long_id
		elif ref_id in PK2DataGlobal.teleporters:
			type_spawn = "PORTAL"  # changed from simply taking portal.long_id
		elif ref_id == 4294967295:  # uint.MaxValue)# EVENT_ZONE (Traps, Buffzones, ...:
			ParseSpecialZone().parse(p, ref_id)
			return
		else:
			sys.stdout.write(f"{format(p.opcode, '02x')} - ref_id {format(ref_id, '02x')} of spawn index {error_index} not found\n")
			logging.log(HANDLER, f"{format(p.opcode, '02x')} - ref_id {format(ref_id, '02x')} of spawn index {error_index} not found")
			logging.log(HANDLER, f"gw_local_send_buffers {str(p)}")
			for j in range(0, GroupSpawnBeginHandler.remaining_spawns):
				SpawnHandler._reduce_remaining_spawns_counter_()
			# In case of error, don't handle but reset up for next split packet
			return

		if type_spawn.startswith("ITEM_"):
			ParseItem().parse(p, ref_id)
		elif "CHAR_" in type_spawn:
			ParseChar().parse(p, ref_id)
		elif "PORTAL" in type_spawn or "_GATE" in type_spawn:  # "_GATE" too broad, can be found in normal NPCs
			ParsePortal().parse(p, ref_id)
		elif "COS_" in type_spawn:
			ParsePet().parse(p, ref_id)
		elif "MOB_" in type_spawn:
			ParseMonster().parse(p, ref_id)
		elif "NPC_" in type_spawn:
			ParseNPC().parse(p, ref_id)
		else:
			ParseOther().parse(p, ref_id)
		SpawnHandler._reduce_remaining_spawns_counter_()

	# TODO some units not despawning and cluttering list
	@staticmethod
	def _despawn_(object_id):
		list_number = SpawnListsGlobal.AllSpawnEntries.get(object_id, None)
		if list_number:
			if list_number == SpawnList.Pets and object_id == Bot.growth_pet.unique_id:
				AutoPotions.pet_hgp_timer.stop()
				AutoPotions.handle_hgp_despawn()
			SpawnListsGlobal.remove_from_bot_list(object_id, list_number)
			SpawnHandler._reduce_remaining_spawns_counter_()
			SpawnHandler.on_despawned.notify(DespawnArgs(object_id, list_number))
