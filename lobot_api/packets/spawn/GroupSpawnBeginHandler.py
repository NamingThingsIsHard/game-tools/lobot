from typing import Optional

from lobot_api.packets.IHandler import IHandler
from lobot_api.packets.IRequest import IRequest
from silkroad_security_api_1_4.Packet import Packet


class GroupSpawnBeginHandler(IHandler):
	remaining_spawns = 0
	spawn_type = 1

	@property
	def server_opcode(self) -> int: return 0x3017

	@classmethod
	def _parse_packet_(cls, p: Packet) -> Optional[IRequest]:
		GroupSpawnBeginHandler.spawn_type = p.read_uint8()
		cls.remaining_spawns = p.read_uint16()
		return None
