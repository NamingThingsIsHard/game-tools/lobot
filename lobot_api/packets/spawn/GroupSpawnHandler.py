import logging
import sys
from typing import Optional

from lobot_api.logger.Logger import HANDLER
from lobot_api.packets.IRequest import IRequest
from lobot_api.packets.spawn.GroupSpawnBeginHandler import GroupSpawnBeginHandler
from lobot_api.packets.spawn.SpawnHandler import SpawnHandler
from silkroad_security_api_1_4.Packet import Packet


#                numberOfSpawns 2
# 8D 07 00 00                                       ref_id-- > mangyang
# 58 4F 00 00                                       object_id
# A5 60                                             x_sector y_sector
# A2 3B CC 44 51 79 01 43 EC 8F 5E 44 70 D4         4bytes(x, z, y, ???) 2 bytes angle
# 01                                                movement_type 1 => walk
# 00                                                flag ???
# A5 60                                             x_sector y_sector
# 61 06                                             x
# 81 00                                             z
# 7A 03                                             y
# 01                                                flag00
# 00                                                flag01
# 00                                                flag02
# 00                                                flag03
# 00 00 00 41 ???
# 00 00 B0 41 ???
# 00 00 C8 42 ???
# 00................
# 02................                                // unkn00
# 01................
# 05................
# 00................
# Spawning npc - jangan beggar
# # 01                                                spawnflag
# 01 00                                             numberOfSpawns
# F7 07 00 00                                       ref_id = 2039
# 13 01 00 00                                       object_id(for npc list)
# A7 61                                             x_sector y_sector
# E1 42 AD 44 1C A2 82 41 B8 EE 07 44 B5 40         .B.D...A...D.@..
# 00                                                flag01 0
# 01                                                flag02 1
# 00                                                flag03 0
# B5 40                                             flag 04 1205
# 01                                                flag05 1
# 00                                                flag06 0
# 00                                                flag07 0
# 00                                                flag08 0
# 00 00 00 00                                       walkSpeed float
# 00 00 00 00                                       runSpeed float
# 00 00 C8 42                                       zerkSpeed float
# 00                                                flag12 0
# 02                                                flag13 2        // unkn00
# 01                                                flag14 1
# 02                                                flag15 2
class GroupSpawnHandler(SpawnHandler):
	@property
	def server_opcode(self) -> int: return 0x3019

	@classmethod
	def _parse_packet_(cls, p: Packet) -> Optional[IRequest]:
		spawns: int = GroupSpawnBeginHandler.remaining_spawns
		if GroupSpawnBeginHandler.spawn_type == 1:  # (spawntype == 1:  spawn
			for i in range(0, spawns):  # numberOfObjects i+=1:
				if GroupSpawnBeginHandler.remaining_spawns < 1:
					break
				SpawnHandler._spawn_(p, i)
		elif GroupSpawnBeginHandler.spawn_type == 2:  # (spawntype == 2: despawn
			for i in range(0, spawns):  # numberOfObjects i+=1:
				try:
					object_id = p.read_uint32()
					SpawnHandler._despawn_(object_id)
				except Exception as e:
					logging.log(HANDLER, f"Despawn: ", exc_info=sys.exc_info())
		return None
