import logging
import sys
from typing import Optional

from lobot_api.logger.Logger import HANDLER
from lobot_api.packets.IRequest import IRequest
from lobot_api.packets.spawn.SpawnHandler import SpawnHandler
from silkroad_security_api_1_4.Packet import Packet


class SingleDespawnHandler(SpawnHandler):

	@property
	def server_opcode(self) -> int:
		return 0x3016

	@classmethod
	def _parse_packet_(cls, p: Packet) -> Optional[IRequest]:
		try:
			object_id = p.read_uint32()
			SpawnHandler._despawn_(object_id)
			return None
		except Exception as e:
			logging.log(HANDLER, f"Despawn error: {str(p)}", exc_info=sys.exc_info())
			sys.stdout.write(f"{str(e)}\n")
			return None
