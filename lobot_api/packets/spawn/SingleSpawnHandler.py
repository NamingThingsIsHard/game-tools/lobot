from typing import Optional

from lobot_api.packets.IRequest import IRequest
from lobot_api.packets.spawn.GroupSpawnBeginHandler import GroupSpawnBeginHandler
from lobot_api.packets.spawn.SpawnHandler import SpawnHandler
from silkroad_security_api_1_4.Packet import Packet


# #   uint	4	refobj_id
#      if(ITEM_QNO)
#      {
#          ushort	2	Owner.name.Length
#          string* Owner.name
#      }
#      elif(EQUIPMENT)
#  {
#      byte	1	opt_level
#  }	
#      uint	4	unique_id
#   ushort	2	region_id
#   float	4	XOffset
#   float	4	ZOffset
#   float	4	YOffset
#   ushort	2	Angle
#  if(not ITEM_QNO)
#  {
# 	    byte	1	hasOwner
# 	    if(hasOwner)
# 	    {
# 		    uint	4	Owner.userj_id(FF FF FF FF = Me)
# 	    }
#  }
#  byte	1	isBlue
#  if(self.Opcode == 0x3015)
#  {
# 	    byte	1	Source	
## switch(Source)
# 	    {
#		elif MovementOption == 5: //Dropped by Monster
# 			    uint	4	Dropper.unique_id
# 		        break
#		elif MovementOption == 6: //Dropped by Player
# 			    uint	4	Dropper.unique_id
# 		    break
# 	    }
#  }
# spawn strong straw item (x 6348,y 906)
# # 61 24 00 00                                       item 9313
# 03 00                                             ownernameLength 3
# 48 61 7A ownerName = Haz
# 6C 9F F5 02                                       item_id
# A8 60                                             xsector ysector
# 47 50 EB 42 65 FF 14 40 EF A5 AD 44 EE 82         x,z,y coords 4bytes each, angle 2 bytes
# 01                                                has owner? 0 no 1 yes
# A9 18 00 00                                       6313
# 00                                                isBlue?
# 05                                                DropSource
# 00 00 00 00                                       dropsourceunique_id
# Drop cotton hat 
# # 17 03 00 00                                       item 791
# 03                                                ................
# 89 D5 49 0A                                       ..I.............
# A8 60                                             .`..............
# CE 13 93 43 C9 3C 0E 41 A8 EE B7 44 14 60         ...C.<.A...D.`..
# 01                                                has owner? 0 no 1 yes
# FF FF FF FF                                       Owner.userj_id (FF FF FF FF = Me)
# 01                                                isBlue?
# 06                                                DropSource (5 Monster, 6 Player)
# 88 D5 49 0A                                       dropsourceunique_id
# Monster Spawn Old Weasel
# # 90 07 00 00                                       ref_id 1936 - 1936,MOB_CH_GYO_CLON,Old Weasel
# FE 65 00 00                                       object_id
# AA 61                                             xsector ysector
# 5F C7 84 44 60 96 B0 41 00 78 AA 3F 65 3F         x,z,y coords 4bytes each, angle 2 bytes
# 01                                                movement_type 1 => walk
# 01                                                flag ???
# AA 61                                             x_sector y_sector
# 29 04                                             x
# 16 00                                             z
# BC 00                                             y
# 01                                                flag00
# 00                                                flag01
# 03                                                flag02
# 00                                                flag03
# 00 00 40 41                                       ..@A............
# 00 00 5C 42                                       ..\B............
# 00 00 C8 42                                       ...B............
# 00                                                ................
# 02                                                ................
# 01                                                ...............
# 05                                                ................
# 00                                                ...............
# 04                                                ................
class SingleSpawnHandler(SpawnHandler):
	@property
	def server_opcode(self) -> int: return 0x3015

	@classmethod
	def _parse_packet_(cls, p: Packet) -> Optional[IRequest]:
		GroupSpawnBeginHandler.remaining_spawns += 1
		super()._spawn_(p, 0)
		return None
