from typing import Optional

from lobot_api.Event import Event
from lobot_api.globals.SpawnListsGlobal import SpawnListsGlobal, SpawnList
from lobot_api.packets.IHandler import IHandler
from lobot_api.packets.IRequest import IRequest
from lobot_api.packets.npc.SpawnArgs import SpawnArgs
from lobot_api.structs.ItemDrop import ItemDrop
from silkroad_security_api_1_4.Packet import Packet


# Spawn (2F 81 5E 06)
# # 6F 1C 00 00                                       o...............
# 2F 81 5E 06                                       /.^.............
# 89 5C                                             .\..............
# B2 80 8D 44 F9 B8 F5 BF 0C 95 C7 41 D0 6F         ...D.......A.o..
# 01                                                ................
# 2E 51 00 00                                       .Q..............
# 00                                                ................
# 05                                                ................
# 2E 81 5E 06                                       ..^.............
# Free to pick
# # 2F 81 5E 06                                       /.^.............


class DropChangeOwnerHandler(IHandler):
	on_drop_owner_changed = Event()

	@property
	def server_opcode(self) -> int: return 0x304D

	@classmethod
	def _parse_packet_(cls, p: Packet) -> Optional[IRequest]:
		unique_id = p.read_uint32()
		found, spawn = SpawnListsGlobal.try_get_spawn_object(unique_id)
		if found and isinstance(spawn, ItemDrop):
			spawn.owner_id = 0xFFFFFFFF
			cls.on_drop_owner_changed.notify(SpawnArgs(spawn, SpawnList.Drops))
		return None
