from typing import Optional

from lobot_api.packets.ExchangePacket import ExchangePacket
from lobot_api.packets.IRequest import IRequest
from silkroad_security_api_1_4.Packet import Packet


class LoginCaptcha(ExchangePacket):
	#        CLIENT_GATEWAY_LOGIN_IBUV_CONFIRM_REQUEST = 0x6323
	# 2   Code: c_ushort.Length
	# *    Code: str
	Counter: int = 0

	@property
	def opcode(self) -> int: return 0x6323

	@property
	def server_opcode(self) -> int: return 0x2322

	def __init__(self, code: str = ''):
		self.code = code

	def _add_payload_(self) -> Packet:
		if LoginCaptcha.Counter > 0:
			return None
		# TODO Fix Captcha
		p: Packet = Packet(self.opcode)
		# p.write_uint16(1) - length automatically prepended by WriteAscii()
		p.write_ascii(self.code)
		LoginCaptcha.Counter += 1
		return p

	@classmethod
	def _parse_packet_(cls, p: Packet) -> Optional[IRequest]:
		# CaptchaProcessor.bytes_to_captcha(p.get_bytes())
		return None
