from ctypes import *
from enum import Enum
from threading import Timer
from typing import Optional

from lobot_api.Event import Event
from lobot_api.packets.ExchangePacket import ExchangePacket
from lobot_api.packets.IRequest import IRequest
from silkroad_security_api_1_4.Packet import Packet


class LogoutMode(Enum):  # byte
	"""Go to Process.CPSQuit"""
	Exit = 1
	"""Go to Process.CPSRestart"""
	Restart = 2


class LogoutErrorCode(Enum):  # ushort
	"""Cannot close the game during combat."""
	UIIT_MSG_LOGOUT_ERR_CANT_LOGOUT_IN_BATTLE_STATE = 0x801
	"""Cannot exit the game while teleporting."""
	UIIT_MSG_LOGOUT_ERR_CANT_LOGOUT_WHILE_TELEPORT_WORKING = 0x802


# # 01                                                ................
class Logout(ExchangePacket):
	on_packet = Event()
	timer: Timer = None

	@property
	def opcode(self) -> int:
		return 0x7005

	@property
	def server_opcode(self) -> int:
		return 0xB005

	def __init__(self, logoutOption: LogoutMode = LogoutMode.Exit):
		self.logout_mode = logoutOption

	def _add_payload_(self) -> Packet:
		p: Packet = Packet(self.opcode, True)
		p.write_uint8(self.logout_mode.value)
		return p

	@classmethod
	def _parse_packet_(cls, p: Packet) -> Optional[IRequest]:
		result = p.read_uint8()
		if result == 1:
			if not cls.timer or not cls.timer.is_alive():
				countdown = p.read_uint8()
				logoutMode = p.read_uint8()
				cls.timer = Timer(countdown, Logout.on_packet.notify)
		else:
			# error
			errorCode: c_ushort = LogoutErrorCode(p.read_uint8())

		return None
