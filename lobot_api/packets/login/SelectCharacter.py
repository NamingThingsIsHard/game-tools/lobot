from typing import Optional

from lobot_api.Bot import Bot
from lobot_api.BotStatus import BotStatus
from lobot_api.Event import Event
from lobot_api.packets.ExchangePacket import ExchangePacket
from lobot_api.packets.IRequest import IRequest
from silkroad_security_api_1_4.Packet import Packet


class SelectCharacter(ExchangePacket):
	on_packet = Event()
	"""05 00 4c 6f 62 6f 74                               ..Lobot........."""

	@property
	def opcode(self) -> int:
		return 0x7001

	@property
	def server_opcode(self) -> int:
		return 0xB001

	# on_SelectedChar = Event()

	# def _on_selected_char_(e: EventArgs) -> void:
	#    handler: EventHandler = SelectedChar
	#    if handler is not None:
	#        handler(this, e)
	def __init__(self, selected_char_name: str = ''):
		self.SelectedCharName: str = selected_char_name
		# if not selectedCharName:
		# 	raise ValueError(selectedCharName)  # nameof
		selected_char_name = selected_char_name

	def _add_payload_(self) -> Packet:
		p: Packet = Packet(self.opcode, True)
		p.write_ascii(self.SelectedCharName)
		p.lock()
		return p

	@classmethod
	def _parse_packet_(cls, p: Packet) -> Optional[IRequest]:
		result = p.read_uint8()
		if result == 0x01:
			Bot.set_status(BotStatus.Loading)
			# PartyStateMachine.Instance.Start()
			# on_selected_char(EventArgs.Empty)
			SelectCharacter.on_packet.notify([])
		elif result == 0x02:
			error_code = p.read_uint8()
		return None
