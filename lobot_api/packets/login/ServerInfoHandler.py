from typing import Optional

from lobot_api.globals.settings.LoginSettings import LoginSettings
from lobot_api.packets.IHandler import IHandler
from lobot_api.packets.IRequest import IRequest
from lobot_api.packets.login.LoginClient import LoginClient
from lobot_api.packets.login.PatchRequest import PatchRequest
from silkroad_security_api_1_4.Packet import Packet


# 0b 00 41 67 65 6e 74 53 65 72 76 65 72 00          ..AgentServer...
class ServerInfoHandler(IHandler):
	GATEWAY: str = "GatewayServer"
	AGENT: str = "AgentServer"

	@property
	def server_opcode(self) -> int:
		return 0x2001

	@classmethod
	def _parse_packet_(cls, p: Packet) -> Optional[IRequest]:
		serverName: str = p.read_ascii()
		if serverName is ServerInfoHandler.GATEWAY:
			return PatchRequest()
		elif serverName is ServerInfoHandler.AGENT:
			return LoginClient(LoginSettings.instance().username, LoginSettings.instance().password,
			                   LoginSettings.instance().session_id,
			                   LoginSettings.instance().locale)
		p.read_uint8()
		return None
