from typing import Optional

from lobot_api.globals.settings.LoginSettings import LoginSettings
from lobot_api.packets.ExchangePacket import ExchangePacket
from lobot_api.packets.IRequest import IRequest
from lobot_api.packets.login.GatewayShardListRequest import GatewayShardListRequest
from silkroad_security_api_1_4.Packet import Packet


class PatchRequest(ExchangePacket):
	SROCLIENT: str = "SR_Client"

	@property
	def opcode(self) -> int:
		return 0x6100

	@property
	def server_opcode(self) -> int:
		return 0xA100

	def _add_payload_(self) -> Packet:
		p: Packet = Packet(self.opcode, True)
		p.write_uint8(LoginSettings.instance().locale)
		p.write_ascii(PatchRequest.SROCLIENT)
		p.write_uint32(LoginSettings.instance().version)  # Version
		return p

	@classmethod
	def _parse_packet_(cls, p: Packet) -> Optional[IRequest]:
		result = p.read_uint8()
		if result == 1:
			return GatewayShardListRequest()
		else:
			return None
