from typing import Optional

from lobot_api.Event import Event
from lobot_api.packets.ExchangePacket import ExchangePacket
from lobot_api.packets.IRequest import IRequest
from lobot_api.packets.login.Logout import LogoutErrorCode
from silkroad_security_api_1_4.Packet import Packet


# # 01                                                ................
class LogoutCancel(ExchangePacket):
	on_packet = Event()

	@property
	def opcode(self) -> int: return 0x7006

	@property
	def server_opcode(self) -> int: return 0xB006

	def __init__(self, content_id, username: str, password: str, shard_id):
		self.content_id: int = content_id
		self.Username: str = username
		self.Password: str = password
		self.shard_id: int = shard_id

	def _add_payload_(self) -> Packet:
		return Packet(self.opcode, True)

	@staticmethod
	def reset_tries():
		pass

	# # 01                                                ................
	@classmethod
	def _parse_packet_(cls, p: Packet) -> Optional[IRequest]:
		result = p.read_uint8()
		if result == 2:
			# error
			errorCode: LogoutErrorCode = LogoutErrorCode(p.read_uint16())
		return None
