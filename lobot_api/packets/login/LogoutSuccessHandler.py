from typing import Optional

from lobot_api.Bot import Bot
from lobot_api.BotStatus import BotStatus
from lobot_api.Event import Event
from lobot_api.packets.IHandler import IHandler
from lobot_api.packets.IRequest import IRequest
from silkroad_security_api_1_4.Packet import Packet


class LogoutSuccessHandler(IHandler):
	on_packet = Event()

	# @staticmethod
	# def __on_disconnected__(e: EventArgs):
	#        handler: EventHandler = Disconnected
	#        if handler is not None:
	#            handler(None, e)
	@property
	def server_opcode(self) -> int: return 0x300A

	@classmethod
	def _parse_packet_(cls, p: Packet) -> Optional[IRequest]:
		Bot.set_status(BotStatus.Disconnected)
		# OnDisconnected(EventArgs.Empty)
		LogoutSuccessHandler.on_packet.notify([])
		return None
