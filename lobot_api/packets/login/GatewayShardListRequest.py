from typing import Optional

from lobot_api.Bot import Bot
from lobot_api.BotStatus import BotStatus
from lobot_api.Event import Event
from lobot_api.globals.settings.LoginSettings import LoginSettings
from lobot_api.packets.ExchangePacket import ExchangePacket
from lobot_api.packets.IRequest import IRequest
from lobot_api.packets.login.LoginGateWay import LoginGateWay
from silkroad_security_api_1_4.Packet import Packet


class GatewayShardListRequest(ExchangePacket):
	on_packet = Event()
	MAX_TRIES: int = 3
	current_tries: int = 0

	# public static EventHandler ReceivedShardList = Event()
	@property
	def opcode(self) -> int:
		return 0x6101

	@property
	def server_opcode(self) -> int:
		return 0xA101

	def _add_payload_(self) -> Packet:
		return Packet(self.opcode, True)

	@staticmethod
	def reset_tries():
		GatewayShardListRequest.current_tries = 0

	# # 01
	# 14						                    //farm.id
	# 1b 00						                    //farm.name.length = 27
	# 53 52 4f 5f 56 69 65 74 6e 61 6d 5f
	# 54 65 73 74 4c 6f 63 61 6c 20 5b 46 5d 20 30	//SRO_Vietnam_TestLocal.[F].0.
	# 00
	# 01						//hasEntries
	# 40 00
	# 08 00						//shard.name.Length
	# 54 65 72 6d 69 6e 75 73	//shard.name = Terminus
	# 1a 00						//shard.Onlinecount = 6656
	# f4 01						//shard.Capacity = 62465
	# 01						//shard.IsOperating = True
	# 14						//shard.farmId = 20
	# 00
	@classmethod
	def _parse_packet_(cls, p: Packet) -> Optional[IRequest]:
		can_login: bool = p.read_uint8() == 0x01
		if can_login:
			LoginSettings.instance().canLogin = can_login
			farm_id = p.read_uint8()
			farm_name: str = p.read_ascii()
			unkn0 = p.read_uint8()
			has_entries = p.read_uint8()
			while has_entries == 0x01:
				LoginSettings.instance().shard_id = p.read_uint16()
				shard_name: str = p.read_ascii()
				shard_online_count = p.read_uint16()
				shard_online_capacity = p.read_uint16()
				is_operating = p.read_uint8()
				shardfarm_id = p.read_uint8()
				has_entries = p.read_uint8()
		Bot.set_status(BotStatus.Login)
		GatewayShardListRequest.on_packet.notify([])
		# on_receivedshard_list(EventArgs.Empty)
		return None  # LoginSettings.instance().autoConnect

	# and LoginSettings.instance().canLogin
	# and not string.IsNullOrEmpty(LoginSettings.instance().username) and not string.IsNullOrEmpty(LoginSettings.instance().password)
	# ? try_toConnect()
	# : None
	def __tryto_connect__(self) -> IRequest:
		if GatewayShardListRequest.current_tries < GatewayShardListRequest.MAX_TRIES and not LoginSettings.instance().username and not LoginSettings.instance().password:
			GatewayShardListRequest.current_tries += 1
			return LoginGateWay(LoginSettings.instance().username, LoginSettings.instance().password, LoginSettings.instance().locale,
								LoginSettings.instance().shard_id)
		else:
			return None
	# def _on_receivedshard_list_(e: EventArgs) -> void:
	#    handler: EventHandler = ReceivedShardList
	#    if handler is not None:
	#        handler(this, e)
