from ctypes import c_byte, c_ushort
from io import BytesIO
from typing import List


class Captcha:
	"""Structure of a captcha image package# 1   byte    image.Flag
	2   image: c_ushort.remain    //in img_bytes
	2   image: c_ushort.compressed //in img_bytes
	2   image: c_ushort.uncompressed //in img_bytes
	2   image: c_ushort.width //200
	2   image: c_ushort.height //64
	* byte[] image.compressedData"""

	flag = 0
	remain: c_ushort = 0  # in img_bytes
	compressed: c_ushort = 0  # in img_bytes
	uncompressed: c_ushort = 0  # in img_bytes
	width: c_ushort = 200  # 200
	height: c_ushort = 64  # 64
	compressedData: List[c_byte] = []


"""Class for dealing with captcha information from a server packet."""
CurrentCaptcha: Captcha = None
__captchaBytes__: List[int] = []
CaptchaImage: BytesIO = None


def bytes_to_captcha(img_bytes: List[int]):
	"""
	Conversion function from a byte array to a captcha struct.

	Bytes are little endian(!) so each field is bitshifted to get the correct value.
	E.g. flag <code>fa 01</code> must be evaluated as <code>01 fa</code>
	so we shift <code>01 << 8</code> to make it <code>01 00</code> then add <code>fa</code>
	by using <code>0100 | fa </code>, ending up with <code>01fa</code>
	:param img_bytes: Bytes to be converted
	:return: Captcha struct
	"""
	test = (img_bytes[2] << 8)  # c_ushort
	CurrentCaptcha.flag = img_bytes[0]
	CurrentCaptcha.remain = 0xFFFF & ((img_bytes[2] << 8) | img_bytes[1])
	CurrentCaptcha.compressed = 0xFFFF & ((img_bytes[4] << 8) | img_bytes[3])
	CurrentCaptcha.uncompressed = 0xFFFF & ((img_bytes[6] << 8) | img_bytes[5])
	CurrentCaptcha.width = 0xFFFF & ((img_bytes[8] << 8) | img_bytes[7])
	CurrentCaptcha.height = 0xFFFF & ((img_bytes[10] << 8) | img_bytes[9])
	CurrentCaptcha.compressedData = [0x00] * (len(img_bytes) - 11)
	CurrentCaptcha.compressedData = img_bytes[11:len(img_bytes)]


def BytesToImage(img_bytes: bytes):
	"""
	Exports img_bytes gained from packet to an image file in the program's directory.
	:param img_bytes: Bytes to be transformed into image file
	:return:
	"""
	global CaptchaImage

	if img_bytes is not None:
		with BytesIO(img_bytes) as ms:
			CaptchaImage = ms.read()

		image_to_file(CaptchaImage)

		return CaptchaImage
	else:
		return None


def text_file_to_bytes(captcha_file: str) -> bytes:
	""" Imports img_bytes from a file to a byte array.
	:param captcha_file: Bytes to be transformed into image file
	:return: image bytes
	"""
	if captcha_file is not None:
		with open(captcha_file, 'b') as f:
			return f.read()
	else:
		return None


def text_file_to_image(captchaFile: str) -> bytes:
	"""
	Exports bytes from a file to bytes of an image in the program's directory.
	:param captchaFile: Bytes to be transformed into image file
	:return: Image as bytes
	"""
	global CaptchaImage

	if captchaFile is not None:
		__captchaBytes__ = open(captchaFile, 'r').read()

		with BytesIO(bytes(__captchaBytes__)) as ms:
			CaptchaImage = ms.read()
		image_to_file(CaptchaImage)

		return CaptchaImage
	else:
		return None


def image_to_file(image: bytes):
	"""
	Export image object to file.
	:param image:
	:return:
	"""
	if image is not None:
		with open('.captcha.jpeg', 'wb') as w:
			w.write(image)


def captcha_ocr(img_bytes: List[int]) -> str:
	"""
	Perform OCR on image to try and get the target string
	:param img_bytes: img_bytes">Bytes to be transformed into string
	:return: string to enter and login with
	"""
	# TODO
	return ""
