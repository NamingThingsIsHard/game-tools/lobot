from enum import Enum
from typing import Optional

from PyQt5.QtCore import QTimer

from lobot_api import ReusableTimer
from lobot_api.Bot import Bot
from lobot_api.BotStatus import BotStatus
from lobot_api.Event import Event
from lobot_api.globals.settings.LoginSettings import LoginSettings
from lobot_api.packets.ExchangePacket import ExchangePacket
from lobot_api.packets.IRequest import IRequest
from lobot_api.packets.login.SelectCharacter import SelectCharacter
from silkroad_security_api_1_4.Packet import Packet


#  1   action: c_byte
# if(action is CharacterSelectionAction.Create)
# {
# 	2   Name: c_ushort.Length
# 	*    Name: str
# 	4    refobj_id: c_uint32
# 	1   Scale: c_byte
# 	4    ref_id: c_uint32 // EQUIP_SLOT_MAIL
# 	4    ref_id: c_uint32 // EQUIP_SLOT_PANTS
# 	4    ref_id: c_uint32 // EQUIP_SLOT_BOOTS
# 	4    ref_id: c_uint32 // EQUIP_SLOT_WEAPON
# }
# elif(action is CharacterSelectionAction.Delete or
#        action is CharacterSelectionAction.CheckName or
#        action is CharacterSelectionAction.Restore)
# {
# 	2   Name: c_ushort.Length
# 	*    Name: str

class CharacterSelectionErrorCode(Enum):  # ushort
	"""A maximum of %d characters can be created."""
	UIO_MSG_ERROR_CHARACTER_OVER_3 = 0x405
	"""Select a Weapon."""
	UIO_MSG_ERROR_CHARACTER_SELECTWEAPON = 0x404
	"""Exceeded the letter limit. \n Only 12 English letters are available.	UIO_MSG_ERROR_CHARACTER_NAME_STRING = 0x40C,"""
	"""Invalid character name."""
	UIO_SMERR_NOT_ALLOWED_CHARNAME = 0x40D
	"""This ID already exists."""
	uio_msg_error__id = 0x410
	"""This user is already connected. The user may still be connected because of an error that forced the game to close. Please try: again in 5 minutes."""
	UIO_MSG_ERROR_OVERLAP = 0x411
	"""Cannot connect to the server because the server reached its capacity."""
	UIO_SMERR_MAX_USER_EXCEEDED = 0x414
	"""Failed to create_request a character. Please try: to connect again."""
	uio_smerr_inval_id_CHARGEN_INFO = 0x403
	"""Failed to create_request a character. Please try: to connect again."""
	UIO_SMERR_FAILED_TO_CREATE_CHARACTER = 0x406
	"""The server is not running.. Please try: to connect again later."""
	UIO_SMERR_CANT_FIND_GAMESERVER = 0x409
	"""The server is not running.. Please try: to connect again later."""
	UIO_SMERR_CANT_ACCESS_PARENT_SERVER = 0x40F
	"""Failed to create_request a character. Please try: to connect again."""
	UIO_SMERR_FAILED_TO_CREATE_NEW_USER = 0x412
	"""Login failed"""
	UIO_SMERR_FAILED_TO_ENTERLOBBY = 0x415
	"""Failed to connect to server."""
# UIO_MSG_ERROR_SEVER_CONNECT = 402,407,408,40A,40B,40E,413,416,417,418,


class CharacterSelectionAction(Enum):  # byte
	"""https://github.com/DummkopfOfHachtenduden/SilkroadDoc/wiki/CharacterSelectionErrorCode"""
	Create = 1
	List = 2
	Delete = 3
	CheckName = 4
	Restore = 5


class CharSelectionExecuteAction(ExchangePacket):
	on_packet = Event()
	# public static event EventHandler on_char_list_received = Event()
	char_select_timer = QTimer()

	@property
	def opcode(self) -> int:
		return 0x7007

	@property
	def server_opcode(self) -> int:
		return 0xB007

	def __init__(self, action: CharacterSelectionAction = CharacterSelectionAction.List,
	             name: str = ''):  # , country: Country, armor: PK2ItemType.GarmentBody, weapon: PK2ItemType.Blade):
		self.action = action
		self.name = name

	# if country == Country.Chinese:

	@staticmethod
	def __timer_elapsed_function__():
		char_name: str = next(
			(s for s in LoginSettings.instance().characters_available if
			 s.casefold() == LoginSettings.instance().char_name.casefold()), None)
		if Bot.get_status() is BotStatus.CharacterSelect:
			# SelectCharacter.SelectedChar += HandleSelectectedChar
			# TODO solve this circular import
			from lobot_api.connection import proxy
			proxy.joymax.send_packet(SelectCharacter(char_name).create_request())

	# Toggle flag to prevent sending multiple SelectCharacter packets after receiving multiple B007 CharSelectLists
	# Taken care of in B001
	# Bot.set_status(BotStatus.Loading)

	@staticmethod
	def __handle_selected_char__():
		pass
		# prevent sending too many char select requests after first login
		CharSelectionExecuteAction.char_select_timer.stop()

	# 1   action: c_byte
	# 1   result: c_byte
	# if(result == 0x01 and type is CharacterSelectionAction.List)
	# {
	# 	1   characterCount: c_byte
	# for character:
	# 	{
	# 		4    characterrefobj_id = p.read_uint32()
	# 		2   characterName: c_ushort.Length
	#        *    characterName: str
	# 		1   characterScale: c_byte
	# 		1   characterCurLevel: c_byte
	# 		8   u: c_ulong# set
	# 		2   characterStrength: c_ushort
	# 		2   characterIntelligence: c_ushort
	# 		2   characterStatPoint: c_ushort
	# 		4    characterCurHP: c_uint32
	# 		4    characterCurMP: c_uint32
	# 		1   bool isDeleting
	# 		if(isDeleting)
	#        {
	#        4    characterDeleteTime: c_uint32	//in Minutes
	#        }
	# 		1   guildMemberClass: c_byte
	# 		1   bool isGuildRenameRequired
	# 		if(isGuildRenameRequired)
	# 		{
	# 			2   CurGuildName: c_ushort.Length
	# 			*    CurGuildName: str
	#        }
	# 		1   academyMemberClass: c_byte
	# 		1   itemCount: c_byte
	# for item:
	# 		{
	# 			4    item: c_uint32.ref_id
	# 			1   item: c_byte.Plus
	# 		}
	# 		1   avatarItemCount: c_byte
	# for avatarItem:
	# 		{
	# 			4    avatarItem: c_uint32.ref_id
	# 			1   avatarItem: c_byte.Plus
	# 		}
	# 	}
	# }
	# elif(result == 0x02)
	# {
	# 	2   errorCode: c_ushort
	# }
	# 02                                                ................
	# 02                                                ................
	# 01                                                ................
	# 01                                                ................
	# 73 07 00 00                                       s...............
	# 05 00                                             ................
	# 4C 6F 62 6F 74                                    Lobot...........
	# 22                                                "...............
	# 06                                                ................
	# 1C 03 00 00 00 00 00 00                           ................
	# 19 00                                             ................
	# 19 00                                             ................
	# 0F 00                                             ................
	# F9 02 00 00                                       ................
	# F9 02 00 00                                       ................
	# 00                                                ................
	# 00                                                ................
	# 00                                                ................
	# 00                                                ................
	# 08                                                ................
	# 80 12 00 00                                       ................
	# 07                                                ................
	# EC 12 00 00                                       ................
	# 07                                                ................
	# C8 12 00 00                                       ................
	# 07                                                ................
	# 34 13 00 00                                       4...............
	# 07                                                ................
	# 10 13 00 00                                       ................
	# 07                                                ................
	# 58 13 00 00                                       X...............
	# 07                                                ................
	# 40 10 00 00                                       @...............
	# 07                                                ................
	# 47 0E 00 00                                       G...............
	# 00                                                ................
	# 02                                                ................
	# A0 5F 00 00                                       ._..............
	# 00                                                ................
	# E8 5A 00 00                                       .Z..............
	# 00                                                ................
	@classmethod
	def _parse_packet_(cls, p: Packet) -> Optional[IRequest]:
		action: CharacterSelectionAction = CharacterSelectionAction(p.read_uint8())
		result = p.read_uint8()
		if result == 0x01 and action is CharacterSelectionAction.List and Bot.get_status() is not BotStatus.Loading:
			# set to prevent duplication after creating character
			LoginSettings.instance().characters_available.clear()
			character_count = p.read_uint8()
			for i in range(0, character_count):
				characterrefobj_id = p.read_uint32()
				character_name: str = p.read_ascii()
				LoginSettings.instance().characters_available.append(character_name)  # Charselect List updatenot
				character_scale = p.read_uint8()
				character_cur_level = p.read_uint8()
				exp_offset = p.read_uint64()
				character_strength = p.read_uint16()
				character_intelligence = p.read_uint16()
				character_stat_point = p.read_uint16()
				character_cur_hp = p.read_uint32()
				character_cur_mp = p.read_uint32()
				is_deleting: bool = p.read_uint8() == 0x01
				if is_deleting:
					character_delete_time = p.read_uint32()  # in Minutes
				guild_member_class = p.read_uint8()
				is_guild_rename_required: bool = p.read_uint8() == 0x01
				if is_guild_rename_required:
					# ushort CurGuildNameLength = p.read_uint8()
					cur_guild_name: str = p.read_ascii()
				academy_member_class = p.read_uint8()
				item_count = p.read_uint8()
				for j in range(0, item_count):
					itemref_id = p.read_uint32()
					item_plus = p.read_uint8()
				avatar_item_count = p.read_uint8()
				for k in range(0, avatar_item_count):
					avatar_item_ref_id = p.read_uint32()
					avatar_item = p.read_uint8()
			cls.__select_char__()
		elif result == 0x02:
			error_code = p.read_uint16()
		LoginSettings.instance().canSelectChar = True
		Bot.set_status(BotStatus.CharacterSelect)
		CharSelectionExecuteAction.on_packet.notify(LoginSettings.instance().characters_available)
		# on_char_listreceived.notify(None)
		return None

	@classmethod
	def __select_char__(cls):
		char_name = ''
		if LoginSettings.instance().auto_select and Bot.get_status() is not BotStatus.Loading:
			char_name: str = next(
				(s for s in LoginSettings.instance().characters_available if
				 s.casefold() == LoginSettings.instance().char_name.casefold()), None)
		if char_name:
			cls.char_select_timer = ReusableTimer.get_q_timer(3000, cls.__timer_elapsed_function__)
			cls.char_select_timer.start()

	# def _on_char_listreceived_(e: EventArgs) -> void:
	#    handler: EventHandler = CharListReceived
	#    if handler is not None:
	#        handler(this, e)

	def _add_payload_(self) -> Packet:
		p: Packet = Packet(self.opcode, True)
		p.write_uint8(self.action)
		# if self.action == CharacterSelectionAction.Create:
		# 	p.write_ascii(self.name)
		# 	p.write_uint32(self.refobj_id)
		# 	p.write_uint8(self.scale)
		# 	p.write_uint32(self.ref_idChest)  # EQUIP_SLOT_MAIL
		# 	p.write_uint32(self.ref_idTrousers)  # EQUIP_SLOT_PANTS
		# 	p.write_uint32(self.ref_idBoots)  # EQUIP_SLOT_BOOTS
		# 	p.write_uint32(self.ref_idWeapon) # EQUIP_SLOT_WEAPON
		if self.action == CharacterSelectionAction.Delete or \
				self.action == CharacterSelectionAction.CheckName or \
				self.action == CharacterSelectionAction.Restore:
			p.write_ascii(self.name)

		return p
