from ctypes import *

from lobot_api.packets.IRequest import IRequest
from silkroad_security_api_1_4.Packet import Packet


# 16						            // Locale 0x16 -> 22 , alias Content.id (?)
# 06 00						            //username length = 6
# 6f 70 70 6f 72 74				        opport
# 0c 00						            //passwordlength = 10
# 31 32 33 34 71 77 65 72 61 73 64 66   1234qwerasdf@........
# 40 00						            //shard.id (?)
# # 01                                                 ................
# # 0b 00 41 67 65 6e 74 53 65 72 76 65 72 00          ..AgentServer...
# # 28 00 00 00 06 00 6f 70 70 6f 72 74 0c 00 31 32    (.....opport..12
# 33 34 71 77 65 72 61 73 64 66 16 00 00 00 00 00    34qwerasdf......
# 00                                                 ................
# # 01
# 14						                    //farm.id
# 1b 00						                    //farm.name.length = 27
# 53 52 4f 5f 56 69 65 74 6e 61 6d 5f    		
# 54 65 73 74 4c 6f 63 61 6c 20 5b 46 5d 20 30	//SRO_Vietnam_TestLocal.[F].0.
# 00  						  
# 01						//hasEntries
# 40 00						//shard.id
# 08 00						//shard.name.Length
# 54 65 72 6d 69 6e 75 73	//shard.name = Terminus
# 1a 00						//shard.Onlinecount = 6656
# f4 01						//shard.Capacity = 62465
# 01						//shard.IsOperating = True
# 14						//shard.farmId = 20 
# 00                                     
class LoginGateWay(IRequest):
	@property
	def opcode(self) -> int: return 0x6102

	def __init__(self, locale: c_byte, username: str, password: str, content_id: c_byte, shard_id: c_ushort):
		self.Username = username
		self.Password = password
		self.Locale = content_id
		self.shard_id = shard_id

	def _add_payload_(self) -> Packet:
		p: Packet = Packet(self.opcode, True)
		p.write_uint8(self.Locale)
		p.write_ascii(self.Username)
		p.write_ascii(self.Password)
		p.write_uint16(self.shard_id)
		return p
