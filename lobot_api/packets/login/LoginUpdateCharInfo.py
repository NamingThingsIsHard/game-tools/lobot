from lobot_api.packets.IRequest import IRequest
from silkroad_security_api_1_4.Packet import Packet


class LoginUpdateCharInfo(IRequest):
	@property
	def opcode(self) -> int: return 0x01

	def __init__(self, p: Packet):
		self.Packet = p

	def _add_payload_(self) -> Packet:
		raise NotImplementedError()
