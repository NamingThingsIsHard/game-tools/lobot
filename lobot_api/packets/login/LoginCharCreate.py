from lobot_api.packets.IRequest import IRequest
from silkroad_security_api_1_4.Packet import Packet


class LoginCharCreate(IRequest):
	@property
	def opcode(self) -> int: return 0x00

	def __init__(self, name: str, height, weight, skin, weapon):
		self.name = name
		self.height = height
		self.weight = weight
		self.Skin = skin
		self.Weapon = weapon

	def _add_payload_(self) -> Packet:
		raise NotImplementedError()
