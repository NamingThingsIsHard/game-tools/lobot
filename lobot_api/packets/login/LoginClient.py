import logging
import os
from ctypes import *
from enum import Enum
from typing import List, Tuple, Optional

from lobot_api.globals.settings.LoginSettings import LoginSettings
from lobot_api.logger.Logger import NETWORK
from lobot_api.packets.ExchangePacket import ExchangePacket
from lobot_api.packets.IRequest import IRequest
from lobot_api.packets.login.CharSelectExecuteAction import CharSelectionExecuteAction, CharacterSelectionAction
from silkroad_security_api_1_4.Packet import Packet


class LoginClientErrorCode(Enum):  # byte
	"""The server is full, please try: again later."""
	ServerIsFull = 4
	"""Cannot connect to the server because access to the current IP has exceeded its limit."""
	IPLimit = 5


# 16						            // Locale 0x16 -> 22 , alias Content.id (?)
# 06 00						            //username length = 6
# 6f 70 70 6f 72 74				        opport
# 0c 00						            //passwordlength = 10
# 31 32 33 34 71 77 65 72 61 73 64 66   1234qwerasdf@........
# 40 00						            //shard.id (?)
# # 01                                                 ................
# # 0b 00 41 67 65 6e 74 53 65 72 76 65 72 00          ..AgentServer...
# # 28 00 00 00 06 00 6f 70 70 6f 72 74 0c 00 31 32    (.....opport..12
# 33 34 71 77 65 72 61 73 64 66 16 00 00 00 00 00    34qwerasdf......
# 00                                                 ................
# # 28 00 00 00                                        session_id
# 06 00                                              NameLength 6
# 6f 70 70 6f 72 74                                  opport
# 0c 00                                              passwordlength 12
# 31 32 33 34 71 77 65 72 61 73 64 66                1234qwerasdf
# 16                                                 locale 40
# 00 00 00 00 00 00                                  mac address(empty)
class LoginClient(ExchangePacket):
	@property
	def opcode(self) -> int:
		return 0x6103

	@property
	def server_opcode(self) -> int:
		return 0xA103

	def __init__(self, username: str, password: str, session_id: c_uint32, locale: c_ushort):
		self.Username = username
		self.Password = password
		self.session_id = session_id
		self.Locale = locale

	def _add_payload_(self) -> Packet:
		p: Packet = Packet(self.opcode, True)
		p.write_uint8(self.session_id)
		p.write_ascii(self.Username)
		p.write_ascii(self.Password)
		p.write_uint16(self.Locale)
		# write random 6 octet mac address
		p.write_uint16(0x0000)  # 2 empty
		p.write_uint32(c_uint32(os.urandom(4)))  # 4 random
		return p

	#  1   result: c_byte
	#  if(result == 0x02)
	#  {
	#      1   errorCode: c_byte
	#  }
	@classmethod
	def _parse_packet_(cls, p: Packet) -> Optional[IRequest]:
		result = p.read_uint8()
		if result == 0x02:
			logging.log(NETWORK,
			            f"LoginClient Error {format(p.opcode, '02X')} - {str(p.encrypted)} - {str(p.massive)} - {' '.join([format(x, '02X') for x in p.get_bytes()])}")
		else:
			char_list: Packet = Packet(0x7007)
			char_list.write_uint8(0x02)
			# Make sure login command cannot be sent again
			LoginSettings.instance().can_login = False
			return CharSelectionExecuteAction(CharacterSelectionAction.List)
		return None

	# out Packet result
	@staticmethod
	def try_spoof_client_login_packet(p: Packet) -> Tuple[bool, Packet]:
		"""  Check login packet and spoof it with valid data if necessary before sending to Agent server.
		After injecting a packet using a proxy program, the gateway connection is closed and a connection is established with the Agent server.
		The Client does not have the username or password however, as these were not entered manually in the client but sent through the proxy instead.
		This causes the client to send an invalid 0x6103 login packet with an empty username (and password) to the agent server after 0xA102.

		:return: Tuple of whether packet was replaced or not and the resulting packet.
		"""
		if p.opcode == 0x6103:
			# Encrypted
			# 4    Token: c_uint32 //from LOGIN_RESPONSE
			# 2   Username: c_ushort.Length
			# * string  Username
			# 2   Password: c_ushort.Length
			# * string  Password
			# 1   Content: c_byte.ID
			# 6   MAC: List[c_byte]-Address
			token = p.read_uint32()
			username: str = p.read_ascii()
			password: str = p.read_ascii()
			locale = p.read_uint8()
			mac_address: List[int] = p.read_uint8_array(6)
			# Spoof packet if it is invalid
			if not username:
				result = Packet(0x6103, True)
				result.write_uint32(token)
				result.write_ascii(LoginSettings.instance().username)
				result.write_ascii(LoginSettings.instance().password)
				result.write_uint8(locale)
				result.write_uint_array(mac_address)
				result.lock()
				return True, result
		# no changes if packet is valid
		return False, p
