from enum import Enum


class HPMPUpdateType(Enum):  # byte
	hp = 0x01
	mp = 0x02
	hp_mp = 0x03
	bad_status = 0x04
	hp_bad_status = 0x05
	mp_bad_status = 0x06
	hp_mp_bad_status = 0x07
