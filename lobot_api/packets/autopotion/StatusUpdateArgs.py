from lobot_api.packets.autopotion.HPMPUpdateType import HPMPUpdateType
from lobot_api.structs.DebuffStatus import DebuffStatus


class StatusUpdateArgs:
	def __init__(self, unique_id: int, type_value: HPMPUpdateType, status: DebuffStatus = DebuffStatus.Normal,
	             status_begin_or_end: bool = False):
		self.unique_id = unique_id
		self.type = type_value
		self.status = status
		self.status_begin_or_end = status_begin_or_end
