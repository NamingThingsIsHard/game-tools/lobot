﻿from abc import ABC, abstractmethod

from silkroad_security_api_1_4.Packet import Packet


class IRequest(ABC):
	"""The unique code linked to the command.
	It determines the packet type recognized by the server."""

	@property
	@abstractmethod
	def opcode(self) -> int:
		pass

	def create_request(self) -> Packet:
		"""Create packet and add payload from information in IRequest object."""
		return self._add_payload_()

	@abstractmethod
	def _add_payload_(self) -> Packet:
		"""Add information to a packet before it is sent.
		Called by create_request.
		"""
		pass

	@classmethod
	def from_empty_constructor(cls):
		"""
		Constructor used to create request instance for opcode handling and testing.
		:return: class instance
		"""
		obj = cls.__new__(cls)  # Does not call __init__
		super(cls, obj).__init__()  # Don't forget to call any polymorphic base class initializers
		return obj
