from enum import Enum


class QuestMark(Enum):  # byte
	New = 1
	Open = 2
	Complete = 3
	Hard = 4
