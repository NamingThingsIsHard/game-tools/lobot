from typing import Optional

from lobot_api.Bot import Bot
from lobot_api.packets.IHandler import IHandler
from lobot_api.packets.IRequest import IRequest
from silkroad_security_api_1_4.Packet import Packet

# TODO add packet handlers for silk
# Update total silk
# [S -> C][3153]
# 21 00 00 00                                       33 total silk
# 00 00 00 00                                       ................
# 00 00 00 00                                       ................

# Reward silk
# [S -> C][3154]
# 00                                                ................
# 00                                                ................
# 03 00 00 00                                       add 3 silk

class StorageInfoGoldHandler(IHandler):
	# # 00 00 00 00 00 00 00 00                          u: c_ulong
	@property
	def server_opcode(self) -> int: return 0x3047

	@classmethod
	def _parse_packet_(cls, p: Packet) -> Optional[IRequest]:
		Bot.storage_gold = p.read_uint64()
		return None
