from typing import Optional

from lobot_api.Bot import Bot
from lobot_api.Event import Event
from lobot_api.packets.IHandler import IHandler
from lobot_api.packets.IRequest import IRequest
from lobot_api.packets.inventory import ItemParser
from lobot_api.structs.Item import Item
from silkroad_security_api_1_4.Packet import Packet


# begin storage info
# # 00 00 00 00 00 00 00 00                           u: c_ulong
# storage data
# # 96 02 01 00 00 00 00 05 00 00 00 01 00 02 00 00   ........
# 00 00 0B 00 00 00 04 00                           ........
# storage data parsed
# # 96                                                storage size 150
# 02                                                number of items 2
# 01                                                slot number 1
# 00 00 00 00                                       unknown uint32
# 05 00 00 00                                       ref_id 5 -> hp herb
# 01 00                                             stack amount 1
# 02                                                slot number 2
# 00 00 00 00                                       unknown uint32
# 0B 00 00 00                                       ref_id 11 -> mp herb
# 04 00                                             stack amount 4


class StorageInfoDataHandler(IHandler):
	on_storage_parsed = Event()

	@property
	def server_opcode(self) -> int:
		return 0x3049

	@classmethod
	def _parse_packet_(cls, p: Packet) -> Optional[IRequest]:
		Bot.storage.clear()
		Bot.storage_size = p.read_uint8()
		number_of_items = p.read_uint8()
		for i in range(0, number_of_items):
			item: Item = cls._parse_new_item_(p)
			if item is not None:
				Bot.storage[item.slot] = item
		cls.on_storage_parsed.notify()
		return None

	@classmethod
	def _parse_new_item_(cls, p: Packet) -> Item:
		item: Item = ItemParser.parse_item(p)
		return item
