from typing import Optional

from lobot_api.packets.IHandler import IHandler
from lobot_api.packets.IRequest import IRequest
from silkroad_security_api_1_4.Packet import Packet


class StorageInfoEndHandler(IHandler):
	@property
	def server_opcode(self) -> int: return 0x3048

	@classmethod
	def _parse_packet_(cls, p: Packet) -> Optional[IRequest]:
		# no info on packet so far
		return None
