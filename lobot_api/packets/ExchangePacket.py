from abc import abstractmethod

from lobot_api.packets.IHandler import IHandler
from lobot_api.packets.IRequest import IRequest


class ExchangePacket(IRequest, IHandler):
	"""class of( IRequest and IHandler.):
	This abstraction is used for pairs of requests and handlers that send and receive packets.
	"""

	@property
	@abstractmethod
	def server_opcode(self) -> int:
		pass

	@property
	@abstractmethod
	def opcode(self) -> int:
		pass
