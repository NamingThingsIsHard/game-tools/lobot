from typing import Optional

from lobot_api.Bot import Bot
from lobot_api.Event import Event
from lobot_api.packets.ExchangePacket import ExchangePacket
from lobot_api.packets.IRequest import IRequest
from lobot_api.packets.alchemy.AlchemyAction import AlchemyAction
from lobot_api.packets.alchemy.AlchemyArgs import AlchemyArgs
from lobot_api.packets.alchemy.AlchemyType import AlchemyType
from lobot_api.structs.Item import Equipment, Item, MagParam
from lobot_api.structs.WhiteAttributes import WhiteAttributes
from silkroad_security_api_1_4.Packet import Packet


# Hero Steel Shield lvl 29 enhance, no lucky powder fail
# # 02                                                action = 2 fuse
# 03                                                type = 3 elixir
# 02                                                factors 2
# 32                                                item_slot = 50
# 34                                                elixir_slot = 52
# # 01                                                ................
# 02                                                ................
# 00                                                ................
# 32                                                2...............
# 00                                                ................
# 00 00 00 00                                       ................
# 06 01 00 00                                       ................
# 00                                                ................
# 49 19 31 14 00 00 00 00                           I.1.............
# 63 00 00 00                                       c...............
# 03                                                ................
# 50 00 00 00 3C 00 00 00                           P...<...........
# 35 00 00 00 03 00 00 00                           5...............
# 74 00 00 00 03 00 00 00                           t...............
# 01                                                ................
# 00                                                ................
# 02                                                ................
# 00                                                ................
# # 01                                                ................
# 0F                                                ................
# 34                                                4...............
# 04                                                ................
# Hero Steel Shield lvl 29 enhance, lucky powder, success +1
# # 02                                                action = 2 fuse
# 03                                                type = elixir
# 03                                                factors
# 32                                                slotItem = 50
# 35                                                slotElixir = 53
# 34                                                slotLuckyPowder = 52 
# # 01                                                reinforceFinished
# 02                                                type 2 fuse
# 01                                                success 1
# 32                                                itemslot = 50
# 00 00 00 00                                       owner? self
# 06 01 00 00                                       ref_id 262 hero steel shield
# 01                                                opt = 1 (success)
# 49 19 31 14 00 00 00 00                           attributes
# 63 00 00 00                                       durability 99
# 03                                                number of blues
# 50 00 00 00 3C 00 00 00                           durability 60%
# 35 00 00 00 03 00 00 00                           critical? 3%
# 74 00 00 00 03 00 00 00                           steady? 3%
# 01                                                opt_type = 1 socket
# 00                                                count: opt = 0
# 02                                                opttype = 2 advanced elixir
# 00                                                optcount = 0
# Remove item
# # 01                                                success = 1 
# 0F                                                itemmove_type = 15 remove
# 34                                                slotItemRemove = 52 (lucky powder)
# 04                                                remove_type? = 4 (alchemy - no lucky powder left)


class AlchemyReinforce(ExchangePacket):
	on_reinforce_parsed = Event()

	def __init__(self, action: AlchemyAction, type_value: AlchemyType, item_slot, elixir_slot, lucky_powder_slot=0):
		self.action = action
		self.alchemy_type = type_value
		self.item_slot = item_slot
		self.elixir_slot = elixir_slot
		self.LuckyPowderSlot = lucky_powder_slot

	@property
	def opcode(self) -> int:
		return 0x7150

	@property
	def server_opcode(self) -> int:
		return 0xB150

	def _add_payload_(self) -> Packet:
		p: Packet = Packet(self.opcode)
		p.write_uint8(self.action)
		p.write_uint8(self.alchemy_type)
		p.write_uint8(self.item_slot)
		p.write_uint8(self.elixir_slot)
		if self.LuckyPowderSlot > 0:
			p.write_uint8(self.LuckyPowderSlot)
		return p

	@classmethod
	def _parse_packet_(cls, p: Packet) -> Optional[IRequest]:
		reinforce_finished: bool = p.read_uint8() == 1
		alchemy_type: AlchemyType = AlchemyType(p.read_uint8())
		success: bool = p.read_uint8() == 1
		e: Equipment = Equipment(Item())
		e.slot = p.read_uint8()
		owner_id = p.read_uint32()  # 0 is self?
		e.ref_id = p.read_uint32()
		e.opt_level = p.read_uint8()
		e.attributes = WhiteAttributes(e.get_attribute_type(), p.read_uint64())
		e.durability = p.read_uint32()
		e.mag_param_num = p.read_uint8()
		for j in range(0, e.mag_param_num):
			e.blues.append(MagParam(p.read_uint32(), p.read_uint32()))
		opt_type_1 = p.read_uint8()  # (1 => Socket)
		opt_count_1 = p.read_uint8()
		for i in range(0, opt_count_1):
			e.item_option.slot = p.read_uint8()
			e.item_option.ID = p.read_uint32()
			e.item_option.NParam1 = p.read_uint32()  # (=> Reference to Socket)
		opt_type_2 = p.read_uint8()  # (2 => Advanced elixir)
		opt_count_2 = p.read_uint8()
		for i in range(0, opt_count_2):
			e.item_option.slot = p.read_uint8()
			e.item_option.ID = p.read_uint32()
			e.item_option.OptValue = p.read_uint32()  # (=> "Advanced elixir in effect [+OptValue]")
		Bot.char_data.inventory.items[e.slot] = e
		cls.on_reinforce_parsed.notify(AlchemyArgs(success, e.opt_level, e.alchemy_settings_string))
		return None
