from enum import Enum


class AlchemyType(Enum):
	Disjoin = 1
	Manufacture = 2
	Elixir = 3
	MagicStone = 4
	AttributeStone = 5
	AdvancedElixir = 8
	Socket = 9
