class AlchemyArgs:
	def __init__(self, success: bool, opt_level, item_string: str):
		self.success: bool = success
		self.opt_level = opt_level
		self.item_string: str = item_string
