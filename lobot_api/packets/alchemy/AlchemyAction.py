from enum import Enum


class AlchemyAction(Enum):
	Cancel = 1
	Fuse = 2
	Create = 3  # for Socket
	Remove = 4  # for Socket
