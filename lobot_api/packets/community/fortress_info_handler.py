import logging
from typing import Optional

from lobot_api.globals.settings.LoginSettings import LoginSettings
from lobot_api.logger.Logger import HANDLER
from lobot_api.packets.IHandler import IHandler
from lobot_api.packets.IRequest import IRequest
from silkroad_security_api_1_4.Packet import Packet


# Fortress messages

# Jangan(large fortress)
# occupying guild: Ulcus
# Guild master: BLink
# Guild message: Zartbitter.Dinkel.Knusperwaffeln.200g.für.3,29€.

# Bandit Fortress
# occupying guild:
# Guild master:
# Guild message:

# Hotan Fortress(Large)
# occupying guild:
# Guild master:
# Guild message:


# [S -> C] [385F]
# 00												unkn
# 03												number of fortresses (?)
# 01 00 00 00									   fortressID jangan
# 05 00											 name length
# 55 6C 63 75 73									Ulcus...........
# DF 09 00 00									   unkn (guild number?)
# 05 00											 guildmaster length
# 42 6C 69 6E 6B									Blink...........
# 31 00											 guild message length
# 5A 61 72 74 62 69 74 74 65 72 20 44 69 6E 6B 65   Zartbitter.Dinke
# 6C 20 4B 6E 75 73 70 65 72 77 61 66 66 65 6C 6E   l.Knusperwaffeln
# 20 32 30 30 67 20 66 FC 72 20 33 2C 32 39 80 20   .200g.f.r.3,29..
# 20												unkn 32
# 03 00 00 00									   3
# 50 00 00 00									   item price 80 (100 - 20% taxes)
# 01 00 00 00									   1
# 00												unkn
# 00												unkn
# 03 00 00 00									   fortressID bandit
# 00 00											 name length 0
# 00 00 00 00									   guild number
# 00 00											 guildmaster name length 0
# 00 00											 guild message length 0
# 00 00 00 00									   unkn
# 00 00 00 00									   item price percent
# 00 00 00 00									   unkn
# 00												unkn
# 00												unkn
# 06 00 00 00									   fortressID hotan
# 00 00											 name length
# 00 00 00 00									   guild number
# 00 00											 guild master length
# 00 00											 guild message length
# 00 00 00 00									   unkn
# 00 00 00 00									   item price percent
# 00 00 00 00									   unkn
# 00												unkn
# 00												unkn
# 02												unkn
# 00 00 00 00									   ................

#  Unknown status
# [S -> C] [385F]
# 04												unkn End of War?


class FortressInfoHandler(IHandler):

	@property
	def server_opcode(self) -> int:
		return 0x385F

	@classmethod
	def _parse_packet_(cls, p: Packet) -> Optional[IRequest]:
		try:
			unkn = p.read_uint8()

			if unkn != 0:  # Anything other than 0 can cause crash
				return None

			number_of_fortresses = p.read_uint8()

			for i in range(0, number_of_fortresses):
				fortress_id = p.read_uint32()

				guild_name = p.read_ascii()
				guild_number = p.read_uint32()
				guild_master_name = p.read_ascii()
				guild_message = p.read_ascii()

				unkn2 = p.read_uint32()
				item_price_percentage = p.read_uint32()
				unkn3 = p.read_uint32()

				if LoginSettings.instance().version > 228:
					unkn4 = p.read_uint8()
					unkn5 = p.read_uint8()
				elif i + 1 < number_of_fortresses:
					unkn4 = p.read_uint8()
					unkn5 = p.read_uint32()
					unkn6 = p.read_uint8()
					unkn7 = p.read_uint32()

		except Exception as e:
			logging.log(HANDLER, f"{e}")

		return None
