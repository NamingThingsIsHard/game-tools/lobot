﻿from lobot_api.BotStatus import BotStatus
from lobot_api.Event import Event
from lobot_api.structs import GrowthPet
from lobot_api.structs import Pet
from lobot_api.structs.CharData import CharData
from lobot_api.structs.CharInfo import CharInfo
from lobot_api.structs.ItemMallInfo import ItemMallInfo
from lobot_api.structs.PickupPet import PickupPet
from lobot_api.structs.TransportPet import TransportPet


class Bot:
	""" The bot instance used to contain all information about character.
	"""
	on_hp_percent = Event()
	on_mp_percent = Event()

	on_party_changed = Event()
	on_bot_status_changed = Event()
	on_is_interacting_changed = Event()

	auto_attack_skills = {}  #: Dict[PK2ItemType, Skill] for quick check by item type
	auto_attack_skills_ids = {}  #: Dict[int, PK2ItemType] for quick lookup in skill casts

	selected_npc = None  # = ISpawnType()
	_status = BotStatus.Disconnected  # BotStatus

	@staticmethod
	def get_status():
		return Bot._status

	@staticmethod
	def set_status(value):
		if Bot._status != value:
			Bot._status = value
			Bot.on_bot_status_changed.notify()

	party_current = None  # PartyEntry
	party_member_id = 0  # c_uint32

	pet_inventory_size = 0
	pet_inventory = {}  #: Dict[c_byte, Item]
	storage_size = 0
	storage_gold = 0
	storage = {}  #: Dict[c_byte, Item]
	guild_storage_size = 0
	guild_storage = {}  #: Dict[c_byte, Item]

	party_matching_list = []  #: PartyEntry
	buy_back_list = []  # List: Item

	is_interacting = False
	char_info = CharInfo()
	char_data = CharData()
	item_mall_info: ItemMallInfo = None
	pickup_pet = PickupPet()
	growth_pet = GrowthPet.GrowthPet()
	transport: TransportPet = TransportPet(Pet.Pet())

	@staticmethod
	def reset():
		Bot.auto_attack_skills = {}  #: Dict[PK2ItemType, Skill] for quick check by item type
		Bot.auto_attack_skills_ids = {}  #: Dict[int, PK2ItemType] for quick lookup in skill casts

		Bot.selected_npc = None  # = ISpawnType()
		Bot._status = BotStatus.Disconnected  # BotStatus

		Bot.party_current = None  # PartyEntry
		Bot.party_member_id = 0  # c_uint32

		Bot.pet_inventory_size = 0
		Bot.pet_inventory = {}  #: Dict[c_byte, Item]
		Bot.storage_size = 0
		Bot.storage = {}  #: Dict[c_byte, Item]
		Bot.guild_storage_size = 0
		Bot.guild_storage = {}  #: Dict[c_byte, Item]

		Bot.party_matching_list = []  #: PartyEntry
		Bot.buy_back_list = []  # List: Item

		Bot.is_interacting = False
		Bot.char_info = CharInfo()
		Bot.char_data = CharData()
		Bot.item_mall_info = None
		Bot.pickup_pet = PickupPet()
		Bot.growth_pet = GrowthPet.GrowthPet()
		Bot.transport = TransportPet(Pet.Pet())

	@property
	def current_hp_percent(self) -> int:
		result = (100
		          if self.char_data.current_hp == 0 or self.char_info.max_hp == 0
		          else (self.char_data.current_hp * 100) / self.char_info.max_hp)
		return result

	@property
	def current_mp_percent(self) -> int:
		result = (100
		          if self.char_data.current_mp == 0 or Bot.char_info.max_mp == 0
		          else (self.char_data.current_mp * 100) / Bot.char_info.max_mp)
		return result

	@staticmethod
	def character_inventory_is_full() -> bool:
		count: int = len(Bot.char_data.inventory.items)
		return count == (Bot.char_data.inventory.size - 13)

	@staticmethod  # @property
	def guild_info() -> str:
		return Bot.char_data.guild_name if Bot.char_data.guild_grant_name else Bot.char_data.guild_name + " - " + Bot.char_data.guild_grant_name

	@staticmethod  # @property
	def name_full() -> str:
		return f"{Bot.char_data.name} (Lvl {Bot.level_by_equipment()})" + (
			"" if Bot.guild_info() == '' else f"[{Bot.guild_info()}]")  # Property for UI WPF purposes

	@staticmethod
	def level_by_equipment():
		max_item_level: int = max(
			value.level for key, value in
			Bot.char_data.inventory.items.items()) if Bot.char_data.inventory.item_count > 0 else 0
		return 0xFF & max(Bot.char_data.level, max_item_level)
