from threading import Timer
from typing import Optional

from PyQt5.QtCore import QTimer


def get_q_timer(interval_msec=0, function=None) -> QTimer:
	"""
	Use as one-liner constructor for QTimer
	:param interval_msec: interval to set in milliseconds
	:param function: callback to set
	:return: QTimer object
	"""
	timer = QTimer()
	if interval_msec > 0:
		timer.setInterval(interval_msec)
	if function:
		timer.timeout.connect(function)
	return timer


class ReusableTimer(object):
	"""
	Timer object for repetitive tasks.
	"""

	def __init__(self, interval, function=None, repeat=False, args=None, kwargs=None):
		"""
		:param interval: Timer interval. Runs at least once. Runs multiple times if repeat==True.
		:param function: Function to be run everytime the interval elapses.
		:param repeat: Should interval repeat indefinitely.
		:param args: Function arguments.
		:param kwargs: Function keyword arguments.
		"""
		if not args:
			args = []
		if not kwargs:
			kwargs = {}

		self._interval = interval
		self._function = function
		self._args = args
		self._kwargs = kwargs
		self.repeat = repeat
		self.is_ready = True
		self._thread_: Optional[Timer] = None

	@property
	def interval(self):
		return self._interval

	@interval.setter
	def interval(self, value):
		if self._interval != value:
			self._interval = value

	def reset(self):
		"""
		Function run after the timer elapses.
		If timer is set to repeat, start function is called until cancelled.
		:return: None
		"""
		self.is_ready = True
		if self._function:
			self._function(*self._args, **self._kwargs)
		if self.repeat:
			self.start()

	def cancel(self):
		if self._thread_:
			self._thread_.cancel()
			self.is_ready = True

	def start(self):
		if self.is_ready:
			self._thread_ = Timer(self._interval, self.reset, [], {})
			self.is_ready = False
			self._thread_.start()
