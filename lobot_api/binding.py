from typing import Callable

from PyQt5.QtCore import pyqtSignal, pyqtSlot, QObject


class BindingType:
	"""
	Source: Variable from Controller/Model.
	Target: UI element getting updated.
	For example, if you want to bind the content of a TextBox to the Employee.name property,
	your target object is the TextBox,
	the target property is the Text property,
	the value to use is Name,
	and the source object is the Employee object.
	"""
	oneway = 0
	onewayToSource = 1
	twoway = 2


#
# def bind(binding_group: List[Tuple[pyqtSignal, Callable, BindingType]]):
# 	"""
# 	Connect a signals to a slots/functions.
# 	:param binding_group: signals, the matching slots and how to bind them
# 	:return:
# 	"""
# 	for signal, func, type in binding_group:
# 		#
# 		if type != BindingType.onewayToSource:
# 			signal.connect(func)
# 		if type != BindingType.oneway:

class LobotProperty(QObject):
	"""
	Implementation of pyqtProperty to encapsulate a property and a signal.
	The signal is emitted on changes.
	"""
	signal = pyqtSignal([str], [bool], [int])

	def __init__(self, getter: Callable, getter_args, setter: Callable, setter_args):
		super(LobotProperty, self).__init__()
		self._getter = getter
		self._getter_args = getter_args
		self._setter = setter
		self._setter_args = setter_args

	@property
	def value(self):
		return self._getter(*self._getter_args)

	@value.setter
	def value(self, value):
		if self._getter(*self._getter_args) != value:
			self._setter(*self._setter_args, str(value))
			if isinstance(value, bool):
				self.signal[bool].emit(value)
			elif isinstance(value, int):
				self.signal[int].emit(value)
			else:
				self.signal.emit(value)

	@pyqtSlot(str)
	@pyqtSlot(int)
	@pyqtSlot(bool)
	def slot(self, value):
		self.value = value

	def __repr__(self):
		return str(self.value) if not isinstance(self.value, str) else self.value

	def __str__(self):
		return str(self.value) if not isinstance(self.value, str) else self.value

# class Binding:
# 	"""Class to encapsulate a binding between a QObject and a variable.
# 	If one-way, the QObject slot or signal is used and the variable acts as the counterpart.
# 	If two-way, the QObject slot and signal are used and a signal and slot are added for the variable."""
#
# 	def __init__(self, type: BindingType, qobject: QObject, lobot_property: LobotProperty, q_signal_func: Callable = None):
# 		if not qobject or not lobot_property:
# 			raise ValueError(f"Binding needs both a source and a target: qobject:{qobject}, variable:{lobot_property}")
# 		self.target = qobject
# 		self.source = lobot_property
#
# 		if type != BindingType.onewayToSource:
# 			self.source.signal.connect()
# 		elif type == BindingType.onewayToSource:
# 		else:
#
# 		self.variable_signal =
#
# 		self.variable_setter_function = var_func
#
# 	@property
# 	def qobject(self):
# 		return self._qobj
#
# 	@property
# 	def variable(self):
# 		return self._variable
#
# 	@variable.setter
# 	def variable(self, value):
# 		if self._variable != value:
# 			self._variable = value
# 			self
