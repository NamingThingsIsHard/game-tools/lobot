import logging
import os
import sys
from datetime import datetime
from pathlib import Path

from lobot_api.globals.DirectoryGlobal import DirectoryGlobal
from silkroad_security_api_1_4.Packet import Packet

SETUP = 100
PK2 = 101

NETWORK = 200
HANDLER = 201
LOGIN = 202
SPAWN = 203

BOTTING = 300
TRAINING = 301
SCRIPT = 302
TOWN = 303
TELEPORT = 310
# ALL = 500

time = datetime.now()
LOG_FILENAME = Path(DirectoryGlobal.log_folder()) / f"{time.year}-{time.month}-{time.day}_{time.hour}-{time.minute}"
FORMAT = '%(asctime)s - %(levelname)s - %(message)s'

# Make sure path exists to create_request logfile in
if not os.path.exists(DirectoryGlobal.log_folder()):
	os.makedirs(DirectoryGlobal.log_folder(), exist_ok=True)
logging.basicConfig(filename=LOG_FILENAME, format=FORMAT)
# Output all logged statements to stdout
logging.getLogger().addHandler(logging.StreamHandler(sys.stdout))

logging.addLevelName(SETUP, "SETUP")
logging.addLevelName(PK2, "PK2")
logging.addLevelName(NETWORK, "NETWORK")
logging.addLevelName(HANDLER, "HANDLER")
logging.addLevelName(LOGIN, "LOGIN")
logging.addLevelName(SPAWN, "SPAWN")
logging.addLevelName(BOTTING, "BOTTING")
logging.addLevelName(TRAINING, "TRAINING")
logging.addLevelName(SCRIPT, "SCRIPT")
logging.addLevelName(TOWN, "TOWN")
logging.addLevelName(TELEPORT, "TELEPORT")


def setup(self, message, *args, **kws):
	if self.isEnabledFor(SETUP):
		# Yes, logger takes its '*args' as 'args'.
		self._log(SETUP, message, args, **kws)


logging.Logger.setup = setup


def pk2(self, message, *args, **kws):
	if self.isEnabledFor(PK2):
		self._log(PK2, message, args, **kws)


logging.Logger.pk2 = pk2


def network(self, message, *args, **kws):
	if self.isEnabledFor(NETWORK):
		self._log(NETWORK, message, args, **kws)


logging.Logger.network = network


def handler(self, message, *args, **kws):
	if self.isEnabledFor(HANDLER):
		self._log(HANDLER, message, args, **kws)


logging.Logger.handler = handler


def login(self, message, *args, **kws):
	if self.isEnabledFor(LOGIN):
		self._log(LOGIN, message, args, **kws)


logging.Logger.login = login


def spawn(self, message, *args, **kws):
	if self.isEnabledFor(SPAWN):
		self._log(SPAWN, message, args, **kws)


logging.Logger.spawn = spawn


def botting(self, message, *args, **kws):
	if self.isEnabledFor(BOTTING):
		self._log(BOTTING, message, args, **kws)


logging.Logger.botting = botting


def training(self, message, *args, **kws):
	if self.isEnabledFor(TRAINING):
		self._log(TRAINING, message, args, **kws)


logging.Logger.training = training


def script(self, message, *args, **kws):
	if self.isEnabledFor(SCRIPT):
		self._log(SCRIPT, message, args, **kws)


logging.Logger.script = script


def town(self, message, *args, **kws):
	if self.isEnabledFor(TOWN):
		self._log(TOWN, message, args, **kws)


logging.Logger.town = town


def teleport(self, message, *args, **kws):
	if self.isEnabledFor(TELEPORT):
		self._log(TELEPORT, message, args, **kws)


logging.Logger.TELEPORT = TELEPORT


def clean_up_empty_files():
	"""
	Remove empty log files to remove clutter.
	:return: None
	"""
	for filename in os.listdir(DirectoryGlobal.log_folder()):
		path = Path(DirectoryGlobal.log_folder()) / filename
		if os.stat(path).st_size < 1:
			os.remove(path)


def proxy_output(level: int, msg: str, p: Packet):
	# Skip if not active
	# if (LogLevelActiveStatus != None && (LogLevelActiveStatus.try_getValue(level, out bool isValid) && isValid)):
	# 	sys.stdout.write(f"{identifier}: {p.Opcode.str("X4")} Data: {BitConverter.str(p.get_bytes()).Replace("-", " ")}")
	logging.log(level, msg, str(p))
