class Event:
	"""The observer pattern implementation for events.
	An instance of this is equivalent to the C# EventHandler.
	"""

	def __init__(self):
		self.listeners = []

	def __iadd__(self, listener):
		"""Shortcut for using += to add a listener."""
		self.listeners.append(listener)
		return self

	def __isub__(self, other):
		"""Shortcut for using -= to remove a listener"""
		if other in self.listeners:
			self.listeners.remove(other)
		return self

	def notify(self, *args, **kwargs):
		"""Call the listeners upon event.
		Pass function to QThreadPool to prevent GUI freezing.
		"""
		from controller.controller import Controller

		def call_func(func):
			func(*args, **kwargs)

		for listener in self.listeners:
			call_func(listener)
