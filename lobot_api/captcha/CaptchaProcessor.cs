﻿using System;
using System.Drawing;
using System.IO;

namespace LobotAPI.Captcha
{
    /// <summary>
    /// Class for dealing with captcha information from a server packet.
    /// Uses the <see href="http://msdn.microsoft.com/en-us/library/windows/apps/xaml/windowspreview.media.ocr.aspx">Microsoft OCR library</see>.
    /// </summary>
    public static class CaptchaProcessor
    {
        public static Captcha CurrentCaptcha;
        private static byte[] captchaBytes;
        static Image CaptchaImage;

        /// <summary>
        /// Conversion function from a byte array to a captcha struct.
        /// 
        /// Bytes are little endian(!) so each field is bitshifted to get the correct value.
        /// E.g. flag <code>fa 01</code> must be evaluated as <code>01 fa</code>
        /// so we shift <code>01 << 8</code> to make it <code>01 00</code> then add <code>fa</code>
        /// by using <code>0100 | fa </code>, ending up with <code>01fa</code>
        /// </summary>
        /// <param name="bytes">Bytes to be converted</param>
        public static void BytesToCaptcha(byte[] bytes)
        {
            ushort test = (ushort)(bytes[2] << 8);
            CurrentCaptcha.flag = bytes[0];
            CurrentCaptcha.remain = (ushort)((bytes[2] << 8) | bytes[1]);
            CurrentCaptcha.compressed = (ushort)((bytes[4] << 8) | bytes[3]);
            CurrentCaptcha.uncompressed = (ushort)((bytes[6] << 8) | bytes[5]);
            CurrentCaptcha.width = (ushort)((bytes[8] << 8) | bytes[7]);
            CurrentCaptcha.height = (ushort)((bytes[10] << 8) | bytes[9]);
            CurrentCaptcha.compressedData = new byte[bytes.Length - 11];
            Array.Copy(bytes, 11, CurrentCaptcha.compressedData, 0, bytes.Length - 11);
        }



        /// <summary>
        /// Exports bytes gained from packet to an image file in the program's directory.
        /// </summary>
        /// <param name="bytes">Bytes to be transformed into image file</param>
        public static Image BytesToImage(byte[] bytes)
        {
            if (bytes != null)
            {
                using (var ms = new MemoryStream(bytes))
                {
                    CaptchaImage = Image.FromStream(ms);
                }

                ImageToFile(CaptchaImage);

                return CaptchaImage;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Imports bytes from a file to a byte array.
        /// </summary>
        /// <param name="bytes">Bytes to be transformed into image file</param>
        public static byte[] TextFileToBytes(String captchaFile)
        {
            if (captchaFile != null)
            {
                return File.ReadAllBytes(captchaFile);
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Exports bytes from a file to an image file in the program's directory.
        /// </summary>
        /// <param name="bytes">Bytes to be transformed into image file</param>
        public static Image TextFileToImage(String captchaFile)
        {
            if (captchaFile != null)
            {
                captchaBytes = File.ReadAllBytes(captchaFile);

                using (var ms = new MemoryStream(captchaBytes))
                {
                    CaptchaImage = Image.FromStream(ms);
                }

                ImageToFile(CaptchaImage);

                return CaptchaImage;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Export image object to file.
        /// </summary>
        /// <param name="image"></param>
        public static void ImageToFile(Image image)
        {
            if (image != null)
            {
                image.Save(".\\captcha.jpeg");
            }
        }

        /// <summary>
        /// Perform OCR on image to try and get the target string
        /// </summary>
        /// <param name="bytes">Bytes to be transformed into string</param>
        /// <returns>String to enter and login with</returns>
        public static string CaptchaOCR(byte[] bytes)
        {
            //TODO
            return "";
        }
    }
}
