from enum import Enum


class PartyChangeType(Enum):
	Empty = 1
	Update = 2
	Full = 3


class PartyChangedArgs:
	def __init__(self, p_type: PartyChangeType):
		self.type = p_type
