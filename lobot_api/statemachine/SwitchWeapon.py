import sys

from lobot_api.Bot import Bot
from lobot_api.connection import proxy
from lobot_api.packets.inventory.ItemMoveArgs import ItemMoveArgs
from lobot_api.packets.inventory.ItemMovementType import ItemMovementType
from lobot_api.packets.inventory.MoveInventoryToInventory import MoveInventoryToInventory
from lobot_api.packets.inventory.MoveItem import MoveItem
from lobot_api.pk2.PK2Item import Country, PK2ItemType
from lobot_api.statemachine.IState import IState, IStateType
from lobot_api.structs.EquipmentSlot import EquipmentSlot
from lobot_api.structs.Item import Equipment


class SwitchWeapon(IState):
	def _get_next_target_(self):
		raise NotImplementedError()

	def _loop_inner_(self):
		raise NotImplementedError()

	def _initialize_event_handlers_(self):
		MoveInventoryToInventory.on_item_switched += self._handle_item_switch_
		MoveInventoryToInventory.on_item_moved += self._handle_item_move_

	def _remove_event_handlers_(self):
		MoveInventoryToInventory.on_item_switched -= self._handle_item_switch_
		MoveInventoryToInventory.on_item_moved -= self._handle_item_move_

	@staticmethod
	def try_switch_weapons() -> bool:
		if not IState.weapon_to_equip:
			return False
		else:
			to_slot = IState.weapon_to_equip.type.get_required_weapon_slot()
			proxy.joymax.send_packet(
				MoveItem(ItemMovementType.InventoryToInventory, IState.weapon_to_equip.slot, to_slot).create_request())
			return True

	def _handle_item_move_(self, e: ItemMoveArgs):
		# Moved items have an updated slot, so the weapon to equip is on the to_slot
		if IState.weapon_to_equip and e.to_slot == IState.weapon_to_equip.slot:
			self._handle_inventory_change_(e)

	def _handle_item_switch_(self, e: ItemMoveArgs):
		# Switched items are re-instantiated to update the collection,
		# so the reference to the weapon to equip stays on the from slot
		if IState.weapon_to_equip and e.to_slot == IState.weapon_to_equip.slot:
			self._handle_inventory_change_(e)

	def _handle_inventory_change_(self, e: ItemMoveArgs):
		sys.stdout.write(f"Equipped {IState.weapon_to_equip.type}\n")
		self.timer.stop()
		# check if a one-hand-weapon was equipped: This usually means the shield was not included
		# then send a packet to equip a shield, if any is found
		equipped_shield: Equipment = Bot.char_data.inventory.equipment.get(EquipmentSlot.Secondary.value, None)
		if (e.to_slot == EquipmentSlot.Primary.value
				and IState.weapon_to_equip.type.is_weapon_with_shield()
				and not equipped_shield):
			# choose CH or EU shieldx
			shield_type = PK2ItemType.ShieldCH if IState.weapon_to_equip.pk2_item.country is Country.Chinese else PK2ItemType.ShieldEU
			# find shield and equip it
			found_shield, shield_to_equip = self.try_find_weapon_to_switch(shield_type)
			if found_shield:
				proxy.joymax.send_packet(
					MoveItem(ItemMovementType.InventoryToInventory, shield_to_equip.slot, EquipmentSlot.Secondary).create_request())
				sys.stdout.write(f"Equipping {shield_to_equip.type}\n")
				# logging.log(BOTTING, f"")

		# reset weapon
		IState.weapon_to_equip = None
		# exit state
		self.transition(IStateType.PREVIOUS)

	def _loop_main_(self):
		self.timer.stop()
		if not IState.weapon_to_equip:
			self.transition(self.get_next_state())
		if self.try_switch_weapons():
			self.timer.start()
		else:
			self.transition(self.get_next_state())
