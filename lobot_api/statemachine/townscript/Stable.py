from typing import Dict

from lobot_api.globals.settings.LoopSettings import ShoppingSettings
from lobot_api.statemachine.scriptingstates.ShoppingState import ShoppingState
from lobot_api.statemachine.townscript.TownScript import TownScript


class Stable(ShoppingState):

	@property
	def shopping_list(self) -> Dict[int, int]: return {
		ShoppingSettings.instance().transport_type: TownScript.transport_quantity(),
		ShoppingSettings.instance().pet_hp_potion_type: TownScript.pet_hp_potion_quantity(),
		ShoppingSettings.instance().pet_pill_type: TownScript.pet_pill_quantity(),
		ShoppingSettings.instance().pethgp_id: TownScript.hgp_potion_quantity(),
		ShoppingSettings.instance().petgrassoflife_id: TownScript.grass_of_life_quantity(),
	}

	@property
	def npc_signature(self) -> str:
		return "_HORSE"  # Some private versions might use "_STABLE"

	def _get_next_target_(self):
		raise NotImplementedError()
