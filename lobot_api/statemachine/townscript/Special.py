from typing import Dict

from lobot_api.globals.settings.LoopSettings import ShoppingSettings
from lobot_api.statemachine.scriptingstates.ShoppingState import ShoppingState
from lobot_api.statemachine.townscript.TownScript import TownScript


class Special(ShoppingState):

	@property
	def shopping_list(self) -> Dict[int, int]:
		return {
			ShoppingSettings.instance().speed_pot_type: TownScript.speed_pot_quantity(),
			ShoppingSettings.instance().return_scroll_type: TownScript.return_scroll_quantity(),
			ShoppingSettings.instance().zerk_potion_type: TownScript.zerk_potion_quantity()
		}

	@property
	def npc_signature(self) -> str:
		return "_ACCESSORY"  # Some versions use "_SPECIAL"

	def _get_next_target_(self):
		raise NotImplementedError()
