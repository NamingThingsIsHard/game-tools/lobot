"""The state of executing the# get to the bot area."""
import logging
import sys

from lobot_api.Bot import Bot
from lobot_api.connection import proxy
from lobot_api.globals.PK2DataGlobal import PK2DataGlobal
from lobot_api.globals.settings.LoopSettings import ShoppingSettings
from lobot_api.logger.Logger import PK2, SCRIPT, HANDLER
from lobot_api.packets.char.BuffEndHandler import BuffEndHandler
from lobot_api.packets.char.CharInfoHandler import CharInfoHandler
from lobot_api.packets.char.SkillHandler import SkillHandler
from lobot_api.packets.inventory.UseItem import UseItem
from lobot_api.packets.pet.UpdateRiderStateHandler import UpdateRiderStateHandler
from lobot_api.pk2.PK2Item import PK2Item
from lobot_api.scripting import ScriptUtils, Town
from lobot_api.scripting.ScriptExpression import CommandExpression
from lobot_api.scripting.ShoppingArgs import ShoppingArgs
from lobot_api.statemachine.scriptingstates.ScriptState import ScriptState
from lobot_api.structs.EquipmentSlot import EquipmentSlot
from lobot_api.structs.Item import Item


class TownScript(ScriptState):

	@staticmethod
	def get_qt_to_buy_type_same_or_better(item_id, quantity: int, digits: int = 2) -> int:
		"""Find number of consumables of a given type that has to be bought.
		This accounts for consumables of an equal or higher type in the inventory.
		"""
		item: PK2Item = PK2DataGlobal.items[item_id]
		item_type_signature: str = item.long_id[0: len(item.long_id) - digits]
		item_type_index: int = int(item.long_id[len(item.long_id) - digits:])
		# Get sum of all items with the same signature and same/better type index
		ammo = Bot.char_data.inventory.equipment.get(EquipmentSlot.Secondary.value)
		amount_ammo = ammo.stack_count if ammo and item.long_id in ammo.long_id else 0
		filtered_pet = filter(lambda value:
		                      item_type_signature in value.long_id
		                      and int(value.long_id[-digits:]) >= item_type_index,
		                      Bot.pet_inventory.values())
		filtered_inventory = filter(lambda value:
		                            item_type_signature in value.long_id
		                            and int(value.long_id[-digits:]) >= item_type_index,
		                            Bot.char_data.inventory.items.values())
		same_or_better_pet: int = sum(f.stack_count for f in filtered_pet)
		same_or_better_inventory: int = sum(f.stack_count for f in filtered_inventory)
		result: int = quantity - amount_ammo - same_or_better_pet - same_or_better_inventory
		return max(result, 0)

	@staticmethod
	def get_qt_to_buy_type(item_id, quantity: int) -> int:
		"""Find number of consumables of a given type that has to be bought.
		This accounts for consumables of an equal type in the inventory.
		"""
		item: PK2Item = PK2DataGlobal.items[item_id]
		filtered_pet = list(filter(lambda v: item.long_id in v.long_id, Bot.pet_inventory.values()))
		filtered_inventory = list(filter(lambda v: item.long_id in v.long_id, Bot.char_data.inventory.items.values()))
		ammo = Bot.char_data.inventory.equipment.get(EquipmentSlot.Secondary.value)
		amount_ammo = ammo.stack_count if ammo and item.long_id in ammo.long_id else 0
		amount_in_equipment: int = sum(f.stack_count for f in filtered_pet)
		amount_in_inventory: int = sum(f.stack_count for f in filtered_inventory)
		result: int = quantity - amount_ammo - amount_in_equipment - amount_in_inventory
		return max(result, 0)

	@staticmethod
	def hp_potion_quantity() -> int:
		return TownScript.get_qt_to_buy_type_same_or_better(
			ShoppingSettings.instance().hp_potion_type,
			ShoppingSettings.instance().hp_potion_quantity)

	@staticmethod
	def mp_potion_quantity() -> int:
		return TownScript.get_qt_to_buy_type_same_or_better(
			ShoppingSettings.instance().mp_potion_type,
			ShoppingSettings.instance().mp_potion_quantity)

	@staticmethod
	def vigor_potion_quantity() -> int:
		return TownScript.get_qt_to_buy_type_same_or_better(
			ShoppingSettings.instance().vigor_potion_type,
			ShoppingSettings.instance().vigor_potion_quantity)

	@staticmethod
	def universal_pill_quantity() -> int:
		return TownScript.get_qt_to_buy_type_same_or_better(
			ShoppingSettings.instance().universal_pill_type,
			ShoppingSettings.instance().universal_pill_quantity)

	@staticmethod
	def purification_pill_quantity() -> int:
		return TownScript.get_qt_to_buy_type_same_or_better(
			ShoppingSettings.instance().purification_pill_type, ShoppingSettings.instance().purification_pill_quantity)

	@staticmethod
	def projectile_quantity() -> int:
		return TownScript.get_qt_to_buy_type(
			ShoppingSettings.instance().projectile_type, ShoppingSettings.instance().projectile_quantity)

	@staticmethod
	def speed_pot_quantity() -> int:
		return TownScript.get_qt_to_buy_type_same_or_better(
			ShoppingSettings.instance().speed_pot_type,
			ShoppingSettings.instance().speed_pot_quantity)

	@staticmethod
	def return_scroll_quantity() -> int:
		return TownScript.get_qt_to_buy_type_same_or_better(
			ShoppingSettings.instance().return_scroll_type,
			ShoppingSettings.instance().return_scroll_quantity)

	@staticmethod
	def transport_quantity() -> int:
		return TownScript.get_qt_to_buy_type_same_or_better(
			ShoppingSettings.instance().transport_type,
			ShoppingSettings.instance().transport_quantity, 1)

	@staticmethod
	def zerk_potion_quantity() -> int:
		return TownScript.get_qt_to_buy_type(
			ShoppingSettings.instance().zerk_potion_type,
			ShoppingSettings.instance().zerk_potion_quantity)

	@staticmethod
	def pet_hp_potion_quantity() -> int:
		return TownScript.get_qt_to_buy_type_same_or_better(
			ShoppingSettings.instance().pet_hp_potion_type,
			ShoppingSettings.instance().pet_hp_potion_quantity)

	@staticmethod
	def pet_pill_quantity() -> int:
		return TownScript.get_qt_to_buy_type_same_or_better(
			ShoppingSettings.instance().pet_pill_type,
			ShoppingSettings.instance().pet_pill_quantity)

	@staticmethod
	def hgp_potion_quantity() -> int:
		return TownScript.get_qt_to_buy_type(
			ShoppingSettings.instance().pethgp_id, ShoppingSettings.instance().hgp_potion_quantity)

	@staticmethod
	def grass_of_life_quantity() -> int:
		return TownScript.get_qt_to_buy_type(
			ShoppingSettings.instance().petgrassoflife_id, ShoppingSettings.instance().grass_of_life_quantity)

	def _initialize_event_handlers_(self):
		self.__initialize__()
		BuffEndHandler.on_buff_ended += self._handle_speed_buff_ended_
		UpdateRiderStateHandler.on_mount_or_dismount += self._handle_mount_or_dismount_horse_
		self._speed_buff_setup_()

		# timer_innerelapsed += RepeatCommand
		# _outer_loop_elapsed += ExecuteNextCommand
		ScriptState.on_executed_script_command += self._loop_main_
		ScriptState.on_skipped_command += self._handle_skipped_command_
		ScriptState.on_executed_shopping_command += self.handle_shopping_command
		ScriptState.on_executed_set_area_command += self._loop_main_
		ScriptState.on_executed_move_command += self._handle_executed_move_command_
		ScriptState.on_executed_teleport_command += self._handle_teleport_command_
		CharInfoHandler.on_parsed_info += self.__delayed_start__
		ScriptState.delay_timer.timeout.connect(self.__delayed_start__)
		# Movement.on_bot_movement_registered += self._handle_executed_move_command_
		SkillHandler.on_teleport_skill_used += self._handle_teleport_skill_

	def _remove_event_handlers_(self):
		# Clean up if not paused
		if not ScriptState.is_paused:
			TownScript.script = None
			ScriptState.command_index = -1

			self._speed_buff_clean_up_()
			BuffEndHandler.on_buff_ended -= self._handle_speed_buff_ended_
			UpdateRiderStateHandler.on_mount_or_dismount -= self._handle_mount_or_dismount_horse_
			# timer_innerelapsed -= RepeatCommand
			# _outer_loop_elapsed -= ExecuteNextCommand
			ScriptState.on_executed_script_command -= self._loop_main_
			ScriptState.on_skipped_command -= self._handle_skipped_command_
			ScriptState.on_executed_shopping_command -= self.handle_shopping_command
			ScriptState.on_executed_set_area_command -= self._loop_main_
			ScriptState.on_executed_move_command -= self._handle_executed_move_command_
			ScriptState.on_executed_teleport_command -= self._handle_teleport_command_
			CharInfoHandler.on_parsed_info -= self.__delayed_start__
			ScriptState.delay_timer.timeout.disconnect(self.__delayed_start__)
			# Movement.on_botmovement_registered -= self._handle_executed_move_command_
			SkillHandler.on_teleport_skill_used -= self._handle_teleport_skill_
			logging.log(SCRIPT, "Stopping script")

	def __initialize__(self):
		# skip initialization and continue with script if paused
		if ScriptState.is_paused:
			ScriptState.is_paused = False
			return
		self.current_town = Town.current_town(Bot.char_data.position)
		script = self.current_town.town_script if self.current_town else None

		# Check if close to training area
		if self._can_start_training_() or not script:
			return

		# Get closest point or first walkpoint in town
		ScriptState.command_index = (
			ScriptUtils.get_first_command_expression_index(script)
			if self.current_town is None
			else ScriptUtils.get_first_walk_point_index(script))
		# Script might not have walkpoints, just get next command then
		if ScriptState.command_index == -1:
			ScriptState.command_index = ScriptUtils.get_first_command_expression_index(script)
		# If nothing found, just return to town
		if ScriptState.command_index == -1:
			self.stop()
			# return to town
			return_scroll: Item = next(
				(tup for tup in Bot.char_data.inventory.items.values() if tup.Type is PK2.PK2ItemType.ReturnScroll), None)
			if return_scroll is not None:
				proxy.joymax.send_packet(UseItem(return_scroll.slot).create_request())
			return
		# TODO MetaExpression Enhancement
		# if ScriptUtils.MatchesRequirements(script.Where(expr => expr is MetaExpression):
		# {
		#    //Try to execute
		# }
		# else:
		# {
		#    //TODO: search script that fulfills the predicates given in the script
		# }
		# Get all commands after closest walkpoint
		ScriptState._script_ = list(filter(lambda expr: isinstance(expr, CommandExpression), script[ScriptState.command_index:]))
		# set to -1 for loop_inner to start with first item
		ScriptState.command_index = -1

		# Start with first command in list
		# RepeatCommand(None, None) #EventArgs.Empty)

	def _get_next_target_(self):
		raise NotImplementedError()

	def handle_shopping_command(self, e: ShoppingArgs):
		self._pause_()
		self.transition(e.i_state_type, e.npc)

	def _loop_inner_(self):
		# Reached the end of script
		if self.current_script_command is None:
			self._transition_to_next_script_()
			return
		try:
			self.execute(self.current_script_command)
		except NotImplementedError as ex:
			logging.log(SCRIPT, str(ex))
			self.timer_inner.stop()
			self.timer.stop()
			self.on_executed_script_command.notify()

	def _loop_main_(self):
		self.timer_inner.stop()
		ScriptState.command_index += 1

		if self._should_mount_():
			self._mount_horse_()
			return

		# Reached the end of script
		if not ScriptState._script_\
			or not self._is_index_in_range_(ScriptState.command_index, len(ScriptState._script_)):
			self._transition_to_next_script_()
			return
		try:
			self.execute(self.current_script_command)
		except NotImplementedError as ex:
			logging.log(HANDLER, ex, exc_info=sys.exc_info())
			self.on_executed_script_command.notify()
