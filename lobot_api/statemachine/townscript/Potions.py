from typing import Dict

from lobot_api.globals.settings.LoopSettings import ShoppingSettings
from lobot_api.statemachine.scriptingstates.ShoppingState import ShoppingState
from lobot_api.statemachine.townscript.TownScript import TownScript


class Potions(ShoppingState):

	@property
	def shopping_list(self) -> Dict[int, int]:
		return {
			ShoppingSettings.instance().hp_potion_type: TownScript.hp_potion_quantity(),
			ShoppingSettings.instance().mp_potion_type: TownScript.mp_potion_quantity(),
			ShoppingSettings.instance().vigor_potion_type: TownScript.vigor_potion_quantity(),
			ShoppingSettings.instance().universal_pill_type: TownScript.universal_pill_quantity(),
			ShoppingSettings.instance().purification_pill_type: TownScript.purification_pill_quantity(),
		}

	@property
	def npc_signature(self) -> str:
		return "_POTION"

	def _get_next_target_(self):
		raise NotImplementedError()
