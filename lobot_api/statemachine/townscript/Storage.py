from typing import Optional

from lobot_api.Bot import Bot
from lobot_api.connection import proxy
from lobot_api.globals.SpawnListsGlobal import SpawnListsGlobal
from lobot_api.packets.char.CharDataHandler import CharDataHandler
from lobot_api.packets.inventory.ItemMoveArgs import ItemMoveArgs
from lobot_api.packets.inventory.ItemMovementType import ItemMovementType
from lobot_api.packets.inventory.MoveInventoryToStorage import MoveInventoryToStorage
from lobot_api.packets.inventory.MoveItem import MoveItem
from lobot_api.packets.inventory.MoveStorageToStorage import MoveStorageToStorage
from lobot_api.packets.npc.NPCCloseDialogue import NPCCloseDialogue
from lobot_api.packets.npc.NPCInteractionType import NPCInteractionType
from lobot_api.packets.npc.NPCOpenDialogue import NPCOpenDialogue
from lobot_api.packets.npc.NPCOpenStorage import NPCOpenStorage
from lobot_api.packets.npc.NPCSelect import NPCSelect
from lobot_api.packets.storage.StorageInfoDataHandler import StorageInfoDataHandler
from lobot_api.statemachine.scriptingstates.NPCInteractionState import NPCInteractionState
from lobot_api.structs.Item import Item


class Storage(NPCInteractionState):
	"""Store action during town script.
	Starts after the abstract NPCInteraction state phase is finished and an NPC has been opened.
	"""

	"""The flag indicating whether the server will send storage info when the storage is open.
	This is True after teleporting and False after the first storage info packet is sent after teleporting.
	"""
	__is_storage_info_needed__: bool = True
	__current_pet_item_to_move__: Item = None
	____current_item_to_store____: Item = None
	__pet_to_inventory_type__ = ItemMovementType.PetToInventory
	__inventory_to_storage_type__ = ItemMovementType.InventoryToStorage

	@property
	def npc_signature(self) -> str:
		return "_WAREHOUSE"

	@property
	def interaction_type(self):
		return NPCInteractionType.Store

	def __init__(self):
		"""set storage info flag after teleporting."""
		super(Storage, self).__init__()
		CharDataHandler.on_packet += self.__handle_char_data_parsed__
		StorageInfoDataHandler.on_storage_parsed += self.__handle_storage_parsed__

	def __handle_char_data_parsed__(self):
		Storage.__is_storage_info_needed__ = True

	def __handle_storage_parsed__(self):
		Storage.__is_storage_info_needed__ = False
		if self.is_running and proxy.is_clientless:
			proxy.joymax.send_delayed_packet(
				NPCOpenDialogue(Storage.current_target.unique_id, self.interaction_type).create_request(),
				self.NPC_DELAY)
		# Continue without interaction
		# InnerLoopFunction() #EventArgs.Empty)

	def _initialize_event_handlers_(self):
		MoveStorageToStorage.on_item_moved += self.__handle_item_sorted__
		MoveInventoryToStorage.on_item_stored += self.__handle_item_sorted__

	def _remove_event_handlers_(self):
		MoveStorageToStorage.on_item_moved -= self.__handle_item_sorted__
		MoveInventoryToStorage.on_item_stored -= self.__handle_item_sorted__

	def _handle_npc_selected_(self):
		if (Bot.selected_npc.unique_id == self.current_target.unique_id
				and Bot.selected_npc.unique_id in SpawnListsGlobal.NPCSpawns):
			NPCSelect.on_selected_npc_changed -= self._handle_npc_selected_
			proxy.joymax.send_packet(NPCCloseDialogue(Storage.current_target.unique_id).create_request())
			# In case storage information has not been parsed yet, wait for info from the StorageInfoHandler 0x3049
			if Storage.__is_storage_info_needed__:
				# Client should send 7046 request to server, wait for this request
				NPCOpenDialogue.on_interaction_started += self._handle_interaction_started_
				proxy.joymax.send_delayed_packet(NPCOpenStorage(
					Storage.current_target.unique_id).create_request(), self.NPC_DELAY)
			else:
				# Continue with waiting for interaction to start
				NPCOpenDialogue.on_interaction_started += self._handle_interaction_started_
				proxy.joymax.send_delayed_packet(NPCOpenDialogue(Storage.current_target.unique_id,
				                                                 self.interaction_type).create_request(),
				                                 self.NPC_DELAY)
				# Continue without interaction
				# InnerLoopFunction() #EventArgs.Empty)

	# def __get_pet_item__(self):
	# 	self.__current_pet_item_to_move__ = next((value for key, value in Bot.pet_inventory.items()), None)

	def _get_next_target_(self):
		self.__current_item_to_store__ = next((value for key, value in Bot.char_data.inventory.items.items() if
		                                       value.pk2_item.should_store()), None)

	def __handle_item_sorted__(self, e: Optional[ItemMoveArgs]):
		for i in range(0xFF & Bot.storage_size - 1, -1):
			if i in Bot.storage:
				self.__move_or_stack_storage__(Bot.storage[i])
				break
		# continue with storing if no sorting is done
		self.__handle_item_stored__(None)

	def __get_number_of_stacks_to_move_storage__(self, from_slot, to_slot):
		stacks_available = Bot.storage[to_slot].max_stacks - Bot.storage[to_slot].stack_count
		if stacks_available > 0:
			return (Bot.storage[from_slot].stack_count
			        if stacks_available >= Bot.storage[from_slot].stack_count  # enough slots left
			        else Bot.storage[to_slot].stack_count)  # insufficient slots
		else:
			return Bot.storage[to_slot].max_stacks - Bot.storage[to_slot].stack_count

	def __move_or_stack_storage__(self, current_item: Item):
		for i in range(0, current_item.slot):
			# Find free slot to move or slot that can be stacked
			if not i in Bot.storage:  # Move
				command: MoveItem = MoveItem(ItemMovementType.StorageToStorage, current_item.slot, i,
				                             current_item.stack_count, self.current_target.unique_id)
				proxy.joymax.send_delayed_packet(command.create_request(), self.NPC_DELAY)
				return
			elif Bot.storage[i].ref_id == current_item.ref_id and Bot.storage[i].stack_count < Bot.storage[
				i].max_stacks:  # Stack
				stacks_to_move = self.__get_number_of_stacks_to_move_storage__(current_item.slot, i)
				command: MoveItem = MoveItem(ItemMovementType.StorageToStorage, current_item.slot, i, stacks_to_move,
				                             self.current_target.unique_id)
				proxy.joymax.send_delayed_packet(command.create_request(), self.NPC_DELAY)
				return
		# continue with storing if no sorting is done
		self.__handle_item_stored__(None)

	# def __try_move_pet_item__(self) -> bool:
	# 	if Bot.char_data.inventory.size == len(Bot.char_data.inventory.items):
	# 		return False
	#
	# 	self.__get_pet_item__()
	# 	if self.__current_pet_item_to_move__ is None:
	# 		return False
	#
	# 	next_free_slot = next(i for i in range(-1, Bot.char_data.inventory.size) if i not in Bot.char_data.inventory)
	# 	command: MoveItem = MoveItem(self.__pet_to_inventory_type__, self.__current_pet_item_to_move__.slot, next_free_slot,
	# 	                             self.__current_pet_item_to_move__.stack_count, Bot.pickup_pet.unique_id)
	# 	proxy.joymax.send_delayed_packet(command.create_request(), self.NPC_DELAY_SEC)
	# 	return self.__current_pet_item_to_move__ is not None
	#
	# def __handle_item_moved_from_pet__(self, e: Optional[ItemMoveArgs]):
	# 	if e.type is ItemMovementType.PetToInventory and e.from_slot == self.__current_pet_item_to_move__.slot:
	# 		self.__handle_item_stored__(e)

	def __handle_item_stored__(self, e: Optional[ItemMoveArgs]):
		# Store next item if any left to store
		# find first free slot
		if Bot.storage_size == len(Bot.storage):
			self._start_stacking_()
			return

		# self._stop_interaction_()
		# move pet items to inventory first
		# if Bot.pickup_pet.unique_id != 0 and self.__try_move_pet_item__():
		# 	return

		# if self.__current_item_to_store__:
		# 	next_free_slot = next(i for i in range(-1, Bot.char_data.inventory.size) if i not in Bot.char_data.inventory)
		# TODO try: and retrieve items from pet

		self._get_next_target_()
		if self.__current_item_to_store__ is None:
			self._start_stacking_()
			return

		to_slot = next(i for i in range(0, Bot.storage_size) if i not in Bot.storage)
		command: MoveItem = MoveItem(self.__inventory_to_storage_type__, self.__current_item_to_store__.slot, to_slot,
		                             self.__current_item_to_store__.stack_count, self.current_target.unique_id)
		proxy.joymax.send_delayed_packet(command.create_request(), self.NPC_DELAY)

	def _loop_inner_(self):
		self.__handle_item_sorted__(None)
