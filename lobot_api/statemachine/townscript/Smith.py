from typing import Dict

from lobot_api.connection import proxy
from lobot_api.globals.settings.LoopSettings import ShoppingSettings
from lobot_api.packets.char.CharDataHandler import CharDataHandler
from lobot_api.packets.npc.NPCRepair import NPCRepair
from lobot_api.statemachine.IState import IState
from lobot_api.statemachine.scriptingstates.ShoppingState import ShoppingState
from lobot_api.statemachine.townscript.TownScript import TownScript


class Smith(ShoppingState):
	__areItemsRepaired__ = False

	@property
	def shopping_list(self) -> Dict[int, int]:
		return {
			ShoppingSettings.instance().projectile_type: TownScript.projectile_quantity()
		}

	@property
	def npc_signature(self) -> str:
		return "_SMITH"

	def __init__(self):
		super(Smith, self).__init__()
		CharDataHandler.on_packet += self.__reset_items_repaired__

	@staticmethod
	def __reset_items_repaired__():
		Smith.__areItemsRepaired__ = False

	def __handle_items_repaired__(self):
		NPCRepair.on_items_repaired -= self.__handle_items_repaired__
		__areItemsRepaired__ = True
		# sys.stdout.write("Repaired items\n")
		if self.has_to_sell_items():
			self._sell_next_item_()
		else:
			self._buy_next_item_()

	def repair(self):
		NPCRepair.on_items_repaired += self.__handle_items_repaired__
		proxy.joymax.send_delayed_packet(NPCRepair(IState.current_target.unique_id).create_request(),
		                                 self.NPC_DELAY)

	def _get_next_target_(self):
		raise NotImplementedError()

	def _loop_inner_(self):
		if not Smith.__areItemsRepaired__:
			self.repair()
		else:
			if self.has_to_sell_items():
				self._sell_next_item_()
			else:
				self._buy_next_item_()
