import sys
from typing import Optional

from lobot_api.Bot import Bot
from lobot_api.BotStatus import BotStatus
from lobot_api.Event import Event
from lobot_api.ObjectStatusChangedArgs import ObjectStatusChangedArgs
from lobot_api.packets.char.MovementArgs import MovementArgs
from lobot_api.scripting import ScriptUtils
from lobot_api.statemachine.IState import IState, IStateType
from lobot_api.statemachine.Idle import Idle
from lobot_api.statemachine.StateChangeException import StateChangeException
from lobot_api.statemachine.SwitchWeapon import SwitchWeapon
from lobot_api.statemachine.scriptingstates.ScriptState import ScriptState
from lobot_api.statemachine.scriptingstates.WalkScript import WalkScript
from lobot_api.statemachine.townscript.Potions import Potions
from lobot_api.statemachine.townscript.Smith import Smith
from lobot_api.statemachine.townscript.Special import Special
from lobot_api.statemachine.townscript.Stable import Stable
from lobot_api.statemachine.townscript.Storage import Storage
from lobot_api.statemachine.townscript.TownScript import TownScript
from lobot_api.statemachine.training.Attack import Attack
from lobot_api.statemachine.training.CastBuff import CastBuff
from lobot_api.statemachine.training.CastHeal import CastHeal
from lobot_api.statemachine.training.Pickup import Pickup
from lobot_api.statemachine.training.Resurrect import Resurrect
from lobot_api.statemachine.training.Return import Return
from lobot_api.statemachine.training.Roam import Roam
from lobot_api.structs.BoolEventArgs import BoolEventArgs
from lobot_api.structs.ISpawnObject import ISpawnObject
from lobot_api.structs.Monster import Monster
from lobot_api.structs.ObjectStatus import ObjectStatus


class FiniteStateMachine:
	__instance__ = None

	@staticmethod
	def instance():
		if FiniteStateMachine.__instance__ is None:
			FiniteStateMachine.__instance__ = FiniteStateMachine()
		return FiniteStateMachine.__instance__

	idle = Idle()
	town_script = TownScript()
	walk_script = WalkScript()
	# Stalling = Stalling()
	# Alchemy = Alchemy()
	# Exchange = Exchange()
	# Trade = Trade()
	# # Town
	storage = Storage()
	smith = Smith()
	potions = Potions()
	specialty = Special()
	stable = Stable()
	# Training
	attack = Attack()
	cast_buff = CastBuff()
	cast_heal = CastHeal()
	pickup = Pickup()
	# PetPickup = PetPickup()
	resurrect = Resurrect()
	return_ = Return()
	roam = Roam()
	switch_weapon = SwitchWeapon()

	state: IState = idle
	last_state: IState = None

	transitions = {
		IStateType.CAST_BUFF: cast_buff,
		IStateType.CAST_HEAL: cast_heal,
		IStateType.ATTACK: attack,
		IStateType.PICKUP: pickup,
		IStateType.IDLE: idle,
		IStateType.TOWN_SCRIPT: town_script,
		IStateType.WALK_SCRIPT: walk_script,
		IStateType.STORAGE: storage,
		IStateType.SHOP_SMITH: smith,
		IStateType.SHOP_POTIONS: potions,
		IStateType.SHOP_SPECIAL: specialty,
		IStateType.SHOP_STABLE: stable,
		IStateType.RESURRECT: resurrect,
		IStateType.RETURN: return_,
		IStateType.ROAM: roam,
		IStateType.SWITCH_WEAPON: switch_weapon
	}

	on_bot_status_changed = Event()

	def __init__(self, state: IState = None):
		if state:
			self.state = state
		self.is_running = False

	def start(self):
		if self.is_running or self.state.is_running:
			return
		self.is_running = True
		Bot.set_status(BotStatus.Botting)
		# PetReviveFunctions._initialize_pet_revive_handlers()
		self.__initialize__()
		self.__handle_state_change__(None)

	def stop(self):
		if not self.state or not self.is_running:
			return
		self.is_running = False
		# PetReviveFunctions.remove_pet_revive_handlers()
		self.__tear_down__()
		if self.state.is_running:
			self.state.stop()
		self.last_state = self.idle
		self.state = self.idle
		Bot.set_status(BotStatus.Idle)
		# logging.log(BOTTING, "Stopping training\n")
		sys.stdout.write("Stopping training\n")

	def _handle_char_status_changed_(self, e: ObjectStatusChangedArgs):
		"""Use this to handle# switching between invincible and non-invincible states."""
		if (e.previous_status is ObjectStatus.Invincible
				and Bot.char_data.is_alive
				and self.state in [IStateType.TOWN_SCRIPT, IStateType.WALK_SCRIPT, IStateType.ROAM, IStateType.IDLE]):
			next_state = IState.get_next_state()
			if next_state is not self.state:
				self.state.transition(next_state)

	def __initialize__(self):
		IState.on_state_changed += self.__handle_state_change__
		Bot.on_bot_status_changed += self.__handle_bot_status_change__
		Bot.char_data.on_status_changed += self._handle_char_status_changed_

		# Movement.on_spawnmovement_registered += self.HandleSpawnMovement
		# ParseMonster.on_parsed += self._HandleMonsterSpawn
		# ParseItem.on_parsed += self.HandleDropSpawn
		# SpawnHandler.on_despawned += self.RemoveSpawn

	def __tear_down__(self):
		IState.on_state_changed -= self.__handle_state_change__
		Bot.on_bot_status_changed -= self.__handle_bot_status_change__
		Bot.char_data.on_status_changed -= self._handle_char_status_changed_

		# Movement.on_spawnmovement_registered -= self.HandleSpawnMovement
		# ParseMonster.on_parsed -= self.HandleMonsterSpawn
		# ParseItem.on_parsed -= self.HandleDropSpawn
		# SpawnHandler.on_despawned -= self.RemoveSpawn

	@staticmethod
	def should_return_to_town(e: BoolEventArgs) -> bool:
		return e.flag

	@staticmethod
	def should_switch_to_attack_mode(e: MovementArgs, spawn: ISpawnObject) -> bool:
		return isinstance(spawn, Monster) and ScriptUtils.is_point_in_any_area(e.destination)

	# and not isinstance(self.__state__, Attack))

	def __handle_state_change__(self, e: Optional[IStateType], target=None):
		if e is None:
			sys.stdout.write(f"Start {self.state.__class__.__name__}\n")
			self.state.start()
		else:
			next_state = self.last_state if e is IStateType.PREVIOUS else self.transitions.get(e, self.idle)

			# if next_state and target:
			# 	next_state.current_target = target

			if next_state is self.idle:
				sys.stdout.write(f"Switch to Idle and stop\n")
				self.stop()
			elif next_state is self.state:
				raise StateChangeException(next_state)
			else:
				self.last_state = self.state
				self.state = next_state
				sys.stdout.write(f"Switch to state {self.state.__class__.__name__}\n")
				self.state.start()

	def __handle_bot_status_change__(self):
		if Bot.get_status() is not BotStatus.Botting\
			and self.state not in [self.return_, self.walk_script, self.town_script]\
			or Bot.get_status() not in [BotStatus.Idle, BotStatus.Loading, BotStatus.Botting]\
			and self.state in [self.walk_script, self.town_script]\
			and not ScriptState.is_paused:
			self.stop()
