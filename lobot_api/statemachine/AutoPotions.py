from PyQt5.QtCore import QTimer

from lobot_api import ReusableTimer
from lobot_api.Bot import Bot
from lobot_api.connection import proxy
from lobot_api.globals.settings import TrainingSettings
from lobot_api.globals.settings.TrainingSettings import PlayerSettings, COSSettings, PetSettings
from lobot_api.packets.char.ObjectActionType import ObjectActionType
from lobot_api.packets.char.UseSkill import UseSkill
from lobot_api.packets.inventory.UseItem import UseItem, Optional
from lobot_api.packets.inventory.UseItemType import UseItemType
from lobot_api.pk2.PK2Item import PK2ItemType, Country
from lobot_api.pk2.PK2Skill import PK2SkillType
from lobot_api.structs.DebuffStatus import DebuffStatus
from lobot_api.structs.Item import Item


def __handle_hgp_tick__():  # e: elapsedEventArgs):
	if Bot.growth_pet and Bot.growth_pet.hgp > 0:
		Bot.growth_pet.hgp -= 1
		AutoPotions.try_use_hgp_pot(Bot.growth_pet.unique_id)
	else:
		AutoPotions.pet_hgp_timer.stop()


def __handle_pill_timer_tick__():
	if Bot.char_data.debuff_status in [DebuffStatus.BadStatusDebuff, DebuffStatus.BadStatus]:
		AutoPotions.try_cure_bad_status()
	else:
		AutoPotions.pet_hgp_timer.stop()


class AutoPotions:
	MAX_HGP: int = 10000
	HGP_TICK_TIME: int = 3000
	CH_INTERVAL: int = 1000
	EU_INTERVAL: int = 15000
	GRAIN_INTERVAL: int = 5000

	current_country_interval = 1000

	player_vigor_timer = ReusableTimer.get_q_timer(CH_INTERVAL)
	player_hp_timer = ReusableTimer.get_q_timer(CH_INTERVAL)
	player_mp_timer = ReusableTimer.get_q_timer(CH_INTERVAL)
	player_pill_timer = ReusableTimer.get_q_timer(CH_INTERVAL, __handle_pill_timer_tick__)
	player_purification_timer = ReusableTimer.get_q_timer(GRAIN_INTERVAL)

	pet_hp_timer = ReusableTimer.get_q_timer(CH_INTERVAL)
	pet_pill_timer = ReusableTimer.get_q_timer(GRAIN_INTERVAL)
	pet_hgp_timer = ReusableTimer.get_q_timer(HGP_TICK_TIME, __handle_hgp_tick__)

	cos_hp_timer = ReusableTimer.get_q_timer(GRAIN_INTERVAL)

	@staticmethod
	def is_zombie_status(flags: int):
		return 0x20 == (flags & 0x20)

	@staticmethod
	def handle_char_data_parsed(country: Country):
		"""Check country for different hp/mp/vigor intervals"""
		interval = AutoPotions.CH_INTERVAL if country is Country.Chinese else AutoPotions.EU_INTERVAL

		AutoPotions.current_country_interval = interval
		AutoPotions.player_vigor_timer = ReusableTimer.get_q_timer(interval)
		AutoPotions.player_hp_timer = ReusableTimer.get_q_timer(interval)
		AutoPotions.player_mp_timer = ReusableTimer.get_q_timer(interval)

	@staticmethod
	def handle_hgp_spawn():
		"""Handle changes by pet update handler 0x30C9"""
		AutoPotions.pet_hgp_timer.start()

	@classmethod
	def handle_hgp_despawn(cls):
		"""Handle changes by pet update handler 0x30C9"""
		AutoPotions.pet_hgp_timer.stop()

	@staticmethod
	def __send_use_potion_command__(potion_index: int, timer: QTimer, target_id: int = 0) -> bool:
		use_item_type = UseItemType.Normal

		if target_id != 0:
			if Bot.growth_pet.unique_id == target_id or Bot.transport and Bot.transport.unique_id == target_id:
				use_item_type = UseItemType.UseOnPet

		if timer.isActive():
			use = UseItem(potion_index, use_item_type, target_id)
			packet = use.create_request()
			proxy.joymax.send_packet(packet)
			timer.start()
			return True
		else:
			return False

	@staticmethod
	def __should_use_pot__(current_value: int, max_value: int, percentage: int, use_condition1: bool,
	                       use_condition2: bool = True) -> bool:
		return use_condition1 and use_condition2 and int(current_value / max_value * 100) < percentage


	@staticmethod
	def __try_use_pot__(timer: QTimer, resource_current: int, resource_max: int, resource_use_percent: int,
	                    use_resource: bool, type_1: PK2ItemType, type_2: PK2ItemType = PK2ItemType.Other,
						interval_1=1, interval_2=5):
		if Bot.char_data.is_alive and AutoPotions.__should_use_pot__(resource_current, resource_max, resource_use_percent,
		                                                             use_resource):
			potion: Optional[Item] = None
			potion_fallback: Optional[Item] = None

			# find potion of preferred type and stop, or go on and look for fallback type
			for key, value in Bot.char_data.inventory.items.items():
				if value.type is type_1:
					potion = value
					break
				if value.type is type_2:
					potion_fallback: Item = value

			if potion:
				timer.interval = interval_1
				AutoPotions.__send_use_potion_command__(potion.slot, timer)
			elif potion_fallback:
				timer.interval = interval_2
				AutoPotions.__send_use_potion_command__(potion_fallback.slot, timer)

	@staticmethod
	def try_use_hp_pot():
		if not AutoPotions.is_zombie_status(Bot.char_data.debuff_flags) and AutoPotions.player_hp_timer.isActive():
			type_1, type_2, interval_1, interval_2 = (PK2ItemType.HPGrain, PK2ItemType.HPPot, AutoPotions.GRAIN_INTERVAL, AutoPotions.current_country_interval) \
				if TrainingSettings.PlayerSettings.prefer_grains.value \
				else (PK2ItemType.HPPot, PK2ItemType.HPGrain, AutoPotions.current_country_interval, AutoPotions.GRAIN_INTERVAL)

			AutoPotions.__try_use_pot__(AutoPotions.player_hp_timer,
			                            Bot.char_data.current_hp,
			                            Bot.char_info.max_hp,
			                            PlayerSettings.hp_pot_percent.value,
			                            PlayerSettings.hp_use_pot.value,
			                            type_1, type_2,
			                            interval_1, interval_2)

	@staticmethod
	def try_use_mp_pot():
		if AutoPotions.player_mp_timer.isActive():
			type_1, type_2, interval_1, interval_2 = (PK2ItemType.MPGrain, PK2ItemType.MPPot, AutoPotions.GRAIN_INTERVAL, AutoPotions.current_country_interval) \
				if TrainingSettings.PlayerSettings.prefer_grains.value \
				else (PK2ItemType.MPPot, PK2ItemType.MPGrain, AutoPotions.current_country_interval, AutoPotions.GRAIN_INTERVAL)
			AutoPotions.__try_use_pot__(AutoPotions.player_mp_timer,
			                            Bot.char_data.current_mp,
			                            Bot.char_info.max_mp,
			                            PlayerSettings.mp_pot_percent.value,
			                            PlayerSettings.mp_use_pot.value,
			                            type_1, type_2,
			                            interval_1, interval_2)

	"""Uses a vigour potion after checking whether conditions to use potions are met and potions are available."""

	# The potion is used then a timer signifying the potion delay is started.
	@staticmethod
	def try_use_vigor() -> bool:
		should_use_mp = AutoPotions.__should_use_pot__(Bot.char_data.current_mp,
		                                               Bot.char_info.max_mp,
		                                               PlayerSettings.mp_vigor_percent.value,
		                                               PlayerSettings.mp_use_vigor.value)
		should_use_hp = AutoPotions.__should_use_pot__(Bot.char_data.current_hp,
		                                             Bot.char_info.max_hp,
		                                             PlayerSettings.hp_vigor_percent.value,
		                                             PlayerSettings.hp_use_vigor.value)

		if not AutoPotions.is_zombie_status(Bot.char_data.debuff_flags) and AutoPotions.player_vigor_timer.isActive()\
				and (should_use_mp or should_use_hp):
			type_1, type_2, interval_1, interval_2 = (PK2ItemType.VigorGrain, PK2ItemType.VigorPot, AutoPotions.GRAIN_INTERVAL, AutoPotions.current_country_interval)\
				if TrainingSettings.PlayerSettings.prefer_grains.value\
				else (PK2ItemType.VigorPot, PK2ItemType.VigorGrain, AutoPotions.current_country_interval, AutoPotions.GRAIN_INTERVAL)

			current, max_, percent = ((Bot.char_data.current_mp, Bot.char_info.max_mp, PlayerSettings.mp_vigor_percent.value)
			                          if should_use_mp else (Bot.char_data.current_hp, Bot.char_info.max_hp, PlayerSettings.hp_vigor_percent.value))

			AutoPotions.__try_use_pot__(AutoPotions.player_mp_timer,
			                            current,
			                            max_,
			                            percent,
			                            True,
			                            type_1, type_2,
			                            interval_1, interval_2)
			return True
		else:
			return False

	@staticmethod
	def try_cure_bad_status():
		if Bot.char_data.debuff_status is DebuffStatus.BadStatus:
			if PlayerSettings.bad_status_pill_or_spell.value:
				skill = next((s for s in Bot.char_data.skills if
				              s.ref_id == PlayerSettings.bad_status_spell.value), None)
				if skill and skill.skill_enabled == 0x01 and PlayerSettings.bad_status_spell.value != 0:
					proxy.joymax.send_packet(UseSkill(PlayerSettings.bad_status_spell.value, ObjectActionType.UseSkill,
					                                  Bot.char_data.unique_id).create_request())
			elif AutoPotions.player_pill_timer.isActive():
				potion: Item = next(
					(value for key, value in Bot.char_data.inventory.items.items() if
					 value.type is PK2ItemType.UniversalPill),
					None)
				if potion:
					AutoPotions.__send_use_potion_command__(potion.slot, AutoPotions.player_pill_timer)

	@staticmethod
	def try_cure_debuff():
		if Bot.char_data.debuff_status == DebuffStatus.Debuff.value \
				or any(b.type is PK2SkillType.Debuff for b in Bot.char_data.active_buffs.values()):
			if PlayerSettings.curse_pill_or_spell.value:
				skill = next((s for s in Bot.char_data.skills if s.ref_id == PlayerSettings.curse_spell.value), None)
				if skill and skill.skill_enabled == 0x01 and PlayerSettings.curse_spell.value != 0:
					proxy.joymax.send_packet(
						UseSkill(PlayerSettings.curse_spell.value, ObjectActionType.UseSkill,
						         Bot.char_data.unique_id).create_request())
			else:
				pill: Item = next(
					(value for key, value in Bot.char_data.inventory.items.items() if
					 value.type is PK2ItemType.PurificationPill),
					None)
				if pill:
					AutoPotions.__send_use_potion_command__(pill.slot, AutoPotions.player_purification_timer)

	@staticmethod
	def try_cure_bad_status_or_cure_debuff():
		if Bot.char_data.debuff_status is DebuffStatus.BadStatusDebuff:
			AutoPotions.try_cure_bad_status()
			AutoPotions.try_cure_debuff()
		if Bot.char_data.debuff_status is DebuffStatus.BadStatus:
			AutoPotions.try_cure_bad_status()
		elif Bot.char_data.debuff_status is DebuffStatus.Debuff:
			AutoPotions.try_cure_debuff()

	@staticmethod
	def try_cure_pet_bad_status():
		if Bot.growth_pet.BadStatus != 0 and AutoPotions.pet_pill_timer.isActive():
			potion: Item = next(
				(value for key, value in Bot.char_data.inventory.items.items() if value.type is PK2ItemType.PetPill),
				None)
			if potion:
				AutoPotions.__send_use_potion_command__(potion.slot, AutoPotions.player_pill_timer)

	# TODO
	@staticmethod
	def try_use_cos_pot(cos_id: int, resource_current: int, resource_max: int, resource_use_percent: int,
	                    use_resource: bool, potion_name_prefix: str) -> bool:
		if Bot.growth_pet.is_alive and AutoPotions.__should_use_pot__(resource_current, resource_max, resource_use_percent,
		                                                              use_resource):
			potion: Item = next(
				(value for key, value in Bot.char_data.inventory.items.items() if value.type is PK2ItemType.PetHPPotion),
				None)
			if potion:
				return AutoPotions.__send_use_potion_command__(potion.slot, AutoPotions.pet_hp_timer, cos_id)
		return False

	"""Uses an transport HP potion after checking whether conditions to use potions are met and potions are available."""

	# The potion is used then a timer signifying the potion delay is started.
	@staticmethod
	def try_use_transport_hp_pot(cos_id: int):
		if not AutoPotions.cos_hp_timer.isActive():
			AutoPotions.try_use_cos_pot(cos_id, Bot.transport.current_hp, Bot.transport.max_hp,
			                            COSSettings.HPPotPercent.value,
			                            COSSettings.HPUsePot.value,
			                         "ITEM_ETC_COS_HP_POTION_")

	"""Uses a pet HP potion after checking whether conditions to use potions are met and potions are available."""

	# The potion is used then a timer signifying the potion delay is started.
	@staticmethod
	def try_use_pet_hp_pot(cos_id: int):
		if not AutoPotions.pet_hp_timer.isActive():
			AutoPotions.try_use_cos_pot(cos_id, Bot.growth_pet.current_hp, Bot.growth_pet.max_hp,
			                            PetSettings.hp_pot_percent.value,
			                            PetSettings.hp_use_pot.value,
			                         "ITEM_ETC_COS_HP_POTION_")

	"""Uses a pet pill after checking whether conditions to use pills are met and potions are available."""

	# The potion is used then a timer signifying the potion delay is started.
	@staticmethod
	def try_use_pet_pill(cos_id: int):
		if not AutoPotions.pet_pill_timer.isActive():
			AutoPotions.try_use_cos_pot(cos_id, Bot.growth_pet.current_hp, Bot.growth_pet.max_hp,
			                            PetSettings.hp_pot_percent.value,
			                            PetSettings.hp_use_pot.value,
			                         "ITEM_ETC_COS_HP_POTION_")

	"""Uses an HGP potion after checking whether conditions to use potions are met and potions are available."""

	# The potion is used then a timer signifying the potion delay is started.
	@staticmethod
	def try_use_hgp_pot(cos_id: int) -> bool:
		return AutoPotions.try_use_cos_pot(cos_id, Bot.growth_pet.hgp, AutoPotions.MAX_HGP,
		                                   PetSettings.hgp_pot_percent.value,
		                                   PetSettings.hgp_use_pot.value, "ITEM_COS_P_HGP_POTION_")
