﻿using LobotAPI;
using LobotAPI.Globals;
using LobotAPI.Globals.Settings;
using LobotAPI.Packets.Party;
using LobotAPI.StateMachine;
using LobotAPI.Structs;
using LobotDLL.Connection;
using LobotDLL.Packets;
using LobotDLL.Packets.Community;
using LobotDLL.Packets.Party;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Threading.Tasks;
using System.Timers;

namespace LobotDLL.StateMachine
{
    public class PartyStateMachine : FiniteStateMachine
    {
        const double InvitationDelay = 1000;
        protected static Timer InvitationTimer = new Timer(InvitationDelay);
        private static PartyStateMachine instance;

        private PartyStateMachine() { }

        /// <summary>
        /// Singleton constructor.
        /// </summary>
        public static PartyStateMachine Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new PartyStateMachine();
                }
                return instance;
            }
        }

        private static readonly TimeSpan InvitationResponseInterval = TimeSpan.FromSeconds(11);
        private static Timer TimerInvite = new Timer(5000);
        private static Timer JoinTimer = new Timer(10000);
        //private static IEnumerable PartiesToJoin;
        private static bool IsInviting = false;
        private static List<uint> TimeoutInvites = new List<uint>();
        private static List<uint> ToInvite = new List<uint>();

        public static void CreatePartyMatch(object sender, EventArgs e)
        {
            if (!Bot.PartyCurrent.IsFull && (string.IsNullOrEmpty(Bot.PartyCurrent.Leader)
                || Bot.PartyCurrent.Leader == Bot.CharData.Name) && PartySettings.AutoReform && Bot.PartyCurrent.PartyNumber == 0)
            {
                PartyEntry party = new PartyEntry(0);
                party.type = PartySettings.type;
                party.Objective = PartySettings.Objective;
                party.MinLevel = PartySettings.MinLevel;
                party.MaxLevel = PartySettings.MaxLevel;
                PartySettings.Title = PartySettings.Title;

                if (IsInviting)
                {
                    Proxy.Instance.SendCommandAG(10000, new PartyCreate(party)).ConfigureAwait(false);
                }
                else
                {
                    Proxy.Instance.SendCommandAG(500, new PartyCreate(party)).ConfigureAwait(false);
                }
            }
        }
        public static void DeletePartyMatch(object sender, NotifyCollectionChangedEventArgs e)
        {
            Proxy.Instance.SendCommandAG(1000, new PartyMatchDeleteEntry()).ConfigureAwait(false);
        }
        public static void AcceptJoinPartyMatch(object sender, PartyJoinArgs e)
        {
            if (!Bot.PartyCurrent.IsFull && PartySettings.AutoAcceptInvite)
            {
                Proxy.Instance.SendCommandAG(new PartyMatchAcceptJoin(e.PartyNumber, e.MemberID));
            }
        }
        public static void AcceptInvitation(object sender, PartyInvitationArgs e)
        {
            if (e.PartyType == PartySettings.type && PartySettings.AutoAcceptInvite)
            {
                Proxy.Instance.SendCommandAG(new PartyReceiveInvitation(PartyInvitationResponseType.Accept));
            }
        }
        public static void JoinPartyMatch(object sender, PartyChangedArgs e)
        {

            switch (e.type)
            {
                case PartyChangeType.Empty:
                    Proxy.Instance.SendCommandAG(new PartyMatchRefresh());
                    //TODO
                    break;
                case PartyChangeType.Update:
                    // Check for actions
                    break;
                case PartyChangeType.Full:
                    break;
                default:
                    break;
            }
        }

        public static void InviteSpawnedPlayer(uint playerID)
        {
            // only invite if player not in timeout
            if (!TimeoutInvites.Contains(playerID))
            {
                ToInvite.Add(playerID);

                if (!TimerInvite.Enabled)
                {
                    TimerInvite.Elapsed += InviteFromList;
                    TimerInvite.Start();
                }
            }
        }
        public static void InvitePlayer(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (sender is ObservableCollection<CharData> && e.Action == NotifyCollectionChangedAction.Add && PartySettings.AutoAcceptInvite)
            {
                foreach (CharData player in e.NewItems)
                {
                    if (!Bot.PartyCurrent.Members.Any(m => m.Name == player.Name))
                    {
                        InviteSpawnedPlayer(player.UniqueID);
                    }
                }
            }
        }
        public static void KickPlayer(uint playerID)
        {
            Proxy.Instance.SendCommandAG(1000, new PartyBanMember(playerID)).ConfigureAwait(false);
        }

        private static async Task RemoveFromTimeOut(uint toInvite)
        {
            await Task.Delay(InvitationResponseInterval).ContinueWith(task =>
            {
                if (TimeoutInvites.Count > 0)
                {
                    TimeoutInvites.Remove(toInvite);
                }
            });
        }
        private static void InviteFromList(object sender, ElapsedEventArgs e)
        {
            // remove despawned players from invitation list
            while (ToInvite.Count > 0 && !SpawnListsGlobal.PlayerList.Any(chardata => chardata.UniqueID == ToInvite[0]))
            {
                ToInvite.RemoveAt(0);
            }

            // invite if remaining elements are still in spawnlist
            if (ToInvite.Count > 0 && SpawnListsGlobal.PlayerList.Any(chardata => chardata.UniqueID == ToInvite[0]))
            {
                Proxy.Instance.SendCommandAG(new PartyInvite(ToInvite[0], PartySettings.type));

                // keep in timeout to prevent spamming requests
                uint toInvite = ToInvite[0];
                TimeoutInvites.Add(toInvite);

                RemoveFromTimeOut(toInvite).ConfigureAwait(false);

                //finally remove from list
                ToInvite.RemoveAt(0);

            }

            //stop once last element has been removed
            if (ToInvite.Count < 1)
            {
                TimerInvite.Elapsed -= InviteFromList;
                TimerInvite.Stop();
            }

        }

        private void InitializeEventhandlers()
        {
            PartySettings.ToggledAutoAcceptInvite += HandleAutoAcceptInvite;
            PartySettings.ToggledAutoReform += HandleAutoReform;

            if (PartySettings.AutoAcceptInvite)
            {
                if (Bot.CharData.Level > 4)
                {
                    SpawnListsGlobal.PlayerList.CollectionChanged += InvitePlayer;
                    PartyMatchJoinHandler.JoinRequest += AcceptJoinPartyMatch;
                }
                PartyReceiveInvitation.ReceivedInvitation += AcceptInvitation;
            }
            if (PartySettings.AutoReform && Bot.CharData.Level > 4)
            {
                FriendInfoHandler.HandledFriendInfo += CreatePartyMatch; // switched to friendinfohandler because charinfohandlder can be sent multiple times in a row
                PartyMatchDeleteEntry.PartyMatchEntryDeleted += CreatePartyMatch;
            }
            if (PartySettings.AutoJoin)
            {
                PartyMemberUpdateHandler.PartyMemberUpdated += JoinPartyMatch;
            }
        }

        private void HandleAutoAcceptInvite(object sender, BoolEventArgs e)
        {
            if (e.Flag)
            {
                SpawnListsGlobal.PlayerList.CollectionChanged += InvitePlayer;
            }
            else
            {
                SpawnListsGlobal.PlayerList.CollectionChanged -= InvitePlayer;
            }
        }
        private void HandleAutoReform(object sender, BoolEventArgs e)
        {
            if (e.Flag)
            {
                PartyMatchDeleteEntry.PartyMatchEntryDeleted += CreatePartyMatch;
                if (Bot.PartyCurrent.PartyNumber == 0)
                {
                    CreatePartyMatch(sender, e);
                }
            }
            else
            {
                PartyMatchDeleteEntry.PartyMatchEntryDeleted -= CreatePartyMatch;
            }
        }

        public override void Start()
        {
            InitializeEventhandlers();
            //AbstractPartyStateMachine.Execute();
        }

        public override void Stop()
        {
            IsRunning = false;
            //AbstractPartyStateMachine.Execute();
        }
    }
}