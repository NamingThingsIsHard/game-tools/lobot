from typing import Optional

from lobot_api.Bot import Bot
from lobot_api.connection import proxy
from lobot_api.packets.inventory.ItemMoveArgs import ItemMoveArgs
from lobot_api.packets.inventory.ItemMovementType import ItemMovementType
from lobot_api.packets.inventory.MoveInventoryToInventory import MoveInventoryToInventory
from lobot_api.packets.inventory.MoveItem import MoveItem
from lobot_api.packets.inventory.MovePetToInventory import MovePetToInventory
from lobot_api.statemachine.IState import IState
from lobot_api.structs import EquipmentSlot
from lobot_api.structs.Item import Item


class PetItemTransfer(IState):
	item_movement_type: ItemMovementType = ItemMovementType.PetToInventory
	ITEM_MOVE_DELAY: int = 5000
	__current_item_to_move__: Optional[Item] = None

	def _get_next_target_(self):
		self.__current_item_to_move__ = Bot.pet_inventory.pop(0)

	def _initialize_event_handlers_(self):
		MoveInventoryToInventory.on_item_moved += self.__handle_inventory_item_sorted__
		MovePetToInventory.on_item_moved += self.__handle_item_moved__

	def _remove_event_handlers_(self):
		MoveInventoryToInventory.on_item_moved -= self.__handle_inventory_item_sorted__
		MovePetToInventory.on_item_moved -= self.__handle_item_moved__
		self.__current_item_to_move__ = None

	def _get_number_of_stacks_to_move_(self, from_slot: int, to_slot: int) -> int:
		stacks_available: int = Bot.pet_inventory[to_slot].MaxStacks - Bot.pet_inventory[
			to_slot].stack_count
		if stacks_available > 0:
			return (Bot.pet_inventory[from_slot].stack_count
			        if stacks_available >= Bot.pet_inventory[from_slot].stack_count  # enough slots left
			        else Bot.pet_inventory[to_slot].stack_count)  # insufficient slots
		else:
			return Bot.pet_inventory[to_slot].MaxStacks - Bot.pet_inventory[to_slot].stack_count

	def __get_number_of_stacks_to_move_storage__(self, from_slot: int, to_slot: int) -> int:
		stacks_available: int = Bot.char_data.inventory[to_slot].MaxStacks - Bot.char_data.inventory[
			to_slot].stack_count
		if stacks_available > 0:
			return (Bot.char_data.inventory[from_slot].stack_count
			        if stacks_available >= Bot.char_data.inventory[from_slot].stack_count  # enough slots left
			        else Bot.char_data.inventory[to_slot].stack_count)  # insufficient slots
		else:
			return Bot.char_data.inventory[to_slot].MaxStacks - Bot.char_data.inventory[to_slot].stack_count

	def _stack_item_(self, current_item: Item) -> bool:
		for i in range(0, current_item.slot):
			#  Find free slot to move or slot that can be stacked
			if (i in Bot.pet_inventory
					and Bot.pet_inventory[i].ref_id == current_item.ref_id
					and Bot.pet_inventory[i].stack_count < Bot.pet_inventory[i].MaxStacks):  # Stack
				stacksToMove: int = 0xFFFF & self._get_number_of_stacks_to_move_(current_item.slot, i)
				command: MoveItem = MoveItem(ItemMovementType.InventoryToInventory, current_item.slot, i, stacksToMove)
				proxy.joymax.send_packet(PetItemTransfer.ITEM_MOVE_DELAY, command)  # .ConfigureAwait(false)
				return True
		return False

	def _sort_item_(self, current_item: Item):
		for i in range(0, current_item.slot):
			#  Find free slot to move or slot that can be stacked
			if not Bot.pet_inventory.ContainsKey(i):  # Move
				command: MoveItem = MoveItem(ItemMovementType.InventoryToInventory, current_item.slot, i,
				                             0xFFFF & current_item.stack_count)
				proxy.joymax.send_packet(PetItemTransfer.ITEM_MOVE_DELAY, command)  # .ConfigureAwait(false)
				return
		self._stop_sorting_()

	def _handle_pet_item_stacked_(self, e: Optional[ItemMoveArgs]):
		for i in range(Bot.pet_inventory_size, 0, -1):
			if i in Bot.pet_inventory:
				if self._stack_item_(Bot.pet_inventory[i]):
					return
		#  continue with sorting
		self._stop_stacking_()

	def _start_stacking_(self):
		# MovePetToPet.ItemStacked += HandlePetItemStacked
		self._handle_pet_item_stacked_(None)

	def _stop_stacking_(self):
		# MovePetToPet.ItemStacked -= HandlePetItemStacked
		self._start_sorting_()

	def _start_sorting_(self):
		# MovePetToPet.ItemMoved += HandlePetItemSorted
		self._handle_pet_item_sorted_(None)

	def _stop_sorting_(self):
		# MovePetToPet.ItemMoved -= HandlePetItemSorted
		self.stop()

	def _handle_pet_item_sorted_(self, e: Optional[ItemMoveArgs] = None):
		for i in range(Bot.pet_inventory_size, 0, -1):
			if i in Bot.pet_inventory:
				self._sort_item_(Bot.pet_inventory[i])
				break

	def __handle_inventory_item_sorted__(self, e: Optional[ItemMoveArgs] = None):
		for i in range(Bot.char_data.inventory.size, EquipmentSlot.MAX_EQUIP_SLOTS, -1):
			if Bot.char_data.inventory.ContainsKey(i):
				self.__move_or_stack_inventory__(Bot.char_data.inventory[i])
				break

	def __handle_item_moved__(self, e: Optional[ItemMoveArgs] = None):
		#  Move next item from pet to inventory if any left to move
		#  find first free slot
		if Bot.char_data.inventory.size is len(Bot.char_data.inventory):
			self.stop()
		else:
			self._get_next_target_()
			#  Check if any item has been selected
			if self.__current_item_to_move__ is None:
				#  Try to sort inventory then stop interaction
				self._start_stacking_()
				return
			to_slot = 0
			for i in range(0, Bot.char_data.inventory.size):
				if i not in Bot.char_data.inventory:
					to_slot = i
					break
			command: MoveItem = MoveItem(PetItemTransfer.item_movement_type,
			                             PetItemTransfer.__current_item_to_move__.slot, to_slot,
			                             0xFFFF & PetItemTransfer.__current_item_to_move__.stack_count)
			proxy.joymax.send_packet(PetItemTransfer.ITEM_MOVE_DELAY, command).ConfigureAwait(False)

	def __move_or_stack_inventory__(self, current_item: Item):
		#  Start after Equipment slots
		for i in range(0, current_item.slot):
			#  Find free slot to move or slot that can be stacked
			if not Bot.char_data.inventory.ContainsKey(i):  # Move
				command: MoveItem = MoveItem(ItemMovementType.InventoryToInventory, current_item.slot, i,
				                             0xFFFF & current_item.stack_count)
				proxy.joymax.send_packet(PetItemTransfer.ITEM_MOVE_DELAY, command)  # .ConfigureAwait(false)
				return
			elif Bot.char_data.inventory[i].ref_id == current_item.ref_id and Bot.char_data.inventory[
				i].stack_count < Bot.char_data.inventory[i].MaxStacks:  # Stack
				stacks_to_move: int = 0xFFFF & self._get_number_of_stacks_to_move_(current_item.slot, i)
				command: MoveItem = MoveItem(ItemMovementType.InventoryToInventory, current_item.slot, i,
				                             stacks_to_move)
				proxy.joymax.send_packet(PetItemTransfer.ITEM_MOVE_DELAY, command)  # .ConfigureAwait(false)
				return
		#  continue with moving from pet to inventory if no sorting is done
		self.__handle_item_moved__(None)

	def _loop_inner_(self):
		raise NotImplementedError()

	def _loop_main_(self):
		self.__handle_inventory_item_sorted__()
