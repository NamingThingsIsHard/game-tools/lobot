﻿using LobotAPI;
using LobotAPI.Globals;
using LobotAPI.Packets.Inventory;
using LobotAPI.Packets.NPC;
using LobotAPI.Scripting;
using LobotAPI.Structs;
using LobotDLL.Connection;
using LobotDLL.Packets;
using LobotDLL.Packets.Inventory;
using System;
using System.Collections.Generic;
using System.Timers;

namespace LobotDLL.StateMachine
{
    /// <summary>
    /// Base class for any npc states.
    /// These states require a chain of
    /// - Outerloop to send NPCSelection package when state starts()
    /// - Wait for correct NPC selection event
    /// - Wait for start NPC interaction event
    /// - Innerloop to start state-specific function (e.g. store item then Wait for item storage during loop)
    /// 
    /// Example
    //select
    //[C -> S][7045]
    //38 00 00 00                                       8...............
    //// open store window
    //[C -> S][704B]
    //38 00 00 00                                       8...............

    //[C -> S][7046]
    //38 00 00 00                                       8...............
    //01                                                ................

    //[S -> C][B04B]
    //01                                                ................

    //[S -> C][B046]
    //01                                                ................
    //01                                                ................

    //// close
    //[C -> S][704B]
    //38 00 00 00                                       8...............

    //[S -> C][B04B]
    //01                                                ................
    /// </summary>
    public abstract class NPCInteractionState : BotState
    {
        /// <summary>
        /// Safety delay for npc actions. Should be used in subclasses as interval to prevent spamming commands.
        /// </summary>
        protected const int NPC_ACTION_DELAY = 500;
        /// <summary>
        /// Safety distance to select NPC.
        /// Attempting a selection past this distance might fail.
        /// </summary>
        private const int MAX_SELECT_DISTANCE = 50;
        /// <summary>
        /// The npc/shop string to searching for in spawn lists 
        /// </summary>
        public readonly string NPCSignature;
        /// <summary>
        /// Item used in sort functions
        /// </summary>
        protected Item CurrentItemToSort = null;

        protected abstract NPCInteractionType InteractionType { get; }

        protected virtual uint GetNumberOfStacksToMove(byte fromSlot, byte toSlot)
        {
            long stacksAvailable = CharInfoGlobal.Inventory[toSlot].MaxStacks - CharInfoGlobal.Inventory[toSlot].StackCount;

            if (stacksAvailable > 0)
            {
                return stacksAvailable >= CharInfoGlobal.Inventory[fromSlot].StackCount
                    ? CharInfoGlobal.Inventory[fromSlot].StackCount // enough slots left
                    : CharInfoGlobal.Inventory[toSlot].StackCount; // insufficient slots
            }
            else
            {
                return (uint)CharInfoGlobal.Inventory[toSlot].MaxStacks - CharInfoGlobal.Inventory[toSlot].StackCount;
            }
        }

        /// <summary>
        /// Stack items then sort them 
        /// </summary>
        protected void StartStacking()
        {
            MoveInventoryToInventory.ItemStacked += HandleInventoryItemStacked;
            HandleInventoryItemStacked(null, null);
        }
        protected void StopStacking()
        {
            MoveInventoryToInventory.ItemStacked -= HandleInventoryItemStacked;
            StartSorting();
        }
        protected void StartSorting()
        {
            MoveInventoryToInventory.ItemMoved += HandleInventoryItemSorted;
            HandleInventoryItemSorted(null, null);
        }
        protected void StopSorting()
        {
            MoveInventoryToInventory.ItemMoved -= HandleInventoryItemSorted;
            StopInteraction(null, EventArgs.Empty);
        }
        /// <summary>
        /// Attempt to stack item and return whether a stackable item was found or not.
        /// </summary>
        /// <param name="currentItem"></param>
        /// <returns>Was command to stack target item sent</returns>
        protected virtual bool StackItem(Item currentItem)
        {
            for (byte i = CharInfoGlobal.EQUIPMENT_SLOTS; i < currentItem.Slot; i++)
            {
                // Find free slot to move or slot that can be stacked
                if (CharInfoGlobal.Inventory.TryGetValue(i, out Item item)
                    && item.RefItemID == currentItem.RefItemID
                    && item.StackCount < item.MaxStacks) // Stack
                {
                    ushort stacksToMove = (ushort)GetNumberOfStacksToMove(currentItem.Slot, i);
                    MoveItem command = new MoveItem(ItemMovementType.InventoryToInventory, currentItem.Slot, i, stacksToMove);
                    Proxy.Instance.SendCommandAG(NPC_ACTION_DELAY, command).ConfigureAwait(false);
                    return true;
                }
            }
            return false;
        }
        protected virtual void SortItem(Item currentItem)
        {
            for (byte i = CharInfoGlobal.EQUIPMENT_SLOTS; i < currentItem.Slot; i++)
            {
                // Find free slot to move or slot that can be stacked
                if (!CharInfoGlobal.Inventory.ContainsKey(i)) // Move
                {
                    MoveItem command = new MoveItem(ItemMovementType.InventoryToInventory, currentItem.Slot, i, (ushort)currentItem.StackCount);
                    Proxy.Instance.SendCommandAG(NPC_ACTION_DELAY, command).ConfigureAwait(false);
                    return;
                }
            }

            StopSorting();
        }
        protected void HandleInventoryItemStacked(object sender, ItemMoveArgs e)
        {
            for (byte i = Bot.CharData.Inventory.Size; i >= CharInfoGlobal.EQUIPMENT_SLOTS; i--)
            {
                if (CharInfoGlobal.Inventory.TryGetValue(i, out Item item)
                    && StackItem(item))
                {
                    return;
                }
            }
            // continue with sorting
            StopStacking();
        }
        protected void HandleInventoryItemSorted(object sender, ItemMoveArgs e)
        {
            for (byte i = Bot.CharData.Inventory.Size; i >= CharInfoGlobal.EQUIPMENT_SLOTS; i--)
            {
                if (CharInfoGlobal.Inventory.TryGetValue(i, out Item item))
                {
                    SortItem(CharInfoGlobal.Inventory[i]);
                    break;
                }
            }
        }

        protected NPCInteractionState(string npcSignature, Timer innerLoop = null, Timer outerLoop = null) : base(innerLoop, outerLoop)
        {
            NPCSignature = npcSignature;
        }
        protected virtual void HandleNPCSelected(object sender, EventArgs e)
        {
            if (Bot.SelectedNPC.UniqueID == CurrentTarget.UniqueID)
            {
                Bot.SelectednpcIDChanged -= HandleNPCSelected;

                if (SpawnListsGlobal.AllSpawnEntries.TryGetValue(Bot.SelectedNPC.UniqueID, out SpawnListsGlobal.SpawnList list))
                {
                    // Continue with waiting for interaction to start
                    Packets.NPC.NPCOpenDialogue.InteractionStarted += HandleInteractionStarted;
                    // Close selection dialogue
                    Proxy.Instance.SendCommandAG(new NPCCloseDialogue(CurrentTarget.UniqueID));
                    // Open interaction dialogue
                    Proxy.Instance.SendCommandAG(NPC_ACTION_DELAY, new Packets.NPC.NPCOpenDialogue(CurrentTarget.UniqueID, LobotAPI.Packets.NPC.NPCInteractionType.Store)).ConfigureAwait(false);
                }

                // NO Need for waiting on interaction: selection is enough to continue with script
                //InnerLoopFunction(null, EventArgs.Empty);
            }
        }
        protected virtual void HandleInteractionStarted(object sender, EventArgs e)
        {
            Packets.NPC.NPCOpenDialogue.InteractionStarted -= HandleInteractionStarted;
            //Start state-specific NPC function
            InnerLoopFunction(null, EventArgs.Empty);
        }
        private void HandleInteractionStopped(object sender, EventArgs e)
        {
            NPCCloseDialogue.DialogueClosed -= HandleInteractionStopped;
            Console.WriteLine($"[{GetType().Name}] Stopped interaction with {CurrentTarget?.Name}");
            Transition(TownScript);
        }

        /// <summary>
        /// this function replaces "Transition" in subclasses
        /// TODO call "Transition" and make this function fire. Suddenly having to switch functions is confusing
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void StopInteraction(object sender, EventArgs e)
        {
            // Remove previous eventhandler and stop interaction before returning to script
            NPCCloseDialogue.DialogueClosed += HandleInteractionStopped;
            Proxy.Instance.SendCommandAG(NPC_ACTION_DELAY, new NPCCloseDialogue(CurrentTarget.UniqueID)).ConfigureAwait(false);
        }

        protected sealed override void OuterLoopFunction(object sender, EventArgs e)
        {
            //Find first entry in the spawnlist matching the shop type, based on the signature
            foreach (KeyValuePair<uint, Monster> spawn in SpawnListsGlobal.NPCSpawns)
            {
                if (PK2DataGlobal.NPCs.TryGetValue(spawn.Value.RefID, out LobotAPI.PK2.Pk2NPC value)
                    && value.LongId.Contains(NPCSignature))
                {
                    //Make sure npc is within selection distance, otherwise it is not safe to attempt an interaction
                    if (ScriptUtils.get_distance(Bot.CharData.position, spawn.Value.position) > MAX_SELECT_DISTANCE)
                    {
                        // Proceed with next script command
                        Logger.Log(LogLevel.SCRIPT, $"NPC {spawn.Value.Name} too far to select -- {ScriptUtils.get_distance(Bot.CharData.position, spawn.Value.position)} of max {MAX_SELECT_DISTANCE} steps away");
                        Transition(TownScript);
                        return;
                    }

                    Bot.SelectednpcIDChanged += HandleNPCSelected;
                    CurrentTarget = spawn.Value;
                    Proxy.Instance.SendCommandAG(NPC_ACTION_DELAY, new NPCSelect(spawn.Key)).ConfigureAwait(false);
                    return;
                }
            }

            // Return to TownScript if nothing is found
            Transition(TownScript);
        }
    }
}