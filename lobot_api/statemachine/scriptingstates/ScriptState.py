import logging
import random
import sys
from abc import ABC
from builtins import staticmethod
from typing import List, Optional, Dict, Callable

from PyQt5.QtCore import QTimer

from lobot_api.Bot import Bot
from lobot_api.BotStatus import BotStatus
from lobot_api.Event import Event
from lobot_api.ObjectStatusChangedArgs import ObjectStatusChangedArgs
from lobot_api.connection import proxy
from lobot_api.globals.PK2DataGlobal import PK2DataGlobal
from lobot_api.globals.SpawnListsGlobal import SpawnListsGlobal
from lobot_api.globals.settings.LoopSettings import ScriptSettings
from lobot_api.globals.settings.TrainingSettings import TrainingAreaSettings
from lobot_api.logger.Logger import SCRIPT
from lobot_api.packets.IRequest import IRequest
from lobot_api.packets.char.BuffArgs import BuffArgs
from lobot_api.packets.char.Movement import Movement
from lobot_api.packets.char.MovementArgs import MovementArgs
from lobot_api.packets.inventory.UseItem import UseItem
from lobot_api.packets.npc.NPCSelect import NPCSelect
from lobot_api.packets.npc.NPCTeleport import NPCTeleport
from lobot_api.packets.pet.PetAction import PetAction, PetActionType
from lobot_api.packets.pet.PetDismount import PetDismount
from lobot_api.pk2.PK2Item import PK2ItemType
from lobot_api.pk2.PK2Skill import PK2SkillType, PK2Skill
from lobot_api.scripting.AreaChangedArgs import AreaType
from lobot_api.scripting.Point import Point
from lobot_api.scripting.ScriptExpression import AbstractExpression, CommandExpression, ExpressionParser
from lobot_api.scripting.ShoppingArgs import ShoppingArgs
from lobot_api.scripting.Town import Town
from lobot_api.statemachine.IState import IState, IStateType
from lobot_api.structs.Item import Item
from lobot_api.structs.Monster import Monster
from lobot_api.structs.ObjectStatus import ObjectStatus


class ScriptState(IState, ABC):
	repetition_interval_ms: int = 5000
	default_teleport_wait_interval_ms: int = 5000
	_script_: List[AbstractExpression] = []
	current_town: Optional[Town] = None
	command_index: int = 0
	is_speed_buff_setup: bool = False
	is_paused: bool = False
	delay_timer = QTimer()

	on_skipped_command = Event()
	on_executed_move_command = Event()
	on_executed_delay_command = Event()
	on_executed_teleport_command = Event()
	on_executed_shopping_command = Event()
	on_executed_set_area_command = Event()
	on_executed_script_command = Event()
	on_stopped_script = Event()

	@property
	def current_script_command(self) -> Optional[CommandExpression]:
		"""Use flag to only add handlers for speed buff once."""
		return ScriptState._script_[ScriptState.command_index]\
			if ScriptState._script_ and -1 < ScriptState.command_index < len(ScriptState._script_)\
			else None

	def _pause_(self):
		ScriptState.is_paused = True
		self._remove_event_handlers_()

	def _resume_(self):
		self._loop_main_()

	def _handle_executed_move_command_(self, e: MovementArgs):
		# Make sure it is the movement command that was sent by the script
		if not self.timer.isActive():
			# Take missing origin point into account
			delay = (Movement.get_walk_time(Bot.char_data.position, e.destination)
			         if not e.origin
			         else Movement.get_walk_time(e.origin, e.destination))
			if delay > 0:
				sys.stdout.write(f"DelayCalculated:{delay}\n")
				self.timer_inner.setInterval(ScriptState.default_teleport_wait_interval_ms)
				self.timer.setInterval(delay)
				self.timer_inner.start()
				self.timer.start()
			else:
				self.on_executed_script_command.notify()

	def _handle_mount_or_dismount_horse_(self):
		self._loop_inner_()

	def _handle_teleport_command_(self, args: List[str]):
		if len(args) == 2:
			ref_id = int(args[0])
			portal = next((value for key, value in SpawnListsGlobal.Portals.items()
			               if value.ref_id == ref_id and int(args[1]) in value.PK2Data.TeleporterData.TeleportLinks),
			              None)
			if portal:
				teleport_option = int(args[1])

				# Send command
				ScriptState.is_paused = True
				proxy.joymax.send_packet(NPCSelect(portal.unique_id).create_request())
				proxy.joymax.send_packet(NPCTeleport(portal.unique_id, teleport_option).create_request())
				# Set timers
				self.timer.stop()
				self.timer_inner.stop()
				ScriptState.delay_timer.start(ScriptState.default_teleport_wait_interval_ms)
		else:
			# reverse return NotImplementedError yet implemented
			# logging.log(SCRIPT, f"Teleport command not implemented. Returning to town")
			raise NotImplementedError()

	def _mount_horse_(self):
		if self._should_mount_():
			pet_scroll_slot = next((key for key, value in Bot.char_data.inventory.items.items() if value.pk2_item.type is PK2ItemType.PetRide), 0)
			if pet_scroll_slot != 0:
				proxy.joymax.send_packet(UseItem(pet_scroll_slot).create_request())
				return

	def _use_speed_buff_(self):
		# Don't try to speed up if horse is chosen
		if self._should_mount_() or Bot.char_data.transport_flag == 1:
			return

		# In case no horse was found
		if self._should_cast_speed_buff_():
			IStateType.IsCastingSpeedBuff = True
			# keep last script command to prevent restarting
			self._pause_()
			self.transition(IStateType.CAST_BUFF)
		elif self._should_use_speed_pot_():
			for key, value in Bot.char_data.inventory.items.items():
				if value.type in [PK2ItemType.SpeedScroll, PK2ItemType.SpeedPotion]:
					# Don't use if speed scroll and not set to use speed scroll
					if "SPEED_UP" in value.long_id and not ScriptSettings.UseSpeedScroll.value:
						continue
					proxy.joymax.send_packet(UseItem(key).create_request())
					break

	def _cast_speed_buff_after_interaction_(self):
		if not Bot.is_interacting:
			self._use_speed_buff_()

	def _cast_speed_buff_after_invincible_(self, e: ObjectStatusChangedArgs):
		if (e.current_status is not ObjectStatus.Invincible
				and e.previous_status is ObjectStatus.Invincible
				and not Bot.is_interacting):  # Wait until interaction stops before casting buff
			self._use_speed_buff_()

	def _speed_buff_setup_(self):
		"""Setup handlers to cast speed buff or use speed pot"""
		if not ScriptState.is_speed_buff_setup:
			ScriptState.is_speed_buff_setup = True
			Bot.char_data.on_status_changed += self._cast_speed_buff_after_invincible_
			Bot.on_is_interacting_changed += self._cast_speed_buff_after_interaction_
			self._use_speed_buff_()

	def _speed_buff_clean_up_(self):
		"""Remove handlers for speed buff or speed pot"""
		ScriptState.is_speed_buff_setup = False
		Bot.char_data.on_status_changed -= self._cast_speed_buff_after_invincible_
		Bot.on_is_interacting_changed -= self._cast_speed_buff_after_interaction_

	def _handle_speed_buff_ended_(self, e: BuffArgs):
		"""Reapply speed buff if canceled or timed out."""
		pk2_skill: PK2Skill = PK2DataGlobal.skills.get(e.ref_id, None)
		if pk2_skill and pk2_skill.type is PK2SkillType.SpeedBuff:
			self._use_speed_buff_()

	# TODO figure out why script crashes instead of using this
	@staticmethod
	def _is_index_in_range_(index: int, upper_bound: int) -> bool:
		return -1 < index < upper_bound

	def __delayed_start__(self):
		"""Resume script after pause or respawn, for example by using wait command."""
		ScriptState.is_paused = False
		Bot.set_status(BotStatus.Botting)
		ScriptState.delay_timer.stop()
		# Skip wait commands after this
		for i in range(ScriptState.command_index + 1, len(ScriptState._script_)):
			if isinstance(ScriptState._script_[i], CommandExpression)\
					and ScriptState._script_[i].command_name != 'wait':
				# _loop_main_ increments after this, so set to i - 1
				ScriptState.command_index = i - 1
				break

		logging.log(SCRIPT, "resuming script")
		self._loop_main_()

	@staticmethod
	def _can_transition_to_walkscript_() -> bool:
		try:
			walk_script = ExpressionParser.parse_script(ScriptSettings.TrainingScriptPath.value)
			return walk_script is not None
		except ValueError as fe:
			logging.log(SCRIPT, f"WalkScript error: {fe}", exc_info=sys.exc_info())

	def _transition_to_next_script_(self) -> bool:
		if self._can_start_training_():  # Check if close to training area
			self.transition(self.get_next_state())
			return True
		elif self._can_transition_to_walkscript_():
			self.transition(IStateType.WALK_SCRIPT)
			return True
		# TODO enable quest script
		# elif CanTransitionToQuestScript():
		# {
		#    _transition_(Quest)
		#    return True
		# }
		else:
			self.transition(IStateType.IDLE)
			return False

	def _handle_skipped_command_(self):
		ScriptState.command_index += 1
		self._loop_main_()

	def __handle_set_area__(self):
		pass
		# if self._can_start_training_():
		# 	self.transition(self.get_next_state())

	def _is_movement_script_command_(self, e: MovementArgs) -> bool:
		x = int(self.current_script_command.arguments[0])
		y = int(self.current_script_command.arguments[1])
		jitter = int(self.current_script_command.arguments[2]) if len(self.current_script_command.arguments) > 2 else 0
		return (self.current_script_command.command_name == "go"
		        and e.unique_id == Bot.char_data.unique_id
		        and x - jitter <= e.destination.x <= x + jitter
		        and y - jitter <= e.destination.y <= y + jitter)

	@staticmethod
	def __get_random_script_jitter__(variance: int) -> Point:
		"""Function to generate randomness for the next walking point, if an argument is given."""
		return Point(random.randint(-variance, variance), random.randint(-variance, variance))

	@staticmethod
	def __go_command__(args: List[str]) -> IRequest:
		if args is None:
			raise ValueError(args)
		elif len(args) < 2:
			raise ValueError(args)
		x: int = int(args[0])
		y: int = int(args[1])
		jitter: Point = ScriptState.__get_random_script_jitter__(int(args[2])) if len(args) == 3 else Point.from_absolute(0, 0)
		sys.stdout.write(f"go {args[0]}, {args[1]}\n")
		# send the correct type, depending on whether a transport pet is being used
		movement_packet: IRequest
		target: Point = Point.from_absolute_coords((x + jitter.x), (y + jitter.y), 0)
		if Bot.char_data.transport_flag == 0x01:
			movement_packet = PetAction(PetActionType.Move, Bot.transport.unique_id, target.x, target.y)
		else:
			movement_packet = Movement(target)
		proxy.joymax.send_packet(movement_packet.create_request())
		ScriptState.on_executed_move_command.notify(MovementArgs(Bot.char_data.unique_id, target, Bot.char_data.position))
		return movement_packet

	@staticmethod
	def __wait_command__(args: List[str]):
		"""Wait before using any further scripts commands.
		Script is paused for 10 seconds or until the character has teleported and respawned.
		This is skipped, if the script is already paused.
		"""
		ScriptState.is_paused = True
		delay = int(args[0]) if len(args) == 1 else ScriptState.default_teleport_wait_interval_ms
		ScriptState.delay_timer.start(delay)
		logging.log(SCRIPT, f"wait for {delay}ms or until respawn")

	@staticmethod
	def __teleport_command__(args: List[str]):
		"""
		Trigger event for teleport command. Does not work in static method
		:param args:
		:return:
		"""
		ScriptState.on_executed_teleport_command.notify(args)

	@staticmethod
	def __get_shop_from_spawn_list__(signature: str) -> Monster:
		return next((value for key, value in SpawnListsGlobal.NPCSpawns.items() if signature in value.long_id), None)

	@staticmethod
	def __open_shop__(state, type_enum: IStateType):  # : IStateTypes
		"""
		Trigger event to switch state to a given shop and type.
		Sends event with shop signature and type.
		:param state: The class of the target shop.
		:param type_enum: The enum for the FiniteStateMachine to choose
		:return:
		"""
		try:
			ScriptState.on_executed_shopping_command.notify(
				ShoppingArgs(ScriptState.__get_shop_from_spawn_list__(state.npc_signature), type_enum))
		except ValueError as ve:
			logging.log(SCRIPT, f"Shop not found in area {Bot.char_data.position}. Continuing script")
			ScriptState.on_skipped_command.notify()

	@staticmethod
	def __mount__():
		item: Item = next((value for key, value in Bot.char_data.inventory.items if value.type is PK2ItemType.PetVehicle),
		                  None)
		if item is not None:
			proxy.joymax.send_packet.SendCommandAG(UseItem(item.slot))  # TODO mount best horse

	@staticmethod
	def __dismount__():
		if Bot.char_data.transport_flag == 0x01:
			proxy.joymax.send_packet(PetDismount())

	@staticmethod
	def __set_area__(area_type: AreaType, args: List[str]):
		x: int = int(args[0])
		y: int = int(args[1])
		radius: int = int(args[2])
		if area_type is AreaType.AreaOne:
			TrainingAreaSettings.instance().area1.x = x
			TrainingAreaSettings.instance().area1.y = y
			TrainingAreaSettings.instance().area1.radius = radius
		else:
			TrainingAreaSettings.instance().area2.x = x
			TrainingAreaSettings.instance().area2.y = y
			TrainingAreaSettings.instance().area2.radius = radius

		ScriptState.on_executed_set_area_command.notify()

	@staticmethod
	def __command_list__() -> Dict[str, Callable]:
		"""List of currently implemented functions.
		Meta expressions match their commandName to the string to Execute the matching function."""
		from lobot_api.statemachine.townscript.Potions import Potions
		from lobot_api.statemachine.townscript.Smith import Smith
		from lobot_api.statemachine.townscript.Special import Special
		from lobot_api.statemachine.townscript.Stable import Stable
		from lobot_api.statemachine.townscript.Storage import Storage

		return {
			"go": lambda s: ScriptState.__go_command__(s),
			"wait": lambda s: ScriptState.__wait_command__(s),
			"teleport": lambda s: ScriptState.__teleport_command__(s),
			"quest": lambda _: (_ for _ in ()).throw(NotImplementedError()),
			"store": lambda s: ScriptState.__open_shop__(Storage, IStateType.STORAGE),
			"stable": lambda s: ScriptState.__open_shop__(Stable, IStateType.SHOP_STABLE),
			"repair": lambda s: ScriptState.__open_shop__(Smith, IStateType.SHOP_SMITH),
			"potions": lambda s: ScriptState.__open_shop__(Potions, IStateType.SHOP_POTIONS),
			"special": lambda s: ScriptState.__open_shop__(Special, IStateType.SHOP_SPECIAL),
			"mount": lambda s: ScriptState.__mount__(),
			"dismount": lambda s: ScriptState.__dismount__(),
			"setArea1": lambda s: ScriptState.__set_area__(AreaType.AreaOne, s),
			"setArea2": lambda s: ScriptState.__set_area__(AreaType.AreaTwo, s)
		}

	@staticmethod
	def execute(expr):
		"""Invoke method in expression"""
		if expr is None:
			raise ValueError(expr)
		else:
			ScriptState.__command_list__()[expr.command_name](expr.arguments)
