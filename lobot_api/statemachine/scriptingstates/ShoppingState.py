import logging
import sys
from abc import ABC, abstractmethod
from typing import Dict, Optional

from lobot_api.Bot import Bot
from lobot_api.BotStatus import BotStatus
from lobot_api.connection import proxy
from lobot_api.globals.PK2DataGlobal import PK2DataGlobal
from lobot_api.globals.SpawnListsGlobal import SpawnListsGlobal
from lobot_api.logger.Logger import SCRIPT
from lobot_api.packets.inventory.ItemMoveArgs import ItemMoveArgs
from lobot_api.packets.inventory.ItemMovementType import ItemMovementType
from lobot_api.packets.inventory.MoveBuy import MoveBuy
from lobot_api.packets.inventory.MoveItem import MoveItem
from lobot_api.packets.inventory.MoveItemErrorArgs import MoveItemErrorArgs
from lobot_api.packets.inventory.MovePickup import MovePickup
from lobot_api.packets.inventory.MoveSell import MoveSell
from lobot_api.packets.npc.NPCInteractionType import NPCInteractionType
from lobot_api.packets.npc.NPCSelect import NPCSelect
from lobot_api.statemachine.scriptingstates.NPCInteractionState import NPCInteractionState
from lobot_api.structs.EquipmentSlot import MAX_EQUIP_SLOTS
from lobot_api.structs.Item import Item
from lobot_api.structs.Monster import Monster
from lobot_api.structs.Shop import Shop
from lobot_api.structs.ShopItemEntry import ShopItemEntry
from silkroad_security_api_1_4.Packet import Packet


class ShoppingState(NPCInteractionState, ABC):
	item_movement_type_index: int = 7
	next_slot_to_sell = -1

	@property
	@abstractmethod
	def shopping_list(self) -> Dict[int, int]: pass

	@property
	def interaction_type(self) -> NPCInteractionType:
		return NPCInteractionType.BuySell

	@staticmethod
	def has_to_sell_items() -> bool:
		return any(key >= MAX_EQUIP_SLOTS and value.pk2_item.should_sell() for key, value in
		           Bot.char_data.inventory.items.items())

	@staticmethod
	def should_filter(p: Packet) -> bool:
		"""Determine whether the client should receive the packet or not. The bot still evaluates the result."""
		is_running: bool = Bot.get_status() is BotStatus.Botting
		is_interaction_packet: bool = p.opcode in [0xB04B, 0xB046, 0x3047, 0x3048, 0x3049]
		return Bot.is_interacting and is_running and is_interaction_packet

	def _handle_npc_selected_(self):
		if Bot.selected_npc.unique_id == self.current_target.unique_id:
			NPCSelect.on_selected_npc_changed -= self._handle_npc_selected_
			spawn_list = SpawnListsGlobal.AllSpawnEntries.get(Bot.selected_npc.unique_id, None)
			if Bot.selected_npc.ref_id in PK2DataGlobal.shops and spawn_list:
				# Continue with waiting for interaction to start
				# NPCOpenDialogue.on_interaction_started += self._handle_interaction_started_
				# proxy.joymax.send_packet(ScriptState.is_paused, NPCToggleInteraction(current_target.unique_id).create_request()) #.ConfigureAwait(false)
				# proxy.joymax.send_delayed_packet(NPCOpenDialogue(self.current_target.unique_id,
				#                                                  self.interaction_type).create_request(),
				#                                  self.NPC_DELAY_SEC)
				# No interaction needed: Continue with buying and selling
				self._loop_inner_()

	def _initialize_event_handlers_(self):
		Bot.on_is_interacting_changed += self._on_stop_interaction_
		# NPCOpenDialogue.on_interaction_started += self._buy_next_item_
		MoveItem.on_item_move_error += self._handle_item_move_error_
		MoveBuy.on_item_bought += self._handle_buy_item_
		MovePickup.on_item_picked_up += self._handle_buy_item_
		MoveSell.on_sold += self._handle_sell_item_
		proxy.add_server_hook(MoveItem.from_empty_constructor().server_opcode, MoveBuy.try_fake_packet)

	def _remove_event_handlers_(self):
		Bot.on_is_interacting_changed -= self._on_stop_interaction_
		# NPCOpenDialogue.on_interaction_started -= self._buy_next_item_
		MoveItem.on_item_move_error -= self._handle_item_move_error_
		MoveBuy.on_item_bought -= self._handle_buy_item_
		MovePickup.on_item_picked_up -= self._handle_buy_item_
		MoveSell.on_sold -= self._handle_sell_item_
		proxy.remove_server_hook(MoveItem.from_empty_constructor().server_opcode)

	def _on_stop_interaction_(self, e):
		"""Usually called when npc selection dialogue is closed"""
		if not Bot.is_interacting:
			self.stop()
			sys.stdout.write(f"Stopped interaction with {self.current_target.name}\n")

	def _sell_next_item_(self):
		item: Item = next((value for key, value in Bot.char_data.inventory.items.items() if
		                   key >= MAX_EQUIP_SLOTS and value.pk2_item.should_sell()), None)
		if item:
			ShoppingState.next_slot_to_sell = item.slot
			proxy.joymax.send_delayed_packet(MoveItem(ItemMovementType.Sell, item.slot, 0, 0xFFFF & item.stack_count,
			                                          self.current_target.unique_id).create_request(),
			                                 self.NPC_DELAY)
			sys.stdout.write(f"[Shop]Sell {item.stack_count} {item.pk2_item.name}\n")
		else:
			# Start buying if nothing left to sell
			self._buy_next_item_()

	def _buy_next_item_(self):
		try:
			shop_spawn: Optional[Monster] = SpawnListsGlobal.NPCSpawns.get(int(self.current_target.unique_id), None)
			shop: Optional[Shop] = PK2DataGlobal.shops.get(shop_spawn.ref_id, None) if shop_spawn else None
			if not shop:
				raise ValueError(f"Shop selected but not found - {self.current_target.name}({self.current_target.ref_id})")
			item_id: int = next((key for key, value in self.shopping_list.items() if key in shop.item_entries and value > 0), 0)
			item: ShopItemEntry = shop.item_entries.get(item_id, None)
			required_stacks: int = self.shopping_list[item_id] if item_id > 0 else 0
			if (required_stacks > 0
					and shop and item_id != 0
					and Bot.char_data.inventory.size > len(Bot.char_data.inventory.items)
					and item):
				if required_stacks >= item.max_stacks:  # Has to be bought more than once
					proxy.joymax.send_delayed_packet(
						MoveItem(ItemMovementType.Buy, item.tab_number, item.slot_number,
						         0xFFFF & item.max_stacks).create_request(),
						self.NPC_DELAY)
					sys.stdout.write(
						f"[{shop.pk2_entry.name}]Buy {item.max_stacks} {PK2DataGlobal.items[item.item_id].name}\n")
				else:  # Can be bought once
					proxy.joymax.send_delayed_packet(
						MoveItem(ItemMovementType.Buy, item.tab_number, item.slot_number,
						         0xFFFF & required_stacks).create_request(),
						self.NPC_DELAY)
					sys.stdout.write(
						f"[{shop.pk2_entry.name}]Buy {required_stacks} {PK2DataGlobal.items[item.item_id].name}\n")
			else:
				# _stop_interaction_(e)
				# No need to stop interaction
				# Stack and sort bought items then continue with the walk script
				self._start_stacking_()
		except Exception as ex:
			logging.log(SCRIPT, f"Shopping error: {sys.exc_info()}")
			self._start_stacking_()

	def _handle_sell_item_(self, args: ItemMoveArgs):
		if args.type is ItemMovementType.Sell and args.from_slot == ShoppingState.next_slot_to_sell:
			ShoppingState.next_slot_to_sell = -1
			self._loop_inner_()

	def _handle_buy_item_(self, args: ItemMoveArgs):
		if args.type is ItemMovementType.Buy:
			self._loop_inner_()

	def _handle_item_move_error_(self, error: MoveItemErrorArgs):
		logging.log(SCRIPT, f"Shopping error: {error.error_type.name}")
		self._start_sorting_()

	def _loop_inner_(self):
		if ShoppingState.has_to_sell_items():
			self._sell_next_item_()
		else:
			self._buy_next_item_()
