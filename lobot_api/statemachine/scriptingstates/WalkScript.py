"""The state of executing the# get to the bot area."""
import logging

from lobot_api.Bot import Bot
from lobot_api.globals.settings.LoopSettings import ScriptSettings
from lobot_api.globals.settings.TrainingSettings import TrainingAreaSettings
from lobot_api.logger.Logger import SCRIPT
from lobot_api.packets.char.BuffEndHandler import BuffEndHandler
from lobot_api.packets.char.CharInfoHandler import CharInfoHandler
from lobot_api.packets.char.Movement import Movement
from lobot_api.packets.char.SkillHandler import SkillHandler
from lobot_api.packets.pet.UpdateRiderStateHandler import UpdateRiderStateHandler
from lobot_api.scripting import ScriptUtils
from lobot_api.scripting.ScriptExpression import CommandExpression, ExpressionParser
from lobot_api.statemachine.IState import IStateType
from lobot_api.statemachine.scriptingstates.ScriptState import ScriptState


class WalkScript(ScriptState):
	def _get_next_target_(self):
		raise NotImplementedError()

	def _initialize_event_handlers_(self):
		self.__initialize__()
		BuffEndHandler.on_buff_ended += self._handle_speed_buff_ended_
		UpdateRiderStateHandler.on_mount_or_dismount += self._handle_mount_or_dismount_horse_
		self._speed_buff_setup_()
		# self.timer_inner += self.RepeatCommand
		# self.timer += self.ExecuteNextCommand
		self.on_executed_script_command += self._loop_main_
		ScriptState.on_skipped_command += self._handle_skipped_command_
		ScriptState.on_executed_set_area_command += self.__handle_set_area__
		ScriptState.on_executed_move_command += self._handle_executed_move_command_
		Movement.on_botmovement_registered += self._handle_executed_move_command_
		ScriptState.on_executed_teleport_command += self._handle_teleport_command_
		CharInfoHandler.on_parsed_info += self.__delayed_start__
		ScriptState.delay_timer.timeout.connect(self.__delayed_start__)
		SkillHandler.on_teleport_skill_used += self._handle_teleport_skill_

	def _remove_event_handlers_(self):
		# Clean up if not paused
		if not ScriptState.is_paused:
			ScriptState._script_ = None
			ScriptState.command_index = -1

			self._speed_buff_clean_up_()
			BuffEndHandler.on_buff_ended -= self._handle_speed_buff_ended_
			UpdateRiderStateHandler.on_mount_or_dismount -= self._handle_mount_or_dismount_horse_
			# self.timer_inner +-= self.RepeatCommand
			# self.timer -= self.ExecuteNextCommand
			self.on_executed_script_command -= self._loop_main_
			ScriptState.on_skipped_command -= self._handle_skipped_command_
			ScriptState.on_executed_set_area_command -= self.__handle_set_area__
			ScriptState.on_executed_move_command -= self._handle_executed_move_command_
			Movement.on_botmovement_registered -= self._handle_executed_move_command_
			ScriptState.on_executed_teleport_command -= self._handle_teleport_command_
			CharInfoHandler.on_parsed_info -= self.__delayed_start__
			ScriptState.delay_timer.timeout.disconnect(self.__delayed_start__)
			SkillHandler.on_teleport_skill_used -= self._handle_teleport_skill_
			logging.log(SCRIPT, "Stopping script")

	def __initialize__(self):
		# skip initialization and continue with script if paused
		if ScriptState.is_paused:
			ScriptState.is_paused = False
			return

		# Check if close to training area
		if self._can_start_training_():
			return

		ScriptState._script_ = ExpressionParser.parse_script(ScriptSettings.TrainingScriptPath.value)
		ScriptState.command_index = ScriptUtils.index_of_closest_go_command(Bot.char_data.position, self._script_)
		if not ScriptState._script_ or len(ScriptState._script_) < 1:
			if ScriptState.command_index == -1:
				logging.log(SCRIPT, "Too far from next walk point")
			else:
				logging.log(SCRIPT, "No walkscript found")
			self.transition(IStateType.RETURN)
			return
		# TODO MetaExpression Enhancement
		# if ScriptUtils.MatchesRequirements(script.Where(expr => expr is MetaExpression):
		# {
		#    //Try to execute
		# }
		# else:
		# {
		#    //TODO: search script that fulfills the predicates given in the script
		# }
		# Get all commands after closest walkpoint
		ScriptState._script_ = [exp for exp in ScriptState._script_ if isinstance(exp, CommandExpression)][
		                     ScriptState.command_index:]
		# set to -1 for loop_inner to start with first item
		ScriptState.command_index = -1

		# Start with first command in list
		# RepeatCommand(None, None) #EventArgs.Empty)

	@staticmethod
	def ensure_script_ends_in_training_area():
		"""Make sure bot is always within training range after script stops.
		If last position does not end up in or close to an area,set to the current position.
		This is done to prevent the bot from just returning to town immediately."""
		if ScriptUtils.distance_to_closest_area() > ScriptUtils.MAX_GO_DISTANCE:
			TrainingAreaSettings.instance().area1.x = Bot.char_data.position.x
			TrainingAreaSettings.instance().area1.y = Bot.char_data.position.y

	def _loop_inner_(self):
		# Reached the end of script
		if not self.current_script_command:
			self._transition_to_next_script_()
			return
		try:
			self.execute(self.current_script_command)
		except NotImplementedError as ex:
			logging.log(SCRIPT, str(ex))
			self.timer_inner.stop()
			self.timer.stop()
			self.on_executed_script_command.notify()

	def _loop_main_(self):
		self.timer_inner.stop()
		self.timer.stop()
		ScriptState.command_index += 1

		if self._should_mount_():
			self._mount_horse_()
			return

		# Reached the end of script
		if not ScriptState._script_ \
				or not self._is_index_in_range_(ScriptState.command_index, len(ScriptState._script_)):
			next_state = self.get_next_state()
			# prevent endless loop
			if next_state is IStateType.WALK_SCRIPT or next_state is IStateType.TOWN_SCRIPT:
				self.ensure_script_ends_in_training_area()
				self.transition(IStateType.IDLE)
			else:
				self.transition(next_state)
			return
		try:
			self.execute(self.current_script_command)
		except NotImplementedError as ex:
			logging.log(SCRIPT, str(ex))
			self.on_executed_script_command.notify()
