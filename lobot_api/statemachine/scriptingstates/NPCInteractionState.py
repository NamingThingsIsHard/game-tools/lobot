import logging
import sys
from abc import abstractmethod
from typing import Optional

from lobot_api.Bot import Bot
from lobot_api.connection import proxy
from lobot_api.globals.PK2DataGlobal import PK2DataGlobal
from lobot_api.globals.SpawnListsGlobal import SpawnListsGlobal
from lobot_api.logger.Logger import SCRIPT
from lobot_api.packets.inventory.ItemMoveArgs import ItemMoveArgs
from lobot_api.packets.inventory.ItemMovementType import ItemMovementType
from lobot_api.packets.inventory.MoveInventoryToInventory import MoveInventoryToInventory
from lobot_api.packets.inventory.MoveItem import MoveItem
from lobot_api.packets.npc.NPCCloseDialogue import NPCCloseDialogue
from lobot_api.packets.npc.NPCInteractionType import NPCInteractionType
from lobot_api.packets.npc.NPCOpenDialogue import NPCOpenDialogue
from lobot_api.packets.npc.NPCSelect import NPCSelect
from lobot_api.statemachine.IState import IState
from lobot_api.statemachine.IState import IStateType
from lobot_api.structs import EquipmentSlot
from lobot_api.structs.Item import Item


# #class#for  any npc states.:
# These states require a chain of
# - Outerloop to send NPCSelection package when state starts()
# - Wait for correct NPC selection event
# - Wait for start NPC interaction event
# - Innerloop to start state-specific function (e.g. store item then Wait for item storage during loop)
#
# Example
# select
# # 38 00 00 00                                       8...............
# open store window
# # 38 00 00 00                                       8...............
# # 38 00 00 00                                       8...............
# 01                                                ................
# # 01                                                ................
# # 01                                                ................
# 01                                                ................
# close
# # 38 00 00 00                                       8...............
# # 01                                                ................


class NPCInteractionState(IState):
	"""Safety delay for npc actions in milliseconds. Should be used in subclasses to prevent spamming commands."""
	NPC_DELAY: int = 500
	"""Safety distance to select NPC. Attempting a selection past this distance might fail."""
	MAX_SELECT_DISTANCE: int = 50
	"""Item used in sort functions"""
	_current_item_to_sort_: Item = None

	@property
	@abstractmethod
	def npc_signature(self) -> str:
		"""The npc/shop string to searching for in spawn lists """
		pass

	@property
	@abstractmethod
	def interaction_type(self) -> NPCInteractionType:
		"""The corresponding type enum."""
		pass

	@staticmethod
	def _get_number_of_stacks_to_move_(from_slot: int, to_slot) -> int:
		stacks_available = Bot.char_data.inventory.items[to_slot].max_stacks - Bot.char_data.inventory.items[
			to_slot].stack_count
		if stacks_available > 0:
			return (Bot.char_data.inventory.items[from_slot].stack_count  # enough slots left
			        if stacks_available >= Bot.char_data.inventory.items[from_slot].stack_count
			        else Bot.char_data.inventory.items[to_slot].stack_count)  # insufficient slots
		else:
			return Bot.char_data.inventory.items[to_slot].max_stacks - Bot.char_data.inventory.items[to_slot].stack_count

	def _start_stacking_(self):
		"""Stack items then sort them """
		MoveInventoryToInventory.on_item_stacked += self._handle_inventory_item_stacked_
		self._handle_inventory_item_stacked_()

	def _stop_stacking_(self):
		MoveInventoryToInventory.on_item_stacked -= self._handle_inventory_item_stacked_
		self._start_sorting_()

	def _start_sorting_(self):
		MoveInventoryToInventory.on_item_moved += self._handle_inventory_item_sorted_
		self._handle_inventory_item_sorted_()

	def _stop_sorting_(self):
		MoveInventoryToInventory.on_item_moved -= self._handle_inventory_item_sorted_
		self._stop_interaction_()

	def _stack_item_(self, current_item: Item) -> bool:
		"""Attempt to stack item and return whether a stackable item was found or not."""
		for i in range(0, current_item.slot):
			# Find free slot to move or slot that can be stacked
			item = Bot.char_data.inventory.items.get(i, None)
			if (item and item.ref_id == current_item.ref_id
					and item.stack_count < item.max_stacks):  # Stack
				stacks_to_move = 0xFFFF & self._get_number_of_stacks_to_move_(current_item.slot, i)
				command: MoveItem = MoveItem(ItemMovementType.InventoryToInventory, current_item.slot, i,
				                             stacks_to_move)
				proxy.joymax.send_delayed_packet(command.create_request(), NPCInteractionState.NPC_DELAY)
				return True
		return False

	def _sort_item_(self, current_item: Item):
		for i in range(EquipmentSlot.MAX_EQUIP_SLOTS, current_item.slot):
			# Find free slot to move or slot that can be stacked
			if i not in Bot.char_data.inventory.items:  # Move
				command: MoveItem = MoveItem(ItemMovementType.InventoryToInventory, current_item.slot, i,
				                             0xFFFF & current_item.stack_count)
				proxy.joymax.send_delayed_packet(command.create_request(), NPCInteractionState.NPC_DELAY)
				return
		self._stop_sorting_()

	def _handle_inventory_item_stacked_(self, args: Optional[ItemMoveArgs] = None):
		for i in range(Bot.char_data.inventory.size, EquipmentSlot.MAX_EQUIP_SLOTS, -1):
			item = Bot.char_data.inventory.items.get(i, None)
			if item and self._stack_item_(item):
				return
		# continue with sorting
		self._stop_stacking_()

	def _handle_inventory_item_sorted_(self, args: Optional[ItemMoveArgs] = None):
		for i in range(Bot.char_data.inventory.size, EquipmentSlot.MAX_EQUIP_SLOTS, -1):
			item: Item = Bot.char_data.inventory.items.get(i, None)
			if item:
				self._sort_item_(item)
				return
		# stop interaction
		self._stop_sorting_()

	def _handle_npc_selected_(self):
		if Bot.selected_npc.unique_id == IState.current_target.unique_id:
			NPCSelect.on_selected_npc_changed -= self._handle_npc_selected_
			# No need to wait for interaction: selection is enough to continue with script
			self._loop_inner_()

	def _handle_interaction_started_(self):
		NPCOpenDialogue.on_interaction_started -= self._handle_interaction_started_
		# Start state-specific NPC function
		self._loop_inner_()

	def __handle_interaction_stopped__(self):
		NPCCloseDialogue.on_dialogue_closed -= self.__handle_interaction_stopped__
		sys.stdout.write(f"Stopped interaction with {IState.current_target.name}\n")
		self.transition(IStateType.TOWN_SCRIPT)

	# TODO call "Transition" and make this function fire. Suddenly having to# switch functions is confusing

	def _stop_interaction_(self):
		"""this function replaces "Transition" in subclasses"""
		# Remove previous eventhandler and stop interaction before returning to script
		NPCCloseDialogue.on_dialogue_closed += self.__handle_interaction_stopped__
		proxy.joymax.send_delayed_packet(NPCCloseDialogue(IState.current_target.unique_id).create_request(),
		                                 NPCInteractionState.NPC_DELAY)

	def _loop_main_(self):
		# Find first npc to select from spawnlist with matching shop type; based on the signature
		for key, spawn in SpawnListsGlobal.NPCSpawns.items():
			value = PK2DataGlobal.npcs.get(spawn.ref_id, None)
			if value and self.npc_signature in value.long_id:
				# Make sure npc is within selection distance, otherwise it is not safe to attempt an interaction
				if Bot.char_data.position.distance_point(spawn.position) > NPCInteractionState.MAX_SELECT_DISTANCE:
					# Proceed with next script command
					logging.log(SCRIPT,
					            f"NPC {spawn.name} too far to select -- {Bot.char_data.position.distance_point(spawn.position)} of max {NPCInteractionState.MAX_SELECT_DISTANCE} steps away")
					self.transition(IStateType.TOWN_SCRIPT)
					return
				NPCSelect.on_selected_npc_changed += self._handle_npc_selected_
				IState.current_target = spawn
				proxy.joymax.send_delayed_packet(NPCSelect(key).create_request(), NPCInteractionState.NPC_DELAY)
				return

		# Return to TownScript if nothing is found
		self.transition(IStateType.TOWN_SCRIPT)
