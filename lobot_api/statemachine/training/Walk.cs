﻿using LobotAPI;
using LobotAPI.Globals;
using LobotAPI.Globals.Settings;
using LobotAPI.Packets;
using LobotAPI.Packets.Char;
using LobotAPI.Scripting;
using LobotAPI.Structs;
using LobotDLL.Connection;
using LobotDLL.Packets.Char;
using LobotDLL.Packets.Spawn;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace LobotDLL.StateMachine.Training
{
    public class Walk : BotState
    {
        private const int RepetitionInterval = 2000;
        private static Random rand = new Random();
        private static Point LastWalkPoint = default(Point);
        private static Point CurrentWalkPoint = default(Point);

        public CancellationTokenSource Cancellation { get; set; }

        public Walk() : base()
        {
            TrainingAreaSettings.AreaChanged += OnAreaChanged; // make sure future area changes are handled
        }

        private bool ShouldSwitchToAttackMode(Point destination, ISpawnType spawn)
        {
            return spawn is Monster && ScriptUtils.IsPointInAnyArea(destination);
        }

        private void HandleStopMovement(object sender, EventArgs e)
        {
            // Switch to next target
            OuterLoopFunction(null, EventArgs.Empty);
        }

        private void HandleSpawnMovement(object sender, CharMoveArgs e)
        {
            if (SpawnListsGlobal.TryGetSpawnObject(e.UniqueID, out ISpawnType spawn)
                && ShouldSwitchToAttackMode(e.Destination, spawn))
            {
                Transition(Attack, spawn);
            }
        }

        private void HandleMonsterSpawn(object sender, SpawnArgs e)
        {
            if (e.Spawn is Monster && e.List == SpawnListsGlobal.SpawnList.MonsterSpawns
                && IsMonsterValidTarget(e.Spawn as Monster))
            {
                Transition(Attack, e.Spawn);
            }
        }

        private void HandleDropSpawn(object sender, SpawnArgs e)
        {
            if (e.Spawn is ItemDrop
                && IsDropValidTarget(e.Spawn as ItemDrop))
            {
                if (Bot.PickupPet.UniqueID != 0 && !PetPickup.IsRunning)
                {
                    Task.Run(() =>
                    {
                        PetPickup.Start();
                    }).ConfigureAwait(false);
                }
                else
                {
                    Transition(Pickup, e.Spawn);
                }
            }
        }

        protected override void GetNextTarget()
        {
            InnerLoop.Stop();
            OuterLoop.Stop();

            HashSet<Point> pointSet;

            if (ScriptUtils.ClosestAreaToChar.AdjacencyList.Keys.Count == 0)
            {
                CurrentWalkPoint = ScriptUtils.ClosestAreaToChar.GetCenterPoint();
                return;
            }

            // Check if walk point has not been initialized yet or walk point is in a distant area
            if (CurrentWalkPoint == default(Point) || !ScriptUtils.IsPointInClosestArea(Bot.CharData.position))
            {
                List<Point> pointList = ScriptUtils.ClosestAreaToChar.AdjacencyList.Keys.ToList();
                int pointIndex = ScriptUtils.IndexOfClosestPoint(Bot.CharData.position, pointList);

                // Check if index of closest point is valid
                if (pointIndex < 0 || pointIndex >= pointList.Count)
                {
                    // take center if index is out of bounds
                    CurrentWalkPoint = ScriptUtils.ClosestAreaToChar.GetCenterPoint();
                }
                else
                {
                    // do not backtrack if more than one walk point is available
                    // get next point instead
                    if (pointList[pointIndex] == LastWalkPoint && pointList.Count > 1)
                    {
                        pointIndex = pointList.Count % (pointIndex + 1);
                    }

                    CurrentWalkPoint = pointList[pointIndex];
                }
                return;
            }
            else
            {
                // update last walk point
                LastWalkPoint = CurrentWalkPoint;

                // Make sure  the center of the current area is chosen if the walk point is not part of the adjacency list
                if (!ScriptUtils.ClosestAreaToChar.AdjacencyList.TryGetValue(CurrentWalkPoint, out pointSet))
                {
                    pointSet = ScriptUtils.ClosestAreaToChar.AdjacencyList[ScriptUtils.ClosestAreaToChar.GetCenterPoint()];
                }

                // Check for number of surrounding points (might be 0 and create error if area radius is small enough)
                // otherwise get next random point
                if (pointSet.Count < 1)
                {
                    CurrentWalkPoint = ScriptUtils.ClosestAreaToChar.GetCenterPoint();
                }
                else
                {
                    int randomIndex = rand.Next(pointSet.Count);

                    // do not backtrack if more than one walk point is available
                    // get next point instead
                    if (pointSet.ElementAt(randomIndex) == LastWalkPoint && pointSet.Count > 1)
                    {
                        randomIndex = pointSet.Count % (randomIndex + 1);
                    }

                    CurrentWalkPoint = pointSet.ElementAt(randomIndex);
                }
            }
        }

        protected override void InnerLoopFunction(object sender, EventArgs e)
        {
            Proxy.Instance.SendCommandAG(new CharMove(CurrentWalkPoint));
        }

        protected override void OuterLoopFunction(object sender, EventArgs e)
        {
            GetNextTarget();
            TimeSpan walkTime = TimeSpan.FromSeconds(ScriptUtils.GetWalkTime(Bot.CharData.position, CurrentWalkPoint));

            if (walkTime.TotalMilliseconds == 0)
            {
                if (ScriptUtils.ClosestAreaToChar.AdjacencyList.Keys.Count < 2) // Single walkpoint -> stay at point
                {
                    walkTime = TimeSpan.FromSeconds(10);
                }
                else
                {
                    walkTime = TimeSpan.FromMilliseconds(250);
                }
            }

            Proxy.Instance.SendCommandAG(new CharMove(CurrentWalkPoint));

            InnerLoop.Interval = RepetitionInterval;
            OuterLoop.Interval = walkTime.TotalMilliseconds;

            InnerLoop.Start();
            OuterLoop.Start();
        }

        protected override void InitializeEventHandlers()
        {
            CharMove.SpawnMovementRegistered += HandleSpawnMovement;
            ParseMonster.Parsed += HandleMonsterSpawn;
            ParseItem.Parsed += HandleDropSpawn;
            CharStopMovementHandler.CharStopMovement += HandleStopMovement;
            SkillHandler.TeleportSkillUsed += HandleTeleportSkill;
            UseSkillHandler.DrewMonsterAggro += HandleDrewAggro;
            UseSkillHandler.DrewMonsterAggroPet += HandleDrewAggroPet;
        }

        private void HandleDrewAggro(object sender, MonsterAggroArgs e)
        {
            if (!GeneralTrainingSettings.DontAttack && ShouldSwitchToAttackMode(e.Monster.position, e.Monster))
            {
                Transition(Attack, e.Monster);
            }
        }

        private void HandleDrewAggroPet(object sender, MonsterAggroArgs e)
        {
            if (GeneralTrainingSettings.DefendPet && ShouldSwitchToAttackMode(e.Monster.position, e.Monster))
            {
                Transition(Attack, e.Monster);
            }
        }

        protected override void RemoveEventHandlers()
        {
            CharMove.SpawnMovementRegistered -= HandleSpawnMovement;
            ParseMonster.Parsed -= HandleMonsterSpawn;
            ParseItem.Parsed -= HandleDropSpawn;
            CharStopMovementHandler.CharStopMovement -= HandleStopMovement;
            SkillHandler.TeleportSkillUsed -= HandleTeleportSkill;
            UseSkillHandler.DrewMonsterAggro -= HandleDrewAggro;
            UseSkillHandler.DrewMonsterAggroPet -= HandleDrewAggroPet;

            //CurrentTarget = null;
            CurrentWalkPoint = default(Point);
        }

        private void OnAreaChanged(object sender, AreaChangedArgs e)
        {
            //TODO cover case of being too far away from training spot
            //Stop(new StrategyChangeArgs(typeof(Return)));

            GetNextTarget();
        }
    }
}
