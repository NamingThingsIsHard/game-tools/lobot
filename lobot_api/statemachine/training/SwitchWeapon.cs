﻿using LobotAPI.Globals;
using LobotAPI.Packets.Inventory;
using LobotAPI.PK2;
using LobotAPI.StateMachine;
using LobotAPI.Structs;
using LobotDLL.Connection;
using LobotDLL.Packets.Inventory;
using System;

namespace LobotDLL.StateMachine.Training
{
    public class SwitchWeapon : BotState
    {
        /// <summary>
        /// The weapon to be switched next
        /// </summary>
        public static Equipment WeaponToEquip;

        protected override void GetNextTarget()
        {
            throw new NotImplementedException();
        }

        protected override void InitializeEventHandlers()
        {
            MoveInventoryToInventory.ItemSwitched += HandleItemSwitch;
            MoveInventoryToInventory.ItemMoved += HandleItemMove;
        }

        protected override void RemoveEventHandlers()
        {
            MoveInventoryToInventory.ItemSwitched -= HandleItemSwitch;
            MoveInventoryToInventory.ItemMoved -= HandleItemMove;
        }

        protected bool TrySwitchWeapons()
        {
            if (WeaponToEquip == null)
            {
                return false;
            }
            else
            {
                EquipmentSlot toSlot = WeaponToEquip.type.GetRequiredWeaponSlot();
                Proxy.Instance.SendCommandAG(new MoveItem(ItemMovementType.InventoryToInventory, WeaponToEquip.Slot, (byte)toSlot));
                return true;
            }
        }

        protected virtual void HandleItemMove(object sender, ItemMoveArgs e)
        {
            //Moved items have an updated slot, so the weapon to equip is on the ToSlot
            if (e.ToSlot == WeaponToEquip.Slot)
            {
                HandleInventoryChange(e);
            }
        }

        protected virtual void HandleItemSwitch(object sender, ItemMoveArgs e)
        {
            //Switched items are reinstantiated to update the collection, so the reference to the weapon to equip stays on the from slot
            if (e.FromSlot == WeaponToEquip.Slot)
            {
                HandleInventoryChange(e);
            }
        }

        protected virtual void HandleInventoryChange(ItemMoveArgs e)
        {
            System.Diagnostics.Debug.WriteLine($"Equipped {WeaponToEquip.type}");
            OuterLoop.Stop();

            // check if a onehand-weapon was equipped: This usually means the shield was not included
            // then send a packet to equip a shield, if any is found
            if (e.ToSlot == (byte)EquipmentSlot.Primary
                && WeaponToEquip.type.IsWeaponWithShield()
                && !CharInfoGlobal.Inventory.TryGetValue((byte)EquipmentSlot.Secondary, out Item equippedShield))
            {
                Pk2ItemType shieldType = WeaponToEquip.Pk2Item.Race == Race.Chinese
                    ? Pk2ItemType.ShieldCH
                    : Pk2ItemType.ShieldEU;

                // find shield and equip it
                if (TryFindWeaponToSwitch(shieldType, out Equipment shieldToEquip))
                {
                    Proxy.Instance.SendCommandAG(new MoveItem(ItemMovementType.InventoryToInventory, shieldToEquip.Slot, (byte)EquipmentSlot.Secondary));

                    System.Diagnostics.Debug.WriteLine($"Equipping {shieldToEquip.type}");
                    //Logger.Log(LogLevel.BOTTING, $"");

                }
            }

            // reset weapon
            WeaponToEquip = null;

            // exit state
            Transition(BotStateMachine.Instance.PrevState);
        }

        protected override void InnerLoopFunction(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        protected override void OuterLoopFunction(object sender, EventArgs e)
        {
            OuterLoop.Stop();

            if (WeaponToEquip == null)
            {
                Transition(GetNextState());
            }

            if (TrySwitchWeapons())
            {
                OuterLoop.Start();
            }
            else
            {
                Transition(GetNextState());
            }
        }
    }
}
