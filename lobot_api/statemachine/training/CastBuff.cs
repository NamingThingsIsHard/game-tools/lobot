﻿using LobotAPI;
using LobotAPI.Globals.Settings;
using LobotAPI.Packets.Char;
using LobotAPI.Structs;
using LobotDLL.Connection;
using LobotDLL.Packets;
using LobotDLL.Packets.Char;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace LobotDLL.StateMachine.Training
{
    public class CastBuff : BotState
    {
        private void OnBuffAdd(object sender, BuffArgs e)
        {
            if (e.RefID == CurrentSkill?.refskill_id)
            {
                InnerLoop.Stop();

                if (IsCastingSpeedBuff)
                {
                    Transition(GetNextState());
                    return;
                }

                // Wait until cast time before continuing with next skill
                OuterLoop.Interval = CurrentSkill.CastTime > 0 ? CurrentSkill.CastTime : 1;
                OuterLoop.Start();
            }
        }

        protected override void InnerLoopFunction(object sender, EventArgs e)
        {
            if (CurrentSkill != null && CurrentSkill?.SkillEnabled == 0x01)
            {
                System.Diagnostics.Debug.WriteLine(string.Format($"Cast Buff : {CurrentSkill.Name}"));
                Proxy.Instance.SendCommandAG(new UseSkill(CurrentSkill.refskill_id, ObjectActionType.UseSkill));
            }
        }

        protected override void OuterLoopFunction(object sender, EventArgs e)
        {
            OuterLoop.Stop();

            GetNextTarget();

            if (CurrentSkill == null)
            {
                Transition(GetNextState());
                return;
            }

            UseSkillOrSwitchWeapon(() => GetNextTarget());
        }



        protected override void InitializeEventHandlers()
        {
            BuffAddHandler.BuffAdded += OnBuffAdd;

            // Use character standing  to buff for transferring items in pickup pet
            // TODO enhancement could prevent this if only speed buff has to be cast (could potentially stop walking for casting a skill that doesn't require stopping like grass walk)
            if (Bot.PickupPet.UniqueID != 0)
            {
                Task.Run(() =>
                {
                    PetItemTransfer.Start();
                }).ConfigureAwait(false);
            }
        }

        protected override void RemoveEventHandlers()
        {
            BuffAddHandler.BuffAdded -= OnBuffAdd;

            CurrentSkill = null;
            // Prevent moving items from interrupt character from moving
            PetItemTransfer.Stop();
        }

        protected override void GetNextTarget()
        {
            InnerLoop.Stop();

            // Just dropping by for some speed
            if (IsCastingSpeedBuff)
            {
                CurrentSkill = SkillSettings.SpeedBuff;
                return;
            }

            int monsterTypeIndex = CurrentTarget != null
                ? SkillSettings.MonsterTypes.IndexOf((CurrentTarget as Monster).type)
                : 0;

            // get a skill that is off cooldown and not in the active skill buff list
            CurrentSkill = SkillSettings.BuffSkills[monsterTypeIndex].FirstOrDefault(s => CanBuffBeCast(s));

            // Cast speed buff last, if one is set
            if (ShouldCastSpeedBuff())
            {
                CurrentSkill = SkillSettings.SpeedBuff;
            }
        }
    }
}
