from lobot_api.connection import proxy
from lobot_api.globals.SpawnListsGlobal import SpawnListsGlobal
from lobot_api.globals.settings.SkillSettings import SkillSettings
from lobot_api.packets.char.ObjectActionType import ObjectActionType
from lobot_api.packets.char.SkillArgs import SkillArgs
from lobot_api.packets.char.UseSkill import UseSkill
from lobot_api.packets.char.UseSkillHandler import UseSkillHandler
from lobot_api.statemachine.IState import IState


class Resurrect(IState):
	def __on_skill_cast__(self, e: SkillArgs):
		if e.skill.ref_id == self.current_skill.ref_id:
			self.timer_inner.stop()
			# TODO ignore target for X seconds to not remain in loop resurrecting
			# Wait until cast time before continuing with next skill
			self.timer.setInterval(max(e.skill.cast_time, 500))
			self.timer.start()

	def _loop_inner_(self):
		if self.current_skill is None:
			return
		if self.current_skill is not None and self.can_skill_be_cast(self.current_skill):
			# sys.stdout.write(str.format(f"Cast Buff : {self.current_skill.name}\n"))
			proxy.joymax.send_packet(
				UseSkill(self.current_skill.ref_id, ObjectActionType.UseSkill, IState.current_target.unique_id).create_request())

	def _loop_main_(self):
		self.timer.stop()
		self._get_next_target_()
		self._get_next_skill_()
		if self.current_skill is None or IState.current_target is None:
			self.transition(self.get_next_state())
			return
		self._use_skill_or_switch_weapon_(self._get_next_skill_)

	def _initialize_event_handlers_(self):
		UseSkillHandler.on_skill_cast += self.__on_skill_cast__

	def _remove_event_handlers_(self):
		UseSkillHandler.on_skill_cast -= self.__on_skill_cast__

	def _get_next_target_(self):
		""" get target that is dead"""
		IState.current_target = next((p for p in SpawnListsGlobal.Players.values() if not p.is_alive), None)

	def _get_next_skill_(self):
		"""get a skill that is off cooldown and not in the active skill buff list"""
		if self.can_skill_be_cast(SkillSettings.res_spell):
			self.current_skill = SkillSettings.res_spell
		else:
			self.current_skill = None
