﻿using LobotAPI;
using LobotAPI.Globals;
using LobotAPI.PK2;
using LobotAPI.Structs;
using LobotDLL.Connection;
using LobotDLL.Packets;
using LobotDLL.Packets.Char;
using LobotDLL.Packets.Inventory;
using System;
using System.Linq;

namespace LobotDLL.StateMachine.Training
{
    public class Return : BotState
    {
        private Item ReturnScroll;

        protected override void GetNextTarget()
        {
            //
        }

        protected override void InitializeEventHandlers()
        {
            CharDataHandler.ParsedCharData += HandleRespawned;
        }

        protected override void RemoveEventHandlers()
        {
            CharDataHandler.ParsedCharData -= HandleRespawned;
            ReturnScroll = null;
        }

        private void HandleRespawned(object sender, EventArgs e)
        {
            Transition(GetNextState());
        }

        protected override void InnerLoopFunction(object sender, EventArgs e)
        {
            //
        }

        private Item FindReturnScroll()
        {
            return CharInfoGlobal.Inventory.Values.FirstOrDefault(i => i.type == Pk2ItemType.ReturnScroll)
                 ?? CharInfoGlobal.Inventory.Values.FirstOrDefault(i => i.type == Pk2ItemType.ReverseReturnScroll);
        }

        protected override void OuterLoopFunction(object sender, EventArgs e)
        {
            if (Bot.CharData.is_alive)
            {
                if (ReturnScroll == null)
                {
                    ReturnScroll = FindReturnScroll();
                }

                // TODO --> walk all the way back to town
                if (ReturnScroll == null)
                {
                    System.Diagnostics.Debug.WriteLine($"Return: No (reverse)return scroll found!");
                    Logger.Log(LogLevel.BOTTING, $"Return: No (reverse)return scroll found!");
                }
                else
                {
                    if (ReturnScroll.type == Pk2ItemType.ReverseReturnScroll)
                    {
                        System.Diagnostics.Debug.WriteLine($"Return: Only reverse return scroll found -> Buying return scoll in town");
                        Logger.Log(LogLevel.BOTTING, $"Return: Only reverse return scroll found -> Buying return scoll in town");
                    }

                    Proxy.Instance.SendCommandAG(new UseItem(ReturnScroll.Slot));
                }
            }
            else
            {
                if (Bot.CharData.Level < 11)
                {
                    Proxy.Instance.SendCommandAG(new CharChooseDeathOption(CharDeathOption.CurrentSpot));
                }
                else
                {
                    Proxy.Instance.SendCommandAG(new CharChooseDeathOption(CharDeathOption.SpecifiedSpot));
                }
            }


            //OuterLoop.Start();
        }
    }
}
