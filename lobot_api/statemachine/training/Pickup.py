from PyQt5.QtCore import QTimer

from lobot_api.Bot import Bot
from lobot_api.connection import proxy
from lobot_api.globals.SpawnListsGlobal import SpawnListsGlobal, SpawnList
from lobot_api.globals.settings.TrainingSettings import GeneralTrainingSettings
from lobot_api.packets.char.CharStopMovementArgs import CharStopMovementArgs
from lobot_api.packets.char.CharStopMovementHandler import CharStopMovementHandler
from lobot_api.packets.char.PickupItem import PickupItem
from lobot_api.packets.char.SkillHandler import SkillHandler
from lobot_api.packets.inventory.MoveItem import MoveItem
from lobot_api.packets.inventory.MoveItemError import MoveItemError
from lobot_api.packets.inventory.MoveItemErrorArgs import MoveItemErrorArgs
from lobot_api.packets.npc.DespawnArgs import DespawnArgs
from lobot_api.packets.pet.PetAction import PetAction
from lobot_api.packets.spawn.SpawnHandler import SpawnHandler
from lobot_api.statemachine.IState import IState, IStateType
from lobot_api.structs.Monster import Monster


class Pickup(IState):
	# # TODO use this later for a slight delay between picks (not for first pick)
	# PICK_COMMAND_DELAY = 500
	"""Minimum distance a monster should be from a drop to safely pick."""
	MIN_MOB_DISTANCE = 6
	"""Amount of times character has stopped movement after packet B023"""

	__interruption_count__: int = 0

	def __handle_stop_movement__(self, e: CharStopMovementArgs):
		# Determine if character stopped to pick item (distance > 5 ) or got stopped on the way there
		if e.unique_id == Bot.char_data.unique_id and IState.current_target is not None and Bot.char_data.position.distance_point(
				IState.current_target.position) > 5:
			self.__interruption_count__ += 1
		if self.__interruption_count__ > 3:
			# ignore item for 15s
			IState.current_target.is_ignored = True
			# Switch to next target
			self._loop_main_()  # EventArgs.Empty)

	def __handle_item_move_error__(self, e: MoveItemErrorArgs):
		# switch (e.ErrorType)
		if e.error_type == MoveItemError.CannotFind:
			found, list_number = IState.current_target is not None and SpawnListsGlobal.AllSpawnEntries.get(
				IState.current_target.unique_id, None)
			if found:
				SpawnListsGlobal.remove_from_bot_list(IState.current_target.unique_id, list_number)
			IState.current_target = None
			self._loop_main_()
			pass
		elif e.error_type == MoveItemError.ShopNotOpen:
			# TODO handle in shop window
			pass
		elif e.error_type == MoveItemError.QuestFinished:
			if IState.current_target is not None:
				IState.current_target.is_ignored = True
			IState.current_target = None
			self._loop_main_()
			# TODO return after X quests finished
			# if GeneralTrainingSettings.instance().ReturnQuestsFinished:
			# {
			#  transition(Return)
			# }
			pass
		elif e.error_type == MoveItemError.InventoryFull:
			if GeneralTrainingSettings.instance().return_inventory_full:
				self.transition(IStateType.RETURN)
			pass
		else:
			pass

	def _handle_despawn_(self, e: DespawnArgs):
		if IState.current_target and e.list_type is SpawnList.Drops and IState.current_target.unique_id == e.unique_id:
			IState.current_target = None  # clean up for next target
			self._loop_main_()

	def _get_next_target_(self):
		# reset counter
		self.__interruption_count__ = 0
		for drop in SpawnListsGlobal.Drops.values():
			if self.is_drop_valid_target(drop):
				# get closest drop
				if (IState.current_target is None
					or (IState.current_target is not None
						and Bot.char_data.position.distance_point(
							IState.current_target.position) > Bot.char_data.position.distance_point(
								drop.position))
				):
					IState.current_target = drop

	def _loop_inner_(self):
		# get next item once
		if IState.current_target is None:
			self._get_next_target_()
		# switch if still no item found
		if IState.current_target is None:
			self.transition(self.get_next_state())
			return
		else:
			# Use pet to pickup if
			if Bot.pickup_pet.unique_id != 0:  # and Bot.pickup_pet.Inventory is not full
				proxy.joymax.send_packet(PetAction(IState.current_target.unique_id).create_request())
			else:
				proxy.joymax.send_packet(PickupItem(IState.current_target.unique_id).create_request())

	def try_get_monster_next_to_drop(self) -> (bool, Monster):
		for key, value in SpawnListsGlobal.MonsterSpawns.items():
			if IState.current_target is None:
				break
			# find first monster within range of the item
			if value.is_alive and value.position.distance_point(IState.current_target.position) < self.MIN_MOB_DISTANCE:
				monster = value
				return True, monster
		return False, None

	def _initialize_event_handlers_(self):
		SpawnHandler.on_despawned += self._handle_despawn_
		SkillHandler.on_teleport_skill_used += self._handle_teleport_skill_
		CharStopMovementHandler.on_char_stop_movement += self.__handle_stop_movement__
		MoveItem.on_item_move_error += self.__handle_item_move_error__

	def _remove_event_handlers_(self):
		SpawnHandler.on_despawned -= self._handle_despawn_
		SkillHandler.on_teleport_skill_used -= self._handle_teleport_skill_
		CharStopMovementHandler.on_char_stop_movement -= self.__handle_stop_movement__
		MoveItem.on_item_move_error -= self.__handle_item_move_error__

	def _loop_main_(self):
		self.timer_inner.stop()
		self.timer.stop()
		self._get_next_target_()
		if IState.current_target is None:
			next_state = self.get_next_state()
			if next_state is IStateType.PICKUP:
				self._loop_main_()  # EventArgs.Empty)# causing random transition loop exceptions
				return
			else:
				self.transition(next_state)
				return
		# Kill monsters around item if it is not valuable(equipment)
		found, monster = self.try_get_monster_next_to_drop()
		if not IState.current_target.pk2_item.is_equipment_item() and found:
			# Make# set during transition
			self.transition(IStateType.ATTACK, monster)
			return
		# Send pickup packet once, then send the packet on a loop, until item despawns
		self._loop_inner_()
		self.timer_inner.start()
