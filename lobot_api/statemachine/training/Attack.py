import logging
import sys
from builtins import int
from itertools import cycle
from typing import List, Optional, Iterable, Iterator

from PyQt5.QtCore import QTimer

from lobot_api.Bot import Bot
from lobot_api.connection import proxy
from lobot_api.globals.PK2DataGlobal import PK2DataGlobal
from lobot_api.globals.SpawnListsGlobal import SpawnListsGlobal, SpawnList
from lobot_api.globals.settings.SkillSettings import SkillSettings
from lobot_api.globals.settings.TrainingSettings import GeneralTrainingSettings
from lobot_api.logger.Logger import BOTTING
from lobot_api.packets.autopotion.StatusUpdateArgs import StatusUpdateArgs
from lobot_api.packets.char.BuffArgs import BuffArgs
from lobot_api.packets.char.BuffEndHandler import BuffEndHandler
from lobot_api.packets.char.CharStopMovementArgs import CharStopMovementArgs
from lobot_api.packets.char.CharStopMovementHandler import CharStopMovementHandler
from lobot_api.packets.char.CharValueUpdateHandler import CharValueUpdateHandler
from lobot_api.packets.char.HPMPUpdateHandler import HPMPUpdateHandler
from lobot_api.packets.char.MonsterAggroArgs import MonsterAggroArgs
from lobot_api.packets.char.ObjectActionType import ObjectActionType
from lobot_api.packets.char.SkillArgs import SkillArgs
from lobot_api.packets.char.SkillErrorArgs import SkillErrorArgs
from lobot_api.packets.char.SkillErrorType import SkillErrorType
from lobot_api.packets.char.SkillHandler import SkillHandler
from lobot_api.packets.char.UseImbue import UseImbue
from lobot_api.packets.char.UseSkill import UseSkill
from lobot_api.packets.char.UseSkillHandler import UseSkillHandler
from lobot_api.packets.char.UseZerk import UseZerk
from lobot_api.packets.char.WarlockDOTAddHandler import WarlockDOTAddHandler
from lobot_api.packets.inventory.ItemMoveArgs import ItemMoveArgs
from lobot_api.packets.inventory.ItemMovementType import ItemMovementType
from lobot_api.packets.inventory.MoveInventoryToInventory import MoveInventoryToInventory
from lobot_api.packets.inventory.MoveItem import MoveItem
from lobot_api.packets.npc.DespawnArgs import DespawnArgs
from lobot_api.packets.npc.NPCSelect import NPCSelect
from lobot_api.packets.npc.ObjectStatusHandler import ObjectStatusHandler
from lobot_api.packets.npc.SpawnArgs import SpawnArgs
from lobot_api.packets.spawn.SpawnHandler import SpawnHandler, ParseItem
from lobot_api.pk2.PK2Item import PK2ItemType
from lobot_api.pk2.PK2Skill import PK2SkillType
from lobot_api.scripting import ScriptUtils
from lobot_api.statemachine import PickupUsePet
from lobot_api.statemachine.IState import IState, IStateType
from lobot_api.structs.BadStatus import BadStatus
from lobot_api.structs.CharData import Skill
from lobot_api.structs.EquipmentSlot import EquipmentSlot
from lobot_api.structs.Item import Item
from lobot_api.structs.Monster import Monster


class Attack(IState):
	ZERK_MAX = 5

	task_imbue: QTimer = None
	target_selected: bool = False
	is_waiting_for_unstun = False
	skills_to_cycle: Iterator = []
	movement_interruption_count: int = 0

	def __init__(self):
		super().__init__()
		self.task_imbue = QTimer()
		self.task_imbue.setSingleShot(True)
		self.skills_to_cycle: Optional[Iterable] = []

	def __delayed_imbue_delete__(self, imbue: Skill):
		""" Remove any imbue that hasn't been deleted from the buff list after its duration is over."""
		def delete_cancellable():
			buff = [b for b in Bot.char_data.active_buffs.values() if b.ref_id == imbue.ref_id]
			for b in buff:
				Bot.char_data.active_buff_count -= 1
				Bot.char_data.active_buffs.pop(b.unique_id)

		# cancel previous scheduled buff deletion
		if self.task_imbue and not self.task_imbue.isActive():
			self.task_imbue.stop()
		# queue task to be run after delay
		QTimer.singleShot(imbue.pk2_skill.duration + 1000, delete_cancellable)

	@staticmethod
	def handle_char_stop_movement(self, e: CharStopMovementArgs):
		pass  # Cancellation.stop()

	def __handle_drop_spawn__(self, e: SpawnArgs):
		""" If drop is valid
		Send pet to pick
		or Switch to picking up if not under attack
		In a full inventory, gold or any drop that fits onto a stack of similar items is valid.
		:param e: Spawn args
		:return:
		"""
		if e.spawn_list is SpawnList.Drops and self.is_drop_valid_target(e.spawn):
			if Bot.pickup_pet and Bot.pickup_pet.unique_id != 0:
				if not PickupUsePet.is_running:
					PickupUsePet.start()
			elif not any(value.is_alive and value.target == Bot.char_data.unique_id for key, value in SpawnListsGlobal.MonsterSpawns.items()):
				self.transition(IStateType.PICKUP, e.spawn)

	def __on_stop_movement__(self, e: CharStopMovementArgs):
		# This should handle player input
		if IState.current_target is not None or e.unique_id != Bot.char_data.unique_id:
			return
		self.movement_interruption_count += 1
		# Make sure movement stop was caused by something outside of attack range to rule out self-root for skillcast
		weapon = Bot.char_data.inventory.items.get(EquipmentSlot.Primary.value, None)

		if weapon and IState.current_target:
			distance = Bot.char_data.position.distance_point(IState.current_target.position)
			if distance > weapon.type.get_attack_range() or self.movement_interruption_count > 5:
				# ignore monster for 15s
				if IState.current_target:
					IState.current_target.is_ignored = True
				# Switch to next target
				self.target_selected = False
				IState.current_target = None
				self._loop_main_()

	def __should_cast_strong_buff__(self, buff_ended_id):
		return (isinstance(IState.current_target, Monster)
		        and IState.current_target.type.value > 1
		        and any(s.ref_id == buff_ended_id and IState.can_buff_be_cast(s) for s in SkillSettings.BuffSkillsStrong))

	def __on_buff_ended__(self, e: BuffArgs):
		if e.ref_id == SkillSettings.imbue.value and e.ref_id in PK2DataGlobal.skills:
			proxy.joymax.send_packet(UseImbue(SkillSettings.imbue.value).create_request())
			imbue = Skill(SkillSettings.imbue.value)
			self.__delayed_imbue_delete__(imbue)
		# backup in		elif MovementOption == no packet for imbue cancellation arrives
		elif (any(s.ref_id == e.ref_id for s in SkillSettings.BuffSkillsWeak)  # Any buff to be recast
			or (not SkillSettings.speed_buff_or_pot.value and e.ref_id == SkillSettings.speed_buff.value)
			or self.__should_cast_strong_buff__(e.ref_id)):
			self.transition(IStateType.CAST_BUFF, IState.current_target)

	def __handle_dot_added__(self, e: BuffArgs):
		"""
		Switch target if target is stronger than champion and has 2 DOT.
		:return: None
		"""
		if IState.current_target and e.target_id == IState.current_target.unique_id \
			and IState.current_target.type.value < 2 \
			and sum(1 for b in IState.current_target.buffs.values() if b.pk2_skill.type is PK2SkillType.WarlockDOT) > 1:
			IState.current_target = None
			self.target_selected = False

	def __on_skill_cast__(self, e: SkillArgs):
		if self.current_skill and e.skill.ref_id == self.current_skill.ref_id:
			self.timer_inner.stop()
			self.timer.stop()
			self.movement_interruption_count = 0
			# Wait until cast time before continuing with next skill
			# Make outerloop switch target if current one is not a valid warlock target
			if IState.current_target and e.skill.type is PK2SkillType.WarlockDOT:
				self.timer.setInterval(self.current_skill.cast_time + 200 if self.current_skill.cast_time > 0 else 200)
			else:
				self.timer.setInterval(self.current_skill.cast_time + 100 if self.current_skill.cast_time > 0 else 100)
			self.timer.start()

	def __on_despawn__(self, e: DespawnArgs):
		if IState.current_target and e.list_type is SpawnList.MonsterSpawns and e.unique_id == IState.current_target.unique_id:
			# sys.stdout.write(f"Despawn {hex(IState.current_target.unique_id)}\n")
			self.target_selected = False
			IState.current_target = None
			self._loop_main_()

	def __on_monster_updated__(self, e: StatusUpdateArgs):
		if IState.current_target and e.unique_id == IState.current_target:
			pass
			# logging.log(BOTTING, f"e.hp: {IState.current_target.current_hp}")

	def __on_monster_died__(self, e: StatusUpdateArgs):
		if IState.current_target and IState.current_target.unique_id == e.unique_id:
			self.target_selected = False
			IState.current_target = None
			self._loop_main_()

	def __on_feared__(self):
		pass
		# TODO : switch targets if feared by target
		# (0x3057,encrypted=False,massive=False,buffer=[229, 228, 34, 0, 1, 1, 4, 0, 2, 0, 0, 2])

	def __on_stunned__(self):
		"""
		Wait for status to change back to normal using event handler.
		:return:
		"""
		if not self.is_waiting_for_unstun:
			self.is_waiting_for_unstun = True
			HPMPUpdateHandler.on_char_updated += self.__on_wait_unstunned__
			self.timer_inner.stop()
			self.timer.stop()

	def __on_wait_unstunned__(self, e: StatusUpdateArgs):
		if Bot.char_data.debuff_flags & BadStatus.Stun.value == 0:
			self.is_waiting_for_unstun = False
			HPMPUpdateHandler.on_char_updated -= self.__on_wait_unstunned__
			self._loop_main_()

	def __on_kb_kd__(self, is_disabled):
		"""
		Handler for knock down or knock back. Stops timer when disabled, starts timer again when disable is over.
		:param is_disabled: Character cannot move.
		:return:
		"""
		if is_disabled:
			self.timer.stop()
			self.timer_inner.stop()
			self.movement_interruption_count = 0
		else:
			self._loop_main_()

	def __handle_skill_error__(self, e: SkillErrorArgs):
		# switch (e.type)
		if e.type == SkillErrorType.Obstacle:
			pass
		elif e.type == SkillErrorType.WrongWeapon:
			weapon: Item = Bot.char_data.inventory.equipment.get(EquipmentSlot.Primary.value, None)
			if weapon:
				sys.stdout.write(
					f"Wrong weapon type equipped: {weapon.type}, needed: {self.current_skill.required_weapon.value}\n")
				logging.log(BOTTING,
					f"Wrong weapon type equipped: {weapon.type}, needed: {self.current_skill.required_weapon.value}")
			elif EquipmentSlot.Secondary.value in Bot.char_data.inventory.equipment.keys():
				shield: Item = Bot.char_data.inventory.equipment.get(EquipmentSlot.Secondary.value, None)
				logging.log(BOTTING,
					f"Wrong weapon type equipped: {shield.type}, needed: {self.current_skill.required_weapon.value}")
		elif e.type == SkillErrorType.SkillOnCooldown:
			pass
		elif e.type == SkillErrorType.InvalidTarget:
			if IState.current_target and isinstance(IState.current_target, Monster):
				# knocked down but non-knockdown skill used
				if IState.current_target.is_knocked_down and not self.current_skill.type.is_knock_down_skill():
					skill: Skill = self.get_first_valid_skill(IState.current_target.type)
					if skill is not None:
						self.current_skill = skill
						self._loop_inner_()
						return
				else:
					# sys.stdout.write(
					# 	f"Skill Error Type: {e.type}, {format(IState.current_target.unique_id, '02X')}--{IState.current_target.name} - Alive: {IState.current_target.is_alive}\n")
					IState.current_target.is_alive = False
			next_state = self.get_next_state()
			if next_state is IStateType.ATTACK:
				self.target_selected = False
				IState.current_target = None
				self._loop_main_()
			else:
				self.target_selected = False
				self.transition(next_state)
		elif e.type == SkillErrorType.NoBolts:
			# sys.stdout.write(f"Skill Error Type: {e.type}\n")
			bolts: Item = next((value.type is PK2ItemType.Bolt for key, value in Bot.char_data.inventory.items.items()),
			                   None)
			if bolts is not None:
				# sys.stdout.write(f"Equipping ammo from slot {bolts.slot}\n")
				command: MoveItem = MoveItem(ItemMovementType.InventoryToInventory, bolts.slot,
				                             EquipmentSlot.Secondary.value)
				MoveInventoryToInventory.on_item_moved += self.__handle_ammo_equipped__
				proxy.joymax.send_packet(command.create_request())
			else:
				sys.stdout.write(f"No ammo found. Returning to town\n")
			self.transition(IStateType.RETURN)
		else:
			pass

	def __handle_ammo_equipped__(self, e: ItemMoveArgs):
		value: Item = e.to_slot == EquipmentSlot.Secondary.value \
		              and Bot.char_data.inventory.items.get(EquipmentSlot.Secondary.value, None)
		if value:
			pass

		MoveInventoryToInventory.on_item_moved -= self.__handle_ammo_equipped__
		sys.stdout.write(f"Ammo equipped\n")
		self._loop_inner_()

	def __handle_drew_aggro__(self, e: MonsterAggroArgs):
		# If monster is different from current target, weaker and should be prioritized
		if (IState.current_target is not None and IState.current_target.unique_id != e.monster.unique_id
				and GeneralTrainingSettings.instance().prioritize_weakest
				and IState.current_target.type.value > e.monster.type.value
				and ScriptUtils.is_point_in_any_area(e.monster.position)):
			IState.current_target = e.monster
			self.target_selected = False
		if GeneralTrainingSettings.instance().zerk_when_x_attackers:
			count = len([value.is_alive and value.target == Bot.char_data.unique_id for key, value in SpawnListsGlobal.MonsterSpawns.items()])
			if count >= GeneralTrainingSettings.instance().x_attackers\
				and Bot.char_data.remain_hwan_count > 0:
				proxy.joymax.send_packet(UseZerk().create_request())


	def __handle_drew_aggro_pet__(self, e: MonsterAggroArgs):
		if IState.current_target \
				and IState.current_target.unique_id != e.monster.unique_id \
				and GeneralTrainingSettings.instance().defend_pet \
				and ScriptUtils.is_point_in_any_area(e.monster.position):
			IState.current_target = e.monster
			self.target_selected = False

	def __handle_zerk_updated__(self):
		if Bot.char_data.remain_hwan_count == 5 and GeneralTrainingSettings.instance().zerk_when_ready:
			proxy.joymax.send_packet(UseZerk().create_request())

	def _loop_inner_(self):
		if IState.current_target is None:
			return
		elif not IState.current_target.is_alive:
			self.target_selected = False
			IState.current_target = None
			self._loop_main_()
			return

		if GeneralTrainingSettings.instance().zerk_vs_strong\
			and IState.current_target.type.value > 1\
			and Bot.char_data.remain_hwan_count > 0:
			proxy.joymax.send_packet(UseZerk().create_request())

		if self.current_skill is not None and self.current_skill.skill_enabled == 0x01:
			# sys.stdout.write(
			# 	f"Use skill {self.current_skill.name} on {IState.current_target.name}({format(IState.current_target.unique_id, '02X')}) HP({IState.current_target.current_hp}/{IState.current_target.max_hp}) Alive:{IState.current_target.is_alive}\n")
			proxy.joymax.send_packet(
				UseSkill(self.current_skill.ref_id, ObjectActionType.UseSkill, IState.current_target.unique_id).create_request())
		else:
			sys.stdout.write(
				f"AutoAttack {format(IState.current_target.unique_id, '02X')} HP({IState.current_target.current_hp}/{IState.current_target.max_hp}) Alive:{IState.current_target.is_alive}\n")
			proxy.joymax.send_packet(
				UseSkill(1, ObjectActionType.AutoAttack, IState.current_target.unique_id).create_request())
		if (not any(b.type is PK2SkillType.Imbue for b in Bot.char_data.active_buffs.values())
				and SkillSettings.imbue.value != 0):
			imbue = next((i for i in Bot.char_data.skills if i.ref_id == SkillSettings.imbue.value), None)
			if imbue and imbue.skill_enabled == 0x01:
				proxy.joymax.send_packet(UseImbue(SkillSettings.imbue.value).create_request())
				# backup in case no packet for imbue cancellation arrives
				self.__delayed_imbue_delete__(imbue)

	def _loop_main_(self):
		self.timer_inner.stop()
		self.timer.stop()

		if not self.is_running:
			return

		if not IState.current_target \
			or IState.current_target and (
				not isinstance(IState.current_target, Monster)
				or not IState.current_target.is_alive
				or IState.current_target.is_ignored):
			self._get_next_target_()
			# Transition if no target was found
			# otherwise, select target (this is the condition for the server to send HP MP updates with opcode 3057)
			if IState.current_target is None:
				self.transition(self.get_next_state())
				return
			elif IState.current_target.type.value > 1:
				next_skill = next((s for s in SkillSettings.BuffSkillsStrong if self.can_buff_be_cast(s)), None)
				if next_skill:
					self.current_skill = next_skill
					self.transition(IStateType.CAST_BUFF, IState.current_target)
					return
		if not self.target_selected and IState.current_target is not None:
			proxy.joymax.send_packet(NPCSelect(IState.current_target.unique_id).create_request())
			self.target_selected = True
			sys.stdout.write(f"Switch to {format(IState.current_target.unique_id, '02X')}\n")
		self.get_next_skill()
		if self.current_skill is None:
			self.transition(self.get_next_state())
			return
		self._use_skill_or_switch_weapon_(self.get_next_skill)

	def __get_closest_monster__(self) -> Monster:
		result: Optional[Monster] = None
		# get the monster within the shortest distance
		# excluding current target, which may be a corpse waiting to despawn
		monsters = {key: value for key, value in SpawnListsGlobal.MonsterSpawns.items()
		            if value.is_alive
		            and (not IState.current_target or IState.current_target and value.unique_id != IState.current_target.unique_id)
		            and self.is_valid_warlock_target(value)}
		for key, value in monsters.items():
			if (ScriptUtils.is_point_in_any_area(value.position)
					and (result is None
					     or Bot.char_data.position.distance_point(value.position)
					     < Bot.char_data.position.distance_point(result.position))):
				result = value
		return result

	def _get_next_target_(self):
		self.timer_inner.stop()
		IState.current_target = self.__get_closest_monster__()
		# set skill in order to start with initiation skill
		self.skills_to_cycle = []
		self.current_skill = None
		self.movement_interruption_count = 0

	def can_cast_warlock_dot(self, skill: Skill):
		if skill.type is PK2SkillType.WarlockDOT:
			sum_dots = sum(1 for b in IState.current_target.buffs.values() if b.pk2_skill.type is PK2SkillType.WarlockDOT)
			already_cast_dot = any(b.ref_id == skill.ref_id for b in IState.current_target.buffs.values())

			return sum_dots < 2 and not already_cast_dot
		else:
			return True

	def get_next_skill_in_cycle(self) -> Optional[Skill]:
		if len(SkillSettings.GeneralSkills) < 1:
			return None

		if not self.skills_to_cycle:
			self.skills_to_cycle = cycle(SkillSettings.GeneralSkills)

		# Cycle through skill list for current monster type
		num_elements = len(SkillSettings.GeneralSkills)
		skill: Optional[Skill] = None

		# Make sure num is not empty before cycling
		if num_elements > 0:
			skill: Skill = next(self.skills_to_cycle)
			# Cycle through skills until a valid one is found or looped back to first skill
			while num_elements > 0:
				# Skip if not castable
				# or Warlock DOT not valid
				# or can't attack knocked down
				if not self.can_skill_be_cast(skill) \
						or not self.can_cast_warlock_dot(skill) \
						or IState.current_target.is_knocked_down and not skill.type.is_knock_down_skill():
					# Don't try: to cast warlock DOT's if target already has them
					skill = next(self.skills_to_cycle)
					num_elements -= 1
					continue
				else:
					break
		return skill

	def get_first_valid_skill(self, skill_list: List[Skill]) -> Optional[Skill]:
		skill: Optional[Skill] = None
		target: Monster = IState.current_target
		weapon = Bot.char_data.inventory.equipment.get(EquipmentSlot.Primary.value, None)
		if weapon and weapon.type is PK2ItemType.Darkstaff:
			# Filter out warlock dots already placed on target from list of skill candidates
			skill_list = [s for s in skill_list if
			              not any(b.ref_id == s.ref_id for b in target.buffs.values())]
			# Don't try: to cast warlock DOT's if target already has them
			if sum(1 for b in target.buffs.values() if b.pk2_skill.type is PK2SkillType.WarlockDOT) > 1:
				# Don't try to cast warlock DOT's if target already has them
				skill_list = [s for s in skill_list if s.type is not PK2SkillType.WarlockDOT]
		if target.is_knocked_down:
			skill = next((s for s in skill_list if self.can_skill_be_cast(s) and s.type.is_knock_down_skill()), None)
		else:
			skill = next(
				(s for s in skill_list if
				self.can_skill_be_cast(s) and s.type is not PK2SkillType.KnockDownOnly),
				None)
		return skill

	@staticmethod
	def get_average_monster_health(level: int) -> int:
		"""Use this to prevent strong monsters like uniques influencing the kill range number.
		Number gained from polynomial of HP values for chinese monster lvl 1-80
		equation -> 135 + 2.07x + 1.33x^2 + 0.00907x^3"""
		return int(135 + 2.07 * level + 1.33 * pow(level, 2) + 0.00907 * (pow(level, 3)))

	def is_within_kill_range(self, m: Monster) -> bool:
		"""Monster below 20% HP is within kill range.
		This takes the smaller value of the average monster level and the current monster's basic hp."""
		return m.current_hp < min((self.get_average_monster_health(m.level) / 5),
		                          (m.max_hp / m.type.get_hp_multiplier()) / 5)

	def get_next_skill(self):
		if IState.current_target is not None:
			if IState.current_target.is_event_monster():
				pass
			elif self.is_within_kill_range(IState.current_target) and len(SkillSettings.FinisherSkills) > 0:  # Finish low hp monsters
				self.current_skill = self.get_first_valid_skill(SkillSettings.FinisherSkills)
				sys.stdout.write(f"Finisher Skill chosen\n")
			elif self.current_skill is None and len(SkillSettings.InitiationSkills) > 0:  # start with initiation
				self.current_skill = self.get_first_valid_skill(SkillSettings.InitiationSkills)
				# Get general skill if no finisher or initiation skill found
				if self.current_skill is None:
					if SkillSettings.cycle_skills.value:
						self.current_skill = self.get_next_skill_in_cycle()
					else:
						self.current_skill = self.get_first_valid_skill(SkillSettings.GeneralSkills)
			else:  # Get normal skills
				if SkillSettings.cycle_skills.value:
					self.current_skill = self.get_next_skill_in_cycle()
				else:
					self.current_skill = self.get_first_valid_skill(SkillSettings.GeneralSkills)
		# if no general skill found (list empty or everything on cooldown) # get simple autoattack instead
		value: Item = Bot.char_data.inventory.equipment.get(EquipmentSlot.Primary.value, None)
		if self.current_skill is None and value:
			weapon_type: PK2ItemType = value.type
			self.current_skill = Bot.auto_attack_skills[weapon_type]

	def _initialize_event_handlers_(self):
		UseSkillHandler.on_skill_cast += self.__on_skill_cast__
		BuffEndHandler.on_buff_ended += self.__on_buff_ended__
		SpawnHandler.on_despawned += self.__on_despawn__
		ObjectStatusHandler.on_death_registered += self.__on_despawn__
		HPMPUpdateHandler.on_monster_updated += self.__on_monster_updated__
		HPMPUpdateHandler.on_monster_died += self.__on_monster_died__
		HPMPUpdateHandler.on_stunned -= self.__on_stunned__
		SkillHandler.on_knocked_back_or_down += self.__on_kb_kd__
		CharStopMovementHandler.on_char_stop_movement += self.__on_stop_movement__
		# SkillHandler.TeleportSkillUsed += HandleTeleportSkill
		ParseItem.on_parsed += self.__handle_drop_spawn__
		UseSkillHandler.on_skillcast_error += self.__handle_skill_error__

		WarlockDOTAddHandler.on_dot_added += self.__handle_dot_added__
		UseSkillHandler.on_drew_monster_aggro += self.__handle_drew_aggro__
		UseSkillHandler.on_drew_aggro_pet += self.__handle_drew_aggro_pet__

		CharValueUpdateHandler.on_zerk_updated += self.__handle_zerk_updated__

	def _remove_event_handlers_(self):
		UseSkillHandler.on_skill_cast -= self.__on_skill_cast__
		BuffEndHandler.on_buff_ended -= self.__on_buff_ended__
		SpawnHandler.on_despawned -= self.__on_despawn__
		ObjectStatusHandler.on_death_registered -= self.__on_despawn__
		HPMPUpdateHandler.on_monster_updated -= self.__on_monster_updated__
		HPMPUpdateHandler.on_monster_died -= self.__on_monster_died__
		HPMPUpdateHandler.on_stunned -= self.__on_stunned__
		HPMPUpdateHandler.on_char_updated -= self.__on_wait_unstunned__  # in case state changes before stun ends
		SkillHandler.on_knocked_back_or_down -= self.__on_kb_kd__
		CharStopMovementHandler.on_char_stop_movement -= self.__on_stop_movement__
		# SkillHandler.TeleportSkillUsed -= HandleTeleportSkill
		ParseItem.on_parsed -= self.__handle_drop_spawn__
		UseSkillHandler.on_skillcast_error -= self.__handle_skill_error__

		WarlockDOTAddHandler.on_dot_added -= self.__handle_dot_added__
		UseSkillHandler.on_drew_monster_aggro -= self.__handle_drew_aggro__
		UseSkillHandler.on_drew_aggro_pet -= self.__handle_drew_aggro_pet__
		self.target_selected = False
		self.is_waiting_for_unstun = False
		self.current_skill = None
		self.movement_interruption_count = 0
