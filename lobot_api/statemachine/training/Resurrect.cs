﻿using LobotAPI.Globals;
using LobotAPI.Globals.Settings;
using LobotAPI.Packets.Char;
using LobotDLL.Connection;
using LobotDLL.Packets;
using LobotDLL.Packets.Char;
using System;
using System.Linq;

namespace LobotDLL.StateMachine.Training
{
    public class Resurrect : BotState
    {
        private void OnSkillCast(object sender, SkillArgs e)
        {
            if (e.skill.refskill_id == CurrentSkill?.refskill_id)
            {
                InnerLoop.Stop();

                // TODO ignore target for X seconds to not remain in loop resurrecting 

                // Wait until cast time before continuing with next skill
                OuterLoop.Interval = Math.Max(e.skill.CastTime, 500);
                OuterLoop.Start();
            }
        }

        protected override void InnerLoopFunction(object sender, EventArgs e)
        {
            if (CurrentSkill == null)
            {
                return;
            }

            if (CurrentSkill != null && CanSkillBeCast(CurrentSkill))
            {
                System.Diagnostics.Debug.WriteLine(string.Format($"Cast Buff : {CurrentSkill.Name}"));
                Proxy.Instance.SendCommandAG(new UseSkill(CurrentSkill.refskill_id, ObjectActionType.UseSkill, CurrentTarget.UniqueID));
            }
        }

        protected override void OuterLoopFunction(object sender, EventArgs e)
        {
            OuterLoop.Stop();

            GetNextTarget();
            get_next_skill();

            if (CurrentSkill == null || CurrentTarget == null)
            {
                Transition(GetNextState());
                return;
            }

            UseSkillOrSwitchWeapon(() => get_next_skill());
        }

        protected override void InitializeEventHandlers()
        {
            UseSkillHandler.SkillCast += OnSkillCast;
        }

        protected override void RemoveEventHandlers()
        {
            UseSkillHandler.SkillCast -= OnSkillCast;

            CurrentSkill = null;
        }

        protected override void GetNextTarget()
        {
            // get target that is dead
            CurrentTarget = SpawnListsGlobal.PlayerList.FirstOrDefault(p => !p.is_alive);
        }

        protected void get_next_skill()
        {
            // get a skill that is off cooldown and not in the active skill buff list
            CurrentSkill = CanSkillBeCast(SkillSettings.ResSpell)
                ? SkillSettings.ResSpell
                : null;
        }
    }
}
