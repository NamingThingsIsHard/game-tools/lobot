﻿using LobotAPI;
using LobotAPI.Globals;
using LobotAPI.Globals.Settings;
using LobotAPI.Packets;
using LobotAPI.Packets.Char;
using LobotAPI.Scripting;
using LobotAPI.Structs;
using LobotDLL.Connection;
using LobotDLL.Packets.Char;
using LobotDLL.Packets.Inventory;
using LobotDLL.Packets.Pet;
using LobotDLL.Packets.Spawn;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LobotDLL.StateMachine.Training
{
    public class Pickup : BotState
    {
        /// <summary>
        /// TODO use this later for a slight delay between picks (not for first pick)
        /// </summary>
        private const int PICK_COMMAND_DELAY = 500;
        /// <summary>
        /// Minimum distance a monster should be from a drop to safely pick.
        /// </summary>
        private const int MIN_MOB_DISTANCE = 6;
        /// <summary>
        /// Amount of times character has stopped movement after packet B023
        /// </summary>
        private int self.__interruption_count__ = 0;

        protected readonly TimeSpan IgnoreDelay = new TimeSpan(15000);

        protected async Task DelayUnIgnoreTarget(uint target_id)
        {
            await Task.Delay(IgnoreDelay);
            if (SpawnListsGlobal.TryGetSpawnObject(target_id, out ISpawnType spawn))
            {
                (spawn as IIgnorable).IsIgnored = false;
            }
        }

        private void HandleStopMovement(object sender, CharStopMovementArgs e)
        {

            // Determine if character stopped to pick item (distance > 5 ) or got stopped on the way there
            if (e.unique_id == Bot.CharData.UniqueID && CurrentTarget != null && ScriptUtils.get_distance(Bot.CharData.position, CurrentTarget.position) > 5)
            {
                self.__interruption_count__++;
            }

            if (self.__interruption_count__ > 3)
            {
                //ignore item for a while
                (CurrentTarget as IIgnorable).IsIgnored = true;
                DelayUnIgnoreTarget(CurrentTarget.UniqueID).ConfigureAwait(false);

                // Switch to next target
                OuterLoopFunction(null, EventArgs.Empty);
            }
        }

        protected override void InitializeEventHandlers()
        {
            SpawnHandler.Despawned += HandleDespawn;
            SkillHandler.TeleportSkillUsed += HandleTeleportSkill;
            CharStopMovementHandler.CharStopMovement += HandleStopMovement;
            MoveItem.ItemMoveError += HandleItemMoveError;
        }

        private void HandleItemMoveError(object sender, MoveItemErrorArgs e)
        {
            switch (e.ErrorType)
            {
                case MoveItemError.CannotFind:
                    if (CurrentTarget != null && SpawnListsGlobal.AllSpawnEntries.TryGetValue(CurrentTarget.UniqueID, out SpawnListsGlobal.SpawnList listNumber))
                    {
                        SpawnListsGlobal.RemoveFromBotList(CurrentTarget.UniqueID, listNumber);
                    }

                    CurrentTarget = null;
                    OuterLoopFunction(null, EventArgs.Empty);
                    break;
                case MoveItemError.ShopNotOpen:
                    // TODO handle in shop window
                    break;
                case MoveItemError.QuestFinished:
                    if (CurrentTarget != null)
                    {
                        (CurrentTarget as ItemDrop).IsIgnored = true;
                    }
                    CurrentTarget = null;
                    OuterLoopFunction(null, EventArgs.Empty);

                    // TODO return after X quests finished
                    //if (GeneralTrainingSettings.instance().ReturnQuestsFinished)
                    //{
                    //    Transition(Return);
                    //}
                    break;
                case MoveItemError.InventoryFull:
                    if (GeneralTrainingSettings.instance().ReturnInventoryFull)
                    {
                        Transition(Return);
                    }
                    break;
                default:
                    break;
            }
        }

        protected override void RemoveEventHandlers()
        {
            SpawnHandler.Despawned -= HandleDespawn;
            SkillHandler.TeleportSkillUsed -= HandleTeleportSkill;
            CharStopMovementHandler.CharStopMovement -= HandleStopMovement;
            MoveItem.ItemMoveError -= HandleItemMoveError;
        }

        protected void HandleDespawn(object sender, DespawnArgs e)
        {
            if (e.type == SpawnListsGlobal.SpawnList.Drops && CurrentTarget?.UniqueID == e.unique_id)
            {
                CurrentTarget = null; // clean up for next target
                OuterLoopFunction(null, null);
            }
        }

        protected override void GetNextTarget()
        {
            // reset counter
            self.__interruption_count__ = 0;

            foreach (ItemDrop drop in SpawnListsGlobal.Drops.Values)
            {
                if (IsDropValidTarget(drop))
                {
                    // get closest drop
                    if (CurrentTarget == null
                        || (CurrentTarget != null && ScriptUtils.get_distance(Bot.CharData.position, CurrentTarget.position) > ScriptUtils.get_distance(Bot.CharData.position, drop.position)))
                    {
                        CurrentTarget = drop;
                    }
                }
            }
        }

        protected override void InnerLoopFunction(object sender, EventArgs e)
        {
            // Try to get next item once
            if (CurrentTarget == null)
            {
                GetNextTarget();
            }

            // switch if still no item found
            if (CurrentTarget == null)
            {
                Transition(GetNextState());
                return;
            }
            else
            {
                // Use pet to pickup if 
                if (Bot.PickupPet.UniqueID != 0) // && Bot.PickupPet.Inventory != full
                {
                    Proxy.Instance.SendCommandAG(new PetAction(CurrentTarget.UniqueID));
                }
                else
                {
                    Proxy.Instance.SendCommandAG(new PickupItem(CurrentTarget.UniqueID));
                }
            }
        }

        private bool TryGetMonsterNextToDrop(out Monster monster)
        {
            foreach (KeyValuePair<uint, Monster> m in SpawnListsGlobal.MonsterSpawns)
            {
                if (CurrentTarget == null)
                {
                    break;
                }

                //find first monster within range of the item
                if (m.Value.is_alive && ScriptUtils.get_distance(m.Value.position, CurrentTarget.position) < MIN_MOB_DISTANCE)
                {
                    monster = m.Value;
                    return true;
                }
            }

            monster = null;
            return false;
        }

        protected override void OuterLoopFunction(object sender, EventArgs e)
        {
            InnerLoop.Stop();
            OuterLoop.Stop();
            GetNextTarget();

            if (CurrentTarget == null)
            {
                BotState nextState = GetNextState();
                if (nextState == this)
                {
                    OuterLoopFunction(null, EventArgs.Empty); // causing random transition loop exceptions
                    return;
                }
                else
                {
                    Transition(nextState);
                    return;
                }
            }

            // Kill monsters around item if it is not valuable(equipment)
            if (!(CurrentTarget as ItemDrop).Pk2Item.IsEquipmentItem()
                && TryGetMonsterNextToDrop(out Monster monster))
            {
                // Make sure target is set during transition
                Transition(Attack, monster);
                return;
            }

            // Send pickup packet once, then send the packet on a loop, until item despawns
            InnerLoopFunction(null, EventArgs.Empty);
            InnerLoop.Start();
        }
    }
}
