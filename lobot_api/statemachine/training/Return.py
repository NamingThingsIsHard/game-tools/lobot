import logging
from typing import Optional

from lobot_api.Bot import Bot
from lobot_api.connection import proxy
from lobot_api.logger.Logger import BOTTING
from lobot_api.packets.char.CharChooseDeathOption import CharChooseDeathOption
from lobot_api.packets.char.CharDeathOption import CharDeathOption
from lobot_api.packets.char.CharInfoHandler import CharInfoHandler
from lobot_api.packets.inventory.ItemUseArgs import ItemUseArgs
from lobot_api.packets.inventory.UseItem import UseItem
from lobot_api.packets.npc.ObjectStatusHandler import ObjectStatusHandler
from lobot_api.pk2.PK2Item import PK2ItemType
from lobot_api.statemachine.IState import IState
from lobot_api.structs.Item import Item


class Return(IState):
	return_scroll: Optional[Item] = None

	def _get_next_target_(self):
		pass

	def _initialize_event_handlers_(self):
		CharInfoHandler.on_parsed_info += self.handle_respawned
		ObjectStatusHandler.on_scroll_mode_change += self.handle_character_scroll_mode_change

	def _remove_event_handlers_(self):
		CharInfoHandler.on_parsed_info -= self.handle_respawned
		ObjectStatusHandler.on_scroll_mode_change -= self.handle_character_scroll_mode_change
		self.return_scroll = None

	def handle_character_scroll_mode_change(self, args: ItemUseArgs):
		"""
		Prevent using return scroll multiple times and safely turn off bot if user interacts.
		:return:
		"""
		if Bot.char_data.scroll_mode:
			self.timer.stop()
		else:
			logging.log(BOTTING, "Return scroll manually stopped. Stopping bot.")
			self.stop()

	def handle_respawned(self):
		self.transition(self.get_next_state())

	def _loop_inner_(self):
		pass

	@staticmethod
	def find_return_scroll() -> Item:
		result = next((i for i in Bot.char_data.inventory.items.values() if i.type is PK2ItemType.ReturnScroll), None)
		# TODO reverse returns are probably too expensive to use for returning.
		# TODO make sure bot leaves town with enough return scrolls instead.
		# if not result:
		# 	result = next(
		# 		(i for i in Bot.char_data.inventory.items.values() if i.type is PK2ItemType.ReverseReturnScroll), None)
		return result

	def _loop_main_(self):
		if Bot.char_data.is_alive:
			if self.return_scroll is None:
				self.return_scroll = self.find_return_scroll()
			# TODO --> walk all the way back to town
			if self.return_scroll is None:
				logging.log(BOTTING, f"Return: No (reverse)return scroll found!")
			else:
				if self.return_scroll.type is PK2ItemType.ReverseReturnScroll:
					logging.log(BOTTING, f"Return: Only reverse return scroll found -> Buying return scroll in town")
				proxy.joymax.send_packet(UseItem(self.return_scroll.slot).create_request())
		else:
			if Bot.char_data.level < 11:
				proxy.joymax.send_packet(CharChooseDeathOption(CharDeathOption.CurrentSpot).create_request())
			else:
				proxy.joymax.send_packet(CharChooseDeathOption(CharDeathOption.SpecifiedSpot).create_request())
		self.timer.start()
