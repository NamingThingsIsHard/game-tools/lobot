from lobot_api.Bot import Bot
from lobot_api.connection import proxy
from lobot_api.globals.settings.SkillSettings import SkillSettings
from lobot_api.packets.char.BuffAddHandler import BuffAddHandler
from lobot_api.packets.char.BuffArgs import BuffArgs
from lobot_api.packets.char.ObjectActionType import ObjectActionType
from lobot_api.packets.char.UseSkill import UseSkill
from lobot_api.statemachine.IState import IState
from lobot_api.structs.Monster import Monster


class CastBuff(IState):

	def _get_next_target_(self):
		self.timer.stop()
		# Just dropping by for some speed
		if self.is_casting_speed_buff:
			skill = next((b for b in Bot.char_data.skills if b.ref_id == SkillSettings.speed_buff.value), self.current_skill)
			if skill:
				self.current_skill = SkillSettings.speed_buff.value
				return

		# Try to get skills for strong monsters
		if isinstance(IState.current_target, Monster) and IState.current_target.type.value > 1:
			self.current_skill = next((s for s in SkillSettings.BuffSkillsStrong if self.can_buff_be_cast(s)), None)
			if self.current_skill:
				return

		# get a skill that is off cooldown and not in the active skill buff list
		self.current_skill = next((s for s in SkillSettings.BuffSkillsWeak if self.can_buff_be_cast(s)), None)

	def handle_buff_added(self, e: BuffArgs):
		"""
		Search next buff to use or transition to next state.
		:return: None
		"""
		if self.current_skill and e.ref_id == self.current_skill.ref_id:
			self.timer_inner.stop()
			if self.is_casting_speed_buff:
				self.transition(self.get_next_state())
				return

			self.timer.setInterval(self.current_skill.cast_time if self.current_skill.cast_time > 0 else 1)
			self.timer.start()

	def _initialize_event_handlers_(self):
		BuffAddHandler.on_buff_added += self.handle_buff_added

	def _remove_event_handlers_(self):
		BuffAddHandler.on_buff_added -= self.handle_buff_added
		self.current_skill = None
		# Prevent moving items from interrupt character from moving
		# PetItemTransfer.stop();

	def _loop_inner_(self):
		if self.current_skill and self.current_skill.skill_enabled == 0x01:
			# sys.stdout.write(f"Cast Buff : {self.current_skill.name}\n")
			proxy.joymax.send_packet(UseSkill(self.current_skill.ref_id, ObjectActionType.UseSkill).create_request())

	def _loop_main_(self):
		self.timer.stop()
		self._get_next_target_()  # Fix bug where 2 instantaneous buffs after another cause a statechange exception

		if self.current_skill is None:
			self.transition(self.get_next_state())
		else:
			self._use_skill_or_switch_weapon_(self._get_next_target_)
