import random

from lobot_api.Bot import Bot
from lobot_api.connection import proxy
from lobot_api.globals.SpawnListsGlobal import SpawnListsGlobal, SpawnList
from lobot_api.globals.settings.TrainingSettings import TrainingAreaSettings, GeneralTrainingSettings
from lobot_api.packets.char.CharStopMovementHandler import CharStopMovementHandler
from lobot_api.packets.char.MonsterAggroArgs import MonsterAggroArgs
from lobot_api.packets.char.Movement import Movement
from lobot_api.packets.char.MovementArgs import MovementArgs
from lobot_api.packets.char.SkillHandler import SkillHandler
from lobot_api.packets.char.UseSkillHandler import UseSkillHandler
from lobot_api.packets.npc.SpawnArgs import SpawnArgs
from lobot_api.packets.spawn.SpawnHandler import ParseMonster, ParseItem
from lobot_api.scripting import ScriptUtils
from lobot_api.scripting.Point import Point
from lobot_api.statemachine.IState import IState, IStateType
from lobot_api.structs.ISpawnObject import ISpawnObject
from lobot_api.structs.ItemDrop import ItemDrop
from lobot_api.structs.Monster import Monster


class Roam(IState):
	last_walk_point: Point = None
	current_walk_point: Point = None

	def __init__(self):
		super().__init__()
		TrainingAreaSettings.on_area_changed += self.__on_area_changed__  # make sure future area changes are handled

	@staticmethod
	def should_switch_to_attack_mode(destination: Point, spawn: ISpawnObject):
		return isinstance(spawn, Monster) and ScriptUtils.is_point_in_any_area(destination)

	def __handle_stop_movement__(self, e):
		# Switch to next target
		self._loop_main_()  # EventArgs.Empty)

	def __handle_spawn_movement__(self, e: MovementArgs):
		found, spawn = SpawnListsGlobal.try_get_spawn_object(e.unique_id)
		if found and self.should_switch_to_attack_mode(e.destination, spawn):
			self.transition(IStateType.ATTACK, spawn)

	def __handle_monster_spawn__(self, e: SpawnArgs):
		if isinstance(e.spawn, Monster) and e.spawn_list is SpawnList.MonsterSpawns and self.is_monster_valid_target(e.spawn):
			self.transition(IStateType.ATTACK, e.spawn)

	def __handle_drop_spawn__(self, e: SpawnArgs):
		if isinstance(e.spawn, ItemDrop) and self.is_drop_valid_target(e.spawn):
			# if Bot.pickup_pet.unique_id != 0 and not PetPickup.IsRunning:
			# 		PetPickup.Start()
			# else:
			self.transition(IStateType.PICKUP, e.spawn)

	def _get_next_target_(self):
		self.timer_inner.stop()
		self.timer.stop()
		if len(ScriptUtils.closest_area_to_char().adjacency_list.keys()) == 0:
			self.current_walk_point = ScriptUtils.closest_area_to_char().center_point
			return

		# Check if walk point has not been initialized yet or walk point is in a distant area
		if not self.current_walk_point or not ScriptUtils.is_position_in_closest_area(Bot.char_data.position):
			point_list = list(ScriptUtils.closest_area_to_char().adjacency_list.keys())
			point_index: int = ScriptUtils.index_of_closest_point(Bot.char_data.position, point_list)
			# Check if index of closest point is valid
			if point_index == -1:
				# take center if index is out of bounds
				self.current_walk_point = ScriptUtils.closest_area_to_char().center_point
			else:
				# do: not backtrack if more than one walk point is available
				# get next point instead
				if point_list == self.last_walk_point and len(point_list) > 1:
					point_index = len(point_list) % (point_index + 1)
				self.current_walk_point = point_list[point_index]
			return
		else:
			# update last walk point
			self.last_walk_point = self.current_walk_point
			# Make sure  the center of the current area is chosen if the walk point is not part of the adjacency list
			point_set = ScriptUtils.closest_area_to_char().adjacency_list.get(self.current_walk_point, None)
			if not point_set:
				point_set = ScriptUtils.closest_area_to_char().adjacency_list[
					ScriptUtils.closest_area_to_char().center_point]

			# Check for number of surrounding points (might be 0 and create_request error if area radius is small enough)
			# get next random point
			if len(point_set) < 1:
				self.current_walk_point = ScriptUtils.closest_area_to_char().center_point
			else:
				random_index: int = random.randint(0, len(point_set) - 1)
				# do: not backtrack if more than one walk point is available
				# get next point instead
				if point_set[random_index] is self.last_walk_point and len(point_set) > 1:
					random_index = len(point_set) % (random_index + 1)
				self.current_walk_point = point_set[random_index]

	def _loop_inner_(self):
		proxy.joymax.send_packet(Movement(self.current_walk_point).create_request())

	def _loop_main_(self):
		self._get_next_target_()
		walk_time = Movement.get_walk_time(Bot.char_data.position, self.current_walk_point)
		if walk_time == 0:
			if len(ScriptUtils.closest_area_to_char().adjacency_list.keys()) < 2:  # stay at point
				walk_time = 10000
			else:
				walk_time = 2500
		proxy.joymax.send_packet(Movement(self.current_walk_point).create_request())
		self.timer.setInterval(walk_time)
		self.timer_inner.setInterval(self.INTERVAL_MS)
		self.timer_inner.start()
		self.timer.start()

	def _initialize_event_handlers_(self):
		Movement.on_spawnmovement_registered += self.__handle_spawn_movement__
		ParseMonster.on_parsed += self.__handle_monster_spawn__
		ParseItem.on_parsed += self.__handle_drop_spawn__
		CharStopMovementHandler.on_char_stop_movement += self.__handle_stop_movement__

		SkillHandler.on_teleport_skill_used += self._handle_teleport_skill_
		UseSkillHandler.on_drew_monster_aggro += self.__handle_drew_aggro__
		UseSkillHandler.on_drew_aggro_pet += self.__handle_drew_aggro_pet__

	def __handle_drew_aggro__(self, e: MonsterAggroArgs):
		if not GeneralTrainingSettings.instance().dont_attack and self.should_switch_to_attack_mode(e.monster.position, e.monster):
			self.transition(IStateType.ATTACK, e.monster)

	def __handle_drew_aggro_pet__(self, e: MonsterAggroArgs):
		if GeneralTrainingSettings.instance().DefendPet and self.should_switch_to_attack_mode(e.monster.position, e.monster):
			self.transition(IStateType.ATTACK, e.monster)

	def _remove_event_handlers_(self):
		Movement.on_spawnmovement_registered -= self.__handle_spawn_movement__
		ParseMonster.on_parsed -= self.__handle_monster_spawn__
		ParseItem.on_parsed -= self.__handle_drop_spawn__
		CharStopMovementHandler.on_char_stop_movement -= self.__handle_stop_movement__
		SkillHandler.on_teleport_skill_used -= self._handle_teleport_skill_
		UseSkillHandler.on_drew_monster_aggro -= self.__handle_drew_aggro__
		UseSkillHandler.on_drew_aggro_pet -= self.__handle_drew_aggro_pet__
		IState.current_target = None
		self.current_walk_point = None

	def __on_area_changed__(self):
		# TODO cover		elif MovementOption == of being too far away from training spot
		# Stop(StrategyChangeArgs(typeof(Return)))
		if not self.current_walk_point or not ScriptUtils.is_point_in_any_area(self.current_walk_point):
			self._get_next_target_()
