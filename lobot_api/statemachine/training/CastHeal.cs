﻿using LobotAPI;
using LobotAPI.Globals.Settings;
using LobotAPI.Packets.Char;
using LobotDLL.Connection;
using LobotDLL.Packets;
using LobotDLL.Packets.Char;
using System;

namespace LobotDLL.StateMachine.Training
{
    public class CastHeal : BotState
    {
        private void OnSkillCast(object sender, SkillArgs e)
        {
            if (e.skill.refskill_id == CurrentSkill?.refskill_id)
            {
                InnerLoop.Stop();

                // Wait until cast time before continuing with next skill
                OuterLoop.Interval = Math.Max(e.skill.CastTime, 500);
                OuterLoop.Start();
            }
        }

        protected override void InnerLoopFunction(object sender, EventArgs e)
        {
            if (CurrentSkill == null)
            {
                return;
            }

            if (CurrentSkill != null && CanSkillBeCast(CurrentSkill))
            {
                System.Diagnostics.Debug.WriteLine(string.Format($"Cast Buff : {CurrentSkill.Name}"));
                Proxy.Instance.SendCommandAG(new UseSkill(CurrentSkill.refskill_id, ObjectActionType.UseSkill));
            }
        }

        protected override void OuterLoopFunction(object sender, EventArgs e)
        {
            OuterLoop.Stop();

            GetNextTarget();

            if (CurrentSkill == null || ((Bot.CharData.CurrentHP * 100) / Bot.CharInfo.MaxHP) > SkillSettings.HealPercentage)
            {
                Transition(GetNextState());
                return;
            }

            UseSkillOrSwitchWeapon(() => GetNextTarget());
        }

        protected override void InitializeEventHandlers()
        {
            UseSkillHandler.SkillCast += OnSkillCast;
        }

        protected override void RemoveEventHandlers()
        {
            UseSkillHandler.SkillCast -= OnSkillCast;

            CurrentSkill = null;
        }

        protected override void GetNextTarget()
        {
            // get a skill that is off cooldown and not in the active skill buff list
            CurrentSkill = CanSkillBeCast(SkillSettings.HealSpell)
                ? SkillSettings.HealSpell
                : null;
        }
    }
}
