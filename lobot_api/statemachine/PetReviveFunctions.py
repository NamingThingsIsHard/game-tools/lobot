#  Summon pet
# 16												pet_scroll slot
# CC 08											 itemType pet summon scroll
# 01												success
# 16												pet_scroll slot
# 01 00											 stacks
# CC 08											 itemType pet summon scroll
# 16												pet_scroll slot
# 40												update type pet?
# 02												status summoned
#  Use Grass of life
# 22												grass of life slot
# EC 30											 item type
# 11												target pet slot
# 01												success
# 22												item used slot
# 02 00											 stack_count
# EC 30											 item type
import logging

from lobot_api.Bot import Bot
from lobot_api.BotStatus import BotStatus
from lobot_api.connection import proxy
from lobot_api.globals.settings.TrainingSettings import PetSettings
from lobot_api.logger.Logger import BOTTING
from lobot_api.packets.inventory.InventoryUpdateArgs import InventoryUpdateArgs
from lobot_api.packets.inventory.ItemUseArgs import ItemUseArgs
from lobot_api.packets.inventory.UpdateInventory import UpdateInventory
from lobot_api.packets.inventory.UseItem import UseItem
from lobot_api.packets.inventory.UseItemType import UseItemType
from lobot_api.packets.npc.ObjectStatusHandler import ObjectStatusHandler
from lobot_api.pk2.PK2Item import PK2ItemType
from lobot_api.structs.Item import PetStatus, PetScroll, Item


class PetReviveFunctions:
	@staticmethod
	def _initialize_pet_revive_handlers():
		PetSettings.revive_pet.signal.connect(PetReviveFunctions.__handle_pet_revive_changed__)
		ObjectStatusHandler.on_combat_mode_changed += PetReviveFunctions.__handle_exited_combat__
		UpdateInventory.on_petscroll_updated += PetReviveFunctions.__handle_pet_status_change__

		if PetSettings.revive_pet:
			PetReviveFunctions.__try_revive__()

	@staticmethod
	def remove_pet_revive_handlers():
		PetSettings.revive_pet.signal.disconnect(PetReviveFunctions.__handle_pet_revive_changed__)
		ObjectStatusHandler.on_combat_mode_changed -= PetReviveFunctions.__handle_exited_combat__
		UpdateInventory.on_petscroll_updated -= PetReviveFunctions.__handle_pet_status_change__

	@staticmethod
	def should_summon_pet() -> bool:
		return PetSettings.revive_pet and Bot.get_status() is BotStatus.Botting and Bot.growth_pet.unique_id == 0

	@staticmethod
	def __summon__(pet_scroll: PetScroll):
		proxy.joymax.send_packet(UseItem(pet_scroll.slot).create_request())

	@staticmethod
	def __try_revive__():
		pet_scroll: PetScroll = next(
			(value for key, value in Bot.char_data.inventory.items.items() if value.type is PK2ItemType.GrowthPet),
			None)
		if pet_scroll is not None:
			# switch (petScroll.status)
			if pet_scroll.status == PetStatus.Summoned:
				pass
			elif pet_scroll.status == PetStatus.Unsummoned:
				pass
			elif pet_scroll.status == PetStatus.Alive:
				if Bot.char_data.InCombat == 0:
					PetReviveFunctions.__summon__(pet_scroll)
			elif pet_scroll.status == PetStatus.Dead:
				PetReviveFunctions.__try_use_grass_of_life__(pet_scroll)
			else:
				pass
		else:
			pass

	@staticmethod
	def __try_use_grass_of_life__(pet_scroll: PetScroll):
		grass_of_life: Item = next(
			(value for key, value in Bot.char_data.inventory.items.items() if value.type is PK2ItemType.GrassOfLife),
			None)
		if grass_of_life is not None:
			proxy.joymax.send_packet(
				UseItem(grass_of_life.slot, UseItemType.UseOnItem, 0, pet_scroll.slot).create_request())

	@staticmethod
	def __handle_exited_combat__():
		if PetReviveFunctions.should_summon_pet():
			PetReviveFunctions.__try_revive__()

	@staticmethod
	def __handle_pet_summoned__(e: ItemUseArgs):
		if e.Item.type is PK2ItemType.GrowthPet:
			logging.log(BOTTING, f"{e.Item.pk2_item.name} summoned from slot {e.slot}")

	@staticmethod
	def __handle_pet_status_change__(e: InventoryUpdateArgs):
		if PetReviveFunctions.should_summon_pet() and (e.update_type == 1 or e.update_type == 4):
			PetReviveFunctions.__try_revive__()

	@staticmethod
	def __handle_pet_revive_changed__():
		if PetReviveFunctions.should_summon_pet():
			PetReviveFunctions.__try_revive__()
