from lobot_api.statemachine.IState import IStateType


class StateChangedArgs:
	def __init__(self, s: IStateType):
		if not s:
			raise ValueError(s)
		self.state = s
