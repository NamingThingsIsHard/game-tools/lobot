import sys
from abc import abstractmethod, ABC
from enum import IntEnum
from typing import Tuple, Callable, List, Optional, Dict, Union

from PyQt5.QtCore import QTimer

from lobot_api import ReusableTimer
from lobot_api.Bot import Bot
from lobot_api.Event import Event
from lobot_api.connection import proxy
from lobot_api.globals.SpawnListsGlobal import SpawnListsGlobal
from lobot_api.globals.settings.ItemFilterSettings import ItemFilterSettings
from lobot_api.globals.settings.LoopSettings import ScriptSettings
from lobot_api.globals.settings.SkillSettings import SkillSettings
from lobot_api.globals.settings.TrainingSettings import GeneralTrainingSettings, TrainingAreaSettings
from lobot_api.packets.char.CharDeathHandler import CharDeathHandler
from lobot_api.packets.char.MovementArgs import MovementArgs
from lobot_api.packets.npc.NPCCloseDialogue import NPCCloseDialogue
from lobot_api.pk2.PK2Item import PK2ItemType
from lobot_api.pk2.PK2Skill import PK2SkillType, PK2Skill
from lobot_api.scripting import ScriptUtils, Town
from lobot_api.structs.CharData import Skill
from lobot_api.structs.EquipmentSlot import EquipmentSlot
from lobot_api.structs.IIgnorable import IIgnorable
from lobot_api.structs.ISpawnObject import ISpawnObject
from lobot_api.structs.Item import Equipment
from lobot_api.structs.ItemDrop import ItemDrop
from lobot_api.structs.Monster import Monster, MonsterType
from lobot_api.structs.ObjectStatus import ObjectStatus
from silkroad_security_api_1_4.Packet import Packet


class IStateType(IntEnum):
	PREVIOUS = -1
	IDLE = 0
	# Town
	TOWN_SCRIPT = 1
	STORAGE = 2
	SHOP_POTIONS = 3
	SHOP_SMITH = 4
	SHOP_SPECIAL = 5
	SHOP_STABLE = 6
	# Training
	WALK_SCRIPT = 10
	ATTACK = 11
	PICKUP = 12
	CAST_BUFF = 13
	CAST_HEAL = 14
	SWITCH_WEAPON = 15
	RESURRECT = 16
	ROAM = 17
	# Pet
	PET_ITEM_TRANSFER = 18
	PET_PICKUP = 19
	RETURN = 20
	# Various
	EXCHANGE = 30
	STALLING = 31
	ALCHEMY = 32

	TRADE_JOB = 50


class IState(ABC):
	"""
	State pattern class.
	"""
	context = None

	on_started = Event()
	on_stopped = Event()
	on_state_changed = Event()

	INTERVAL_MS = 5000

	monster_types: List[MonsterType] = [i.value for i in MonsterType]

	current_skill: Optional[Skill] = None
	current_target: Optional[Union[ISpawnObject, Monster]] = None
	is_casting_speed_buff = False
	is_running = False
	is_transitioning = False

	weapon_to_equip: Equipment = None

	def __init__(self):
		self.is_running = False
		self.timer: QTimer = ReusableTimer.get_q_timer(self.INTERVAL_MS, self._loop_main_)
		self.timer_inner: QTimer = ReusableTimer.get_q_timer(self.INTERVAL_MS, self._loop_inner_)

	def __str__(self) -> str:
		return self.__class__.__name__

	def __repr__(self) -> str:
		return self.__class__.__name__

	@abstractmethod
	def _get_next_target_(self):
		pass

	@abstractmethod
	def _initialize_event_handlers_(self):
		"""
		First function to be run when starting.
		Wires events up and allows them to be triggered from incoming packets.
		:return: None
		"""
		pass

	@abstractmethod
	def _remove_event_handlers_(self):
		"""
		Function to be run before transitioning or stopping.
		Prevents any state events being triggered after it is stopped.
		:return: None
		"""
		pass

	def start(self):
		if self.is_running:
			return
		elif Bot.is_interacting:
			NPCCloseDialogue.on_dialogue_closed += self.__handle_interaction_stopped_for_bot_start__
			proxy.joymax.send_packet(NPCCloseDialogue(Bot.selected_npc.unique_id).create_request())
		else:
			# Eventhandlers for general events
			self.is_running = True
			self.is_transitioning = False
			CharDeathHandler.on_char_died += self._handle_death_
			proxy.on_client_sent_skill_update_packet += self._handle_skill_update_packet_
			self._initialize_event_handlers_()
			# Init error might cut start() short
			if self.is_transitioning:
				return
			self._loop_main_()
			self.on_started.notify()

	def __prepare_for_transition__(self, next_target: ISpawnObject = None):
		IState.current_target = next_target
		CharDeathHandler.on_char_died -= self._handle_death_
		proxy.on_client_sent_skill_update_packet -= self._handle_skill_update_packet_
		self._remove_event_handlers_()
		self.timer.stop()
		self.timer_inner.stop()
		self.is_running = False
		self.is_transitioning = True

	def stop(self):
		self.__prepare_for_transition__()
		self.on_stopped.notify()

	def transition(self, next_state, next_target: ISpawnObject = None):
		"""
		Signal transition to another state to state machine.
		Does not transition if already transitioning.
		:param next_state: Next state to start.
		:param next_target: Next target to choose.
		:return: None
		"""
		if self.is_transitioning:
			return
		self.__prepare_for_transition__(next_target)
		self.on_state_changed.notify(next_state)

	@abstractmethod
	def _loop_inner_(self):
		"""
		The function to be called periodically by inner timer.
		Important for ensuring bot stays on track in walkscripts or keeps pursuing monsters.
		"""
		pass

	@abstractmethod
	def _loop_main_(self):
		"""
		The function to be called periodically by timer.
		Important for ensuring bot stays on track in walkscripts or keeps pursuing monsters.
		"""
		pass

	@staticmethod
	def is_monster_valid_target(m) -> bool:
		"""Predicate to find next monster to attack"""
		return ScriptUtils.is_point_in_any_area(m.position) and m.is_alive and not m.is_ignored

	@staticmethod
	def is_drop_valid_target(item: ItemDrop):
		"""Predicate to evaluate next drop to pick up
		A drop is valid when it is in the training area and is set to be picked in the item filter.
		If a pick up pet is active, the drop does not depend on training areas, only on the item filter.
		In a full inventory, gold or any drop that fits onto a stack of similar items can be valid.
		"""
		pick = item.pk2_item.should_pick()
		in_area = True if Bot.pickup_pet.unique_id != 0 else ScriptUtils.is_point_in_any_area(item.position)
		id_match = item.owner_id == Bot.char_data.account_id or item.owner_id == 0
		valid_settings = (item.owner_id == 0xFFFFFFFF and ItemFilterSettings.FreeItems.value or id_match)

		if item.pk2_item.type == PK2ItemType.Gold:
			return pick and in_area and valid_settings

		full = Bot.character_inventory_is_full()
		stackable = (True
		             if not full
		             else (any(value.ref_id == item.ref_id and value.stack_count < value.max_stacks
		                       for value in Bot.char_data.inventory.items.values())))

		valid_position = (item is not None and stackable and pick and in_area)

		#  0xFFFFFFFF is *Bot.char_data.unique_id* or i.unique_id == 0x0
		#  TODO unclaimed drop for pet manager

		return valid_position and valid_settings

	@staticmethod
	def is_valid_warlock_target(m: Monster) -> bool:
		"""Predicate to make sure that monster is valid target in case of warlock DOT conditions.
		Default to True, if char is not actually a warlock
		or if monster is stronger than champion
		or if monster is weaker than giant but has less than 2 DOTs."""
		item = Bot.char_data.inventory.equipment.get(EquipmentSlot.Primary.value, None)
		is_not_warlock = item and item.type is not PK2ItemType.Darkstaff
		valid_monster = True if m.type.value > 1 else \
			m and sum(1 for b in m.buffs.values() if b.type is PK2SkillType.WarlockDOT) < 2
		settings_switch_on_dot = GeneralTrainingSettings.instance().warlock_dot_switch
		return is_not_warlock or valid_monster or not settings_switch_on_dot

	@staticmethod
	def _can_start_training_() -> bool:
		# Check if out of town and close to any training area
		return (not Town.current_town(Bot.char_data.position)
		        and (ScriptUtils.MAX_GO_DISTANCE > ScriptUtils.distance_to_training_area1()
		             or (TrainingAreaSettings.instance().use_second_area
		                 and ScriptUtils.MAX_GO_DISTANCE > ScriptUtils.distance_to_training_area2())))

	@staticmethod
	def _should_attack_() -> bool:
		status = Bot.char_data.status is not ObjectStatus.Invincible
		value = any(value for key, value in SpawnListsGlobal.MonsterSpawns.items()
					if IState.is_monster_valid_target(value)
					and IState.is_valid_warlock_target(value))
		return status and value

	@staticmethod
	def _should_roam_():
		return ScriptUtils.MAX_GO_DISTANCE > ScriptUtils.distance_to_closest_area() - ScriptUtils.closest_area_to_char().radius

	@staticmethod
	def can_skill_weapon_be_equipped(s: Skill) -> bool:
		"""Predicate to check whether skill can be cast.
		Either the item is equipped or it can be switched from the inventory."""
		item = Bot.char_data.inventory.items.get(s.required_weapon_slot, None)
		if item and s.pk2_skill.does_skill_match_weapon(s, item.type):
			return True
		else:
			weapon = Bot.char_data.inventory.equipment.get(EquipmentSlot.Primary.value, None)
			primary_equipped: bool = weapon is not None
			is_inventory_full: bool = Bot.character_inventory_is_full()
			# find item in inventory and find out whether the weapon has enough space to be# switched in.
			# A weapon must exist and there should be enough space to# switch out the equipped items
			any_weapon = any(
				s.pk2_skill.does_skill_match_weapon(s, value.type) for key, value in
				Bot.char_data.inventory.items.items())
			return any_weapon and not primary_equipped or weapon and weapon.type.is_2_slot_weapon() or not is_inventory_full

	@staticmethod
	def can_skill_be_cast(s: Skill) -> bool:
		"""Predicate to check whether attack can be cast."""
		is_enabled = s.skill_enabled == 0x01
		weapons_match_equipment = any(
			s.pk2_skill.does_skill_match_weapon(s, value.type) for value in Bot.char_data.inventory.equipment.values())
		weapons_match_items = any(
			s.pk2_skill.does_skill_match_weapon(s, value.type) for value in Bot.char_data.inventory.items.values())
		return is_enabled and (weapons_match_equipment or weapons_match_items)

	@staticmethod
	def can_buff_be_cast(s: Skill):
		is_enabled = s.skill_enabled == 0x01
		already_cast = s in Bot.char_data.active_buffs.values()
		return is_enabled and not already_cast and IState.can_skill_weapon_be_equipped(s)

	@staticmethod
	def try_find_weapon_to_switch(item_type: PK2ItemType) -> Tuple[bool, Optional[Equipment]]:
		""" Find a weapon from the inventory with the given type to# switch, in order to use a skill.
		This takes the last weapon switched and gets the weapon with the highest level (weapon level + enhancement ).
		:param item_type:  Type of weapon to look for
		:return: Tuple[bool, Equipment] of whether weapon was found and the weapon
		"""
		weapon: Optional[Equipment] = None
		for key, value in {**Bot.char_data.inventory.equipment, **Bot.char_data.inventory.items}.items():
			# Choose item with matching type if weapon has not been assigned yet or the effective level is highest
			if (value.type is item_type
					and (weapon is None or value.effective_level > weapon.effective_level)):
				weapon = value
		return weapon is not None, weapon

	@staticmethod
	def is_required_weapon_equipped(skill: PK2Skill) -> bool:
		"""Predicate of whether the weapon in the primary slot is the correct one for a given skill."""
		weapon = Bot.char_data.inventory.equipment.get(skill.required_weapon.get_required_weapon_slot(), None)
		if weapon:
			return skill.does_skill_match_weapon(skill, weapon.type)
		return False

	def _use_skill_or_switch_weapon_(self, get_next_skill: Callable) -> bool:
		"""Choose to either attack or# switch weapons based on current skill.
		# Takes an action to determine the next skill from the skill list.
		# If the current skill can be cast, the skill is cast normally.
		# If the weapon required to cast the current skill is not yet equipped but is found in the inventory,
		# the state switches to SwitchWeapon.
		# If the weapon is not found, a loop commences to find the next skill with a weapon that can cast it,
		# disabling all uncastable skills.
		# In case no attacks are found, the loop concludes with a normal attack."""
		required_item: Optional[Equipment] = None
		# loop until right weapon is equipped or required item for weapon# switch is found
		while True:
			if self.current_skill is None:
				self.transition(self.get_next_state())
				return False
			# Check if corresponding weapon is equipped
			elif self.is_required_weapon_equipped(self.current_skill.pk2_skill):
				# Send skill packet once, then send the skill on a loop, until it has been cast
				self._loop_inner_()
				self.timer_inner.start()
				return False
			else:
				# Check if item is available
				found_weapon, required_item = self.try_find_weapon_to_switch(self.current_skill.required_weapon)
				if found_weapon:
					# TODO: Disable skill until weapon of required type is added to inventory
					IState.weapon_to_equip = required_item
					self.transition(IStateType.SWITCH_WEAPON)
					return True
				else:
					get_next_skill()
			if self.is_required_weapon_equipped(self.current_skill.pk2_skill) or required_item is not None:
				return False

	def _handle_teleport_skill_(self, e: MovementArgs):
		if e.unique_id == Bot.char_data.unique_id and self.is_running:
			self._loop_main_()

	def _handle_death_(self):
		# Make sure buffs are reset to allow imbues to be cast after death
		Bot.char_data.active_buffs.clear()
		# Return to town if conditions are met
		if GeneralTrainingSettings.should_return_to_town():
			self.transition(IStateType.RETURN)
		else:  # wait for resurrection
			self.transition(IStateType.IDLE)

	def __handle_interaction_stopped_for_bot_start__(self):
		sys.stdout.write(f"Stopped interaction with {IState.current_target.name} to start botting\n")
		# Eventhandlers for general events
		CharDeathHandler.on_char_died += self._handle_death_
		self.is_running = True
		self._initialize_event_handlers_()
		self._loop_main_()

	@staticmethod
	def _handle_skill_update_packet_(packet_dict: Dict):
		"""
		Use this to update skill ref_id before bot tries to use the updated skill.
		This is necessary because the server cancels the updated skill with 0xB072 before confirming the update with 0xB05A.
		Since the bot registers the cancelled buff first, it will try to cast the buff by sending the old ref_id before
		receiving 0xB05A, causing a client crash.
		:param packet_dict: The packet information sent as c->p->s
		:return:
		"""
		if packet_dict['opcode'] == 0x705A:
			p = Packet(packet_dict['opcode'], packet_dict['encrypted'], packet_dict['massive'], packet_dict['data'])
			p.lock()
			ref_id = p.read_uint32()
			skill = next((s for s in Bot.char_data.skills if s.ref_id == ref_id - 1), None)
			if skill:
				skill.ref_id = ref_id

	@staticmethod
	def _should_cast_buff_() -> bool:
		any_buffs: bool = len(SkillSettings.BuffSkillsWeak) > 0
		can_cast: bool = any(IState.can_buff_be_cast(b) for b in SkillSettings.BuffSkillsWeak)
		is_speed_buff_set: bool = not SkillSettings.speed_buff_or_pot.value and SkillSettings.speed_buff.value > 0
		no_speed_buff_cast: bool = not any(ab.type is PK2SkillType.SpeedBuff for ab in Bot.char_data.active_buffs.values())
		# if (any_buffs and can_cast or is_speed_buff_set and no_speed_buff_cast) and IStateMachine.Instance.State is CastBuff:
		# 	raise StateChangeException(CastBuff)
		return any_buffs and can_cast or is_speed_buff_set and no_speed_buff_cast

	@staticmethod
	def _should_use_speed_pot_() -> bool:
		speed_up_types = [PK2ItemType.SpeedScroll, PK2ItemType.SpeedPotion]
		no_speed_buff_cast: bool = not any(ab.type is PK2SkillType.SpeedBuff for ab in Bot.char_data.active_buffs.values())
		has_speed_buff_item: bool = any(
			value.type in speed_up_types for key, value in Bot.char_data.inventory.items.items())
		return (no_speed_buff_cast
		        and has_speed_buff_item
		        and ScriptSettings.SpeedBuffOrPotion.value)

	@staticmethod
	def _should_mount_() -> bool:
		is_not_mounted = Bot.char_data.transport_flag == 0
		not_in_combat = Bot.char_data.is_alive and not Bot.char_data.in_combat
		valid_settings = ScriptSettings.RideHorse.value
		has_horse = any(key for key, value in Bot.char_data.inventory.items.items()
		                   if value.pk2_item.type is PK2ItemType.PetRide)
		return is_not_mounted and not_in_combat and valid_settings and has_horse

	@staticmethod
	def _should_cast_speed_buff_() -> bool:
		valid_settings = SkillSettings.speed_buff_or_pot.value is False and SkillSettings.speed_buff.value != 0
		valid_skill = IState.current_skill is None
		valid_skill2 = any(b for b in Bot.char_data.active_buffs.values() if b.ref_id == SkillSettings.speed_buff.value)
		return valid_settings and valid_skill and not valid_skill2

	@staticmethod
	def _should_pickup_():
		no_pickup_pet = Bot.pickup_pet.unique_id == 0
		valid_drop = any(IState.is_drop_valid_target(value) for key, value in SpawnListsGlobal.Drops.items())
		no_monster_threat = not any(value for key, value in SpawnListsGlobal.MonsterSpawns.items()
		                     if value.target == Bot.char_data.unique_id and value.is_alive)
		return valid_drop and no_monster_threat and no_pickup_pet

	@staticmethod
	def get_next_state() -> IStateType:
		"""Use to  determine to transition to next state. Default is Idle state.
		:return: StateType of Next state
		 """
		if IState.is_casting_speed_buff:
			IState.is_casting_speed_buff = False
			return IStateType.PREVIOUS

		# Check if in town or too far from training area and# set
		if Town.current_town(Bot.char_data.position) is not None:
			return IStateType.TOWN_SCRIPT
		elif not IState._can_start_training_() and ScriptSettings.TrainingScriptPath.value:
			return IStateType.WALK_SCRIPT

		next_state: IStateType = IStateType.IDLE

		# Only use combat/skill states after the 5 second invincibility after death/teleporting
		# TODO res
		if Bot.char_data.is_alive\
			and SkillSettings.heal_spell.value\
			and SkillSettings.use_heal.value\
			and SkillSettings.heal_percentage.value <= Bot.current_hp_percent:
			next_state = IStateType.CAST_HEAL
		# if resurrect_skill and party_member_dead and party_member_close and number_attacks < x:
		# 	next_state = IStateType.RESURRECT
		elif GeneralTrainingSettings.instance().should_return_to_town():
			next_state = IStateType.RETURN
		elif Bot.char_data.status != ObjectStatus.Invincible and IState._should_cast_buff_():
			next_state = IStateType.CAST_BUFF
		elif IState._should_pickup_():
			next_state = IStateType.PICKUP
		elif Bot.char_data.status != ObjectStatus.Invincible and IState._should_attack_():
			next_state = IStateType.ATTACK
		# wait
		elif IState._should_roam_():
			next_state = IStateType.ROAM
		else:
			next_state = IStateType.IDLE
		return next_state
