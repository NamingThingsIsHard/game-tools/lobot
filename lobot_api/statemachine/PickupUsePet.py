﻿from typing import Optional

from PyQt5.QtCore import QTimer, QThreadPool

from lobot_api.Bot import Bot
from lobot_api.connection import proxy
from lobot_api.globals.SpawnListsGlobal import SpawnListsGlobal, SpawnList
from lobot_api.packets.char.CharStopMovementArgs import CharStopMovementArgs
from lobot_api.packets.char.CharStopMovementHandler import CharStopMovementHandler
from lobot_api.packets.npc.DespawnArgs import DespawnArgs
from lobot_api.packets.npc.SpawnArgs import SpawnArgs
from lobot_api.packets.pet.PetAction import PetAction, PetActionType
from lobot_api.packets.spawn.SpawnHandler import SpawnHandler, ParseItem
from lobot_api.scripting import ScriptUtils
from lobot_api.statemachine.IState import IState
from lobot_api.structs.ItemDrop import ItemDrop

is_initialized = False
is_running = False
current_target_item: Optional[ItemDrop] = None
pick_delay = 5000

timer = QTimer()
timer_inner = QTimer()


def _loop_inner_():
	if current_target_item is None:
		timer_inner.stop()
		timer.start()
		return
	else:
		# Use pet to pickup if
		if Bot.pickup_pet.unique_id != 0:  # and Bot.PickupPet.Inventory != full:
			proxy.joymax.send_packet(PetAction(PetActionType.Pickup, current_target_item.unique_id).create_request())
		else:
			timer.stop()


def _loop_main_():
	timer_inner.stop()
	timer.stop()
	_get_next_target_()

	if current_target_item is None:
		timer.stop()
		return

	# Send pickup packet once, then send the packet on a loop, until item despawns
	_loop_inner_()
	timer_inner.start()


def __handle_stop_movement__(e: CharStopMovementArgs):
	# Determine if character stopped to pick item (distance > 5 ) or got stopped on the way there
	if e.unique_id == Bot.pickup_pet.unique_id and current_target_item is not None:
		# ignore item for 15s
		current_target_item.is_ignored = True

		# Switch to next target
		_loop_main_()


def __handle_spawn_item__(e: SpawnArgs):
	""" If drop is valid
	Send pet to pick
	or Switch to picking up if not under attack
	:param e: Spawn args
	:return:
	"""
	global current_target_item
	if not current_target_item and e.spawn_list is SpawnList.Drops and IState.is_drop_valid_target(e.spawn):
		current_target_item = e.spawn
		_loop_main_()


def __handle_despawn_item__(e: DespawnArgs):
	global current_target_item
	if current_target_item and e.list_type == SpawnList.Drops and current_target_item.unique_id == e.unique_id:
		current_target_item = None  # clean up for next target
		_loop_main_()
	elif e.unique_id == Bot.pickup_pet.unique_id:
		stop()


def handle_pet_spawn(e: SpawnArgs):
	if e.spawn.unique_id == Bot.pickup_pet.unique_id and not is_running:
		start()


def _get_next_target_():
	global current_target_item
	for drop in SpawnListsGlobal.Drops.values():
		if IState.is_drop_valid_target(drop) and (current_target_item is None
		                                          or ScriptUtils.is_position_in_closest_area(drop.position)
		                                          and Bot.pickup_pet.position.distance_point(
					current_target_item.position) > Bot.pickup_pet.position.distance_point(drop.position)):
			current_target_item = drop


def start():
	global is_running, is_initialized
	if not is_initialized:
		timer.setInterval(pick_delay * 2)
		timer_inner.setInterval(pick_delay)
		timer.timeout.connect(_loop_main_)
		timer_inner.timeout.connect(_loop_inner_)

	if is_running:
		return
	else:
		ParseItem.on_parsed += __handle_spawn_item__
		SpawnHandler.on_despawned += __handle_despawn_item__
		CharStopMovementHandler.on_char_stop_movement += __handle_stop_movement__
		is_running = True


def stop():
	global current_target_item, is_running
	ParseItem.on_parsed -= __handle_spawn_item__
	SpawnHandler.on_despawned -= __handle_despawn_item__
	CharStopMovementHandler.on_char_stop_movement -= __handle_stop_movement__
	is_running = False
	current_target_item = None
	timer.stop()
	timer_inner.stop()
