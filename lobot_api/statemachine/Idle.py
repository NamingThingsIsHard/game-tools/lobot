from PyQt5.QtCore import QTimer

from lobot_api.Bot import Bot
from lobot_api.globals.settings.TrainingSettings import GeneralTrainingSettings
from lobot_api.statemachine.IState import IState
from lobot_api.statemachine.IState import IStateType


class Idle(IState):
	"""The default state.
	The bot should only be in this state before it is started or after a fatal error has occurred.
	"""

	__death_timer__: QTimer = QTimer()

	def _get_next_target_(self):
		pass

	def __return_on_death_timer_elapsed__(self):  # elapsedEventArgs):
		Idle.__death_timer__.stop()
		self.transition(IStateType.RETURN)

	def _loop_inner_(self):
		pass

	def _loop_main_(self):
		# Stop stalling
		if Bot.char_data.interaction_mode == 4:
			self.transition(IStateType.IDLE)
			return
		# This should be invoked when bot is started and immediately# switch to the next best state.
		if not self.__death_timer__.isActive():
			self.transition(self.get_next_state())

	def _initialize_event_handlers_(self):
		# TODO accept res
		self.__death_timer__.timeout.connect(self.__return_on_death_timer_elapsed__)
		self.__death_timer__.setInterval(GeneralTrainingSettings.instance().x_minutes * 60)

		if not Bot.char_data.is_alive and GeneralTrainingSettings.instance().return_dead_x_minutes:
			if GeneralTrainingSettings.instance().x_minutes < 1:
				self.transition(IStateType.RETURN)  # waiting less than a minute means instantaneous return attempt
			else:
				self.__death_timer__.start()

	def _remove_event_handlers_(self):
		self.__death_timer__.timeout.disconnect(self.__return_on_death_timer_elapsed__)
		self.__death_timer__.stop()
