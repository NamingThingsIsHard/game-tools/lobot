from lobot_api.structs.Item import Item


class ItemSwitchArgs:
	def __init__(self, primary: Item = None, secondary: Item = None):
		self.Primary = primary
		self.Secondary = secondary
