from builtins import bool
from enum import Enum, IntEnum
from typing import Dict, Tuple

from lobot_api.globals.settings.AlchemyFilterSettings import AlchemyFilterSettings
from lobot_api.globals.settings.ItemFilterSettings import ItemFilterSettings
from lobot_api.globals.settings.Settings import settings
from lobot_api.pk2.IPK2Type import IPK2Type
from lobot_api.structs.DegreeFilter import DegreeFilter
from lobot_api.structs.EquipmentSlot import EquipmentSlot
from lobot_api.structs.ItemFilter import ItemFilter


class PK2ItemType(IntEnum):
	"""
	Made up of optionalCustomType(uint16) type2(byte), type3(byte) type4(uint16)
	Custom types like BuffScrolls have 2 optional bytes: customType(uint16) type2(byte), type3(byte) type4(uint16)
	The customType number must be ignored for operations that only use the pk2 bytes.
	For example when using items, using a mask to get the 4 least significant bytes: 0xFFFF & Type

	E.g. for an type number: hp pot the value would be 0x3101 fom => "3 1  1"
	Type 4 can be greater than 15 and needs 2 bytes, e.g. InventoryScrolls => "3 13 17"
	from pk2 entry:
	1	4	ITEM_ETC_HP_POTION_01	HP ?? ??	xxx	SN_ITEM_ETC_HP_POTION_01	SN_ITEM_ETC_HP_POTION_01_TT_DESC	0	0	3	3	1	1
	the important part starts after the 7th column:	0	0	3	3	1	1
		with 0(cashItem)    0(Bionic)    3(type1--isItem (always 3))    3(type2--{1 = equip 2 = pets 3 = etc}) 1(type3--itemtype)  1(type4--itemsubtype)
	"""
	Other = 0x01
	Equipment = 0x1100
	# Garment
	GarmentHead = 0x1101
	GarmentShoulder = 0x1102
	GarmentBody = 0x1103
	GarmentLegs = 0x1104
	GarmentGloves = 0x1105
	GarmentFeet = 0x1106
	# Protector
	ProtectorHead = 0x1201
	ProtectorShoulder = 0x1202
	ProtectorBody = 0x1203
	ProtectorLegs = 0x1204
	ProtectorGloves = 0x1205
	ProtectorFeet = 0x1206
	# Armor
	ArmorHead = 0x1301
	ArmorShoulder = 0x1302
	ArmorBody = 0x1303
	ArmorLegs = 0x1304
	ArmorGloves = 0x1305
	ArmorFeet = 0x1306
	# Shield
	ShieldCH = 0x1401
	ShieldEU = 0x1402
	# CH Accessory
	EarringCH = 0x1501
	NecklaceCH = 0x1502
	RingCH = 0x1503
	# Robe
	RobeHead = 0x1901
	RobeShoulder = 0x1902
	RobeBody = 0x1903
	RobeLegs = 0x1904
	RobeGloves = 0x1905
	RobeFeet = 0x1906
	# LA
	LightArmorHead = 0x1A01
	LightArmorShoulder = 0x1A02
	LightArmorBody = 0x1A03
	LightArmorLegs = 0x1A04
	LightArmorGloves = 0x1A05
	LightArmorFeet = 0x1A06
	# HA
	HeavyArmorHead = 0x1B01
	HeavyArmorShoulder = 0x1B02
	HeavyArmorBody = 0x1B03
	HeavyArmorLegs = 0x1B04
	HeavyArmorGloves = 0x1B05
	HeavyArmorFeet = 0x1B06
	# Weapon
	Sword = 0x1602
	Blade = 0x1603
	Spear = 0x1604
	Glaive = 0x1605
	Bow = 0x1606
	SwordOnehander = 0x1607
	SwordTwohander = 0x1608
	Axe = 0x1609
	Darkstaff = 0x160A
	MageStaff = 0x160B
	Crossbow = 0x160C
	Dagger = 0x160D
	Harp = 0x160E
	ClericStaff = 0x160F
	# Other
	# Trade
	TraderFlag = 0x1701
	ThiefFlag = 0x1702
	HunterFlag = 0x1703
	# EU Accessory
	EarringEU = 0x1C01
	NecklaceEU = 0x1C02
	RingEU = 0x1C03
	# Itemmall
	AvatarHead = 0x1D01
	AvatarDress = 0x1D02
	AvatarAttachment = 0x1D03
	DevilsSpirit = 0x1E01
	# Pet
	GrowthPet = 0x2101
	AbilityPet = 0x2102
	MagicCube = 0x2301
	# Consumables
	# Potion
	HPPot = 0x3101
	HPGrain = 0x013101
	MPPot = 0x3102
	MPGrain = 0x013102
	VigorPot = 0x3103
	VigorGrain = 0x013103
	PetHPPotion = 0x3104
	GrassOfLife = 0x3106
	HGPPotion = 0x3109
	FortRepair = 0x310A
	# BadStatus
	PurificationPill = 0x3201
	UniversalPill = 0x3206
	PetPill = 0x3207
	#
	ReturnScroll = 0x3301
	PetVehicle = 0x3302
	PetRide = 0x013302  # COS_C
	PetTransport = 0x023302  # COS_T Trading
	ReverseReturnScroll = 0x3303
	StallDecoration = 0x3304
	GlobalChatting = 0x3305
	FortMonsterTablet = 0x3306
	GuildMonsterScroll = 0x3307
	FortManual = 0x3308
	FortFlag = 0x3309
	EXP_SP_or_Zerk_Scroll = 0x330A
	EXPScroll = 0x01330A
	SPScroll = 0x02330A
	ZerkScroll = 0x03330A
	FortScroll = 0x330B
	SkillPointScroll = 0x330C
	ItemPlusEnhanceScroll = 0x330E
	# Ammo
	Bolt = 0x3402
	Arrow = 0x3401
	# Gold
	Gold = 0x3500
	GoldTrade = 0x3501
	ItemTrade = 0x3801
	ItemQuest = 0x3900
	# alchemy
	Elixir = 0x3A01
	WeaponElixir = 0x013A01
	ShieldElixir = 0x023A01
	ProtectorElixir = 0x033A01
	AccessoryElixir = 0x043A01
	LuckyPowder = 0x3A02
	AdvancedElixir = 0x3A04
	# Stones
	MagicStone = 0x3B01
	MagicStoneStrength = 0x013B01
	MagicStoneIntelligence = 0x023B01
	MagicStoneMaster = 0x033B01
	MagicStoneStrikes = 0x043B01
	MagicStoneDiscipline = 0x053B01
	MagicStonePenetration = 0x063B01
	MagicStoneDodging = 0x073B01
	MagicStoneStamina = 0x083B01
	MagicStoneMagic = 0x093B01
	MagicStoneFogs = 0x0A3B01
	MagicStoneAir = 0x0B3B01
	MagicStoneFire = 0x0C3B01
	MagicStoneImmunity = 0x0D3B01
	MagicStoneRevival = 0x0E3B01
	MagicStoneSteady = 0x0F3B01
	MagicStoneLuck = 0x103B01
	MagicStoneAstral = 0x113B01
	MagicStoneImmortal = 0x123B01
	AttributeStone = 0x3B02
	AttributeStoneCourage = 0x013B02
	AttributeStoneWarriors = 0x023B02
	AttributeStonePhilosophy = 0x033B02
	AttributeStoneMeditation = 0x043B02
	AttributeStoneChallenge = 0x053B02
	AttributeStoneFocus = 0x063B02
	AttributeStoneFlesh = 0x073B02
	AttributeStoneLife = 0x083B02
	AttributeStoneMind = 0x093B02
	AttributeStoneSpirit = 0x0A3B02
	AttributeStoneDodging = 0x0B3B02
	AttributeStoneAgility = 0x0C3B02
	AttributeStoneTraining = 0x0D3B02
	AttributeStonePrayer = 0x0E3B02
	# Tablets
	AlchemyTablet = 0x3B03
	JadeTabletStrength = 0x013B03
	JadeTabletIntelligence = 0x023B03
	JadeTabletMaster = 0x033B03
	JadeTabletStrikes = 0x043B03
	JadeTabletDiscipline = 0x053B03
	JadeTabletPenetration = 0x063B03
	JadeTabletDodging = 0x073B03
	JadeTabletStamina = 0x083B03
	JadeTabletMagic = 0x093B03
	JadeTabletFogs = 0x0A3B03
	JadeTabletAir = 0x0B3B03
	JadeTabletFire = 0x0C3B03
	JadeTabletImmunity = 0x0D3B03
	JadeTabletRevival = 0x0E3B03
	JadeTabletSteady = 0x0F3B03
	JadeTabletLuck = 0x103B03
	RubyTabletCourage = 0x113B03
	RubyTabletWarriors = 0x123B03
	RubyTabletPhilosophy = 0x133B03
	RubyTabletMeditation = 0x143B03
	RubyTabletChallenge = 0x153B03
	RubyTabletFocus = 0x163B03
	RubyTabletFlesh = 0x173B03
	RubyTabletLife = 0x183B03
	RubyTabletMind = 0x193B03
	RubyTabletSpirit = 0x1A3B03
	RubyTabletDodging = 0x1B3B03
	RubyTabletAgility = 0x1C3B03
	RubyTabletTraining = 0x1D3B03
	RubyTabletPrayer = 0x1E3B03
	AlchemyMaterial = 0x3B04
	AlchemyElement = 0x3B05
	DestructionRondo = 0x3B06
	MagicStoneMall = 0x3B07  # Immortality
	MagicPOP = 0x3E01
	ItemExchangeCoupon = 0x3E02
	BuffScroll = 0x3D01	 # ITEM_ETC_E051123_xxx_SCROLL
	ParryScroll = 0x013D01
	HitRatioScroll = 0x023D01
	SpeedScroll = 0x033D01
	TriggerScroll = 0x043D01
	HPScroll = 0x053D01
	MPScroll = 0x063D01
	SpeedPotion = 0x073D01
	DamageIncreaseScroll = 0x083D01
	DamageAbsorbtionScroll = 0x093D01
	EXPBoostScroll = 0x3D04
	SPBoostScroll = 0x3D05
	RepairHammer = 0x3D07
	ItemGenderSwitchScroll = 0x3D08
	SkinChangeScroll = 0x3D09
	ItemMallScroll = 0x3D0E  # TODO get other mall scrolls including Pet Growth Potion
	Premium = 0x013D0E  # TODO get other mall scrolls including Pet Growth Potion
	PetWatch = 0x3D0F
	ExpansionScroll = 0x3D11
	InventoryExpansionScroll = 0x013D11
	StorageExpansionScroll = 0x023D14

	def get_attack_range(self) -> float:
		"""Returns the character's attack range based on weapon"""
		# switch (pk2ItemType)

		if self == PK2ItemType.Sword \
			or self == PK2ItemType.Blade:
			return 0.6
		elif self == PK2ItemType.Spear \
			or self == PK2ItemType.Glaive:
			return 1.8
		elif self == PK2ItemType.Bow:
			return 18
		elif self == PK2ItemType.SwordOnehander:
			return 0.6
		elif self == PK2ItemType.SwordTwohander:
			return 1.8
		elif self == PK2ItemType.Axe:
			return 0.6
		elif self == PK2ItemType.Darkstaff:
			return 6
		elif self == PK2ItemType.MageStaff:
			return 6
		elif self == PK2ItemType.Crossbow:
			return 18
		elif self == PK2ItemType.Dagger:
			return 3
		elif self == PK2ItemType.Harp:
			return 6
		elif self == PK2ItemType.ClericStaff:
			return 6
		else:
			return 0.6

	def get_required_weapon_slot(self) -> EquipmentSlot:
		# switch (pk2ItemType)
		if self == PK2ItemType.ShieldCH or self == PK2ItemType.ShieldEU:
			return EquipmentSlot.Secondary.value
		else:
			return EquipmentSlot.Primary.value

	def is_weapon_with_shield(self) -> bool:
		# switch (pk2ItemType)
		if self == PK2ItemType.Blade \
				or self == PK2ItemType.Sword \
				or self == PK2ItemType.SwordOnehander \
				or self == PK2ItemType.Darkstaff \
				or self == PK2ItemType.ClericStaff:
			return True
		else:
			return False

	def is_2_slot_weapon(self) -> bool:
		# switch (pk2ItemType)
		if self == PK2ItemType.Glaive \
				or self == PK2ItemType.Spear \
				or self == PK2ItemType.SwordTwohander \
				or self == PK2ItemType.Axe \
				or self == PK2ItemType.MageStaff \
				or self == PK2ItemType.Harp:
			return True
		else:
			return False

	def is_magic_stone(self) -> bool:
		return self.value & 0xFFFF == 0x3B01

	def is_attribute_stone(self) -> bool:
		return self.value & 0xFFFF == 0x3B02

	def is_jade_tablet(self) -> bool:
		return self.value >> 16 <= 0x10

	def is_ruby_tablet(self) -> bool:
		return self.value >> 16 >= 0x10


class Gender(Enum):
	Male = 0
	Female = 1
	Unisex = 2


class Country(Enum):
	Chinese = 0
	Euro = 1
	Universal = 2
	NoCountry = 3
	"""Biggest ref_id for chinese characters"""
	MAX_CHINESE = 1932


class PK2Item(IPK2Type):
	def __init__(self, ref_id: str, long_id: str, name: str, is_mall_item: str, type_value: str, country: str, is_sox: str,
				is_storable: str, level: str, max_stacks: str, gender: str, degree: str):
		super(PK2Item, self).__init__(ref_id, long_id, name)
		self.ID = int(ref_id)  # c_uint32(id)  non hashable
		self.long_id = long_id
		self.name = name
		self.is_mall_item = is_mall_item == "1"
		self.type = PK2Item.get_item_type(type_value, long_id)
		self.country = Country(int(country)) if int(country) in Country._value2member_map_ else Country.Universal
		self.is_SOX = is_sox == "1"
		self.is_storable = int(is_storable) > 0
		self.level = int(level)
		self.max_stacks = int(max_stacks)
		self.gender = Gender(int(gender)) if gender in Gender._value2member_map_ else Gender.Unisex
		self.degree = int(degree)

	def __repr__(self):
		return f"{self.ID}|{self.name}"

	def __str__(self):
		return f"{self.ID}|{self.long_id}|{self.name}|{int(self.is_mall_item)}|{format(self.type.value, 'x')}" \
			f"|{self.country.value}|{int(self.is_SOX)}|{int(self.is_storable)}|{self.level}|{self.max_stacks}" \
			f"|{self.gender.value}|{self.degree}"

	def to_type(self) -> IPK2Type:
		return self

	def is_consumable(self) -> bool:
		"""Return whether the item type is a type that counts as a consumable.
		Type c_uint32-value is bitshifted to see whether the MSB equals 1."""
		return (self.type.value >> 12) == 3

	def is_equipment_item(self) -> bool:
		"""Return whether the item type is a type that counts as equipment.
		Type c_uint32-value is bitshifted to see whether the MSB equals 1."""
		return (self.type.value >> 12) == 1

	def is_weapon(self) -> bool:
		"""Return whether the item type is a type that counts as a weapon.
			A bitwise AND comparison with the basic weapon flags (0x160) should result in 0x160:"""

		# 0x100 for equipment and 0x060 for weapon.
		# 0x1E1 can cause false positive, so check that the difference is only in the lowest byte (up to 0xF)
		return (self.type.value ^ 0x1600) < 0x10

	def is_shield(self) -> bool:
		"""Return whether the item type is a type that counts as a shield.
			A bitwise AND comparison with the basic shield flags (0x140) should result in 0x140:"""

		# 0x100 for equipment and 0x040 for shield. If 0x300 then bolt or arrow."""
		return self.type.value in [0x1401, 0x1402]

	def is_clothing(self) -> bool:
		"""Return whether the item type is a type that counts as apparel.
			It should be an equipment item that is not a shield, or: accessory weapon"""
		return self.is_equipment_item() and not (
			self.is_shield() or self.is_weapon() or self.is_accessory()
		)

	def is_accessory(self) -> bool:
		"""Return whether the item type is a type that counts as an accessory.
			It either has the CH accessory flags (0x150) or EU accessory flags (0x1C0)."""
		return self.type.value in [0x1501, 0x1502, 0x1503, 0x1C01, 0x1C02, 0x1C03]

	def is_durability_item(self) -> bool:
		return self.is_shield() or self.is_weapon() or self.is_clothing()

	type_filters: Dict[PK2ItemType, Tuple[str, str]] = {
		# Other = 0x01
		# Equipment = 0x110

		# Garment
		PK2ItemType.GarmentHead: (ItemFilterSettings.Head, settings['Filter']['DegreesAll']),
		PK2ItemType.GarmentShoulder: (ItemFilterSettings.Shoulder, settings['Filter']['DegreesAll']),
		PK2ItemType.GarmentBody: (ItemFilterSettings.Chest, settings['Filter']['DegreesAll']),
		PK2ItemType.GarmentLegs: (ItemFilterSettings.Hose, settings['Filter']['DegreesAll']),
		PK2ItemType.GarmentGloves: (ItemFilterSettings.Glove, settings['Filter']['DegreesAll']),
		PK2ItemType.GarmentFeet: (ItemFilterSettings.Boots, settings['Filter']['DegreesAll']),
		# Protector
		PK2ItemType.ProtectorHead: (ItemFilterSettings.Head, settings['Filter']['DegreesAll']),
		PK2ItemType.ProtectorShoulder: (ItemFilterSettings.Shoulder, settings['Filter']['DegreesAll']),
		PK2ItemType.ProtectorBody: (ItemFilterSettings.Chest, settings['Filter']['DegreesAll']),
		PK2ItemType.ProtectorLegs: (ItemFilterSettings.Hose, settings['Filter']['DegreesAll']),
		PK2ItemType.ProtectorGloves: (ItemFilterSettings.Glove, settings['Filter']['DegreesAll']),
		PK2ItemType.ProtectorFeet: (ItemFilterSettings.Boots, settings['Filter']['DegreesAll']),
		# Armor
		PK2ItemType.ArmorHead: (ItemFilterSettings.Head, settings['Filter']['DegreesAll']),
		PK2ItemType.ArmorShoulder: (ItemFilterSettings.Shoulder, settings['Filter']['DegreesAll']),
		PK2ItemType.ArmorBody: (ItemFilterSettings.Chest, settings['Filter']['DegreesAll']),
		PK2ItemType.ArmorLegs: (ItemFilterSettings.Hose, settings['Filter']['DegreesAll']),
		PK2ItemType.ArmorGloves: (ItemFilterSettings.Glove, settings['Filter']['DegreesAll']),
		PK2ItemType.ArmorFeet: (ItemFilterSettings.Boots, settings['Filter']['DegreesAll']),

		# Shield
		PK2ItemType.ShieldCH: (ItemFilterSettings.Shield, settings['Filter']['DegreesAll']),
		PK2ItemType.ShieldEU: (ItemFilterSettings.EUShield, settings['Filter']['DegreesAll']),

		# CH Accessory
		PK2ItemType.EarringCH: (ItemFilterSettings.Earrings, settings['Filter']['DegreesAll']),
		PK2ItemType.NecklaceCH: (ItemFilterSettings.Necklace, settings['Filter']['DegreesAll']),
		PK2ItemType.RingCH: (ItemFilterSettings.Ring, settings['Filter']['DegreesAll']),

		# Robe
		PK2ItemType.RobeHead: (ItemFilterSettings.EUHead, settings['Filter']['DegreesAll']),
		PK2ItemType.RobeShoulder: (ItemFilterSettings.EUShoulder, settings['Filter']['DegreesAll']),
		PK2ItemType.RobeBody: (ItemFilterSettings.EUChest, settings['Filter']['DegreesAll']),
		PK2ItemType.RobeLegs: (ItemFilterSettings.EUHose, settings['Filter']['DegreesAll']),
		PK2ItemType.RobeGloves: (ItemFilterSettings.EUGloves, settings['Filter']['DegreesAll']),
		PK2ItemType.RobeFeet: (ItemFilterSettings.EUBoots, settings['Filter']['DegreesAll']),
		# LA
		PK2ItemType.LightArmorHead: (ItemFilterSettings.EUHead, settings['Filter']['DegreesAll']),
		PK2ItemType.LightArmorShoulder: (ItemFilterSettings.EUShoulder, settings['Filter']['DegreesAll']),
		PK2ItemType.LightArmorBody: (ItemFilterSettings.EUChest, settings['Filter']['DegreesAll']),
		PK2ItemType.LightArmorLegs: (ItemFilterSettings.EUHose, settings['Filter']['DegreesAll']),
		PK2ItemType.LightArmorGloves: (ItemFilterSettings.EUGloves, settings['Filter']['DegreesAll']),
		PK2ItemType.LightArmorFeet: (ItemFilterSettings.EUBoots, settings['Filter']['DegreesAll']),
		# HA
		PK2ItemType.HeavyArmorHead: (ItemFilterSettings.EUHead, settings['Filter']['DegreesAll']),
		PK2ItemType.HeavyArmorShoulder: (ItemFilterSettings.EUShoulder, settings['Filter']['DegreesAll']),
		PK2ItemType.HeavyArmorBody: (ItemFilterSettings.EUChest, settings['Filter']['DegreesAll']),
		PK2ItemType.HeavyArmorLegs: (ItemFilterSettings.EUHose, settings['Filter']['DegreesAll']),
		PK2ItemType.HeavyArmorGloves: (ItemFilterSettings.EUGloves, settings['Filter']['DegreesAll']),
		PK2ItemType.HeavyArmorFeet: (ItemFilterSettings.EUBoots, settings['Filter']['DegreesAll']),

		# Weapon
		PK2ItemType.Sword: (ItemFilterSettings.Sword, settings['Filter']['DegreesAll']),
		PK2ItemType.Blade: (ItemFilterSettings.Blade, settings['Filter']['DegreesAll']),
		PK2ItemType.Spear: (ItemFilterSettings.Spear, settings['Filter']['DegreesAll']),
		PK2ItemType.Glaive: (ItemFilterSettings.Glaive, settings['Filter']['DegreesAll']),
		PK2ItemType.Bow: (ItemFilterSettings.Bow, settings['Filter']['DegreesAll']),
		PK2ItemType.SwordOnehander: (ItemFilterSettings.EUSword1H, settings['Filter']['DegreesAll']),
		PK2ItemType.SwordTwohander: (ItemFilterSettings.EUSword2H, settings['Filter']['DegreesAll']),
		PK2ItemType.Axe: (ItemFilterSettings.EUAxe, settings['Filter']['DegreesAll']),
		PK2ItemType.Darkstaff: (ItemFilterSettings.EUWarlockStaff, settings['Filter']['DegreesAll']),
		PK2ItemType.MageStaff: (ItemFilterSettings.EUWizardStaff, settings['Filter']['DegreesAll']),
		PK2ItemType.Crossbow: (ItemFilterSettings.EUCrossbow, settings['Filter']['DegreesAll']),
		PK2ItemType.Dagger: (ItemFilterSettings.EUDagger, settings['Filter']['DegreesAll']),
		PK2ItemType.Harp: (ItemFilterSettings.EUHarp, settings['Filter']['DegreesAll']),
		PK2ItemType.ClericStaff: (ItemFilterSettings.EUClericStaff, settings['Filter']['DegreesAll']),
		# Other
		# Trade
		PK2ItemType.TraderFlag: (None, None),
		PK2ItemType.ThiefFlag: (None, None),
		PK2ItemType.HunterFlag: (None, None),

		# EU Accessory
		PK2ItemType.EarringEU: (ItemFilterSettings.EUEarrings, settings['Filter']['DegreesAll']),
		PK2ItemType.NecklaceEU: (ItemFilterSettings.EUNecklace, settings['Filter']['DegreesAll']),
		PK2ItemType.RingEU: (ItemFilterSettings.EURing, settings['Filter']['DegreesAll']),

		# Itemmall
		PK2ItemType.AvatarHead: (None, None),
		PK2ItemType.AvatarDress: (None, None),
		PK2ItemType.AvatarAttachment: (None, None),
		PK2ItemType.DevilsSpirit: (None, None),

		# Pet
		PK2ItemType.GrowthPet: (None, None),
		PK2ItemType.AbilityPet: (None, None),

		PK2ItemType.MagicCube: (None, None),

		# Consumables
		# Potion
		PK2ItemType.HPPot: (ItemFilterSettings.HPPots, None),
		PK2ItemType.HPGrain: (ItemFilterSettings.Grains, None),
		PK2ItemType.MPPot: (ItemFilterSettings.MPPots, None),
		PK2ItemType.MPGrain: (ItemFilterSettings.Grains, None),
		PK2ItemType.VigorPot: (ItemFilterSettings.VigorPots, None),
		PK2ItemType.VigorGrain: (ItemFilterSettings.Grains, None),
		PK2ItemType.PetHPPotion: (ItemFilterSettings.COSItems, None),
		PK2ItemType.GrassOfLife: (ItemFilterSettings.COSItems, None),
		PK2ItemType.HGPPotion: (ItemFilterSettings.COSItems, None),
		PK2ItemType.FortRepair: (None, None),
		# BadStatus
		PK2ItemType.PurificationPill: (ItemFilterSettings.PurificationPills, None),
		PK2ItemType.UniversalPill: (ItemFilterSettings.UniversalPills, None),
		PK2ItemType.PetPill: (ItemFilterSettings.COSItems, None),

		#
		PK2ItemType.ReturnScroll: (ItemFilterSettings.ReturnScrolls, None),
		PK2ItemType.PetVehicle: (ItemFilterSettings.COSItems, None),
		PK2ItemType.PetRide: (ItemFilterSettings.COSItems, None),
		PK2ItemType.PetTransport: (ItemFilterSettings.COSItems, None),
		PK2ItemType.ReverseReturnScroll: (ItemFilterSettings.ReturnScrolls, None),
		PK2ItemType.StallDecoration: (None, None),
		PK2ItemType.GlobalChatting: (None, None),
		PK2ItemType.FortMonsterTablet: (None, None),
		PK2ItemType.GuildMonsterScroll: (None, None),
		PK2ItemType.FortManual: (None, None),
		PK2ItemType.FortFlag: (None, None),
		PK2ItemType.EXP_SP_or_Zerk_Scroll: (None, None),
		PK2ItemType.FortScroll: (None, None),
		PK2ItemType.SkillPointScroll: (None, None),
		PK2ItemType.ItemPlusEnhanceScroll: (None, None),

		# Ammo
		PK2ItemType.Bolt: (ItemFilterSettings.Bolts, None),
		PK2ItemType.Arrow: (ItemFilterSettings.Arrows, None),

		# Gold
		PK2ItemType.Gold: (f"{'1' if ItemFilterSettings.Gold else '0'}000", None),
		PK2ItemType.GoldTrade: (f"{'1' if ItemFilterSettings.Gold else '0'}000", None),

		PK2ItemType.ItemTrade: (None, None),
		PK2ItemType.ItemQuest: (f"{'1' if ItemFilterSettings.QuestItems else '0'}000", None),

		# alchemy
		PK2ItemType.WeaponElixir: (ItemFilterSettings.WeaponElixir, None),
		PK2ItemType.ShieldElixir: (ItemFilterSettings.ShieldElixir, None),
		PK2ItemType.ProtectorElixir: (ItemFilterSettings.ProtectorElixir, None),
		PK2ItemType.AccessoryElixir: (ItemFilterSettings.AccessoryElixir, None),
		PK2ItemType.LuckyPowder: (ItemFilterSettings.AlchemyMaterial, None),
		PK2ItemType.AdvancedElixir: (ItemFilterSettings.AlchemyMaterial, None),

		PK2ItemType.MagicStoneStrength: (AlchemyFilterSettings.FilterJadeStrength, settings['Filter']['DegreesAll']),
		PK2ItemType.MagicStoneIntelligence:
			(AlchemyFilterSettings.FilterJadeIntelligence, settings['Filter']['DegreesAll']),
		PK2ItemType.MagicStoneMaster: (AlchemyFilterSettings.FilterJadeMaster, settings['Filter']['DegreesAll']),
		PK2ItemType.MagicStoneStrikes: (AlchemyFilterSettings.FilterJadeStrikes, settings['Filter']['DegreesAll']),
		PK2ItemType.MagicStoneDiscipline:
			(AlchemyFilterSettings.FilterJadeDiscipline, settings['Filter']['DegreesAll']),
		PK2ItemType.MagicStonePenetration:
			(AlchemyFilterSettings.FilterJadePenetration, settings['Filter']['DegreesAll']),
		PK2ItemType.MagicStoneDodging: (AlchemyFilterSettings.FilterJadeDodging, settings['Filter']['DegreesAll']),
		PK2ItemType.MagicStoneStamina: (AlchemyFilterSettings.FilterJadeStamina, settings['Filter']['DegreesAll']),
		PK2ItemType.MagicStoneMagic: (AlchemyFilterSettings.FilterJadeMagic, settings['Filter']['DegreesAll']),
		PK2ItemType.MagicStoneFogs: (AlchemyFilterSettings.FilterJadeFogs, settings['Filter']['DegreesAll']),
		PK2ItemType.MagicStoneAir: (AlchemyFilterSettings.FilterJadeAir, settings['Filter']['DegreesAll']),
		PK2ItemType.MagicStoneFire: (AlchemyFilterSettings.FilterJadeFire, settings['Filter']['DegreesAll']),
		PK2ItemType.MagicStoneImmunity: (AlchemyFilterSettings.FilterJadeImmunity, settings['Filter']['DegreesAll']),
		PK2ItemType.MagicStoneRevival: (AlchemyFilterSettings.FilterJadeRevival, settings['Filter']['DegreesAll']),
		PK2ItemType.MagicStoneSteady: (AlchemyFilterSettings.FilterJadeSteady, settings['Filter']['DegreesAll']),
		PK2ItemType.MagicStoneLuck: (AlchemyFilterSettings.FilterJadeLuck, settings['Filter']['DegreesAll']),
		PK2ItemType.AttributeStoneCourage:
			(AlchemyFilterSettings.FilterRubyCourage, settings['Filter']['DegreesAll']),
		PK2ItemType.AttributeStoneWarriors:
			(AlchemyFilterSettings.FilterRubyWarriors, settings['Filter']['DegreesAll']),
		PK2ItemType.AttributeStonePhilosophy:
			(AlchemyFilterSettings.FilterRubyPhilosophy, settings['Filter']['DegreesAll']),
		PK2ItemType.AttributeStoneMeditation:
			(AlchemyFilterSettings.FilterRubyMeditation, settings['Filter']['DegreesAll']),
		PK2ItemType.AttributeStoneChallenge:
			(AlchemyFilterSettings.FilterRubyChallenge, settings['Filter']['DegreesAll']),
		PK2ItemType.AttributeStoneFocus: (AlchemyFilterSettings.FilterRubyFocus, settings['Filter']['DegreesAll']),
		PK2ItemType.AttributeStoneFlesh: (AlchemyFilterSettings.FilterRubyFlesh, settings['Filter']['DegreesAll']),
		PK2ItemType.AttributeStoneLife: (AlchemyFilterSettings.FilterRubyLife, settings['Filter']['DegreesAll']),
		PK2ItemType.AttributeStoneMind: (AlchemyFilterSettings.FilterRubyMind, settings['Filter']['DegreesAll']),
		PK2ItemType.AttributeStoneSpirit: (AlchemyFilterSettings.FilterRubySpirit, settings['Filter']['DegreesAll']),
		PK2ItemType.AttributeStoneDodging:
			(AlchemyFilterSettings.FilterRubyDodging, settings['Filter']['DegreesAll']),
		PK2ItemType.AttributeStoneAgility:
			(AlchemyFilterSettings.FilterRubyAgility, settings['Filter']['DegreesAll']),
		PK2ItemType.AttributeStoneTraining:
			(AlchemyFilterSettings.FilterRubyTraining, settings['Filter']['DegreesAll']),
		PK2ItemType.AttributeStonePrayer: (AlchemyFilterSettings.FilterRubyPrayer, settings['Filter']['DegreesAll']),
		PK2ItemType.JadeTabletStrength: (AlchemyFilterSettings.JadeTabletStrength, settings['Filter']['DegreesAll']),
		PK2ItemType.JadeTabletIntelligence:
			(AlchemyFilterSettings.JadeTabletIntelligence, settings['Filter']['DegreesAll']),
		PK2ItemType.JadeTabletMaster: (AlchemyFilterSettings.JadeTabletMaster, settings['Filter']['DegreesAll']),
		PK2ItemType.JadeTabletStrikes: (AlchemyFilterSettings.JadeTabletStrikes, settings['Filter']['DegreesAll']),
		PK2ItemType.JadeTabletDiscipline:
			(AlchemyFilterSettings.JadeTabletDiscipline, settings['Filter']['DegreesAll']),
		PK2ItemType.JadeTabletPenetration:
			(AlchemyFilterSettings.JadeTabletPenetration, settings['Filter']['DegreesAll']),
		PK2ItemType.JadeTabletDodging: (AlchemyFilterSettings.JadeTabletDodging, settings['Filter']['DegreesAll']),
		PK2ItemType.JadeTabletStamina: (AlchemyFilterSettings.JadeTabletStamina, settings['Filter']['DegreesAll']),
		PK2ItemType.JadeTabletMagic: (AlchemyFilterSettings.JadeTabletMagic, settings['Filter']['DegreesAll']),
		PK2ItemType.JadeTabletFogs: (AlchemyFilterSettings.JadeTabletFogs, settings['Filter']['DegreesAll']),
		PK2ItemType.JadeTabletAir: (AlchemyFilterSettings.JadeTabletAir, settings['Filter']['DegreesAll']),
		PK2ItemType.JadeTabletFire: (AlchemyFilterSettings.JadeTabletFire, settings['Filter']['DegreesAll']),
		PK2ItemType.JadeTabletImmunity: (AlchemyFilterSettings.JadeTabletImmunity, settings['Filter']['DegreesAll']),
		PK2ItemType.JadeTabletRevival: (AlchemyFilterSettings.JadeTabletRevival, settings['Filter']['DegreesAll']),
		PK2ItemType.JadeTabletSteady: (AlchemyFilterSettings.JadeTabletSteady, settings['Filter']['DegreesAll']),
		PK2ItemType.JadeTabletLuck: (AlchemyFilterSettings.JadeTabletLuck, settings['Filter']['DegreesAll']),
		PK2ItemType.RubyTabletCourage: (AlchemyFilterSettings.RubyTabletCourage, settings['Filter']['DegreesAll']),
		PK2ItemType.RubyTabletWarriors: (AlchemyFilterSettings.RubyTabletWarriors, settings['Filter']['DegreesAll']),
		PK2ItemType.RubyTabletPhilosophy:
			(AlchemyFilterSettings.RubyTabletPhilosophy, settings['Filter']['DegreesAll']),
		PK2ItemType.RubyTabletMeditation:
			(AlchemyFilterSettings.RubyTabletMeditation, settings['Filter']['DegreesAll']),
		PK2ItemType.RubyTabletChallenge:
			(AlchemyFilterSettings.RubyTabletChallenge, settings['Filter']['DegreesAll']),
		PK2ItemType.RubyTabletFocus: (AlchemyFilterSettings.RubyTabletFocus, settings['Filter']['DegreesAll']),
		PK2ItemType.RubyTabletFlesh: (AlchemyFilterSettings.RubyTabletFlesh, settings['Filter']['DegreesAll']),
		PK2ItemType.RubyTabletLife: (AlchemyFilterSettings.RubyTabletLife, settings['Filter']['DegreesAll']),
		PK2ItemType.RubyTabletMind: (AlchemyFilterSettings.RubyTabletMind, settings['Filter']['DegreesAll']),
		PK2ItemType.RubyTabletSpirit: (AlchemyFilterSettings.RubyTabletSpirit, settings['Filter']['DegreesAll']),
		PK2ItemType.RubyTabletDodging: (AlchemyFilterSettings.RubyTabletDodging, settings['Filter']['DegreesAll']),
		PK2ItemType.RubyTabletAgility: (AlchemyFilterSettings.RubyTabletAgility, settings['Filter']['DegreesAll']),
		PK2ItemType.RubyTabletTraining: (AlchemyFilterSettings.RubyTabletTraining, settings['Filter']['DegreesAll']),
		PK2ItemType.RubyTabletPrayer: (AlchemyFilterSettings.RubyTabletPrayer, settings['Filter']['DegreesAll']),

		PK2ItemType.AlchemyMaterial: (ItemFilterSettings.AlchemyMaterial, None),
		PK2ItemType.AlchemyElement: (ItemFilterSettings.AlchemyMaterial, settings['Filter']['DegreesAll']),
		PK2ItemType.DestructionRondo: (ItemFilterSettings.AlchemyMaterial, None),
		PK2ItemType.MagicStoneMall: (ItemFilterSettings.QuestItems, settings['Filter']['DegreesAll']),

		PK2ItemType.MagicPOP: (None, None),
		PK2ItemType.ItemExchangeCoupon: (None, None),

		# InventoryExpansionItem = 0x3D 11, --> 3 3 13 17
		PK2ItemType.BuffScroll: (ItemFilterSettings.QuestItems, None),  # INT, STR, Speed
		PK2ItemType.SpeedPotion: (ItemFilterSettings.QuestItems, None),
		PK2ItemType.EXPBoostScroll: (ItemFilterSettings.QuestItems, None),
		PK2ItemType.SPBoostScroll: (ItemFilterSettings.QuestItems, None),
		PK2ItemType.RepairHammer: (ItemFilterSettings.QuestItems, None),
		PK2ItemType.ItemGenderSwitchScroll: (ItemFilterSettings.QuestItems, None),
		PK2ItemType.SkinChangeScroll: (ItemFilterSettings.QuestItems, None),

		PK2ItemType.Premium: (ItemFilterSettings.QuestItems, None),  # including Pet Growth Potion
		PK2ItemType.PetWatch: (ItemFilterSettings.QuestItems, None)
	}

	@staticmethod
	def get_elixir_type(long_id: str) -> PK2ItemType:
		if long_id.startswith("ITEM_ETC_ARCHEMY_REINFORCE_RECIPE_WEAPON"):
			return PK2ItemType.WeaponElixir
		elif long_id.startswith("ITEM_ETC_ARCHEMY_REINFORCE_RECIPE_SHIELD"):
			return PK2ItemType.ShieldElixir
		elif long_id.startswith("ITEM_ETC_ARCHEMY_REINFORCE_RECIPE_ARMOR"):
			return PK2ItemType.ProtectorElixir
		elif long_id.startswith("ITEM_ETC_ARCHEMY_REINFORCE_RECIPE_ACCESSARY"):
			return PK2ItemType.AccessoryElixir
		else:
			return PK2ItemType.Elixir

	@staticmethod
	def get_magic_stone_type(long_id: str) -> PK2ItemType:
		if long_id.startswith("ITEM_ETC_ARCHEMY_MAGICSTONE_STR"):
			return PK2ItemType.MagicStoneStrength
		elif long_id.startswith("ITEM_ETC_ARCHEMY_MAGICSTONE_INT"):
			return PK2ItemType.MagicStoneIntelligence
		elif long_id.startswith("ITEM_ETC_ARCHEMY_MAGICSTONE_DUR"):
			return PK2ItemType.MagicStoneMaster
		elif long_id.startswith("ITEM_ETC_ARCHEMY_MAGICSTONE_HR"):
			return PK2ItemType.MagicStoneStrikes
		elif long_id.startswith("ITEM_ETC_ARCHEMY_MAGICSTONE_EVADE_BLOCK"):
			return PK2ItemType.MagicStoneDiscipline
		elif long_id.startswith("ITEM_ETC_ARCHEMY_MAGICSTONE_EVADE_CRITICAL"):
			return PK2ItemType.MagicStonePenetration
		elif long_id.startswith("ITEM_ETC_ARCHEMY_MAGICSTONE_ER"):
			return PK2ItemType.MagicStoneDodging
		elif long_id.startswith("ITEM_ETC_ARCHEMY_MAGICSTONE_HP"):
			return PK2ItemType.MagicStoneStamina
		elif long_id.startswith("ITEM_ETC_ARCHEMY_MAGICSTONE_MP"):
			return PK2ItemType.MagicStoneMagic
		elif long_id.startswith("ITEM_ETC_ARCHEMY_MAGICSTONE_FROSTBITE"):
			return PK2ItemType.MagicStoneFogs
		elif long_id.startswith("ITEM_ETC_ARCHEMY_MAGICSTONE_ESHOCK"):
			return PK2ItemType.MagicStoneAir
		elif long_id.startswith("ITEM_ETC_ARCHEMY_MAGICSTONE_BURN"):
			return PK2ItemType.MagicStoneFire
		elif long_id.startswith("ITEM_ETC_ARCHEMY_MAGICSTONE_POISON"):
			return PK2ItemType.MagicStoneImmunity
		elif long_id.startswith("ITEM_ETC_ARCHEMY_MAGICSTONE_ZOMBIE"):
			return PK2ItemType.MagicStoneRevival
		elif long_id.startswith("ITEM_ETC_ARCHEMY_MAGICSTONE_SOLID"):
			return PK2ItemType.MagicStoneSteady
		elif long_id.startswith("ITEM_ETC_ARCHEMY_MAGICSTONE_LUCK"):
			return PK2ItemType.MagicStoneLuck
		elif long_id.startswith("ITEM_ETC_ARCHEMY_MAGICSTONE_ASTRAL"):
			return PK2ItemType.MagicStoneAstral
		elif long_id.startswith("ITEM_ETC_ARCHEMY_MAGICSTONE_ATHANASIA"):
			return PK2ItemType.MagicStoneImmortal
		else:
			return PK2ItemType.MagicStone

	@staticmethod
	def get_attribute_stone_type(long_id: str) -> PK2ItemType:

		if long_id.startswith("ITEM_ETC_ARCHEMY_ATTRSTONE_PA_"):
			return PK2ItemType.AttributeStoneCourage
		elif long_id.startswith("ITEM_ETC_ARCHEMY_ATTRSTONE_PASTR"):
			return PK2ItemType.AttributeStoneWarriors
		elif long_id.startswith("ITEM_ETC_ARCHEMY_ATTRSTONE_MA_"):
			return PK2ItemType.AttributeStonePhilosophy
		elif long_id.startswith("ITEM_ETC_ARCHEMY_ATTRSTONE_MAINT"):
			return PK2ItemType.AttributeStoneMeditation
		elif long_id.startswith("ITEM_ETC_ARCHEMY_ATTRSTONE_CRITICAL"):
			return PK2ItemType.AttributeStoneChallenge
		elif long_id.startswith("ITEM_ETC_ARCHEMY_ATTRSTONE_HR"):
			return PK2ItemType.AttributeStoneFocus
		elif long_id.startswith("ITEM_ETC_ARCHEMY_ATTRSTONE_PD_"):
			return PK2ItemType.AttributeStoneFlesh
		elif long_id.startswith("ITEM_ETC_ARCHEMY_ATTRSTONE_PDSTR"):
			return PK2ItemType.AttributeStoneLife
		elif long_id.startswith("ITEM_ETC_ARCHEMY_ATTRSTONE_MD_"):
			return PK2ItemType.AttributeStoneMind
		elif long_id.startswith("ITEM_ETC_ARCHEMY_ATTRSTONE_MDINT"):
			return PK2ItemType.AttributeStoneSpirit
		elif long_id.startswith("ITEM_ETC_ARCHEMY_ATTRSTONE_ER"):
			return PK2ItemType.AttributeStoneDodging
		elif long_id.startswith("ITEM_ETC_ARCHEMY_ATTRSTONE_BR"):
			return PK2ItemType.AttributeStoneAgility
		elif long_id.startswith("ITEM_ETC_ARCHEMY_ATTRSTONE_PAR"):
			return PK2ItemType.AttributeStoneTraining
		elif long_id.startswith("ITEM_ETC_ARCHEMY_ATTRSTONE_MAR"):
			return PK2ItemType.AttributeStonePrayer
		else:
			return PK2ItemType.AttributeStone

	@staticmethod
	def get_exp_sp_zerk_scroll_type(long_id: str) -> PK2ItemType:
		if any(s in long_id for s in ["EXP_SCROLL", "EXP", "GOLD_TIME"]):
			return PK2ItemType.EXPScroll
		elif any(s in long_id for s in ["SP_SCROLL", "SP", "SKILL_EXP"]):
			return PK2ItemType.SPScroll
		elif "HWAN_TIME" in long_id:
			return PK2ItemType.ZerkScroll
		else:
			return PK2ItemType.EXP_SP_or_Zerk_Scroll

	@staticmethod
	def get_pet_vehicle_type(long_id: str) -> PK2ItemType:
		if "COS_C" in long_id:
			return PK2ItemType.PetRide
		elif "COS_T" in long_id:
			return PK2ItemType.PetTransport
		else:
			return PK2ItemType.PetVehicle

	@staticmethod
	def get_buff_scroll_type(long_id: str) -> PK2ItemType:
		if "EVATION_SCROLL" in long_id:
			return PK2ItemType.ParryScroll
		elif "POTION_SPEED" in long_id:
			return PK2ItemType.SpeedPotion
		elif "SPEED_UP" in long_id:
			return PK2ItemType.SpeedScroll
		elif "HIT" in long_id:
			return PK2ItemType.HitRatioScroll
		elif "AGILITY_SCROLL" in long_id:
			return PK2ItemType.TriggerScroll
		elif any(s in long_id for s in ["HP500_SCROLL", "HP_INC"]):
			return PK2ItemType.HPScroll
		elif any(s in long_id for s in ["MP500_SCROLL", "MP_INC"]):
			return PK2ItemType.MPScroll
		elif "DAMAGE_INC" in long_id:
			return PK2ItemType.DamageIncreaseScroll
		elif "DAMAGE_ABS" in long_id:
			return PK2ItemType.DamageAbsorbtionScroll
		else:
			return PK2ItemType.BuffScroll

	@staticmethod
	def get_item_mall_scroll_type(long_id: str) -> PK2ItemType:
		if "PREMIUM" in long_id:
			return PK2ItemType.Premium
		else:
			return PK2ItemType.ItemMallScroll

	@staticmethod
	def get_expansion_scroll_type(long_id: str) -> PK2ItemType:
		if any(s in long_id for s in ["_INVENTORY_EXTEND", "INVENTORY_ADDITION"]):
			return PK2ItemType.InventoryExpansionScroll
		elif long_id.endswith("_WAREHOUSE_ADDITION"):
			return PK2ItemType.StorageExpansionScroll
		else:
			return PK2ItemType.ExpansionScroll

	@staticmethod
	def get_tablet_type(long_id: str) -> PK2ItemType:
		if long_id.startswith("ITEM_ETC_ARCHEMY_MAGICTABLET_STR"):
			return PK2ItemType.JadeTabletStrength
		elif long_id.startswith("ITEM_ETC_ARCHEMY_MAGICTABLET_INT"):
			return PK2ItemType.JadeTabletIntelligence
		elif long_id.startswith("ITEM_ETC_ARCHEMY_MAGICTABLET_DUR"):
			return PK2ItemType.JadeTabletMaster
		elif long_id.startswith("ITEM_ETC_ARCHEMY_MAGICTABLET_HR"):
			return PK2ItemType.JadeTabletStrikes
		elif long_id.startswith("ITEM_ETC_ARCHEMY_MAGICTABLET_EVADE_BLOCK"):
			return PK2ItemType.JadeTabletDiscipline
		elif long_id.startswith("ITEM_ETC_ARCHEMY_MAGICTABLET_EVADE_CRITICAL"):
			return PK2ItemType.JadeTabletPenetration
		elif long_id.startswith("ITEM_ETC_ARCHEMY_MAGICTABLET_ER"):
			return PK2ItemType.JadeTabletDodging
		elif long_id.startswith("ITEM_ETC_ARCHEMY_MAGICTABLET_HP"):
			return PK2ItemType.JadeTabletStamina
		elif long_id.startswith("ITEM_ETC_ARCHEMY_MAGICTABLET_MP"):
			return PK2ItemType.JadeTabletMagic
		elif long_id.startswith("ITEM_ETC_ARCHEMY_MAGICTABLET_FROSTBITE"):
			return PK2ItemType.JadeTabletFogs
		elif long_id.startswith("ITEM_ETC_ARCHEMY_MAGICTABLET_ESHOCK"):
			return PK2ItemType.JadeTabletAir
		elif long_id.startswith("ITEM_ETC_ARCHEMY_MAGICTABLET_BURN"):
			return PK2ItemType.JadeTabletFire
		elif long_id.startswith("ITEM_ETC_ARCHEMY_MAGICTABLET_POISON"):
			return PK2ItemType.JadeTabletImmunity
		elif long_id.startswith("ITEM_ETC_ARCHEMY_MAGICTABLET_ZOMBIE"):
			return PK2ItemType.JadeTabletRevival
		elif long_id.startswith("item_etc_archemy_magictablet_sol_id"):
			return PK2ItemType.JadeTabletSteady
		elif long_id.startswith("ITEM_ETC_ARCHEMY_MAGICTABLET_LUCK"):
			return PK2ItemType.JadeTabletLuck
		elif long_id.startswith("ITEM_ETC_ARCHEMY_ATTRTABLET_PA_"):
			return PK2ItemType.RubyTabletCourage
		elif long_id.startswith("ITEM_ETC_ARCHEMY_ATTRTABLET_PASTR"):
			return PK2ItemType.RubyTabletWarriors
		elif long_id.startswith("ITEM_ETC_ARCHEMY_ATTRTABLET_MA_"):
			return PK2ItemType.RubyTabletPhilosophy
		elif long_id.startswith("ITEM_ETC_ARCHEMY_ATTRTABLET_MAINT"):
			return PK2ItemType.RubyTabletMeditation
		elif long_id.startswith("ITEM_ETC_ARCHEMY_ATTRTABLET_CRITICAL"):
			return PK2ItemType.RubyTabletChallenge
		elif long_id.startswith("ITEM_ETC_ARCHEMY_ATTRTABLET_HR"):
			return PK2ItemType.RubyTabletFocus
		elif long_id.startswith("ITEM_ETC_ARCHEMY_ATTRTABLET_PD_"):
			return PK2ItemType.RubyTabletFlesh
		elif long_id.startswith("ITEM_ETC_ARCHEMY_ATTRTABLET_PDSTR"):
			return PK2ItemType.RubyTabletLife
		elif long_id.startswith("ITEM_ETC_ARCHEMY_ATTRTABLET_MD_"):
			return PK2ItemType.RubyTabletMind
		elif long_id.startswith("ITEM_ETC_ARCHEMY_ATTRTABLET_MDINT"):
			return PK2ItemType.RubyTabletSpirit
		elif long_id.startswith("ITEM_ETC_ARCHEMY_ATTRTABLET_ER"):
			return PK2ItemType.RubyTabletDodging
		elif long_id.startswith("ITEM_ETC_ARCHEMY_ATTRTABLET_BR"):
			return PK2ItemType.RubyTabletAgility
		elif long_id.startswith("ITEM_ETC_ARCHEMY_ATTRTABLET_PAR"):
			return PK2ItemType.RubyTabletTraining
		elif long_id.startswith("ITEM_ETC_ARCHEMY_ATTRTABLET_MAR"):
			return PK2ItemType.RubyTabletPrayer
		else:
			return PK2ItemType.AlchemyTablet

	@staticmethod
	def disambiguate_type(value, long_id: str):
		"""Specify type when the numeric value is not enough"""

		if value == PK2ItemType.HPPot.value:
			return PK2ItemType.HPGrain if "SPOTION" in long_id else PK2ItemType.HPPot
		elif value == PK2ItemType.MPPot.value:
			return PK2ItemType.MPGrain if "SPOTION" in long_id else PK2ItemType.MPPot
		elif value == PK2ItemType.VigorPot.value:
			return PK2ItemType.VigorGrain if "SPOTION" in long_id else PK2ItemType.VigorPot
		elif value == PK2ItemType.EXP_SP_or_Zerk_Scroll.value:
			return PK2Item.get_exp_sp_zerk_scroll_type(long_id)
		elif value == PK2ItemType.PetVehicle.value:
			return PK2Item.get_pet_vehicle_type(long_id)
		elif value == PK2ItemType.BuffScroll.value:
			return PK2Item.get_buff_scroll_type(long_id)
		elif value == PK2ItemType.Elixir.value:
			return PK2Item.get_elixir_type(long_id)
		elif value == PK2ItemType.MagicStone.value:
			return PK2Item.get_magic_stone_type(long_id)
		elif value == PK2ItemType.AttributeStone.value:
			return PK2Item.get_attribute_stone_type(long_id)
		elif value == PK2ItemType.AlchemyTablet.value:
			return PK2Item.get_tablet_type(long_id)
		elif value == PK2ItemType.MagicStoneMall.value:
			return PK2Item.get_magic_stone_type(long_id)
		elif value == PK2ItemType.ItemMallScroll:
			return PK2Item.get_item_mall_scroll_type(long_id)
		elif value == PK2ItemType.ExpansionScroll:
			return PK2Item.get_expansion_scroll_type(long_id)
		else:
			return PK2ItemType(value)

	def should_pick(self) -> bool:
		tup = self.type_filters.get(self.type, None)
		if tup:
			matching_pick_setting: bool = False if not tup[0] else ItemFilter.from_str(str(tup[0])).pick
			matching_degree: bool = True if not tup[1] else DegreeFilter.get_flag(tup[1], self.degree)
			return matching_pick_setting and matching_degree
		else:
			return False

	def should_store(self) -> bool:
		tup = self.type_filters.get(self.type, None)
		if tup:
			matching_store_setting: bool = False if not tup[0] else ItemFilter.from_str(str(tup[0])).store
			matching_degree: bool = True if not tup[1] else DegreeFilter.get_flag(tup[1], self.degree)
			return matching_store_setting and matching_degree
		else:
			return False

	def should_sell(self) -> bool:
		tup = self.type_filters.get(self.type, None)
		if tup:
			matching_store_setting: bool = False if not tup[0] else ItemFilter.from_str(str(tup[0])).sell
			matching_degree: bool = True if not tup[1] else DegreeFilter.get_flag(tup[1], self.degree)
			return matching_store_setting and matching_degree
		else:
			return False

	def should_drop(self) -> bool:
		tup = self.type_filters.get(self.type, None)
		if tup:
			matching_store_setting: bool = False if not tup[0] else ItemFilter.from_str(str(tup[0])).drop
			matching_degree: bool = True if not tup[1] else DegreeFilter.get_flag(tup[1], self.degree)
			return matching_store_setting and matching_degree
		else:
			return False

	@staticmethod
	def get_item_type(type_flags: str, long_id: str) -> PK2ItemType:
		value = int(type_flags, 16)
		if value in PK2ItemType._value2member_map_:
			return PK2Item.disambiguate_type(value, long_id)
		else:
			return PK2ItemType.Other

	def get_type_filter(self) -> tuple:
		if self.type.value in self.type_filters:
			tup = self.type_filters[self.type]
			return tup
		else:
			return None, None
