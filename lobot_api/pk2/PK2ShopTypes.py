# shoptabdata.txt --> 	shoptabdata_id, shoptabdatalong_id, shoptabdata_id, shoptabdataName
from typing import List

from lobot_api.pk2.IPK2Type import IPK2Type
from lobot_api.structs.PK2ShopItemRef import PK2ShopItemRef


class PK2ShopTabData(IPK2Type):
	def __init__(self, shop_tab_data_id: str, shop_tab_data_long_id: str, shop_tab_group_id: str, shop_tab_data_name: str,
	             shop_tab_items: List[PK2ShopItemRef]):
		super(PK2ShopTabData, self).__init__(shop_tab_data_id, shop_tab_data_long_id, shop_tab_data_name)
		self.shop_tab_data_id = int(shop_tab_data_id)
		self.shop_tab_data_long_id = shop_tab_data_long_id
		self.shop_tab_group_id = int(shop_tab_group_id)
		self.shop_tab_data_name = shop_tab_data_name
		self.shop_tab_items = shop_tab_items

	def to_type(self) -> IPK2Type: return self

	def __iter__(self):
		return iter(str(self.shop_tab_data_id))

	def __repr__(self):
		return self.shop_tab_data_name

	def __str__(self):
		return f"{self.shop_tab_data_id}|{self.shop_tab_data_long_id}|{self.shop_tab_group_id}|{self.shop_tab_data_name}|{'|'.join([str(item) for item in self.shop_tab_items])}"


# shopdata.txt -->	shop_id, shoplong_id, xxx 0	???	tab_id
class PK2ShopData:
	def __init__(self, shop_id: str, shop_npc_id: str, shop_tabs: List[PK2ShopTabData]):
		self.shop_id = int(shop_id)
		self.shop_npc_id = int(shop_npc_id)
		self.shop_tabs = shop_tabs

	def __eq__(self, other):
		return self.shop_id == other

	def __iter__(self):
		return iter(str(self.shop_id))

	def __repr__(self):
		return self.shop_id

	def __str__(self):
		return f"{self.shop_id}|{self.shop_npc_id}|{[str(x.shop_tab_data_long_id) for x in self.shop_tabs]}"


# shopitemdata -->	shoptab_id, item_id, shopgroupdata_id
class PK2ShopItemData(IPK2Type):
	def __init__(self, shop_tab_id: str, item_id: str):  # /*, shopgrouptab_id: str*/)
		super(PK2ShopItemData, self).__init__()
		self.shoptab_id = shop_tab_id
		self.item_id = item_id

	# str shopgrouptab_id

	def to_type(self) -> IPK2Type: return self

	def __iter__(self):
		return iter(str(self.item_id))

	def __repr__(self):
		return f"{self.shoptab_id}|{self.item_id}"

	def __str__(self):
		return f"{self.shoptab_id}|{self.item_id}"
