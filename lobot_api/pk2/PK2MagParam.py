import logging
import re
import sys
from enum import Enum, auto
from typing import List

from lobot_api.logger.Logger import PK2
from lobot_api.pk2.IPK2Type import IPK2Type
from lobot_api.pk2.PK2Item import PK2Item


class PK2MagParamType(Enum):
	"""
	Blue types.
	Normal ones just increment each time a magic stone is used, like lucky blue
	Tiered ones add a value or change an existing one, like +str blue
	"""
	Normal = 0
	Tiered = 1

	normal_params: List[str] = [
		"MATTR_SOLID",  # steady
		"MATTR_LUCK",
		"MATTR_ASTRAL",
		"MATTR_ATHANASIA"  # immortal
	]

	@staticmethod
	def is_param_normal(code_name) -> bool:
		return any(param.startswith(code_name) for param in PK2MagParamType.normal_params)

	@staticmethod
	def is_param_tiered(code_name) -> bool:
		return not PK2MagParamType.is_param_normal(code_name)


class PK2MagParamItemGroupType(Enum):
	weapon = 0
	shield = 1
	armor = 2
	accessory = 3
	necklace = 4
	earring = 5
	ring = 6
	pants = 7
	helm = 8
	mail = 9
	avatar_set = 10
	avatar_helm = 11
	avatar_mail = 12
	avatar_mail_addon = 13
	avatar_special_armlet = 14
	avatar_extra = 15
	hunter = 16
	thief = 17
	trader = 18
	other = auto()


class PK2MagParam(IPK2Type):

	@staticmethod
	def capitalize(match: re.Match) -> str:
		return f"{match.group(1)}{match.group(2).lower()}"

	@staticmethod
	def get_name(code_name):
		result = re.sub(r'MATTR_(\w)', r'\1', code_name)
		result = re.sub(r'_', ' ', result)
		result = re.sub(r'(\w)(\w+)', PK2MagParam.capitalize, result)
		return result

	@staticmethod
	def _params_to_tiers(params):
		"""
		Take param numbers and return tiers.
		Each param is a 32 bit number that is split into low (upper 16 bits) and high tier value(lower 16 bits).
		E.g. accessory tiers are 3 param numbers resulting in [1,3,5,20]%

		:param params:
		:return:
		"""
		tiers = []

		for p in params:
			value = int(p)
			low = value >> 16
			if low > 0:
				tiers.append(low)

			high = value & 0xFFFF
			if high > 0:
				tiers.append(high)

		return tiers

	def __init__(self, id_: str, long_id: str, degree: str, params: List[str], item_groups: List[str], ):
		name = self.get_name(long_id)
		super(PK2MagParam, self).__init__(id_, long_id, name)
		self.degree = degree
		self.tiers = self._params_to_tiers(params)
		self.item_groups = self.names_to_item_groups(item_groups)

	def to_type(self) -> IPK2Type:
		return self

	def __iter__(self):
		return iter(str(self.ID))

	def __repr__(self):
		return f"{self.ID}|{self.name}"

	def __str__(self):
		return f"{self.ID}|{self.long_id}|{self.degree}|{','.join([str(t) for t in self.tiers])}|{','.join(self.item_groups_to_string(self.item_groups))}"

	@staticmethod
	def names_to_item_groups(names: List[str]):
		result = []
		for name in names:
			name = name.casefold()
			if name in [member.casefold() for member in PK2MagParamItemGroupType._member_names_]:
				result.append(PK2MagParamItemGroupType[name])
			else:
				result.append(PK2MagParamItemGroupType.Other)
				logging.log(PK2, f"MagParam: unknown group type {name})")
				sys.stdout.write(f"MagParam: unknown group type {name})\n")

		return result

	@staticmethod
	def item_groups_to_string(groups: List[PK2MagParamItemGroupType]):
		return [str(group.value) for group in groups]

	@staticmethod
	def get_from_file_entry(id_: str, long_id: str, degree: str, params: List[str], item_groups: List[str]):
		parsed_item_groups = []
		for group in item_groups:
			value = int(group)
			if value in PK2MagParamItemGroupType._value2member_map_.keys():
				parsed_item_groups.append(PK2MagParamItemGroupType._value2member_map_.get(value).name)
			else:
				parsed_item_groups.append("other")
				logging.log(PK2, f"MagParam: unknown group type {group}({value})")
				sys.stdout.write(f"MagParam: unknown group type {group}({value})\n")

		return PK2MagParam(id_, long_id, degree, params, parsed_item_groups)

	@staticmethod
	def does_param_match_equipment(pk2_mag_param, item: PK2Item) -> bool:
		"""Predicate of whether a weapon suits a given skill.
		Multiple weapons can match a skill. For example, glaive and spear can cast heuksal skills.
		"""
		# switch (pk2_skill.required_weapon)
		return (
				"weapon" in pk2_mag_param.item_groups and item.is_weapon()
				or "shield" in pk2_mag_param.item_groups and item.is_shield()
				or "armor" in pk2_mag_param.item_groups and item.is_shield()
				or "accessory" in pk2_mag_param.item_groups and item.is_accessory()
		)
