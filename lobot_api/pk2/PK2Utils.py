import logging
import os
import sys
from builtins import bool
from pathlib import Path

from lobot_api.Event import Event
from lobot_api.globals.DirectoryGlobal import DirectoryGlobal
from lobot_api.globals.PK2DataGlobal import PK2DataGlobal
from lobot_api.logger.Logger import PK2
from lobot_api.pk2.PK2Main import PK2Main
from lobot_api.pk2.extractors.PK2ExtractorItems import PK2ExtractorItems
from lobot_api.pk2.extractors.PK2ExtractorLevelData import PK2ExtractorLevelData
from lobot_api.pk2.extractors.PK2ExtractorMagParams import PK2ExtractorMagParams
from lobot_api.pk2.extractors.PK2ExtractorNPCs import PK2ExtractorNPCs
from lobot_api.pk2.extractors.PK2ExtractorNames import PK2ExtractorNames
from lobot_api.pk2.extractors.PK2ExtractorShops import PK2ExtractorShops
from lobot_api.pk2.extractors.PK2ExtractorSkills import PK2ExtractorSkills
from lobot_api.pk2.extractors.PK2ExtractorTeleporters import PK2ExtractorTeleporters
from lobot_api.pk2.extractors.PK2PartyTitleExtractor import PK2PartyTitleExtractor


class PK2Utils:
	"""class used( for pk2 operations which simplifies underlying procedures.):"""
	is_initialized: bool = False

	on_extraction_started = Event()
	on_extraction_changed = Event()
	on_extraction_ended = Event()

	@staticmethod
	def lookup_shop(ref_id: int):
		return PK2DataGlobal.shops.get(ref_id, None)

	@staticmethod
	def lookup_npc(ref_id: int):
		return PK2DataGlobal.shops.get(ref_id, None)

	@staticmethod
	def lookup_skill(ref_id: int):
		return PK2DataGlobal.shops.get(ref_id, None)

	@classmethod
	def sro_client_str(cls):
		return 'sro_client.exe'

	@classmethod
	def mediapk2_str(cls):
		return 'Media.pk2'

	@staticmethod
	def exist_media_pk2_and_client(directory_path: str) -> bool:
		"""Check for existence of Media.pk2 and sro_client.exe.
		This should be used to ensure that the necessary information can be extracted and that the game can be started.

		:returns: Whether both files exist"""
		if not os.path.exists(directory_path):
			return False

		files = os.listdir(directory_path)
		return PK2Utils.sro_client_str() in files and PK2Utils.mediapk2_str() in files

	@staticmethod
	def delete_old_pk2_info():
		"""Prepare to switch to another silkroad version or another server"""
		to_delete = [
			DirectoryGlobal.name_data(), DirectoryGlobal.level_data(), DirectoryGlobal.skill_data(),
			DirectoryGlobal.item_data(), DirectoryGlobal.npc_data(), DirectoryGlobal.teleporter_data(),
			DirectoryGlobal.shop_data()
		]
		for filename in to_delete:
			if os.path.exists(filename):
				os.remove(filename)

	@staticmethod
	def extract_all_information(delete_old_info: bool = False):
		"""The function for extracting and mapping information from pk2 files after choosing the sro directory.
		It makes use of the pk2 extractors and tries to load any previously extracted information
		from the bot data folder before attempting the more time-consuming extraction.
		"""
		try:
			PK2Utils.on_extraction_started.notify(0, "Started extraction")

			Path(DirectoryGlobal.data_folder()).mkdir(exist_ok=True)
			Path(DirectoryGlobal.log_folder()).mkdir(exist_ok=True)
			Path(DirectoryGlobal.nav_mesh_folder()).mkdir(exist_ok=True)
			if PK2Utils.is_initialized and delete_old_info:
				PK2Utils.delete_old_pk2_info()
				PK2Utils.is_initialized = False

			if not PK2Utils.is_initialized:
				# PK2Utils.on_extraction_changed.notify(5, "Extracting Names")
				PK2ExtractorNames().extract_all()  # Has to be parsed first for all others to use
				PK2Utils.on_extraction_changed.notify(10, "Extracting Levels")
				PK2ExtractorLevelData().extract_all()  # Quick and simple
				PK2Utils.on_extraction_changed.notify(15, "Extracting Blues")
				PK2ExtractorMagParams().extract_all()  # Quick and simple
				PK2Utils.on_extraction_changed.notify(25, "Extracting Blues")
				PK2Utils.on_extraction_changed.notify(15, "Extracting Party titles")
				PK2PartyTitleExtractor().extract_all()  # Quick and simple
				# Depend on Names
				PK2Utils.on_extraction_changed.notify(30, "Extracting Skills (might take long!)")
				PK2ExtractorSkills().extract_all()  # Most time consuming due to encryption
				PK2Utils.on_extraction_changed.notify(60, "Extracting Shops")
				PK2ExtractorItems().extract_all()  # Parse before Shops
				PK2Utils.on_extraction_changed.notify(70, "Extracting General NPCs")
				PK2ExtractorNPCs().extract_all()  # Parse before Shops (and Teleporters?)
				PK2Utils.on_extraction_changed.notify(85, "Extracting Teleporter NPCs")
				PK2ExtractorTeleporters().extract_all()  # So far Independent from NPCs

				PK2Utils.on_extraction_changed.notify(95, "Extracting Shop NPCs")
				PK2ExtractorShops().extract_all()  # Depends on Items and (possibly NPCS in some cases?)
				# PK2ExtractorNavMesh().extract_all()  # TODO# get pathfinding data

				PK2Utils.on_extraction_ended.notify("")
				PK2Utils.is_initialized = True
		except IndexError:
			logging.log(PK2, "Error extracting all pk2 data", exc_info=sys.exc_info())


	@staticmethod
	def setup(delete_old_info: bool = False):
		if delete_old_info:
			PK2Main.reset()
		pk2 = PK2Main().instance()
		pk2.get_server_ip_address()
		pk2.get_server_port()
		pk2.get_silkroad_version()

		PK2Utils.extract_all_information(delete_old_info)
