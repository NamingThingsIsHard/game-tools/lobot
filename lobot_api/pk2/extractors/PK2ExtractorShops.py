import logging
import os
from io import StringIO
from typing import Dict, List

from lobot_api.globals.DirectoryGlobal import DirectoryGlobal
from lobot_api.globals.PK2DataGlobal import PK2DataGlobal
from lobot_api.logger.Logger import PK2
from lobot_api.pk2.IPK2Type import IPK2Type
from lobot_api.pk2.PK2Main import PK2Main
from lobot_api.pk2.PK2ShopTypes import PK2ShopData, PK2ShopTabData
from lobot_api.pk2.extractors.PK2ExtractorTemplate import PK2ExtractorTemplate
from lobot_api.structs.PK2ShopItemRef import PK2ShopItemRef
from lobot_api.structs.Shop import Shop
from lobot_api.structs.ShopItemEntry import ShopItemEntry


class PK2ExtractorShops(PK2ExtractorTemplate):
	def extract_and_map_data(self):
		shop_data: Dict[str, PK2ShopData] = self.__extract_shop_data_from_pk2_entry__("shopdata.txt")
		shops: Dict[int, Shop] = {}

		with open(DirectoryGlobal.shop_data(), 'w+') as file:
			for data in shop_data.values():
				tab_number = 0
				for tab in data.shop_tabs:
					slot_number = 0
					for item in tab.shop_tab_items:  # empty tabs not added
						try:
							file.write(f"{data.shop_npc_id}|{item.item_id}|{tab_number}|{item.slot}|{item.price}\n")
							result_shop = shops.get(data.shop_npc_id, None)
							if not result_shop:
								shops[data.shop_npc_id] = Shop(data.shop_npc_id)
							shops[data.shop_npc_id].item_entries[item.item_id] = ShopItemEntry(item.item_id,
								tab_number,
								slot_number,
								item.price)
						except Exception as e:
							logging.log(PK2, "extractShopData: extract and map  " + str(item))
						slot_number += 1
					tab_number += 1
			# lambda loop returns bad string for entries
			# File.WriteAllLines(Global.ShopData, shops.Select(x => x.Key + "," + str(x.Value))
			PK2DataGlobal.shops = shops

	def __extract_shop_data_from_pk2_entry__(self, entry: str) -> Dict[str, PK2ShopData]:
		shop_file_entry = PK2Main.instance().PK2Class.get_file(entry).decode('utf-16')
		shop_data: Dict[str, PK2ShopData] = {}
		shop_tab_data: Dict[str, PK2ShopTabData] = self.__extract_shop_tabs_from_pk2_entry__("shoptabdata.txt")

		with StringIO(shop_file_entry, newline=None) as sr:
			for line in sr.readlines():
				shop_data_attributes: List[str] = line.strip('\n').split(self._separator_)
				current_tabs: List[PK2ShopTabData] = []
				if len(shop_data_attributes) < 1 or shop_data_attributes[0].startswith('//'):
					continue
				try:
					# add tabs from possible slots
					for i in range(6, 16):
						result = shop_tab_data.get(shop_data_attributes[i], None)
						if result:  # guard for single-civilization-servers (ch-only -> no shop tabs for euro items:
							if shop_data_attributes[i] != "0":
								current_tabs.append(result)
							else:
								break
					shop_data[shop_data_attributes[1]] = PK2ShopData(shop_data_attributes[1], shop_data_attributes[5],
					                                              current_tabs)
				except Exception as e:
					logging.log(PK2, "extractShopData: " + ",".join(shop_data_attributes))
		return shop_data

	def __extract_shop_tabs_from_pk2_entry__(self, entry: str) -> Dict[str, PK2ShopTabData]:
		shop_file_entry = PK2Main.instance().PK2Class.get_file(entry).decode('utf-16')
		tabs: Dict[str, PK2ShopTabData] = {}
		shop_item_data: Dict[str, List[PK2ShopItemRef]] = self.__extract_shop_items_from_pk2_entry__()

		with StringIO(shop_file_entry, newline=None) as sr:
			for line in sr.readlines():
				shop_tab_attributes: List[str] = line.strip('\n').split(self._separator_)
				# shoptabdata_id,		shoptabdatalong_id,	shoptabdata_id, 	shoptabdataName
				try:
					values = shop_item_data.get(shop_tab_attributes[2], None)
					if values:
						tabs[shop_tab_attributes[1]] = PK2ShopTabData(shop_tab_attributes[1], shop_tab_attributes[2],
						                                            shop_tab_attributes[3], shop_tab_attributes[4], values)
					else:
						tabs[shop_tab_attributes[1]] = PK2ShopTabData(shop_tab_attributes[1], shop_tab_attributes[2],
						                                            shop_tab_attributes[3], shop_tab_attributes[4], [])
				except Exception as e:
					logging.log(PK2, "extractShopTabs: " + ",".join(shop_tab_attributes))
		return tabs

	def __extract_shop_items_from_pk2_entry__(self):
		"""Map TabName to list of items in tab"""
		shop_file_entry = PK2Main.instance().PK2Class.get_file("refpricepolicyofitem.txt").decode('utf-16')
		item_names: Dict[str, str] = {}
		item_data: List[PK2ShopItemRef] = []
		with StringIO(shop_file_entry, newline=None) as sr:
			for line in sr.readlines():
				shop_item_attributes: List[str] = line.strip('\n').split(self._separator_)
				if len(shop_item_attributes) < 6:
					continue
				# package_item_long_id, price
				try:
					# Add key and itemlist or add item to existing key
					item_names[shop_item_attributes[2]] = shop_item_attributes[5]
				except Exception as e:
					logging.log(PK2, "extractShopItem: " + ",".join(shop_item_attributes))

		# Add tabnames and item_ids for the next steps
		item_data = self.__extract_tab_names_and_slot_indexes__(item_names)
		self.__find_item_ids__(item_data)
		return self.__map_tabs_to_items__(item_data)  # Key(ID), Value(Item))

	def __extract_tab_names_and_slot_indexes__(self, items: Dict[str, str]) -> List[PK2ShopItemRef]:
		shop_file_entry = PK2Main.instance().PK2Class.get_file("refshopgoods.txt").decode('utf-16')
		result: List[PK2ShopItemRef] = []
		with StringIO(shop_file_entry, newline=None) as sr:
			for line in sr.readlines():
				ref_tab_attributes: List[str] = line.strip('\n').split(self._separator_)
				try:
					result.append(PK2ShopItemRef(ref_tab_attributes[3], ref_tab_attributes[2], items[ref_tab_attributes[3]],
					                             int(ref_tab_attributes[4])))
				except KeyError as ke:
					logging.log(PK2, f"extractShopItem: ref_tab_attributes - item {ref_tab_attributes[3]} not found")
				except Exception as e:
					logging.log(PK2, "extractShopItem: ref_tab_attributes " + ",".join(ref_tab_attributes))
		return result

	def __find_item_ids__(self, items: List[PK2ShopItemRef]):
		shop_file_entry = PK2Main.instance().PK2Class.get_file("refscrapofpackageitem.txt").decode('utf-16')
		pack_name_to_ref_name: Dict[str, str] = {}
		with StringIO(shop_file_entry, newline=None) as sr:
			for line in sr.readlines():
				item_entry: List[str] = line.strip('\n').split(self._separator_)
				if len(item_entry) > 3:
					pack_name_to_ref_name[item_entry[2]] = item_entry[3]

		for item in items:
			actual_item_ref_name: str = pack_name_to_ref_name[item.long_id]
			result = next((i for i in PK2DataGlobal.items.values() if i.long_id == actual_item_ref_name), None)
			item.item_id = result.ID if result else ""

	@staticmethod
	def __map_tabs_to_items__(items: List[PK2ShopItemRef]) -> Dict[str, List[PK2ShopItemRef]]:
		result: Dict[str, List[PK2ShopItemRef]] = {}
		for item in items:
			try:
				if item.tab_name.startswith(r"//"):
					continue
				# Add key and itemlist or add item to existing key
				if item.tab_name not in result:
					result[item.tab_name] = [item]
				else:
					# Insert in slot order
					if item.slot < len(result[item.tab_name]):  #
						result[item.tab_name].insert(item.slot, item)
					else:
						result[item.tab_name].append(item)
			except Exception as e:
				logging.log(PK2, "extractShopItem: " + str(item))
		return result

	def extract_data_from_pk2_entry(self, entry: str) -> List[IPK2Type]:
		raise NotImplementedError()

	def extract_file_names(self) -> List[str]:
		raise NotImplementedError()

	def load_from_txt_file(self):
		try:
			# force normal extraction in case last extraction was faulty
			if not os.path.isfile(DirectoryGlobal.shop_data()) or os.stat(DirectoryGlobal.shop_data()).st_size < 128:
				raise BufferError()

			shops: Dict[int, Shop] = {}
			with open(DirectoryGlobal.shop_data()) as sr:
				for line in sr.readlines():
					shop_str: List[str] = line.strip('\n').split('|')
					shop: List[int] = [int(x) for x in shop_str]
					if not self.is_valid(shop_str, len(shop_str), 4):
						continue
					result_shop = shops.get(shop[0], None)
					if not result_shop:
						shops[shop[0]] = Shop(shop[0])

					# shop id - tab - slot - item_id
					shops[shop[0]].item_entries[shop[1]] = ShopItemEntry(shop[1], shop[2], shop[3], shop[4])
			PK2DataGlobal.shops = shops
		except Exception as e:
			logging.log(PK2, "\"data/shops.txt\" not found. Attempting to extract PK2-Info.")
			self.check_pk2_then_extract()
