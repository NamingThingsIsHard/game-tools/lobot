import logging
import os
from io import StringIO
from typing import List, Dict

from lobot_api.globals.DirectoryGlobal import DirectoryGlobal
from lobot_api.globals.PK2DataGlobal import PK2DataGlobal
from lobot_api.logger.Logger import PK2
from lobot_api.pk2.IPK2Type import IPK2Type
from lobot_api.pk2.PK2Main import PK2Main
from lobot_api.pk2.PK2TeleporterTypes import PK2Teleporter, PK2TeleporterData
from lobot_api.pk2.extractors.PK2ExtractorTemplate import PK2ExtractorTemplate


class PK2ExtractorTeleporters(PK2ExtractorTemplate):
	def extract_and_map_data(self):
		teleporter_file_names: List[str] = self.extract_file_names()
		teleporters: List[IPK2Type] = []
		mapped_teleporters: Dict[int, PK2Teleporter] = {}

		for file_name in teleporter_file_names:
			teleporters.extend(self.extract_data_from_pk2_entry(file_name))

		for teleporter in teleporters:
			mapped_teleporters[teleporter.ID] = teleporter
		with open(DirectoryGlobal.teleporter_data(), 'w+', encoding='utf-16') as f:
			f.writelines([f'{skill}\n' for skill in mapped_teleporters.values()])
		PK2DataGlobal.teleporters = mapped_teleporters

	def __extractteleporter_links__(self, entry: str) -> Dict[int, List[int]]:
		file_entry = PK2Main.instance().PK2Class.get_file(entry).decode('utf-16')
		teleporter_links: Dict[int, List[int]] = {}
		with StringIO(file_entry, newline=None) as sr:
			for line in sr.readlines():
				link_attributes: List[str] = line.strip('\n').split(self._separator_)
				if len(link_attributes) < 3:
					continue
				# Make string with
				# ID   long_id  name_id  level   hp
				try:
					values = teleporter_links.get(int(link_attributes[1]), None)
					if values:
						values.append(int(link_attributes[2]))
					else:
						teleporter_links[int(link_attributes[1])] = [int(link_attributes[2])]
				except Exception as e:
					logging.log(PK2, "extractTeleporterLinks: " + ",".join(link_attributes))
		return teleporter_links

	def __extract_teleporter_data__(self, entry: str) -> List[PK2TeleporterData]:
		file_entry: List[int] = PK2Main.instance().PK2Class.get_file(entry).decode('utf-16')
		teleporter_data: List[PK2TeleporterData] = []
		teleporter_links: Dict[int, List[int]] = self.__extractteleporter_links__("teleportlink.txt")

		with StringIO(file_entry, newline=None) as sr:
			for line in sr.readlines():
				teleporter_data_object: List[str] = line.strip('\n').split(self._separator_)
				if len(teleporter_data_object) < 5:
					continue
				# Make string with
				# ID   long_id  name_id  level   hp
				try:
					value = PK2DataGlobal.names.get(teleporter_data_object[4], None)
					if value:
						teleporter_data_object[2] = value
					values = teleporter_links.get(int(teleporter_data_object[1]))
					if values:
						teleporter_data.append(
							PK2TeleporterData(teleporter_data_object[1], teleporter_data_object[3], teleporter_data_object[2],
							                  values))
					else:
						teleporter_data.append(
							PK2TeleporterData(teleporter_data_object[1], teleporter_data_object[3], teleporter_data_object[2],
							                  []))
				except Exception as e:
					logging.log(PK2, "extractTeleporterData: " + ",".join(teleporter_data_object))
		return teleporter_data

	def extract_data_from_pk2_entry(self, entry: str) -> List[IPK2Type]:
		file_names = PK2Main.instance().PK2Class.get_file(entry).decode('utf-16')
		teleporter_data: List[PK2TeleporterData] = self.__extract_teleporter_data__("teleportdata.txt")
		teleporters: List[IPK2Type] = []
		with StringIO(file_names, newline=None) as sr:
			for line in sr.readlines():
				teleporter_attributes: List[str] = line.strip('\n').split(self._separator_)
				if len(teleporter_attributes) < 6 or teleporter_attributes[0].startswith('//'):
					continue
				# Make string with
				# ID   long_id  name_id  level   hp
				try:
					value = PK2DataGlobal.names.get(teleporter_attributes[5])
					if value:
						teleporter_attributes[5] = value
					td: PK2TeleporterData = next(
						(item for item in teleporter_data if item.long_id == teleporter_attributes[1]), None)
					if td:
						teleporters.append(
							PK2Teleporter(teleporter_attributes[1], teleporter_attributes[2], teleporter_attributes[5],
							              td))
					else:
						teleporters.append(
							PK2Teleporter(teleporter_attributes[1], teleporter_attributes[2], teleporter_attributes[5],
							              PK2TeleporterData("", "", "", [])))
				except Exception as e:
					logging.log(PK2, "extractTeleporters: " + ",".join(teleporter_attributes))
		return teleporters

	def extract_file_names(self):
		return ["teleportbuilding.txt"]

	def __loadteleporter_link__(self, teleporterLinkInfo: List[str]) -> List[int]:
		teleporter_link: List[int] = []
		if len(teleporterLinkInfo) < 1:
			return []
		else:
			for s in teleporterLinkInfo:
				teleporter_link.append(int(s))
			return teleporter_link

	def __loadteleporter_data__(self, teleporter_data_info: List[str]) -> PK2TeleporterData:
		# Add possible teleporterLinks
		return PK2TeleporterData(teleporter_data_info[0],
		                         teleporter_data_info[1],
		                         teleporter_data_info[2],
		                         self.__loadteleporter_link__(teleporter_data_info[3].split('|'))) \
			if (len(teleporter_data_info) == 4) \
			else PK2TeleporterData("", "", "", [])

	def __load_teleporter__(self, teleporter_info: List[str]) -> PK2Teleporter:
		if len(teleporter_info) == 4:
			return PK2Teleporter(teleporter_info[0],
			                     teleporter_info[1],
			                     teleporter_info[2],
			                     self.__loadteleporter_data__(teleporter_info[3].split('|', 3)))
		elif len(teleporter_info) == 3:
			return PK2Teleporter(teleporter_info[0], teleporter_info[1], teleporter_info[2],
			                     PK2TeleporterData("", "", "", []))
		else:
			return PK2Teleporter()

	def load_from_txt_file(self):
		try:
			# force normal extraction in case last extraction was faulty
			if not os.path.isfile(DirectoryGlobal.teleporter_data()) or os.stat(
					DirectoryGlobal.teleporter_data()).st_size < 128:
				raise BufferError()

			teleporters: Dict[int, PK2Teleporter] = {}
			with open(DirectoryGlobal.teleporter_data(), encoding='utf-16') as sr:
				for line in sr.readlines():
					portal: List[str] = line.strip('\n').split('|', 3)
					# Check for possible comment
					if not self.is_valid(portal, len(portal), 3) or int(portal[0]) in teleporters:
						continue
					# Load teleporter
					teleporter: PK2Teleporter = self.__load_teleporter__(portal)
					# TODO add list of teleport links in last parameter List
					# if teleporter == # default(PK2Teleporter):
					if teleporter.ID == 0:
						continue
					teleporters[teleporter.ID] = teleporter
				PK2DataGlobal.teleporters = teleporters
		except Exception as e:
			logging.log(PK2, "\"data/teleporters.txt\" not found. Attempting to extract PK2-Info.")
			self.check_pk2_then_extract()
