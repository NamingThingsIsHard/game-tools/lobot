import logging
from io import StringIO
from typing import List

from lobot_api.globals.settings.PartySettings import PartySettings
from lobot_api.logger.Logger import PK2
from lobot_api.pk2.IPK2Type import IPK2Type
from lobot_api.pk2.PK2Main import PK2Main
from lobot_api.pk2.extractors.PK2ExtractorTemplate import PK2ExtractorTemplate
from lobot_api.structs.PartyObjective import PartyObjective


class PK2PartyTitleExtractor(PK2ExtractorTemplate):
	def __ishunting_text__(self, text: str) -> bool:
		return text == "UIIT_MSG_PARTYMATCH_RECORD_DEFAULT1"

	def __isquest_text__(self, text: str) -> bool:
		return text == "UIIT_MSG_PARTYMATCH_RECORD_DEFAULT3"

	def __isthief_text__(self, text: str) -> bool:
		return text == "UIIT_MSG_PARTYMATCH_RECORD_DEFAULT4"

	def __istraderhunter_text__(self, text: str) -> bool:
		return text == "UIIT_MSG_PARTYMATCH_RECORD_DEFAULT2"

	def __set_text__(self, objective: PartyObjective, text: List[str], titles_set: int):
		"""ref titlesSet"""
		index = 8
		# find entry: with meaningful text
		while text[index] == "0" and index < len(text) - 1:
			index += 1
		PartySettings.default_party_entries[objective] = text[index]
		return titles_set + 1

	def extract_and_map_data(self):
		text_ui_system = PK2Main.instance().PK2Class.get_file("textuisystem.txt").decode('utf-16')
		titles_set = 0

		with StringIO(text_ui_system) as sr:
			for line in sr.readlines():
				ui_text_entry: List[str] = line.strip('\n').split(self._separator_)
				if not self.is_valid(ui_text_entry, len(ui_text_entry), 8):
					continue
				if titles_set == 4:  # set
					break
				try:
					# Add key and item_list or add item to existing key
					if self.__ishunting_text__(ui_text_entry[1]):
						titles_set = self.__set_text__(PartyObjective.Hunting, ui_text_entry, titles_set)
					elif self.__isquest_text__(ui_text_entry[1]):
						titles_set = self.__set_text__(PartyObjective.Quest, ui_text_entry, titles_set)
					elif self.__istraderhunter_text__(ui_text_entry[1]):
						titles_set = self.__set_text__(PartyObjective.TradeHunter, ui_text_entry, titles_set)
					elif self.__isthief_text__(ui_text_entry[1]):
						titles_set = self.__set_text__(PartyObjective.Thief, ui_text_entry, titles_set)
				except Exception as e:
					logging.log(PK2, "extract# default party title: " + ','.join(ui_text_entry))

	def extract_data_from_pk2_entry(self, entry: str) -> List[IPK2Type]:
		raise NotImplementedError()

	def extract_file_names(self) -> List[str]:
		raise NotImplementedError()

	def load_from_txt_file(self):
		self.check_pk2_then_extract()
