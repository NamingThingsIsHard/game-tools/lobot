import logging
import os
from ctypes import c_byte
from io import StringIO
from typing import List, Dict

from lobot_api.globals.DirectoryGlobal import DirectoryGlobal
from lobot_api.globals.PK2DataGlobal import PK2DataGlobal
from lobot_api.logger.Logger import PK2
from lobot_api.pk2.PK2Main import PK2Main
from lobot_api.pk2.extractors.PK2ExtractorTemplate import PK2ExtractorTemplate


class PK2ExtractorNames(PK2ExtractorTemplate):
	def extract_and_map_data(self):
		file_names: List[str] = self.extract_file_names()
		names: Dict[str, str] = {}
		for fileName in file_names:
			names.update(self.__extractnamesfrompk_entry__(fileName))
			with open(DirectoryGlobal.name_data(), 'w+', encoding='utf-16') as f:
				# TODO occasional error with encoding
				f.writelines([f'{key}|{"".join(value)}\n' for (key, value) in names.items()])
			PK2DataGlobal.names = names

	def __extractnamesfrompk_entry__(self, entry: str):
		name_file_entry: List[c_byte] = PK2Main.instance().PK2Class.get_file(entry).decode('utf-16').rstrip('\x00')
		names: Dict[str, str] = {}
		if PK2Main.instance().PK2Class is None or name_file_entry is None:
			return names
		with StringIO(name_file_entry, newline=None) as sr:
			for x in sr.readlines():
				name_attributes: List[str] = x.strip('\n').split(self._separator_)
				if len(name_attributes) < 1 or name_attributes[0].startswith("//") or name_attributes[0] == "0":
					continue
				try:
					if len(name_attributes) > 8 and name_attributes[8] != '0':
						names[name_attributes[1]] = name_attributes[8]
					elif len(name_attributes) > 9 and name_attributes[9] != '0':
						names[name_attributes[1]] = name_attributes[9]
					elif len(name_attributes) > 2:
						names[name_attributes[1]] = name_attributes[2]
				except Exception as e:
					logging.log(PK2, "extractName: " + ",".join(name_attributes))
		return names

	def extract_data_from_pk2_entry(self, entry: str):
		raise NotImplementedError()

	def extract_file_names(self):
		name_files: List[str] = []
		item_data: List[c_byte] = PK2Main.instance().PK2Class.get_file("textdataname.txt").decode('utf-16').rstrip('\x00')
		if PK2Main.instance().PK2Class is None or item_data is None:
			return name_files

		with StringIO(item_data, newline=None) as sr:
			name_files = [x.strip('\n') for x in sr.readlines()]
		return name_files

	def load_from_txt_file(self):
		try:
			# force normal extraction in case last extraction was faulty
			if not os.path.isfile(DirectoryGlobal.name_data()) or os.stat(DirectoryGlobal.name_data()).st_size < 128:
				raise BufferError()

			names: Dict[str, str] = {}
			with open(DirectoryGlobal.name_data(), newline=None, encoding='utf-16') as sr:
				for line in sr:
					name: List[str] = line.strip('\n').split('|')
					if not self.is_valid(name, len(name)) or name[0] in names:
						continue
					names[name[0]] = name[1]
			PK2DataGlobal.names = names
		except Exception as e:
			# exc_type, exc_value, exc_traceback = sys.exc_info()
			logging.log(PK2, "\"data/names.txt\" not found. Attempting to extract PK2-Info.")
			self.check_pk2_then_extract()
