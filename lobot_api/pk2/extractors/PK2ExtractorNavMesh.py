#
# class PK2ExtractorNavMesh(PK2ExtractorTemplate):
#        private PK2Class dataPK2
#        def extract_and_map_data(self):
# # Find out which .nav files have already been extracted to save some time.
#            navMeshFileNames: List[-] = extract_file_names()
#            IEnumerableserializedFiles = Directory.enumerate_files(DirectoryGlobal.NavMeshFolder, "*.nvm").Select(Path.GetFileName)
#            fileNames: List[-] = navMeshFileNames.Except(serializedFiles).ToList[str]
#            List[str]
# for var fileName in fileNames:
#                NavEntries.add_range(extract_data_from_pk2_entry(fileName))
#        def __getsectory_x__(ushort sectorX, ushort sectorY) -> ushort:
#            return (ushort)(sectorY << 8 ^ sectorX)
#        private GetObjects: List[-](BinaryReader br)
#            List[str]
#            float x, y, z
#            ushort entry:Count = br.read_uint16()
# #for i = 0 i < entry:Count i++:
#                uint id_ = br.read_uint32()
#                x = br.read_single() / 10
#                z = br.read_single() / 10
#                y = 192 - br.read_single() / 10
#                Position position = Position(x, y, z)
#                ushort collisionFlag = br.read_uint16()
#                float yaw = br.read_single()# radiant
#                ushort unique_id = br.read_uint16()
#                ushort scale = br.read_uint16()
#                ushort eventZoneFlag = br.read_uint16()
#                ushort region_id = br.read_uint16()
#                ushort mountCount = br.read_uint16()
# # Skip mountPointData
#                br.BaseStream.position += 6 * mountCount
#                meshes.append(new NavObject(id_, position, collisionFlag, yaw, unique_id, scale, eventZoneFlag, region_id))
#            return meshes
#        private GetCells: List[-](BinaryReader reader)
#            List[str]
#            float x, y
#            Position min
#            Position max
#            uint cellCount = reader.read_uint32()
#            uint cellExtraCount = reader.read_uint32()
# #for cellIndex = 0 cellIndex < cellCount cellIndex++:
#                x = reader.read_single() / 10
#                y = 192 - reader.read_single() / 10
#                min = Position(x, y)
#                x = reader.read_single() / 10
#                y = 192 - reader.read_single() / 10
#                max = Position(x, y)
#                List[str]
#                List[str]
#                List[str]
#                byte entry:Count = reader.read_byte()
# #for i = 0 i < entry:Count i++:
#                    navObjectIndex.append(reader.read_uint16())
#                cells.append(new NavCell(min, max, navObjectIndex, navCellIndex, navCellLinkIndex))
#            return cells
#        private GetBorders: List[-](BinaryReader reader, NavMeshSegment navSegment)
#            List[str]
#            float x, y
#            Point min
#            Point max
#            byte lineFlag
#            byte lineSource
#            byte lineDestination
#            ushort cellSource
#            ushort cellDestination
#            ushort regionSource
#            ushort regionDestination
#            uint regionLinkCount = reader.read_uint32()
# #for linkIndex = 0 linkIndex < regionLinkCount linkIndex++:
#                x = reader.read_single() / 10
#                y = 192 - reader.read_single() / 10
#                min = Point(x, y)
#                x = reader.read_single() / 10
#                y = 192 - reader.read_single() / 10
#                max = Point(x, y)
#                lineFlag = reader.read_byte()
#                lineSource = reader.read_byte()
#                lineDestination = reader.read_byte()
#                cellSource = reader.read_uint16()
#                cellDestination = reader.read_uint16()
#                regionSource = reader.read_uint16()
#                regionDestination = reader.read_uint16()
#                if GetSectorYX(navSegment.SectorX, navSegment.SectorY) is regionSource:
#                    navSegment.NavigationCells[cellSource].NavBorderIndex.append((ushort)linkIndex)
#                else:
#                    navSegment.NavigationCells[cellDestination].NavBorderIndex.append((ushort)linkIndex)
#                navBorders.append(new NavBorder(min, max, lineFlag, lineSource, lineDestination, cellSource, cellDestination, regionSource, regionDestination))
#            return navBorders
#        private GetCellLinks: List[-](BinaryReader reader, NavMeshSegment navSegment)
#            List[str]
#            float x, y
#            Point min
#            Point max
#            byte lineFlag
#            byte lineSource
#            byte lineDestination
#            ushort cellSource
#            ushort cellDestination
#            uint cellLinkCount = reader.read_uint32()
# #for linkIndex = 0 linkIndex < cellLinkCount linkIndex++:
#                x = reader.read_uint32() / 10
#                y = 192 - reader.read_single() / 10
#                min = Point(x, y)
#                x = reader.read_uint32() / 10
#                y = 192 - reader.read_single() / 10
#                max = Point(x, y)
#                lineFlag = reader.read_byte()
#                lineSource = reader.read_byte()
#                lineDestination = reader.read_byte()
#                cellSource = reader.read_uint16()
#                cellDestination = reader.read_uint16()
#                cellLinks.append(new NavCellLink(min, max, lineFlag, lineSource, lineDestination, cellSource, cellDestination))
#                if cellSource not  ushort.MaxValue:
#                    navSegment.NavigationCells[cellSource].NavCellLinkIndex.append((ushort)linkIndex)
#                if cellDestination not  ushort.MaxValue:
#                    navSegment.NavigationCells[cellDestination].NavCellLinkIndex.append((ushort)linkIndex)
#            return cellLinks
#        def extract_data_from_pk2_entry(self, entry: str) -> List[IPK2Type]:
#            fileBytes: List[c_byte] = dataPK2.get_file(entry)
#            NavMeshSegment navSegment = NavMeshSegment(entry)
# with StringIO-) as sr:
#                float[] heightMap = float[9409]
#                try:
#                    char[] header = reader.read_chars(12)# JMXVNVM 1000
#                    navSegment.NavigationObjects = GetObjects(reader)
# # sys.stdout.write(reader.BaseStream.position + "/" + reader.len(BaseStream) + "\tNavEntries")
#                    navSegment.NavigationCells = GetCells(reader)
# # sys.stdout.write(reader.BaseStream.position + "/" + reader.len(BaseStream) + "\tNavCells")
#                    navSegment.NavigationBorders = GetBorders(reader, navSegment)
# # sys.stdout.write(reader.BaseStream.position + "/" + reader.len(BaseStream) + "\tRegionLinks")
#                    navSegment.NavigationCellLinks = GetCellLinks(reader, navSegment)
# # sys.stdout.write(reader.BaseStream.position + "/" + reader.len(BaseStream) + "\tCellLinks")
# # skip texturemaps
#                    reader.BaseStream.position += 0x12000
# # sys.stdout.write(reader.BaseStream.position + "/" + reader.len(BaseStream) + "\tTextureMap")
# # continue until end of stream
#                    long remainingFloats = (reader.len(BaseStream) - reader.BaseStream.position - 36 - 36 * 4) / 4# last3 and last4*4
#                    long endOfHeightMap = Math.Min(9409, remainingFloats)
# #for i = 0 i < endOfHeightMap i++:
#                        heightMap= reader.read_single()
#                    navSegment.HeightMap = heightMap
# # sys.stdout.write(reader.BaseStream.position + "/" + reader.len(BaseStream) + "\tHeightMap")
#                    Conversion.WriteToBinaryFile(DirectoryGlobal.NavMeshFolder + entry:, navSegment, False)
#                catch at_eof()Exception:
#                    logging.log(LogLevel.NETWORK, String.Format("extractNav: {0} {1}", entry:, str(e)))
#                    sys.stdout.write(String.Format("extractNav: {0} {1}", entry:, str(e)))
# # catch serialization:
# # {
# #    logging.log(LogLevel.NETWORK, String.Format("extractNav: {0} {1}", entry:, str(e)))
# #    sys.stdout.write(String.Format("extractNav: {0} {1}", entry:, str(e)))
# # }
#                except Exception as e:
#                    logging.log(LogLevel.PK2, String.Format("extractNav: {0} {1}", entry:, str(e)))
#                    sys.stdout.write(String.Format("extractNav: {0} {1}", entry:, str(e)))
#                return List{ navSegment }
#        def extract_file_names(self) -> List[str]:
#            return dataPK2.GetFileNames().find_all(file => file.ends_with(".nvm"))
#        def load_from_txt_file(self):
#            try:
# # use PK2Class for data.pk2
#                dataPK2 = PK2Class(PK2DataGlobal.pk2_data_file_path)
#                extract_and_map_data()
#                dataPK2 = None
#            except Exception as e:
#                logging.log(LogLevel.PK2, "\"NavEntry:\" not found. Attempting to extract PK2-Info.")
#                sys.stdout.write("\"NavEntry:\" not found. Attempting to extract PK2-Info.\n")
