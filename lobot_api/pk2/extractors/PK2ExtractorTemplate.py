import logging
from abc import ABC, abstractmethod
from typing import List

from lobot_api.logger.Logger import PK2
from lobot_api.pk2.IPK2Type import IPK2Type
from lobot_api.pk2.PK2Main import PK2Main


class PK2ExtractorTemplate(ABC):
	_separator_ = '\t'

	@staticmethod
	def is_valid(s: List[str], args: int = 2, min_args: int = 2) -> bool:
		return len(s) >= min_args and s[0] and not s[0].isspace() and args >= min_args and not s[0].startswith('//')

	@abstractmethod
	def extract_data_from_pk2_entry(self, entry: str) -> List[IPK2Type]:
		pass

	@abstractmethod
	def extract_file_names(self) -> List[str]:
		pass

	@abstractmethod
	def extract_and_map_data(self):
		pass

	@abstractmethod
	def load_from_txt_file(self):
		"""Try to load the pk2 information from previously extracted *.txt file
		in <bot folder>/pk2 before extracting directly from the pk2 file.
		"""
		pass

	def check_pk2_then_extract(self):
		if not PK2Main.instance().PK2Class:
			raise FileNotFoundError("PK2 File not set.")
		else:
			self.extract_and_map_data()

	def extract_all(self):
		"""Template method summarizing the process to get pk2 data."""
		try:
			if not PK2Main.instance().PK2Class:
				raise FileNotFoundError("PK2 File not set.")
			logging.log(PK2, f"{self.__class__.__name__} extraction")
			self.load_from_txt_file()
		except Exception as e:
			logging.log(PK2, f"\"{self.__class__.__name__} Failed to extract PK2-Info. {e}",)
