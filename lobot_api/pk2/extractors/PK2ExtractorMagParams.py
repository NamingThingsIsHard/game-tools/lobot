import logging
import os
import sys
from io import StringIO
from typing import List, Dict

from lobot_api.globals.DirectoryGlobal import DirectoryGlobal
from lobot_api.globals.PK2DataGlobal import PK2DataGlobal
from lobot_api.logger.Logger import PK2
from lobot_api.pk2.IPK2Type import IPK2Type
from lobot_api.pk2.PK2Main import PK2Main
from lobot_api.pk2.PK2MagParam import PK2MagParam
from lobot_api.pk2.extractors.PK2ExtractorTemplate import PK2ExtractorTemplate


class PK2ExtractorMagParams(PK2ExtractorTemplate):
	def extract_and_map_data(self):
		file_name = "magicoption.txt"
		mapped_params: Dict[int, PK2MagParam] = {}
		extracted_params: List[IPK2Type] = self.extract_data_from_pk2_entry(file_name)

		for p in extracted_params:
			mapped_params[p.ID] = p

		with open(DirectoryGlobal.mag_param_data(), 'w+', encoding='utf-16') as f:
			f.writelines([f'{mag_param}\n' for mag_param in mapped_params.values()])
		PK2DataGlobal.mag_params = mapped_params

	def extract_data_from_pk2_entry(self, entry: str) -> List[IPK2Type]:
		file_bytes: List[int] = PK2Main.instance().PK2Class.get_file(entry).decode('utf-16')
		mag_params: List[IPK2Type] = []

		with StringIO(file_bytes) as sr:
			for x in sr.readlines():
				mag_param_attributes: List[str] = x.strip('\n').split(self._separator_)
				if len(mag_param_attributes) < 5 or mag_param_attributes[0].startswith('//'):
					continue
				# Make string with
				# ID, long_id, degree, params, item_groups
				try:
					params = mag_param_attributes[8:11]
					item_groups = list(set(filter(lambda a: a != "xxx", mag_param_attributes[29::2])))

					mag_params.append(
						PK2MagParam(
							mag_param_attributes[1],
							mag_param_attributes[2],
							mag_param_attributes[4],
							params,
							item_groups
						)
					)
				except Exception as e:
					logging.log(PK2, "extractMagParam: " + "".join(mag_param_attributes))
					sys.stdout.write("extractMagParam: " + "".join(mag_param_attributes))

		return mag_params

	def extract_file_names(self) -> List[str]:
		raise NotImplementedError()

	def load_from_txt_file(self):
		try:
			# force normal extraction in case last extraction was faulty
			if not os.path.isfile(DirectoryGlobal.mag_param_data()) or os.stat(DirectoryGlobal.mag_param_data()).st_size < 128:
				raise BufferError()

			mag_params: Dict[int, PK2MagParam] = {}
			with open(DirectoryGlobal.mag_param_data(), newline=None, encoding='utf-16') as sr:
				for line in sr:
					mag_param: List[str] = line.strip('\n').split('|')
					if not self.is_valid(mag_param, len(mag_param), 5) or int(mag_param[0]) in mag_params:
						continue
					mag_params[int(mag_param[0])] = PK2MagParam.get_from_file_entry(
						mag_param[0],
						mag_param[1],
						mag_param[2],
						mag_param[3].split(','),
						mag_param[4].split(',')
					)
			PK2DataGlobal.mag_params = mag_params
		except Exception as e:
			logging.log(PK2, "\"data/magoptions.txt\" not found. Attempting to extract PK2-Info.")
			sys.stdout.write("\"data/magoptions.txt\" not found. Attempting to extract PK2-Info.\n")
			self.check_pk2_then_extract()

