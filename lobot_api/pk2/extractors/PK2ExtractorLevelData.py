import logging
import os
from ctypes import c_byte, c_ulong
from io import StringIO
from typing import List, Dict

from lobot_api.globals.DirectoryGlobal import DirectoryGlobal
from lobot_api.globals.PK2DataGlobal import PK2DataGlobal
from lobot_api.logger.Logger import PK2
from lobot_api.pk2.IPK2Type import IPK2Type
from lobot_api.pk2.PK2Main import PK2Main
from lobot_api.pk2.extractors.PK2ExtractorTemplate import PK2ExtractorTemplate


class PK2ExtractorLevelData(PK2ExtractorTemplate):
	def extract_and_map_data(self):
		level_entry: List[c_byte] = PK2Main.instance().PK2Class.get_file("leveldata.txt").decode('utf-16')
		levels: Dict[str, str] = {}
		with StringIO(level_entry, newline=None) as sr:
			for x in sr.readlines():
				level_info: List[str] = x.strip('\n').split(self._separator_)
				value_type = int(level_info[0]) if level_info[0].isdigit() else None
				if len(level_info) > 1 and value_type:
					levels[level_info[0]] = level_info[1]

		with open(DirectoryGlobal.level_data(), 'w+', encoding='utf-16') as f:
			f.writelines([f'{key}|{value}\n' for (key, value) in levels.items()])
		PK2DataGlobal.level_data = dict([[int(key), int(value)] for (key, value) in levels.items()])

	def extract_data_from_pk2_entry(self, entry: str) -> List[IPK2Type]:
		raise NotImplementedError()

	def extract_file_names(self) -> List[str]:
		raise NotImplementedError()

	def load_from_txt_file(self):
		try:
			# force normal extraction in case last extraction was faulty
			if not os.path.isfile(DirectoryGlobal.level_data()) or os.stat(DirectoryGlobal.level_data()).st_size < 128:
				raise BufferError()

			levels: Dict[int, c_ulong] = {}
			with open(DirectoryGlobal.level_data(), newline=None, encoding='utf-16') as sr:
				for line in sr.readlines():
					level: List[str] = line.strip('\n').split('|')
					if not self.is_valid(level, len(level)) or int(level[0]) in levels:
						continue
					levels[int(level[0])] = int(level[1])
			PK2DataGlobal.level_data = levels
		except Exception as e:
			logging.log(PK2, "\"data/levels.txt\" not found. Attempting to extract PK2-Info.")
			self.check_pk2_then_extract()
