import logging
import os
import re
import sys
from ctypes import c_byte
from io import StringIO
from typing import List, Dict

from lobot_api.globals.DirectoryGlobal import DirectoryGlobal
from lobot_api.globals.PK2DataGlobal import PK2DataGlobal
from lobot_api.logger.Logger import PK2
from lobot_api.pk2.IPK2Type import IPK2Type
from lobot_api.pk2.PK2Main import PK2Main
from lobot_api.pk2.PK2NPC import PK2NPC
from lobot_api.pk2.extractors.PK2ExtractorTemplate import PK2ExtractorTemplate


class PK2ExtractorNPCs(PK2ExtractorTemplate):
	def extract_and_map_data(self):
		npc_file_names: List[str] = self.extract_file_names()
		npcs: List[IPK2Type] = []
		mapped_npcs: Dict[int, PK2NPC] = {}
		for fileName in npc_file_names:
			npcs.extend(self.extract_data_from_pk2_entry(fileName))
		for npc in npcs:
			value = mapped_npcs.get(npc.ID, None)
			if value:
				sys.stdout.write(f"duplicate npc at {npc}\n")
			else:
				mapped_npcs[npc.ID] = npc

		with open(DirectoryGlobal.npc_data(), 'w+', encoding='utf-16') as f:
			f.writelines([f'{npc}\n' for npc in mapped_npcs.values()])
		PK2DataGlobal.npcs = mapped_npcs

	def extract_data_from_pk2_entry(self, entry: str) -> List[IPK2Type]:
		file_bytes: List[c_byte] = PK2Main.instance().PK2Class.get_file(entry).decode('utf-16')
		monsters: List[IPK2Type] = []

		with StringIO(file_bytes, newline=None) as sr:
			for x in sr.readlines():
				npc_attributes: List[str] = x.strip('\n').split(self._separator_)
				if len(npc_attributes) < 6 or npc_attributes[0].startswith('//'):
					continue
				# Make string with
				# ID   long_id  name_id  level   hp
				try:
					value = PK2DataGlobal.names.get(npc_attributes[5], None)
					if value:
						npc_attributes[5] = value
					# concatenate type flags to define type later on
					# first turn 3rd type attribute to hex
					hex_12 = format(int(npc_attributes[12]), 'X') if len(npc_attributes[12]) > 1 else npc_attributes[12]
					type_name = npc_attributes[10] + npc_attributes[11] + hex_12 + npc_attributes[14] + npc_attributes[15]
					monsters.append(PK2NPC(npc_attributes[1], npc_attributes[2], npc_attributes[5], npc_attributes[57],
					                       npc_attributes[59], type_name))
				except Exception as e:
					logging.log(PK2, "extractNPC: " + ",".join(npc_attributes))
		return monsters

	def extract_file_names(self) -> List[str]:
		monster_files: List[str] = []
		item_data: List[c_byte] = PK2Main.instance().PK2Class.get_file("characterdata.txt").decode('utf-16')
		if PK2Main.instance().PK2Class is None or item_data is None:
			return monster_files

		with StringIO(item_data, newline=None) as sr:
			monster_files = [x.strip('\n') for x in sr.readlines() if re.match(r'^\w', x)]

		return monster_files

	def load_from_txt_file(self):
		try:
			# force normal extraction in case last extraction was faulty
			if not os.path.isfile(DirectoryGlobal.npc_data()) or os.stat(DirectoryGlobal.npc_data()).st_size < 128:
				raise BufferError()

			npcs: Dict[int, PK2NPC] = {}
			with open(DirectoryGlobal.npc_data(), newline=None, encoding='utf-16') as sr:
				for line in sr.readlines():
					npc: List[str] = line.strip('\n').split('|')
					if not self.is_valid(npc, len(npc), 6) or int(npc[0]) in npcs:
						continue
					npcs[int(npc[0])] = PK2NPC(npc[0], npc[1], npc[2], npc[3], npc[4], npc[5])
			PK2DataGlobal.npcs = npcs
		except Exception as e:
			logging.log(PK2, "\"data/npcs.txt\" not found. Attempting to extract PK2-Info.")
			self.check_pk2_then_extract()
