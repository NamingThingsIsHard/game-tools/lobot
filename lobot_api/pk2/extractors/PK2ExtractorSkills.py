import logging
import multiprocessing
import os
import re
import sys
from io import StringIO
from typing import List, Dict, Set

from lobot_api.Bot import Bot
from lobot_api.globals.DirectoryGlobal import DirectoryGlobal
from lobot_api.globals.PK2DataGlobal import PK2DataGlobal
from lobot_api.logger.Logger import PK2
from lobot_api.pk2.IPK2Type import IPK2Type
from lobot_api.pk2.PK2Item import PK2ItemType
from lobot_api.pk2.PK2Main import PK2Main
from lobot_api.pk2.PK2Skill import PK2Skill
from lobot_api.pk2.extractors.PK2CryptoHelper import PK2CryptoHelper
from lobot_api.pk2.extractors.PK2ExtractorTemplate import PK2ExtractorTemplate
from lobot_api.structs.CharData import Skill


def parallel_call(params):  # a helper for calling 'remote' instances
	cls = getattr(sys.modules[__name__], params[0])  # get our class type
	instance = cls.__new__(cls)  # create a new instance without invoking __init__
	instance.__dict__ = params[1]  # apply the passed state to the new instance
	method = getattr(instance, params[2])  # get the requested method
	args = params[3] if isinstance(params[3], (list, tuple)) else [params[3]]
	return method(*args)  # expand arguments, call our method and return the result


class PK2ExtractorSkills(PK2ExtractorTemplate):
	
	"""Files to process. Mapping of name to file contents."""
	skill_files: Dict[str, str] = {}
	
	@staticmethod
	def name_filter():
		return re.compile(r"^SKILL_((PUNCH)_01|(CH|EU)_(\w+)_BASE_01)")

	@staticmethod
	def preprocess_skills(file_names):
		"""
		Save file bytes to static dictionary to prevent preprocessing conflicts while reading pk2 file.
		:param file_names: Files to be preprocessed.
		:return: None
		"""
		for name in file_names:
			PK2ExtractorSkills.skill_files[name] = PK2Main.instance().PK2Class.get_file(name)

	def multiprocess_skills(self, file_names):
		""" Use this to create pickled remote call for multiprocessing
		:param file_names: The skill files to be extracted
		:return: None
		"""
		for name in file_names:
			yield [self.__class__.__name__, self.__dict__, 'extract_data_from_pk2_entry', name]

	def extract_and_map_data(self):
		skill_file_names: List[str] = self.extract_file_names()
		skills = []  # : List[IPK2Type]
		mapped_skills: Dict[int, PK2Skill] = {}

		self.preprocess_skills(skill_file_names)

		# Split slow, cpu-bound decryption in skill extraction using multiprocessing
		# Use default number of processes (processes= # of cpu's)
		with multiprocessing.Pool() as pool:
			extracted = pool.map(parallel_call, self.multiprocess_skills(skill_file_names))
			flattened = [skill for sublist in extracted for skill in sublist]
			skills.extend(flattened)

		PK2ExtractorSkills.skill_files.clear()

		for skill in skills:
			try:
				mapped_skills.update({skill.ID: skill})  # [skill.ID] = skill
			except Exception as e:
				logging.log(PK2, "Exception mapSkill: " + str(skill))
		with open(DirectoryGlobal.skill_data(), 'w+', encoding='utf-16') as f:
			f.writelines([f'{skill}\n' for skill in mapped_skills.values()])
		PK2DataGlobal.skills = mapped_skills
		self.__add_autoattack_skills_to_skill_list__()

	def extract_data_from_pk2_entry(self, entry: str) -> List[IPK2Type]:
		"""Get skill file from preprocessed list or from PK2.
		Decrypt file if name is e.g. skilldata_5000enc.txt
		"""
		encrypted = PK2ExtractorSkills.skill_files.get(entry, PK2Main.instance().PK2Class.get_file(entry))
		file_bytes = PK2CryptoHelper.decrypt(encrypted)
		skills: List[PK2Skill] = []

		with StringIO(file_bytes) as sr:
			for x in sr.readlines():
				skill_attributes: List[str] = x.strip('\n').split(self._separator_)
				if len(skill_attributes) < 63 or skill_attributes[0].startswith('//'):
					continue
				# Make string with
				# ID,Data,Name,CastTime,Cool# do:wn,Duration,MP,Type
				try:
					value = PK2DataGlobal.names.get(skill_attributes[62], None)
					if value:
						skill_attributes[62] = value
					skills.append(
						PK2Skill.PK2Skill_type_list(skill_attributes[1], skill_attributes[3], skill_attributes[62],
						                            skill_attributes[13],
						                            skill_attributes[14], skill_attributes[70], skill_attributes[53],
						                            skill_attributes,
						                            skill_attributes[22]))
					self.__try_add_autoattack_skills__(skill_attributes[1], skill_attributes[3])
				except Exception as e:
					logging.log(PK2, "extractSkill: " + "".join(skill_attributes))

		return skills

	def extract_file_names(self) -> List[str]:
		skill_files: List[str] = []

		# filter out names for skilldata files
		regex = re.compile(r"skilldata_\d+\.txt", flags=re.IGNORECASE)
		regex_enc = re.compile(r"skilldata_\d+enc\.txt", flags=re.IGNORECASE)

		files: Set[str] = set(
			filter(lambda file_name: regex.match(file_name), PK2Main.instance().PK2Class.get_file_names()))
		files_enc: Set[str] = set(
			filter(lambda file_name: regex_enc.match(file_name), PK2Main.instance().PK2Class.get_file_names()))

		# Compare files and keep the bigger one
		if len(files) < 1:
			skill_files = list(files_enc)
		elif len(files_enc) < 1:
			skill_files = list(files)
		else:
			# Switched to extracting using text file signature because some private servers leave random additional (IMPORTANT) skill files without replacing old ones
			for fString in files:
				value = PK2Main.instance().PK2Class.get_file(fString)
				bytes: List[int] = value if value else [0]
				f_enc_string: str = fString.replace(".txt", "enc.txt")

				next_enc = next((fEnc for fEnc in files_enc if fEnc == f_enc_string), "")
				bytes_enc: List[int] = PK2Main.instance().PK2Class.get_file(next_enc) if next_enc else [0]
				if len(bytes) >= len(bytes_enc) - 22:
					skill_files.append(fString)
				else:
					skill_files.append(f_enc_string)
		return skill_files

	@staticmethod
	def __try_add_autoattack_skills__(id_: str, skill: str):
		filter_skills = PK2ExtractorSkills.name_filter()
		match = filter_skills.match(skill)
		if match:
			s: Skill = Skill(int(id_), 1, True)
			types = []
			if match.group(2) == "PUNCH":
				types.append(PK2ItemType.Other)
			elif match.group(3) == "CH" and match.group(4) == "SWORD":
				types += [PK2ItemType.Blade, PK2ItemType.Sword]
			elif match.group(4) == "SPEAR":
				types += [PK2ItemType.Glaive, PK2ItemType.Spear]
			elif match.group(4) == "BOW":
				types.append(PK2ItemType.Bow)
			elif match.group(3) == "EU" and match.group(4) == "SWORD":
				types.append(PK2ItemType.SwordOnehander)
			elif match.group(4) == "TSWORD":
				types.append(PK2ItemType.SwordTwohander)
			elif match.group(4) == "AXE":
				types.append(PK2ItemType.Axe)
			elif match.group(4) == "CROSSBOW":
				types.append(PK2ItemType.Crossbow)
			elif match.group(4) == "DAGGER":
				types.append(PK2ItemType.Dagger)
			elif match.group(4) == "STAFF":
				types.append(PK2ItemType.MageStaff)
			elif match.group(4) == "WAND_WARLOCK":
				types.append(PK2ItemType.Darkstaff)
			elif match.group(4) == "HARP":
				types.append(PK2ItemType.Harp)
			elif match.group(4) == "WAND_CLERIC":
				types.append(PK2ItemType.ClericStaff)

			if types:
				for t in types:
					Bot.auto_attack_skills[t] = s
				Bot.auto_attack_skills_ids[s.ref_id] = s

	@staticmethod
	def __add_autoattack_skills_to_skill_list__():
		for k, v in Bot.auto_attack_skills.items():
			if v not in Bot.char_data.skills:
				Bot.char_data.skills.append(v)

	def load_from_txt_file(self):
		try:
			# force normal extraction in case last extraction was faulty
			if not os.path.isfile(DirectoryGlobal.skill_data()) or os.stat(DirectoryGlobal.skill_data()).st_size < 128:
				raise BufferError("no file")

			skills: Dict[int, PK2Skill] = {}
			with open(DirectoryGlobal.skill_data(), newline=None, encoding='utf-16') as sr:
				for line in sr:
					skill: List[str] = line.strip('\n').split('|')
					if not self.is_valid(skill, len(skill), 9) or int(skill[0]) in skills:
						continue
					skills[int(skill[0])] = PK2Skill(skill[0], skill[1], skill[2], skill[3], skill[4], skill[5],
					                                 skill[6],
					                                 skill[7], skill[8])
					self.__try_add_autoattack_skills__(skill[0], skill[1])
			PK2DataGlobal.skills = skills
			self.__add_autoattack_skills_to_skill_list__()
		except Exception as e:
			logging.log(PK2, "\"data/skillData.txt\" not found. Attempting to extract PK2-Info.")
			self.check_pk2_then_extract()

# if __name__ == "__main__":
# 	with multiprocessing.Pool() as pool:
# 		pool.map(cpu_bound, numbers)
