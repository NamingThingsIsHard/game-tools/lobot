import logging
import os
import re
from io import StringIO
from typing import List, Dict

from lobot_api.globals.DirectoryGlobal import DirectoryGlobal
from lobot_api.globals.PK2DataGlobal import PK2DataGlobal
from lobot_api.logger.Logger import PK2
from lobot_api.pk2.IPK2Type import IPK2Type
from lobot_api.pk2.PK2Item import PK2Item
from lobot_api.pk2.PK2Main import PK2Main
from lobot_api.pk2.extractors.PK2ExtractorTemplate import PK2ExtractorTemplate


class PK2ExtractorItems(PK2ExtractorTemplate):
	def extract_and_map_data(self):
		item_file_names: List[str] = self.extract_file_names()
		items = []
		mapped_items: Dict[int, IPK2Type] = {}
		for fileName in item_file_names:
			items.extend(self.extract_data_from_pk2_entry(fileName))
		for item in items:
			try:
				# mapped_items.append(item.ID, item)
				mapped_items[item.ID] = item
			except Exception as e:
				pass
		with open(DirectoryGlobal.item_data(), 'w+', encoding='utf-16') as f:
			f.writelines([f'{item}\n' for item in mapped_items.values()])
		PK2DataGlobal.items = mapped_items

	def extract_data_from_pk2_entry(self, entry: str) -> List[IPK2Type]:
		item_file_bytes = PK2Main.instance().PK2Class.get_file(entry).decode('utf-16')
		items: List[IPK2Type] = []

		with StringIO(item_file_bytes, newline=None) as sr:
			for x in sr.readlines():
				item_attributes: List[str] = x.strip('\n').split(self._separator_)

				if len(item_attributes) < 58 or item_attributes[0].startswith('//'):
					continue

				# id, long_id, name, type, level, maxStacks
				try:
					value: str = PK2DataGlobal.names.get(item_attributes[5], None)
					if value:
						item_attributes[5] = value

					# concatenate type flags to define type later on
					type2 = format(int(item_attributes[10]), 'X')
					type3 = format(int(item_attributes[11]), 'X')
					type4 = format(int(item_attributes[12]), '02X')
					type_name: str = f"{type2}{type3}{type4}"

					# prevent out of bounds exceptions for items without any gender or degree
					gender_string: str = item_attributes[58] if len(item_attributes) > 58 else "2"

					degree = item_attributes[61] if len(item_attributes) > 61 else 0
					# For equipment, degree number is given as (the tier number * degree), so a division by 3 is needed,
					# degree: int = len(item_attributes) > 61
					if type_name.startswith('1'):
						degree = int((int(item_attributes[61]) + 3 - 1) / 3 if len(item_attributes) > 61 else 0)

					items.append(
						PK2Item(item_attributes[1], item_attributes[2], item_attributes[5], item_attributes[7],
						        type_name,
						        item_attributes[14], item_attributes[15], item_attributes[19], item_attributes[33],
						        item_attributes[57], gender_string, str(degree)))
				except Exception as e:
					logging.log(PK2, "extractItem: " + ",".join(item_attributes))

		return items

	def extract_file_names(self) -> List[str]:
		item_files: List[str] = []
		item_data = PK2Main.instance().PK2Class.get_file("itemdata.txt").decode('utf-16')
		if PK2Main.instance().PK2Class is None or item_data is None:
			return item_files

		with StringIO(item_data, newline=None) as sr:
			item_files = [x.strip('\n') for x in sr.readlines() if re.match(r'^\w', x)]

		return item_files

	def load_from_txt_file(self):
		try:
			# force normal extraction in case last extraction was faulty
			if not os.path.isfile(DirectoryGlobal.item_data()) or os.stat(DirectoryGlobal.item_data()).st_size < 128:
				raise BufferError()

			items: Dict[int, PK2Item] = {}
			with open(DirectoryGlobal.item_data(), newline=None, encoding='utf-16') as sr:
				for line in sr.readlines():
					item: List[str] = line.strip('\n').split('|')
					if not PK2ExtractorTemplate.is_valid(item, len(item), 12) or int(item[0]) in items:
						continue
					items[int(item[0])] = PK2Item(item[0], item[1], item[2], item[3], item[4], item[5], item[6],
					                              item[7],
					                              item[8], item[9], item[10], item[11])
			PK2DataGlobal.items = items
		except Exception as e:
			logging.log(PK2, "\"data/items.txt\" not found. Attempting to extract PK2-Info.")
			self.check_pk2_then_extract()
