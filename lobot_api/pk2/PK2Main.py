import os
import struct
from typing import List

from lobot_api.globals.NetworkGlobal import NetworkGlobal
from lobot_api.globals.PK2DataGlobal import PK2DataGlobal
from lobot_api.globals.settings.LoginSettings import LoginSettings
from lobot_api.pk2.PK2Class import PK2Class
from silkroad_security_api_1_4.Blowfish import Blowfish


class PK2Main:
	s_instance = None  #: PK2Main
	# _enc: UnicodeEncoding
	s_pK2Class: PK2Class = None

	@property
	def PK2Class(self) -> PK2Class:
		if PK2Main.s_pK2Class is None and os.path.exists(PK2DataGlobal.pk2_media_file_path()):
			PK2Main.s_pK2Class = PK2Class(PK2DataGlobal.pk2_media_file_path())
		return PK2Main.s_pK2Class

	@staticmethod
	def instance():
		if PK2Main.s_instance is None:
			PK2Main.s_instance = PK2Main()
		return PK2Main.s_instance

	@staticmethod
	def reset():
		PK2Main.s_instance = None
		PK2Main.s_pK2Class = None

	def get_silkroad_version(self):
		""" Get server version from sv.t file in media.pk2 to send a patch request.
		"""
		b_silkroad_version = self.PK2Class.get_file("sv.t")
		initialization_string = "SILKROADVERSION".encode(encoding='ascii')
		bytes_to_decode = struct.unpack('I', bytes(b_silkroad_version[:4]))[0]
		if b_silkroad_version is None or bytes_to_decode > 8:
			return

		bf: Blowfish = Blowfish()
		bf.initialize(initialization_string, 0, 8)
		# decoded: List[c_byte] = [0x00] * bytes_to_decode
		decoded = bf.decode(b_silkroad_version, 4, int(bytes_to_decode))
		decoded_ascii: str = bytes(decoded).decode('windows-1250').rstrip('\x00')
		LoginSettings.instance().version = int(decoded_ascii)

	def get_server_ip_address(self):
		b_server_ip_address = self.PK2Class.get_file("divisioninfo.txt")
		if b_server_ip_address is None:
			return

		str_server_ip_address: str = b_server_ip_address.decode('windows-1250')
		arr_server_ip_address: List[str] = str_server_ip_address.split('\0')
		LoginSettings.instance().locale = ord(arr_server_ip_address[0][0])
		NetworkGlobal.instance().gateway_server_division = arr_server_ip_address[len(arr_server_ip_address) - 6]
		NetworkGlobal.instance().gateway_ip = arr_server_ip_address[len(arr_server_ip_address) - 2]

	def get_server_port(self):
		b_server_port = self.PK2Class.get_file("gateport.txt")
		if b_server_port is None:
			return
		else:
			str_port = b_server_port.decode('windows-1250').rstrip('\x00')
			NetworkGlobal.instance().gateway_port = int(str_port)
