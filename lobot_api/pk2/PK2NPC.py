from enum import Enum

from lobot_api.pk2.IPK2Type import IPK2Type


class PK2NPCType(Enum):
	Other = 0x00
	MonsterEvent = 0x01
	PlayerCh = 0x10000
	PlayerEU = 0x10010
	Monster = 0x21100
	MonsterUniqueA = 0x21110
	MonsterUniqueB = 0x21113
	MonsterBossA = 0x21130
	MonsterBossB = 0x21131
	MonsterBossC = 0x21133
	MonsterBossD = 0x21136
	MonsterEventStrong = 0x21137  # BossE
	MonsterRaidBoss = 0x21138  # BossF
	MonsterTradeThief = 0x21230
	MonsterTradeHunter = 0x21330
	MonsterQuestCH = 0x21400
	MonsterQuestEU = 0x21410
	MonsterQuestUnique = 0x21430
	MonsterSummon = 0x21530
	NPCInteractive = 0x22030
	NPCFortressStructure = 0x22130
	FortressSpecial1 = 0x23A32
	FortressSpecial2 = 0x23B32
	PetGrowth = 0x23330
	PetPickupStart = 0x23400
	PetPickup = 0x23430   # alias "PetAbility"
	PetGuildCH = 0x23500
	PetGuildEU = 0x23510
	PetGuardianB = 0x24130
	PetFortressHangar = 0x24330
	PetRide = 0x23130
	PetTransport = 0x23231
	PetTransportMall = 0x23232


class PK2NPC(IPK2Type):
	def __init__(self,id_: str, long_id: str, name: str, level: str, hp: str, type_value: str):
		super(PK2NPC, self).__init__(int(id_), long_id, name)  # id_ c_uint32 is not hashable
		self.level = int(level)
		self.HP = int(hp)
		self.type = PK2NPC.get_npc_type(type_value, long_id)

	def __str__(self):
		return f"{self.ID}|{self.long_id}|{self.name}|{self.level}|{self.HP}|{format(self.type.value, 'X')}"

	def __iter__(self):
		return iter(str(self.ID))

	def to_type(self) -> IPK2Type:
		return self

	@staticmethod
	def get_npc_type(type_flags: str, long_id: str) -> PK2NPCType:
		if long_id.startswith("MOB_EV"):
			return PK2NPCType.MonsterEvent

		value = int(type_flags, 16)
		if value in PK2NPCType._value2member_map_.keys():
			return PK2NPCType(value)
		else:
			return PK2NPCType.Other
