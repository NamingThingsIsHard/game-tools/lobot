from abc import ABC


class IPK2Type(ABC):
	"""Marker interface for PK2 Types"""

	def __init__(self, id=0, long_id=0, name=''):
		self.ID = id
		self.long_id = long_id
		self.name = name

	@property
	def to_type(self): return self

	def __eq__(self, other):
		return other is not None and self.ID == other.ID

	def __bool__(self):
		return self is not None
