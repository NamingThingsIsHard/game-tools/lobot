from typing import List

from lobot_api.pk2.IPK2Type import IPK2Type


class PK2TeleporterData(IPK2Type):
	def __init__(self, id_: str = "", long_id: str = "", name: str = "", teleport_links: List[int] = None):
		if not teleport_links:
			teleport_links = []
		super(PK2TeleporterData, self).__init__(id_, long_id, name)
		self.ID = 0 if not id_ else int(id_)
		self.long_id = long_id
		self.name = name
		self.TeleportLinks = teleport_links

	def __repr__(self):
		return f"({self.ID}){self.name}"

	def __str__(self):
		if self.ID == 0 and self.long_id == '' and self.name == '' and len(self.TeleportLinks) == 0:
			return ""
		elif len(self.TeleportLinks) == 0:
			return f"{self.ID}|{self.long_id}|{self.name}"
		else:
			return f"{str(self.ID)}|{self.long_id}|{self.name}|{'|'.join(str(link) for link in self.TeleportLinks)}"


class PK2Teleporter(IPK2Type):
	def __init__(self, id_: str = "", long_id: str = "", name: str = "", teleporter_data: PK2TeleporterData = None):
		super(PK2Teleporter, self).__init__(id_, long_id, name)
		self.ID = 0 if not id_ else int(id_)
		self.long_id = long_id
		self.name = name
		self.TeleporterData = teleporter_data

	def __repr__(self):
		return f"({self.ID}){self.name}"

	def __str__(self):
		try:
			return (f"{self.ID}|{self.long_id}|{self.name}"
			        if self.TeleporterData.TeleportLinks and len(self.TeleporterData.TeleportLinks) == 0
			        else f"{self.ID}|{self.long_id}|{self.name}|{str(self.TeleporterData)}")
		except Exception as e:
			pass

	def to_type(self) -> IPK2Type:
		return self
