from enum import Enum
from typing import List, Union

from lobot_api.pk2.IPK2Type import IPK2Type
from lobot_api.pk2.PK2Item import PK2ItemType


class PK2SkillType(Enum):
	# BasicType  == 0
	Passive = 0
	# BasicType == 1
	Imbue = 1
	SpeedBuff = 2
	# BasicType == 2
	Buff = 3
	Heal = 4
	SelfHeal = 5
	ManaBuff = 6
	Resurrection = 7
	Debuff = 8
	Cure = 9
	KnockDownOnly = 0x0A
	Knockdown = 0x0B
	WarlockDOT = 0x0C
	Other = 0x0F

	# BasicType  == 0
	# Passive = 0
	# BasicType == 1
	# Imbue = 1 // Selfbuff -> Powerup scrolls count as imbues as well
	# MovementBuff = 2 // BasicType == 1 and skillAttributes> 0
	# BasicType == 2
	# Buff = 3
	# Heal = 4 //Target required = 1 (index 22) and Heal > 0 (index 70)
	# SelfHeal = 5 //Target required = 0 (index 22) and Heal > 0 (index 70)
	# KnockDown = 6
	# Resurrection = 6 //Target required = 0 (index 22) and Heal > 0 (index 70)
	# Active = 7
	# Other = 8yy

	def is_knock_down_skill(self) -> bool:
		return self is PK2SkillType.Knockdown \
		       or self is PK2SkillType.KnockDownOnly

	def is_attack(self) -> bool:
		return (self is PK2SkillType.Debuff
		        or self is PK2SkillType.Knockdown
		        or self is PK2SkillType.KnockDownOnly
		        or self is PK2SkillType.WarlockDOT
		        or self is PK2SkillType.Other)

	def is_buff(self) -> bool:
		return (self is PK2SkillType.Buff
		        or self is PK2SkillType.ManaBuff
		        or self is PK2SkillType.Heal
		        or self is PK2SkillType.SelfHeal
		        or self is PK2SkillType.SpeedBuff)

	def is_heal(self) -> bool:
		return self is PK2SkillType.Heal or self is PK2SkillType.SelfHeal

	@staticmethod
	def type_predicate():  # -> Dict[PK2SkillType, Callable[[List[str]], bool]]:
		return {
			PK2SkillType.Passive: lambda s: s[8] == "0",
			PK2SkillType.Imbue: lambda s: "GIGONGTA" in s[3] or "POISONA_BLADE" in s[3],
			PK2SkillType.SpeedBuff: lambda s: "SKILL_CH_LIGHTNING_GYEONGGONG_A" in s[
				3] or "SKILL_CH_LIGHTNING_GYEONGGONG_C" in s[3] or "BARD_SPEEDUPA_MSPEED" in s[3] or "SPEED_UP" in s[3],
			# Chinese,  Euro and Item Speedbuff
			PK2SkillType.Buff: lambda s: ("SKILL_CH_SWORD_SHIELD" in s[3]  # Blade
			                              or "SKILL_CH_SPEAR_SPIN" in s[3]  # Spear
			                              or "SKILL_CH_BOW_CALL" in s[3] or "SKILL_CH_BOW_NORMAL" in s[3]  # Bow
			                              or "SKILL_CH_COLD_GANGGI" in s[3] or "SKILL_CH_COLD_SHIELD" in s[3]  # Cold
			                              or "SKILL_CH_FIRE_GONGUP" in s[3] or "SKILL_CH_FIRE_GANGGI" in s[
				                              3] or "SKILL_CH_FIRE_SHIELD" in s[3] or "SKILL_CH_FIRE_HWABYEOK" in s[
				                              3]  # Fire
			                              or "SKILL_CH_LIGHTNING_GWANTONG" in s[3] or "SKILL_CH_LIGHTNING_JIPJUNG" in s[
				                              3]  # Lightnining
			                              or "SKILL_EU_CLERIC_HEALA_TARGET" in s[3]  # Force
			                              or "SKILL_EU_WARRIOR_FRENZYA_HEALTH" in s[
				                              3] or "SKILL_EU_WARRIOR_FRENZYA_PHYSICAL_BLOCK" in s[
				                              3] or "SKILL_EU_WARRIOR_FRENZYA_MASICAL_BLOCK" in s[3]
			                              or "SKILL_EU_WARRIOR_FRENZYA_DAMAGE" in s[3] or "SKILL_EU_WARRIOR_GUARDA" in
			                              s[3]  # Warrior
			                              or "SKILL_EU_ROG_POISONA_GUARD" in s[3] or "SKILL_EU_ROG_BOWP_MAD_BOW_UP" in
			                              s[3]
			                              or "SKILL_EU_ROG_POIS_DAGP_DAGGAR_UP" in s[3]  # Rogue
			                              or "SKILL_EU_WIZARD_MENTALA_DAMAGEUP" in s[
				                              3] or "SKILL_EU_WIZARD_EARTHA_GUARD" in s[
				                              3] or "SKILL_EU_WIZARD_MENTALA_DAMAGEUP" in s[3]  # Wizard
			                              or "SKILL_EU_WARLOCK_SOULA_CHAOS" in s[
				                              3] or "SKILL_EU_WARLOCK_SOULA_STUNLINK" in s[
				                              3] or "SKILL_EU_WARLOCK_SOULA_RETURN" in s[3]  # Warlock
			                              or "SKILL_EU_BARD_BATTLAA_GUARD" in s[3] or "EU_BARD_SPEEDUPA_HITRATE" in s[3]
			                              or "SKILL_EU_BARD_RECOVERA_ABNORMAL" in s[3] or "SKILL_EU_BARD_DANCEA" in s[
				                              3] or "SKILL_EU_BARD_FORGETA_ATTACK" in s[3]  # Bard
			                              or "SKILL_EU_CLERIC_SAINTA_INNOCENT" in s[
				                              3] or "SKILL_EU_CLERIC_RECOVERYA_HEALSHIELD" in s[
				                              3] or "SKILL_EU_CLERIC_SAINTA_ABNORMAL" in s[3]  # Cleric
			                              or "_damage_div_idE" in s[3]  # Chinese damage divide
			                              or "SKILL_OP_HARMONY" in s[3]),  # GM Skill
			PK2SkillType.Heal: lambda s: ("SKILL_CH_WATER_HEAL" in s[3]  # Force
			                              or "SKILL_EU_CLERIC_HEALA_TARGET" in s[
				                              3] or "skill_eu_cleric_heala_div_idE" in
			                              s[3]
			                              or "SKILL_EU_CLERIC_HEALA_CYCLE" in s[
				                              3] or "SKILL_EU_CLERIC_RECOVERYA_TARGET" in s[3]),  # Cleric
			PK2SkillType.SelfHeal: lambda s: ("SN_SKILL_CH_WATER_SELFHEAL" in s[3]  # Force
			                                  or "SKILL_EU_CLERIC_HEALA_GROUP" in s[
				                                  3] or "SKILL_EU_CLERIC_RECOVERYA_GROUP" in s[3]
			                                  or "SKILL_EU_CLERIC_RECOVERYA_QUICK" in s[3]),  # Cleric
			PK2SkillType.ManaBuff: lambda s: "SKILL_EU_BARD_RECOVERA_MPHEAL" in s[3] or "EU_BARD_RECOVERA_MANATRANS" in
			                                 s[3],  # Bard mana buffs
			PK2SkillType.Resurrection: lambda s: "SKILL_CH_WATER_RESURRECTION" in s[
				3] or "SN_SKILL_EU_CLERIC_REBIRTH" in s[3],
			PK2SkillType.Debuff: lambda s: ("SKILL_CH_COLD_GIGONGJANG" in s[3]  # Coldwave
			                                or "SKILL_CH_COLD_BINGPAN" in s[3]  # Frost nova
			                                or "SKILL_CH_WATER_CANCEL" in s[3]  # Vital Spot
			                                or "SKILL_EU_WIZARD_COLDA_MANADRY" in s[
				                                3] or "SKILL_EU_WIZARD_EARTHA_ABNORMAL" in s[
				                                3] or "SKILL_EU_WIZARD_PSYCHICA_UNTOUCH" in s[
				                                3]  # Wizard mana drain, root and fear
			                                or "SKILL_EU_WARLOCK_SOULA" in s[3] or "SKILL_EU_WARLOCK_RAZEA" in s[
				                                3] or "SKILL_EU_WARLOCK_CONFUSIONA" in s[3]  # Warlock debuffs
			                                or "SKILL_EU_BARD_FORGETA_TARGET" in s[3] or "SKILL_EU_BARD_BATTLAA_ROOT" in
			                                s[3]),  # Bard temptation, root
			PK2SkillType.Cure: lambda s: ("SKILL_CH_WATER_CURE" in s[3]  # Force Cure
			                              or "SKILL_EU_BARD_RECOVERA_ABNORMAL" in s[3]  # Bard cure series
			                              or "SKILL_EU_CLERIC_SAINTA_INNOCENT" in s[3]),  # Cleric cure series
			PK2SkillType.KnockDownOnly: lambda s: "SKILL_CH_SWORD_DOWNATTACK" in s[3],  # Blader Skills
			PK2SkillType.Knockdown: lambda s: (
					"SKILL_EU_WARRIOR_TWOHANDA_CHARGE" in s[3] or "SKILL_EU_WARRIOR_TWOHANDA_CRY" in s[
				3] or "SKILL_EU_WARRIOR_TWOHANDA_CHARGE" in s[3]  # Warrior 2hand
					or "SKILL_EU_ROG_BOWA_POWER" in s[3] or "SN_SKILL_EU_ROG_DAGGERA_WOUND" in s[3]),  # Rogue
			PK2SkillType.WarlockDOT: lambda s: "SKILL_EU_WARLOCK_DOT" in s[3]  # Warlock DOT
		}

	@staticmethod
	def get_type(skill_entry: List[str]):  # -> PK2SkillType:
		for key, value in PK2SkillType.type_predicate().items():
			if value(skill_entry):
				return key
		return PK2SkillType.Other


class PK2Skill(IPK2Type):
	def __init__(self, id_: str, long_id: str, name: str, cast_time: str, cooldown: str, duration: str, mp: str,
	             type_value: Union[PK2SkillType, str], target_required: str):
		name = '' if name == 'xxx' else name
		super(PK2Skill, self).__init__(int(id_), long_id, name)
		self.cast_time = int(cast_time)
		self.cooldown = int(cooldown)
		self.duration = int(duration)
		self.mp = int(mp)

		if isinstance(type_value, str):
			type_value = self.get_skill_type_str(type_value)
		self.type = type_value
		self.required_weapon = self.get_required_weapon_type(long_id)
		self.target_required = target_required == "1"

	@staticmethod
	def get_required_weapon_type( skill_id: str) -> PK2ItemType:
		if skill_id.startswith("SKILL_CH_SWORD"):
			return PK2ItemType.Sword
		elif skill_id.startswith("SKILL_CH_SPEAR"):
			return PK2ItemType.Spear
		elif skill_id.startswith("SKILL_CH_BOW"):
			return PK2ItemType.Bow
		elif skill_id.startswith("SKILL_CH_FIRE_SHIELD") or skill_id.startswith("SKILL_CH_SWORD_SHIELD"):
			return PK2ItemType.ShieldCH
		elif skill_id.startswith("SKILL_EU_WARRIOR_ONEHANDA_SHIELD"):
			return PK2ItemType.ShieldEU
		elif skill_id.startswith("SKILL_EU_WARRIOR_ONEHANDA") or skill_id.startswith("SKILL_EU_SWORD"):
			return PK2ItemType.SwordOnehander
		elif skill_id.startswith("SKILL_EU_WARRIOR_TWOHAND") or skill_id.startswith("SKILL_EU_TSWORD"):
			return PK2ItemType.SwordTwohander
		elif skill_id.startswith("SKILL_EU_WARRIOR_DUALA") or skill_id.startswith("SKILL_EU_AXE"):
			return PK2ItemType.Axe
		elif skill_id.startswith("SKILL_EU_WARLOCK") or skill_id.startswith("SKILL_EU_WAND_WARLOCK"):
			return PK2ItemType.Darkstaff
		elif skill_id.startswith("SKILL_EU_WIZARD") or skill_id.startswith("SKILL_EU_STAFF"):
			return PK2ItemType.MageStaff
		elif skill_id.startswith("SKILL_EU_ROG_BOW") or skill_id.startswith("SKILL_EU_CROSSBOW"):
			return PK2ItemType.Crossbow
		elif skill_id.startswith("SKILL_EU_ROG_DAGGER") or skill_id.startswith("SKILL_EU_DAGGER") \
			or skill_id.startswith("SKILL_EU_ROG_STEALTHA_ATTACK") or skill_id.startswith("SKILL_EU_ROG_POIS_DAGP_DAGGAR"):
			return PK2ItemType.Dagger
		elif skill_id.startswith("SKILL_EU_BARD") or skill_id.startswith("SKILL_EU_HARP"):
			return PK2ItemType.Harp
		elif skill_id.startswith("SKILL_EU_CLERIC") or skill_id.startswith("SKILL_EU_WAND_CLERIC"):
			return PK2ItemType.ClericStaff
		else:
			return PK2ItemType.Other

	@staticmethod
	def PK2Skill_type_str(id_: str, long_id: str, name: str, cast_time: str, cooldown: str, duration: str, mp: str,
	                      type_value: str, target_required: str):
		return PK2Skill(id_, long_id, name, cast_time, cooldown, duration, mp, PK2Skill.get_skill_type_str(type_value),
		                target_required)

	"""ID,long_id,Name,CastTime,Cooldown,Duration,MP,Type"""

	@staticmethod
	def PK2Skill_type_list(id_: str, long_id: str, name: str, cast_time: str, cooldown: str, duration: str, mp: str,
	                       type_value: List[str], target_required: str):
		return PK2Skill(id_, long_id, name, cast_time, cooldown, duration, mp, PK2Skill.get_skill_type_list(type_value),
		                target_required)

	@staticmethod
	def get_skill_type_str(type_string: str) -> PK2SkillType:
		"""Parse skill type from text file"""
		value = int(type_string, 16)
		if value in PK2SkillType._value2member_map_:
			return PK2SkillType(value)
		return PK2SkillType.Other

	# |format(self.type, 'X')+ "|" + (TargetRequired ? "1" : "0")
	@staticmethod
	def get_skill_type_list(skill_entry: List[str]) -> PK2SkillType:
		"""Custom chain of command has to be used due to opaque flags in pk2-entry."""
		return PK2SkillType.get_type(skill_entry)

	def to_type(self) -> IPK2Type:
		return self

	def __iter__(self):
		return iter(str(self.ID))

	def __repr__(self):
		return f"{self.ID}|{self.name}"

	def __str__(self):
		# take index 1 of type to remove preceding 0 instead of .trim_start('0'), removes: which value 0x00 entirely, in: resulting "other" being parsed after loading instead.
		return f"{self.ID}|{self.long_id}|{self.name}|{self.cast_time}|{self.cooldown}|{self.duration}|{self.mp}|{format(self.type.value, 'X')}|{int(self.target_required)}"

	@staticmethod
	def does_skill_match_weapon(pk2_skill, item_type: PK2ItemType) -> bool:
		"""Predicate of whether a weapon suits a given skill.
		Multiple weapons can match a skill. For example, glaive and spear can cast heuksal skills.
		"""
		# switch (pk2_skill.required_weapon)
		if pk2_skill.required_weapon == PK2ItemType.Other:
			return True
		if pk2_skill.required_weapon == PK2ItemType.ShieldCH:
			return item_type is PK2ItemType.ShieldCH
		if pk2_skill.required_weapon == PK2ItemType.ShieldEU:
			return item_type is PK2ItemType.ShieldEU
		if pk2_skill.required_weapon == PK2ItemType.Sword:
			return item_type is PK2ItemType.Sword or item_type is PK2ItemType.Blade
		if pk2_skill.required_weapon == PK2ItemType.Blade:
			return item_type is PK2ItemType.Sword or item_type is PK2ItemType.Blade
		if pk2_skill.required_weapon == PK2ItemType.Spear:
			return item_type is PK2ItemType.Spear or item_type is PK2ItemType.Glaive
		if pk2_skill.required_weapon == PK2ItemType.Glaive:
			return item_type is PK2ItemType.Spear or item_type is PK2ItemType.Glaive
		if pk2_skill.required_weapon == PK2ItemType.Bow:
			return item_type is PK2ItemType.Bow
		if pk2_skill.required_weapon == PK2ItemType.SwordOnehander:
			return item_type is PK2ItemType.SwordOnehander
		if pk2_skill.required_weapon == PK2ItemType.SwordTwohander:
			return item_type is PK2ItemType.SwordTwohander
		if pk2_skill.required_weapon == PK2ItemType.Axe:
			return item_type is PK2ItemType.Axe
		if pk2_skill.required_weapon == PK2ItemType.Darkstaff:
			return item_type is PK2ItemType.Darkstaff
		if pk2_skill.required_weapon == PK2ItemType.MageStaff:
			return item_type is PK2ItemType.MageStaff
		if pk2_skill.required_weapon == PK2ItemType.Crossbow:
			return item_type is PK2ItemType.Crossbow
		if pk2_skill.required_weapon == PK2ItemType.Dagger:
			return item_type is PK2ItemType.Dagger
		if pk2_skill.required_weapon == PK2ItemType.Harp:
			return item_type is PK2ItemType.Harp
		if pk2_skill.required_weapon == PK2ItemType.ClericStaff:
			return item_type is PK2ItemType.ClericStaff
		else:
			return False

	def is_transferable_buff(self) -> bool:
		return (self.type is PK2SkillType.Buff
		        and "SKILL_EU_CLERIC_RECOVERYA_QUICK_B" in self.long_id
		        or "SKILL_EU_CLERIC_RECOVERYA_GROUP" in self.long_id
		        or "BATTLAA_GUARD" in self.long_id
		        or "BARD_DANCEA" in self.long_id
		        or "BARD_SPEEDUPA_HITRATE" in self.long_id)
