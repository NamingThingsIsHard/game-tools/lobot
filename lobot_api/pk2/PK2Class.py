import atexit
import os
import struct
from ctypes import c_byte, c_uint64
from typing import List, BinaryIO

from lobot_api.structs.PK2File import PK2File
from silkroad_security_api_1_4.Blowfish import Blowfish


class PK2Class:
	"""The only folders to consider to reduce processing time. """
	relevant_folders = ['server_dep', 'silkroad', 'textdata']
	"""The Blowfish object to encrypt and decrypt."""
	blowfish: Blowfish = Blowfish()
	"""The blowfish key to encrypt and decrypt."""
	bKey: List[c_byte] = [0x32, 0xCE, 0xDD, 0x7C, 0xBC, 0xA8]

	def __init__(self, pk2_file_path: str):
		self.header: PK2File.PK2Header = None
		self.mainFolder = PK2File.PK2Folder()
		self.current_folder = PK2File.PK2Folder()

		self.entryBlocks: List[PK2File.PK2EntryBlock] = []
		self.Files: List[PK2File.PK2File] = []
		self.Folders: List[PK2File.PK2Folder] = []

		self.file_stream: BinaryIO = BinaryIO()

		if os.path.isfile(pk2_file_path):
			self.file_stream = open(pk2_file_path, "rb")
			PK2Class.blowfish.initialize(PK2Class.bKey)
			tup = self.buffer_to_struct(self.file_stream.read(256), PK2File.PK2Header.parse_str())
			header = PK2File.PK2Header(tup[0], tup[1], tup[2], tup[3], tup[4])

			self.current_folder = PK2File.PK2Folder()
			self.current_folder.name = pk2_file_path
			self.current_folder.files = []  # List[PK2File.PK2File]
			self.current_folder.subfolders = []  # List[PK2File.PK2Folder]

			self.mainFolder = self.current_folder
			# self.Read(reader.BaseStream.position)
			self.read(self.file_stream.tell())

		# destructor clean up open filestream
		atexit.register(self.file_stream.close)

	def file_exists(self, name: str) -> bool:
		file: PK2File.PK2File = next(item for item in self.Files if item.name.lower() == name.lower())

		if file.position != 0:
			return True
		else:
			return False

	def get_file(self, name: str) -> List[c_byte]:
		file: PK2File.PK2File = next(item for item in self.Files if item.name.lower() == name.lower())
		if file:
			self.file_stream.seek(file.position)
			return self.file_stream.read(file.size)
		else:
			return None

	def get_file_names(self) -> List[str]:

		tmp_list: List[str] = []

		for file in self.Files:
			tmp_list.append(file.name)

		return tmp_list

	def read(self, position: c_uint64):
		reader: BinaryIO = self.file_stream  # io.BytesIO(self.fileStream)
		reader.seek(position)
		result_bytes = reader.read(PK2File.PK2EntryBlock.struct_size())
		# open("lobot_read_bytes", 'wb').write(bytes(result_bytes))
		tmp_folders: List[PK2File.PK2Folder] = []  # List[PK2File.PK2Folder]
		entry_list: List[PK2File.PK2Entry] = []
		for x in range(0, len(result_bytes), 128):
			entry_bytes = self.buffer_to_struct(PK2Class.blowfish.decode(result_bytes[x:x + 128]),
			                                                  PK2File.PK2Entry.parse_str())
			# remove null bytes \x00 .decode('latin_1').rstrip('\x00')
			entry_list.append(
				PK2File.PK2Entry(entry_bytes[0], entry_bytes[1].decode('windows-1250').rstrip('\x00'), entry_bytes[2],
				                 entry_bytes[3], entry_bytes[4],
				                 entry_bytes[5], entry_bytes[6], entry_bytes[7], entry_bytes[8]))

		entry_block: PK2File.PK2EntryBlock = PK2File.PK2EntryBlock(entry_list)

		for i in range(0, 20):
			entry: PK2File.PK2Entry = entry_block.entries[i]
			# switch (entry.type)

			if entry.type_ == 0:
				break
			elif entry.type_ == 1:
				# if not entry.name.startswith('.') and not entry.name.startswith('..'):
				if entry.name in PK2Class.relevant_folders:
					tmp_folder: PK2File.PK2Folder = PK2File.PK2Folder()
					tmp_folder.name = entry.name
					tmp_folder.position = struct.unpack('q', entry.position_)[0]
					tmp_folders.append(tmp_folder)
					self.Folders.append(tmp_folder)

					if tmp_folder is not None and self.current_folder.subfolders is None:
						self.current_folder.subfolders = []  # List[PK2File.PK2Folder]

					self.current_folder.subfolders.append(tmp_folder)
			elif entry.type_ == 2:

				tmp_file: PK2File.PK2File = PK2File.PK2File()
				tmp_file.position = entry.position
				tmp_file.name = entry.name
				tmp_file.size = entry.size
				tmp_file.parentFolder = self.current_folder
				self.Files.append(tmp_file)

				self.current_folder.files.append(tmp_file)

		if entry_block.entries[19].nChain != 0:
			self.read(entry_block.entries[19].nChain)

		for folder in tmp_folders:

			self.current_folder = folder

			if folder.files is None:
				folder.files = List[PK2File.PK2File]
			elif folder.subfolders is None:
				folder.subfolders = List[PK2File.PK2Folder]

			self.read(folder.position)

	def buffer_to_struct(self, buffer: List[c_byte], return_struct_format: str) -> object:  # returnStruct: Type)-> object:
		return struct.unpack(return_struct_format, bytes(buffer))
