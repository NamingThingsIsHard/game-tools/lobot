from enum import Enum


class BotStatus(Enum):  # : c_byte
	Disconnected = 0
	Login = 1
	CharacterSelect = 2
	Loading = 3
	Idle = 4
	Botting = 5
