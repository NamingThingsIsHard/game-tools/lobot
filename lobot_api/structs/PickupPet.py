from lobot_api.Event import Event
from lobot_api.structs.Pet import Pet


class PickupPet(Pet):
	on_settings_changed = Event()
	on_name_changed = Event()

	def __init__(self, pet: Pet = None):
		super(PickupPet, self).__init__()
		if pet:
			self.set_from_pet(pet)
		self._grab_gold = True
		self._grab_equipment = True
		self._grab_others = True
		self._grab_all = False
		self._is_on = False

	def update_settings(self, new_settings: int):
		self.grab_gold = 0x01 & new_settings
		self.grab_equipment = 0x02 & new_settings
		self.grab_others = 0x04 & new_settings
		self.grab_all = 0x40 & new_settings
		self.is_on = 0x80 & new_settings

	@property
	def grab_gold(self):
		return self._grab_gold

	@grab_gold.setter
	def grab_gold(self, value):
		if self._grab_gold != value:
			self._grab_gold = value
			self.on_settings_changed.notify()

	@property
	def grab_equipment(self):
		return self._grab_equipment

	@grab_equipment.setter
	def grab_equipment(self, value):
		if self._grab_equipment != value:
			self._grab_equipment = value
			self.on_settings_changed.notify()

	@property
	def grab_others(self):
		return self._grab_others

	@grab_others.setter
	def grab_others(self, value):
		if self._grab_others != value:
			self._grab_others = value
			self.on_settings_changed.notify()

	@property
	def grab_all(self):
		return self._grab_all

	@grab_all.setter
	def grab_all(self, value):
		if self._grab_all != value:
			self._grab_all = value
			self.on_settings_changed.notify()

	@property
	def is_on(self):
		return self.is_on

	@is_on.setter
	def is_on(self, value):
		if self._is_on != value:
			self._is_on = value
			self.on_settings_changed.notify()

	@property
	def name(self):
		return self._name

	@name.setter
	def name(self, value):
		if self._name != value:
			self._name = value
			self.on_name_changed.notify(value)

	def set_from_pet(self, pet: Pet):
		"""Use to update static pet in Bot for updating  UI
		# Also this to reuse variables EXP and current_hp for Pet that was relocated by server from being stuck.
		# These# set by the petinfohandler after a normal respawn, not after being relocated.
		# This can be seen in the stats part of the bot window.
		"""

		self.custom_name = pet.custom_name
		self.owner_name = pet.owner_name
		self.owner_id = pet.owner_id
		self.ref_id = pet.ref_id
		self.unique_id = pet.unique_id
		self.position = pet.position
		self.current_hp = pet.current_hp
		self.max_hp = pet.max_hp
		self.is_alive = pet.is_alive
		self.is_knocked_down = pet.is_knocked_down
		self.target = pet.target
		self.is_invisible = pet.is_invisible
