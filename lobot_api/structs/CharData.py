from ctypes import *
from typing import List, Dict

from lobot_api.Event import Event
from lobot_api.ObjectStatusChangedArgs import ObjectStatusChangedArgs
from lobot_api.globals.PK2DataGlobal import PK2DataGlobal
from lobot_api.pk2.PK2Item import PK2ItemType, Country
from lobot_api.pk2.PK2Skill import PK2Skill, PK2SkillType
from lobot_api.scripting.Point import Point
from lobot_api.structs import EquipmentSlot
from lobot_api.structs.DebuffStatus import DebuffStatus
from lobot_api.structs.ICombatType import ICombatType
from lobot_api.structs.ISpawnObject import ISpawnObject
from lobot_api.structs.Item import Item
from lobot_api.structs.ObjectStatus import ObjectStatus


class AvatarInventory:
	def __init__(self):
		self.size = 0
		self.items: Dict[int: Item] = {}
		self.item_count = 0


class BlockedPlayer:
	def __init__(self):
		self.target_name: str = ''
		self.target_name_length = 0


class Hotkey:
	def __init__(self):
		self.slotSeq = 0
		self.slotType = 0  # (37 = COS Command, 70 = InventoryItem, 71 = EquipedItem, 73 = Skill, 74 = Action, 78 = EquipedAvatar)
		self.Data = 0

	def __repr__(self):
		return f"SlotSeq={self.slotSeq} SlotType={self.slotType} Data={self.Data}"


class Inventory:
	def __init__(self):
		self.size = 0
		self.item_count = 0
		self.equipment: Dict[int, Item] = {}
		self.items: Dict[int, Item] = {}
		self.avatar_inventory: AvatarInventory = AvatarInventory()

	def get_slot(self, slot: int):
		"""
		Search in equipment or items, depending on slot.
		:param slot:
		:return:
		"""
		if slot < EquipmentSlot.MAX_EQUIP_SLOTS:
			return self.equipment.get(slot, None)
		else:
			return self.items.get(slot, None)

	def __repr__(self):
		return f"Avatar({self.avatar_inventory})|{self.items}"

	def __str__(self):
		return self.__repr__()


class Mastery:
	def __init__(self):
		self.ID = 0
		self.level = 0

	def __repr__(self):
		return f"{self.ID}|{self.level}"

	def __str__(self):
		return self.__repr__()


class Objective:
	def __init__(self):
		self.ID = 0
		self.Status = 0  # (00 =done, 01 = incomplete)
		self.nameLength = 0
		self.name: str = ''
		self.Tasks: List[c_uint32] = []
		self.AchievementCount = 0
		self.task_count = 0


class Quest:
	def __init__(self):
		self.ref_id = 0
		self.AchievementCount = 0
		self.RequiresSharePt = 0
		self.type = 0
		self.Status = 0
		self.Objectives: List[Objective] = []
		self.objective_count = 0
		self.task_count = 0
		self.taskrefobj_id: List[c_uint32] = []  # List NPCs to deliver to,  # get reward)


class Skill:
	def __init__(self, ref_id, skill_enable=1, is_auto_attack: bool = False):
		self.ref_id = ref_id
		self.skill_enabled = skill_enable
		self.is_auto_attack = is_auto_attack

	@property
	def name(self) -> str: return self.pk2_skill.name

	@property
	def type(self) -> PK2SkillType: return self.pk2_skill.type

	@property
	def cast_time(self) -> int: return self.pk2_skill.cast_time

	@property
	def cooldown(self) -> int: return self.pk2_skill.cooldown

	@property
	def pk2_skill(self) -> PK2Skill: return PK2DataGlobal.skills[self.ref_id]

	@property
	def required_weapon(self) -> PK2ItemType: return self.pk2_skill.required_weapon

	@property
	def required_weapon_slot(self) -> EquipmentSlot: return self.pk2_skill.required_weapon.get_required_weapon_slot()

	def __repr__(self):
		return f"{self.ref_id}, {self.name}, {self.type}, Weapon: {self.required_weapon}, Enabled: {self.skill_enabled}"

	def __str__(self):
		return f"{self.ref_id}, {self.name}, {self.type}, Weapon: {self.required_weapon}, Enabled: {self.skill_enabled}"

	def __eq__(self, other):
		return other and self.ref_id == other.ref_id or self.name == other.name

	def __hash__(self):
		return hash(self.ref_id)


class Buff(Skill):
	def __init__(self, ref_id, skill_enable=1):
		super().__init__(ref_id, skill_enable, False)
		self.target_id = 0
		self.buff_duration = 0
		self.ref_skill_param = 0
		self.is_creator = 0
		self.unique_id = 0

	def __repr__(self):
		return self.pk2_skill.__repr__()

	def __str__(self):
		return self.pk2_skill.__repr__()

	def __eq__(self, other):
		return self.ref_id == other.ref_id


class CharData(ISpawnObject, ICombatType):

	def __init__(self):
		super(CharData, self).__init__()
		# events
		self.on_hp_changed = Event()
		self.on_mp_changed = Event()
		self.on_exp_percent_changed = Event()
		self.on_name_changed = Event()
		self.on_level_changed = Event()
		self.on_guild_name_changed = Event()
		self.on_sp_changed = Event()
		self.on_x_changed = Event()
		self.on_y_changed = Event()

		self.on_zerk_ready = Event()
		self.on_status_changed = Event()
		self.on_name_changed = Event()

		self.server_time = 0x00
		self.ref_id = 0x00
		self.country = Country.Chinese
		self.scale = 0x00
		self._level = 0x00  # Property for UI WPF purposes
		self.max_level = 0x00
		self._exp_offset = -1  # setExpOffset  => expOffset set  expOffset = value NotifyPropertyChanged("ExpPercent") } }# Notify to update UI EXP bar
		self.skill_exp_offset = 0x00
		self.remain_gold = 0x00
		self._sp = 0x00
		self.remain_stat_point = 0x00
		self.remain_hwan_count = 0  # if remain_hwan_count is HwanLevel)  # on_zerk_ready(EventArgs.Empty) NotifyPropertyChanged(: } }
		self.gathered_exp_point = 0x00
		self.max_hp = 0x00
		self._current_hp = 0x00  # set  current_hp = value NotifyPropertyChanged("current_hp_percent") } }# Notify to update UI HP bar
		self._currentMP = 0x00  # set  currentMP = value NotifyPropertyChanged("current_mp_percent") } }# Notify to update UI MP bar
		self.AutoInverstExp = 0x00  # (1 = Beginner Icon, 2 = Helpful, 3 = Beginner&Helpful)
		self.daily_pk = 0x00
		self.total_pk = 0x00
		self.PKPenaltyPoint = 0x00
		self.HwanLevel = 0x00
		self.free_pvp = 0x00
		# -> Check for != 0
		self.inventory: Inventory = Inventory()
		self.has_mask = 0x00  # -> Check for != 0 (MaskFlag?)
		self.mastery_flag = 0x00
		self.masteries: List[Mastery] = []
		self.skill_flag = 0x00
		self.skills: List[Skill] = []
		self.completed_quest_count = 0x00
		self.completed_quests: List[Quest] = []
		self.active_quest_count = 0x00
		self.active_quests: List[Quest] = []
		self.quest_achievement_count = 0x00  # (Repetition Amount = Bit and Completion Amount = Bit)
		self.quest_requires_auto_share_party = 0x00  # -> Check for != 0
		self.quest_type = 0x00  # (8 = , 24 = , 88 = )
		self.quest_status = 0x00  # (1 = Untouched, 7 = Started, 8 = Complete)
		self.quest_objective_count = 0
		self.unk05 = 0x00  # -> Check for != 0
		self.collection_book_count = 0x00  # -> Check for != 0
		self.unique_id = 0x00  # set  _unique_id = value
		self.has_destination = 0x00
		self.movement_type = 0x00  # (0 = Walking, 1 = Running)
		self.destination = Point()
		self.sky_click_flag = 0x00  # (1 = Sky-/ArrowKey-walking)
		self.angle = 0x00
		self.is_alive = 1  # (1 = Alive, 2 = Dead)
		self.debuff_flags: int = 0x00  # -> Check for != 0
		self.debuff_status: DebuffStatus = DebuffStatus.Normal  # -> Check for != 0
		self.motion_state = 0x00  # (0 = None, 2 = Walking, 3 = Running, 4 = Sitting)
		self.walk_speed: float = 22
		self.run_speed: float = 55
		self.hwan_speed = 0x00
		self.active_buff_count = 0x00
		self.active_buffs: Dict[int, Buff] = {}
		self._name: str = ''  # Property for UI WPF purposes
		self.job_name_length = 0x00
		self.job_name: str = ''
		self.job_type = 0x00
		# (0 = None, 1 = Trader, 2 = Thief, 3 = Hunter)
		self.job_level = 0x00
		self.job_exp = 0x00
		self.job_contribution = 0x00
		self.job_reward = 0x00
		self._guild_name: str = ''  # set  guildName = value# Property for UI WPF purposes
		self.guild_grant_name: str = ''
		self.guild_id = 0x00
		self.guild_last_crest = 0x00
		self.union_id = 0x00
		self.union_last_crest = 0x00
		self.guild_is_friendly = 0x00  # 0 = Hostile, 1 = Friendly
		self.guild_siege_authority = 0x00
		self.scroll_mode = 0x00  # 0 = None, 1 = Return Scroll, 2 = Bandit Return Scroll
		self.interaction_mode = 0x00  # 4 if stall is open //0 = None 2 = P2P, 4 = P2N_TALK, 6 = OPNMKT_DEAL
		self.PVP_state = 0x00  # -> Check for != 0	(According to Spawn structure => MurderFlag?)
		self.transport_flag = 0x00  # -> Check for != 0	(According to Spawn structure => RideFlag or AttackFlag?)
		self.transport_id = 0x00
		self.in_combat = 0x00  # -> Check for != 0	(According to Spawn structure => EquipmentCountdown?)
		self.PVP_flag = 0x00  # Flag(255 = Disable, 34 = Enable)
		self.guide_flag = 0x00
		self.account_id = 0x00  # (=> gameaccount_id)
		self.GM_flag = 0x00
		self.activation_flag = 0x00
		self.hot_key_count = 0x00
		self.hot_keys: List[Hotkey] = []
		self.hp_slot = 0x00  # (FValue  10 + Slot)
		self.hp_value = 0x00  # (Enabled = 128 + Value)
		self.mp_slot = 0x00  # (FValue  10 + Slot)
		self.mp_value = 0x00  # (Enabled = 128 + Value)
		self.universal_slot = 0x00  # (Enabled = 128 + Value)
		self.universal_value = 0x00  # (Enabled = 128,0 = Disabled)
		self.potion_delay = 0x00  # (Enabled = 128 + Value)
		self.blocked_play_count = 0x00
		blocked_players: List[BlockedPlayer] = []
		# TargetNameLength
		self.unk13 = 0x00
		self.unk14 = 0x00
		self.is_knocked_down = False  # TODO check if any of the unknown bytes changes when char_data is loaded for knockeddown player
		self.is_using_zerk = False

		self._status = ObjectStatus.Unknown

	@property
	def current_hp(self):
		return self._current_hp

	@current_hp.setter
	def current_hp(self, value):
		if self._current_hp != value:
			self._current_hp = value
			self.on_hp_changed.notify([])

	@property
	def current_mp(self):
		return self._currentMP

	@current_mp.setter
	def current_mp(self, value):
		if self._currentMP != value:
			self._currentMP = value
			self.on_mp_changed.notify([])

	def exp_percent(self) -> int:
		lvlEXP = PK2DataGlobal.level_data.get(self._level, None)
		result = self._exp_offset * 100 / lvlEXP if lvlEXP else 100  # exp rounded up to second decimal place
		return result

	@property
	def exp_offset(self):
		return self._exp_offset

	@exp_offset.setter
	def exp_offset(self, value):
		if self._exp_offset != value:
			self._exp_offset = value
			if PK2DataGlobal.level_data is not None:
				self.on_exp_percent_changed.notify(self.exp_percent())

	@property
	def name(self):
		return self._name

	@name.setter
	def name(self, value):
		if value != self._name:
			self.on_name_changed.notify(value)
			self._name = value

	@property
	def level(self):
		return self._level

	@level.setter
	def level(self, value):
		if value != self._level:
			self.on_level_changed.notify(value)
			self._level = value

	@property
	def guild_name(self):
		return self._guild_name

	@guild_name.setter
	def guild_name(self, value):
		if value != self._guild_name:
			self.on_guild_name_changed.notify(value)
			self._guild_name = value

	@property
	def sp(self):
		return self._sp

	@sp.setter
	def sp(self, value):
		if value != self._sp:
			self.on_sp_changed.notify(value)
			self._sp = value

	@property
	def x_position(self):
		return self.position.x

	@property
	def y_position(self):
		return self.position.y

	@property
	def status(self) -> ObjectStatus:
		return self._status

	@status.setter
	def status(self, value):
		prev: ObjectStatus = self._status
		self._status = value
		if prev != value:
			e: ObjectStatusChangedArgs = ObjectStatusChangedArgs(prev, value)
			# (0 = None, 1 = Zerk, 2 = Untouchable, 3 = GMInvincible, 4 = GMInvisible, 5= ??, 6 = Stealth, 7 = Invisible)
			self.on_status_changed.notify(e)

	@property
	def current_speed(self) -> float:
		return self.walk_speed if self.movement_type == 0x00 else self.run_speed  # For calculating time to travel a distance with current speed

	@property
	def is_in_cave(self) -> bool:
		return self.position.y_sector & 0x80
