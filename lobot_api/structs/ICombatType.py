from abc import ABC

from lobot_api.scripting.Point import Point


class ICombatType(ABC):
	def __init__(self):
		self._current_hp = 0
		self._max_hp = 0
		self._is_alive = True
		self._is_knocked_down = False
		self._target = 0
		self._position: Point = Point(0, 0, 0)

	@property
	def current_hp(self) -> int:
		return self._current_hp  #

	@current_hp.setter
	def current_hp(self, value):
		self._current_hp = value

	@property
	def position(self) -> Point:
		return self._position

	@position.setter
	def position(self, value):
		self._position = value

	@property
	def is_knocked_down(self) -> bool:
		return self._is_knocked_down

	@is_knocked_down.setter
	def is_knocked_down(self, value):
		self._is_knocked_down = value

	@property
	def target(self) -> int:
		return self._target

	@target.setter
	def target(self, value):
		self._target = value

	@property
	def is_alive(self) -> bool:
		return self._is_alive

	@is_alive.setter
	def is_alive(self, value):
		self._is_alive = value
