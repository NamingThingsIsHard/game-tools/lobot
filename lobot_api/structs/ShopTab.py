from ctypes import c_uint32
from typing import Dict


class ShopTab:
	"""A concise version of the class.
	# Used as a value object to map item slot numbers to item_ids."""

	def __init__(self):
		Items: Dict[int, c_uint32]
