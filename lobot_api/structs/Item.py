from enum import Enum
from typing import List, Optional

from lobot_api.globals.PK2DataGlobal import PK2DataGlobal
from lobot_api.pk2.PK2Item import Country, Gender, PK2Item
from lobot_api.pk2.PK2MagParam import PK2MagParam
from lobot_api.structs.WhiteAttributes import AttributeType, WhiteAttributes
import math


class RentItem:
	def __init__(self):
		self.period_begin_time = 0
		self.period_end_time = 0
		self.can_delete = 0
		self.meter_rate_time = 0
		self.can_recharge = 0
		self.packing_time = 0


class Option:
	def __init__(self):
		self.slot = 0
		self.ID = 0  # c_uint32
		self.NParam1 = 0  # (=> Reference to Socket)
		self.OptValue = 0  # c_uint32


class MagParam:
	""" Blue attributes granted by jade stones.
	"""

	@property
	def max_value(self):
		return self.pk2_mag_param.tiers[-1] if self.pk2_mag_param.tiers else 1

	# FIXME low level percentages have too few tiers (e.g. str. 1-3, but client shows 2 str as 50% instead of 66%)
	@property
	def percentage(self):
		return int((self.value * 100) / self.max_value) if self.pk2_mag_param else 0

	def __repr__(self):
		return f"{self.ref_id}|{self.pk2_mag_param.name}|{self.value}({self.percentage}%)"

	def __str__(self):
		return f"{self.ref_id}|{self.pk2_mag_param.__str__()}"

	@property
	def pk2_mag_param(self) -> PK2MagParam:
		return PK2DataGlobal.mag_params.get(self.ref_id, None)

	def __init__(self, ref_id: int, value: int):
		self.ref_id = ref_id
		self.value = value


# TODO use this in alchemy features
class MagParamAlchemyValues:
	def __init__(self):
		self.Str = 0
		self.Int = 0
		self.Durability = 0
		self.HP = 0
		self.MP = 0
		self.HitRatio = 0
		self.BlockRatio = 0
		self.CriticalBlock = 0
		self.ParryRatio = 0
		self.Burn = 0
		self.Shock = 0
		self.Freeze = 0
		self.Poison = 0
		self.Zombie = 0
		self.MagParamValue = 0

	# def change_value(self, mag_param_type: MagParam, mag_param_value):
	# 	if mag_param_type == MagParam.Str:
	# 		self.Str = mag_param_value
	# 	elif mag_param_type == MagParam.Int:
	# 		self.Int = mag_param_value
	# 	elif mag_param_type == MagParam.HP:
	# 		self.HP = mag_param_value
	# 	elif mag_param_type == MagParam.MP:
	# 		self.MP = mag_param_value
	# 	elif mag_param_type == MagParam.Durability:
	# 		self.Durability = mag_param_value
	# 	elif mag_param_type == MagParam.AttackRatio:
	# 		self.HitRatio = mag_param_value
	# 	elif mag_param_type == MagParam.BlockRatio:
	# 		self.BlockRatio = mag_param_value
	# 	elif mag_param_type == MagParam.CriticalBlock:
	# 		self.CriticalBlock = mag_param_value
	# 	elif mag_param_type == MagParam.ParryRatio:
	# 		self.ParryRatio = mag_param_value
	# 	elif mag_param_type == MagParam.Burn:
	# 		self.Burn = mag_param_value
	# 	elif mag_param_type == MagParam.ElectricShock:
	# 		self.Shock = mag_param_value
	# 	elif mag_param_type == MagParam.Freeze:
	# 		self.Freeze = mag_param_value
	# 	elif mag_param_type == MagParam.Poison:
	# 		self.Poison = mag_param_value
	# 	elif mag_param_type == MagParam.Zombie:
	# 		self.Zombie = mag_param_value
	# 	else:
	# 		pass


class Item:
	def __init__(self, ref_id=0, stack_count=1):
		self.ref_id = ref_id
		self.stack_count = stack_count
		self.rent_type = 0
		self.rent = RentItem()
		self.slot = 0
		self.attribute_assimilation_probability = 0

	@classmethod
	def from_pk2item(cls, pk2item):
		return cls(pk2item.ID)

	@classmethod
	def from_item(cls, item):
		instance = cls(item)
		instance.rent_type = item.rent_type
		instance.slot = item.slot
		instance.ref_id = item.ref_id
		instance.stack_count = item.stack_count
		instance.attribute_assimilation_probability = item.attribute_assimilation_probability
		return instance

	def __repr__(self):
		return f"{self.ref_id}|Slot({self.slot})|{self.pk2_item.__repr__()}"

	def __str__(self):
		return f"{self.ref_id}|Slot({self.slot})|{self.pk2_item.__str__()}"

	@property
	def pk2_item(self) -> PK2Item: return PK2DataGlobal.items[self.ref_id]

	@pk2_item.setter
	def pk2_item(self, val):
		item = PK2DataGlobal.items.get(val.ref_id, None)
		if item:
			self.ref_id = val.ref_id

	@property
	def long_id(self) -> str: return self.pk2_item.long_id

	@property
	def level(self) -> int: return self.pk2_item.level

	@property
	def max_stacks(self) -> int: return self.pk2_item.max_stacks

	@property
	def degree(self) -> int: return self.pk2_item.degree

	@property
	def gender(self) -> Gender: return self.pk2_item.gender

	@property
	def country(self) -> Country: return self.pk2_item.country

	@property
	def is_sox(self) -> bool: return self.pk2_item.is_SOX

	@property
	def is_storable(self) -> bool: return self.pk2_item.is_storable

	@property
	def is_mall_item(self) -> bool: return self.pk2_item.is_mall_item

	@property
	def type(self):
		"""The Type property displayed in the GUI inventory list."""
		return self.pk2_item.type


# # The Name property displayed in the GUI inventory list.
# ItemName   return pk2_item.name
# # The Quantity property displayed in the GUI inventory list.
# Quantity   return str(stack_count)

# @property
# def SellOrStoreString(self) -> str: return pk2_item.should_sell() ? "Sell" : pk2_item.should_store() ? "Store" : "Keep"
#
# 		def __str__(self):
#            str(pk2_item)
# 		public virtual object Clone() => Item(this)


class Equipment(Item):
	def __init__(self, i: Item = None):
		ref_id = 0
		stack_count = 1

		if Item:
			ref_id = i.ref_id
			stack_count = i.stack_count
		super(Equipment, self).__init__(ref_id, stack_count)
		"""The number of plus from elixirs """
		self.opt_level = 0
		self.item_option: Option = Option()
		"""The variance of white attributes on item"""
		self.attributes: Optional[WhiteAttributes] = None
		# set to 5 so bought item does not count as broken. A durability update on the item reveals true value later
		self.durability = 5
		self.mag_param_num = 0
		self.blues: List[MagParam] = []
		self.rent_type = i.rent_type if i else 0
		self.rent = i.rent if i else RentItem()
		self.slot = i.slot if i else 0
		self.ref_id = i.ref_id if i else 0
		self.stack_count = i.stack_count if i else 1
		self.attribute_assimilation_probability = i.attribute_assimilation_probability if i else 0

	@property
	def alchemy_settings_string(self) -> str:
		"""String representation for alchemy tab in GUI"""
		return f'(+{self.opt_level}) {self.pk2_item.name}'

	@property
	def effective_level(self):
		return self.level + self.opt_level

	def get_attribute_type(self) -> AttributeType:
		if self.pk2_item.is_weapon():
			return AttributeType.Weapon
		elif self.pk2_item.is_shield():
			return AttributeType.Shield
		elif self.pk2_item.is_accessory():
			return AttributeType.Accessory
		else:
			return AttributeType.Equipment


class StallItemStatus(Enum):
	InStall = 0x00
	Reserved = 0x01


class StallItem:

	cardinal_names = ['', 'K', ' Million', ' Billion', ' Trillion']

	@staticmethod
	def millify(n):
		n = float(n)
		millidx = max(
			0,
			min(
				len(StallItem.cardinal_names) - 1,
				int(math.floor(0 if n == 0 else math.log10(abs(n))/3)))
		)

		return '{:.0f}{}'.format(n / 10 ** (3 * millidx), StallItem.cardinal_names[millidx])

	def __init__(
		self,
		item: Item,
		slot: int,
		source_slot: int,
		stack_count: int,
		price: int,
	):
		self.item = item
		self.slot = slot
		self.source_slot = source_slot
		self.stack_count = stack_count
		self.price = price

	def __str__(self):
		return f"Slot {self.slot} -- {self.millify(self.price)} for {self.stack_count}x {self.item.pk2_item.name}(inventory slot {self.source_slot})"

	def __repr__(self):
		return self.__str__()

	def __lt__(self, other):
		if isinstance(other, StallItem):
			return self.slot < other.slot
		else:
			return False

	def __gt__(self, other):
		if isinstance(other, StallItem):
			return self.slot > other.slot
		else:
			return False

# class StallItem:
#
# 	def __init__(self, i: Item, slot=0, price=0):
# 		self.inventory_item = i
# 		self._slot = slot
# 		self._stack_count = self.inventory_item.stack_count
# 		self._price = price
# 		self._status = StallItemStatus.InStall
#
# 	@property
# 	def slot(self): return self._slot
#
# 	@property
# 	def stack_count(self): return self._stack_count
#
# 	@property
# 	def price(self): return self._price  # set  _price = value NotifyPropertyChanged()
#
# 	@property
# 	def status(self) -> StallItemStatus: return self._status  # set  _status = value NotifyPropertyChanged()
#
# 	@property
# 	def name(self) -> str: return self.inventory_item.pk2_item.name


# def __str__(self):
# 	return InventoryItem is Equipment ? f"Name}(+InventoryItem.opt_level})" : Name

# def __eq__(self, other):object obj)
# 	if obj is StallItem:
# 		return Equals(obj.InventoryItem)
# 	elif obj is Item:
# 		return Equalsobj
# 	elif c_byte.try_parse(obj asstr, out c_byte result):# slot number
# 		return InventoryItem.slot is result
# 	else:
# 		return False

# public bool Equals(obj: Item)
#
# 	return InventoryItem.slot is obj.slot

# public bool Equals(other: StallItem)
# 	return other is not None and
# 		   _slot is other._slot and
# 		   EqualityComparer.Default == InventoryItem, other.InventoryItem


class PetStatus(Enum):
	Unsummoned = 1
	Summoned = 2
	Alive = 3
	Dead = 4


class PetScroll(Item):
	def __init__(self, i: Item = None):
		ref_id = 0
		stack_count = 1

		if i:
			ref_id = i.ref_id
			stack_count = i.stack_count
		super(PetScroll, self).__init__(ref_id, stack_count)

		self.rent_type = i.rent_type if i else 0
		self.rent = i.rent if i else 0
		self.slot = i.slot if i else 0
		self.ref_id = i.ref_id if i else 0
		self.stack_count = i.stack_count if i else 0
		self.attribute_assimilation_probability = i.attribute_assimilation_probability if i else 0
		self.status: PetStatus = PetStatus(1)  # (1 = Unsumonned, 2 = Summoned, 3 = Alive, 4 = Dead)
		self.unique_id = 0
		self.name_length = 0
		self.name: str = ''
		self.unk02 = 0

	def __repr__(self):
		return f"Slot({self.slot}),{self.long_id}"

	def __str__(self):
		return f"Slot({self.slot}),{self.long_id}"


# public override object Clone() => PetScroll(this)


class AbilityPet(PetScroll):
	def __init__(self, i: Item):
		super(AbilityPet, self).__init__(i)
		self.seconds_to_rent_end_time = 0  # c_ulong

	def __repr__(self):
		return f"Slot({self.slot}),({self.name}){self.long_id}"

	def __str__(self):
		return f"Slot({self.slot}),({self.name}){self.long_id}"


# public override object Clone() => AbilityPet(this)


class MagicCube(Item):
	def __init__(self, i: Item):
		ref_id = 0
		stack_count = 1

		if i:
			ref_id = i.ref_id
			stack_count = i.stack_count
		super(MagicCube, self).__init__(ref_id, stack_count)

		self.stored_item_count = 0
		self.rent_type = i.rent_type
		self.rent = i.rent
		self.slot = i.slot
		self.ref_id = i.ref_id
		self.stack_count = i.stack_count
		self.attribute_assimilation_probability = i.attribute_assimilation_probability
		

class ItemExchangeCoupon(Item):
	def __init__(self, i: Item):
		super(ItemExchangeCoupon, self).__init__()
		self.mag_param_values: List[int] = []
# public override object Clone() => ItemExchangeCoupon(this)
