class PK2ShopItemRef:

	def __init__(self, long_id: str, tab_name: str, price: str, slot: int):
		if not tab_name or not long_id:
			raise ValueError(tab_name)
		elif not long_id:
			raise ValueError(long_id)

		self.long_id = long_id
		self.tab_name = tab_name
		self.price = int(price)
		self.slot = slot
		self.item_id = 0

	def __eq__(self, other):  # : PK2ShopItemRef = None):
		if other:
			i = self.item_id == other.item_id
			long = self.long_id == other.long_id
			t = self.tab_name == other.tab_name
			p = self.price == other.price
			s = self.slot == other.slot
			return i and long and t and p and s
		elif other is not None:
			return isinstance(other, PK2ShopItemRef) and (self.item_id, self.long_id) == (other.item_id, other.long_id)
		return False

	def __repr__(self):
		return f"{self.tab_name},{self.long_id},{self.item_id},{self.price}, {self.slot}"

	def __str__(self):
		return f"{self.tab_name},{self.long_id},{self.item_id},{self.price}, {self.slot}"
