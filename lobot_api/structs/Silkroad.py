from ctypes import c_ushort
from typing import NamedTuple


class Silkroad:
	pass


class Server(NamedTuple):
	id: c_ushort = 0
	name: str = ''
	curUser: c_ushort = 0
	maxUser: c_ushort = 0
	isOnline: bool = False


class ListedCharacter(NamedTuple):
	characterSlotId = ''
	name: str = ''
