from enum import Enum, auto
from typing import Dict

from lobot_api.globals.PK2DataGlobal import PK2DataGlobal
from lobot_api.pk2.PK2NPC import PK2NPCType
from lobot_api.structs.CharData import Buff
from lobot_api.structs.ICombatType import ICombatType
from lobot_api.structs.IIgnorable import IIgnorable
from lobot_api.structs.ISpawnObject import ISpawnObject

"""Attribute describing a monster's strength level."""


class MonsterType(Enum):
	Normal = 0x00
	Champion = 0x01
	Unique = 0x03
	Giant = 0x04
	Strong = 0x05  # Titan
	Elite1 = 0x06
	Elite2 = 0x07
	Party = 0x10
	PartyChampion = 0x11
	PartyUnique = 0x13
	PartyGiant = 0x14
	PartyStrong = 0x15  # = 0x00 TBD
	Elite = 0x16  # = 0x00 TBD
	Event = auto()  # = 0x00 TBD
	Quest = auto()  # = 0x00 TBD

	def get_hp_multiplier(self) -> int:
		# switch (type)
		if self == MonsterType.Normal:
			pass
		elif self == MonsterType.Champion:
			return 2
		elif self == MonsterType.Unique:
			pass
		elif self == MonsterType.Giant:
			return 20
		elif self == MonsterType.Strong:
			pass
		elif self == MonsterType.Elite1:
			pass
		elif self == MonsterType.Elite2:
			pass
		elif self == MonsterType.Party:
			return 10
		elif self == MonsterType.PartyChampion:
			return 20
		elif self == MonsterType.PartyGiant:
			return 200
		elif self == MonsterType.PartyStrong:
			pass
		elif self == MonsterType.Elite:
			pass
		elif self == MonsterType.Event:
			pass
		elif self == MonsterType.Quest:
			pass
		else:
			pass

		return 1


class Monster(ISpawnObject, ICombatType, IIgnorable):
	def __init__(self):
		ISpawnObject.__init__(self)
		ICombatType.__init__(self)
		IIgnorable.__init__(self)
		self.ref_id = 0  # ID for game class
		self.unique_id = 0  # ID for instance of class
		self.type = MonsterType.Normal
		self._max_hp: int = 0
		self._current_hp: int = 0
		self._is_knocked_down: bool = False
		self._is_alive: bool = True
		self.is_invisible: bool = False
		self.buffs: Dict[int, Buff] = {}

	def is_event_monster(self):
		result = False
		if self.ref_id in PK2DataGlobal.npcs:
			npc = PK2DataGlobal.npcs[self.ref_id]
			result = npc.type in [PK2NPCType.MonsterEvent, PK2NPCType.MonsterEventStrong]
		return result

	@property
	def name(self):
		if self.ref_id in PK2DataGlobal.npcs:
			type_suffix = f"({self.type.name})" if self.type != MonsterType.Normal else ""
			return f"{PK2DataGlobal.npcs[self.ref_id].name}{type_suffix}"
		else:
			return str(self.ref_id)

	@property
	def long_id(self) -> str:
		if self.ref_id in PK2DataGlobal.npcs:
			return PK2DataGlobal.npcs[self.ref_id].long_id
		else:
			return str(self.ref_id)

	@property
	def level(self) -> int:
		if self.ref_id in PK2DataGlobal.npcs:
			return PK2DataGlobal.npcs[self.ref_id].level
		else:
			return 0

	@property
	def max_hp(self) -> int:
		return self._max_hp * self.type.get_hp_multiplier()

	@max_hp.setter
	def max_hp(self, value):
		self._max_hp = value
