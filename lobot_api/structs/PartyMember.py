from lobot_api.pk2.PK2Item import Country
from lobot_api.scripting.Point import Point


class PartyMember:
	def __init__(self, member_id=0):
		self.avatar = 0
		self.member_id = member_id
		self.level = 0
		self.hp_mp = 0
		self.mastery1 = 0
		self.mastery2 = 0
		self.name: str = ''
		self.guild_name: str = ''
		self.Country = Country.Chinese
		self.x_sector = 0
		self.y_sector = 0
		self.point = Point()
		self.angle = 0

	# return str.Format("({0,-3}){1,-13}-{2,-13}", Level, Name, GuildName)
	def __str__(self):
		return f"{self.name}({self.level}){self.guild_name}"
