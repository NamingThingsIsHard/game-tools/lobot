from ctypes import c_uint32


class ItemMallInfo:
	def __init__(self, iPCoins: c_uint32, silk: c_uint32, soulTickets: c_uint32):
		self.IPCoins: c_uint32 = iPCoins
		self.Silk: c_uint32 = silk
		self.SoulTickets: c_uint32 = soulTickets
