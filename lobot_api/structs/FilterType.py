from enum import Enum


class FilterType(Enum):  # : c_byte
	"""Type used to save filter options"""
	Pick = 8
	Store = 4
	Sell = 2
	Drop = 1
