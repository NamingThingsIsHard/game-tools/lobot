from typing import NamedTuple


class ShopItemEntry(NamedTuple):

	# shopnpc_id + "," + tabNumber + "," + slotNumber++ + "," + item.item_id + "," + item.Price
	item_id: int = 0
	tab_number: int = 0
	slot_number: int = 0
	price: int = 0

	@property
	def max_stacks(self):
		from lobot_api.globals.PK2DataGlobal import PK2DataGlobal  # import here to prevent circular import error
		return PK2DataGlobal.items[self.item_id].max_stacks

	def __str__(self):
		return f"{self.item_id},{self.tab_number},{self.slot_number},{self.price}"
