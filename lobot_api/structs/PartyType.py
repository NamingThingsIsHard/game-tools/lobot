from ctypes import c_byte
from enum import Enum


class PartyType(Enum):  # : c_byte
	# result of 3 bytes -> bool canInvite - bool itemshare - bool expshare
	ClosedFreeFree = 0x00
	ClosedFreeShare = 0x01
	ClosedShareFree = 0x02
	ClosedShareShare = 0x03
	LTP = 0x04  # OpenFreeFree
	OpenFreeShare = 0x05
	OpenShareFree = 0x06
	OpenShareShare = 0x07

	def ToFriendlyString(self):
		# switch (type)
		if self == PartyType.ClosedFreeFree:
			return "0 IF - EF"
		# return "Closed Item FFA, Exp FFA"
		elif self == PartyType.ClosedFreeShare:
			return "0 IF - ES"
		# return "Closed Item FFA, Exp Share"
		elif self == PartyType.ClosedShareFree:
			return "0 IS - EF"
		# return "Closed Item Share, Exp FFA"
		elif self == PartyType.ClosedShareShare:
			return "0 IS - ES"
		# return "Closed Item Share, Exp Share"
		elif self == PartyType.LTP:
			return "LTP"
		# return "Open Item FFA, Exp FFA"
		elif self == PartyType.OpenFreeShare:
			return "1 IF - ES"
		# return "Open Item FFA, Exp Share"
		elif self == PartyType.OpenShareFree:
			return "1 IS - EF"
		# return "Open Item Share, Exp Free"
		elif self == PartyType.OpenShareShare:
			return "1 IS - ES"
		# return "Open Item Share, Exp Share"
		else:
			return "Time To Partynot "

	def GetMaxMembers(self) -> c_byte:
		return 4 if (self.value % 2 == 0) else 8  # expshare == 0

	# def IsSameTypeOpenOrClosed(self, type : PartyType) -> bool:
	#    return type == PartySettings.type or abs((c_byte)PartySettings.type - (c_byte)type) == 0

	""" Return the type without the most significant bit that says whether members can invite.
	This is used to choose the party type in the GUI.
	E.g. OpenShareShare = 0x07 (bits 111) becomes ClosedShareShare = 0x03 (bits 11)
	"""

	@property
	def GetTypeWithoutMemberCanInvite(self):  # -> PartyType:
		return PartyType(self & 3)
