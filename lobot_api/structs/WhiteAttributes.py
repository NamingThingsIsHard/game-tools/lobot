from enum import Enum, auto
from typing import List


class AttributeType(Enum):
	Weapon = auto()
	Equipment = auto()
	Shield = auto()
	Accessory = auto()


class WhiteAttributes:
	"""Created by Daxter (2012)
	Credits to Stratti for his awesome code example
	"""

	def update_value(self):
		variance = 0
		# switch self.type)
		if self.type is AttributeType.Weapon:
			variance |= self.durability
			variance <<= 5
			variance |= self.phy_reinforce
			variance <<= 5
			variance |= self.mag_reinforce
			variance <<= 5
			variance |= self.hit_ratio
			variance <<= 5
			variance |= self.phy_attack
			variance <<= 5
			variance |= self.mag_attack
			variance <<= 5
			variance |= self.critical_ratio
		elif self.type is AttributeType.Equipment:
			variance |= self.durability
			variance <<= 5
			variance |= self.phy_reinforce
			variance <<= 5
			variance |= self.mag_reinforce
			variance <<= 5
			variance |= self.phy_defense
			variance <<= 5
			variance |= self.mag_defense
			variance <<= 5
			variance |= self.parry_ratio
		elif self.type is AttributeType.Shield:
			variance |= self.durability
			variance <<= 5
			variance |= self.phy_reinforce
			variance <<= 5
			variance |= self.mag_reinforce
			variance <<= 5
			variance |= self.block_ratio
			variance <<= 5
			variance |= self.phy_defense
			variance <<= 5
			variance |= self.mag_defense
		elif self.type is AttributeType.Accessory:
			variance |= self.phy_absorb
			variance <<= 5
			variance |= self.mag_absorb
		self.value = variance

	def update_stats(self):
		variance = self.value
		counter = 0

		# switch self.type)
		if self.type == AttributeType.Weapon:  # 7 Slots
			while variance > 0:
				stat = variance & 0x1F

				# switch (counter)
				if counter == 0:  # Durability
					self._durability = stat
				elif counter == 1:  # Physical Reinforce
					self._phy_reinforce = stat
				elif counter == 2:  # Magical Reinforce
					self._mag_reinforce = stat
				elif counter == 3:  # Hit Ratio (Attack Ratio)
					self._hit_ratio = stat
				elif counter == 4:  # Physical Attack
					self._phy_attack = stat
				elif counter == 5:  # Magical Attack
					self._mag_attack = stat
				elif counter == 6:  # Critical Ratio
					self._critical_ratio = stat

				# left shift by 5
				variance >>= 5
				counter += 1
		elif self.type == AttributeType.Equipment:  # 6 Slots
			while variance > 0:
				stat = variance & 0x1F
				# switch (counter)
				if counter == 0:  # Durability
					self._durability = stat
				elif counter == 1:  # Physical Reinforce
					self._phy_reinforce = stat
				elif counter == 2:  # Magical Reinforce
					self._mag_reinforce = stat
				elif counter == 3:  # Physical Defense
					self._phy_defense = stat
				elif counter == 4:  # Magical Defense
					self._mag_defense = stat
				elif counter == 5:  # Evasion Rate(Parry Rate)
					self._parry_ratio = stat
				# left shift by 5
				variance >>= 5
				counter += 1

		elif self.type == AttributeType.Shield:  # 6 Slots
			while variance > 0:
				stat = variance & 0x1F
				# switch (counter)

				if counter == 0:  # Durability
					self._durability = stat
				elif counter == 1:  # Physical Reinforce
					self._phy_reinforce = stat
				elif counter == 2:  # Magical Reinforce
					self._mag_reinforce = stat
				elif counter == 3:  # Block Ratio
					self._block_ratio = stat
				elif counter == 4:  # Physical Defense
					self._phy_defense = stat
				elif counter == 5:  # Magical Defense
					self._mag_defense = stat
				# left shift by 5
				variance >>= 5
				counter += 1

		if self.type == AttributeType.Accessory:  # 2 Slots
			while variance > 0:
				stat = variance & 0x1F
				# switch (counter)
				if counter == 0:  # Durability
					self._phy_absorb = stat
				elif counter == 1:  # Physical Reinforce
					self._mag_absorb = stat

				# left shift by 5
				variance >>= 5
				counter += 1

	def __init__(self, type_value: AttributeType, input=0, stats: List[int] = None):
		if not stats:
			stats = []
		self._type = type_value
		self._value = input

		self._durability = 0
		self._phy_reinforce = 0
		self._mag_reinforce = 0
		self._hit_ratio = 0
		self._phy_attack = 0
		self._mag_attack = 0
		self._critical_ratio = 0
		self._block_ratio = 0

		self._phy_defense = 0
		self._mag_defense = 0
		self._parry_ratio = 0

		self._phy_absorb = 0
		self._mag_absorb = 0

		if stats:
			if self._type == AttributeType.Weapon:
				self._durability = stats[0]
				self._phy_reinforce = stats[1]
				self._mag_reinforce = stats[2]
				self._hit_ratio = stats[3]
				self._phy_attack = stats[4]
				self._mag_attack = stats[5]
				self._critical_ratio = stats[6]
			elif self._type == AttributeType.Equipment:
				self._durability = stats[0]
				self._phy_reinforce = stats[1]
				self._mag_reinforce = stats[2]
				self._phy_defense = stats[3]
				self._mag_defense = stats[4]
				self._parry_ratio = stats[5]
			elif self._type == AttributeType.Shield:
				self._durability = stats[0]
				self._phy_reinforce = stats[1]
				self._mag_reinforce = stats[2]
				self._block_ratio = stats[3]
				self._phy_defense = stats[4]
				self._mag_defense = stats[5]
			elif self._type == AttributeType.Accessory:
				self._phy_absorb = stats[0]
				self._mag_absorb = stats[1]
		self.update_stats()

	def __repr__(self):
		if self.type == AttributeType.Weapon:
			return f"Dura{self.durability_percentage}, PhySpec{self.phy_reinforce_percentage}, MagSpec{self.mag_reinforce_percentage}, Hit{self.hit_ratio_percentage}, PhyAtk{self.phy_attack_percentage}, MagAtk{self.mag_attack_percentage}, Crit{self.critical_ratio_percentage}"
		elif self.type == AttributeType.Shield:
			return f"Dura{self.durability_percentage}, PhySpec{self.phy_reinforce_percentage}, MagSpec{self.mag_reinforce_percentage}, Block{self.block_ratio_percentage}, PhyDef{self.phy_defense_percentage}, MagDef{self.mag_defense_percentage}"
		elif self.type == AttributeType.Equipment:
			return f"Dura{self.durability_percentage}, PhySpec{self.phy_reinforce_percentage}, MagSpec{self.mag_reinforce_percentage}, PhyDef{self.phy_defense_percentage}, MagDef{self.mag_defense_percentage}, ParryRate{self.parry_ratio_percentage}"
		elif self.type == AttributeType.Accessory:
			return f"PhyAbsorb{self.phy_absorb_percentage}, MagAbsorb{self.mag_absorb_percentage}"
		else:
			return ""

	def __str__(self):
		if self.type == AttributeType.Weapon:
			return f"Dura{self.durability_percentage}, PhySpec{self.phy_reinforce_percentage}, MagSpec{self.mag_reinforce_percentage}, Hit{self.parry_ratio_percentage}, PhyAtk{self.phy_attack_percentage}, MagAtk{self.mag_attack_percentage}, Crit{self.critical_ratio_percentage}"
		elif self.type == AttributeType.Shield:
			return f"Dura{self.durability_percentage}, PhySpec{self.phy_reinforce_percentage}, MagSpec{self.mag_reinforce_percentage}, Block{self.block_ratio_percentage}, PhyDef{self.phy_defense_percentage}, MagDef{self.mag_defense_percentage}"
		elif self.type == AttributeType.Equipment:
			return f"Dura{self.durability_percentage}, PhySpec{self.phy_reinforce_percentage}, MagSpec{self.mag_reinforce_percentage}, PhyDef{self.phy_defense_percentage}, MagDef{self.mag_defense_percentage}, ParryRate{self.parry_ratio_percentage}"
		elif self.type == AttributeType.Accessory:
			return f"PhyAbsorb{self.phy_absorb_percentage}, MagAbsorb{self.mag_absorb_percentage}"
		else:
			return ""

	@property
	def value(self) -> int:
		return self._value

	@value.setter
	def value(self, value):
		self._value = value
		self.update_stats()

	@property
	def type(self) -> AttributeType:
		return self._type

	@type.setter
	def type(self, value):
		self._type = value

	# WhiteStat Fields
	# Weapon: Slot 0
	# Armor: Slot 0
	# Shield: Slot
	@property
	def durability(self) -> int:
		return self._durability

	@durability.setter
	def durability(self, value):
		if value <= 31:  # less or equal 31
			self._durability = value
			self.update_value()
		else:
			raise ValueError("value", "value must be less or equal to 31")

	@property
	def durability_percentage(self) -> int:
		return self.durability * 100 // 31

	@durability_percentage.setter
	def durability_percentage(self, value):
		if value <= 100:
			self.durability = int(31 / 100 * value)
			self.update_value()
		else:
			raise ValueError("value", "value must be less or equal to 100")

	# Weapon: Slot 3
	@property
	def hit_ratio(self) -> int:
		return self._hit_ratio

	@hit_ratio.setter
	def hit_ratio(self, value):
		if value <= 31:  # less or equal 31
			self._hit_ratio = value
			self.update_value()
		else:
			raise ValueError("value", "value must be less or equal to 31")

	@property
	def hit_ratio_percentage(self) -> int:
		return self.hit_ratio * 100 // 31

	@hit_ratio_percentage.setter
	def hit_ratio_percentage(self, value):
		if value <= 100:
			self.hit_ratio = int(31 / 100 * value)
			self.update_value()
		else:
			raise ValueError("value", "value must be less or equal to 100")

	# Armor: Slot 5
	@property
	def parry_ratio(self) -> int:
		return self._parry_ratio

	@parry_ratio.setter
	def parry_ratio(self, value):
		if value <= 31:  # less or equal 31
			self._parry_ratio = value
			self.update_value()
		else:
			raise ValueError("value", "value must be less or equal to 31")

	@property
	def parry_ratio_percentage(self) -> int:
		return self.parry_ratio * 100 // 31

	@parry_ratio_percentage.setter
	def parry_ratio_percentage(self, value):
		if value <= 100:
			self.parry_ratio = int(31 / 100 * value)
			self.update_value()
		else:
			raise ValueError("value", "value must be less or equal to 100")

	# Shield: Slot 3

	@property
	def block_ratio(self) -> int:
		return self._block_ratio

	@block_ratio.setter
	def block_ratio(self, value):
		if value <= 31:  # less or equal 31
			self._block_ratio = value
			self.update_value()
		else:
			raise ValueError("value", "value must be less or equal to 31")

	@property
	def block_ratio_percentage(self) -> int:
		return self.block_ratio * 100 // 31

	@block_ratio_percentage.setter
	def block_ratio_percentage(self, value):
		if value <= 100:
			self.block_ratio = int(31 / 100 * value)
			self.update_value()
		else:
			raise ValueError("value", "value must be less or equal to 100")

	# Weapon: Slot 6
	@property
	def critical_ratio(self) -> int:
		return self._critical_ratio

	@critical_ratio.setter
	def critical_ratio(self, value):
		if value <= 31:  # less or equal 31
			self._critical_ratio = value
			self.update_value()
		else:
			raise ValueError("value", "value must be less or equal to 31")

	@property
	def critical_ratio_percentage(self) -> int:
		return self.critical_ratio * 100 // 31

	@critical_ratio_percentage.setter
	def critical_ratio_percentage(self, value):
		if value <= 100:
			self.critical_ratio = int(31 / 100 * value)
			self.update_value()
		else:
			raise ValueError("value", "value must be less or equal to 100")

	# Weapon: Slot 1
	# Armor: Slot 1
	# Shield: Slot 1
	@property
	def phy_reinforce(self) -> int:
		return self._phy_reinforce

	@phy_reinforce.setter
	def phy_reinforce(self, value):
		if value <= 31:  # less or equal 31
			self._phy_reinforce = value
			self.update_value()
		else:
			raise ValueError("value", "value must be less or equal to 31")

	@property
	def phy_reinforce_percentage(self) -> int:
		return self.phy_reinforce * 100 // 31

	@phy_reinforce_percentage.setter
	def phy_reinforce_percentage(self, value):
		if value <= 100:
			self.phy_reinforce = int(31 / 100 * value)
			self.update_value()
		else:
			raise ValueError("value", "value must be less or equal to 100")

	# Weapon: Slot 2
	# Armor: Slot 2
	# Shield: Slot 2
	@property
	def mag_reinforce(self) -> int:
		return self._mag_reinforce

	@mag_reinforce.setter
	def mag_reinforce(self, value):
		if value <= 31:  # less or equal 31
			self._mag_reinforce = value
			self.update_value()
		else:
			raise ValueError("value", "value must be less or equal to 31")

	@property
	def mag_reinforce_percentage(self) -> int:
		return self.mag_reinforce * 100 // 31

	@mag_reinforce_percentage.setter
	def mag_reinforce_percentage(self, value):
		if value <= 100:
			self.mag_reinforce = int(31 / 100 * value)
			self.update_value()
		else:
			raise ValueError("value", "value must be less or equal to 100")

	# Weapon: Slot 4
	@property
	def phy_attack(self) -> int:
		return self._phy_attack

	@phy_attack.setter
	def phy_attack(self, value):
		if value <= 31:  # less or equal 31
			self._phy_attack = value
			self.update_value()
		else:
			raise ValueError("value", "value must be less or equal to 31")

	@property
	def phy_attack_percentage(self) -> int:
		return self.phy_attack * 100 // 31

	@phy_attack_percentage.setter
	def phy_attack_percentage(self, value):
		if value <= 100:
			self.phy_attack = int(31 / 100 * value)
			self.update_value()
		else:
			raise ValueError("value", "value must be less or equal to 100")

	# Weapon: Slot 5
	@property
	def mag_attack(self) -> int:
		return self._mag_attack

	@mag_attack.setter
	def mag_attack(self, value):
		if value <= 31:  # less or equal 31
			self._mag_attack = value
			self.update_value()
		else:
			raise ValueError("value", "value must be less or equal to 31")

	@property
	def mag_attack_percentage(self) -> int:
		return self.mag_attack * 100 // 31

	@mag_attack_percentage.setter
	def mag_attack_percentage(self, value):
		if value <= 100:
			self.mag_attack = int(31 / 100 * value)
			self.update_value()
		else:
			raise ValueError("value", "value must be less or equal to 100")

	# Armor: Slot 3
	# Shield: Slot 4
	@property
	def phy_defense(self) -> int:
		return self._phy_defense

	@phy_defense.setter
	def phy_defense(self, value):
		if value <= 31:  # less or equal 31
			self._phy_defense = value
			self.update_value()
		else:
			raise ValueError("value", "value must be less or equal to 31")

	@property
	def phy_defense_percentage(self) -> int:
		return self.phy_defense * 100 // 31

	@phy_defense_percentage.setter
	def phy_defense_percentage(self, value):
		if value <= 100:
			self.phy_defense = int(31 / 100 * value)
			self.update_value()
		else:
			raise ValueError("value", "value must be less or equal to 100")

	# Armor: Slot 4
	# Shield: Slot 5
	@property
	def mag_defense(self) -> int:
		return self._mag_defense

	@mag_defense.setter
	def mag_defense(self, value):
		if value <= 31:  # less or equal 31
			self._mag_defense = value
			self.update_value()
		else:
			raise ValueError("value", "value must be less or equal to 31")

	@property
	def mag_defense_percentage(self) -> int:
		return self.mag_defense * 100 // 31

	@mag_defense_percentage.setter
	def mag_defense_percentage(self, value):
		if value <= 100:
			self.mag_defense = int(31 / 100 * value)
			self.update_value()
		else:
			raise ValueError("value", "value must be less or equal to 100")

	# Accessory: Slot 0
	@property
	def phy_absorb(self) -> int:
		return self._phy_absorb

	@phy_absorb.setter
	def phy_absorb(self, value):
		if value <= 31:  # less or equal 31
			self._phy_absorb = value
			self.update_value()
		else:
			raise ValueError("value", "value must be less or equal to 31")

	@property
	def phy_absorb_percentage(self) -> int:
		return self.phy_absorb * 100 // 31

	@phy_absorb_percentage.setter
	def phy_absorb_percentage(self, value):
		if value <= 100:
			self.phy_absorb = int(31 / 100 * value)
			self.update_value()
		else:
			raise ValueError("value", "value must be less or equal to 100")

	# Accessory: Slot 1
	@property
	def mag_absorb(self) -> int:
		return self._mag_absorb

	@mag_absorb.setter
	def mag_absorb(self, value):
		if value <= 31:  # less or equal 31
			self._mag_absorb = value
			self.update_value()
		else:
			raise ValueError("value", "value must be less or equal to 31")

	@property
	def mag_absorb_percentage(self) -> int:
		return self.mag_absorb * 100 // 31

	@mag_absorb_percentage.setter
	def mag_absorb_percentage(self, value):
		if value <= 100:
			self.mag_absorb = int(31 / 100 * value)
			self.update_value()
		else:
			raise ValueError("value", "value must be less or equal to 100")
