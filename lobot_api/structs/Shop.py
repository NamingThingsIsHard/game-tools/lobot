from typing import Dict

from lobot_api.pk2.PK2NPC import PK2NPC
from lobot_api.structs.ShopItemEntry import ShopItemEntry


class Shop:
	"""  A concise version of the class used to map tab numbers to tabs.
	"""

	def __init__(self, npc_id):
		self.npc_id = npc_id
		# # Item reference IDs mapped to an entry: containing their page and slot in an npc's store window.
		self.item_entries: Dict[int, ShopItemEntry] = {}

	@property
	def pk2_entry(self) -> PK2NPC:
		from lobot_api.globals.PK2DataGlobal import PK2DataGlobal  # import here to prevent circular import
		return PK2DataGlobal.npcs.get(self.npc_id, None)

	def __eq__(self, other):
		if isinstance(other, Shop):
			return self.npc_id == Shop(other).npc_id
		else:
			return False

	def __str__(self):
		entry, exists = self.pk2_entry
		return str(self.pk2_entry) if exists else str(self.npc_id)
