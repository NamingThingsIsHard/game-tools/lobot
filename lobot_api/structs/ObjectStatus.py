from enum import Enum

"""  The status of characters in the game.
(0 = None, 1 = Zerk, 2 = Untouchable, 3 = GMInvincible, 4 = GMInvisible, 5= ??, 6 = Stealth, 7 = Invisible)
"""


class ObjectStatus(Enum):  # c_byte
	Normal = 0x00
	Zerk = 0x01
	Invincible = 0x02
	GMInvincible = 0x03
	GMInvisible = 0x04
	Unknown = 0x05
	Stealth = 0x06
	Invisible = 0x07
	Combat = 0x07
