from lobot_api.structs.Pet import Pet


class TransportPet(Pet):
	def __init__(self, pet: Pet):
		super(TransportPet, self).__init__()
		self.custom_name = pet.custom_name
		self.OwnerName = pet.owner_name
		self.owner_id = pet.owner_id
		self.ref_id = pet.ref_id
		self.unique_id = pet.unique_id
		self.position = pet.position
		self.current_hp = pet.current_hp
		self.max_hp = pet.max_hp
		self.is_alive = pet.is_alive
		self.run_speed = pet.run_speed
		self.is_knocked_down = pet.is_knocked_down
		self.target = pet.target
		self.is_invisible = pet.is_invisible

	def set_from_pet(self, pet: Pet):
		""" Use to update static pet in Bot for updating  UI"""
		self.custom_name = pet.custom_name
		self.OwnerName = pet.owner_name
		self.owner_id = pet.owner_id
		self.ref_id = pet.ref_id
		self.unique_id = pet.unique_id
		self.position = pet.position
		self.current_hp = pet.current_hp
		self.max_hp = pet.max_hp
		self.is_alive = pet.is_alive
		self.run_speed = pet.run_speed
		self.is_knocked_down = pet.is_knocked_down
		self.target = pet.target
		self.is_invisible = pet.is_invisible
