from typing import List

from lobot_api.scripting.ScriptExpression import AbstractExpression
from lobot_api.scripting.Point import Point
from lobot_api.structs.Shop import Shop


class Town:
	def __init__(self, n: str, s: Point, p: List[Point], script: List[AbstractExpression]):
		self.name = n
		self.SpawnPoint = s
		self.Polygon = p
		self.TownScript = script

		self.Teleporter: Shop = None
		self.Smith: Shop = None
		self.Armor = None
		self.Potions = None
		self.Accessories = None
		self.Special = None
		self.Stable = None

	def __str__(self):
		return f"{self.name}({self.SpawnPoint})"


def is_in_town(town: Town, char_location: Point) -> bool:
	p1 = None
	p2 = None
	inside: bool = False

	if len(town.Polygon) < 3:
		return inside

	oldPoint: Point = Point.from_absolute(town.Polygon[len(town.Polygon) - 1].x, town.Polygon[len(town.Polygon) - 1].y)

	for currentPoint in town.Polygon:
		if currentPoint.x > oldPoint.x:
			p1 = oldPoint
			p2 = currentPoint
		else:
			p1 = currentPoint
			p2 = oldPoint

		if ((currentPoint.x < char_location.x) is (char_location.x <= oldPoint.x)
				and (char_location.y - p1.y) * (p2.x - p1.x)
				< (p2.y - p1.y) * (char_location.x - p1.x)):
			inside = not inside
		oldPoint = currentPoint

	return inside
