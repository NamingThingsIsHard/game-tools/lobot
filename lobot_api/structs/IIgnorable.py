﻿from abc import ABC

from PyQt5.QtCore import QTimer


class IIgnorable(ABC):
	IGNORE_DELAY = 15000

	""" SpawnType that can be disregarded.
	This is helpful in handling unreachable targets.
	Upon being ignored a timer of 15s is set after which the target is deemed viable again.
	"""

	def __init__(self):
		self.ignore_timer = QTimer()
		self.ignore_timer.setInterval(self.IGNORE_DELAY)

	@property
	def is_ignored(self) -> bool:
		return self.ignore_timer.isActive()

	@is_ignored.setter
	def is_ignored(self, value):
		"""Ignore target for 15s or override and stop ignoring.
		:param value: Whether to ignore or not
		:return: None
		"""
		if value:
			if not self.is_ignored:
				self.ignore_timer.start()
		else:
			self.ignore_timer.stop()
