from lobot_api.globals.PK2DataGlobal import PK2DataGlobal
from lobot_api.structs.IIgnorable import IIgnorable
from lobot_api.structs.ISpawnObject import ISpawnObject


class ItemDrop(ISpawnObject, IIgnorable):
	def __init__(self):
		super(ItemDrop, self).__init__()
		self.ref_id = 0
		self.unique_id = 0
		self.name: str = ''
		self.owner_id = 0
		self.owner_name = ''
		self.amount: float = 0
		self.plus = 0
		self.rarity = 0  # Item Type -> 0 = normal , 1 = blue, 2= sox

	@property
	def pk2_item(self): return PK2DataGlobal.items[self.ref_id]

	@property
	def owner_id_string(self): return hex(self.owner_id)
