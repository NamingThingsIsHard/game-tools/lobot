import struct
from ctypes import *
from typing import List, NamedTuple


class PK2File:
	class PK2Header(NamedTuple):
		name: str = ''
		version: List[c_byte] = []
		encryption: c_byte = []
		verify: List[c_byte] = []
		reserved: List[c_byte] = []

		""" The string used to unpack this type from a byte array
		encodes following values:
		
		[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 30)]
		public string name
		[MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
		public version: List[c_byte]
		[MarshalAs(UnmanagedType.I1, SizeConst = 1)]
		public byte encryption
		[MarshalAs(UnmanagedType.ByValArray, SizeConst = 16)]
		public verify: List[c_byte]
		[MarshalAs(UnmanagedType.ByValArray, SizeConst = 205)]
		public reserved: List[c_byte]
		"""

		@staticmethod
		def parse_str(): return '30s4sb16s205s'

		""" The number of bytes used for this structure"""

		@staticmethod
		def struct_size() -> int: return 30 + 4 + 1 + 16 + 205  # 256

	class PK2Entry(NamedTuple):
		type_: c_byte = []
		name: str = ''
		accessTime: List[c_byte] = []
		createTime: List[c_byte] = []
		modifyTime: List[c_byte] = []
		position_: List[c_byte] = []
		size_: List[c_byte] = []
		nextChain: List[c_byte] = []
		padding: List[c_byte] = []

		@property
		def nChain(self) -> c_long: return struct.unpack('l', self.nextChain)[0]

		@property
		def position(self) -> c_long: return struct.unpack('l', self.position_)[0]

		@property
		def size(self) -> c_uint32: return struct.unpack('I', self.size_)[0]

		""" The string used to unpack this type from a byte array
		encodes following values:
		
		[MarshalAs(UnmanagedType.I1)]
		public byte type
		[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 81)]
		public string name
		[MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
		public accessTime: List[c_byte]
		[MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
		public createTime: List[c_byte]
		[MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
		public modifyTime: List[c_byte]
		[MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
		public position: List[c_byte]
		[MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
		public size: List[c_byte]
		[MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
		public nextChain: List[c_byte]
		[MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
		public padding: List[c_byte]
		"""

		@staticmethod
		def parse_str(): return 'b81s8s8s8s8s4s8s2s'

		""" The number of bytes used for this structure"""

		@staticmethod
		def struct_size() -> int: return 81 + 8 + 8 + 8 + 8 + 4 + 8 + 2  # 128

	class PK2EntryBlock(NamedTuple):
		entries: List = []  # * 128] #List[pk2_skill] = [2560]

		""" The string used to unpack this type from a byte array
				encodes following values:
		[MarshalAs(UnmanagedType.ByValArray, SizeConst = 20)]
		"""

		@staticmethod
		def parse_str(): return '2560s'  # '20s'

		""" The number of bytes used for this structure"""

		@staticmethod
		def struct_size() -> int: return 2560

	class PK2File:
		def __init__(self):
			self.name: str = ''
			self.position: c_long = 0
			self.size: c_uint32 = 0
			self.parentFolder = None  #: pk2Folder

		def __str__(self): return self.name

	class PK2Folder:
		def __init__(self):
			self.name: str = ''
			self.position: c_long = 0
			self.files: List = []  #: List[pk2File]
			self.subfolders: List = []  # List[pk2Folder]

		def __str__(self): return self.name
