from enum import Enum

from PyQt5.QtCore import pyqtSignal


class ItemFilter:
	sig_pick = pyqtSignal(bool)
	sig_store = pyqtSignal(bool)
	sig_drop = pyqtSignal(bool)
	sig_sell = pyqtSignal(bool)

	class FilterProperty(Enum):
		Pick = 0
		Store = 1
		Drop = 2
		Sell = 3

	def __init__(self, pick: bool = False, store: bool = False, sell: bool = False, drop: bool = False):
		self._pick: bool = pick
		self._store: bool = store
		self._sell: bool = sell
		self._drop: bool = drop

	@classmethod
	def from_str(cls, s: str):
		s_array = [v in ['True', '1'] for v in list(s[:4])]
		return cls(s_array[0], s_array[1], s_array[2], s_array[3])

	@property
	def pick(self):
		return self._pick

	@pick.setter
	def pick(self, value):
		if self._pick != value:
			self._pick = value
			self.sig_pick.emit(value)

	@property
	def store(self):
		return self._store

	@store.setter
	def store(self, value):
		if self._store != value:
			self._store = value
			self.sig_store.emit(value)

	@property
	def sell(self):
		return self._sell

	@sell.setter
	def sell(self, value):
		if self._sell != value:
			self._sell = value
			self.sig_sell.emit(value)

	@property
	def drop(self):
		return self._drop

	@drop.setter
	def drop(self, value):
		if self._drop != value:
			self._drop = value
			self.sig_drop.emit(value)

	def __repr__(self):
		return f"{self.pick},{self.store},{self.sell},{self.drop}"

	def __str__(self):
		return f"{int(self.pick)}{int(self.store)}{int(self.sell)}{int(self.drop)}"
