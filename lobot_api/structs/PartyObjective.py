from enum import Enum


class PartyObjective(Enum):  # : c_byte
	Hunting = 0x00
	Quest = 0x01
	TradeHunter = 0x02
	Thief = 0x03
