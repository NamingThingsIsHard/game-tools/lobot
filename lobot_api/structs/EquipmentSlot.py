from enum import Enum

MAX_EQUIP_SLOTS = 13


class EquipmentSlot(Enum):
	"""   %_HA_% or %_CA_% """
	Helm = 0
	"""   %_BA_% """
	Mail = 1
	"""   %_SA_% """
	Shoulder = 2
	"""   %_AA_% """
	Gauntlet = 3
	"""   %_LA_% """
	Pants = 4
	"""   %_FA_% """
	Boots = 5
	"""   Weapon """
	Primary = 6
	"""   Shield, Ammo """
	Secondary = 7
	"""   Earring """
	Earring = 9  # previously 8
	"""   Necklace """
	Necklace = 10  # previously 9
	"""   Left ring """
	Ring1 = 11  # previously 10
	"""   Right ring """
	Ring2 = 12  # previously 11
	"""   PVPCape, JobSuit """
	Extra = 13  # previously 12
