from lobot_api.Event import Event
from lobot_api.globals.PK2DataGlobal import PK2DataGlobal
from lobot_api.structs.BadStatus import BadStatus
from lobot_api.structs.ICombatType import ICombatType
from lobot_api.structs.Pet import Pet


class GrowthPet(Pet, ICombatType):

	on_hp_changed = Event()
	on_exp_changed = Event()
	on_hgp_changed = Event()
	on_level_changed = Event()
	on_name_changed = Event()

	def __init__(self, pet: Pet = None):
		super(GrowthPet, self).__init__()

		if pet is not None:
			self.set_from_pet(pet)
		else:
			self._level = 1
			self._exp = 0
			self._hgp = 0
			self.BadStatus = BadStatus.Normal
			self.AttackStatus = False

	@property
	def level(self):
		return self._level

	@level.setter
	def level(self, value):
		if self._level != value:
			self._level = value
			self.on_level_changed.notify(value)

	@property
	def hp_percent(self) -> int:
		return 0 if self.current_hp == 0 or self.max_hp == 0 else (self.current_hp * 100) / self.max_hp

	@property
	def exp_percent(self) -> int:
		lvl_exp = PK2DataGlobal.level_data.get(self.level)
		return (
				self.exp * 100 / lvl_exp) if PK2DataGlobal.level_data and lvl_exp else 0  # exp rounded up to second decimal place

	@property
	def hgp_percent(self) -> int:
		return self.hgp * 100 // 10000

	@property
	def hp(self):
		return self.current_hp

	@hp.setter
	def hp(self, value):
		if self.current_hp != value:
			self.current_hp = value
			self.on_hp_changed.notify(value)

	@property
	def hgp(self):
		return self._hgp

	@hgp.setter
	def hgp(self, value):
		if self._hgp != value:
			self._hgp = value
			self.on_hgp_changed.notify(value)

	@property
	def exp(self):
		return self._exp

	@exp.setter
	def exp(self, value):
		if self._exp != value:
			self._exp = value
			self.on_exp_changed.notify(self.exp_percent)

	@property
	def name(self):
		return self._name

	@name.setter
	def name(self, value):
		if self._name != value:
			self._name = value
			self.on_name_changed.notify(value)

	def set_from_pet(self, pet: Pet):
		"""Use to update static pet in Bot for updating  UI
		# Also this to reuse variables EXP and current_hp for Pet that was relocated by server from being stuck.
		# These# set by the petinfohandler after a normal respawn, not after being relocated.
		# This can be seen in the stats part of the bot window.
		"""

		self.custom_name = pet.custom_name
		self.owner_name = pet.owner_name
		self.owner_id = pet.owner_id
		self.ref_id = pet.ref_id
		self.unique_id = pet.unique_id
		self.position = pet.position
		self.current_hp = pet.current_hp
		self.max_hp = pet.max_hp
		self.is_alive = pet.is_alive
		self.is_knocked_down = pet.is_knocked_down
		self.target = pet.target
		self.is_invisible = pet.is_invisible
		if pet is GrowthPet:
			self._exp = GrowthPet(pet).exp if GrowthPet(pet).exp > 0 else self.exp

		if pet.max_hp > 0:
			self.max_hp = pet.max_hp
		elif PK2DataGlobal.npcs is not None and pet.ref_id in PK2DataGlobal.npcs:
			self.max_hp = PK2DataGlobal.npcs.get(pet.ref_id).HP
