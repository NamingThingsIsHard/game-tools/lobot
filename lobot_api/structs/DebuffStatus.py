﻿from enum import Enum


class DebuffStatus(Enum):  # c_byte
	Normal = 0x00
	BadStatus = 0x01
	Debuff = 0x02
	BadStatusDebuff = 0x03
