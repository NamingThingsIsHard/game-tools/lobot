﻿from lobot_api.Event import Event

""" <summary>
# Opcode: 0x303D
# Name: SERVER_AGENT_CHARACTER_STATS
# Description:
# Encryption: False
# Massive: False
# public const ushort SERVER_AGENT_CHARACTER_STATS = 0x303D
#	4	uint	PhyAtkMin
#	4	uint	PhyAtkMax
#	4	uint	MagAtkMin
#	4	uint	MagAtkMax
#	2	ushort	PhyDef
#	2	ushort	MagDef
#	2	ushort	HitRate
#	2	ushort	ParryRate
#	4	uint	max_hp
#	4	uint	max_mp
#	2	ushort	STR
#	2	ushort	INT
"""


class CharInfo:
	on_str_changed = Event()
	on_int_changed = Event()
	on_max_hp_changed = Event()
	on_max_mp_changed = Event()

	def __init__(self):
		self._str = 0
		self._int = 0
		self.PhyAtkMin = 0
		self.PhyAtkMax = 0
		self.MagAtkMin = 0
		self.MagAtkMax = 0
		self.PhyDef = 0
		self.MagDef = 0
		self.HitRate = 0
		self.ParryRate = 0
		self._max_hp = 0
		self._max_mp = 0

	@property
	def max_hp(self):
		return self._max_hp

	@max_hp.setter
	def max_hp(self, value):
		if value != self._max_hp:
			self.on_max_hp_changed.notify(value)
			self._max_hp = value

	@property
	def max_mp(self):
		return self._max_mp

	@max_mp.setter
	def max_mp(self, value):
		if value != self._max_mp:
			self.on_max_mp_changed.notify(value)
			self._max_mp = value

	@property
	def STR(self):
		return self._str

	@STR.setter
	def STR(self, value):
		if value != self._str:
			self.on_str_changed.notify(value)
			self._str = value

	@property
	def INT(self):
		return self._int

	@INT.setter
	def INT(self, value):
		if value != self._int:
			self.on_int_changed.notify(value)
			self._int = value
