from ctypes import *
from typing import List

from lobot_api.Bot import Bot
from lobot_api.globals.settings.PartySettings import PartySettings
from lobot_api.structs import PartyMember, PartyObjective, PartyType


class PartyEntry:
	def __init__(self, number=0):
		self.party_number = number
		self.member_count = 0
		self.leader_id = 0
		self.leader = ''
		self.title = PartySettings.instance().title
		self.country = Bot.char_data.country
		self.type: PartyType = PartySettings.instance().type
		self.objective: PartyObjective = PartySettings.instance().objective
		self.min_level: c_uint32 = PartySettings.instance().min_level
		self.max_level: c_uint32 = PartySettings.instance().max_level
		self.members: List[PartyMember] = []

	@property
	def type_string(self) -> str: return self.type.ToFriendlyString()

	@property
	def min_max(self) -> str: return f"({self.min_level}/{self.max_level})"  # {0,-8}", MinLevel + "~" + MaxLevel)

	@property
	def party_fullness(self) -> str: return f"{self.member_count}/{self.type.GetMaxMembers()}"

	@property
	def max_members(self) -> c_byte: return self.type.GetMaxMembers()

	@property
	def is_full(self) -> bool: return self.member_count is self.type.GetMaxMembers()

	def __str__(self):
		return f"{self.party_number}-({self.party_fullness}) {self.type.ToFriendlyString()} {self.min_max}, {self.leader}, {self.title}"

	# f"{str(c.PartyNumber)}\t({c.MemberCount}){c.Leader}\t{c.MinLevel}-{c.MaxLevel}"
	# 0, -8}  1}\t2, -13}  3}  4}:5}", PartyNumber, PartyFullness, cls.type.ToFriendlyString(), MinMax, Leader, Title)

	def __eq__(self, other):
		return isinstance(other, PartyEntry) and self.party_number == other.party_number
