from lobot_api.globals.PK2DataGlobal import PK2DataGlobal
from lobot_api.pk2.PK2NPC import PK2NPC
from lobot_api.scripting.Point import Point
from lobot_api.structs.ICombatType import ICombatType
from lobot_api.structs.ISpawnObject import ISpawnObject


class Pet(ISpawnObject, ICombatType):

	def __init__(self):
		super(Pet, self).__init__()
		self.owner_name = ''
		self._custom_name = ''
		self.owner_id = 0x00
		self.ref_id = 0x00  # ID for game class
		self.unique_id = 0x00  # ID for instance of class
		self.position = Point(0, 0, 0)
		self.current_hp = 0x00  # => _current_hp  # set  _current_hp = value NotifyPropertyChanged("hp_percent") } }
		self.max_hp = 0x00
		self.is_alive = True
		self.run_speed = 0
		self.is_knocked_down = False
		self.target = ''
		self.is_invisible: bool = False
		self.ScrollSlot = 0x00

	@property
	def pk2_entry(self) -> PK2NPC: return PK2DataGlobal.npcs[self.ref_id]

	@property
	def name(self) -> str:
		return self.pk2_entry.name

	@property
	def custom_name(self) -> str: return self._custom_name

	@custom_name.setter
	def custom_name(self, value):
		self._custom_name = value

	def return_to_player(self):
		"""commands pet to return to Player. Useful to stop attacking. """
		pass
