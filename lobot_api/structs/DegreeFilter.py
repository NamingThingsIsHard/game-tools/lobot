class DegreeFilter:
	"""The type# settings in item filters
	Uses 3 digit hex number for all 12 degree flags (~lvl 120)"""

	"""Mask used to initialize 12 flags"""
	__flag_mask__ = 0xFFF
	"""Number of degrees"""
	__flag_count = 12

	def __init__(self, val: int = 0xE00):
		self.flags = DegreeFilter.__flag_mask__ & val

	# True,  # for items without degree -> degree == 0

	@staticmethod
	def get_flag(flag_string: str, degree: int):
		"""
		Get a boolean of whether to consider an item from a degree or not.
		Flag string is written as normal number from left to right, so degree is accessed counting backwards.
		:param flag_string: string with boolean values
		:param degree: Degree to consider
		:return: Boolean from flags
		"""
		return flag_string[-degree] == '1'

	def set_flag(self, degree: int, val):
		"""
		Set a degree flag to boolean value.
		:param degree: Degree to be set. Bit shifts value by this int.
		:param val: Value to be set
		:return: Boolean from flags
		"""
		mask = 1 << (degree-1)
		if val:
			self.flags |= mask
		else:
			self.flags &= ~mask
		return bool(val)

	@staticmethod
	def get_index_binary_different_binary_digit(x: int, y: int):
		"""Get the index of the different bit between two integers.
		Used to show a change made in checkboxes for DegreeFilter."""
		difference = (x ^ y).bit_length()
		return difference

	def __repr__(self):
		return format(self.flags, '012b')

	def __str__(self):
		return format(self.flags, '012b')
