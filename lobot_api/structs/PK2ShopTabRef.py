from typing import List

from lobot_api.structs.PK2ShopItemRef import PK2ShopItemRef


class PK2ShopTabRef:
	def __init__(self, tablong_id: str, items: List[PK2ShopItemRef]):
		if not tablong_id:
			raise ValueError(tablong_id)
		elif not items:
			raise ValueError(items)

		self.TabNumber: int
		self.tablong_id = tablong_id
		self.Items = items
