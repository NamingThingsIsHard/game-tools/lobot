from lobot_api.pk2.PK2TeleporterTypes import PK2Teleporter
from lobot_api.scripting.Point import Point
from lobot_api.structs.ISpawnObject import ISpawnObject


class Portal(ISpawnObject):
	def __init__(self):
		super(Portal, self).__init__()
		self.position = Point()
		self.ref_id = 0  # ID for game class
		self.unique_id = 0  # ID for instance of class
		self.PK2Data: PK2Teleporter = None

	@property
	def name(self):
		if self.PK2Data.name is not None:
			return self.PK2Data.name
		else:
			return str(self.ref_id)
