﻿from abc import ABC

from lobot_api.Event import Event
from lobot_api.packets.char.MovementArgs import MovementArgs
from lobot_api.scripting.Point import Point


class ISpawnObject(ABC):
	""" The interface types to provide both ID types."""

	def __init__(self):
		self.on_moved = Event()

		""" The spawn unit name """
		self._name = ''

		""" ID to lookup info from pk2 lists """
		self.ref_id = 0

		""" ID to lookup in spawn list or perform in-game interactions. """
		self.unique_id = 0

		""" Position to determine interaction with unit. """
		self._position: Point = Point()

	@property
	def position(self):
		return self._position

	@position.setter
	def position(self, value):
		if self._position != value:
			self.on_moved.notify(MovementArgs(self.unique_id, value, value))
			self._position = value

	@property
	def name(self):
		return self._name

	@name.setter
	def name(self, value):
		if self._name != value:
			self._name = value
