from math import floor
from typing import Dict, List

from lobot_api.scripting.Point import Point


class Area:

	def __init__(self, x: float = 0, y: float = 0, r: float = 0):

		self._x: float = x
		self._y: float = y
		self._radius: float = r
		self.adjacency_list: Dict[Point, List[Point]] = {}

		self.add_adjacent_points(self.center_point)

	@classmethod
	def from_string(cls, s):
		array = [int(v) if v else 0 for v in s.split(',', 2)]
		x = array[0]
		y = array[1] if len(array) > 1 else 0
		r = array[2] if len(array) > 2 else 20
		return cls(x, y, r)

	def __str__(self):
		return f"{self.x},{self.y},{self._radius}"

	def __eq__(self, other):
		return self.x == other.x and self.y == other.y and self.radius == other.radius

	@property
	def x(self) -> float:
		return self._x

	@x.setter
	def x(self, value):
		if self._x != value:
			self._x = value
			self.reset_adjacency_list()

	@property
	def y(self) -> float:
		return self._y

	@y.setter
	def y(self, value):
		if self._y != value:
			self._y = value
			self.reset_adjacency_list()

	@property
	def radius(self) -> float:
		return self._radius

	@radius.setter
	def radius(self, value):
		if self._radius != value:
			self._radius = value
			self.reset_adjacency_list()

	@property
	def radius_no_edge(self):
		"""Prevent roaming along edge and stay closer to middle"""
		return self._radius - 15 if self._radius > 30 else self.radius


	@property
	def point_distance(self):
		"""
		Distance between two walk points in area. Increases proportionally to radius.
		:return: distance to next point
		"""
		return self._radius // 4 + 15 if self._radius > 30 else self.radius_no_edge

	@property
	def get_x_sector(self) -> int:
		return floor(self.x / 192 + 135)

	@property
	def get_y_sector(self) -> int:
		return floor(self.y / 192 + 92)

	@property
	def get_x_offset(self) -> float:
		result = self.x % 192
		return result * 10

	@property
	def get_y_offset(self) -> float:
		result = self.y % 192
		return result * 10

	@property
	def center_point(self) -> Point:
		return Point.from_absolute(self.x, self.y)

	def reset_adjacency_list(self):
		self.adjacency_list.clear()
		self.add_adjacent_points(self.center_point)

	"""Get an adjacency list of points within the area radius 
	"""

	def add_adjacent_points(self, start: Point):
		"""
		Generate walkpoints within area using an 8 flood fill 8 manner.
		The points make up a grid of points separated by a calculated point distance which the bot uses while roaming
		to look for spawn.
		:param start: Starting point
		:return: None
		"""
		stack = [start]

		while len(stack) > 0:
			p = stack.pop(0)
			# loop if not yet in adjacency list
			self.adjacency_list[p] = []

			candidates = [
				Point.from_absolute(p.x, p.y - self.point_distance),  # down
				Point.from_absolute(p.x, p.y + self.point_distance),  # up
				Point.from_absolute(p.x - self.point_distance, p.y),  # left
				Point.from_absolute(p.x + self.point_distance, p.y),  # right
				Point.from_absolute(p.x - self.point_distance, p.y - self.point_distance),  # bottom left
				Point.from_absolute(p.x - self.point_distance, p.y + self.point_distance),  # top left
				Point.from_absolute(p.x - self.point_distance, p.y + self.point_distance),  # bottom right
				Point.from_absolute(p.x + self.point_distance, p.y + self.point_distance)  # top right
			]

			for c in candidates:
				distance = self.center_point.distance_point(c)
				if distance <= self.radius_no_edge:
					self.adjacency_list[p].append(c)
					if c not in self.adjacency_list:
						stack.append(c)
