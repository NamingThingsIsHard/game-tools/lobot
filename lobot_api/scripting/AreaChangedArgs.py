from enum import Enum


class AreaType(Enum):  # : c_byte

	none = 0x0
	AreaOne = 0x01
	AreaTwo = 0x02


class AreaChangedArgs:
	def __init__(self, area_number: AreaType):
		self.AreaNumber = area_number
