import math
import random
from typing import List, Optional

from lobot_api.Bot import Bot
from lobot_api.globals.settings.TrainingSettings import TrainingAreaSettings
from lobot_api.scripting.Area import Area
from lobot_api.scripting.Point import Point
from lobot_api.scripting.ScriptExpression import CommandExpression, AbstractExpression

MAX_GO_DISTANCE: int = 80

# @staticmethod
# def get_distance(point1: Position, x: float, y: float) -> float:
# 	""" Calculates the distance between two points."""
# 	return get_distance(point1, Position(x, y))

# @staticmethod
# def get_distance(x1: float, y1: float, x2: float, y2: float) -> float:
# 	""" Calculates the distance between two points by their coordinates."""
# 	return get_distance(Position(x1, y1), Position(x2, y2))


def get_distance_go_gommand(go1: CommandExpression, go2: CommandExpression) -> float:
	"""Calculates the distance between two points by their go commands."""
	return Point.from_absolute_coords(float(go1.arguments[0]), float(go1.arguments[1]))\
		.distance_point(Point.from_absolute_coords(float(go2.arguments[0]), float(go2.arguments[1])))


def get_char_distance_bot_to_area(a: Area) -> float:
	"""Calculates distance to from bot position to area."""
	return Bot.char_data.position.distance_point(a.center_point) - a.radius


def get_distance_to_area(a: Area, p: Point) -> float:
	"""Calculates distance from given position to area."""
	return p.distance_point(a.center_point) - a.radius


def distance_to_training_area1():
	return get_char_distance_bot_to_area(TrainingAreaSettings.instance().area1)


def distance_to_training_area2():
	return get_char_distance_bot_to_area(TrainingAreaSettings.instance().area2)


def distance_to_closest_area():
	return get_char_distance_bot_to_area(closest_area_to_char())


def is_char_in_area1():
	return distance_to_training_area1() <= TrainingAreaSettings.instance().area1.radius


def is_char_in_area2():
	return distance_to_training_area2() <= TrainingAreaSettings.instance().area2.radius


def is_point_in_any_area(p: Point) -> bool:
	a1 = TrainingAreaSettings.instance().area1
	a2 = TrainingAreaSettings.instance().area2
	return p.distance_point(a1.center_point) <= a1.radius or p.distance_point(a2.center_point) <= a2.radius

def is_position_in_closest_area(p: Point) -> bool:
	return get_distance_to_area(closest_area_to_char(),
	                            p) <= closest_area_to_char().radius


def closest_area_to_char() -> Area:
	distance_area1 = get_char_distance_bot_to_area(TrainingAreaSettings.instance().area1)
	distance_area2 = get_char_distance_bot_to_area(TrainingAreaSettings.instance().area2)
	return (TrainingAreaSettings.instance().area1
	        if distance_area1 < distance_area2
	        else TrainingAreaSettings.instance().area2
	        )


def get_closest_area_to_position(p: Point) -> Area:
	return (TrainingAreaSettings.instance().area1
	        if get_distance_to_area(TrainingAreaSettings.instance().area1, p) < get_distance_to_area(TrainingAreaSettings.instance().area2, p)
	        else TrainingAreaSettings.instance().area2)


def generate_position_in_area1() -> Point:
	r = random.randint(0, 360)
	angle: int = r  # r.Next(360)
	x: int = int(
		TrainingAreaSettings.instance().area1.get_x_offset + TrainingAreaSettings.instance().area1.radius_no_edge * math.cos(
			angle))
	y: int = int(
		TrainingAreaSettings.instance().area1.get_y_offset + TrainingAreaSettings.instance().area1.radius_no_edge * math.sin(
			angle))
	return Point.from_absolute_coords(TrainingAreaSettings.instance().area1.x + x,
	                                  TrainingAreaSettings.instance().area1.y + y)


def generate_position_in_area2() -> Point:
	r = random.randint(0, 360)
	angle: int = r  # r.Next(360)
	x: int = int(
		TrainingAreaSettings.instance().area2.get_x_offset + TrainingAreaSettings.instance().area2.radius_no_edge * math.cos(
			angle))
	y: int = int(
		TrainingAreaSettings.instance().area2.get_y_offset + TrainingAreaSettings.instance().area2.radius_no_edge * math.sin(
			angle))
	return Point.from_absolute_coords(TrainingAreaSettings.instance().area2.x + x,
	                                  TrainingAreaSettings.instance().area2.y + y)


def get_walk_position_within_closest_area() -> Point:
	return (generate_position_in_area1()
	        if distance_to_training_area1() < distance_to_training_area2()
	        else generate_position_in_area2())


def get_x_coord(x_offset: float, x_sector, offset_divisor: int = 10) -> int:
	"""Calculate the x-coordinate using the x-sector in movement response packet B021."""
	return int((x_sector - 135) * 192 + (x_offset / offset_divisor))


def get_y_coord(y_offset: float, y_sector, offset_divisor: int = 10) -> int:
	"""Calculate the y- coordinate using the X sector in movement response packet B021."""
	return int((y_sector - 92) * 192 + (y_offset / offset_divisor))


def get_x_coord_origin(x_offset, x_sector) -> int:
	"""Calculate the origin x-coordinate using the x-sector in movement response packet B021."""
	return get_x_coord(x_offset, x_sector, 100)


def get_y_coord_origin(y_offset, y_sector) -> int:
	"""Calculate the origin y-coordinate using the X sector in movement response packet B021."""
	return get_y_coord(y_offset, y_sector, 100)


def get_sector_x(x: int):
	"""Get a walk packet sector to calculate the character location."""
	return 0xFF & math.floor(x / 192 + 135)


def get_sector_y(y: int):
	"""Get a walk packet sector to calculate the character location."""
	return 0xFF & math.floor(y / 192 + 92)


def get_first_walk_position(script: List[AbstractExpression]) -> Point:
	"""Get the first walk point."""
	command: Optional[CommandExpression] = None
	for expr in script:
		if isinstance(expr, CommandExpression) and expr.command_name == "go":
			command = expr
			return Point.from_absolute_coords(float(command.arguments[0]), float(command.arguments[1]))
	raise ValueError("No walk point found.")


def get_first_walk_point_index(script: List[AbstractExpression]):
	"""Get the index of the first walk point."""
	command: Optional[CommandExpression] = None
	for i in range(0, len(script)):
		if isinstance(script[i], CommandExpression):
			command = script[i]
			if command.command_name == "go":
				return i
	return -1


def get_first_command_expression_index(script: List[AbstractExpression]):
	"""Get the index of the first command expression."""
	return next((i for i, e in enumerate(script) if isinstance(e, CommandExpression)), -1)


def total_distance(script: List[AbstractExpression]) -> float:
	total: float = 0
	if len(script) == 0 or script is None:
		return total

	current_position_index: int = 0
	previous_command: Optional[CommandExpression] = None
	current_command: CommandExpression
	# Find first movement command
	while not previous_command:
		if isinstance(script[current_position_index], CommandExpression):
			current_command = script[current_position_index]
			if current_command.command_name == "go":
				previous_command = script[current_position_index]
		else:
			current_position_index += 1

	# Calculate distance between all points
	for i in range(current_position_index, len(script)):

		if isinstance(script[i], CommandExpression):
			current_command = script[i]
			if current_command.command_name == "go":
				total += get_distance_go_gommand(previous_command, current_command)
				previous_command = current_command
	return total


def index_of_closest_go_command(current_position: Point, script: List[AbstractExpression]) -> int:
	"""Goes through the script and tries to find closest point to start at."""
	if not script:
		return -1

	current = CommandExpression("go", [str(current_position.x), str(current_position.y)])
	command: Optional[CommandExpression] = None
	command_position: Optional[Point] = None
	distance: float
	current_distance: float = 1000
	current_index: int = -1
	for i in range(0, len(script)):
		if isinstance(script[i], CommandExpression):
			command = script[i]
			if command.command_name == "go":
				distance = get_distance_go_gommand(current, command)
				if distance < current_distance:
					current_distance = distance
					current_index = i
	return current_index if current_distance <= MAX_GO_DISTANCE else -1


def index_of_closest_point(current_position: Point, points: List[Point]) -> int:
	"""Returns the index of the point in a list with the shortest distance to a given position."""

	distance: float
	current_distance: float = 1000
	current_index: int = -1
	for i in range(0, len(points)):
		distance = current_position.distance_point(points[i])
		if distance < current_distance:
			current_distance = distance
			current_index = i
	return current_index


# def matches_requirement(meta: MetaExpression, script: List[AbstractExpression]) -> bool:
# 	"""Check whether the script matches the MetaExpression."""
# 	# switch (meta.ExpressionName)
#
# 	if meta.ExpressionName is "StartingTown":
# 		value = Towns.get(str.lower(meta.Argument1), None)
# 		if value and Town.current_town(get_first_walk_position(script)).name == value.name:
# 			return True
# 		return False
# 	elif meta.ExpressionName is "MonsterLevel":
# 		# TODO compare internal monster list level to given MonsterLevel
# 		return False
# 	elif meta.ExpressionName is "MonsterDifficulty":
# 		# TODO find character's level difference (# set +) compared to monster level
# 		return False
# 	elif meta.ExpressionName is "ScriptDifficulty":
# 		# TODO find a way to determine monster levels of points that character has to run through
# 		return False
# 	elif meta.ExpressionName is "NextScript":
# 		# TODO search script folder for script with containing meta name
# 		return False
# 	else:
# 		return True
#
# def matches_requirements(script: List[AbstractExpression]) -> bool:
# 	return True
# # TODO Enhancement
# MetaExpression meta
##for i in range(0, len(script)):
#
#    if script[i].get_type() == typeof(MetaExpression):
#
#        meta = (MetaExpression)script[i]
#        if not MatchesRequirement(meta, script):
#
#            return False
#
