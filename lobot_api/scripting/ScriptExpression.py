import logging
import re
from abc import ABC
from typing import List, Optional, Dict, Callable

from lobot_api.logger.Logger import SCRIPT


class AbstractExpression(ABC):
	pass


class CommentExpression(AbstractExpression):
	""" A script expression used to add information to the script that is not computed."""

	def __init__(self, token: str):
		if re.match(Patterns.COMMENT, token):
			self.comment = token
		else:
			raise SyntaxError(f"Invalid comment: {token}")

	def __eq__(self, other):
		return self.comment == other.comment

	def __repr__(self):
		return self.comment

	def __str__(self):
		return self.comment


class CommandExpression(AbstractExpression):
	"""  A script expression that can be used to execute an action inside the game. """

	def __init__(self, expr: str, args: List[str] = None):
		self.command_name: str = ''
		self.arguments = []
		if args:
			expr = f"{expr} {','.join(args)}"

		if re.search(Patterns.COMMAND_EXPR, expr):
			self.evaluate_expression(expr)
		else:
			raise SyntaxError(f"Invalid CMD_EXPR: {expr}")

	def __eq__(self, other):
		return self.command_name == other.command_name and self.arguments == other.arguments

	def __repr__(self):
		return self.__str__()

	def __str__(self):
		return self.command_name + (f" {','.join(self.arguments)}" if self.arguments else '')

	def evaluate_expression(self, expression: str):
		""" Glean information from the token given in the constructor. """
		# remove trailing comments
		split_token = list(filter(None, expression.split("//", 2)))
		split_token = list(filter(None, re.split(r"[ ,]", split_token[0])))
		self.command_name = split_token.pop(0)
		self.arguments = split_token

	@staticmethod
	def raise_notimplemented():
		raise NotImplementedError()

	def execute(self):
		self.command_list[self.command_name]()

	# TODO implement
	command_list: Dict[str, Callable] = {
		"go": lambda: CommandExpression.raise_notimplemented(),  # Proxy.Instance.SendCommandAG(GoCommand(s)),
		"wait": lambda: CommandExpression.raise_notimplemented(),  # WaitCommand(s) ,
		"teleport": lambda: CommandExpression.raise_notimplemented(),  # TeleportCommand(s) ,
		"quest": lambda: CommandExpression.raise_notimplemented(),  # throw new NotImplementedException(),
		"store": lambda: CommandExpression.raise_notimplemented(),  # OpenShop(BotState.Store) ,
		"stable": lambda: CommandExpression.raise_notimplemented(),  # OpenShop(BotState.Stable) ,
		"repair": lambda: CommandExpression.raise_notimplemented(),  # OpenShop(BotState.Smith),
		"potions": lambda: CommandExpression.raise_notimplemented(),  # OpenShop(BotState.Potions) ,
		"special": lambda: CommandExpression.raise_notimplemented(),  # OpenShop(BotState.Special) ,
		"mount": lambda: CommandExpression.raise_notimplemented(),  # Mount(),
		"dismount": lambda: CommandExpression.raise_notimplemented(),  # Dismount(),
		"setArea1": lambda: CommandExpression.raise_notimplemented(),  # SetArea(AreaType.AreaOne, s),
		"setArea2": lambda: CommandExpression.raise_notimplemented(),  # SetArea(AreaType.AreaTwo, s)}
	}


class MetaExpression(AbstractExpression):
	"""A script expression that can be used to decide which script to execute."""

	# def __init__(self, token: str, command: str, arg1: str, arg2: str = ''):
	def __init__(self, token: str):
		if re.match(re.compile(Patterns.META_EXPR), token):
			self.expression_name = ''
			self.argument1 = ''
			self.argument2 = ''
			self.evaluate(token)
		else:
			raise SyntaxError(f"Invalid META_EXPR: {token}")

	def evaluate(self, token: str):
		"""Parse information from the injected token."""
		split_token = list(filter(None, token.split("//", 2)))
		split_token = list(filter(None, re.split(r'\W+', split_token[0])))
		self.expression_name = split_token[0]
		self.argument1 = split_token[1]
		if len(split_token) == 3:
			if self.expression_name == "NextScript":
				self.argument2 = split_token[2]
			else:
				raise SyntaxError(f"Invalid META_EXPR, too many arguments: {token}")

	def __eq__(self, other):
		return (self.expression_name == other.expression_name
		        and self.argument1 == other.argument1
		        and self.argument2 == other.argument2)

	def __str__(self):
		sb = [self.expression_name]
		if self.argument1:
			sb.append(f" {self.argument1}")
			if self.argument2:
				sb.append(f", {self.argument2}")
		return "".join(sb)

	@staticmethod
	def raise_notimplemented():
		raise NotImplementedError()

	"""List of currently implemented functions.
	Metaexpressions match their commandName to the string to execute the matching function."""
	meta_expression_list = {
		"StartingTown": lambda s1, s2: MetaExpression.raise_notimplemented(),
		"MonsterLevel": lambda s1, s2: MetaExpression.raise_notimplemented(),
		"MonsterDifficulty": lambda s1, s2: MetaExpression.raise_notimplemented(),
		"ScriptDifficult": lambda s1, s2: MetaExpression.raise_notimplemented(),
		"ScriptDifficulty": lambda s1, s2: MetaExpression.raise_notimplemented(),
		"NextScript": lambda s1, s2: MetaExpression.raise_notimplemented()
	}

	def execute(self):
		"""Invoke method in expression"""
		self.meta_expression_list[self.expression_name](self.argument1, self.argument2)


class Patterns:
	"""The patterns used to identify valid expressions for a script."""
	COMMENT = "\\s*#(\\s*)(.*)$"
	TELEPORTMODE = ("\\b((\\s*\\d+\\s*,\\s*\\d+\\s*)" +  # npc_id,TeleportOption
	                "|reverseRecall" +
	                "|reverseDeath" +
	                "|reverseLocation\\s+location\\b)")
	COMMAND = ("\\bgo (?x)((\\s*(-)?\\d+\\s*),(\\s*(-)?\\d+\\s*)(\\s*\\s*$|,\\d+\\s*))" +
	           "|wait\\b(\\s*|\\s+\\d*\\s*)$" +
	           "|teleport\\s+" +  # TELEPORTMODE +
	           "|quest\\s+(?x)((\\s*\\d+\\s*),(\\s*\\d+\\s*))" +
	           "|store\\s*$" +  # stash items
	           "|stable\\s*$" +  # buy COSitems
	           "|repair\\s*$" +  # repair items at the weapons trader
	           "|potions\\s*$" +  # buy potions
	           "|special\\s*$" +  # buy special items
	           "|mount\\s*$" +
	           "|dismount\\s*$" +
	           "|# setArea1\\s+(?x)((\\s*(-)?\\d+\\s*),(\\s*(-)?\\d+\\s*),(\\s*\\d+\\s*))" +
	           "|# setArea2\\s+(?x)((\\s*(-)?\\d+\\s*),(\\s*(-)?\\d+\\s*),(\\s*\\d+\\s*)))")
	TOWN = ("\\b(?i)(Jangan" +
	        "|Donwang " +
	        "|Hotan" +
	        "|Samarkand" +
	        "|Constantinople" +
	        "|Alexandria(\\s+(North|South))?)\\b")
	# TODO rewrite to prevent error:
	#  ScriptExpression.py:190: DeprecationWarning: Flags not at the start of the expression '(\\bStartingTown\\s*\\b' (truncated)
	#   elif re.match(Patterns.META_EXPR, line):
	META_EXPR = ("\\b(StartingTown\\s*" + TOWN +
	             "|MonsterLevel\\s+\\b(1|[0-9]?[0-9]|100)\\b" +
	             "|MonsterDifficulty\\s+\\b([1-9]|10)\\b" +
	             "|ScriptDifficulty\\s+\\b([1-9]|10)\\b" +
	             "|NextScript\\s+\\b([01]?[0-9]|100)\\b(?x)(\\b(\\s*,\\s*[0-9]|10)?\\b))" + COMMENT)  # MonsterLevel(,MonsterDifficulty)?
	COMMAND_EXPR = f"{COMMAND}\\s*($|{COMMENT})|{COMMENT})"
	LINE_EXPR = f"{META_EXPR}|{COMMAND_EXPR}|(\\s)*"


class ExpressionParser:
	""" Class used to parse script files."""

	@staticmethod
	def parse_lines(lines: List[str]) -> List[AbstractExpression]:
		""" Parse loop for lines in file."""
		command_list: List[AbstractExpression] = []

		for line in lines:
			if re.match(Patterns.COMMENT, line) or line == "":
				continue
			elif re.match(Patterns.META_EXPR, line):
				command_list.append(MetaExpression(line))
			elif re.match(Patterns.COMMAND_EXPR, line):
				command_list.append(CommandExpression(line))
			else:
				raise SyntaxError("Invalid line in script: " + line)

		return command_list

	@staticmethod
	def parse_script(file_path: str) -> Optional[List[AbstractExpression]]:
		""" Main parsing function for script in file."""
		if not file_path:
			return None
		else:
			with open(file_path) as f:
				return ExpressionParser.parse_lines(f.read().splitlines())

	@staticmethod
	def parse_script_lines(lines: List[str]) -> List[AbstractExpression]:
		""" Main parsing function for script as a array: str."""
		try:
			return ExpressionParser.parse_lines(lines)
		except Exception as e:
			logging.log(SCRIPT, repr(e))
			raise
