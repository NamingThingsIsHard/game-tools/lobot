from lobot_api.scripting.ScriptExpression import AbstractExpression


class ScriptCommandArgs:

	def __init__(self, expr: AbstractExpression = None):
		if expr is None:
			raise ValueError(AbstractExpression.__name__)
		self.expr = expr
