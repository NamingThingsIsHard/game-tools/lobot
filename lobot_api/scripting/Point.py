# from lobot_api.scripting import ScriptUtils
import math
from typing import List, Tuple


# class PointScript:
# 	"""Simple 3d struct to show absolute values of coordinates:
# 	Points after calculation using xsector/ysector and x/y point."""
#
# 	def __init__(self, x=0, z=0, y=0):
# 		self.x = x
# 		self.y = y
# 		self.z = z
#
# 	def __repr__(self):
# 		return f"{self.x},{self.y},{self.z}"
#
# 	def __str__(self):
# 		return f"x({self.x}), y({self.y}), z({self.z})"
#
# 	def __eq__(self, other):
# 		if isinstance(other, PointScript):
# 			return self.x == other.x and self.y == other.y and self.z == other.z
# 		else:
# 			return False
#
# 	def __hash__(self):
# 		return hash((self.x, self.y, self.z))
#
# 	def distance(self, p):
# 		return 0 if self is p else float(math.sqrt(abs((self.x - p.x)**2 + (self.y - p.y)**2)))


class Point:
	"""
	3d struct combining point and x,y sector for silkroad coordinates.
	The values in this struct are all replaced whenever a movement is made.
	"""

	def __init__(self, x_sector=0, y_sector=0, x_offset=0, z_offset=0, y_offset=0, angle=0):
		self.x_sector = x_sector
		self.y_sector = y_sector
		self._x_offset = x_offset
		self._y_offset = y_offset
		self._z_offset = z_offset
		# self._point_script = PointScript(self.get_x_absolute(x_sector, x_offset), z,
		#                                 self.get_y_absolute(y_sector, y_offset))
		self.angle = angle

	def set_from_point(self, p):
		"""
		Change coordinates to those of an existing Point.
		:param p: Point to use
		:return: None
		"""
		self.x_sector = p.x_sector
		self.y_sector = p.y_sector
		self._x_offset = p.x_offset
		self._y_offset = p.y_offset
		self._z_offset = p.z
		# self._point_script = PointScript(self.get_x_absolute(x_sector, x_offset), z,
		#                                 self.get_y_absolute(y_sector, y_offset))
		self.angle = p.angle

	def set_from_values(self, x_sector=0, y_sector=0, x_offset=0, z_offset=0, y_offset=0, angle=0):
		"""
		Change coordinates after movement packet received.
		:param x_sector: x map tile
		:param y_sector: y map tile
		:param x_offset: x offset on map tile
		:param z_offset: height on map
		:param y_offset: y offset on map tile
		:param angle: facing direction
		:return: None
		"""
		self.x_sector = x_sector
		self.y_sector = y_sector
		self._x_offset = x_offset
		self._y_offset = y_offset
		self._z_offset = z_offset
		# self._point_script = PointScript(self.get_x_absolute(x_sector, x_offset), z,
		#                                 self.get_y_absolute(y_sector, y_offset))
		self.angle = angle

	@classmethod
	def from_absolute_coords(cls, x, y, z=0):
		"""Constructor for getting Position from bot script coordinates."""
		x_sector = Point.get_x_sector(x)
		y_sector = Point.get_y_sector(y)
		x_offset = Point.get_x_offset(x)
		y_offset = Point.get_y_offset(y)
		z_offset = z
		return cls(x_sector, y_sector, x_offset, z_offset, y_offset)

	@classmethod
	def from_absolute(cls, x, y, z=0):
		"""Constructor for getting Position from bot script point."""
		x_sector = Point.get_x_sector(x)
		y_sector = Point.get_y_sector(y)
		x_offset = Point.get_x_offset(x)
		y_offset = Point.get_y_offset(y)
		z_offset = z
		return cls(x_sector, y_sector, x_offset, z_offset, y_offset)

	@classmethod
	def from_absolute_coords_list(cls, points: List[Tuple[int, int]]):
		"""Constructor for getting Position from list of bot script coordinates."""
		result = []
		for p in points:
			x_sector = Point.get_x_sector(p[0])
			y_sector = Point.get_y_sector(p[1])
			x_offset = Point.get_x_absolute(x_sector, p[0])
			y_offset = Point.get_y_absolute(y_sector, p[1])
			z_offset = 0
			result.append(cls(x_sector, y_sector, x_offset, z_offset, y_offset))
		return result

	@classmethod
	def get_x_offset(cls, x) -> int:
		"""Calculate the game x-coordinate using the x-sector in movement response packet B021."""
		result = x % 192
		if result < 0:
			result += 192
		return result * 10

	@classmethod
	def get_y_offset(cls, y) -> int:
		"""Calculate the game y-coordinate using the X sector in movement response packet B021."""
		result = y % 192
		if result < 0:
			result += 192
		return result * 10

	@classmethod
	def get_x_sector(cls, x: int):
		"""Get a walk packet sector to calculate the character location."""
		return 0xFF & math.floor(x / 192 + 135)

	@classmethod
	def get_y_sector(cls, y: int):
		"""Get a walk packet sector to calculate the character location."""
		return 0xFF & math.floor(y / 192 + 92)

	@staticmethod
	def get_x_absolute(x_sector, x_offset: float, offset_divisor: int = 10) -> int:
		"""Calculate the x-coordinate using the x-sector in movement response packet B021."""
		return int((x_sector - 135) * 192 + (x_offset / offset_divisor))

	@staticmethod
	def get_y_absolute(y_sector, y_offset: float, offset_divisor: int = 10) -> int:
		"""Calculate the y- coordinate using the X sector in movement response packet B021."""
		return int((y_sector - 92) * 192 + (y_offset / offset_divisor))

	def distance_point(self, p):
		""" Calculates the distance between two points."""
		return 0 if self == p else float(
			math.sqrt(
				abs((self.x - p.x) ** 2 + (self.y - p.y) ** 2)))

	def distance_point_absolute(self, x, y):
		""" Calculates the distance between a point and a point in a script."""
		p = self.from_absolute_coords(x, y)
		return self.distance_point(p)

	@property
	def x_offset(self):
		return self._x_offset

	@x_offset.setter
	def x_offset(self, value):
		self._y_offset = value

	@property
	def y_offset(self):
		return self._y_offset

	@y_offset.setter
	def y_offset(self, value):
		self._y_offset = value

	@property
	def x(self) -> int:
		result = self.get_x_absolute(self.x_sector, self._x_offset)
		return result

	@property
	def y(self) -> int:
		result = self.get_y_absolute(self.y_sector, self._y_offset)
		return result

	@property
	def z(self):
		return self._z_offset

	@z.setter
	def z(self, value):
		self._z_offset = value

	def __repr__(self):
		return f"{self.x_sector},{self.y_sector},{self.x},{self.y},{self.z},{self.angle}"

	def __str__(self):
		return f"xsec({self.x_sector}),ysec({self.y_sector}),x({self.x}),y({self.y}),z({self.z}),angle({self.angle})"

	def __eq__(self, other):
		if isinstance(other, Point):
			return self.x_sector == other.x_sector and self.y_sector == other.y_sector \
			       and self.x_offset == other.x_offset \
			       and self.y_offset == other.y_offset \
			       and self.z == other.z
		# elif isinstance(other, PointScript):
		# 	return self._point_script == other
		else:
			return False

	def __hash__(self):
		return hash((self.x, self.y, self.z))

# @staticmethod
# def GetXOffset(x: int) -> Tuple[int, c_byte]:
# 	"""Calculate x offset to send coordinate with 7021 packet.
# 	:return: Tuple of x offset and x sector
# 	"""
# 	x_sector = ScriptUtils.get_sector_x(x)
# 	xOffset: int = ((x - ((x_sector - 135) * 192)) * 10
# 	                if (Bot.char_data.is_in_cave)
# 	                else 0xFFFF & ((x - ((x_sector - 135) * 192)) * 10)
# 	                )
# 	return (xOffset, x_sector)
#
# @staticmethod
# def GetYOffset(y: int) -> Tuple[int, c_byte]:
# 	"""Calculate x offset to send coordinate with 7021 packet.
# 	:return: Tuple of y offset and y sector
# 	"""
# 	y_sector = 0
# 	yOffset = 0
# 	if Bot.char_data.is_in_cave:
# 		y_sector = 0x80
# 		yOffset = (y - ((y_sector - 92) * 192)) * 10
# 	else:
# 		y_sector = ScriptUtils.get_sector_y(y)
# 		yOffset = 0xFFFF & ((y - ((y_sector - 92) * 192)) * 10)
#
# 	return (yOffset, y_sector)
