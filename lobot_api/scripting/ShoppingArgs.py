from lobot_api.statemachine.IState import IStateType
from lobot_api.structs.Monster import Monster


class ShoppingArgs:
	def __init__(self, npc: Monster, shopping_state: IStateType = None):
		if npc is None:
			raise ValueError(Monster.__name__)
		elif shopping_state is None:
			raise ValueError(IStateType.__name__)
		self.npc = npc
		self.i_state_type = shopping_state
