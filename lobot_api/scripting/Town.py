from enum import Enum
from typing import List, Dict, Optional

from lobot_api.scripting.Point import Point
from lobot_api.scripting.ScriptExpression import AbstractExpression, CommandExpression


def walk_script_folder() -> str: return ""  # os.CurrentDomain.BaseDirectory + "Scripts\\"


def town_script_folder() -> str: return ""  # os.CurrentDomain.BaseDirectory + "Townscripts\\"


class TownType(Enum):  # : int
	""" The constant values for towns."""
	none = -1
	Jangan = 0
	Donwang = 1
	Hotan = 2
	Samarkand = 3
	Constantinople = 4
	AlexNorth = 5
	AlexSouth = 6


class Town:
	def __init__(self, n: str, s: Point, p: List[Point], script: List[AbstractExpression]):
		self.name = n
		self.spawn_point = s
		self.polygon = p
		self.town_script = script

		self.teleporter = None
		self.smith = None
		self.armor = None
		self.potions = None
		self.accessories = None
		self.special = None
		self.stable = None

	def __repr__(self):
		return f"{self.name}({self.spawn_point})"

	def __str__(self):
		return f"{self.name}({self.spawn_point})"

	@staticmethod
	def is_point_in_town(town, char_location: Point) -> bool:
		"""
		Test whether point is in polygon.
		Taken from http://www.visibone.com/inpoly/
		:param town: Town to compare
		:param char_location: Point to compare
		:return: True if point is in town
		"""
		p1: Optional[Point] = None
		p2: Optional[Point] = None
		inside: bool = False

		if len(town.polygon) < 3:
			return inside

		old_point: Point = Point.from_absolute(town.polygon[-1].x, town.polygon[-1].y)

		for current_point in town.polygon:
			if current_point.x > old_point.x:
				p1 = old_point
				p2 = current_point
			else:
				p1 = current_point
				p2 = old_point

			if ((current_point.x < char_location.x) is (char_location.x <= old_point.x)
					and (char_location.y - p1.y) * (p2.x - p1.x) < (p2.y - p1.y) * (char_location.x - p1.x)):
				inside = not inside

			old_point = current_point

		return inside


def current_town(coord: Point) -> Optional[Town]:
	"""Check the list of towns to see whether a given point is within any of their radii."""

	for key, value in Towns.items():
		if value.is_point_in_town(value, coord):
			return value
	return None


town_script_folders: Dict[str, str] = {
	"jangan": town_script_folder() + "jangan.txt",
	"donwang": town_script_folder() + "donwang.txt",
	"hotan": town_script_folder() + "hotan.txt",
	"samarkand": town_script_folder() + "samarkand.txt",
	"constantinople": town_script_folder() + "constantinople.txt",
	"alexandria north": town_script_folder() + "alexandria north.txt",
	"alexandria south": town_script_folder() + "alexandria south.txt"
}

TownScriptJangan: List[AbstractExpression] = [CommandExpression("go", ["6427", "1082"]),
                                              CommandExpression("go", ["6420", "1050"]),
                                              CommandExpression("store"),
                                              CommandExpression("go", ["6402", "1026"]),
                                              CommandExpression("go", ["6377", "1020"]),
                                              CommandExpression("stable"),
                                              CommandExpression("go", ["6377", "1048"]),
                                              CommandExpression("go", ["6377", "1078"]),
                                              CommandExpression("go", ["6382", "1093"]),
                                              CommandExpression("repair"),
                                              CommandExpression("go", ["6434", "1090"]),
                                              CommandExpression("go", ["6464", "1089"]),
                                              CommandExpression("go", ["6497", "1085"]),
                                              CommandExpression("potions"),
                                              CommandExpression("special"),
                                              CommandExpression("go", ["6464", "1089"]),
                                              CommandExpression("go", ["6434", "1090"])]

TownScriptDonwang: List[AbstractExpression] = [CommandExpression("go", ["3548", "2070"]),
                                               CommandExpression("go", ["3565", "2030"]),
                                               CommandExpression("go", ["3567", "2005"]),
                                               CommandExpression("store"),
                                               CommandExpression("go", ["3535", "2006"]),
                                               CommandExpression("go", ["3515", "2012"]),
                                               CommandExpression("special"),
                                               CommandExpression("potions"),
                                               CommandExpression("go", ["3545", "2032"]),
                                               CommandExpression("go", ["3558", "2046"]),
                                               CommandExpression("repair"),
                                               CommandExpression("go", ["3565", "2088"]),
                                               CommandExpression("go", ["3583", "2088"]),
                                               CommandExpression("stable"),
                                               CommandExpression("go", ["3550", "2087"])]

TownScriptHotan: List[AbstractExpression] = [CommandExpression("go", ["103", "30"]),
                                             CommandExpression("go", ["95", "60"]),
                                             CommandExpression("store"),
                                             CommandExpression("go", ["80", "95"]),
                                             CommandExpression("potions"),
                                             CommandExpression("go", ["67", "76"]),
                                             CommandExpression("repair"),
                                             CommandExpression("go", ["75", "30"]),
                                             CommandExpression("go", ["81", "10"]),
                                             CommandExpression("special"),
                                             CommandExpression("go", ["110", "10"]),
                                             CommandExpression("go", ["142", "8"]),
                                             CommandExpression("stable"),
                                             CommandExpression("go", ["113", "24"])]

TownScriptSamarkand: List[AbstractExpression] = [CommandExpression("go", ["-5183", "2847"]),
                                                 CommandExpression("go", ["-5141", "2816"]),
                                                 CommandExpression("store"),
                                                 CommandExpression("go", ["-5138", "2875"]),
                                                 CommandExpression("stable"),
                                                 CommandExpression("go", ["-5191", "2938"]),
                                                 CommandExpression("repair"),
                                                 CommandExpression("go", ["-5227", "2878"]),
                                                 CommandExpression("potions"),
                                                 CommandExpression("go", ["-5214", "2844"]),
                                                 CommandExpression("special"),
                                                 CommandExpression("go", ["-5183", "2847"])]

TownScriptConstantinople: List[AbstractExpression] = [CommandExpression("go", ["-10642", "2602"]),
                                                      CommandExpression("go", ["-10631", "2584"]),
                                                      CommandExpression("store"),
                                                      CommandExpression("go", ["-10672", "2534"]),
                                                      CommandExpression("special"),
                                                      CommandExpression("go", ["-10747", "2550"]),
                                                      CommandExpression("stable"),
                                                      CommandExpression("go", ["-10726", "2606"]),
                                                      CommandExpression("go", ["-10681", "2639"]),
                                                      CommandExpression("repair"),
                                                      CommandExpression("go", ["-10630", "2634"]),
                                                      CommandExpression("potions"),
                                                      CommandExpression("go", ["-10642", "2602"])]

TownScriptAlexNorth: List[AbstractExpression] = [CommandExpression("go", ["-16127", "61"]),
                                                 CommandExpression("go", ["-16090", "34"]),
                                                 CommandExpression("store"),
                                                 CommandExpression("go", ["-16133", "58"]),
                                                 CommandExpression("go", ["-16184", "51"]),
                                                 CommandExpression("special"),
                                                 CommandExpression("go", ["-16219", "38"]),
                                                 CommandExpression("potions"),
                                                 CommandExpression("go", ["-16249", "-1"]),
                                                 CommandExpression("repair"),
                                                 CommandExpression("go", ["-16253", "-59"]),
                                                 CommandExpression("go", ["-16261", "-85"]),
                                                 CommandExpression("go", ["-16294", "-90"]),
                                                 CommandExpression("go", ["-16336", "-98"]),
                                                 CommandExpression("go", ["-16368", "-133"]),
                                                 CommandExpression("go", ["-16396", "-171"]),
                                                 CommandExpression("go", ["-16428", "-213"]),
                                                 CommandExpression("stable"),
                                                 CommandExpression("go", ["-16396", "-171"]),
                                                 CommandExpression("go", ["-16368", "-133"]),
                                                 CommandExpression("go", ["-16336", "-98"]),
                                                 CommandExpression("go", ["-16294", "-90"]),
                                                 CommandExpression("go", ["-16261", "-85"]),
                                                 CommandExpression("go", ["-16233", "-78"]),
                                                 CommandExpression("go", ["-16174", "-67"]),
                                                 CommandExpression("go", ["-16126", "-46"]),
                                                 CommandExpression("go", ["-16099", "-14"]),
                                                 CommandExpression("go", ["-16114", "52"])]

TownScriptAlexSouth: List[AbstractExpression] = [
	CommandExpression("go", ["-16615", "-302"]),
	CommandExpression("go", ["-16577", "-339"]),
	CommandExpression("go", ["-16544", "-347"]),
	CommandExpression("go", ["-16513", "-323"]),
	CommandExpression("go", ["-16477", "-283"]),
	CommandExpression("store"),
	CommandExpression("go", ["-16457", "-253"]),
	CommandExpression("go", ["-16431", "-219"]),
	CommandExpression("stable"),
	CommandExpression("go", ["-16457", "-253"]),
	CommandExpression("go", ["-16480", "-271"]),
	CommandExpression("go", ["-16518", "-262"]),
	CommandExpression("go", ["-16560", "-255"]),
	CommandExpression("special"),
	CommandExpression("go", ["-16630", "-248"]),
	CommandExpression("go", ["-16696", "-265"]),
	CommandExpression("go", ["-16717", "-279"]),
	CommandExpression("repair"),
	CommandExpression("go", ["-16670", "-317"]),
	CommandExpression("go", ["-16629", "-349"]),
	CommandExpression("potions"),
	CommandExpression("go", ["-16614", "-302"])
]

"""Dictionary containing "Town", (Coordinates, radius).
The player must be within the radius to be considered in a town"""
Towns: Dict[str, Town] = {
	"jangan": Town("Jangan", Point.from_absolute(6434, 1121),
	               [Point.from_absolute(6500, 953), Point.from_absolute(6500, 1152),
	                Point.from_absolute(6360, 1151), Point.from_absolute(6360, 950)],
	               TownScriptJangan),
	"donwang": Town("Donwang", Point.from_absolute(3549, 2070),
	                [Point.from_absolute(3631, 2161), Point.from_absolute(3464, 2162),
	                 Point.from_absolute(3461, 1940),
	                 Point.from_absolute(3632, 1941)],
	                TownScriptDonwang),
	"hotan": Town("Hotan", Point.from_absolute(113, 47),
	              [Point.from_absolute(159, 151), Point.from_absolute(70, 150), Point.from_absolute(10, 90),
	               Point.from_absolute(10, 5),
	               Point.from_absolute(71, -55), Point.from_absolute(157, -53), Point.from_absolute(216, 5),
	               Point.from_absolute(215, 90)],
	              TownScriptHotan),
	"samarkand": Town("Samarkand", Point.from_absolute(-5183, 2892),
	                  [Point.from_absolute(-5000, 2880), Point.from_absolute(-5001, 2921),
	                   Point.from_absolute(-5168, 3054),
	                   Point.from_absolute(-5204, 3056),
	                   Point.from_absolute(-5366, 2907), Point.from_absolute(-5368, 2877),
	                   Point.from_absolute(-5194, 2704),
	                   Point.from_absolute(-5174, 2705)],
	                  TownScriptSamarkand),
	"constantinople": Town("Constantinople", Point.from_absolute(-10683, 2584),
	                       [Point.from_absolute(-10562, 2682), Point.from_absolute(-10745, 2675),
	                        Point.from_absolute(-10787, 2513),
	                        Point.from_absolute(-10621, 2534)],
	                       TownScriptConstantinople),
	"alexandria north": Town("AlexNorth", Point.from_absolute(-16166, 4),
	                         [Point.from_absolute(-16011, 106), Point.from_absolute(-15990, 66),
	                          Point.from_absolute(-16087, 17),
	                          Point.from_absolute(-16118, -72),
	                          Point.from_absolute(-16223, -100), Point.from_absolute(-16303, -103),
	                          Point.from_absolute(-16351, -134),
	                          Point.from_absolute(-16406, -274),
	                          Point.from_absolute(-16429, -292), Point.from_absolute(-16483, -241),
	                          Point.from_absolute(-16401, -151),
	                          Point.from_absolute(-16349, -53),
	                          Point.from_absolute(-16317, 42), Point.from_absolute(-16258, 40),
	                          Point.from_absolute(-16180, 85),
	                          Point.from_absolute(-16168, 99),
	                          Point.from_absolute(-16121, 94), Point.from_absolute(-16093, 71)],
	                         TownScriptAlexNorth),
	"alexandria south": Town("AlexSouth", Point.from_absolute(-16580, 292),
	                         [Point.from_absolute(-16597, 167), Point.from_absolute(-16649, 166),
	                          Point.from_absolute(-16751, 277),
	                          Point.from_absolute(-16645, 386),
	                          Point.from_absolute(-16591, 346), Point.from_absolute(-16473, 484),
	                          Point.from_absolute(-16438, 447),
	                          Point.from_absolute(-16519, 352),
	                          Point.from_absolute(-16449, 286), Point.from_absolute(-16480, 253)],
	                         TownScriptAlexSouth)
}
