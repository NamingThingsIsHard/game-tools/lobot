# This Python file uses the following encoding: utf-8
import logging
import os
import sys
from pathlib import Path
from typing import List

from PyQt5.QtCore import pyqtSlot
from PyQt5.QtGui import QIntValidator
from PyQt5.QtWidgets import QFileDialog, QMessageBox

from controller.controller import Controller
from lobot_api.Bot import Bot
from lobot_api.BotStatus import BotStatus
from lobot_api.connection import proxy
from lobot_api.connection.Loader import Loader
from lobot_api.globals.NetworkGlobal import NetworkGlobal
from lobot_api.globals.settings.LoginSettings import LoginSettings
from lobot_api.logger.Logger import PK2, SETUP, NETWORK
from lobot_api.packets.Handling import Handling
from lobot_api.packets.char.CharDataHandler import CharDataHandler
from lobot_api.packets.login.CharSelectExecuteAction import CharSelectionExecuteAction
from lobot_api.packets.login.GatewayShardListRequest import GatewayShardListRequest
from lobot_api.packets.login.Logout import Logout
from lobot_api.packets.login.LogoutSuccessHandler import LogoutSuccessHandler
from lobot_api.packets.login.SelectCharacter import SelectCharacter
from lobot_api.pk2.PK2Utils import PK2Utils
from lobot_api.statemachine.FiniteStateMachine import FiniteStateMachine


class LoginController(Controller):

	@pyqtSlot(str)
	def on_div_changed(self, s):
		if s and s != NetworkGlobal.instance().gateway_server_division:
			NetworkGlobal.instance().gateway_server_division = s

	# @pyqtSlot(str)
	# def on_div_added(self, s):
	# 	if s and s not in [self._view.divisionServerComboBox.itemText(i) for i in range(self._view.divisionServerComboBox.count())]:
	# 		self._view.divisionServerComboBox.addItem(s)

	@pyqtSlot(str)
	def on_char_changed(self, s):
		if s:
			pass  # LoginSettings.instance().char_name = s

	@pyqtSlot(str)
	def on_username_changed(self, s):
		if s:
			LoginSettings.instance().username = s
			t = self._view.usernameLineEdit.text()
			b = Bot.get_status() == BotStatus.Login
			# allow login depending on whether username and password are entered
			self._view.loginPushButton.setEnabled(b and len(t) > 0)

	@pyqtSlot(str)
	def on_password_changed(self, s):
		if s:
			LoginSettings.instance().password = s
			t = self._view.passwordLineEdit.text()
			b = Bot.get_status() == BotStatus.Login
			# allow login depending on whether username and password are entered
			self._view.loginPushButton.setEnabled(b and len(t) > 0)

	def on_select_char_pressed(self):
		char = self._view.charsComboBox.currentText()
		if proxy.joymax:
			proxy.joymax.send_packet(SelectCharacter(char).create_request())

	def on_login_pressed(self):
		proxy.send_login()

	def on_logout_pressed(self):
		if proxy.joymax:
			proxy.send_logout()
		else:
			proxy.disconnect()
			self.handle_logged_out([])

	@pyqtSlot(int)
	def on_autoselect_changed(self, i):
		LoginSettings.instance().auto_select = bool(i)

	@pyqtSlot(str)
	def on_autoselectChar_changed(self, s):
		if s:
			LoginSettings.instance().char_name = s

	@pyqtSlot(int)
	def on_reconnect_changed(self, i):
		LoginSettings.instance().auto_connect = bool(i)

	@pyqtSlot(str)
	def on_reconnectX_changed(self, i):
		if i.isnumeric():
			LoginSettings.instance().auto_connect_seconds = i

	@pyqtSlot(int)
	def on_actionLogin_changed(self, i):
		LoginSettings.instance().action_on_login = i

	@pyqtSlot(int)
	def on_clientless_changed(self, i):
		LoginSettings.instance().client_or_clientless = bool(i) and Bot.get_status() == BotStatus.Disconnected

	def try_refresh_client(self):
		client_path = self._view.clientPathLineEdit.text()
		if client_path:
			qm = QMessageBox()
			ret = qm.question(self._view, '', "Are you sure to refresh all client info?", qm.Yes | qm.No)
			if ret == qm.Yes:
				self.try_client_choice(client_path, refresh_client=True)

	def try_client_choice(self, client_path: str, refresh_client: bool = False):
		"""Attempt to initialize pk2 based on a list of chosen files.

		:param client_path: sro_client.exe chosen for silkroad version
		:param refresh_client: Whether to refresh client.
		:return: bool of whether the file was valid or not
		"""
		# Prevent endless loop upon initializing
		if client_path == LoginSettings.instance().login_client_directory_path == self._view.clientPathLineEdit.text()\
				and not refresh_client:
			return False

		path = os.path.dirname(client_path) if os.path.isfile(client_path) else os.path.normpath(client_path)

		# make sure both client and pk2 are in directory
		if PK2Utils.exist_media_pk2_and_client(path):
			self._view.clientPathLineEdit.setText(path)
			LoginSettings.instance().login_client_directory_path = path
			# initialize server values and update GUI with them
			Controller.thread_pool.start(lambda: PK2Utils.setup(refresh_client))

			if refresh_client:
				self._view.saveSettingsPushButton.click()

			self._view.serverAddressLineEdit.setText(NetworkGlobal.instance().gateway_ip)
			self._view.refreshClientToolButton.setEnabled(True)
			# self._view.refreshWineToolButton.setEnabled(True)

			return True
		else:
			if not path:
				logging.log(PK2,
				            f"sro_client path({LoginSettings.instance().login_client_directory_path}) does not contain \"media.pk2\" and \"sro_client.exe\"")
			return False

	def choose_client(self):
		"""clientPath slot
		:return: None
		"""
		home = str(Path.home())
		file = QFileDialog.getOpenFileName(self._view, self._view.tr("Select sro_client.exe"), home,
		                                   self._view.tr("exe files (sro_client.exe)"))

		if file[0]:
			if self._view.clientPathLineEdit.text() and PK2Utils.is_initialized:
				self.try_refresh_client()
			else:
				self.try_client_choice(file[0])

	def try_wine_choice(self, file: List[str]):
		"""Check for valid wine binary file

		:param file: wine binary for loading with linux
		:return: bool of whether the file was valid or not
		"""
		path = os.path.dirname(file[0])

		if not path:
			return False

		files = os.listdir(path)
		lib_dir = Path(path).parent / "lib64"
		kernel32 = ''
		libwine = ''
		ws2_32 = ''

		for root, dirs, names in os.walk(lib_dir):
			if "kernel32.dll.so" in names:
				kernel32 = os.path.join(root, "kernel32.dll.so")

			if "libwine.so.1" in names:
				libwine = os.path.join(root, "libwine.so.1")

			if "ws2_32.dll.so" in names:
				ws2_32 = os.path.join(root, "ws2_32.dll.so")

			if kernel32 and libwine and ws2_32:
				break

		# make sure wine binary and dll's are available
		if "wine" in files and kernel32 and ws2_32 and libwine:
			self._view.winePathLineEdit.setText(file[0])
			Loader.wine_file_path = file[0]
			Loader.k32_file_path = kernel32
			Loader.ws2_32_file_path = ws2_32
			Loader.libwine_path = libwine

		else:
			logging.log(SETUP, f"sro_client path({path}) does not contain \"wine\" binary file\n")

	def choose_wine(self):
		"""clientPath slot
		:return: None
		"""
		home = str(Path.home())
		file = QFileDialog.getOpenFileName(self._view, self._view.tr("Select wine binary"), home,
		                                   self._view.tr("wine binary (wine wine64)"))
		self.try_wine_choice(file)

	def connect_client(self):
		"""
		Try to connect to the client using the Loader class.
		The client directory must be set. If not on windows, the wine path must be set too.
		:return: None
		:raises: Exception if loader runs into a problem
		"""
		if LoginSettings.instance().client_or_clientless:
			proxy.connect_clientless()
		else:
			if LoginSettings.instance().login_client_directory_path and (
					sys.platform.startswith("win_32") or Loader.wine_file_path):
				try:
					Loader.instance().start_client(LoginSettings.instance().login_client_directory_path,
					                              NetworkGlobal.instance().gateway_port, LoginSettings.instance().locale)
				except Exception:
					logging.log(NETWORK, "Error connecting to client", exc_info=sys.exc_info())

	def start_proxy(self):
		"""Start proxy loop in its own thread to minimize gui freezing."""
		Controller.thread_pool.start(proxy.main)

	def handle_server_division_changed(self):
		self._view.divisionServerComboBox.clear()
		self._view.divisionServerComboBox.addItem(NetworkGlobal.instance().gateway_server_division)

	def handle_login_screen(self, *args):
		self._view.clientlessComboBox.setEnabled(False)
		self._view.connectClientPushButton.setEnabled(False)
		if self._view.usernameLineEdit.text() and self._view.passwordLineEdit.text():
			self._view.loginPushButton.setEnabled(True)

	def handle_char_select_reached(self, *args):
		self._view.loginPushButton.setEnabled(False)

		if LoginSettings.instance().characters_available:
			self._view.charsComboBox.clear()
			self._view.charsComboBox.addItems(LoginSettings.instance().characters_available)
			self._view.selectCharPushButton.setEnabled(True)

			items = [self._view.charsComboBox.itemText(i) for i in range(self._view.charsComboBox.count())]
			try:
				index = items.index(LoginSettings.instance().char_name)
				self._view.charsComboBox.setCurrentIndex(index)
			except ValueError:
				pass


	def handle_loading_game(self, *args):
		self._view.charsComboBox.setEnabled(False)
		self._view.selectCharPushButton.setEnabled(False)

	def handle_in_game(self, *args):
		self._view.logoutPushButton.setEnabled(True)

	def handle_logged_out(self, *args):
		proxy.disconnect()
		self._view.charsComboBox.clear()
		self._view.charsComboBox.setEnabled(True)
		self._view.logoutPushButton.setEnabled(False)
		self._view.clientlessComboBox.setEnabled(True)
		if self._view.clientlessComboBox.currentIndex() == 1:
			self._view.connectClientPushButton.setEnabled(True)

	def debug(self):
		"""
		Use to simulate game interactions.
		:return: None
		"""
		# from silkroad_security_api_1_4.Packet import Packet
		# QThreadPool.globalInstance().start(PK2Utils.ExtractAllInformation())
		# p = Packet(0xb034,encrypted=False,massive=False,buffer=[1, 8, 5, 0, 1, 19, 2, 0, 0, 0, 0, 0])
		# p = Packet(0x3013,encrypted=False,massive=False,buffer=[212, 190, 88, 21, 132, 7, 0, 0, 68, 33, 33, 13, 239, 13, 0, 0, 0, 0, 0, 10, 0, 0, 0, 73, 136, 38, 0, 0, 0, 0, 0, 165, 122, 7, 0, 0, 0, 1, 0, 0, 0, 0, 127, 6, 0, 0, 205, 12, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 59, 38, 0, 0, 0, 0, 0, 159, 21, 0, 0, 0, 0, 28, 208, 14, 0, 0, 0, 0, 50, 0, 0, 0, 7, 131, 0, 0, 0, 10, 0, 0, 0, 56, 0, 0, 0, 2, 0, 0, 0, 155, 0, 0, 0, 30, 0, 0, 0, 143, 0, 0, 0, 10, 0, 0, 0, 62, 0, 0, 0, 1, 0, 0, 0, 14, 0, 0, 0, 2, 0, 0, 0, 8, 0, 0, 0, 2, 0, 0, 0, 1, 0, 2, 0, 1, 0, 0, 0, 0, 231, 21, 0, 0, 0, 224, 128, 211, 14, 0, 0, 0, 0, 51, 0, 0, 0, 5, 131, 0, 0, 0, 10, 0, 0, 0, 155, 0, 0, 0, 50, 0, 0, 0, 14, 0, 0, 0, 3, 0, 0, 0, 8, 0, 0, 0, 2, 0, 0, 0, 143, 0, 0, 0, 100, 0, 0, 0, 1, 0, 2, 0, 2, 0, 0, 0, 0, 195, 21, 0, 0, 0, 224, 28, 112, 14, 0, 0, 0, 0, 49, 0, 0, 0, 3, 131, 0, 0, 0, 20, 0, 0, 0, 8, 0, 0, 0, 3, 0, 0, 0, 14, 0, 0, 0, 2, 0, 0, 0, 1, 0, 2, 0, 3, 0, 0, 0, 0, 50, 22, 0, 0, 0, 224, 156, 6, 26, 0, 0, 0, 0, 52, 0, 0, 0, 1, 132, 0, 0, 0, 30, 0, 0, 0, 1, 0, 2, 0, 4, 0, 0, 0, 0, 11, 22, 0, 0, 0, 224, 52, 112, 14, 0, 0, 0, 0, 48, 0, 0, 0, 4, 155, 0, 0, 0, 50, 0, 0, 0, 131, 0, 0, 0, 30, 0, 0, 0, 14, 0, 0, 0, 3, 0, 0, 0, 8, 0, 0, 0, 2, 0, 0, 0, 1, 0, 2, 0, 5, 0, 0, 0, 0, 83, 22, 0, 0, 0, 160, 29, 112, 0, 0, 0, 0, 0, 48, 0, 0, 0, 4, 131, 0, 0, 0, 30, 0, 0, 0, 62, 0, 0, 0, 1, 0, 0, 0, 56, 0, 0, 0, 1, 0, 0, 0, 8, 0, 0, 0, 1, 0, 0, 0, 1, 0, 2, 0, 6, 0, 0, 0, 0, 2, 16, 0, 0, 3, 0, 128, 57, 65, 3, 0, 0, 0, 108, 0, 0, 0, 5, 84, 0, 0, 0, 100, 0, 0, 0, 96, 0, 0, 0, 10, 0, 0, 0, 57, 0, 0, 0, 2, 0, 0, 0, 108, 0, 0, 0, 60, 0, 0, 0, 9, 0, 0, 0, 3, 0, 0, 0, 1, 0, 2, 0, 9, 0, 0, 0, 0, 155, 22, 0, 0, 0, 231, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 32, 0, 0, 0, 3, 0, 0, 0, 38, 0, 0, 0, 1, 0, 0, 0, 26, 0, 0, 0, 5, 0, 0, 0, 44, 0, 0, 0, 15, 0, 0, 0, 8, 0, 0, 0, 3, 0, 0, 0, 1, 0, 2, 0, 10, 0, 0, 0, 0, 191, 22, 0, 0, 0, 231, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 26, 0, 0, 0, 15, 0, 0, 0, 44, 0, 0, 0, 10, 0, 0, 0, 32, 0, 0, 0, 5, 0, 0, 0, 14, 0, 0, 0, 2, 0, 0, 0, 8, 0, 0, 0, 2, 0, 0, 0, 1, 0, 2, 0, 11, 0, 0, 0, 0, 122, 22, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 2, 0, 12, 0, 0, 0, 0, 122, 22, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 2, 0, 13, 0, 0, 0, 0, 119, 96, 0, 0, 3, 0, 14, 0, 0, 0, 0, 104, 94, 0, 0, 3, 0, 15, 0, 0, 0, 0, 109, 94, 0, 0, 1, 0, 16, 0, 0, 0, 0, 150, 8, 0, 0, 9, 0, 17, 0, 0, 0, 0, 61, 0, 0, 0, 10, 0, 18, 0, 0, 0, 0, 6, 0, 0, 0, 124, 1, 19, 0, 0, 0, 0, 13, 0, 0, 0, 44, 1, 20, 0, 0, 0, 0, 109, 94, 0, 0, 20, 0, 21, 0, 0, 0, 0, 19, 0, 0, 0, 47, 0, 22, 0, 0, 0, 0, 103, 94, 0, 0, 3, 0, 23, 0, 0, 0, 0, 12, 0, 0, 0, 3, 0, 24, 0, 0, 0, 0, 13, 0, 0, 0, 244, 1, 25, 0, 0, 0, 0, 56, 0, 0, 0, 111, 0, 26, 0, 0, 0, 0, 103, 94, 0, 0, 3, 0, 27, 0, 0, 0, 0, 98, 14, 0, 0, 1, 0, 28, 0, 0, 0, 0, 236, 90, 0, 0, 2, 13, 91, 0, 0, 0, 0, 219, 220, 36, 0, 1, 5, 142, 89, 0, 0, 190, 96, 25, 23, 49, 83, 67, 66, 1, 29, 0, 0, 0, 0, 99, 26, 0, 0, 2, 0, 0, 30, 0, 0, 0, 0, 97, 14, 0, 0, 3, 0, 31, 0, 0, 0, 0, 111, 26, 0, 0, 2, 0, 0, 32, 0, 0, 0, 0, 95, 27, 0, 0, 2, 0, 0, 33, 0, 0, 0, 0, 59, 27, 0, 0, 1, 0, 0, 36, 0, 0, 0, 0, 71, 27, 0, 0, 2, 0, 0, 38, 0, 0, 0, 0, 123, 26, 0, 0, 1, 0, 0, 41, 0, 0, 0, 0, 95, 14, 0, 0, 2, 0, 53, 0, 0, 0, 0, 20, 0, 0, 0, 11, 0, 54, 0, 0, 0, 0, 118, 43, 0, 0, 0, 35, 176, 128, 138, 0, 0, 0, 0, 54, 0, 0, 0, 0, 1, 0, 2, 0, 55, 0, 0, 0, 0, 175, 51, 0, 0, 0, 0, 20, 225, 6, 0, 0, 0, 0, 60, 0, 0, 0, 0, 1, 0, 2, 0, 5, 3, 0, 0, 0, 0, 0, 78, 163, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 252, 0, 0, 0, 5, 0, 0, 0, 248, 0, 0, 0, 5, 0, 0, 0, 110, 1, 0, 0, 1, 0, 0, 0, 111, 1, 0, 0, 1, 0, 0, 0, 1, 0, 2, 0, 1, 0, 0, 0, 0, 80, 163, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 248, 0, 0, 0, 5, 0, 0, 0, 1, 0, 2, 0, 2, 0, 0, 0, 0, 241, 94, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 249, 0, 0, 0, 5, 0, 0, 0, 253, 0, 0, 0, 5, 0, 0, 0, 1, 0, 2, 0, 0, 1, 1, 1, 0, 0, 0, 1, 2, 1, 0, 0, 33, 1, 3, 1, 0, 0, 0, 1, 17, 1, 0, 0, 33, 1, 18, 1, 0, 0, 0, 1, 19, 1, 0, 0, 33, 1, 20, 1, 0, 0, 0, 2, 0, 1, 48, 0, 0, 0, 1, 1, 131, 0, 0, 0, 1, 1, 196, 2, 0, 0, 1, 1, 199, 2, 0, 0, 1, 1, 220, 2, 0, 0, 1, 1, 224, 2, 0, 0, 1, 1, 253, 2, 0, 0, 1, 1, 21, 3, 0, 0, 1, 1, 44, 3, 0, 0, 1, 1, 150, 3, 0, 0, 1, 1, 167, 3, 0, 0, 1, 1, 108, 4, 0, 0, 1, 1, 110, 4, 0, 0, 1, 1, 143, 4, 0, 0, 1, 1, 212, 4, 0, 0, 1, 1, 97, 5, 0, 0, 1, 1, 101, 5, 0, 0, 1, 1, 118, 5, 0, 0, 1, 1, 119, 5, 0, 0, 1, 1, 130, 5, 0, 0, 1, 1, 142, 5, 0, 0, 1, 1, 161, 5, 0, 0, 1, 1, 169, 5, 0, 0, 1, 1, 221, 5, 0, 0, 1, 1, 160, 31, 0, 0, 1, 2, 12, 0, 1, 0, 0, 0, 2, 0, 0, 0, 220, 0, 0, 0, 221, 0, 0, 0, 129, 1, 0, 0, 130, 1, 0, 0, 131, 1, 0, 0, 132, 1, 0, 0, 133, 1, 0, 0, 134, 1, 0, 0, 136, 1, 0, 0, 141, 1, 0, 0, 1, 135, 1, 0, 0, 16, 0, 24, 8, 1, 1, 1, 22, 0, 83, 78, 95, 67, 79, 78, 95, 81, 69, 86, 95, 65, 76, 76, 95, 66, 65, 83, 73, 67, 95, 55, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 149, 9, 63, 0, 109, 107, 0, 0, 96, 67, 0, 0, 52, 67, 0, 0, 20, 67, 198, 5, 0, 1, 0, 198, 5, 0, 0, 0, 0, 154, 153, 153, 65, 1, 0, 112, 66, 0, 0, 200, 66, 0, 5, 0, 76, 111, 98, 111, 116, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 255, 215, 27, 161, 7, 0, 0, 0, 0, 154, 38, 0, 0, 0, 7, 14, 0, 73, 169, 5, 0, 0, 1, 73, 101, 5, 0, 0, 2, 73, 21, 3, 0, 0, 3, 73, 48, 0, 0, 0, 4, 73, 199, 2, 0, 0, 5, 73, 44, 3, 0, 0, 38, 70, 5, 0, 0, 0, 39, 70, 6, 0, 0, 0, 40, 70, 12, 0, 0, 0, 41, 73, 150, 3, 0, 0, 42, 73, 119, 5, 0, 0, 43, 73, 131, 0, 0, 0, 44, 73, 220, 2, 0, 0, 45, 73, 160, 31, 0, 0, 72, 188, 73, 178, 74, 128, 138, 0, 1, 0, 1, 0, 0])
		# p.lock()
		# MoveItem._parse_packet_(p)
		# CharDataHandler._parse_packet_(p)

	def _load_settings(self):
		if self.try_client_choice(LoginSettings.instance().login_client_directory_path):
			# if NetworkGlobal.instance().GatewayServerDivision:
			# 	self._view.divisionServerComboBox.setCurrentText(str(NetworkGlobal.instance().GatewayServerDivision))
			self._view.serverAddressLineEdit.setText(NetworkGlobal.instance().gateway_ip)
			self._view.serverPortLineEdit.setText(str(NetworkGlobal.instance().gateway_port))
			self._view.localeLineEdit.setText(str(LoginSettings.instance().locale))
		self._view.usernameLineEdit.setText(LoginSettings.instance().username)
		self._view.passwordLineEdit.setText(LoginSettings.instance().password)

		self._view.autoselectCharCheckBox.setChecked(LoginSettings.instance().auto_select)
		self._view.autoselectCharLineEdit.setText(LoginSettings.instance().char_name)

		self._view.reconnectAfterCheckBox.setChecked(LoginSettings.instance().auto_connect)
		self._view.reconnectAfterLineEdit.setText(str(LoginSettings.instance().auto_connect_seconds))

		self._view.actionLogincomboBox.setCurrentIndex(LoginSettings.instance().action_on_login)

		self._view.clientlessComboBox.setCurrentIndex(LoginSettings.instance().client_or_clientless)
		# NetworkGlobal.instance().GatewayIP = "127.0.0.1"

	def handle_bot_status_changed(self):
		if Bot.get_status().value < Bot.get_status().Idle.value:
			self._view.startBotPushButton.setEnabled(False)
		else:
			self._view.startBotPushButton.setEnabled(True)

		if Bot.get_status() == BotStatus.Disconnected:
			self._view.startBotPushButton.setText("Disconnected")
		if Bot.get_status() == BotStatus.Login:
			self._view.startBotPushButton.setText("Logging in")
		if Bot.get_status() == BotStatus.CharacterSelect:
			self._view.startBotPushButton.setText("Char Select")
		if Bot.get_status() == BotStatus.Loading:
			self._view.startBotPushButton.setText("Loading")
		if Bot.get_status() == BotStatus.Idle:
			self._view.startBotPushButton.setText("Start Bot")
		elif Bot.get_status() == BotStatus.Botting:
			self._view.startBotPushButton.setText("Stop Bot")

	def toggle_bot(self):
		if Bot.get_status() == BotStatus.Botting:
			FiniteStateMachine.instance().stop()
			self._view.startBotPushButton.setText("Start Bot")
		elif Bot.get_status() == BotStatus.Idle:
			FiniteStateMachine.instance().start()
			self._view.startBotPushButton.setText("Stop Bot")

	def _connect_signals(self):
		self._view.startBotPushButton.setEnabled(False)
		self._view.startBotPushButton.clicked.connect(self.toggle_bot)
		Bot.on_bot_status_changed += self.handle_bot_status_changed

		# Wine is unnecessary in windows
		if sys.platform.startswith("win_32"):
			self._view.winePathLineEdit.hide()
			self._view.winePathToolButton.hide()
		else:
			self._view.winePathToolButton.clicked.connect(self.choose_wine)

		self._view.clientPathToolButton.clicked.connect(self.choose_client)
		self._view.refreshClientToolButton.clicked.connect(self.try_refresh_client)
		# self._view.clientPathLineEdit.textChanged.connect(self.try_client_choice)
		LoginSettings.instance().sig_loginClientDirectoryPath.connect(self._view.clientPathLineEdit.setText)

		NetworkGlobal.instance().sig_serverGWIP.connect(self._view.serverAddressLineEdit.setText)
		NetworkGlobal.instance().sig_serverGWPort.connect(self._view.serverPortLineEdit.setText)
		NetworkGlobal.instance().sig_serverDivision.connect(
			self.handle_server_division_changed)  # self.on_div_added)
		self._view.divisionServerComboBox.currentTextChanged.connect(self.on_div_changed)

		LoginSettings.instance().sig_locale.connect(self._view.localeLineEdit.setText)
		self._view.connectClientPushButton.clicked.connect(self.connect_client)

		LoginSettings.instance().sig_username.connect(self._view.usernameLineEdit.setText)
		self._view.usernameLineEdit.textChanged.connect(self.on_username_changed)

		LoginSettings.instance().sig_password.connect(self._view.passwordLineEdit.setText)
		self._view.passwordLineEdit.textChanged.connect(self.on_password_changed)

		self._view.loginPushButton.clicked.connect(self.on_login_pressed)
		self._view.selectCharPushButton.clicked.connect(self.on_select_char_pressed)
		self._view.logoutPushButton.clicked.connect(self.on_logout_pressed)

		LoginSettings.instance().sig_charName.connect(self._view.charsComboBox.setCurrentText)
		self._view.charsComboBox.currentTextChanged.connect(self.on_char_changed)

		LoginSettings.instance().sig_autoSelect.connect(self._view.autoselectCharCheckBox.setChecked)
		self._view.autoselectCharCheckBox.stateChanged.connect(self.on_autoselect_changed)
		LoginSettings.instance().sig_charName.connect(self._view.autoselectCharLineEdit.setText)
		self._view.autoselectCharLineEdit.textChanged.connect(self.on_autoselectChar_changed)

		LoginSettings.instance().sig_autoconnect.connect(self._view.reconnectAfterCheckBox.setChecked)
		self._view.reconnectAfterCheckBox.stateChanged.connect(self.on_reconnect_changed)
		self._view.reconnectAfterLineEdit.setValidator(QIntValidator(0, 100, self._view))
		LoginSettings.instance().sig_autoconnectX.connect(self._view.reconnectAfterLineEdit.setText)
		self._view.reconnectAfterLineEdit.textChanged.connect(self.on_reconnectX_changed)

		LoginSettings.instance().sig_actionOnLogin.connect(self._view.actionLogincomboBox.setCurrentIndex)
		self._view.actionLogincomboBox.currentIndexChanged.connect(self.on_actionLogin_changed)

		GatewayShardListRequest.on_packet += self.handle_login_screen
		CharSelectionExecuteAction.on_packet += self.handle_char_select_reached
		SelectCharacter.on_packet += self.handle_loading_game
		CharDataHandler.on_packet += self.handle_in_game
		LogoutSuccessHandler.on_packet += self.handle_logged_out
		Logout.on_packet += self.handle_logged_out

		self._view.clientlessComboBox.setCurrentIndex(1)
		# TODO don't enable loader until implemented
		LoginSettings.instance().sig_clientOrClientless.connect(self._view.connectClientPushButton.setEnabled)
		self._view.clientlessComboBox.currentIndexChanged.connect(self.on_clientless_changed)

		# Set login button to correct text first
		self.handle_bot_status_changed()
		Handling.init_handling_functions()
		self.debug()
		# self.connect_client()

		self.start_proxy()
