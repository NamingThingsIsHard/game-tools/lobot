from typing import List

from PyQt5.QtWidgets import QCheckBox, QGridLayout

from controller.controller import Controller
from lobot_api.binding import LobotProperty
from lobot_api.globals.settings.AlchemyFilterSettings import AlchemyFilterSettings
from lobot_api.globals.settings.ItemFilterSettings import ItemFilterSettings
from lobot_api.structs.DegreeFilter import DegreeFilter


class ItemController(Controller):

	def change_bool(self, item_property: LobotProperty, value):
		"""
		Change bool in ItemSettings using property.
		:param item_property: Property pointer
		:param value: new value
		:return: None
		"""
		item_property.value = value == 1

	def change_item(self, item_property: LobotProperty, option_index, value):
		"""
		Change ItemFilter in ItemSettings using property.
		:param item_property: Property pointer
		:param option_index: value to be changed
		:param value: new value
		:return: None
		"""
		filter_str = list(item_property.value)
		filter_str[option_index] = str(int(value))
		item_property.value = ''.join(filter_str)

	def change_degree(self, l_property: LobotProperty, degree, value):
		"""
		Change DegreeFilter in ItemSettings using property.
		:param l_property: Property pointer
		:param degree: degree to be changed
		:param value: new value
		:return: None
		"""
		d = DegreeFilter(int(l_property.value, 2))
		d.set_flag(degree, int(value))
		if l_property.value != str(d):
			l_property.value = str(d)

	def handle_item_filter_changed(self, q_object: QCheckBox, value: int):
		"""
		Change UI checkbox using string.
		:param q_object:
		:param value: New filter value.
		:return: None
		"""
		q_object.setChecked(value)

	def toggle_checkbox_column(self, grid: QGridLayout, column_index: int):
		"""
		Toggle all check boxes in a QGridLayout column.
		If any box is unchecked, this checks all boxes.
		If no box is unchecked, this unchecks all boxes.
		:param grid:
		:param column_index:
		:return:
		"""
		any_unchecked = False
		all_check_boxes: List[QCheckBox] = []
		for row in range(1, grid.rowCount()):
			item = grid.itemAtPosition(row, column_index)

			# make sure QLayoutItem exists and check for widget type
			if item:
				item = item.widget()
			if isinstance(item, QCheckBox):
				check_box: QCheckBox = item
				all_check_boxes.append(check_box)
				if not check_box.isChecked():
					any_unchecked = True

		for cb in all_check_boxes:
			if any_unchecked:
				cb.setChecked(True)
			else:
				cb.setChecked(False)

	def add_item_filters(self):
		self._view.hpPotPickCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.HPPots, 0, self._view.hpPotPickCheckBox.isChecked()))
		self._view.hpPotStoreCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.HPPots, 1, self._view.hpPotStoreCheckBox.isChecked()))
		self._view.hpPotSellCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.HPPots, 2, self._view.hpPotSellCheckBox.isChecked()))
		self._view.hpPotDropCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.HPPots, 3, self._view.hpPotDropCheckBox.isChecked()))

		self._view.mpPotPickCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.MPPots, 0, self._view.mpPotPickCheckBox.isChecked()))
		self._view.mpPotStoreCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.MPPots, 1, self._view.mpPotStoreCheckBox.isChecked()))
		self._view.mpPotSellCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.MPPots, 2, self._view.mpPotSellCheckBox.isChecked()))
		self._view.mpPotDropCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.MPPots, 3, self._view.mpPotDropCheckBox.isChecked()))

		self._view.vigorPickCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.VigorPots, 0, self._view.vigorPickCheckBox.isChecked()))
		self._view.vigorStoreCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.VigorPots, 1, self._view.vigorStoreCheckBox.isChecked()))
		self._view.vigorSellCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.VigorPots, 2, self._view.vigorSellCheckBox.isChecked()))
		self._view.vigorDropCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.VigorPots, 3, self._view.vigorDropCheckBox.isChecked()))

		self._view.grainsPickCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.Grains, 0, self._view.grainsPickCheckBox.isChecked()))
		self._view.grainsStoreCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.Grains, 1, self._view.grainsStoreCheckBox.isChecked()))
		self._view.grainsSellCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.Grains, 2, self._view.grainsSellCheckBox.isChecked()))
		self._view.grainsDropCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.Grains, 3, self._view.grainsDropCheckBox.isChecked()))

		self._view.uniPillPickCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.UniversalPills, 0, self._view.uniPillPickCheckBox.isChecked()))
		self._view.uniPillStoreCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.UniversalPills, 1, self._view.uniPillStoreCheckBox.isChecked()))
		self._view.uniPillSellCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.UniversalPills, 2, self._view.uniPillSellCheckBox.isChecked()))
		self._view.uniPillDropCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.UniversalPills, 3, self._view.uniPillDropCheckBox.isChecked()))

		self._view.weaponElixirPickCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.WeaponElixir, 0, self._view.weaponElixirPickCheckBox.isChecked()))
		self._view.weaponElixirStoreCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.WeaponElixir, 1, self._view.weaponElixirStoreCheckBox.isChecked()))
		self._view.weaponElixirSellCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.WeaponElixir, 2, self._view.weaponElixirSellCheckBox.isChecked()))
		self._view.weaponElixirDropCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.WeaponElixir, 3, self._view.weaponElixirDropCheckBox.isChecked()))

		self._view.protectorElixirPickCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.ProtectorElixir, 0, self._view.protectorElixirPickCheckBox.isChecked()))
		self._view.protectorElixirStoreCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.ProtectorElixir, 1, self._view.protectorElixirStoreCheckBox.isChecked()))
		self._view.protectorElixirSellCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.ProtectorElixir, 2, self._view.protectorElixirSellCheckBox.isChecked()))
		self._view.protectorElixirDropCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.ProtectorElixir, 3, self._view.protectorElixirDropCheckBox.isChecked()))

		self._view.shieldElixirPickCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.ShieldElixir, 0, self._view.shieldElixirPickCheckBox.isChecked()))
		self._view.shieldElixirStoreCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.ShieldElixir, 1, self._view.shieldElixirStoreCheckBox.isChecked()))
		self._view.shieldElixirSellCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.ShieldElixir, 2, self._view.shieldElixirSellCheckBox.isChecked()))
		self._view.shieldElixirDropCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.ShieldElixir, 3, self._view.shieldElixirDropCheckBox.isChecked()))

		self._view.accElixirPickCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.AccessoryElixir, 0, self._view.accElixirPickCheckBox.isChecked()))
		self._view.accElixirStoreCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.AccessoryElixir, 1, self._view.accElixirStoreCheckBox.isChecked()))
		self._view.accElixirSellCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.AccessoryElixir, 2, self._view.accElixirSellCheckBox.isChecked()))
		self._view.accElixirDropCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.AccessoryElixir, 3, self._view.accElixirDropCheckBox.isChecked()))

		self._view.alchemyMaterialPickCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.AlchemyMaterial, 0, self._view.alchemyMaterialPickCheckBox.isChecked()))
		self._view.alchemyMaterialStoreCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.AlchemyMaterial, 1, self._view.alchemyMaterialStoreCheckBox.isChecked()))
		self._view.alchemyMaterialSellCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.AlchemyMaterial, 2, self._view.alchemyMaterialSellCheckBox.isChecked()))
		self._view.alchemyMaterialDropCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.AlchemyMaterial, 3, self._view.alchemyMaterialDropCheckBox.isChecked()))

		self._view.returnScrollsPickCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.ReturnScrolls, 0, self._view.returnScrollsPickCheckBox.isChecked()))
		self._view.returnScrollsStoreCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.ReturnScrolls, 1, self._view.returnScrollsStoreCheckBox.isChecked()))
		self._view.returnScrollsSellCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.ReturnScrolls, 2, self._view.returnScrollsSellCheckBox.isChecked()))
		self._view.returnScrollsDropCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.ReturnScrolls, 3, self._view.returnScrollsDropCheckBox.isChecked()))


		self._view.arrowsPickCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.Arrows, 0, self._view.arrowsPickCheckBox.isChecked()))
		self._view.arrowsStoreCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.Arrows, 1, self._view.arrowsStoreCheckBox.isChecked()))
		self._view.arrowsSellCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.Arrows, 2, self._view.arrowsSellCheckBox.isChecked()))
		self._view.arrowsDropCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.Arrows, 3, self._view.arrowsDropCheckBox.isChecked()))

		self._view.boltsPickCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.Bolts, 0, self._view.boltsPickCheckBox.isChecked()))
		self._view.boltsStoreCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.Bolts, 1, self._view.boltsStoreCheckBox.isChecked()))
		self._view.boltsSellCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.Bolts, 2, self._view.boltsSellCheckBox.isChecked()))
		self._view.boltsDropCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.Bolts, 3, self._view.boltsDropCheckBox.isChecked()))

		self._view.cosItemsPickCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.COSItems, 0, self._view.cosItemsPickCheckBox.isChecked()))
		self._view.cosItemsStoreCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.COSItems, 1, self._view.cosItemsStoreCheckBox.isChecked()))
		self._view.cosItemsSellCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.COSItems, 2, self._view.cosItemsSellCheckBox.isChecked()))
		self._view.cosItemsDropCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.COSItems, 3, self._view.cosItemsDropCheckBox.isChecked()))

		self._view.questEventItemsPickCheckBox.stateChanged.connect(lambda: self.change_bool(ItemFilterSettings.QuestItems, self._view.questEventItemsPickCheckBox.isChecked()))
		self._view.goldPickCheckBox.stateChanged.connect(lambda: self.change_bool(ItemFilterSettings.Gold, self._view.goldPickCheckBox.isChecked()))
		self._view.freeItemsPickCheckBox.stateChanged.connect(lambda: self.change_bool(ItemFilterSettings.FreeItems, self._view.freeItemsPickCheckBox.isChecked()))
		self._view.ownItemsPickCheckBox.stateChanged.connect(lambda: self.change_bool(ItemFilterSettings.OwnItems, self._view.ownItemsPickCheckBox.isChecked()))

	def add_item_filter_slots(self):
		ItemFilterSettings.HPPots.signal.connect(lambda x: self._view.hpPotPickCheckBox.setChecked(x[0] == '1'))
		ItemFilterSettings.HPPots.signal.connect(lambda x: self._view.hpPotStoreCheckBox.setChecked(x[1] == '1'))
		ItemFilterSettings.HPPots.signal.connect(lambda x: self._view.hpPotSellCheckBox.setChecked(x[2] == '1'))
		ItemFilterSettings.HPPots.signal.connect(lambda x: self._view.hpPotDropCheckBox.setChecked(x[3] == '1'))

		ItemFilterSettings.MPPots.signal.connect(lambda x: self._view.mpPotPickCheckBox.setChecked(x[0] == '1'))
		ItemFilterSettings.MPPots.signal.connect(lambda x: self._view.mpPotStoreCheckBox.setChecked(x[1] == '1'))
		ItemFilterSettings.MPPots.signal.connect(lambda x: self._view.mpPotSellCheckBox.setChecked(x[2] == '1'))
		ItemFilterSettings.MPPots.signal.connect(lambda x: self._view.mpPotDropCheckBox.setChecked(x[3] == '1'))

		ItemFilterSettings.VigorPots.signal.connect(lambda x: self._view.vigorPickCheckBox.setChecked(x[0] == '1'))
		ItemFilterSettings.VigorPots.signal.connect(lambda x: self._view.vigorStoreCheckBox.setChecked(x[1] == '1'))
		ItemFilterSettings.VigorPots.signal.connect(lambda x: self._view.vigorSellCheckBox.setChecked(x[2] == '1'))
		ItemFilterSettings.VigorPots.signal.connect(lambda x: self._view.vigorDropCheckBox.setChecked(x[3] == '1'))

		ItemFilterSettings.Grains.signal.connect(lambda x: self._view.grainsPickCheckBox.setChecked(x[0] == '1'))
		ItemFilterSettings.Grains.signal.connect(lambda x: self._view.grainsStoreCheckBox.setChecked(x[1] == '1'))
		ItemFilterSettings.Grains.signal.connect(lambda x: self._view.grainsSellCheckBox.setChecked(x[2] == '1'))
		ItemFilterSettings.Grains.signal.connect(lambda x: self._view.grainsDropCheckBox.setChecked(x[3] == '1'))

		ItemFilterSettings.UniversalPills.signal.connect(lambda x: self._view.uniPillPickCheckBox.setChecked(x[0] == '1'))
		ItemFilterSettings.UniversalPills.signal.connect(lambda x: self._view.uniPillStoreCheckBox.setChecked(x[1] == '1'))
		ItemFilterSettings.UniversalPills.signal.connect(lambda x: self._view.uniPillSellCheckBox.setChecked(x[2] == '1'))
		ItemFilterSettings.UniversalPills.signal.connect(lambda x: self._view.uniPillDropCheckBox.setChecked(x[3] == '1'))

		ItemFilterSettings.WeaponElixir.signal.connect(lambda x: self._view.weaponElixirPickCheckBox.setChecked(x[0] == '1'))
		ItemFilterSettings.WeaponElixir.signal.connect(lambda x: self._view.weaponElixirStoreCheckBox.setChecked(x[1] == '1'))
		ItemFilterSettings.WeaponElixir.signal.connect(lambda x: self._view.weaponElixirSellCheckBox.setChecked(x[2] == '1'))
		ItemFilterSettings.WeaponElixir.signal.connect(lambda x: self._view.weaponElixirDropCheckBox.setChecked(x[3] == '1'))

		ItemFilterSettings.ProtectorElixir.signal.connect(lambda x: self._view.protectorElixirPickCheckBox.setChecked(x[0] == '1'))
		ItemFilterSettings.ProtectorElixir.signal.connect(lambda x: self._view.protectorElixirStoreCheckBox.setChecked(x[1] == '1'))
		ItemFilterSettings.ProtectorElixir.signal.connect(lambda x: self._view.protectorElixirSellCheckBox.setChecked(x[2] == '1'))
		ItemFilterSettings.ProtectorElixir.signal.connect(lambda x: self._view.protectorElixirDropCheckBox.setChecked(x[3] == '1'))

		ItemFilterSettings.ShieldElixir.signal.connect(lambda x: self._view.shieldElixirPickCheckBox.setChecked(x[0] == '1'))
		ItemFilterSettings.ShieldElixir.signal.connect(lambda x: self._view.shieldElixirStoreCheckBox.setChecked(x[1] == '1'))
		ItemFilterSettings.ShieldElixir.signal.connect(lambda x: self._view.shieldElixirSellCheckBox.setChecked(x[2] == '1'))
		ItemFilterSettings.ShieldElixir.signal.connect(lambda x: self._view.shieldElixirDropCheckBox.setChecked(x[3] == '1'))

		ItemFilterSettings.AccessoryElixir.signal.connect(lambda x: self._view.accElixirPickCheckBox.setChecked(x[0] == '1'))
		ItemFilterSettings.AccessoryElixir.signal.connect(lambda x: self._view.accElixirStoreCheckBox.setChecked(x[1] == '1'))
		ItemFilterSettings.AccessoryElixir.signal.connect(lambda x: self._view.accElixirSellCheckBox.setChecked(x[2] == '1'))
		ItemFilterSettings.AccessoryElixir.signal.connect(lambda x: self._view.accElixirDropCheckBox.setChecked(x[3] == '1'))

		ItemFilterSettings.AlchemyMaterial.signal.connect(lambda x: self._view.alchemyMaterialPickCheckBox.setChecked(x[0] == '1'))
		ItemFilterSettings.AlchemyMaterial.signal.connect(lambda x: self._view.alchemyMaterialStoreCheckBox.setChecked(x[1] == '1'))
		ItemFilterSettings.AlchemyMaterial.signal.connect(lambda x: self._view.alchemyMaterialSellCheckBox.setChecked(x[2] == '1'))
		ItemFilterSettings.AlchemyMaterial.signal.connect(lambda x: self._view.alchemyMaterialDropCheckBox.setChecked(x[3] == '1'))

		ItemFilterSettings.ReturnScrolls.signal.connect(lambda x: self._view.returnScrollsPickCheckBox.setChecked(x[0] == '1'))
		ItemFilterSettings.ReturnScrolls.signal.connect(lambda x: self._view.returnScrollsStoreCheckBox.setChecked(x[1] == '1'))
		ItemFilterSettings.ReturnScrolls.signal.connect(lambda x: self._view.returnScrollsSellCheckBox.setChecked(x[2] == '1'))
		ItemFilterSettings.ReturnScrolls.signal.connect(lambda x: self._view.returnScrollsDropCheckBox.setChecked(x[3] == '1'))


		ItemFilterSettings.Arrows.signal.connect(lambda x: self._view.arrowsPickCheckBox.setChecked(x[0] == '1'))
		ItemFilterSettings.Arrows.signal.connect(lambda x: self._view.arrowsStoreCheckBox.setChecked(x[1] == '1'))
		ItemFilterSettings.Arrows.signal.connect(lambda x: self._view.arrowsSellCheckBox.setChecked(x[2] == '1'))
		ItemFilterSettings.Arrows.signal.connect(lambda x: self._view.arrowsDropCheckBox.setChecked(x[3] == '1'))

		ItemFilterSettings.Bolts.signal.connect(lambda x: self._view.boltsPickCheckBox.setChecked(x[0] == '1'))
		ItemFilterSettings.Bolts.signal.connect(lambda x: self._view.boltsStoreCheckBox.setChecked(x[1] == '1'))
		ItemFilterSettings.Bolts.signal.connect(lambda x: self._view.boltsSellCheckBox.setChecked(x[2] == '1'))
		ItemFilterSettings.Bolts.signal.connect(lambda x: self._view.boltsDropCheckBox.setChecked(x[3] == '1'))

		ItemFilterSettings.COSItems.signal.connect(lambda x: self._view.cosItemsPickCheckBox.setChecked(x[0] == '1'))
		ItemFilterSettings.COSItems.signal.connect(lambda x: self._view.cosItemsPickCheckBox.setChecked(x[1] == '1'))
		ItemFilterSettings.COSItems.signal.connect(lambda x: self._view.cosItemsPickCheckBox.setChecked(x[2] == '1'))
		ItemFilterSettings.COSItems.signal.connect(lambda x: self._view.cosItemsPickCheckBox.setChecked(x[3] == '1'))

		ItemFilterSettings.QuestItems.signal.connect(lambda x: self._view.questEventItemsPickCheckBox.setChecked(bool(x)))
		ItemFilterSettings.Gold.signal.connect(lambda x: self._view.goldPickCheckBox.setChecked(bool(x)))
		ItemFilterSettings.FreeItems.signal.connect(lambda x: self._view.freeItemsPickCheckBox.setChecked(bool(x)))
		ItemFilterSettings.OwnItems.signal.connect(lambda x: self._view.ownItemsPickCheckBox.setChecked(bool(x)))

	def add_jade_item_filters(self):
		self._view.strPickCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterJadeStrength, 0, self._view.strPickCheckBox.isChecked()))
		self._view.strStoreCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterJadeStrength, 1, self._view.strStoreCheckBox.isChecked()))
		self._view.strSellCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterJadeStrength, 2, self._view.strSellCheckBox.isChecked()))
		self._view.strDropCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterJadeStrength, 3, self._view.strDropCheckBox.isChecked()))

		self._view.intPickCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterJadeIntelligence, 0, self._view.intPickCheckBox.isChecked()))
		self._view.intStoreCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterJadeIntelligence, 1, self._view.intStoreCheckBox.isChecked()))
		self._view.intSellCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterJadeIntelligence, 2, self._view.intSellCheckBox.isChecked()))
		self._view.intDropCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterJadeIntelligence, 3, self._view.intDropCheckBox.isChecked()))

		self._view.masterPickCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterJadeMaster, 0, self._view.masterPickCheckBox.isChecked()))
		self._view.masterStoreCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterJadeMaster, 1, self._view.masterStoreCheckBox.isChecked()))
		self._view.masterSellCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterJadeMaster, 2, self._view.masterSellCheckBox.isChecked()))
		self._view.masterDropCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterJadeMaster, 3, self._view.masterDropCheckBox.isChecked()))

		self._view.strikesPickCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterJadeStrikes, 0, self._view.strikesPickCheckBox.isChecked()))
		self._view.strikesStoreCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterJadeStrikes, 1, self._view.strikesStoreCheckBox.isChecked()))
		self._view.strikesSellCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterJadeStrikes, 2, self._view.strikesSellCheckBox.isChecked()))
		self._view.strikesDropCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterJadeStrikes, 3, self._view.strikesDropCheckBox.isChecked()))

		self._view.disciplinePickCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterJadeDiscipline, 0, self._view.disciplinePickCheckBox.isChecked()))
		self._view.disciplineStoreCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterJadeDiscipline, 1, self._view.disciplineStoreCheckBox.isChecked()))
		self._view.disciplineSellCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterJadeDiscipline, 2, self._view.disciplineSellCheckBox.isChecked()))
		self._view.disciplineDropCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterJadeDiscipline, 3, self._view.disciplineDropCheckBox.isChecked()))

		self._view.penetrationPickCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterJadePenetration, 0, self._view.penetrationPickCheckBox.isChecked()))
		self._view.penetrationStoreCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterJadePenetration, 1, self._view.penetrationStoreCheckBox.isChecked()))
		self._view.penetrationSellCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterJadePenetration, 2, self._view.penetrationSellCheckBox.isChecked()))
		self._view.penetrationDropCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterJadePenetration, 3, self._view.penetrationDropCheckBox.isChecked()))

		self._view.dodgePickCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterJadeDodging, 0, self._view.dodgePickCheckBox.isChecked()))
		self._view.dodgeStoreCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterJadeDodging, 1, self._view.dodgeStoreCheckBox.isChecked()))
		self._view.dodgeSellCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterJadeDodging, 2, self._view.dodgeSellCheckBox.isChecked()))
		self._view.dodgeDropCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterJadeDodging, 3, self._view.dodgeDropCheckBox.isChecked()))

		self._view.staminaPickCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterJadeStamina, 0, self._view.staminaPickCheckBox.isChecked()))
		self._view.staminaStoreCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterJadeStamina, 1, self._view.staminaStoreCheckBox.isChecked()))
		self._view.staminaSellCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterJadeStamina, 2, self._view.staminaSellCheckBox.isChecked()))
		self._view.staminaDropCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterJadeStamina, 3, self._view.staminaDropCheckBox.isChecked()))

		self._view.magicPickCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterJadeMagic, 0, self._view.magicPickCheckBox.isChecked()))
		self._view.magicStoreCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterJadeMagic, 1, self._view.magicStoreCheckBox.isChecked()))
		self._view.magicSellCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterJadeMagic, 2, self._view.magicSellCheckBox.isChecked()))
		self._view.magicDropCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterJadeMagic, 3, self._view.magicDropCheckBox.isChecked()))

		self._view.fogsPickCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterJadeFogs, 0, self._view.fogsPickCheckBox.isChecked()))
		self._view.fogsStoreCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterJadeFogs, 1, self._view.fogsStoreCheckBox.isChecked()))
		self._view.fogsSellCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterJadeFogs, 2, self._view.fogsSellCheckBox.isChecked()))
		self._view.fogsDropCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterJadeFogs, 3, self._view.fogsDropCheckBox.isChecked()))

		self._view.airPickCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterJadeAir, 0, self._view.airPickCheckBox.isChecked()))
		self._view.airStoreCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterJadeAir, 1, self._view.airStoreCheckBox.isChecked()))
		self._view.airSellCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterJadeAir, 2, self._view.airSellCheckBox.isChecked()))
		self._view.airDropCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterJadeAir, 3, self._view.airDropCheckBox.isChecked()))

		self._view.firePickCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterJadeFire, 0, self._view.firePickCheckBox.isChecked()))
		self._view.fireStoreCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterJadeFire, 1, self._view.fireStoreCheckBox.isChecked()))
		self._view.fireSellCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterJadeFire, 2, self._view.fireSellCheckBox.isChecked()))
		self._view.fireDropCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterJadeFire, 3, self._view.fireDropCheckBox.isChecked()))

		self._view.immunityPickCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterJadeImmunity, 0, self._view.immunityPickCheckBox.isChecked()))
		self._view.immunityStoreCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterJadeImmunity, 1, self._view.immunityStoreCheckBox.isChecked()))
		self._view.immunitySellCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterJadeImmunity, 2, self._view.immunitySellCheckBox.isChecked()))
		self._view.immunityDropCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterJadeImmunity, 3, self._view.immunityDropCheckBox.isChecked()))

		self._view.revivalPickCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterJadeRevival, 0, self._view.revivalPickCheckBox.isChecked()))
		self._view.revivalStoreCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterJadeRevival, 1, self._view.revivalStoreCheckBox.isChecked()))
		self._view.revivalSellCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterJadeRevival, 2, self._view.revivalSellCheckBox.isChecked()))
		self._view.revivalDropCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterJadeRevival, 3, self._view.revivalDropCheckBox.isChecked()))

		self._view.steadyPickCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterJadeSteady, 0, self._view.steadyPickCheckBox.isChecked()))
		self._view.steadyStoreCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterJadeSteady, 1, self._view.steadyStoreCheckBox.isChecked()))
		self._view.steadySellCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterJadeSteady, 2, self._view.steadySellCheckBox.isChecked()))
		self._view.steadyDropCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterJadeSteady, 3, self._view.steadyDropCheckBox.isChecked()))

		self._view.luckyPickCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterJadeLuck, 0, self._view.luckyPickCheckBox.isChecked()))
		self._view.luckyStoreCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterJadeLuck, 1, self._view.luckyStoreCheckBox.isChecked()))
		self._view.luckySellCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterJadeLuck, 2, self._view.luckySellCheckBox.isChecked()))
		self._view.luckyDropCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterJadeLuck, 3, self._view.luckyDropCheckBox.isChecked()))

	def add_jade_item_filter_slots(self):
		AlchemyFilterSettings.FilterJadeStrength.signal.connect(lambda x: self._view.strPickCheckBox.setChecked(x[0] == '1'))
		AlchemyFilterSettings.FilterJadeStrength.signal.connect(lambda x: self._view.strStoreCheckBox.setChecked(x[1] == '1'))
		AlchemyFilterSettings.FilterJadeStrength.signal.connect(lambda x: self._view.strSellCheckBox.setChecked(x[2] == '1'))
		AlchemyFilterSettings.FilterJadeStrength.signal.connect(lambda x: self._view.strDropCheckBox.setChecked(x[3] == '1'))

		AlchemyFilterSettings.FilterJadeIntelligence.signal.connect(lambda x: self._view.intPickCheckBox.setChecked(x[0] == '1'))
		AlchemyFilterSettings.FilterJadeIntelligence.signal.connect(lambda x: self._view.intStoreCheckBox.setChecked(x[1] == '1'))
		AlchemyFilterSettings.FilterJadeIntelligence.signal.connect(lambda x: self._view.intSellCheckBox.setChecked(x[2] == '1'))
		AlchemyFilterSettings.FilterJadeIntelligence.signal.connect(lambda x: self._view.intDropCheckBox.setChecked(x[3] == '1'))

		AlchemyFilterSettings.FilterJadeMaster.signal.connect(lambda x: self._view.masterPickCheckBox.setChecked(x[0] == '1'))
		AlchemyFilterSettings.FilterJadeMaster.signal.connect(lambda x: self._view.masterStoreCheckBox.setChecked(x[1] == '1'))
		AlchemyFilterSettings.FilterJadeMaster.signal.connect(lambda x: self._view.masterSellCheckBox.setChecked(x[2] == '1'))
		AlchemyFilterSettings.FilterJadeMaster.signal.connect(lambda x: self._view.masterDropCheckBox.setChecked(x[3] == '1'))

		AlchemyFilterSettings.FilterJadeStrikes.signal.connect(lambda x: self._view.strikesPickCheckBox.setChecked(x[0] == '1'))
		AlchemyFilterSettings.FilterJadeStrikes.signal.connect(lambda x: self._view.strikesStoreCheckBox.setChecked(x[1] == '1'))
		AlchemyFilterSettings.FilterJadeStrikes.signal.connect(lambda x: self._view.strikesSellCheckBox.setChecked(x[2] == '1'))
		AlchemyFilterSettings.FilterJadeStrikes.signal.connect(lambda x: self._view.strikesDropCheckBox.setChecked(x[3] == '1'))

		AlchemyFilterSettings.FilterJadeDiscipline.signal.connect(lambda x: self._view.disciplinePickCheckBox.setChecked(x[0] == '1'))
		AlchemyFilterSettings.FilterJadeDiscipline.signal.connect(lambda x: self._view.disciplineStoreCheckBox.setChecked(x[1] == '1'))
		AlchemyFilterSettings.FilterJadeDiscipline.signal.connect(lambda x: self._view.disciplineSellCheckBox.setChecked(x[2] == '1'))
		AlchemyFilterSettings.FilterJadeDiscipline.signal.connect(lambda x: self._view.disciplineDropCheckBox.setChecked(x[3] == '1'))

		AlchemyFilterSettings.FilterJadePenetration.signal.connect(lambda x: self._view.penetrationPickCheckBox.setChecked(x[0] == '1'))
		AlchemyFilterSettings.FilterJadePenetration.signal.connect(lambda x: self._view.penetrationStoreCheckBox.setChecked(x[1] == '1'))
		AlchemyFilterSettings.FilterJadePenetration.signal.connect(lambda x: self._view.penetrationSellCheckBox.setChecked(x[2] == '1'))
		AlchemyFilterSettings.FilterJadePenetration.signal.connect(lambda x: self._view.penetrationDropCheckBox.setChecked(x[3] == '1'))

		AlchemyFilterSettings.FilterJadeDodging.signal.connect(lambda x: self._view.dodgePickCheckBox.setChecked(x[0] == '1'))
		AlchemyFilterSettings.FilterJadeDodging.signal.connect(lambda x: self._view.dodgeStoreCheckBox.setChecked(x[1] == '1'))
		AlchemyFilterSettings.FilterJadeDodging.signal.connect(lambda x: self._view.dodgeSellCheckBox.setChecked(x[2] == '1'))
		AlchemyFilterSettings.FilterJadeDodging.signal.connect(lambda x: self._view.dodgeDropCheckBox.setChecked(x[3] == '1'))

		AlchemyFilterSettings.FilterJadeStamina.signal.connect(lambda x: self._view.staminaPickCheckBox.setChecked(x[0] == '1'))
		AlchemyFilterSettings.FilterJadeStamina.signal.connect(lambda x: self._view.staminaStoreCheckBox.setChecked(x[1] == '1'))
		AlchemyFilterSettings.FilterJadeStamina.signal.connect(lambda x: self._view.staminaSellCheckBox.setChecked(x[2] == '1'))
		AlchemyFilterSettings.FilterJadeStamina.signal.connect(lambda x: self._view.staminaDropCheckBox.setChecked(x[3] == '1'))

		AlchemyFilterSettings.FilterJadeMagic.signal.connect(lambda x: self._view.magicPickCheckBox.setChecked(x[0] == '1'))
		AlchemyFilterSettings.FilterJadeMagic.signal.connect(lambda x: self._view.magicStoreCheckBox.setChecked(x[1] == '1'))
		AlchemyFilterSettings.FilterJadeMagic.signal.connect(lambda x: self._view.magicSellCheckBox.setChecked(x[2] == '1'))
		AlchemyFilterSettings.FilterJadeMagic.signal.connect(lambda x: self._view.magicDropCheckBox.setChecked(x[3] == '1'))

		AlchemyFilterSettings.FilterJadeFogs.signal.connect(lambda x: self._view.fogsPickCheckBox.setChecked(x[0] == '1'))
		AlchemyFilterSettings.FilterJadeFogs.signal.connect(lambda x: self._view.fogsStoreCheckBox.setChecked(x[1] == '1'))
		AlchemyFilterSettings.FilterJadeFogs.signal.connect(lambda x: self._view.fogsSellCheckBox.setChecked(x[2] == '1'))
		AlchemyFilterSettings.FilterJadeFogs.signal.connect(lambda x: self._view.fogsDropCheckBox.setChecked(x[3] == '1'))

		AlchemyFilterSettings.FilterJadeAir.signal.connect(lambda x: self._view.airPickCheckBox.setChecked(x[0] == '1'))
		AlchemyFilterSettings.FilterJadeAir.signal.connect(lambda x: self._view.airStoreCheckBox.setChecked(x[1] == '1'))
		AlchemyFilterSettings.FilterJadeAir.signal.connect(lambda x: self._view.airSellCheckBox.setChecked(x[2] == '1'))
		AlchemyFilterSettings.FilterJadeAir.signal.connect(lambda x: self._view.airDropCheckBox.setChecked(x[3] == '1'))

		AlchemyFilterSettings.FilterJadeFire.signal.connect(lambda x: self._view.firePickCheckBox.setChecked(x[0] == '1'))
		AlchemyFilterSettings.FilterJadeFire.signal.connect(lambda x: self._view.fireStoreCheckBox.setChecked(x[1] == '1'))
		AlchemyFilterSettings.FilterJadeFire.signal.connect(lambda x: self._view.fireSellCheckBox.setChecked(x[2] == '1'))
		AlchemyFilterSettings.FilterJadeFire.signal.connect(lambda x: self._view.fireDropCheckBox.setChecked(x[3] == '1'))

		AlchemyFilterSettings.FilterJadeImmunity.signal.connect(lambda x: self._view.immunityPickCheckBox.setChecked(x[0] == '1'))
		AlchemyFilterSettings.FilterJadeImmunity.signal.connect(lambda x: self._view.immunityStoreCheckBox.setChecked(x[1] == '1'))
		AlchemyFilterSettings.FilterJadeImmunity.signal.connect(lambda x: self._view.immunitySellCheckBox.setChecked(x[2] == '1'))
		AlchemyFilterSettings.FilterJadeImmunity.signal.connect(lambda x: self._view.immunityDropCheckBox.setChecked(x[3] == '1'))

		AlchemyFilterSettings.FilterJadeRevival.signal.connect(lambda x: self._view.revivalPickCheckBox.setChecked(x[0] == '1'))
		AlchemyFilterSettings.FilterJadeRevival.signal.connect(lambda x: self._view.revivalStoreCheckBox.setChecked(x[1] == '1'))
		AlchemyFilterSettings.FilterJadeRevival.signal.connect(lambda x: self._view.revivalSellCheckBox.setChecked(x[2] == '1'))
		AlchemyFilterSettings.FilterJadeRevival.signal.connect(lambda x: self._view.revivalDropCheckBox.setChecked(x[3] == '1'))

		AlchemyFilterSettings.FilterJadeSteady.signal.connect(lambda x: self._view.steadyPickCheckBox.setChecked(x[0] == '1'))
		AlchemyFilterSettings.FilterJadeSteady.signal.connect(lambda x: self._view.steadyStoreCheckBox.setChecked(x[1] == '1'))
		AlchemyFilterSettings.FilterJadeSteady.signal.connect(lambda x: self._view.steadySellCheckBox.setChecked(x[2] == '1'))
		AlchemyFilterSettings.FilterJadeSteady.signal.connect(lambda x: self._view.steadyDropCheckBox.setChecked(x[3] == '1'))

		AlchemyFilterSettings.FilterJadeLuck.signal.connect(lambda x: self._view.luckyPickCheckBox.setChecked(x[0] == '1'))
		AlchemyFilterSettings.FilterJadeLuck.signal.connect(lambda x: self._view.luckyStoreCheckBox.setChecked(x[1] == '1'))
		AlchemyFilterSettings.FilterJadeLuck.signal.connect(lambda x: self._view.luckySellCheckBox.setChecked(x[2] == '1'))
		AlchemyFilterSettings.FilterJadeLuck.signal.connect(lambda x: self._view.luckyDropCheckBox.setChecked(x[3] == '1'))

	def add_jade_tablet_filters(self):
		self._view.strPickTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.JadeTabletStrength, 0, self._view.strPickTabletCheckBox.isChecked()))
		self._view.strStoreTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.JadeTabletStrength, 1, self._view.strStoreTabletCheckBox.isChecked()))
		self._view.strSellTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.JadeTabletStrength, 2, self._view.strSellTabletCheckBox.isChecked()))
		self._view.strDropTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.JadeTabletStrength, 3, self._view.strDropTabletCheckBox.isChecked()))

		self._view.intPickTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.JadeTabletIntelligence, 0, self._view.intPickTabletCheckBox.isChecked()))
		self._view.intStoreTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.JadeTabletIntelligence, 1, self._view.intStoreTabletCheckBox.isChecked()))
		self._view.intSellTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.JadeTabletIntelligence, 2, self._view.intSellTabletCheckBox.isChecked()))
		self._view.intDropTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.JadeTabletIntelligence, 3, self._view.intDropTabletCheckBox.isChecked()))

		self._view.masterPickTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.JadeTabletMaster, 0, self._view.masterPickTabletCheckBox.isChecked()))
		self._view.masterStoreTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.JadeTabletMaster, 1, self._view.masterStoreTabletCheckBox.isChecked()))
		self._view.masterSellTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.JadeTabletMaster, 2, self._view.masterSellTabletCheckBox.isChecked()))
		self._view.masterDropTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.JadeTabletMaster, 3, self._view.masterDropTabletCheckBox.isChecked()))

		self._view.strikesPickTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.JadeTabletStrikes, 0, self._view.strikesPickTabletCheckBox.isChecked()))
		self._view.strikesStoreTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.JadeTabletStrikes, 1, self._view.strikesStoreTabletCheckBox.isChecked()))
		self._view.strikesSellTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.JadeTabletStrikes, 2, self._view.strikesSellTabletCheckBox.isChecked()))
		self._view.strikesDropTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.JadeTabletStrikes, 3, self._view.strikesDropTabletCheckBox.isChecked()))

		self._view.disciplinePickTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.JadeTabletDiscipline, 0, self._view.disciplinePickTabletCheckBox.isChecked()))
		self._view.disciplineStoreTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.JadeTabletDiscipline, 1, self._view.disciplineStoreTabletCheckBox.isChecked()))
		self._view.disciplineSellTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.JadeTabletDiscipline, 2, self._view.disciplineSellTabletCheckBox.isChecked()))
		self._view.disciplineDropTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.JadeTabletDiscipline, 3, self._view.disciplineDropTabletCheckBox.isChecked()))

		self._view.penetrationPickTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.JadeTabletPenetration, 0, self._view.penetrationPickTabletCheckBox.isChecked()))
		self._view.penetrationStoreTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.JadeTabletPenetration, 1, self._view.penetrationStoreTabletCheckBox.isChecked()))
		self._view.penetrationSellTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.JadeTabletPenetration, 2, self._view.penetrationSellTabletCheckBox.isChecked()))
		self._view.penetrationDropTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.JadeTabletPenetration, 3, self._view.penetrationDropTabletCheckBox.isChecked()))

		self._view.dodgePickTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.JadeTabletDodging, 0, self._view.dodgePickTabletCheckBox.isChecked()))
		self._view.dodgeStoreTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.JadeTabletDodging, 1, self._view.dodgeStoreTabletCheckBox.isChecked()))
		self._view.dodgeSellTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.JadeTabletDodging, 2, self._view.dodgeSellTabletCheckBox.isChecked()))
		self._view.dodgeDropTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.JadeTabletDodging, 3, self._view.dodgeDropTabletCheckBox.isChecked()))

		self._view.staminaPickTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.JadeTabletStamina, 0, self._view.staminaPickTabletCheckBox.isChecked()))
		self._view.staminaStoreTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.JadeTabletStamina, 1, self._view.staminaStoreTabletCheckBox.isChecked()))
		self._view.staminaSellTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.JadeTabletStamina, 2, self._view.staminaSellTabletCheckBox.isChecked()))
		self._view.staminaDropTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.JadeTabletStamina, 3, self._view.staminaDropTabletCheckBox.isChecked()))

		self._view.magicPickTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.JadeTabletMagic, 0, self._view.magicPickTabletCheckBox.isChecked()))
		self._view.magicStoreTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.JadeTabletMagic, 1, self._view.magicStoreTabletCheckBox.isChecked()))
		self._view.magicSellTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.JadeTabletMagic, 2, self._view.magicSellTabletCheckBox.isChecked()))
		self._view.magicDropTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.JadeTabletMagic, 3, self._view.magicDropTabletCheckBox.isChecked()))

		self._view.fogsPickTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.JadeTabletFogs, 0, self._view.fogsPickTabletCheckBox.isChecked()))
		self._view.fogsStoreTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.JadeTabletFogs, 1, self._view.fogsStoreTabletCheckBox.isChecked()))
		self._view.fogsSellTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.JadeTabletFogs, 2, self._view.fogsSellTabletCheckBox.isChecked()))
		self._view.fogsDropTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.JadeTabletFogs, 3, self._view.fogsDropTabletCheckBox.isChecked()))

		self._view.airPickTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.JadeTabletAir, 0, self._view.airPickTabletCheckBox.isChecked()))
		self._view.airStoreTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.JadeTabletAir, 1, self._view.airStoreTabletCheckBox.isChecked()))
		self._view.airSellTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.JadeTabletAir, 2, self._view.airSellTabletCheckBox.isChecked()))
		self._view.airDropTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.JadeTabletAir, 3, self._view.airDropTabletCheckBox.isChecked()))

		self._view.firePickTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.JadeTabletFire, 0, self._view.firePickTabletCheckBox.isChecked()))
		self._view.fireStoreTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.JadeTabletFire, 1, self._view.fireStoreTabletCheckBox.isChecked()))
		self._view.fireSellTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.JadeTabletFire, 2, self._view.fireSellTabletCheckBox.isChecked()))
		self._view.fireDropTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.JadeTabletFire, 3, self._view.fireDropTabletCheckBox.isChecked()))

		self._view.immunityPickTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.JadeTabletImmunity, 0, self._view.immunityPickTabletCheckBox.isChecked()))
		self._view.immunityStoreTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.JadeTabletImmunity, 1, self._view.immunityStoreTabletCheckBox.isChecked()))
		self._view.immunitySellTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.JadeTabletImmunity, 2, self._view.immunitySellTabletCheckBox.isChecked()))
		self._view.immunityDropTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.JadeTabletImmunity, 3, self._view.immunityDropTabletCheckBox.isChecked()))

		self._view.revivalPickTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.JadeTabletRevival, 0, self._view.revivalPickTabletCheckBox.isChecked()))
		self._view.revivalStoreTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.JadeTabletRevival, 1, self._view.revivalStoreTabletCheckBox.isChecked()))
		self._view.revivalSellTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.JadeTabletRevival, 2, self._view.revivalSellTabletCheckBox.isChecked()))
		self._view.revivalDropTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.JadeTabletRevival, 3, self._view.revivalDropTabletCheckBox.isChecked()))

		self._view.steadyPickTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.JadeTabletSteady, 0, self._view.steadyPickTabletCheckBox.isChecked()))
		self._view.steadyStoreTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.JadeTabletSteady, 1, self._view.steadyStoreTabletCheckBox.isChecked()))
		self._view.steadySellTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.JadeTabletSteady, 2, self._view.steadySellTabletCheckBox.isChecked()))
		self._view.steadyDropTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.JadeTabletSteady, 3, self._view.steadyDropTabletCheckBox.isChecked()))

		self._view.luckyPickTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.JadeTabletLuck, 0, self._view.luckyPickTabletCheckBox.isChecked()))
		self._view.luckyStoreTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.JadeTabletLuck, 1, self._view.luckyStoreTabletCheckBox.isChecked()))
		self._view.luckySellTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.JadeTabletLuck, 2, self._view.luckySellTabletCheckBox.isChecked()))
		self._view.luckyDropTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.JadeTabletLuck, 3, self._view.luckyDropTabletCheckBox.isChecked()))

	def add_jade_tablet_filter_slots(self):
		AlchemyFilterSettings.JadeTabletStrength.signal.connect(lambda x: self._view.strPickTabletCheckBox.setChecked(x[0] == '1'))
		AlchemyFilterSettings.JadeTabletStrength.signal.connect(lambda x: self._view.strStoreTabletCheckBox.setChecked(x[1] == '1'))
		AlchemyFilterSettings.JadeTabletStrength.signal.connect(lambda x: self._view.strSellTabletCheckBox.setChecked(x[2] == '1'))
		AlchemyFilterSettings.JadeTabletStrength.signal.connect(lambda x: self._view.strDropTabletCheckBox.setChecked(x[3] == '1'))

		AlchemyFilterSettings.JadeTabletIntelligence.signal.connect(lambda x: self._view.intPickTabletCheckBox.setChecked(x[0] == '1'))
		AlchemyFilterSettings.JadeTabletIntelligence.signal.connect(lambda x: self._view.intStoreTabletCheckBox.setChecked(x[1] == '1'))
		AlchemyFilterSettings.JadeTabletIntelligence.signal.connect(lambda x: self._view.intSellTabletCheckBox.setChecked(x[2] == '1'))
		AlchemyFilterSettings.JadeTabletIntelligence.signal.connect(lambda x: self._view.intDropTabletCheckBox.setChecked(x[3] == '1'))

		AlchemyFilterSettings.JadeTabletMaster.signal.connect(lambda x: self._view.masterPickTabletCheckBox.setChecked(x[0] == '1'))
		AlchemyFilterSettings.JadeTabletMaster.signal.connect(lambda x: self._view.masterStoreTabletCheckBox.setChecked(x[1] == '1'))
		AlchemyFilterSettings.JadeTabletMaster.signal.connect(lambda x: self._view.masterSellTabletCheckBox.setChecked(x[2] == '1'))
		AlchemyFilterSettings.JadeTabletMaster.signal.connect(lambda x: self._view.masterDropTabletCheckBox.setChecked(x[3] == '1'))

		AlchemyFilterSettings.JadeTabletStrikes.signal.connect(lambda x: self._view.strikesPickTabletCheckBox.setChecked(x[0] == '1'))
		AlchemyFilterSettings.JadeTabletStrikes.signal.connect(lambda x: self._view.strikesStoreTabletCheckBox.setChecked(x[1] == '1'))
		AlchemyFilterSettings.JadeTabletStrikes.signal.connect(lambda x: self._view.strikesSellTabletCheckBox.setChecked(x[2] == '1'))
		AlchemyFilterSettings.JadeTabletStrikes.signal.connect(lambda x: self._view.strikesDropTabletCheckBox.setChecked(x[3] == '1'))

		AlchemyFilterSettings.JadeTabletDiscipline.signal.connect(lambda x: self._view.disciplinePickTabletCheckBox.setChecked(x[0] == '1'))
		AlchemyFilterSettings.JadeTabletDiscipline.signal.connect(lambda x: self._view.disciplineStoreTabletCheckBox.setChecked(x[1] == '1'))
		AlchemyFilterSettings.JadeTabletDiscipline.signal.connect(lambda x: self._view.disciplineSellTabletCheckBox.setChecked(x[2] == '1'))
		AlchemyFilterSettings.JadeTabletDiscipline.signal.connect(lambda x: self._view.disciplineDropTabletCheckBox.setChecked(x[3] == '1'))

		AlchemyFilterSettings.JadeTabletPenetration.signal.connect(lambda x: self._view.penetrationPickTabletCheckBox.setChecked(x[0] == '1'))
		AlchemyFilterSettings.JadeTabletPenetration.signal.connect(lambda x: self._view.penetrationStoreTabletCheckBox.setChecked(x[1] == '1'))
		AlchemyFilterSettings.JadeTabletPenetration.signal.connect(lambda x: self._view.penetrationSellTabletCheckBox.setChecked(x[2] == '1'))
		AlchemyFilterSettings.JadeTabletPenetration.signal.connect(lambda x: self._view.penetrationDropTabletCheckBox.setChecked(x[3] == '1'))

		AlchemyFilterSettings.JadeTabletDodging.signal.connect(lambda x: self._view.dodgePickTabletCheckBox.setChecked(x[0] == '1'))
		AlchemyFilterSettings.JadeTabletDodging.signal.connect(lambda x: self._view.dodgeStoreTabletCheckBox.setChecked(x[1] == '1'))
		AlchemyFilterSettings.JadeTabletDodging.signal.connect(lambda x: self._view.dodgeSellTabletCheckBox.setChecked(x[2] == '1'))
		AlchemyFilterSettings.JadeTabletDodging.signal.connect(lambda x: self._view.dodgeDropTabletCheckBox.setChecked(x[3] == '1'))

		AlchemyFilterSettings.JadeTabletStamina.signal.connect(lambda x: self._view.staminaPickTabletCheckBox.setChecked(x[0] == '1'))
		AlchemyFilterSettings.JadeTabletStamina.signal.connect(lambda x: self._view.staminaStoreTabletCheckBox.setChecked(x[1] == '1'))
		AlchemyFilterSettings.JadeTabletStamina.signal.connect(lambda x: self._view.staminaSellTabletCheckBox.setChecked(x[2] == '1'))
		AlchemyFilterSettings.JadeTabletStamina.signal.connect(lambda x: self._view.staminaDropTabletCheckBox.setChecked(x[3] == '1'))

		AlchemyFilterSettings.JadeTabletMagic.signal.connect(lambda x: self._view.magicPickTabletCheckBox.setChecked(x[0] == '1'))
		AlchemyFilterSettings.JadeTabletMagic.signal.connect(lambda x: self._view.magicStoreTabletCheckBox.setChecked(x[1] == '1'))
		AlchemyFilterSettings.JadeTabletMagic.signal.connect(lambda x: self._view.magicSellTabletCheckBox.setChecked(x[2] == '1'))
		AlchemyFilterSettings.JadeTabletMagic.signal.connect(lambda x: self._view.magicDropTabletCheckBox.setChecked(x[3] == '1'))

		AlchemyFilterSettings.JadeTabletFogs.signal.connect(lambda x: self._view.fogsPickTabletCheckBox.setChecked(x[0] == '1'))
		AlchemyFilterSettings.JadeTabletFogs.signal.connect(lambda x: self._view.fogsStoreTabletCheckBox.setChecked(x[1] == '1'))
		AlchemyFilterSettings.JadeTabletFogs.signal.connect(lambda x: self._view.fogsSellTabletCheckBox.setChecked(x[2] == '1'))
		AlchemyFilterSettings.JadeTabletFogs.signal.connect(lambda x: self._view.fogsDropTabletCheckBox.setChecked(x[3] == '1'))

		AlchemyFilterSettings.JadeTabletAir.signal.connect(lambda x: self._view.airPickTabletCheckBox.setChecked(x[0] == '1'))
		AlchemyFilterSettings.JadeTabletAir.signal.connect(lambda x: self._view.airStoreTabletCheckBox.setChecked(x[1] == '1'))
		AlchemyFilterSettings.JadeTabletAir.signal.connect(lambda x: self._view.airSellTabletCheckBox.setChecked(x[2] == '1'))
		AlchemyFilterSettings.JadeTabletAir.signal.connect(lambda x: self._view.airDropTabletCheckBox.setChecked(x[3] == '1'))

		AlchemyFilterSettings.JadeTabletFire.signal.connect(lambda x: self._view.firePickTabletCheckBox.setChecked(x[0] == '1'))
		AlchemyFilterSettings.JadeTabletFire.signal.connect(lambda x: self._view.fireStoreTabletCheckBox.setChecked(x[1] == '1'))
		AlchemyFilterSettings.JadeTabletFire.signal.connect(lambda x: self._view.fireSellTabletCheckBox.setChecked(x[2] == '1'))
		AlchemyFilterSettings.JadeTabletFire.signal.connect(lambda x: self._view.fireDropTabletCheckBox.setChecked(x[3] == '1'))

		AlchemyFilterSettings.JadeTabletImmunity.signal.connect(lambda x: self._view.immunityPickTabletCheckBox.setChecked(x[0] == '1'))
		AlchemyFilterSettings.JadeTabletImmunity.signal.connect(lambda x: self._view.immunityStoreTabletCheckBox.setChecked(x[1] == '1'))
		AlchemyFilterSettings.JadeTabletImmunity.signal.connect(lambda x: self._view.immunitySellTabletCheckBox.setChecked(x[2] == '1'))
		AlchemyFilterSettings.JadeTabletImmunity.signal.connect(lambda x: self._view.immunityDropTabletCheckBox.setChecked(x[3] == '1'))

		AlchemyFilterSettings.JadeTabletRevival.signal.connect(lambda x: self._view.revivalPickTabletCheckBox.setChecked(x[0] == '1'))
		AlchemyFilterSettings.JadeTabletRevival.signal.connect(lambda x: self._view.revivalStoreTabletCheckBox.setChecked(x[1] == '1'))
		AlchemyFilterSettings.JadeTabletRevival.signal.connect(lambda x: self._view.revivalSellTabletCheckBox.setChecked(x[2] == '1'))
		AlchemyFilterSettings.JadeTabletRevival.signal.connect(lambda x: self._view.revivalDropTabletCheckBox.setChecked(x[3] == '1'))

		AlchemyFilterSettings.JadeTabletSteady.signal.connect(lambda x: self._view.steadyPickTabletCheckBox.setChecked(x[0] == '1'))
		AlchemyFilterSettings.JadeTabletSteady.signal.connect(lambda x: self._view.steadyStoreTabletCheckBox.setChecked(x[1] == '1'))
		AlchemyFilterSettings.JadeTabletSteady.signal.connect(lambda x: self._view.steadySellTabletCheckBox.setChecked(x[2] == '1'))
		AlchemyFilterSettings.JadeTabletSteady.signal.connect(lambda x: self._view.steadyDropTabletCheckBox.setChecked(x[3] == '1'))

		AlchemyFilterSettings.JadeTabletLuck.signal.connect(lambda x: self._view.luckyPickTabletCheckBox.setChecked(x[0] == '1'))
		AlchemyFilterSettings.JadeTabletLuck.signal.connect(lambda x: self._view.luckyStoreTabletCheckBox.setChecked(x[1] == '1'))
		AlchemyFilterSettings.JadeTabletLuck.signal.connect(lambda x: self._view.luckySellTabletCheckBox.setChecked(x[2] == '1'))
		AlchemyFilterSettings.JadeTabletLuck.signal.connect(lambda x: self._view.luckyDropTabletCheckBox.setChecked(x[3] == '1'))

	def add_ruby_item_filters(self):
		self._view.couragePickCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterRubyCourage, 0, self._view.couragePickCheckBox.isChecked()))
		self._view.courageStoreCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterRubyCourage, 1, self._view.courageStoreCheckBox.isChecked()))
		self._view.courageSellCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterRubyCourage, 2, self._view.courageSellCheckBox.isChecked()))
		self._view.courageDropCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterRubyCourage, 3, self._view.courageDropCheckBox.isChecked()))

		self._view.warriorsPickCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterRubyWarriors, 0, self._view.warriorsPickCheckBox.isChecked()))
		self._view.warriorsStoreCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterRubyWarriors, 1, self._view.warriorsStoreCheckBox.isChecked()))
		self._view.warriorsSellCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterRubyWarriors, 2, self._view.warriorsSellCheckBox.isChecked()))
		self._view.warriorsDropCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterRubyWarriors, 3, self._view.warriorsDropCheckBox.isChecked()))

		self._view.philosophyPickCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterRubyPhilosophy, 0, self._view.philosophyPickCheckBox.isChecked()))
		self._view.philosophyStoreCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterRubyPhilosophy, 1, self._view.philosophyStoreCheckBox.isChecked()))
		self._view.philosophySellCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterRubyPhilosophy, 2, self._view.philosophySellCheckBox.isChecked()))
		self._view.philosophyDropCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterRubyPhilosophy, 3, self._view.philosophyDropCheckBox.isChecked()))

		self._view.meditationPickCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterRubyMeditation, 0, self._view.meditationPickCheckBox.isChecked()))
		self._view.meditationStoreCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterRubyMeditation, 1, self._view.meditationStoreCheckBox.isChecked()))
		self._view.meditationSellCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterRubyMeditation, 2, self._view.meditationSellCheckBox.isChecked()))
		self._view.meditationDropCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterRubyMeditation, 3, self._view.meditationDropCheckBox.isChecked()))

		self._view.challengePickCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterRubyChallenge, 0, self._view.challengePickCheckBox.isChecked()))
		self._view.challengeStoreCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterRubyChallenge, 1, self._view.challengeStoreCheckBox.isChecked()))
		self._view.challengeSellCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterRubyChallenge, 2, self._view.challengeSellCheckBox.isChecked()))
		self._view.challengeDropCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterRubyChallenge, 3, self._view.challengeDropCheckBox.isChecked()))

		self._view.focusPickCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterRubyFocus, 0, self._view.focusPickCheckBox.isChecked()))
		self._view.focusStoreCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterRubyFocus, 1, self._view.focusStoreCheckBox.isChecked()))
		self._view.focusSellCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterRubyFocus, 2, self._view.focusSellCheckBox.isChecked()))
		self._view.focusDropCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterRubyFocus, 3, self._view.focusDropCheckBox.isChecked()))

		self._view.fleshPickCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterRubyFlesh, 0, self._view.fleshPickCheckBox.isChecked()))
		self._view.fleshStoreCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterRubyFlesh, 1, self._view.fleshStoreCheckBox.isChecked()))
		self._view.fleshSellCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterRubyFlesh, 2, self._view.fleshSellCheckBox.isChecked()))
		self._view.fleshDropCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterRubyFlesh, 3, self._view.fleshDropCheckBox.isChecked()))

		self._view.lifePickCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterRubyLife, 0, self._view.lifePickCheckBox.isChecked()))
		self._view.lifeStoreCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterRubyLife, 1, self._view.lifeStoreCheckBox.isChecked()))
		self._view.lifeSellCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterRubyLife, 2, self._view.lifeSellCheckBox.isChecked()))
		self._view.lifeDropCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterRubyLife, 3, self._view.lifeDropCheckBox.isChecked()))

		self._view.mindPickCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterRubyMind, 0, self._view.mindPickCheckBox.isChecked()))
		self._view.mindStoreCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterRubyMind, 1, self._view.mindStoreCheckBox.isChecked()))
		self._view.mindSellCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterRubyMind, 2, self._view.mindSellCheckBox.isChecked()))
		self._view.mindDropCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterRubyMind, 3, self._view.mindDropCheckBox.isChecked()))

		self._view.spiritPickCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterRubySpirit, 0, self._view.spiritPickCheckBox.isChecked()))
		self._view.spiritStoreCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterRubySpirit, 1, self._view.spiritStoreCheckBox.isChecked()))
		self._view.spiritSellCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterRubySpirit, 2, self._view.spiritSellCheckBox.isChecked()))
		self._view.spiritDropCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterRubySpirit, 3, self._view.spiritDropCheckBox.isChecked()))

		self._view.dodgingPickCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterRubyDodging, 0, self._view.dodgingPickCheckBox.isChecked()))
		self._view.dodgingStoreCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterRubyDodging, 1, self._view.dodgingStoreCheckBox.isChecked()))
		self._view.dodgingSellCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterRubyDodging, 2, self._view.dodgingSellCheckBox.isChecked()))
		self._view.dodgingDropCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterRubyDodging, 3, self._view.dodgingDropCheckBox.isChecked()))

		self._view.agilityPickCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterRubyAgility, 0, self._view.agilityPickCheckBox.isChecked()))
		self._view.agilityStoreCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterRubyAgility, 1, self._view.agilityStoreCheckBox.isChecked()))
		self._view.agilitySellCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterRubyAgility, 2, self._view.agilitySellCheckBox.isChecked()))
		self._view.agilityDropCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterRubyAgility, 3, self._view.agilityDropCheckBox.isChecked()))

		self._view.trainingPickCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterRubyTraining, 0, self._view.trainingPickCheckBox.isChecked()))
		self._view.trainingStoreCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterRubyTraining, 1, self._view.trainingStoreCheckBox.isChecked()))
		self._view.trainingSellCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterRubyTraining, 2, self._view.trainingSellCheckBox.isChecked()))
		self._view.trainingDropCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterRubyTraining, 3, self._view.trainingDropCheckBox.isChecked()))


		self._view.prayerPickCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterRubyPrayer, 0, self._view.prayerPickCheckBox.isChecked()))
		self._view.prayerStoreCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterRubyPrayer, 1, self._view.prayerStoreCheckBox.isChecked()))
		self._view.prayerSellCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterRubyPrayer, 2, self._view.prayerSellCheckBox.isChecked()))
		self._view.prayerDropCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.FilterRubyPrayer, 3, self._view.prayerDropCheckBox.isChecked()))

	def add_ruby_item_filter_slots(self):
		AlchemyFilterSettings.FilterRubyCourage.signal.connect(lambda x: self._view.couragePickCheckBox.setChecked(x[0] == '1'))
		AlchemyFilterSettings.FilterRubyCourage.signal.connect(lambda x: self._view.courageStoreCheckBox.setChecked(x[1] == '1'))
		AlchemyFilterSettings.FilterRubyCourage.signal.connect(lambda x: self._view.courageSellCheckBox.setChecked(x[2] == '1'))
		AlchemyFilterSettings.FilterRubyCourage.signal.connect(lambda x: self._view.courageDropCheckBox.setChecked(x[3] == '1'))

		AlchemyFilterSettings.FilterRubyWarriors.signal.connect(lambda x: self._view.warriorsPickCheckBox.setChecked(x[0] == '1'))
		AlchemyFilterSettings.FilterRubyWarriors.signal.connect(lambda x: self._view.warriorsStoreCheckBox.setChecked(x[1] == '1'))
		AlchemyFilterSettings.FilterRubyWarriors.signal.connect(lambda x: self._view.warriorsSellCheckBox.setChecked(x[2] == '1'))
		AlchemyFilterSettings.FilterRubyWarriors.signal.connect(lambda x: self._view.warriorsDropCheckBox.setChecked(x[3] == '1'))

		AlchemyFilterSettings.FilterRubyPhilosophy.signal.connect(lambda x: self._view.philosophyPickCheckBox.setChecked(x[0] == '1'))
		AlchemyFilterSettings.FilterRubyPhilosophy.signal.connect(lambda x: self._view.philosophyStoreCheckBox.setChecked(x[1] == '1'))
		AlchemyFilterSettings.FilterRubyPhilosophy.signal.connect(lambda x: self._view.philosophySellCheckBox.setChecked(x[2] == '1'))
		AlchemyFilterSettings.FilterRubyPhilosophy.signal.connect(lambda x: self._view.philosophyDropCheckBox.setChecked(x[3] == '1'))

		AlchemyFilterSettings.FilterRubyMeditation.signal.connect(lambda x: self._view.meditationPickCheckBox.setChecked(x[0] == '1'))
		AlchemyFilterSettings.FilterRubyMeditation.signal.connect(lambda x: self._view.meditationStoreCheckBox.setChecked(x[1] == '1'))
		AlchemyFilterSettings.FilterRubyMeditation.signal.connect(lambda x: self._view.meditationSellCheckBox.setChecked(x[2] == '1'))
		AlchemyFilterSettings.FilterRubyMeditation.signal.connect(lambda x: self._view.meditationDropCheckBox.setChecked(x[3] == '1'))

		AlchemyFilterSettings.FilterRubyChallenge.signal.connect(lambda x: self._view.challengePickCheckBox.setChecked(x[0] == '1'))
		AlchemyFilterSettings.FilterRubyChallenge.signal.connect(lambda x: self._view.challengeStoreCheckBox.setChecked(x[1] == '1'))
		AlchemyFilterSettings.FilterRubyChallenge.signal.connect(lambda x: self._view.challengeSellCheckBox.setChecked(x[2] == '1'))
		AlchemyFilterSettings.FilterRubyChallenge.signal.connect(lambda x: self._view.challengeDropCheckBox.setChecked(x[3] == '1'))

		AlchemyFilterSettings.FilterRubyFocus.signal.connect(lambda x: self._view.focusPickCheckBox.setChecked(x[0] == '1'))
		AlchemyFilterSettings.FilterRubyFocus.signal.connect(lambda x: self._view.focusStoreCheckBox.setChecked(x[1] == '1'))
		AlchemyFilterSettings.FilterRubyFocus.signal.connect(lambda x: self._view.focusSellCheckBox.setChecked(x[2] == '1'))
		AlchemyFilterSettings.FilterRubyFocus.signal.connect(lambda x: self._view.focusDropCheckBox.setChecked(x[3] == '1'))

		AlchemyFilterSettings.FilterRubyFlesh.signal.connect(lambda x: self._view.fleshPickCheckBox.setChecked(x[0] == '1'))
		AlchemyFilterSettings.FilterRubyFlesh.signal.connect(lambda x: self._view.fleshStoreCheckBox.setChecked(x[1] == '1'))
		AlchemyFilterSettings.FilterRubyFlesh.signal.connect(lambda x: self._view.fleshSellCheckBox.setChecked(x[2] == '1'))
		AlchemyFilterSettings.FilterRubyFlesh.signal.connect(lambda x: self._view.fleshDropCheckBox.setChecked(x[3] == '1'))

		AlchemyFilterSettings.FilterRubyLife.signal.connect(lambda x: self._view.lifePickCheckBox.setChecked(x[0] == '1'))
		AlchemyFilterSettings.FilterRubyLife.signal.connect(lambda x: self._view.lifeStoreCheckBox.setChecked(x[1] == '1'))
		AlchemyFilterSettings.FilterRubyLife.signal.connect(lambda x: self._view.lifeSellCheckBox.setChecked(x[2] == '1'))
		AlchemyFilterSettings.FilterRubyLife.signal.connect(lambda x: self._view.lifeDropCheckBox.setChecked(x[3] == '1'))

		AlchemyFilterSettings.FilterRubyMind.signal.connect(lambda x: self._view.mindPickCheckBox.setChecked(x[0] == '1'))
		AlchemyFilterSettings.FilterRubyMind.signal.connect(lambda x: self._view.mindStoreCheckBox.setChecked(x[1] == '1'))
		AlchemyFilterSettings.FilterRubyMind.signal.connect(lambda x: self._view.mindSellCheckBox.setChecked(x[2] == '1'))
		AlchemyFilterSettings.FilterRubyMind.signal.connect(lambda x: self._view.mindDropCheckBox.setChecked(x[3] == '1'))

		AlchemyFilterSettings.FilterRubySpirit.signal.connect(lambda x: self._view.spiritPickCheckBox.setChecked(x[0] == '1'))
		AlchemyFilterSettings.FilterRubySpirit.signal.connect(lambda x: self._view.spiritStoreCheckBox.setChecked(x[1] == '1'))
		AlchemyFilterSettings.FilterRubySpirit.signal.connect(lambda x: self._view.spiritSellCheckBox.setChecked(x[2] == '1'))
		AlchemyFilterSettings.FilterRubySpirit.signal.connect(lambda x: self._view.spiritDropCheckBox.setChecked(x[3] == '1'))

		AlchemyFilterSettings.FilterRubyDodging.signal.connect(lambda x: self._view.dodgingPickCheckBox.setChecked(x[0] == '1'))
		AlchemyFilterSettings.FilterRubyDodging.signal.connect(lambda x: self._view.dodgingStoreCheckBox.setChecked(x[1] == '1'))
		AlchemyFilterSettings.FilterRubyDodging.signal.connect(lambda x: self._view.dodgingSellCheckBox.setChecked(x[2] == '1'))
		AlchemyFilterSettings.FilterRubyDodging.signal.connect(lambda x: self._view.dodgingDropCheckBox.setChecked(x[3] == '1'))

		AlchemyFilterSettings.FilterRubyAgility.signal.connect(lambda x: self._view.agilityPickCheckBox.setChecked(x[0] == '1'))
		AlchemyFilterSettings.FilterRubyAgility.signal.connect(lambda x: self._view.agilityStoreCheckBox.setChecked(x[1] == '1'))
		AlchemyFilterSettings.FilterRubyAgility.signal.connect(lambda x: self._view.agilitySellCheckBox.setChecked(x[2] == '1'))
		AlchemyFilterSettings.FilterRubyAgility.signal.connect(lambda x: self._view.agilityDropCheckBox.setChecked(x[3] == '1'))

		AlchemyFilterSettings.FilterRubyTraining.signal.connect(lambda x: self._view.trainingPickCheckBox.setChecked(x[0] == '1'))
		AlchemyFilterSettings.FilterRubyTraining.signal.connect(lambda x: self._view.trainingStoreCheckBox.setChecked(x[1] == '1'))
		AlchemyFilterSettings.FilterRubyTraining.signal.connect(lambda x: self._view.trainingSellCheckBox.setChecked(x[2] == '1'))
		AlchemyFilterSettings.FilterRubyTraining.signal.connect(lambda x: self._view.trainingDropCheckBox.setChecked(x[3] == '1'))


		AlchemyFilterSettings.FilterRubyPrayer.signal.connect(lambda x: self._view.prayerPickCheckBox.setChecked(x[0] == '1'))
		AlchemyFilterSettings.FilterRubyPrayer.signal.connect(lambda x: self._view.prayerStoreCheckBox.setChecked(x[1] == '1'))
		AlchemyFilterSettings.FilterRubyPrayer.signal.connect(lambda x: self._view.prayerSellCheckBox.setChecked(x[2] == '1'))
		AlchemyFilterSettings.FilterRubyPrayer.signal.connect(lambda x: self._view.prayerDropCheckBox.setChecked(x[3] == '1'))

	def add_ruby_tablet_filters(self):
		self._view.couragePickTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.RubyTabletCourage, 0, self._view.couragePickTabletCheckBox.isChecked()))
		self._view.courageStoreTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.RubyTabletCourage, 1, self._view.courageStoreTabletCheckBox.isChecked()))
		self._view.courageSellTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.RubyTabletCourage, 2, self._view.courageSellTabletCheckBox.isChecked()))
		self._view.courageDropTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.RubyTabletCourage, 3, self._view.courageDropTabletCheckBox.isChecked()))

		self._view.warriorsPickTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.RubyTabletWarriors, 0, self._view.warriorsPickTabletCheckBox.isChecked()))
		self._view.warriorsStoreTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.RubyTabletWarriors, 1, self._view.warriorsStoreTabletCheckBox.isChecked()))
		self._view.warriorsSellTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.RubyTabletWarriors, 2, self._view.warriorsSellTabletCheckBox.isChecked()))
		self._view.warriorsDropTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.RubyTabletWarriors, 3, self._view.warriorsDropTabletCheckBox.isChecked()))

		self._view.philosophyPickTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.RubyTabletPhilosophy, 0, self._view.philosophyPickTabletCheckBox.isChecked()))
		self._view.philosophyStoreTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.RubyTabletPhilosophy, 1, self._view.philosophyStoreTabletCheckBox.isChecked()))
		self._view.philosophySellTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.RubyTabletPhilosophy, 2, self._view.philosophySellTabletCheckBox.isChecked()))
		self._view.philosophyDropTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.RubyTabletPhilosophy, 3, self._view.philosophyDropTabletCheckBox.isChecked()))

		self._view.meditationPickTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.RubyTabletMeditation, 0, self._view.meditationPickTabletCheckBox.isChecked()))
		self._view.meditationStoreTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.RubyTabletMeditation, 1, self._view.meditationStoreTabletCheckBox.isChecked()))
		self._view.meditationSellTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.RubyTabletMeditation, 2, self._view.meditationSellTabletCheckBox.isChecked()))
		self._view.meditationDropTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.RubyTabletMeditation, 3, self._view.meditationDropTabletCheckBox.isChecked()))

		self._view.challengePickTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.RubyTabletChallenge, 0, self._view.challengePickTabletCheckBox.isChecked()))
		self._view.challengeStoreTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.RubyTabletChallenge, 1, self._view.challengeStoreTabletCheckBox.isChecked()))
		self._view.challengeSellTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.RubyTabletChallenge, 2, self._view.challengeSellTabletCheckBox.isChecked()))
		self._view.challengeDropTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.RubyTabletChallenge, 3, self._view.challengeDropTabletCheckBox.isChecked()))

		self._view.focusPickTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.RubyTabletFocus, 0, self._view.focusPickTabletCheckBox.isChecked()))
		self._view.focusStoreTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.RubyTabletFocus, 1, self._view.focusStoreTabletCheckBox.isChecked()))
		self._view.focusSellTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.RubyTabletFocus, 2, self._view.focusSellTabletCheckBox.isChecked()))
		self._view.focusDropTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.RubyTabletFocus, 3, self._view.focusDropTabletCheckBox.isChecked()))

		self._view.fleshPickTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.RubyTabletFlesh, 0, self._view.fleshPickTabletCheckBox.isChecked()))
		self._view.fleshStoreTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.RubyTabletFlesh, 1, self._view.fleshStoreTabletCheckBox.isChecked()))
		self._view.fleshSellTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.RubyTabletFlesh, 2, self._view.fleshSellTabletCheckBox.isChecked()))
		self._view.fleshDropTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.RubyTabletFlesh, 3, self._view.fleshDropTabletCheckBox.isChecked()))

		self._view.lifePickTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.RubyTabletLife, 0, self._view.lifePickTabletCheckBox.isChecked()))
		self._view.lifeStoreTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.RubyTabletLife, 1, self._view.lifeStoreTabletCheckBox.isChecked()))
		self._view.lifeSellTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.RubyTabletLife, 2, self._view.lifeSellTabletCheckBox.isChecked()))
		self._view.lifeDropTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.RubyTabletLife, 3, self._view.lifeDropTabletCheckBox.isChecked()))

		self._view.mindPickTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.RubyTabletMind, 0, self._view.mindPickTabletCheckBox.isChecked()))
		self._view.mindStoreTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.RubyTabletMind, 1, self._view.mindStoreTabletCheckBox.isChecked()))
		self._view.mindSellTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.RubyTabletMind, 2, self._view.mindSellTabletCheckBox.isChecked()))
		self._view.mindDropTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.RubyTabletMind, 3, self._view.mindDropTabletCheckBox.isChecked()))

		self._view.spiritPickTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.RubyTabletSpirit, 0, self._view.spiritPickTabletCheckBox.isChecked()))
		self._view.spiritStoreTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.RubyTabletSpirit, 1, self._view.spiritStoreTabletCheckBox.isChecked()))
		self._view.spiritSellTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.RubyTabletSpirit, 2, self._view.spiritSellTabletCheckBox.isChecked()))
		self._view.spiritDropTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.RubyTabletSpirit, 3, self._view.spiritDropTabletCheckBox.isChecked()))

		self._view.dodgingPickTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.RubyTabletDodging, 0, self._view.dodgingPickTabletCheckBox.isChecked()))
		self._view.dodgingStoreTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.RubyTabletDodging, 1, self._view.dodgingStoreTabletCheckBox.isChecked()))
		self._view.dodgingSellTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.RubyTabletDodging, 2, self._view.dodgingSellTabletCheckBox.isChecked()))
		self._view.dodgingDropTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.RubyTabletDodging, 3, self._view.dodgingDropTabletCheckBox.isChecked()))

		self._view.agilityPickTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.RubyTabletAgility, 0, self._view.agilityPickTabletCheckBox.isChecked()))
		self._view.agilityStoreTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.RubyTabletAgility, 1, self._view.agilityStoreTabletCheckBox.isChecked()))
		self._view.agilitySellTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.RubyTabletAgility, 2, self._view.agilitySellTabletCheckBox.isChecked()))
		self._view.agilityDropTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.RubyTabletAgility, 3, self._view.agilityDropTabletCheckBox.isChecked()))

		self._view.trainingPickTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.RubyTabletTraining, 0, self._view.trainingPickTabletCheckBox.isChecked()))
		self._view.trainingStoreTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.RubyTabletTraining, 1, self._view.trainingStoreTabletCheckBox.isChecked()))
		self._view.trainingSellTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.RubyTabletTraining, 2, self._view.trainingSellTabletCheckBox.isChecked()))
		self._view.trainingDropTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.RubyTabletTraining, 3, self._view.trainingDropTabletCheckBox.isChecked()))


		self._view.prayerPickTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.RubyTabletPrayer, 0, self._view.prayerPickTabletCheckBox.isChecked()))
		self._view.prayerStoreTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.RubyTabletPrayer, 1, self._view.prayerStoreTabletCheckBox.isChecked()))
		self._view.prayerSellTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.RubyTabletPrayer, 2, self._view.prayerSellTabletCheckBox.isChecked()))
		self._view.prayerDropTabletCheckBox.stateChanged.connect(lambda: self.change_item(AlchemyFilterSettings.RubyTabletPrayer, 3, self._view.prayerDropTabletCheckBox.isChecked()))

	def add_ruby_tablet_filter_slots(self):
		AlchemyFilterSettings.RubyTabletCourage.signal.connect(lambda x: self._view.couragePickTabletCheckBox.setChecked(x[0] == '1'))
		AlchemyFilterSettings.RubyTabletCourage.signal.connect(lambda x: self._view.courageStoreTabletCheckBox.setChecked(x[1] == '1'))
		AlchemyFilterSettings.RubyTabletCourage.signal.connect(lambda x: self._view.courageSellTabletCheckBox.setChecked(x[2] == '1'))
		AlchemyFilterSettings.RubyTabletCourage.signal.connect(lambda x: self._view.courageDropTabletCheckBox.setChecked(x[3] == '1'))

		AlchemyFilterSettings.RubyTabletWarriors.signal.connect(lambda x: self._view.warriorsPickTabletCheckBox.setChecked(x[0] == '1'))
		AlchemyFilterSettings.RubyTabletWarriors.signal.connect(lambda x: self._view.warriorsStoreTabletCheckBox.setChecked(x[1] == '1'))
		AlchemyFilterSettings.RubyTabletWarriors.signal.connect(lambda x: self._view.warriorsSellTabletCheckBox.setChecked(x[2] == '1'))
		AlchemyFilterSettings.RubyTabletWarriors.signal.connect(lambda x: self._view.warriorsDropTabletCheckBox.setChecked(x[3] == '1'))

		AlchemyFilterSettings.RubyTabletPhilosophy.signal.connect(lambda x: self._view.philosophyPickTabletCheckBox.setChecked(x[0] == '1'))
		AlchemyFilterSettings.RubyTabletPhilosophy.signal.connect(lambda x: self._view.philosophyStoreTabletCheckBox.setChecked(x[1] == '1'))
		AlchemyFilterSettings.RubyTabletPhilosophy.signal.connect(lambda x: self._view.philosophySellTabletCheckBox.setChecked(x[2] == '1'))
		AlchemyFilterSettings.RubyTabletPhilosophy.signal.connect(lambda x: self._view.philosophyDropTabletCheckBox.setChecked(x[3] == '1'))

		AlchemyFilterSettings.RubyTabletMeditation.signal.connect(lambda x: self._view.meditationPickTabletCheckBox.setChecked(x[0] == '1'))
		AlchemyFilterSettings.RubyTabletMeditation.signal.connect(lambda x: self._view.meditationStoreTabletCheckBox.setChecked(x[1] == '1'))
		AlchemyFilterSettings.RubyTabletMeditation.signal.connect(lambda x: self._view.meditationSellTabletCheckBox.setChecked(x[2] == '1'))
		AlchemyFilterSettings.RubyTabletMeditation.signal.connect(lambda x: self._view.meditationDropTabletCheckBox.setChecked(x[3] == '1'))

		AlchemyFilterSettings.RubyTabletChallenge.signal.connect(lambda x: self._view.challengePickTabletCheckBox.setChecked(x[0] == '1'))
		AlchemyFilterSettings.RubyTabletChallenge.signal.connect(lambda x: self._view.challengeStoreTabletCheckBox.setChecked(x[1] == '1'))
		AlchemyFilterSettings.RubyTabletChallenge.signal.connect(lambda x: self._view.challengeSellTabletCheckBox.setChecked(x[2] == '1'))
		AlchemyFilterSettings.RubyTabletChallenge.signal.connect(lambda x: self._view.challengeDropTabletCheckBox.setChecked(x[3] == '1'))

		AlchemyFilterSettings.RubyTabletFocus.signal.connect(lambda x: self._view.focusPickTabletCheckBox.setChecked(x[0] == '1'))
		AlchemyFilterSettings.RubyTabletFocus.signal.connect(lambda x: self._view.focusStoreTabletCheckBox.setChecked(x[1] == '1'))
		AlchemyFilterSettings.RubyTabletFocus.signal.connect(lambda x: self._view.focusSellTabletCheckBox.setChecked(x[2] == '1'))
		AlchemyFilterSettings.RubyTabletFocus.signal.connect(lambda x: self._view.focusDropTabletCheckBox.setChecked(x[3] == '1'))

		AlchemyFilterSettings.RubyTabletFlesh.signal.connect(lambda x: self._view.fleshPickTabletCheckBox.setChecked(x[0] == '1'))
		AlchemyFilterSettings.RubyTabletFlesh.signal.connect(lambda x: self._view.fleshStoreTabletCheckBox.setChecked(x[1] == '1'))
		AlchemyFilterSettings.RubyTabletFlesh.signal.connect(lambda x: self._view.fleshSellTabletCheckBox.setChecked(x[2] == '1'))
		AlchemyFilterSettings.RubyTabletFlesh.signal.connect(lambda x: self._view.fleshDropTabletCheckBox.setChecked(x[3] == '1'))

		AlchemyFilterSettings.RubyTabletLife.signal.connect(lambda x: self._view.lifePickTabletCheckBox.setChecked(x[0] == '1'))
		AlchemyFilterSettings.RubyTabletLife.signal.connect(lambda x: self._view.lifeStoreTabletCheckBox.setChecked(x[1] == '1'))
		AlchemyFilterSettings.RubyTabletLife.signal.connect(lambda x: self._view.lifeSellTabletCheckBox.setChecked(x[2] == '1'))
		AlchemyFilterSettings.RubyTabletLife.signal.connect(lambda x: self._view.lifeDropTabletCheckBox.setChecked(x[3] == '1'))

		AlchemyFilterSettings.RubyTabletMind.signal.connect(lambda x: self._view.mindPickTabletCheckBox.setChecked(x[0] == '1'))
		AlchemyFilterSettings.RubyTabletMind.signal.connect(lambda x: self._view.mindStoreTabletCheckBox.setChecked(x[1] == '1'))
		AlchemyFilterSettings.RubyTabletMind.signal.connect(lambda x: self._view.mindSellTabletCheckBox.setChecked(x[2] == '1'))
		AlchemyFilterSettings.RubyTabletMind.signal.connect(lambda x: self._view.mindDropTabletCheckBox.setChecked(x[3] == '1'))

		AlchemyFilterSettings.RubyTabletSpirit.signal.connect(lambda x: self._view.spiritPickTabletCheckBox.setChecked(x[0] == '1'))
		AlchemyFilterSettings.RubyTabletSpirit.signal.connect(lambda x: self._view.spiritStoreTabletCheckBox.setChecked(x[1] == '1'))
		AlchemyFilterSettings.RubyTabletSpirit.signal.connect(lambda x: self._view.spiritSellTabletCheckBox.setChecked(x[2] == '1'))
		AlchemyFilterSettings.RubyTabletSpirit.signal.connect(lambda x: self._view.spiritDropTabletCheckBox.setChecked(x[3] == '1'))

		AlchemyFilterSettings.RubyTabletDodging.signal.connect(lambda x: self._view.dodgingPickTabletCheckBox.setChecked(x[0] == '1'))
		AlchemyFilterSettings.RubyTabletDodging.signal.connect(lambda x: self._view.dodgingStoreTabletCheckBox.setChecked(x[1] == '1'))
		AlchemyFilterSettings.RubyTabletDodging.signal.connect(lambda x: self._view.dodgingSellTabletCheckBox.setChecked(x[2] == '1'))
		AlchemyFilterSettings.RubyTabletDodging.signal.connect(lambda x: self._view.dodgingDropTabletCheckBox.setChecked(x[3] == '1'))

		AlchemyFilterSettings.RubyTabletAgility.signal.connect(lambda x: self._view.agilityPickTabletCheckBox.setChecked(x[0] == '1'))
		AlchemyFilterSettings.RubyTabletAgility.signal.connect(lambda x: self._view.agilityStoreTabletCheckBox.setChecked(x[1] == '1'))
		AlchemyFilterSettings.RubyTabletAgility.signal.connect(lambda x: self._view.agilitySellTabletCheckBox.setChecked(x[2] == '1'))
		AlchemyFilterSettings.RubyTabletAgility.signal.connect(lambda x: self._view.agilityDropTabletCheckBox.setChecked(x[3] == '1'))

		AlchemyFilterSettings.RubyTabletTraining.signal.connect(lambda x: self._view.trainingPickTabletCheckBox.setChecked(x[0] == '1'))
		AlchemyFilterSettings.RubyTabletTraining.signal.connect(lambda x: self._view.trainingStoreTabletCheckBox.setChecked(x[1] == '1'))
		AlchemyFilterSettings.RubyTabletTraining.signal.connect(lambda x: self._view.trainingSellTabletCheckBox.setChecked(x[2] == '1'))
		AlchemyFilterSettings.RubyTabletTraining.signal.connect(lambda x: self._view.trainingDropTabletCheckBox.setChecked(x[3] == '1'))

		AlchemyFilterSettings.RubyTabletPrayer.signal.connect(lambda x: self._view.prayerPickTabletCheckBox.setChecked(x[0] == '1'))
		AlchemyFilterSettings.RubyTabletPrayer.signal.connect(lambda x: self._view.prayerStoreTabletCheckBox.setChecked(x[1] == '1'))
		AlchemyFilterSettings.RubyTabletPrayer.signal.connect(lambda x: self._view.prayerSellTabletCheckBox.setChecked(x[2] == '1'))
		AlchemyFilterSettings.RubyTabletPrayer.signal.connect(lambda x: self._view.prayerDropTabletCheckBox.setChecked(x[3] == '1'))

	def add_ch_filters(self):
		self._view.degree1CheckBox.stateChanged.connect(lambda: self.change_degree(ItemFilterSettings.DegreesAll, 1, self._view.degree1CheckBox.isChecked()))
		self._view.degree2CheckBox.stateChanged.connect(lambda: self.change_degree(ItemFilterSettings.DegreesAll, 2, self._view.degree2CheckBox.isChecked()))
		self._view.degree3CheckBox.stateChanged.connect(lambda: self.change_degree(ItemFilterSettings.DegreesAll, 3, self._view.degree3CheckBox.isChecked()))
		self._view.degree4CheckBox.stateChanged.connect(lambda: self.change_degree(ItemFilterSettings.DegreesAll, 4, self._view.degree4CheckBox.isChecked()))
		self._view.degree5CheckBox.stateChanged.connect(lambda: self.change_degree(ItemFilterSettings.DegreesAll, 5, self._view.degree5CheckBox.isChecked()))
		self._view.degree6CheckBox.stateChanged.connect(lambda: self.change_degree(ItemFilterSettings.DegreesAll, 6, self._view.degree6CheckBox.isChecked()))
		self._view.degree7CheckBox.stateChanged.connect(lambda: self.change_degree(ItemFilterSettings.DegreesAll, 7, self._view.degree7CheckBox.isChecked()))
		self._view.degree8CheckBox.stateChanged.connect(lambda: self.change_degree(ItemFilterSettings.DegreesAll, 8, self._view.degree8CheckBox.isChecked()))
		self._view.degree9CheckBox.stateChanged.connect(lambda: self.change_degree(ItemFilterSettings.DegreesAll, 9, self._view.degree9CheckBox.isChecked()))
		self._view.degree10CheckBox.stateChanged.connect(lambda: self.change_degree(ItemFilterSettings.DegreesAll, 10, self._view.degree10CheckBox.isChecked()))
		self._view.degree11CheckBox.stateChanged.connect(lambda: self.change_degree(ItemFilterSettings.DegreesAll, 11, self._view.degree11CheckBox.isChecked()))
		self._view.degree12CheckBox.stateChanged.connect(lambda: self.change_degree(ItemFilterSettings.DegreesAll, 12, self._view.degree12CheckBox.isChecked()))

		self._view.bladePickCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.Blade, 0, self._view.bladePickCheckBox.isChecked()))
		self._view.bladeStoreCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.Blade, 1, self._view.bladeStoreCheckBox.isChecked()))
		self._view.bladeSellCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.Blade, 2, self._view.bladeSellCheckBox.isChecked()))
		self._view.bladeDropCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.Blade, 3, self._view.bladeDropCheckBox.isChecked()))

		self._view.swordPickCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.Sword, 0, self._view.swordPickCheckBox.isChecked()))
		self._view.swordStoreCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.Sword, 1, self._view.swordStoreCheckBox.isChecked()))
		self._view.swordSellCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.Sword, 2, self._view.swordSellCheckBox.isChecked()))
		self._view.swordDropCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.Sword, 3, self._view.swordDropCheckBox.isChecked()))


		self._view.spearPickCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.Spear, 0, self._view.spearPickCheckBox.isChecked()))
		self._view.spearStoreCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.Spear, 1, self._view.spearStoreCheckBox.isChecked()))
		self._view.spearSellCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.Spear, 2, self._view.spearSellCheckBox.isChecked()))
		self._view.spearDropCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.Spear, 3, self._view.spearDropCheckBox.isChecked()))

		self._view.glaivePickCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.Glaive, 0, self._view.glaivePickCheckBox.isChecked()))
		self._view.glaiveStoreCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.Glaive, 1, self._view.glaiveStoreCheckBox.isChecked()))
		self._view.glaiveSellCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.Glaive, 2, self._view.glaiveSellCheckBox.isChecked()))
		self._view.glaiveDropCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.Glaive, 3, self._view.glaiveDropCheckBox.isChecked()))

		self._view.bowPickCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.Bow, 0, self._view.bowPickCheckBox.isChecked()))
		self._view.bowStoreCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.Bow, 1, self._view.bowStoreCheckBox.isChecked()))
		self._view.bowSellCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.Bow, 2, self._view.bowSellCheckBox.isChecked()))
		self._view.bowDropCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.Bow, 3, self._view.bowDropCheckBox.isChecked()))

		self._view.shieldPickCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.Shield, 0, self._view.shieldPickCheckBox.isChecked()))
		self._view.shieldStoreCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.Shield, 1, self._view.shieldStoreCheckBox.isChecked()))
		self._view.shieldSellCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.Shield, 2, self._view.shieldSellCheckBox.isChecked()))
		self._view.shieldDropCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.Shield, 3, self._view.shieldDropCheckBox.isChecked()))

		self._view.headPickCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.Head, 0, self._view.headPickCheckBox.isChecked()))
		self._view.headStoreCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.Head, 1, self._view.headStoreCheckBox.isChecked()))
		self._view.headSellCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.Head, 2, self._view.headSellCheckBox.isChecked()))
		self._view.headDropCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.Head, 3, self._view.headDropCheckBox.isChecked()))

		self._view.chestPickCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.Chest, 0, self._view.chestPickCheckBox.isChecked()))
		self._view.chestStoreCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.Chest, 1, self._view.chestStoreCheckBox.isChecked()))
		self._view.chestSellCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.Chest, 2, self._view.chestSellCheckBox.isChecked()))
		self._view.chestDropCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.Chest, 3, self._view.chestDropCheckBox.isChecked()))

		self._view.shoulderPickCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.Shoulder, 0, self._view.shoulderPickCheckBox.isChecked()))
		self._view.shoulderStoreCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.Shoulder, 1, self._view.shoulderStoreCheckBox.isChecked()))
		self._view.shoulderSellCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.Shoulder, 2, self._view.shoulderSellCheckBox.isChecked()))
		self._view.shoulderDropCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.Shoulder, 3, self._view.shoulderDropCheckBox.isChecked()))

		self._view.glovePickCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.Glove, 0, self._view.glovePickCheckBox.isChecked()))
		self._view.gloveStoreCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.Glove, 1, self._view.gloveStoreCheckBox.isChecked()))
		self._view.gloveSellCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.Glove, 2, self._view.gloveSellCheckBox.isChecked()))
		self._view.gloveDropCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.Glove, 3, self._view.gloveDropCheckBox.isChecked()))

		self._view.trouserPickCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.Hose, 0, self._view.trouserPickCheckBox.isChecked()))
		self._view.trouserStoreCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.Hose, 1, self._view.trouserStoreCheckBox.isChecked()))
		self._view.trouserSellCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.Hose, 2, self._view.trouserSellCheckBox.isChecked()))
		self._view.trouserDropCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.Hose, 3, self._view.trouserDropCheckBox.isChecked()))

		self._view.bootPickCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.Boots, 0, self._view.bootPickCheckBox.isChecked()))
		self._view.bootStoreCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.Boots, 1, self._view.bootStoreCheckBox.isChecked()))
		self._view.bootSellCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.Boots, 2, self._view.bootSellCheckBox.isChecked()))
		self._view.bootDropCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.Boots, 3, self._view.bootDropCheckBox.isChecked()))

		self._view.necklacePickCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.Necklace, 0, self._view.necklacePickCheckBox.isChecked()))
		self._view.necklaceStoreCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.Necklace, 1, self._view.necklaceStoreCheckBox.isChecked()))
		self._view.necklaceSellCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.Necklace, 2, self._view.necklaceSellCheckBox.isChecked()))
		self._view.necklaceDropCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.Necklace, 3, self._view.necklaceDropCheckBox.isChecked()))

		self._view.earringPickCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.Earrings, 0, self._view.earringPickCheckBox.isChecked()))
		self._view.earringStoreCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.Earrings, 1, self._view.earringStoreCheckBox.isChecked()))
		self._view.earringSellCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.Earrings, 2, self._view.earringSellCheckBox.isChecked()))
		self._view.earringDropCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.Earrings, 3, self._view.earringDropCheckBox.isChecked()))

		self._view.ringPickCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.Ring, 0, self._view.ringPickCheckBox.isChecked()))
		self._view.ringStoreCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.Ring, 1, self._view.ringStoreCheckBox.isChecked()))
		self._view.ringSellCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.Ring, 2, self._view.ringSellCheckBox.isChecked()))
		self._view.ringDropCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.Ring, 3, self._view.ringDropCheckBox.isChecked()))

	def add_ch_filter_slots(self):
		ItemFilterSettings.DegreesAll.signal.connect(lambda x: self._view.degree1CheckBox.setChecked(x[11] == '1'))
		ItemFilterSettings.DegreesAll.signal.connect(lambda x: self._view.degree2CheckBox.setChecked(x[10] == '1'))
		ItemFilterSettings.DegreesAll.signal.connect(lambda x: self._view.degree3CheckBox.setChecked(x[9] == '1'))
		ItemFilterSettings.DegreesAll.signal.connect(lambda x: self._view.degree4CheckBox.setChecked(x[8] == '1'))
		ItemFilterSettings.DegreesAll.signal.connect(lambda x: self._view.degree5CheckBox.setChecked(x[7] == '1'))
		ItemFilterSettings.DegreesAll.signal.connect(lambda x: self._view.degree6CheckBox.setChecked(x[6] == '1'))
		ItemFilterSettings.DegreesAll.signal.connect(lambda x: self._view.degree7CheckBox.setChecked(x[5] == '1'))
		ItemFilterSettings.DegreesAll.signal.connect(lambda x: self._view.degree8CheckBox.setChecked(x[4] == '1'))
		ItemFilterSettings.DegreesAll.signal.connect(lambda x: self._view.degree9CheckBox.setChecked(x[3] == '1'))
		ItemFilterSettings.DegreesAll.signal.connect(lambda x: self._view.degree10CheckBox.setChecked(x[2] == '1'))
		ItemFilterSettings.DegreesAll.signal.connect(lambda x: self._view.degree11CheckBox.setChecked(x[1] == '1'))
		ItemFilterSettings.DegreesAll.signal.connect(lambda x: self._view.degree12CheckBox.setChecked(x[0] == '1'))

		ItemFilterSettings.Blade.signal.connect(lambda x: self._view.bladePickCheckBox.setChecked(x[0] == '1'))
		ItemFilterSettings.Blade.signal.connect(lambda x: self._view.bladeStoreCheckBox.setChecked(x[1] == '1'))
		ItemFilterSettings.Blade.signal.connect(lambda x: self._view.bladeSellCheckBox.setChecked(x[2] == '1'))
		ItemFilterSettings.Blade.signal.connect(lambda x: self._view.bladeDropCheckBox.setChecked(x[3] == '1'))

		ItemFilterSettings.Sword.signal.connect(lambda x: self._view.swordPickCheckBox.setChecked(x[0] == '1'))
		ItemFilterSettings.Sword.signal.connect(lambda x: self._view.swordStoreCheckBox.setChecked(x[1] == '1'))
		ItemFilterSettings.Sword.signal.connect(lambda x: self._view.swordSellCheckBox.setChecked(x[2] == '1'))
		ItemFilterSettings.Sword.signal.connect(lambda x: self._view.swordDropCheckBox.setChecked(x[3] == '1'))


		ItemFilterSettings.Spear.signal.connect(lambda x: self._view.spearPickCheckBox.setChecked(x[0] == '1'))
		ItemFilterSettings.Spear.signal.connect(lambda x: self._view.spearStoreCheckBox.setChecked(x[1] == '1'))
		ItemFilterSettings.Spear.signal.connect(lambda x: self._view.spearSellCheckBox.setChecked(x[2] == '1'))
		ItemFilterSettings.Spear.signal.connect(lambda x: self._view.spearDropCheckBox.setChecked(x[3] == '1'))

		ItemFilterSettings.Glaive.signal.connect(lambda x: self._view.glaivePickCheckBox.setChecked(x[0] == '1'))
		ItemFilterSettings.Glaive.signal.connect(lambda x: self._view.glaiveStoreCheckBox.setChecked(x[1] == '1'))
		ItemFilterSettings.Glaive.signal.connect(lambda x: self._view.glaiveSellCheckBox.setChecked(x[2] == '1'))
		ItemFilterSettings.Glaive.signal.connect(lambda x: self._view.glaiveDropCheckBox.setChecked(x[3] == '1'))

		ItemFilterSettings.Bow.signal.connect(lambda x: self._view.bowPickCheckBox.setChecked(x[0] == '1'))
		ItemFilterSettings.Bow.signal.connect(lambda x: self._view.bowStoreCheckBox.setChecked(x[1] == '1'))
		ItemFilterSettings.Bow.signal.connect(lambda x: self._view.bowSellCheckBox.setChecked(x[2] == '1'))
		ItemFilterSettings.Bow.signal.connect(lambda x: self._view.bowDropCheckBox.setChecked(x[3] == '1'))

		ItemFilterSettings.Shield.signal.connect(lambda x: self._view.shieldPickCheckBox.setChecked(x[0] == '1'))
		ItemFilterSettings.Shield.signal.connect(lambda x: self._view.shieldStoreCheckBox.setChecked(x[1] == '1'))
		ItemFilterSettings.Shield.signal.connect(lambda x: self._view.shieldSellCheckBox.setChecked(x[2] == '1'))
		ItemFilterSettings.Shield.signal.connect(lambda x: self._view.shieldDropCheckBox.setChecked(x[3] == '1'))

		ItemFilterSettings.Head.signal.connect(lambda x: self._view.headPickCheckBox.setChecked(x[0] == '1'))
		ItemFilterSettings.Head.signal.connect(lambda x: self._view.headStoreCheckBox.setChecked(x[1] == '1'))
		ItemFilterSettings.Head.signal.connect(lambda x: self._view.headSellCheckBox.setChecked(x[2] == '1'))
		ItemFilterSettings.Head.signal.connect(lambda x: self._view.headDropCheckBox.setChecked(x[3] == '1'))

		ItemFilterSettings.Chest.signal.connect(lambda x: self._view.chestPickCheckBox.setChecked(x[0] == '1'))
		ItemFilterSettings.Chest.signal.connect(lambda x: self._view.chestStoreCheckBox.setChecked(x[1] == '1'))
		ItemFilterSettings.Chest.signal.connect(lambda x: self._view.chestSellCheckBox.setChecked(x[2] == '1'))
		ItemFilterSettings.Chest.signal.connect(lambda x: self._view.chestDropCheckBox.setChecked(x[3] == '1'))

		ItemFilterSettings.Shoulder.signal.connect(lambda x: self._view.shoulderPickCheckBox.setChecked(x[0] == '1'))
		ItemFilterSettings.Shoulder.signal.connect(lambda x: self._view.shoulderStoreCheckBox.setChecked(x[1] == '1'))
		ItemFilterSettings.Shoulder.signal.connect(lambda x: self._view.shoulderSellCheckBox.setChecked(x[2] == '1'))
		ItemFilterSettings.Shoulder.signal.connect(lambda x: self._view.shoulderDropCheckBox.setChecked(x[3] == '1'))

		ItemFilterSettings.Glove.signal.connect(lambda x: self._view.glovePickCheckBox.setChecked(x[0] == '1'))
		ItemFilterSettings.Glove.signal.connect(lambda x: self._view.gloveStoreCheckBox.setChecked(x[1] == '1'))
		ItemFilterSettings.Glove.signal.connect(lambda x: self._view.gloveSellCheckBox.setChecked(x[2] == '1'))
		ItemFilterSettings.Glove.signal.connect(lambda x: self._view.gloveDropCheckBox.setChecked(x[3] == '1'))

		ItemFilterSettings.Hose.signal.connect(lambda x: self._view.trouserPickCheckBox.setChecked(x[0] == '1'))
		ItemFilterSettings.Hose.signal.connect(lambda x: self._view.trouserStoreCheckBox.setChecked(x[1] == '1'))
		ItemFilterSettings.Hose.signal.connect(lambda x: self._view.trouserSellCheckBox.setChecked(x[2] == '1'))
		ItemFilterSettings.Hose.signal.connect(lambda x: self._view.trouserDropCheckBox.setChecked(x[3] == '1'))

		ItemFilterSettings.Boots.signal.connect(lambda x: self._view.bootPickCheckBox.setChecked(x[0] == '1'))
		ItemFilterSettings.Boots.signal.connect(lambda x: self._view.bootStoreCheckBox.setChecked(x[1] == '1'))
		ItemFilterSettings.Boots.signal.connect(lambda x: self._view.bootSellCheckBox.setChecked(x[2] == '1'))
		ItemFilterSettings.Boots.signal.connect(lambda x: self._view.bootDropCheckBox.setChecked(x[3] == '1'))

		ItemFilterSettings.Necklace.signal.connect(lambda x: self._view.necklacePickCheckBox.setChecked(x[0] == '1'))
		ItemFilterSettings.Necklace.signal.connect(lambda x: self._view.necklaceStoreCheckBox.setChecked(x[1] == '1'))
		ItemFilterSettings.Necklace.signal.connect(lambda x: self._view.necklaceSellCheckBox.setChecked(x[2] == '1'))
		ItemFilterSettings.Necklace.signal.connect(lambda x: self._view.necklaceDropCheckBox.setChecked(x[3] == '1'))

		ItemFilterSettings.Earrings.signal.connect(lambda x: self._view.earringPickCheckBox.setChecked(x[0] == '1'))
		ItemFilterSettings.Earrings.signal.connect(lambda x: self._view.earringStoreCheckBox.setChecked(x[1] == '1'))
		ItemFilterSettings.Earrings.signal.connect(lambda x: self._view.earringSellCheckBox.setChecked(x[2] == '1'))
		ItemFilterSettings.Earrings.signal.connect(lambda x: self._view.earringDropCheckBox.setChecked(x[3] == '1'))

		ItemFilterSettings.Ring.signal.connect(lambda x: self._view.ringPickCheckBox.setChecked(x[0] == '1'))
		ItemFilterSettings.Ring.signal.connect(lambda x: self._view.ringStoreCheckBox.setChecked(x[1] == '1'))
		ItemFilterSettings.Ring.signal.connect(lambda x: self._view.ringSellCheckBox.setChecked(x[2] == '1'))
		ItemFilterSettings.Ring.signal.connect(lambda x: self._view.ringDropCheckBox.setChecked(x[3] == '1'))

	def add_eu_weapon_filters(self):
		self._view.euWeaponDegree1CheckBox.stateChanged.connect(lambda: self.change_degree(ItemFilterSettings.DegreesAll, 1, self._view.euWeaponDegree1CheckBox.isChecked()))
		self._view.euWeaponDegree2CheckBox.stateChanged.connect(lambda: self.change_degree(ItemFilterSettings.DegreesAll, 2, self._view.euWeaponDegree2CheckBox.isChecked()))
		self._view.euWeaponDegree3CheckBox.stateChanged.connect(lambda: self.change_degree(ItemFilterSettings.DegreesAll, 3, self._view.euWeaponDegree3CheckBox.isChecked()))
		self._view.euWeaponDegree4CheckBox.stateChanged.connect(lambda: self.change_degree(ItemFilterSettings.DegreesAll, 4, self._view.euWeaponDegree4CheckBox.isChecked()))
		self._view.euWeaponDegree5CheckBox.stateChanged.connect(lambda: self.change_degree(ItemFilterSettings.DegreesAll, 5, self._view.euWeaponDegree5CheckBox.isChecked()))
		self._view.euWeaponDegree6CheckBox.stateChanged.connect(lambda: self.change_degree(ItemFilterSettings.DegreesAll, 6, self._view.euWeaponDegree6CheckBox.isChecked()))
		self._view.euWeaponDegree7CheckBox.stateChanged.connect(lambda: self.change_degree(ItemFilterSettings.DegreesAll, 7, self._view.euWeaponDegree7CheckBox.isChecked()))
		self._view.euWeaponDegree8CheckBox.stateChanged.connect(lambda: self.change_degree(ItemFilterSettings.DegreesAll, 8, self._view.euWeaponDegree8CheckBox.isChecked()))
		self._view.euWeaponDegree9CheckBox.stateChanged.connect(lambda: self.change_degree(ItemFilterSettings.DegreesAll, 9, self._view.euWeaponDegree9CheckBox.isChecked()))
		self._view.euWeaponDegree10CheckBox.stateChanged.connect(lambda: self.change_degree(ItemFilterSettings.DegreesAll, 10, self._view.euWeaponDegree10CheckBox.isChecked()))
		self._view.euWeaponDegree11CheckBox.stateChanged.connect(lambda: self.change_degree(ItemFilterSettings.DegreesAll, 11, self._view.euWeaponDegree11CheckBox.isChecked()))
		self._view.euWeaponDegree12CheckBox.stateChanged.connect(lambda: self.change_degree(ItemFilterSettings.DegreesAll, 12, self._view.euWeaponDegree12CheckBox.isChecked()))

		self._view.axePickCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.EUAxe, 0, self._view.axePickCheckBox.isChecked()))
		self._view.axeStoreCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.EUAxe, 1, self._view.axeStoreCheckBox.isChecked()))
		self._view.axeSellCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.EUAxe, 2, self._view.axeSellCheckBox.isChecked()))
		self._view.axeDropCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.EUAxe, 3, self._view.axeDropCheckBox.isChecked()))

		self._view.onehandPickCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.EUSword1H, 0, self._view.onehandPickCheckBox.isChecked()))
		self._view.onehandStoreCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.EUSword1H, 1, self._view.onehandStoreCheckBox.isChecked()))
		self._view.onehandSellCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.EUSword1H, 2, self._view.onehandSellCheckBox.isChecked()))
		self._view.onehandDropCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.EUSword1H, 3, self._view.onehandDropCheckBox.isChecked()))

		self._view.twohandPickCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.EUSword2H, 0, self._view.twohandPickCheckBox.isChecked()))
		self._view.twohandStoreCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.EUSword2H, 1, self._view.twohandStoreCheckBox.isChecked()))
		self._view.twohandSellCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.EUSword2H, 2, self._view.twohandSellCheckBox.isChecked()))
		self._view.twohandDropCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.EUSword2H, 3, self._view.twohandDropCheckBox.isChecked()))

		self._view.crossbowPickCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.EUCrossbow, 0, self._view.crossbowPickCheckBox.isChecked()))
		self._view.crossbowStoreCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.EUCrossbow, 1, self._view.crossbowStoreCheckBox.isChecked()))
		self._view.crossbowSellCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.EUCrossbow, 2, self._view.crossbowSellCheckBox.isChecked()))
		self._view.crossbowDropCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.EUCrossbow, 3, self._view.crossbowDropCheckBox.isChecked()))

		self._view.daggerPickCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.EUDagger, 0, self._view.daggerPickCheckBox.isChecked()))
		self._view.daggerStoreCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.EUDagger, 1, self._view.daggerStoreCheckBox.isChecked()))
		self._view.daggerSellCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.EUDagger, 2, self._view.daggerSellCheckBox.isChecked()))
		self._view.daggerDropCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.EUDagger, 3, self._view.daggerDropCheckBox.isChecked()))

		self._view.mageStaffPickCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.EUWizardStaff, 0, self._view.mageStaffPickCheckBox.isChecked()))
		self._view.mageStaffStoreCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.EUWizardStaff, 1, self._view.mageStaffStoreCheckBox.isChecked()))
		self._view.mageStaffSellCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.EUWizardStaff, 2, self._view.mageStaffSellCheckBox.isChecked()))
		self._view.mageStaffDropCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.EUWizardStaff, 3, self._view.mageStaffDropCheckBox.isChecked()))

		self._view.darkstaffPickCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.EUWarlockStaff, 0, self._view.darkstaffPickCheckBox.isChecked()))
		self._view.darkstaffStoreCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.EUWarlockStaff, 1, self._view.darkstaffStoreCheckBox.isChecked()))
		self._view.darkstaffSellCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.EUWarlockStaff, 2, self._view.darkstaffSellCheckBox.isChecked()))
		self._view.darkstaffDropCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.EUWarlockStaff, 3, self._view.darkstaffDropCheckBox.isChecked()))

		self._view.harpPickCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.EUHarp, 0, self._view.harpPickCheckBox.isChecked()))
		self._view.harpStoreCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.EUHarp, 1, self._view.harpStoreCheckBox.isChecked()))
		self._view.harpSellCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.EUHarp, 2, self._view.harpSellCheckBox.isChecked()))
		self._view.harpDropCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.EUHarp, 3, self._view.harpDropCheckBox.isChecked()))

		self._view.clericStaffPickCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.EUClericStaff, 0, self._view.clericStaffPickCheckBox.isChecked()))
		self._view.clericStaffStoreCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.EUClericStaff, 1, self._view.clericStaffStoreCheckBox.isChecked()))
		self._view.clericStaffSellCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.EUClericStaff, 2, self._view.clericStaffSellCheckBox.isChecked()))
		self._view.clericStaffDropCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.EUClericStaff, 3, self._view.clericStaffDropCheckBox.isChecked()))

		self._view.euShieldPickCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.EUShield, 0, self._view.euShieldPickCheckBox.isChecked()))
		self._view.euShieldStoreCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.EUShield, 1, self._view.euShieldStoreCheckBox.isChecked()))
		self._view.euShieldSellCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.EUShield, 2, self._view.euShieldSellCheckBox.isChecked()))
		self._view.euShieldDropCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.EUShield, 3, self._view.euShieldDropCheckBox.isChecked()))

	def add_eu_weapon_filter_slots(self):
		ItemFilterSettings.DegreesAll.signal.connect(lambda x: self._view.euWeaponDegree1CheckBox.setChecked(x[11] == '1'))
		ItemFilterSettings.DegreesAll.signal.connect(lambda x: self._view.euWeaponDegree2CheckBox.setChecked(x[10] == '1'))
		ItemFilterSettings.DegreesAll.signal.connect(lambda x: self._view.euWeaponDegree3CheckBox.setChecked(x[9] == '1'))
		ItemFilterSettings.DegreesAll.signal.connect(lambda x: self._view.euWeaponDegree4CheckBox.setChecked(x[8] == '1'))
		ItemFilterSettings.DegreesAll.signal.connect(lambda x: self._view.euWeaponDegree5CheckBox.setChecked(x[7] == '1'))
		ItemFilterSettings.DegreesAll.signal.connect(lambda x: self._view.euWeaponDegree6CheckBox.setChecked(x[6] == '1'))
		ItemFilterSettings.DegreesAll.signal.connect(lambda x: self._view.euWeaponDegree7CheckBox.setChecked(x[5] == '1'))
		ItemFilterSettings.DegreesAll.signal.connect(lambda x: self._view.euWeaponDegree8CheckBox.setChecked(x[4] == '1'))
		ItemFilterSettings.DegreesAll.signal.connect(lambda x: self._view.euWeaponDegree9CheckBox.setChecked(x[3] == '1'))
		ItemFilterSettings.DegreesAll.signal.connect(lambda x: self._view.euWeaponDegree10CheckBox.setChecked(x[2] == '1'))
		ItemFilterSettings.DegreesAll.signal.connect(lambda x: self._view.euWeaponDegree11CheckBox.setChecked(x[1] == '1'))
		ItemFilterSettings.DegreesAll.signal.connect(lambda x: self._view.euWeaponDegree12CheckBox.setChecked(x[0] == '1'))

		ItemFilterSettings.EUAxe.signal.connect(lambda x: self._view.axePickCheckBox.setChecked(x[0] == '1'))
		ItemFilterSettings.EUAxe.signal.connect(lambda x: self._view.axeStoreCheckBox.setChecked(x[1] == '1'))
		ItemFilterSettings.EUAxe.signal.connect(lambda x: self._view.axeSellCheckBox.setChecked(x[2] == '1'))
		ItemFilterSettings.EUAxe.signal.connect(lambda x: self._view.axeDropCheckBox.setChecked(x[3] == '1'))

		ItemFilterSettings.EUSword1H.signal.connect(lambda x: self._view.onehandPickCheckBox.setChecked(x[0] == '1'))
		ItemFilterSettings.EUSword1H.signal.connect(lambda x: self._view.onehandStoreCheckBox.setChecked(x[1] == '1'))
		ItemFilterSettings.EUSword1H.signal.connect(lambda x: self._view.onehandSellCheckBox.setChecked(x[2] == '1'))
		ItemFilterSettings.EUSword1H.signal.connect(lambda x: self._view.onehandDropCheckBox.setChecked(x[3] == '1'))

		ItemFilterSettings.EUSword2H.signal.connect(lambda x: self._view.twohandPickCheckBox.setChecked(x[0] == '1'))
		ItemFilterSettings.EUSword2H.signal.connect(lambda x: self._view.twohandStoreCheckBox.setChecked(x[1] == '1'))
		ItemFilterSettings.EUSword2H.signal.connect(lambda x: self._view.twohandSellCheckBox.setChecked(x[2] == '1'))
		ItemFilterSettings.EUSword2H.signal.connect(lambda x: self._view.twohandDropCheckBox.setChecked(x[3] == '1'))

		ItemFilterSettings.EUCrossbow.signal.connect(lambda x: self._view.crossbowPickCheckBox.setChecked(x[0] == '1'))
		ItemFilterSettings.EUCrossbow.signal.connect(lambda x: self._view.crossbowStoreCheckBox.setChecked(x[1] == '1'))
		ItemFilterSettings.EUCrossbow.signal.connect(lambda x: self._view.crossbowSellCheckBox.setChecked(x[2] == '1'))
		ItemFilterSettings.EUCrossbow.signal.connect(lambda x: self._view.crossbowDropCheckBox.setChecked(x[3] == '1'))

		ItemFilterSettings.EUDagger.signal.connect(lambda x: self._view.daggerPickCheckBox.setChecked(x[0] == '1'))
		ItemFilterSettings.EUDagger.signal.connect(lambda x: self._view.daggerStoreCheckBox.setChecked(x[1] == '1'))
		ItemFilterSettings.EUDagger.signal.connect(lambda x: self._view.daggerSellCheckBox.setChecked(x[2] == '1'))
		ItemFilterSettings.EUDagger.signal.connect(lambda x: self._view.daggerDropCheckBox.setChecked(x[3] == '1'))

		ItemFilterSettings.EUWizardStaff.signal.connect(lambda x: self._view.mageStaffPickCheckBox.setChecked(x[0] == '1'))
		ItemFilterSettings.EUWizardStaff.signal.connect(lambda x: self._view.mageStaffStoreCheckBox.setChecked(x[1] == '1'))
		ItemFilterSettings.EUWizardStaff.signal.connect(lambda x: self._view.mageStaffSellCheckBox.setChecked(x[2] == '1'))
		ItemFilterSettings.EUWizardStaff.signal.connect(lambda x: self._view.mageStaffDropCheckBox.setChecked(x[3] == '1'))

		ItemFilterSettings.EUWarlockStaff.signal.connect(lambda x: self._view.darkstaffPickCheckBox.setChecked(x[0] == '1'))
		ItemFilterSettings.EUWarlockStaff.signal.connect(lambda x: self._view.darkstaffStoreCheckBox.setChecked(x[1] == '1'))
		ItemFilterSettings.EUWarlockStaff.signal.connect(lambda x: self._view.darkstaffSellCheckBox.setChecked(x[2] == '1'))
		ItemFilterSettings.EUWarlockStaff.signal.connect(lambda x: self._view.darkstaffDropCheckBox.setChecked(x[3] == '1'))

		ItemFilterSettings.EUHarp.signal.connect(lambda x: self._view.harpPickCheckBox.setChecked(x[0] == '1'))
		ItemFilterSettings.EUHarp.signal.connect(lambda x: self._view.harpStoreCheckBox.setChecked(x[1] == '1'))
		ItemFilterSettings.EUHarp.signal.connect(lambda x: self._view.harpSellCheckBox.setChecked(x[2] == '1'))
		ItemFilterSettings.EUHarp.signal.connect(lambda x: self._view.harpDropCheckBox.setChecked(x[3] == '1'))

		ItemFilterSettings.EUClericStaff.signal.connect(lambda x: self._view.clericStaffPickCheckBox.setChecked(x[0] == '1'))
		ItemFilterSettings.EUClericStaff.signal.connect(lambda x: self._view.clericStaffStoreCheckBox.setChecked(x[1] == '1'))
		ItemFilterSettings.EUClericStaff.signal.connect(lambda x: self._view.clericStaffSellCheckBox.setChecked(x[2] == '1'))
		ItemFilterSettings.EUClericStaff.signal.connect(lambda x: self._view.clericStaffDropCheckBox.setChecked(x[3] == '1'))

		ItemFilterSettings.EUShield.signal.connect(lambda x: self._view.euShieldPickCheckBox.setChecked(x[0] == '1'))
		ItemFilterSettings.EUShield.signal.connect(lambda x: self._view.euShieldStoreCheckBox.setChecked(x[1] == '1'))
		ItemFilterSettings.EUShield.signal.connect(lambda x: self._view.euShieldSellCheckBox.setChecked(x[2] == '1'))
		ItemFilterSettings.EUShield.signal.connect(lambda x: self._view.euShieldDropCheckBox.setChecked(x[3] == '1'))

	def add_eu_filters(self):
		self._view.euDegree1CheckBox.stateChanged.connect(lambda: self.change_degree(ItemFilterSettings.DegreesAll, 1, self._view.euDegree1CheckBox.isChecked()))
		self._view.euDegree2CheckBox.stateChanged.connect(lambda: self.change_degree(ItemFilterSettings.DegreesAll, 2, self._view.euDegree2CheckBox.isChecked()))
		self._view.euDegree3CheckBox.stateChanged.connect(lambda: self.change_degree(ItemFilterSettings.DegreesAll, 3, self._view.euDegree3CheckBox.isChecked()))
		self._view.euDegree4CheckBox.stateChanged.connect(lambda: self.change_degree(ItemFilterSettings.DegreesAll, 4, self._view.euDegree4CheckBox.isChecked()))
		self._view.euDegree5CheckBox.stateChanged.connect(lambda: self.change_degree(ItemFilterSettings.DegreesAll, 5, self._view.euDegree5CheckBox.isChecked()))
		self._view.euDegree6CheckBox.stateChanged.connect(lambda: self.change_degree(ItemFilterSettings.DegreesAll, 6, self._view.euDegree6CheckBox.isChecked()))
		self._view.euDegree7CheckBox.stateChanged.connect(lambda: self.change_degree(ItemFilterSettings.DegreesAll, 7, self._view.euDegree7CheckBox.isChecked()))
		self._view.euDegree8CheckBox.stateChanged.connect(lambda: self.change_degree(ItemFilterSettings.DegreesAll, 8, self._view.euDegree8CheckBox.isChecked()))
		self._view.euDegree9CheckBox.stateChanged.connect(lambda: self.change_degree(ItemFilterSettings.DegreesAll, 9, self._view.euDegree9CheckBox.isChecked()))
		self._view.euDegree10CheckBox.stateChanged.connect(lambda: self.change_degree(ItemFilterSettings.DegreesAll, 10, self._view.euDegree10CheckBox.isChecked()))
		self._view.euDegree11CheckBox.stateChanged.connect(lambda: self.change_degree(ItemFilterSettings.DegreesAll, 11, self._view.euDegree11CheckBox.isChecked()))
		self._view.euDegree12CheckBox.stateChanged.connect(lambda: self.change_degree(ItemFilterSettings.DegreesAll, 12, self._view.euDegree12CheckBox.isChecked()))

		self._view.euHeadPickCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.EUHead, 0, self._view.euHeadPickCheckBox.isChecked()))
		self._view.euHeadStoreCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.EUHead, 1, self._view.euHeadStoreCheckBox.isChecked()))
		self._view.euHeadSellCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.EUHead, 2, self._view.euHeadSellCheckBox.isChecked()))
		self._view.euHeadDropCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.EUHead, 3, self._view.euHeadDropCheckBox.isChecked()))

		self._view.euChestPickCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.EUChest, 0, self._view.euChestPickCheckBox.isChecked()))
		self._view.euChestStoreCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.EUChest, 1, self._view.euChestStoreCheckBox.isChecked()))
		self._view.euChestSellCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.EUChest, 2, self._view.euChestSellCheckBox.isChecked()))
		self._view.euChestDropCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.EUChest, 3, self._view.euChestDropCheckBox.isChecked()))

		self._view.euShoulderPickCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.EUShoulder, 0, self._view.euShoulderPickCheckBox.isChecked()))
		self._view.euShoulderStoreCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.EUShoulder, 1, self._view.euShoulderStoreCheckBox.isChecked()))
		self._view.euShoulderSellCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.EUShoulder, 2, self._view.euShoulderSellCheckBox.isChecked()))
		self._view.euShoulderDropCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.EUShoulder, 3, self._view.euShoulderDropCheckBox.isChecked()))

		self._view.euGlovePickCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.EUGloves, 0, self._view.euGlovePickCheckBox.isChecked()))
		self._view.euGloveStoreCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.EUGloves, 1, self._view.euGloveStoreCheckBox.isChecked()))
		self._view.euGloveSellCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.EUGloves, 2, self._view.euGloveSellCheckBox.isChecked()))
		self._view.euGloveDropCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.EUGloves, 3, self._view.euGloveDropCheckBox.isChecked()))

		self._view.euTrouserPickCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.EUHose, 0, self._view.euTrouserPickCheckBox.isChecked()))
		self._view.euTrouserStoreCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.EUHose, 1, self._view.euTrouserStoreCheckBox.isChecked()))
		self._view.euTrouserSellCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.EUHose, 2, self._view.euTrouserSellCheckBox.isChecked()))
		self._view.euTrouserDropCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.EUHose, 3, self._view.euTrouserDropCheckBox.isChecked()))

		self._view.euBootPickCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.EUBoots, 0, self._view.euBootPickCheckBox.isChecked()))
		self._view.euBootStoreCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.EUBoots, 1, self._view.euBootStoreCheckBox.isChecked()))
		self._view.euBootSellCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.EUBoots, 2, self._view.euBootSellCheckBox.isChecked()))
		self._view.euBootDropCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.EUBoots, 3, self._view.euBootDropCheckBox.isChecked()))

		self._view.euNecklacePickCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.EUNecklace, 0, self._view.euNecklacePickCheckBox.isChecked()))
		self._view.euNecklaceStoreCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.EUNecklace, 1, self._view.euNecklaceStoreCheckBox.isChecked()))
		self._view.euNecklaceSellCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.EUNecklace, 2, self._view.euNecklaceSellCheckBox.isChecked()))
		self._view.euNecklaceDropCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.EUNecklace, 3, self._view.euNecklaceDropCheckBox.isChecked()))

		self._view.euEarringPickCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.EUEarrings, 0, self._view.euEarringPickCheckBox.isChecked()))
		self._view.euEarringStoreCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.EUEarrings, 1, self._view.euEarringStoreCheckBox.isChecked()))
		self._view.euEarringSellCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.EUEarrings, 2, self._view.euEarringSellCheckBox.isChecked()))
		self._view.euEarringDropCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.EUEarrings, 3, self._view.euEarringDropCheckBox.isChecked()))

		self._view.euRingPickCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.EURing, 0, self._view.euRingPickCheckBox.isChecked()))
		self._view.euRingStoreCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.EURing, 1, self._view.euRingStoreCheckBox.isChecked()))
		self._view.euRingSellCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.EURing, 2, self._view.euRingSellCheckBox.isChecked()))
		self._view.euRingDropCheckBox.stateChanged.connect(lambda: self.change_item(ItemFilterSettings.EURing, 3, self._view.euRingDropCheckBox.isChecked()))

	def add_eu_filter_slots(self):
		ItemFilterSettings.DegreesAll.signal.connect(lambda x: self._view.euDegree1CheckBox.setChecked(x[11] == '1'))
		ItemFilterSettings.DegreesAll.signal.connect(lambda x: self._view.euDegree2CheckBox.setChecked(x[10] == '1'))
		ItemFilterSettings.DegreesAll.signal.connect(lambda x: self._view.euDegree3CheckBox.setChecked(x[9] == '1'))
		ItemFilterSettings.DegreesAll.signal.connect(lambda x: self._view.euDegree4CheckBox.setChecked(x[8] == '1'))
		ItemFilterSettings.DegreesAll.signal.connect(lambda x: self._view.euDegree5CheckBox.setChecked(x[7] == '1'))
		ItemFilterSettings.DegreesAll.signal.connect(lambda x: self._view.euDegree6CheckBox.setChecked(x[6] == '1'))
		ItemFilterSettings.DegreesAll.signal.connect(lambda x: self._view.euDegree7CheckBox.setChecked(x[5] == '1'))
		ItemFilterSettings.DegreesAll.signal.connect(lambda x: self._view.euDegree8CheckBox.setChecked(x[4] == '1'))
		ItemFilterSettings.DegreesAll.signal.connect(lambda x: self._view.euDegree9CheckBox.setChecked(x[3] == '1'))
		ItemFilterSettings.DegreesAll.signal.connect(lambda x: self._view.euDegree10CheckBox.setChecked(x[2] == '1'))
		ItemFilterSettings.DegreesAll.signal.connect(lambda x: self._view.euDegree11CheckBox.setChecked(x[1] == '1'))
		ItemFilterSettings.DegreesAll.signal.connect(lambda x: self._view.euDegree12CheckBox.setChecked(x[0] == '1'))


		ItemFilterSettings.EUHead.signal.connect(lambda x: self._view.euHeadPickCheckBox.setChecked(x[0] == '1'))
		ItemFilterSettings.EUHead.signal.connect(lambda x: self._view.euHeadStoreCheckBox.setChecked(x[1] == '1'))
		ItemFilterSettings.EUHead.signal.connect(lambda x: self._view.euHeadSellCheckBox.setChecked(x[2] == '1'))
		ItemFilterSettings.EUHead.signal.connect(lambda x: self._view.euHeadDropCheckBox.setChecked(x[3] == '1'))

		ItemFilterSettings.EUChest.signal.connect(lambda x: self._view.euChestPickCheckBox.setChecked(x[0] == '1'))
		ItemFilterSettings.EUChest.signal.connect(lambda x: self._view.euChestStoreCheckBox.setChecked(x[1] == '1'))
		ItemFilterSettings.EUChest.signal.connect(lambda x: self._view.euChestSellCheckBox.setChecked(x[2] == '1'))
		ItemFilterSettings.EUChest.signal.connect(lambda x: self._view.euChestDropCheckBox.setChecked(x[3] == '1'))

		ItemFilterSettings.EUShoulder.signal.connect(lambda x: self._view.euShoulderPickCheckBox.setChecked(x[0] == '1'))
		ItemFilterSettings.EUShoulder.signal.connect(lambda x: self._view.euShoulderStoreCheckBox.setChecked(x[1] == '1'))
		ItemFilterSettings.EUShoulder.signal.connect(lambda x: self._view.euShoulderSellCheckBox.setChecked(x[2] == '1'))
		ItemFilterSettings.EUShoulder.signal.connect(lambda x: self._view.euShoulderDropCheckBox.setChecked(x[3] == '1'))

		ItemFilterSettings.EUGloves.signal.connect(lambda x: self._view.euGlovePickCheckBox.setChecked(x[0] == '1'))
		ItemFilterSettings.EUGloves.signal.connect(lambda x: self._view.euGloveStoreCheckBox.setChecked(x[1] == '1'))
		ItemFilterSettings.EUGloves.signal.connect(lambda x: self._view.euGloveSellCheckBox.setChecked(x[2] == '1'))
		ItemFilterSettings.EUGloves.signal.connect(lambda x: self._view.euGloveDropCheckBox.setChecked(x[3] == '1'))

		ItemFilterSettings.EUHose.signal.connect(lambda x: self._view.euTrouserPickCheckBox.setChecked(x[0] == '1'))
		ItemFilterSettings.EUHose.signal.connect(lambda x: self._view.euTrouserStoreCheckBox.setChecked(x[1] == '1'))
		ItemFilterSettings.EUHose.signal.connect(lambda x: self._view.euTrouserSellCheckBox.setChecked(x[2] == '1'))
		ItemFilterSettings.EUHose.signal.connect(lambda x: self._view.euTrouserDropCheckBox.setChecked(x[3] == '1'))

		ItemFilterSettings.EUBoots.signal.connect(lambda x: self._view.euBootPickCheckBox.setChecked(x[0] == '1'))
		ItemFilterSettings.EUBoots.signal.connect(lambda x: self._view.euBootStoreCheckBox.setChecked(x[1] == '1'))
		ItemFilterSettings.EUBoots.signal.connect(lambda x: self._view.euBootSellCheckBox.setChecked(x[2] == '1'))
		ItemFilterSettings.EUBoots.signal.connect(lambda x: self._view.euBootDropCheckBox.setChecked(x[3] == '1'))

		ItemFilterSettings.EUNecklace.signal.connect(lambda x: self._view.euNecklacePickCheckBox.setChecked(x[0] == '1'))
		ItemFilterSettings.EUNecklace.signal.connect(lambda x: self._view.euNecklaceStoreCheckBox.setChecked(x[1] == '1'))
		ItemFilterSettings.EUNecklace.signal.connect(lambda x: self._view.euNecklaceSellCheckBox.setChecked(x[2] == '1'))
		ItemFilterSettings.EUNecklace.signal.connect(lambda x: self._view.euNecklaceDropCheckBox.setChecked(x[3] == '1'))

		ItemFilterSettings.EUEarrings.signal.connect(lambda x: self._view.euEarringPickCheckBox.setChecked(x[0] == '1'))
		ItemFilterSettings.EUEarrings.signal.connect(lambda x: self._view.euEarringStoreCheckBox.setChecked(x[1] == '1'))
		ItemFilterSettings.EUEarrings.signal.connect(lambda x: self._view.euEarringSellCheckBox.setChecked(x[2] == '1'))
		ItemFilterSettings.EUEarrings.signal.connect(lambda x: self._view.euEarringDropCheckBox.setChecked(x[3] == '1'))

		ItemFilterSettings.EURing.signal.connect(lambda x: self._view.euRingPickCheckBox.setChecked(x[0] == '1'))
		ItemFilterSettings.EURing.signal.connect(lambda x: self._view.euRingStoreCheckBox.setChecked(x[1] == '1'))
		ItemFilterSettings.EURing.signal.connect(lambda x: self._view.euRingSellCheckBox.setChecked(x[2] == '1'))
		ItemFilterSettings.EURing.signal.connect(lambda x: self._view.euRingDropCheckBox.setChecked(x[3] == '1'))

	def add_check_box_toggle(self):
		self._view.itemPickButton.mousePressEvent = lambda _: self.toggle_checkbox_column(self._view.itemGridLayout, 1)
		self._view.itemStoreButton.mousePressEvent = lambda _: self.toggle_checkbox_column(self._view.itemGridLayout, 2)
		self._view.itemSellButton.mousePressEvent = lambda _: self.toggle_checkbox_column(self._view.itemGridLayout, 3)
		self._view.itemDropButton.mousePressEvent = lambda _: self.toggle_checkbox_column(self._view.itemGridLayout, 4)

		self._view.jadeFilterPickButton.mousePressEvent = lambda _: self.toggle_checkbox_column(self._view.jadeGridLayout, 1)
		self._view.jadeFilterStoreButton.mousePressEvent = lambda _: self.toggle_checkbox_column(self._view.jadeGridLayout, 2)
		self._view.jadeFilterSellButton.mousePressEvent = lambda _: self.toggle_checkbox_column(self._view.jadeGridLayout, 3)
		self._view.jadeFilterDropButton.mousePressEvent = lambda _: self.toggle_checkbox_column(self._view.jadeGridLayout, 4)

		self._view.jadeFilterPickTabletButton.mousePressEvent = lambda _: self.toggle_checkbox_column(self._view.jadeTabletGridLayout, 1)
		self._view.jadeFilterStoreTabletButton.mousePressEvent = lambda _: self.toggle_checkbox_column(self._view.jadeTabletGridLayout, 2)
		self._view.jadeFilterSellTabletButton.mousePressEvent = lambda _: self.toggle_checkbox_column(self._view.jadeTabletGridLayout, 3)
		self._view.jadeFilterDropTabletButton.mousePressEvent = lambda _: self.toggle_checkbox_column(self._view.jadeTabletGridLayout, 4)

		self._view.rubyFilterPickButton.mousePressEvent = lambda _: self.toggle_checkbox_column(self._view.rubyFilterGridLayout, 1)
		self._view.rubyFilterStoreButton.mousePressEvent = lambda _: self.toggle_checkbox_column(self._view.rubyFilterGridLayout, 2)
		self._view.rubyFilterSellButton.mousePressEvent = lambda _: self.toggle_checkbox_column(self._view.rubyFilterGridLayout, 3)
		self._view.rubyFilterDropButton.mousePressEvent = lambda _: self.toggle_checkbox_column(self._view.rubyFilterGridLayout, 4)

		self._view.rubyFilterPickTabletButton.mousePressEvent = lambda _: self.toggle_checkbox_column(self._view.rubyTabletGridLayout, 1)
		self._view.rubyFilterStoreTabletButton.mousePressEvent = lambda _: self.toggle_checkbox_column(self._view.rubyTabletGridLayout, 2)
		self._view.rubyFilterSellTabletButton.mousePressEvent = lambda _: self.toggle_checkbox_column(self._view.rubyTabletGridLayout, 3)
		self._view.rubyFilterDropTabletButton.mousePressEvent = lambda _: self.toggle_checkbox_column(self._view.rubyTabletGridLayout, 4)

		self._view.pickFilterButton.mousePressEvent = lambda _: self.toggle_checkbox_column(self._view.equipmentGridLayout, 1)
		self._view.storeFilterButton.mousePressEvent = lambda _: self.toggle_checkbox_column(self._view.equipmentGridLayout, 2)
		self._view.sellFilterButton.mousePressEvent = lambda _: self.toggle_checkbox_column(self._view.equipmentGridLayout, 3)
		self._view.dropFilterButton.mousePressEvent = lambda _: self.toggle_checkbox_column(self._view.equipmentGridLayout, 4)

		self._view.euWeaponPickFilterButton.mousePressEvent = lambda _: self.toggle_checkbox_column(self._view.euWeaponGridLayout, 1)
		self._view.euWeaponStoreFilterButton.mousePressEvent = lambda _: self.toggle_checkbox_column(self._view.euWeaponGridLayout, 2)
		self._view.euWeaponSellFilterButton.mousePressEvent = lambda _: self.toggle_checkbox_column(self._view.euWeaponGridLayout, 3)
		self._view.euWeaponDropFilterButton.mousePressEvent = lambda _: self.toggle_checkbox_column(self._view.euWeaponGridLayout, 4)

		self._view.euEquipmentPickFilterButton.mousePressEvent = lambda _: self.toggle_checkbox_column(self._view.euEquipmentGridLayout, 1)
		self._view.euEquipmentStoreFilterButton.mousePressEvent = lambda _: self.toggle_checkbox_column(self._view.euEquipmentGridLayout, 2)
		self._view.euEquipmentSellFilterButton.mousePressEvent = lambda _: self.toggle_checkbox_column(self._view.euEquipmentGridLayout, 3)
		self._view.euEquipmentDropFilterButton.mousePressEvent = lambda _: self.toggle_checkbox_column(self._view.euEquipmentGridLayout, 4)

		self._view.chinaDegreeButton.mousePressEvent = lambda _: self.toggle_checkbox_column(self._view.equipmentGridLayout, 5)
		self._view.euWeaponDegreeButton.mousePressEvent = lambda _: self.toggle_checkbox_column(self._view.euWeaponGridLayout, 5)
		self._view.euEquipmentDegreeButton.mousePressEvent = lambda _: self.toggle_checkbox_column(self._view.euEquipmentGridLayout, 5)

	def _load_settings(self):
		#ItemFilterSettings.DegreesAll.signal.emit(ItemFilterSettings.DegreesAll.value)

		ItemFilterSettings.HPPots.signal.emit(ItemFilterSettings.HPPots.value)
		ItemFilterSettings.MPPots.signal.emit(ItemFilterSettings.MPPots.value)
		ItemFilterSettings.VigorPots.signal.emit(ItemFilterSettings.VigorPots.value)
		ItemFilterSettings.Grains.signal.emit(ItemFilterSettings.Grains.value)
		ItemFilterSettings.UniversalPills.signal.emit(ItemFilterSettings.UniversalPills.value)
		ItemFilterSettings.WeaponElixir.signal.emit(ItemFilterSettings.WeaponElixir.value)
		ItemFilterSettings.ProtectorElixir.signal.emit(ItemFilterSettings.ProtectorElixir.value)
		ItemFilterSettings.ShieldElixir.signal.emit(ItemFilterSettings.ShieldElixir.value)
		ItemFilterSettings.AccessoryElixir.signal.emit(ItemFilterSettings.AccessoryElixir.value)
		ItemFilterSettings.AlchemyMaterial.signal.emit(ItemFilterSettings.AlchemyMaterial.value)
		ItemFilterSettings.ReturnScrolls.signal.emit(ItemFilterSettings.ReturnScrolls.value)
		ItemFilterSettings.Arrows.signal.emit(ItemFilterSettings.Arrows.value)
		ItemFilterSettings.Bolts.signal.emit(ItemFilterSettings.Bolts.value)
		ItemFilterSettings.COSItems.signal.emit(ItemFilterSettings.COSItems.value)

		ItemFilterSettings.QuestItems.signal.emit(str(ItemFilterSettings.QuestItems.value))
		ItemFilterSettings.Gold.signal.emit(str(ItemFilterSettings.Gold.value))
		ItemFilterSettings.FreeItems.signal.emit(str(ItemFilterSettings.FreeItems.value))
		ItemFilterSettings.OwnItems.signal.emit(str(ItemFilterSettings.OwnItems.value))

		AlchemyFilterSettings.FilterJadeStrength.signal.emit(AlchemyFilterSettings.FilterJadeStrength.value)
		AlchemyFilterSettings.FilterJadeIntelligence.signal.emit(AlchemyFilterSettings.FilterJadeIntelligence.value)
		AlchemyFilterSettings.FilterJadeMaster.signal.emit(AlchemyFilterSettings.FilterJadeMaster.value)
		AlchemyFilterSettings.FilterJadeStrikes.signal.emit(AlchemyFilterSettings.FilterJadeStrikes.value)
		AlchemyFilterSettings.FilterJadeDiscipline.signal.emit(AlchemyFilterSettings.FilterJadeDiscipline.value)
		AlchemyFilterSettings.FilterJadePenetration.signal.emit(AlchemyFilterSettings.FilterJadePenetration.value)
		AlchemyFilterSettings.FilterJadeDodging.signal.emit(AlchemyFilterSettings.FilterJadeDodging.value)
		AlchemyFilterSettings.FilterJadeStamina.signal.emit(AlchemyFilterSettings.FilterJadeStamina.value)
		AlchemyFilterSettings.FilterJadeMagic.signal.emit(AlchemyFilterSettings.FilterJadeMagic.value)
		AlchemyFilterSettings.FilterJadeFogs.signal.emit(AlchemyFilterSettings.FilterJadeFogs.value)
		AlchemyFilterSettings.FilterJadeAir.signal.emit(AlchemyFilterSettings.FilterJadeAir.value)
		AlchemyFilterSettings.FilterJadeFire.signal.emit(AlchemyFilterSettings.FilterJadeFire.value)
		AlchemyFilterSettings.FilterJadeImmunity.signal.emit(AlchemyFilterSettings.FilterJadeImmunity.value)
		AlchemyFilterSettings.FilterJadeRevival.signal.emit(AlchemyFilterSettings.FilterJadeRevival.value)
		AlchemyFilterSettings.FilterJadeSteady.signal.emit(AlchemyFilterSettings.FilterJadeSteady.value)
		AlchemyFilterSettings.FilterJadeLuck.signal.emit(AlchemyFilterSettings.FilterJadeLuck.value)

		AlchemyFilterSettings.JadeTabletStrength.signal.emit(AlchemyFilterSettings.JadeTabletStrength.value)
		AlchemyFilterSettings.JadeTabletIntelligence.signal.emit(AlchemyFilterSettings.JadeTabletIntelligence.value)
		AlchemyFilterSettings.JadeTabletMaster.signal.emit(AlchemyFilterSettings.JadeTabletMaster.value)
		AlchemyFilterSettings.JadeTabletStrikes.signal.emit(AlchemyFilterSettings.JadeTabletStrikes.value)
		AlchemyFilterSettings.JadeTabletDiscipline.signal.emit(AlchemyFilterSettings.JadeTabletDiscipline.value)
		AlchemyFilterSettings.JadeTabletPenetration.signal.emit(AlchemyFilterSettings.JadeTabletPenetration.value)
		AlchemyFilterSettings.JadeTabletDodging.signal.emit(AlchemyFilterSettings.JadeTabletDodging.value)
		AlchemyFilterSettings.JadeTabletStamina.signal.emit(AlchemyFilterSettings.JadeTabletStamina.value)
		AlchemyFilterSettings.JadeTabletMagic.signal.emit(AlchemyFilterSettings.JadeTabletMagic.value)
		AlchemyFilterSettings.JadeTabletFogs.signal.emit(AlchemyFilterSettings.JadeTabletFogs.value)
		AlchemyFilterSettings.JadeTabletAir.signal.emit(AlchemyFilterSettings.JadeTabletAir.value)
		AlchemyFilterSettings.JadeTabletFire.signal.emit(AlchemyFilterSettings.JadeTabletFire.value)
		AlchemyFilterSettings.JadeTabletImmunity.signal.emit(AlchemyFilterSettings.JadeTabletImmunity.value)
		AlchemyFilterSettings.JadeTabletRevival.signal.emit(AlchemyFilterSettings.JadeTabletRevival.value)
		AlchemyFilterSettings.JadeTabletSteady.signal.emit(AlchemyFilterSettings.JadeTabletSteady.value)
		AlchemyFilterSettings.JadeTabletLuck.signal.emit(AlchemyFilterSettings.JadeTabletLuck.value)

		AlchemyFilterSettings.FilterRubyCourage.signal.emit(AlchemyFilterSettings.FilterRubyCourage.value)
		AlchemyFilterSettings.FilterRubyWarriors.signal.emit(AlchemyFilterSettings.FilterRubyWarriors.value)
		AlchemyFilterSettings.FilterRubyPhilosophy.signal.emit(AlchemyFilterSettings.FilterRubyPhilosophy.value)
		AlchemyFilterSettings.FilterRubyMeditation.signal.emit(AlchemyFilterSettings.FilterRubyMeditation.value)
		AlchemyFilterSettings.FilterRubyChallenge.signal.emit(AlchemyFilterSettings.FilterRubyChallenge.value)
		AlchemyFilterSettings.FilterRubyFocus.signal.emit(AlchemyFilterSettings.FilterRubyFocus.value)
		AlchemyFilterSettings.FilterRubyFlesh.signal.emit(AlchemyFilterSettings.FilterRubyFlesh.value)
		AlchemyFilterSettings.FilterRubyLife.signal.emit(AlchemyFilterSettings.FilterRubyLife.value)
		AlchemyFilterSettings.FilterRubyMind.signal.emit(AlchemyFilterSettings.FilterRubyMind.value)
		AlchemyFilterSettings.FilterRubySpirit.signal.emit(AlchemyFilterSettings.FilterRubySpirit.value)
		AlchemyFilterSettings.FilterRubyDodging.signal.emit(AlchemyFilterSettings.FilterRubyDodging.value)
		AlchemyFilterSettings.FilterRubyAgility.signal.emit(AlchemyFilterSettings.FilterRubyAgility.value)
		AlchemyFilterSettings.FilterRubyTraining.signal.emit(AlchemyFilterSettings.FilterRubyTraining.value)
		AlchemyFilterSettings.FilterRubyPrayer.signal.emit(AlchemyFilterSettings.FilterRubyPrayer.value)

		AlchemyFilterSettings.RubyTabletCourage.signal.emit(AlchemyFilterSettings.RubyTabletCourage.value)
		AlchemyFilterSettings.RubyTabletWarriors.signal.emit(AlchemyFilterSettings.RubyTabletWarriors.value)
		AlchemyFilterSettings.RubyTabletPhilosophy.signal.emit(AlchemyFilterSettings.RubyTabletPhilosophy.value)
		AlchemyFilterSettings.RubyTabletMeditation.signal.emit(AlchemyFilterSettings.RubyTabletMeditation.value)
		AlchemyFilterSettings.RubyTabletChallenge.signal.emit(AlchemyFilterSettings.RubyTabletChallenge.value)
		AlchemyFilterSettings.RubyTabletFocus.signal.emit(AlchemyFilterSettings.RubyTabletFocus.value)
		AlchemyFilterSettings.RubyTabletFlesh.signal.emit(AlchemyFilterSettings.RubyTabletFlesh.value)
		AlchemyFilterSettings.RubyTabletLife.signal.emit(AlchemyFilterSettings.RubyTabletLife.value)
		AlchemyFilterSettings.RubyTabletMind.signal.emit(AlchemyFilterSettings.RubyTabletMind.value)
		AlchemyFilterSettings.RubyTabletSpirit.signal.emit(AlchemyFilterSettings.RubyTabletSpirit.value)
		AlchemyFilterSettings.RubyTabletDodging.signal.emit(AlchemyFilterSettings.RubyTabletDodging.value)
		AlchemyFilterSettings.RubyTabletAgility.signal.emit(AlchemyFilterSettings.RubyTabletAgility.value)
		AlchemyFilterSettings.RubyTabletTraining.signal.emit(AlchemyFilterSettings.RubyTabletTraining.value)
		AlchemyFilterSettings.RubyTabletPrayer.signal.emit(AlchemyFilterSettings.RubyTabletPrayer.value)

		ItemFilterSettings.DegreesAll.signal.emit(ItemFilterSettings.DegreesAll.value)
		ItemFilterSettings.Blade.signal.emit(ItemFilterSettings.Blade.value)
		ItemFilterSettings.Sword.signal.emit(ItemFilterSettings.Sword.value)
		ItemFilterSettings.Spear.signal.emit(ItemFilterSettings.Spear.value)
		ItemFilterSettings.Glaive.signal.emit(ItemFilterSettings.Glaive.value)
		ItemFilterSettings.Bow.signal.emit(ItemFilterSettings.Bow.value)
		ItemFilterSettings.Shield.signal.emit(ItemFilterSettings.Shield.value)
		ItemFilterSettings.Head.signal.emit(ItemFilterSettings.Head.value)
		ItemFilterSettings.Chest.signal.emit(ItemFilterSettings.Chest.value)
		ItemFilterSettings.Shoulder.signal.emit(ItemFilterSettings.Shoulder.value)
		ItemFilterSettings.Glove.signal.emit(ItemFilterSettings.Glove.value)
		ItemFilterSettings.Hose.signal.emit(ItemFilterSettings.Hose.value)
		ItemFilterSettings.Boots.signal.emit(ItemFilterSettings.Boots.value)
		ItemFilterSettings.Necklace.signal.emit(ItemFilterSettings.Necklace.value)
		ItemFilterSettings.Earrings.signal.emit(ItemFilterSettings.Earrings.value)
		ItemFilterSettings.Ring.signal.emit(ItemFilterSettings.Ring.value)

		ItemFilterSettings.DegreesAll.signal.emit(ItemFilterSettings.DegreesAll.value)
		ItemFilterSettings.EUAxe.signal.emit(ItemFilterSettings.EUAxe.value)
		ItemFilterSettings.EUSword1H.signal.emit(ItemFilterSettings.EUSword1H.value)
		ItemFilterSettings.EUSword2H.signal.emit(ItemFilterSettings.EUSword2H.value)
		ItemFilterSettings.EUCrossbow.signal.emit(ItemFilterSettings.EUCrossbow.value)
		ItemFilterSettings.EUDagger.signal.emit(ItemFilterSettings.EUDagger.value)
		ItemFilterSettings.EUWizardStaff.signal.emit(ItemFilterSettings.EUWizardStaff.value)
		ItemFilterSettings.EUWarlockStaff.signal.emit(ItemFilterSettings.EUWarlockStaff.value)
		ItemFilterSettings.EUHarp.signal.emit(ItemFilterSettings.EUHarp.value)
		ItemFilterSettings.EUClericStaff.signal.emit(ItemFilterSettings.EUClericStaff.value)
		ItemFilterSettings.EUShield.signal.emit(ItemFilterSettings.EUShield.value)
		ItemFilterSettings.EUHead.signal.emit(ItemFilterSettings.EUHead.value)
		ItemFilterSettings.EUChest.signal.emit(ItemFilterSettings.EUChest.value)
		ItemFilterSettings.EUShoulder.signal.emit(ItemFilterSettings.EUShoulder.value)
		ItemFilterSettings.EUGloves.signal.emit(ItemFilterSettings.EUGloves.value)
		ItemFilterSettings.EUHose.signal.emit(ItemFilterSettings.EUHose.value)
		ItemFilterSettings.EUBoots.signal.emit(ItemFilterSettings.EUBoots.value)
		ItemFilterSettings.EUNecklace.signal.emit(ItemFilterSettings.EUNecklace.value)
		ItemFilterSettings.EUEarrings.signal.emit(ItemFilterSettings.EUEarrings.value)
		ItemFilterSettings.EURing.signal.emit(ItemFilterSettings.EURing.value)

	def _connect_signals(self):
		self.add_item_filters()
		self.add_item_filter_slots()
		self.add_jade_item_filters()
		self.add_jade_item_filter_slots()
		self.add_jade_tablet_filters()

		self.add_jade_tablet_filter_slots()
		self.add_ruby_item_filters()
		self.add_ruby_item_filter_slots()
		self.add_ruby_tablet_filters()
		self.add_ruby_tablet_filter_slots()

		self.add_ch_filters()
		self.add_ch_filter_slots()
		self.add_eu_filters()
		self.add_eu_filter_slots()
		self.add_eu_weapon_filters()
		self.add_eu_weapon_filter_slots()

		self.add_check_box_toggle()
