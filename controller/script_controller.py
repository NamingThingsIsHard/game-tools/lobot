import os
from typing import Dict

from PyQt5.QtWidgets import QFileDialog

from controller.controller import Controller
from lobot_api.Bot import Bot
from lobot_api.binding import LobotProperty
from lobot_api.connection import proxy
from lobot_api.globals.DirectoryGlobal import DirectoryGlobal
from lobot_api.globals.SpawnListsGlobal import SpawnListsGlobal
from lobot_api.globals.settings.LoopSettings import ScriptSettings
from lobot_api.packets.char.CharDataHandler import CharDataHandler
from lobot_api.packets.char.Movement import Movement
from lobot_api.packets.char.MovementArgs import MovementArgs
from silkroad_security_api_1_4.Packet import Packet


class ScriptController(Controller):
	is_recording = False

	def speed_buff_toggled(self):
		ScriptSettings.SpeedBuffOrPotion.value = str(self._view.useSpeedBuffRadioButton.checked())

	def choose_walk_script(self):
		path = DirectoryGlobal.cwd()
		if 'data' in os.listdir():
			path / 'data'
		file = QFileDialog.getOpenFileName(self._view, self._view.tr("Select script file"), str(path))

		if file[0]:
			self._view.walkScriptLineEdit.setText(file[0])

		return file[0] and len(file[0]) > 0

	# TODO load walk script

	def choose_quest_script(self):
		path = DirectoryGlobal.cwd()
		if 'data' in os.listdir():
			path / 'data'
		file = QFileDialog.getOpenFileName(self._view, self._view.tr("Select script file"), str(path))

		if file[0]:
			self._view.questScriptLineEdit.setText(file[0])

		return file[0]

	# TODO load quest script

	def set_bool(self, obj, value):
		if isinstance(obj, LobotProperty):
			obj.value = value > 0
		else:
			obj = value > 0

	def speef_buff_toggled(self):
		ScriptSettings.SpeedBuffOrPotion.slot(self._view.useSpeedPotionRadioButton.isChecked())

	def set_speed_potion(self, i):
		ScriptSettings.speed_potion_id.value = '7099' \
			if i == 0 \
			else '7100'

	def handle_movement(self, e: MovementArgs):
		if Bot.char_data.unique_id == e.unique_id\
				or Bot.transport.unique_id == e.unique_id:
			self._view.scriptTextEdit.append(f"go {e.destination.x}, {e.destination.y}")

	def handle_teleportation_packet(self, packet_dict: Dict):
		if packet_dict['opcode'] == 0x705A:
			p = Packet(packet_dict['opcode'], packet_dict['encrypted'], packet_dict['massive'], packet_dict['data'])
			p.lock()
			npc_id = p.read_uint32()
			p.read_uint8()
			teleport_option = p.read_uint32()
			npc = SpawnListsGlobal.Portals.get(npc_id, None)
			if npc:
				self._view.scriptTextEdit.append(f"teleport {npc.ref_id}, {teleport_option}")

	def handle_record_wait_command(self):
		self._view.scriptTextEdit.append("wait")

	def record_script(self):
		if self._view.recordPushButton.text() == "Record":
			Movement.on_botmovement_registered += self.handle_movement
			proxy.on_client_sent_teleport_packet += self.handle_teleportation_packet
			CharDataHandler.on_packet += self.handle_record_wait_command
			self._view.recordPushButton.setText("Recording")
			self._view.recordPushButton.setStyleSheet("background-color: rgb(255, 255, 127);")
		else:
			Movement.on_botmovement_registered -= self.handle_movement
			proxy.on_client_sent_teleport_packet -= self.handle_teleportation_packet
			CharDataHandler.on_packet -= self.handle_record_wait_command
			self._view.recordPushButton.setText("Record")
			self._view.recordPushButton.setStyleSheet("")

	def load_script(self):
		path = DirectoryGlobal.cwd()
		if 'data' in os.listdir():
			path / 'data'
		file = QFileDialog.getOpenFileName(self._view, self._view.tr("Select script file"), str(path))

		if file[0]:
			with open(file[0], 'r') as f:
				self._view.scriptTextEdit.setText(f.read())

	def save_script(self):
		script_text = self._view.scriptTextEdit.toPlainText()
		if script_text == "":
			return

		options = QFileDialog.Options()
		options |= QFileDialog.DontUseNativeDialog
		if not os.path.exists(DirectoryGlobal.script_folder()):
			os.makedirs(DirectoryGlobal.script_folder(), exist_ok=True)
		directory = str(DirectoryGlobal.script_folder())
		file_name, _ = QFileDialog.getSaveFileName(self._view, "QFileDialog.getSaveFileName()", directory,
		                                           "All Files (*);;Text Files (*.txt)", options=options)
		if file_name:
			with open(file_name, 'w+') as f:
				f.write(script_text)
				print(f"Saved script as {file_name}")

	def _load_settings(self):
		ScriptSettings.RecordWalk.signal[bool].emit(ScriptSettings.RecordWalk.value)
		ScriptSettings.RecordSkills.signal[bool].emit(ScriptSettings.RecordSkills.value)
		ScriptSettings.RecordQuests.signal[bool].emit(ScriptSettings.RecordQuests.value)

		ScriptSettings.TrainingScriptPath.signal.emit(ScriptSettings.TrainingScriptPath.value)
		ScriptSettings.QuestScriptPath.signal.emit(ScriptSettings.QuestScriptPath.value)
		ScriptSettings.UseReverseReturn.signal[bool].emit(ScriptSettings.UseReverseReturn.value)
		ScriptSettings.SwitchToShield.signal[bool].emit(ScriptSettings.SwitchToShield.value)
		ScriptSettings.RideHorse.signal[bool].emit(ScriptSettings.RideHorse.value)
		ScriptSettings.UseSpeedScroll.signal[bool].emit(ScriptSettings.UseSpeedScroll.value)

		if ScriptSettings.SpeedBuffOrPotion.value:
			self._view.useSpeedPotionRadioButton.setChecked(True)
		else:
			self._view.useSpeedBuffRadioButton.setChecked(True)

		if ScriptSettings.speed_potion_id == 7100:
			self._view.speedPotionComboBox.setCurrentIndex(1)
		else:
			self._view.speedPotionComboBox.setCurrentIndex(0)

		self._view.reverseReturnComboBox.setCurrentIndex(ScriptSettings.ReverseReturnOption.value)

	def _connect_signals(self):
		self._view.recordWalkCheckBox.stateChanged.connect(lambda x: self.set_bool(ScriptSettings.RecordWalk, x))
		ScriptSettings.RecordWalk.signal[bool].connect(lambda x: self._view.recordWalkCheckBox.setChecked(x))
		self._view.recordSkillCheckBox.stateChanged.connect(
			lambda x: self.set_bool(ScriptSettings.RecordSkills.slot, x))
		ScriptSettings.RecordSkills.signal[bool].connect(lambda x: self._view.recordSkillCheckBox.setChecked(x))
		self._view.recordQuestsCheckBox.stateChanged.connect(
			lambda x: self.set_bool(ScriptSettings.RecordQuests.slot, x))
		ScriptSettings.RecordQuests.signal[bool].connect(lambda x: self._view.recordQuestsCheckBox.setChecked(x))

		self._view.walkScriptToolButton.clicked.connect(self.choose_walk_script)
		self._view.questScriptToolButton.clicked.connect(self.choose_quest_script)

		self._view.walkScriptLineEdit.textChanged.connect(ScriptSettings.TrainingScriptPath.slot)
		ScriptSettings.TrainingScriptPath.signal.connect(self._view.walkScriptLineEdit.setText)
		self._view.questScriptLineEdit.textChanged.connect(ScriptSettings.QuestScriptPath.slot)
		ScriptSettings.QuestScriptPath.signal.connect(self._view.questScriptLineEdit.setText)

		self._view.useReverseReturnCheckBox.stateChanged.connect(
			lambda x: self.set_bool(ScriptSettings.UseReverseReturn, x))
		ScriptSettings.UseReverseReturn.signal[bool].connect(
			lambda x: self._view.useReverseReturnCheckBox.setChecked(x))
		self._view.switchToShieldCheckBox.stateChanged.connect(
			lambda x: self.set_bool(ScriptSettings.SwitchToShield, x))
		ScriptSettings.SwitchToShield.signal[bool].connect(lambda x: self._view.switchToShieldCheckBox.setChecked(x))
		self._view.rideHorseCheckBox.stateChanged.connect(lambda x: self.set_bool(ScriptSettings.RideHorse, x))
		ScriptSettings.RideHorse.signal[bool].connect(lambda x: self._view.rideHorseCheckBox.setChecked(x))
		self._view.useSpeedScrollsCheckBox.stateChanged.connect(
			lambda x: self.set_bool(ScriptSettings.UseSpeedScroll, x))
		ScriptSettings.UseSpeedScroll.signal[bool].connect(lambda x: self._view.useSpeedScrollsCheckBox.setChecked(x))

		self._view.useSpeedPotionRadioButton.toggled.connect(self.speef_buff_toggled)
		self._view.speedPotionComboBox.currentIndexChanged.connect(self.set_speed_potion)
		self._view.reverseReturnComboBox.currentIndexChanged.connect(ScriptSettings.ReverseReturnOption.slot)

		self._view.recordPushButton.clicked.connect(self.record_script)
		self._view.loadPushButton.clicked.connect(self.load_script)
		self._view.savePushButton.clicked.connect(self.save_script)
