import os
from typing import Dict, Optional, Tuple

from PyQt5.QtCore import Qt, QRect, QPointF
from PyQt5.QtGui import QPixmap, QBrush, QPainter, QIntValidator, QPen
from PyQt5.QtWidgets import QTableWidgetItem, QAbstractItemView, QHeaderView, QGraphicsScene, QGraphicsItem, \
	QGraphicsEllipseItem, QFrame, QGraphicsSceneMouseEvent

from controller.controller import Controller
from lobot_api.Bot import Bot
from lobot_api.connection import proxy
from lobot_api.globals.DirectoryGlobal import DirectoryGlobal
from lobot_api.globals.SpawnListsGlobal import SpawnListsGlobal, SpawnList
from lobot_api.globals.settings.TrainingSettings import TrainingAreaSettings
from lobot_api.packets.char.CharStopMovementHandler import CharStopMovementHandler
from lobot_api.packets.char.Movement import Movement
from lobot_api.packets.char.MovementArgs import MovementArgs
from lobot_api.packets.npc.DespawnArgs import DespawnArgs
from lobot_api.packets.npc.NPCTeleport import NPCTeleport
from lobot_api.packets.spawn.SpawnHandler import ParseChar, ParseMonster, ParseItem, SpawnHandler
from lobot_api.scripting import ScriptUtils
from lobot_api.scripting.Area import Area
from lobot_api.scripting.Point import Point

spawn_list_to_color: Dict[SpawnList, int] = {
	SpawnList.none: Qt.yellow,
	SpawnList.Drops: Qt.black,
	SpawnList.MonsterSpawns: Qt.red,
	SpawnList.NPCs: Qt.blue,
	SpawnList.Portals: Qt.blue,
	SpawnList.Players: Qt.darkGreen,
	SpawnList.Pets: Qt.black
}


class MapController(Controller):
	"""
	array of map tiles used to create_request whole map, indexed from top left to bottom right:
	012
	345
	678
	"""
	tiles: QPixmap = [None] * 9
	edge_size = 256
	painter = QPainter()

	def __init__(self, view):
		super(MapController, self).__init__(view)
		"""variable to keep track of views"""
		self.previous_button = self._view.playersPushButton
		self._view.playersPushButton.setEnabled(False)

		self.empty_pixmap: QPixmap = QPixmap()
		self.empty_pixmap.fill(Qt.white)
		# use to scale 256x256 map tile to map in bot
		self.map_scaling_factor = 7  # self._view.mapGraphicsView.width() / 192
		self.area_radius_factor = 256 / 192

		# position of current map tile
		self.current_position = Point(168, 97, 0, 0, 0, 0)
		self.current_map: Optional[QPixmap] = None
		# all items to be displayed on map
		self.graphics_items: Dict[int, QGraphicsItem] = {}
		# scene to be displayed in UI
		self.graphics_scene = QGraphicsScene(0, 0, 401, 401, self._view.mapGraphicsView)
		self.graphics_scene.mousePressEvent = self.handle_map_click
		# self.graphics_scene = QGraphicsScene(0, 0, self._view.mapGraphicsView.width(), self._view.mapGraphicsView.width(), self._view.mapGraphicsView)
		self._view.mapGraphicsView.setViewportMargins(-2, -2, -2, -2)
		self._view.mapGraphicsView.setFrameStyle(QFrame.NoFrame)
		self._view.mapGraphicsView.setScene(self.graphics_scene)
		# self._load_map_(Bot.char_data.position.x_sector, Bot.char_data.position.y_sector)
		self._load_map_parts(168, 97)
		self.get_area_scene_coordinates(1, TrainingAreaSettings.instance().area1)
		self.get_area_scene_coordinates(2, TrainingAreaSettings.instance().area2)

	def set_table_view(self):
		self._view.spawnTableWidget.setEditTriggers(QAbstractItemView.NoEditTriggers)
		self._view.spawnTableWidget.setSelectionBehavior(QAbstractItemView.SelectRows)
		self._view.spawnTableWidget.clear()
		self._view.spawnTableWidget.setColumnCount(2)
		self._view.spawnTableWidget.setHorizontalHeaderLabels(['ID', 'Name'])

		self._view.spawnTableWidget.setColumnWidth(0, self._view.spawnTableWidget.width() / 4)
		self._view.spawnTableWidget.horizontalHeader().setStretchLastSection(True)
		self._view.spawnTableWidget.horizontalHeader().setSectionResizeMode(QHeaderView.Fixed)
		self._view.spawnTableWidget.verticalHeader().setVisible(False)

	def update_table(self, collection):
		if collection == {}:
			pass
		length = len(collection)
		self._view.spawnTableWidget.setRowCount(length)
		for i in range(0, length):
			item = list(collection.values())[i]
			self._view.spawnTableWidget.setItem(i, 0, QTableWidgetItem(format(item.unique_id, '02X')))
			self._view.spawnTableWidget.setItem(i, 1, QTableWidgetItem(item.name))

	def update_table_with_collection(self, collection, button):
		self.update_table(collection)
		# prevent loading same list
		self.previous_button.setEnabled(True)
		button.setEnabled(False)
		self.previous_button = button

	def handle_list_changed(self, collection, is_target):
		if is_target:
			self.update_table(collection)

	def _load_map_parts(self, x_sector, y_sector):
		x_offset_factor = self.current_position.x_sector - x_sector
		y_offset_factor = y_sector - self.current_position.y_sector
		if abs(x_offset_factor) > 2 or abs(y_offset_factor) > 2:
			self.graphics_scene.clear()
			self.graphics_items.clear()
			self.current_position.x_sector = x_sector
			self.current_position.y_sector = y_sector
			self.get_area_scene_coordinates(1, TrainingAreaSettings.instance().area1)
			self.get_area_scene_coordinates(2, TrainingAreaSettings.instance().area2)
		else:
			list_except_bot = filter(lambda x: x[0] != Bot.char_data.unique_id, self.graphics_items.items())

			for k, v in list_except_bot:
				v.moveBy(x_offset_factor * 133, y_offset_factor * 133)

		pim: QPixmap = QPixmap(768, 768)
		painter = QPainter(pim)
		# painter.save()

		for i in range(3):
			self.tiles[i] = QPixmap(f'{DirectoryGlobal.map_folder()}/{x_sector - 1}x{y_sector + 1 - i}.jpg')
			painter.drawPixmap(QRect(0, i * 256, 256, 256), self.tiles[i])
			self.tiles[i + 1] = QPixmap(f'{DirectoryGlobal.map_folder()}/{x_sector}x{y_sector + 1 - i}.jpg')
			painter.drawPixmap(QRect(256, i * 256, 256, 256), self.tiles[i + 1])
			self.tiles[i + 2] = QPixmap(f'{DirectoryGlobal.map_folder()}/{x_sector + 1}x{y_sector + 1 - i}.jpg')
			painter.drawPixmap(QRect(512, i * 256, 256, 256), self.tiles[i + 2])

		self.current_map = pim
		self.graphics_scene.setBackgroundBrush(QBrush(pim.scaled(401, 401)))
		# painter.restore()
		painter.end()

	def _load_map_(self, x_sector, y_sector):
		self.graphics_scene.clear()
		self.graphics_items.clear()
		pim: QPixmap = QPixmap(f'{DirectoryGlobal.map_folder()}/{x_sector}x{y_sector}.jpg')
		# replace with black if not found
		if not pim or pim.width() < 256 or pim.height() < 256:
			pim = self.empty_pixmap
		self.current_map = pim
		self.graphics_scene.setBackgroundBrush(QBrush(pim))

	# self.graphics_scene.setBackgroundBrush(
	# QBrush(pim.scaled(self._view.mapGraphicsView.width(), self._view.mapGraphicsView.height())))
	# if self.current_pixmap is not None:
	# 	self.graphics_scene.removeItem(self.current_pixmap)
	# self.current_pixmap = QGraphicsPixmapItem(pim)
	# self.scene.addItem(self.current_pixmap)

	def get_scene_coordinates(self, p: Point) -> Tuple[int, int]:
		if p.x_offset == 0 and p.y_offset == 0:
			pass
		x_tile_offset = 133 * (p.x_sector - self.current_position.x_sector)
		y_offset_factor = p.y_sector - self.current_position.y_sector
		y_tile_offset = 133 * y_offset_factor

		map_x = 133 + (p.x_offset / 100) * self.map_scaling_factor + x_tile_offset
		map_y = 266 - (p.y_offset / 100) * self.map_scaling_factor - y_tile_offset
		return map_x, map_y

	def get_scene_coordinates_and_set(self, unique_id, p: Point, spawn_list: SpawnList):
		map_x, map_y = self.get_scene_coordinates(p)

		if unique_id not in self.graphics_items:
			color = spawn_list_to_color.get(spawn_list, Qt.black)
			self.graphics_items[unique_id] = QGraphicsEllipseItem(0, 0, 5, 5)  # 0,0 to be same as parent coordinates
			self.graphics_items[unique_id].setZValue(spawn_list.value + 5)
			self.graphics_items[unique_id].setBrush(QBrush(color))

			self.graphics_scene.addItem(self.graphics_items[unique_id])
			self.graphics_items[unique_id].setPos(map_x, map_y)
			# finally set position here for correct scene coordinate
		# debug
		else:
			self.graphics_items[unique_id].setPos(map_x, map_y)

	def get_area_scene_coordinates(self, area_id, area: Area):
		p = Point(area.get_x_sector, area.get_y_sector, area.get_x_offset, 0, area.get_y_offset)
		radius = area.radius * self.area_radius_factor
		map_x, map_y = self.get_scene_coordinates(p)
		map_x = map_x - radius / 2  # adjust to show circle based on its center point
		map_y = map_y - radius / 2
		if area_id not in self.graphics_items:
			self.graphics_items[area_id] = QGraphicsEllipseItem(0, 0, radius,
			                                                    radius)  # 0,0 to be same as parent coordinates
			self.graphics_items[area_id].setZValue(50)
			self.graphics_items[area_id].setPen(QPen(Qt.red, 2, Qt.SolidLine, Qt.RoundCap))
			self.graphics_scene.addItem(self.graphics_items[area_id])
			self.graphics_items[area_id].setRect(map_x, map_y, radius,
			                                     radius)  # finally set position here for correct scene coordinate
		# debug
		else:
			# self.graphics_items[area_id].setRect(map_x, map_y, radius, radius)
			self.graphics_items[area_id].setPos(0, 0)
			self.graphics_items[area_id].setRect(map_x, map_y, radius, radius)

	def get_game_coordinates(self, scene_x, scene_y) -> Point:
		"""
		Transform scene coordinates into game coordinates.
		:param scene_x:
		:param scene_y:
		:return: Game coordinates as point
		"""
		bot_position = self.graphics_items.get(Bot.char_data.unique_id, None)
		if bot_position is None:
			bot_position = QPointF(200, 200)
		else:
			bot_position = bot_position.scenePos()

		# find tile offset from middle
		map_x_tile_offset = (scene_x // 134) - 1
		map_y_tile_offset = - ((scene_y // 134) - 1)
		x_sector = self.current_position.x_sector + map_x_tile_offset
		y_sector = self.current_position.y_sector + map_y_tile_offset

		map_x = ((scene_x % 134) * 100) / self.map_scaling_factor
		map_y = (266 - (map_y_tile_offset * 133) - scene_y) * 100 / self.map_scaling_factor

		game_x = Point.get_x_absolute(x_sector, map_x)
		game_y = Point.get_y_absolute(y_sector, map_y) + 5  # make up 5px cursor
		point = Point.from_absolute_coords(game_x, game_y)
		return point

	def handle_map_click(self, event: QGraphicsSceneMouseEvent):
		# if Bot.char_data.position.x_sector == 0:
		# 	return

		scene_x = event.scenePos().x()
		scene_y = event.scenePos().y()
		# print(f"{scene_x},{scene_y}")
		# validate for correct mouseclick
		if any(0 > pos > 402 for pos in [scene_x, scene_y]):
			return

		point = self.get_game_coordinates(scene_x, scene_y)
		distance = point.distance_point(self.current_position)
		if distance <= ScriptUtils.MAX_GO_DISTANCE:
			proxy.joymax.send_packet(Movement(point, self.current_position).create_request())
			print(f"move to {point.x},{point.y}")

	def handle_spawn(self, unique_id, spawn, spawn_list):
		if unique_id not in self.graphics_items:
			self.get_scene_coordinates_and_set(spawn.unique_id, spawn.position, spawn_list)

	def handle_despawn(self, e: DespawnArgs):
		if e.list_type == SpawnList.Players:
			self.handle_list_changed(SpawnListsGlobal.Players, not self._view.playersPushButton.isEnabled())
		elif e.list_type == SpawnList.MonsterSpawns:
			self.handle_list_changed(SpawnListsGlobal.MonsterSpawns, not self._view.monstersPushButton.isEnabled())
		elif e.list_type == SpawnList.Drops:
			self.handle_list_changed(SpawnListsGlobal.Drops, not self._view.dropsPushButton.isEnabled())

		if e.unique_id in self.graphics_items:
			self.graphics_scene.removeItem(self.graphics_items[e.unique_id])
			self.graphics_items.pop(e.unique_id, None)

	def handle_moved(self, e: MovementArgs):
		# update position
		if e.unique_id == Bot.char_data.unique_id:
			# load new map, if sector changed
			if self.current_position.x_sector != e.origin.x_sector or self.current_position.y_sector != e.origin.y_sector:
				self._load_map_parts(e.origin.x_sector, e.origin.y_sector)
			self.current_position.set_from_point(e.origin)
			self.get_scene_coordinates_and_set(e.unique_id, e.origin, SpawnList.none)
		else:
			spawn_list = SpawnListsGlobal.AllSpawnEntries.get(e.unique_id, None)
			if spawn_list:
				self.get_scene_coordinates_and_set(e.unique_id, e.origin, spawn_list)

	def handle_stop_movement(self, *args):
		if args[0].unique_id == Bot.char_data.unique_id:
			self.handle_moved(MovementArgs(Bot.char_data.unique_id, Bot.char_data.position, Bot.char_data.position))

	def change_area(self, area_number, index):
		if area_number == 1:
			area_string = ','.join([self._view.area1XLineEdit.text(), self._view.area1YLineEdit.text(),
			                        self._view.area1RadiusLineEdit.text()])
			if area_string.split(',', 2)[index] != str(TrainingAreaSettings.instance().area1).split(',', 2)[index]:
				TrainingAreaSettings.instance().area1 = area_string
				self.get_area_scene_coordinates(1, TrainingAreaSettings.instance().area1)
		else:
			area_string = ','.join([self._view.area2XLineEdit.text(), self._view.area2YLineEdit.text(),
			                        self._view.area2RadiusLineEdit.text()])
			if area_string.split(',', 2)[index] != str(TrainingAreaSettings.instance().area2).split(',', 2)[index]:
				TrainingAreaSettings.instance().area2 = area_string
				self.get_area_scene_coordinates(2, TrainingAreaSettings.instance().area2)

	def handle_teleported(self):
		self.graphics_scene.clear()
		self.graphics_items.clear()

	def on_use_area_2_clicked(self):
		TrainingAreaSettings.use_second_area = self._view.useArea2CheckBox.isChecked()

	def on_set_area_1(self):
		if Bot.char_data.position.x_sector != 0 and Bot.char_data.position.y_sector != 0:
			self._view.area1XLineEdit.setText(str(Bot.char_data.position.x))
			self._view.area1YLineEdit.setText(str(Bot.char_data.position.y))

	def on_set_area_2(self):
		if Bot.char_data.position.x_sector != 0 and Bot.char_data.position.y_sector != 0:
			self._view.area2XLineEdit.setText(str(Bot.char_data.position.x))
			self._view.area2YLineEdit.setText(str(Bot.char_data.position.y))

	def on_switch_areas(self):
		x1, y1, r1 = self._view.area1XLineEdit.text(), self._view.area1YLineEdit.text(), self._view.area1RadiusLineEdit.text()
		x2, y2, r2 = self._view.area2XLineEdit.text(), self._view.area2YLineEdit.text(), self._view.area2RadiusLineEdit.text()
		self._view.area1XLineEdit.setText(x2)
		self._view.area1YLineEdit.setText(y2)
		self._view.area1RadiusLineEdit.setText(r2)
		self._view.area2XLineEdit.setText(x1)
		self._view.area2YLineEdit.setText(y1)
		self._view.area2RadiusLineEdit.setText(r1)

	def _load_settings(self):
		self._view.area1XLineEdit.setText(str(TrainingAreaSettings.instance().area1.x))
		self._view.area1YLineEdit.setText(str(TrainingAreaSettings.instance().area1.y))
		self._view.area1RadiusLineEdit.setText(str(TrainingAreaSettings.instance().area1.radius))
		self._view.area2XLineEdit.setText(str(TrainingAreaSettings.instance().area2.x))
		self._view.area2YLineEdit.setText(str(TrainingAreaSettings.instance().area2.y))
		self._view.area2RadiusLineEdit.setText(str(TrainingAreaSettings.instance().area2.radius))

		self._view.useArea2CheckBox.setChecked(self._view.useArea2CheckBox.isChecked())

	def _connect_signals(self):
		self.set_table_view()
		self._view.playersPushButton.clicked.connect(
			lambda x: self.update_table_with_collection(SpawnListsGlobal.Players, self._view.playersPushButton))
		self._view.monstersPushButton.clicked.connect(
			lambda x: self.update_table_with_collection(SpawnListsGlobal.MonsterSpawns, self._view.monstersPushButton))
		self._view.dropsPushButton.clicked.connect(
			lambda x: self.update_table_with_collection(SpawnListsGlobal.Drops, self._view.dropsPushButton))

		ParseChar.on_parsed += lambda x: self.handle_list_changed(SpawnListsGlobal.Players,
		                                                          not self._view.playersPushButton.isEnabled())
		ParseMonster.on_parsed += lambda x: self.handle_list_changed(SpawnListsGlobal.MonsterSpawns,
		                                                             not self._view.monstersPushButton.isEnabled())
		ParseItem.on_parsed += lambda x: self.handle_list_changed(SpawnListsGlobal.Drops,
		                                                          not self._view.dropsPushButton.isEnabled())

		# ParseChar.on_parsed += lambda x: self.handle_list_changed(SpawnListsGlobal.Players,
		#                                                          not self._view.playersPushButton.isEnabled())
		# ParseMonster.on_parsed += lambda x: self.handle_list_changed(SpawnListsGlobal.MonsterSpawns,
		#                                                             not self._view.monstersPushButton.isEnabled())
		# ParseItem.on_parsed += lambda x: self.handle_list_changed(SpawnListsGlobal.Drops,
		#                                                          not self._view.monstersPushButton.isEnabled())
		self._view.area1XLineEdit.textChanged.connect(lambda x: self.change_area(1, 0))
		self._view.area1YLineEdit.textChanged.connect(lambda x: self.change_area(1, 1))
		self._view.area1RadiusLineEdit.textChanged.connect(lambda x: self.change_area(1, 2))
		self._view.area2XLineEdit.textChanged.connect(lambda x: self.change_area(2, 0))
		self._view.area2YLineEdit.textChanged.connect(lambda x: self.change_area(2, 1))
		self._view.area2RadiusLineEdit.textChanged.connect(lambda x: self.change_area(2, 2))
		self._view.area1XLineEdit.setValidator(QIntValidator(-2147483648, 2147483647, self._view))
		self._view.area1YLineEdit.setValidator(QIntValidator(-2147483648, 2147483647, self._view))
		self._view.area1RadiusLineEdit.setValidator(QIntValidator(0, 1000, self._view))
		self._view.area2XLineEdit.setValidator(QIntValidator(-2147483648, 2147483647, self._view))
		self._view.area2YLineEdit.setValidator(QIntValidator(-2147483648, 2147483647, self._view))
		self._view.area2RadiusLineEdit.setValidator(QIntValidator(0, 1000, self._view))

		self._view.useArea2CheckBox.clicked.connect(self.on_use_area_2_clicked)
		self._view.switchAreasPushButton.clicked.connect(self.on_switch_areas)
		self._view.setArea1PushButton.clicked.connect(self.on_set_area_1)
		self._view.setArea2PushButton.clicked.connect(self.on_set_area_2)

		SpawnHandler.on_spawned += self.handle_spawn
		SpawnHandler.on_despawned += self.handle_despawn

		NPCTeleport.on_teleport_used += self.handle_teleported

		# allow map handling if map folder exists
		if os.path.exists(DirectoryGlobal.map_folder()) and os.listdir(DirectoryGlobal.map_folder()):
			Bot.char_data.on_moved += self.handle_moved
			Movement.on_spawnmovement_registered += self.handle_moved
			CharStopMovementHandler.on_char_stop_movement += self.handle_stop_movement
