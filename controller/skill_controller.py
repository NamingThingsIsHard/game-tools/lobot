import logging
import sys

from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QListWidget

from controller.CustomWidgets import QListWidgetSkillItem
from controller.controller import Controller
from lobot_api.Bot import Bot
from lobot_api.globals.PK2DataGlobal import PK2DataGlobal
from lobot_api.globals.settings.SkillSettings import SkillSettings
from lobot_api.logger.Logger import PK2
from lobot_api.packets.char.CharDataHandler import CharDataHandler
from lobot_api.packets.char.CharSkillUpdateLearn import CharSkillUpdateLearn
from lobot_api.pk2.PK2Skill import PK2SkillType
from lobot_api.pk2.extractors.PK2ExtractorSkills import PK2ExtractorSkills
from lobot_api.structs.CharData import Skill


class SkillController(Controller):
	@staticmethod
	def remove_duplicate_skills():
		# Remove lesser duplicate skills
		raw_list = [Skill(int(s)) for s in SkillSettings.skill_list.value.split(',')] if SkillSettings.skill_list.value else []
		filtered_list = []

		for s in raw_list:
			if s in filtered_list:
				existing_skill = next((e for e in filtered_list if e.name == s.name), None)
				if existing_skill and existing_skill.ref_id < s.ref_id:
					filtered_list[raw_list.index(existing_skill)] = s
			else:
				filtered_list.append(s)

		# Update settings list
		filtered_list.sort(key=lambda s: s.name)
		string_list = ",".join([str(s.ref_id) for s in filtered_list])
		SkillSettings.skill_list.value = string_list

		return filtered_list

	def _load_settings(self):
		"""
		Add skills to lists then from lists to views.
		:return:
		"""
		if not PK2DataGlobal.skills:
			self._prepare_pk2_then_load()

	def _prepare_pk2_then_load(self):
		"""
		Await pk2 changes before trying to load skills.
		:return:
		"""
		if not PK2DataGlobal.skills:
			PK2ExtractorSkills().extract_all()

		if not PK2DataGlobal.skills:
			sys.stdout.write(f"Unable to extract skills. Delete skills.txt before trying again\n")
			logging.log(PK2, f"Unable to extract skills. Delete skills.txt before trying again")
			return
		Bot.char_data.skills = self.remove_duplicate_skills()

		SkillSettings.imbue.value = next((i.ref_id for i in Bot.char_data.skills if i.ref_id == SkillSettings.imbue.value)
		                           , 0)

		initiations = [int(s) for s in SkillSettings.initiations_list.value.split(',')] if SkillSettings.initiations_list.value else []
		general = [int(s) for s in SkillSettings.general_list.value.split(',')] if SkillSettings.general_list.value else []
		finishers = [int(s) for s in SkillSettings.finishers_list.value.split(',')] if SkillSettings.finishers_list.value else []
		buffs_weak = [int(s) for s in SkillSettings.buff_skills_weak.value.split(',')] if SkillSettings.buff_skills_weak.value else []
		buffs_strong = [int(s) for s in SkillSettings.buff_skills_strong.value.split(',')] if SkillSettings.buff_skills_strong.value else []
		# SkillSettings.MonsterTypes = []
		SkillSettings.InitiationSkills = [next(s for s in Bot.char_data.skills if s.ref_id == ref_id and self.filter_attack(s)) for ref_id in initiations] if Bot.char_data.skills else []
		SkillSettings.GeneralSkills = [next(s for s in Bot.char_data.skills if s.ref_id == ref_id and self.filter_attack(s)) for ref_id in general] if Bot.char_data.skills else []
		SkillSettings.FinisherSkills = [next(s for s in Bot.char_data.skills if s.ref_id == ref_id and self.filter_attack(s)) for ref_id in finishers] if Bot.char_data.skills else []
		SkillSettings.BuffSkillsWeak = [next(s for s in Bot.char_data.skills if s.ref_id == ref_id and self.filter_buff(s)) for ref_id in buffs_weak] if Bot.char_data.skills else []
		SkillSettings.BuffSkillsStrong = [next(s for s in Bot.char_data.skills if s.ref_id == ref_id and self.filter_buff(s)) for ref_id in buffs_strong] if Bot.char_data.skills else []

		# SkillSettings.MembersToBuff = [MembersToBuff]
		# SkillSettings.MemberBuffs = [MembersBuffs]
		collections = [(SkillSettings.InitiationSkills, self._view.initiationsListWidget),
		               (SkillSettings.GeneralSkills, self._view.generalListWidget),
		               (SkillSettings.FinisherSkills, self._view.finishersListWidget),
		               (SkillSettings.BuffSkillsWeak, self._view.buffWeakListWidget),
		               (SkillSettings.BuffSkillsStrong, self._view.buffStrongListWidget)]
		for collection, view in collections:
			view.clear()
			for c in collection:
				view.addItem(QListWidgetSkillItem(c, c.name))
			self.update_settings(view)
		self.update_skills_in_view(0)
		self.update_imbue()
		self.save_settings()

	@staticmethod
	def filter_active(skill):
		return skill.type != PK2SkillType.Passive and skill not in Bot.auto_attack_skills.values()

	@staticmethod
	def filter_attack(skill):
		return skill.type.is_attack() and skill not in Bot.auto_attack_skills.values()

	@staticmethod
	def filter_knockdown(skill):
		return skill.type.is_knock_down_skill()

	@staticmethod
	def filter_debuff(skill):
		return skill.type is PK2SkillType.Debuff

	@staticmethod
	def filter_warlockDOT(skill):
		return skill.type is PK2SkillType.WarlockDOT

	@staticmethod
	def filter_buff(skill):
		return skill.type.is_buff()

	@staticmethod
	def filter_heals(skill):
		return skill.type.is_heal()

	@staticmethod
	def filter_mana(skill):
		return skill.type != PK2SkillType.Passive and skill.type == PK2SkillType.ManaBuff

	@staticmethod
	def filter_resurrect(skill):
		return skill.type is PK2SkillType.Resurrection

	skill_filter = {
		0: lambda: [s for s in Bot.char_data.skills if SkillController.filter_active(s)],  # Active skills
		1: lambda: [s for s in Bot.char_data.skills if SkillController.filter_attack(s)],  # Attacks
		2: lambda: [s for s in Bot.char_data.skills if SkillController.filter_knockdown(s)],  # Knockdown
		3: lambda: [s for s in Bot.char_data.skills if SkillController.filter_debuff(s)],  # Debuffs
		4: lambda: [s for s in Bot.char_data.skills if SkillController.filter_warlockDOT(s)],  # WarlockDOT
		5: lambda: [s for s in Bot.char_data.skills if SkillController.filter_buff(s)],  # Buffs
		6: lambda: [s for s in Bot.char_data.skills if SkillController.filter_heals(s)],  # Heals
		7: lambda: [s for s in Bot.char_data.skills if SkillController.filter_mana(s)],  # Mana
		8: lambda: [s for s in Bot.char_data.skills if SkillController.filter_resurrect(s)]  # Resurrect
	}

	current_skills = []

	def handle_add_or_update_skills(self, *args):
		"""Sync skills learned/updated with the skills in the bot list."""
		#save to skill settings for next bot startup
		collections = [(SkillSettings.InitiationSkills, self._view.initiationsListWidget),
		               (SkillSettings.GeneralSkills, self._view.generalListWidget),
		               (SkillSettings.FinisherSkills, self._view.finishersListWidget),
		               (SkillSettings.BuffSkillsWeak, self._view.buffWeakListWidget),
		               (SkillSettings.BuffSkillsStrong, self._view.buffStrongListWidget)]
		if args:
			for arg in args:
				# update leveled up/down skill
				s = next((s for s in SkillSettings.Skills if arg.name == s.name), None)
				if s:
					s.ref_id = arg.ref_id
				else:
					SkillSettings.Skills.append(arg)
			string_list = ",".join([str(s.ref_id) for s in Bot.char_data.skills])
			SkillSettings.skill_list.value = string_list
		else:
			existing_skill_ids = [s.ref_id for s in SkillSettings.Skills]
			SkillSettings.Skills = [s for s in Bot.char_data.skills if s.ref_id not in existing_skill_ids]

		collections = [(SkillSettings.InitiationSkills, self._view.initiationsListWidget),
		               (SkillSettings.GeneralSkills, self._view.generalListWidget),
		               (SkillSettings.FinisherSkills, self._view.finishersListWidget),
		               (SkillSettings.BuffSkillsWeak, self._view.buffWeakListWidget),
		               (SkillSettings.BuffSkillsStrong, self._view.buffStrongListWidget)]
		for collection, view in collections:
			view.clear()
			for skill in collection:
				view.addItem(QListWidgetSkillItem(skill, skill.name))
			self.update_settings(view)

		self.update_skills_in_view(0)
		self.save_settings()

	def update_skills_in_view(self, i: int):
		self._view.skillListWidget.clear()
		self.current_skills = self.skill_filter[i]()
		if self.current_skills:
			self.current_skills.sort(key=lambda s: s.name)
			for s in self.current_skills:
				q = QListWidgetSkillItem(s, s.name)
				self._view.skillListWidget.addItem(q)

	def update_imbue(self, *args):
		"""
		Update imbue combobox with imbue names and ref_id.
		Searches for last set imbue in skill list or sets the first one found.
		:param args: Signal args
		:return: None
		"""
		self._view.imbueComboBox.clear()
		imbues = [i for i in Bot.char_data.skills if i.type is PK2SkillType.Imbue]
		imbue_ref_ids = [i.ref_id for i in imbues]
		imbues.sort(key=lambda i: i.name)
		imbue_index = 0
		if SkillSettings.imbue.value != 0 and SkillSettings.imbue.value in imbue_ref_ids:
			for index in range(0, len(imbues)):
				if imbues[index].ref_id == SkillSettings.imbue.value:
					imbue_index = index
					break
		else:  # get next imbue
			# TODO extract pk2skill level to be able to automatically choose highest level imbue
			skill = next(iter(imbues), None)
			SkillSettings.imbue.value = skill.ref_id if skill else 0

		for i in imbues:
			self._view.imbueComboBox.addItem(i.name, i.ref_id)
		self._view.imbueComboBox.setCurrentIndex(imbue_index)

		self.save_settings()

	def update_settings(self, collection):
		if collection is self._view.initiationsListWidget:
			SkillSettings.InitiationSkills.clear()
			ref_ids = [self._view.initiationsListWidget.item(i).skill.ref_id for i in range(self._view.initiationsListWidget.count())]
			for ref in ref_ids:
				for s in Bot.char_data.skills:
					if s.ref_id == ref:
						SkillSettings.InitiationSkills.append(s)
			SkillSettings.initiations_list.value = ",".join([str(s.ref_id) for s in SkillSettings.InitiationSkills])
		elif collection is self._view.generalListWidget:
			SkillSettings.GeneralSkills.clear()
			ref_ids = [self._view.generalListWidget.item(i).skill.ref_id for i in range(self._view.generalListWidget.count())]
			for ref in ref_ids:
				for s in Bot.char_data.skills:
					if s.ref_id == ref:
						SkillSettings.GeneralSkills.append(s)
			SkillSettings.general_list.value = ",".join([str(s.ref_id) for s in SkillSettings.GeneralSkills])
		elif collection is self._view.finishersListWidget:
			SkillSettings.FinisherSkills.clear()
			ref_ids = [self._view.finishersListWidget.item(i).skill.ref_id for i in range(self._view.finishersListWidget.count())]
			for ref in ref_ids:
				for s in Bot.char_data.skills:
					if s.ref_id == ref:
						SkillSettings.FinisherSkills.append(s)
			SkillSettings.finishers_list.value = ",".join([str(s.ref_id) for s in SkillSettings.FinisherSkills])
		elif collection is self._view.buffWeakListWidget:
			SkillSettings.BuffSkillsWeak.clear()
			ref_ids = [self._view.buffWeakListWidget.item(i).skill.ref_id for i in range(self._view.buffWeakListWidget.count())]
			for ref in ref_ids:
				for s in Bot.char_data.skills:
					if s.ref_id == ref:
						SkillSettings.BuffSkillsWeak.append(s)
			SkillSettings.buff_skills_weak.value = ",".join([str(s.ref_id) for s in SkillSettings.BuffSkillsWeak])
		elif collection is self._view.buffStrongListWidget:
			SkillSettings.BuffSkillsStrong.clear()
			ref_ids = [self._view.buffStrongListWidget.item(i).skill.ref_id for i in range(self._view.buffStrongListWidget.count())]
			for ref in ref_ids:
				for s in Bot.char_data.skills:
					if s.ref_id == ref:
						SkillSettings.BuffSkillsStrong.append(s)
			SkillSettings.buff_skills_strong.value = ",".join([str(s.ref_id) for s in SkillSettings.BuffSkillsStrong])

	def on_add_skill_to_list(self, widget: QListWidget, filter_function):
		"""
		Add filtered not already in list to the list.
		:param widget: target widget for adding skills
		:param filter_function: Function to filter out valid skills
		:return: None
		"""
		selected_indexes = [s.row() for s in self._view.skillListWidget.selectedIndexes()]
		for index in selected_indexes:
			if filter_function(self.current_skills[index]):
				skill_widget = self._view.skillListWidget.item(index)
				if widget.findItems(skill_widget.text(), Qt.MatchExactly):
					continue
				target_indexes = widget.selectedIndexes()
				insert_index = widget.count() if len(target_indexes) < 1 else widget.selectedIndexes()[-1].row() + 1
				widget.insertItem(insert_index, QListWidgetSkillItem(skill_widget.skill, skill_widget.text()))
		self.update_settings(widget)

	def on_delete_skill_from_list(self, widget: QListWidget):
		selected = [s.row() for s in widget.selectedIndexes()]
		selected.reverse()  # Start deleting from the end
		for index in selected:
			widget.takeItem(index)
		self.update_settings(widget)

	def on_move_up(self, widget: QListWidget):
		indices = [s.row() for s in widget.selectedIndexes()]
		for index in indices:
			if index <= 0 or (index - 1) in indices:
				continue
			skill = widget.takeItem(index - 1)
			widget.insertItem(index, skill)
		self.update_settings(widget)

	def on_move_down(self, widget: QListWidget):
		indexes = [s.row() for s in widget.selectedIndexes()]
		indexes.reverse()
		for index in indexes:
			count = widget.count()
			if not (index + 1 < count) or (index + 1) in indexes:
				continue
			skill = widget.takeItem(index + 1)
			widget.insertItem(index, skill)
		self.update_settings(widget)

	def on_imbue_selection_changed(self, index):
		ref_id = self._view.imbueComboBox.itemData(index)
		if ref_id in [i.ref_id for i in Bot.char_data.skills]:
			SkillSettings.imbue.value = ref_id

	def _connect_signals(self):
		self._view.skillFilterComboBox.currentIndexChanged.connect(self.update_skills_in_view)
		CharDataHandler.on_packet += lambda: self.handle_add_or_update_skills(*Bot.char_data.skills)
		CharDataHandler.on_packet += self.update_imbue
		CharSkillUpdateLearn.on_packet += self.handle_add_or_update_skills
		CharSkillUpdateLearn.on_packet += self.update_imbue
		CharDataHandler.on_packet += lambda: self.update_skills_in_view(self._view.skillFilterComboBox.currentIndex())
		CharSkillUpdateLearn.on_packet += lambda _: self.update_skills_in_view(self._view.skillFilterComboBox.currentIndex())

		self._view.imbueComboBox.currentIndexChanged.connect(self.on_imbue_selection_changed)

		self._view.initiationAddToolButton.clicked.connect(
			lambda s: self.on_add_skill_to_list(self._view.initiationsListWidget, self.filter_attack))
		self._view.initiationDeleteToolButton.clicked.connect(
			lambda s: self.on_delete_skill_from_list(self._view.initiationsListWidget))
		self._view.initiationUpToolButton.clicked.connect(
			lambda s: self.on_move_up(self._view.initiationsListWidget))
		self._view.initiationDownToolButton.clicked.connect(
			lambda s: self.on_move_down(self._view.initiationsListWidget))

		self._view.generalAddToolButton.clicked.connect(
			lambda s: self.on_add_skill_to_list(self._view.generalListWidget, self.filter_attack))
		self._view.generalDeleteToolButton.clicked.connect(
			lambda s: self.on_delete_skill_from_list(self._view.generalListWidget))
		self._view.generalUpToolButton.clicked.connect(
			lambda s: self.on_move_up(self._view.generalListWidget))
		self._view.generalDownToolButton.clicked.connect(
			lambda s: self.on_move_down(self._view.generalListWidget))

		self._view.finisherAddToolButton.clicked.connect(
			lambda s: self.on_add_skill_to_list(self._view.finishersListWidget, self.filter_attack))
		self._view.finisherDeleteToolButton.clicked.connect(
			lambda s: self.on_delete_skill_from_list(self._view.finishersListWidget))
		self._view.finishersUpToolButton.clicked.connect(
			lambda s: self.on_move_up(self._view.finishersListWidget))
		self._view.finishersDownToolButton.clicked.connect(
			lambda s: self.on_move_down(self._view.finishersListWidget))

		self._view.buffWeakAddToolButton.clicked.connect(
			lambda s: self.on_add_skill_to_list(self._view.buffWeakListWidget, self.filter_buff))
		self._view.buffWeakDeleteToolButton.clicked.connect(
			lambda s: self.on_delete_skill_from_list(self._view.buffWeakListWidget))
		self._view.buffWeakUpToolButton.clicked.connect(
			lambda s: self.on_move_up(self._view.buffWeakListWidget))
		self._view.buffWeakDownToolButton.clicked.connect(
			lambda s: self.on_move_down(self._view.buffWeakListWidget))

		self._view.buffStrongAddToolButton.clicked.connect(
			lambda s: self.on_add_skill_to_list(self._view.buffStrongListWidget, self.filter_buff))
		self._view.buffStrongDeleteToolButton.clicked.connect(
			lambda s: self.on_delete_skill_from_list(self._view.buffStrongListWidget))
		self._view.buffStrongUpToolButton.clicked.connect(
			lambda s: self.on_move_up(self._view.buffStrongListWidget))
		self._view.buffStrongDownToolButton.clicked.connect(
			lambda s: self.on_move_down(self._view.buffStrongListWidget))
