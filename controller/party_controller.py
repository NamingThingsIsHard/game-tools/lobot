import re

from PyQt5.QtCore import pyqtSlot

from controller.controller import Controller
from lobot_api.Bot import Bot
from lobot_api.connection import proxy
from lobot_api.globals.settings.PartySettings import PartySettings
from lobot_api.packets.party.PartyBanMember import PartyBanMember
from lobot_api.packets.party.PartyCreate import PartyCreate
from lobot_api.packets.party.PartyLeave import PartyLeave
from lobot_api.packets.party.PartyMatchJoinHandler import PartyMatchJoinHandler
from lobot_api.packets.party.PartyMatchRefresh import PartyMatchRefresh
from lobot_api.packets.party.PartyUpdateHandle import PartyUpdateHandler
from lobot_api.structs.PartyEntry import PartyEntry


class PartyController(Controller):
	def __init__(self, view):
		super().__init__(view)

	@pyqtSlot(bool)
	def on_party_autoremake_click(self, b):
		if PartySettings.instance().auto_reform != b:
			PartySettings.instance().auto_reform = b
		if PartySettings.instance().auto_join == b:
			PartySettings.instance().auto_join = not b

	@pyqtSlot(bool)
	def on_party_autojoin_click(self, b):
		if PartySettings.instance().auto_join != b:
			PartySettings.instance().auto_join = b
		if PartySettings.instance().auto_reform == b:
			PartySettings.instance().auto_reform = not b

	@pyqtSlot(bool)
	def on_auto_invite_clicked(self, b):
		PartySettings.instance().auto_invite = b
	@pyqtSlot(bool)
	def on_party_autoaccept_clicked(self, b):
		PartySettings.instance().auto_accept_invite = b
	@pyqtSlot(bool)
	def on_party_onlylist_clicked(self, b):
		PartySettings.instance().only_members = b
	@pyqtSlot(bool)
	def on_allowinvite_clicked(self, b):
		PartySettings.instance().members_can_invite = b

	def on_party_form_clicked(self):
		if proxy.joymax and not Bot.party_current:
			party = PartyEntry()
			proxy.joymax.send_packet(PartyCreate(party).create_request())

	def on_party_leave_clicked(self):
		if proxy.joymax and Bot.party_current:
			proxy.joymax.send_packet(PartyLeave().create_request())

	def on_party_join_clicked(self):
		party: str = self._view.partyListWidget.current_item().text()
		party_number = int(party.split('-', 1)[0])
		if party and proxy.joymax:
			proxy.joymax.send_packet(PartyMatchJoinHandler(party_number).create_request())
	def on_party_match_refresh_clicked(self):
		if proxy.joymax:
			proxy.joymax.send_packet(PartyMatchRefresh().create_request())

	@pyqtSlot(str)
	def on_party_title_changed(self, s):
		if s:
			PartySettings.instance().title = s

	@pyqtSlot(int)
	def on_party_objective_changed(self, i):
		if i > -1:
			PartySettings.instance().objective = i

	@pyqtSlot(int)
	def on_party_type_changed(self, i):
		if i > -1:
			isChecked = 4 if self._view.partyAllowInviteCheckBox.checkState else 0
			type_bits = i | isChecked
			PartySettings.instance().type = type_bits

	def on_add_to_list_clicked(self):
		item = self._view.partyMemberCurrentListWidget.current_item()
		if item:
			entry: str = item.text()
			name = entry.split('(', 1)[0]
			items = []
			for index in range(self._view.partyMemberAllListWidget.count()):
				items.append(self._view.partyMemberAllListWidget.item(index))
			if name and re.match(r'\w+', name) and name not in items:
				self._view.partyMemberAllListWidget.addItem(name)
				PartySettings.instance().MemberList.append(name)

	def on_kick_clicked(self):
		item = self._view.partyMemberCurrentListWidget.current_item()
		if item:
			entry: str = item.text()
			name = int(entry.split('(', 1)[0])
			if name:
				member = next((m for m in Bot.party_current.members if m.name == name), None)
				if name and proxy.joymax:
					proxy.joymax.send_packet(PartyBanMember(member.member_id).create_request())

	def on_add_name_to_list_clicked(self):
		name = self._view.partyMemberNameLineEdit.text()
		items = []
		for index in range(self._view.partyMemberAllListWidget.count()):
			items.append(self._view.partyMemberAllListWidget.item(index))
		if name and re.match(r'\w+', name) and name not in items:
			self._view.partyMemberAllListWidget.addItem(name)
			PartySettings.instance().MemberList.append(name)

	def on_remove_from_list_clicked(self):
		item = self._view.partyMemberAllListWidget.current_item()
		if item:
			self._view.partyMemberAllListWidget.takeItem(self._view.partyMemberAllListWidget.row(item))

	def handle_party_matching_list(self, items):
		"""Refresh party matching list after receiving update packet."""
		self._view.partyListWidget.clear()
		self._view.partyListWidget.addItems(items)

	def handle_party_update(self, items):
		"""Refresh party matching list after receiving update packet."""
		self._view.partyMemberCurrentListWidget.clear()
		self._view.partyMemberCurrentListWidget.addItems(items)

		self._view.partyFormPushButton.setEnabled(False)
		self._view.partyLeavePushButton.setEnabled(True)
		self._view.partyJoinPushButton.setEnabled(False)
		if Bot.party_current.leader != Bot.char_data.name:
			self._view.partyKickPushButton.setEnabled(False)

	def _load_settings(self):
		PartySettings.instance().sig_title.emit(PartySettings.instance().title)
		PartySettings.instance().sig_objective[int].emit(PartySettings.instance().objective.value)
		PartySettings.instance().sig_type[int].emit(PartySettings.instance().type.value & 3)
		PartySettings.instance().sig_auto_reform[bool].emit(PartySettings.instance().auto_reform)
		PartySettings.instance().sig_auto_join[bool].emit(PartySettings.instance().auto_join)

		PartySettings.instance().sig_auto_invite[bool].emit(PartySettings.instance().auto_invite)
		PartySettings.instance().sig_auto_accept_invite[bool].emit(PartySettings.instance().auto_accept_invite)
		PartySettings.instance().sig_only_members[bool].emit(PartySettings.instance().only_members)
		PartySettings.instance().sig_members_can_invite[bool].emit(PartySettings.instance().members_can_invite)

		if PartySettings.instance().auto_reform:
			self._view.partyAutoRemakeRadioButton.setChecked(True)
		else:
			self._view.partyAutoJoinRadioButton.setChecked(True)

		# self._view.partyEXPItemComboBox.setCurrentIndex(PartySettings.instance().type.value - )

	def _connect_signals(self):
		PartySettings.instance().sig_title.connect(self._view.partyTitleLineEdit.setText)
		self._view.partyTitleLineEdit.textChanged.connect(self.on_party_title_changed)

		PartySettings.instance().sig_objective[int].connect(self._view.partyTypeComboBox.setCurrentIndex)
		self._view.partyTypeComboBox.currentIndexChanged.connect(self.on_party_objective_changed)

		PartySettings.instance().sig_type[int].connect(self._view.partyEXPItemComboBox.setCurrentIndex)
		self._view.partyEXPItemComboBox.currentIndexChanged.connect(self.on_party_type_changed)

		PartySettings.instance().sig_auto_reform.connect(self._view.partyAutoRemakeRadioButton.clicked)
		self._view.partyAutoRemakeRadioButton.clicked.connect(self.on_party_autoremake_click)
		PartySettings.instance().sig_auto_join.connect(self._view.partyAutoJoinRadioButton.clicked)
		self._view.partyAutoJoinRadioButton.clicked.connect(self.on_party_autojoin_click)

		PartySettings.instance().sig_auto_invite[bool].connect(self._view.partyInviteCheckBox.setChecked)
		self._view.partyInviteCheckBox.clicked.connect(self.on_auto_invite_clicked)
		PartySettings.instance().sig_auto_accept_invite[bool].connect(self._view.partyAcceptCheckBox.setChecked)
		self._view.partyAcceptCheckBox.clicked.connect(self.on_party_autoaccept_clicked)
		PartySettings.instance().sig_only_members[bool].connect(self._view.partyOnlyListCheckBox.setChecked)
		self._view.partyOnlyListCheckBox.clicked.connect(self.on_party_onlylist_clicked)
		PartySettings.instance().sig_members_can_invite[bool].connect(self._view.partyAllowInviteCheckBox.setChecked)
		self._view.partyAllowInviteCheckBox.clicked.connect(self.on_allowinvite_clicked)

		self._view.partyFormPushButton.clicked.connect(self.on_party_form_clicked)
		self._view.partyLeavePushButton.clicked.connect(self.on_party_leave_clicked)
		self._view.partyJoinPushButton.clicked.connect(self.on_party_join_clicked)
		self._view.partyUpdatePushButton.clicked.connect(self.on_party_match_refresh_clicked)

		self._view.partyAddToListPushButton.clicked.connect(self.on_add_to_list_clicked)
		self._view.partyKickPushButton.clicked.connect(self.on_kick_clicked)
		self._view.partyMemberAddToolButton.clicked.connect(self.on_add_name_to_list_clicked)
		self._view.partyMemberDeleteToolButton.clicked.connect(self.on_remove_from_list_clicked)

		PartyMatchRefresh.on_packet += self.handle_party_matching_list
		PartyUpdateHandler.on_packet += self.handle_party_update
