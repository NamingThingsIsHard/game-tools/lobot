# This Python file uses the following encoding: utf-8
# from PyQt5 import QtCore
# from PyQt5 import QtWidgets
import os

from PyQt5.QtCore import pyqtSignal, QRect
from PyQt5.QtWidgets import QDesktopWidget, QProgressBar

from controller.controller import Controller
from controller.item_containers_controller import ItemContainersController
from controller.item_controller import ItemController
from controller.login_controller import LoginController
from controller.map_controller import MapController
from controller.party_controller import PartyController
from controller.potion_controller import PotionController
from controller.script_controller import ScriptController
from controller.skill_controller import SkillController
from controller.status_controller import StatusController
from controller.town_controller import TownController
from lobot_api.packets.char.CharStopMovementHandler import CharStopMovementHandler
from lobot_api.packets.char.Movement import Movement
from lobot_api.packets.spawn.SpawnHandler import ParsePet
from lobot_api.pk2.PK2Utils import PK2Utils
from lobot_api.statemachine import PickupUsePet


class MainWindowController(Controller):
	def _load_settings(self):
		CharStopMovementHandler.on_char_stop_movement += Movement.handle_char_stop_movement
		ParsePet.on_parsed += PickupUsePet.handle_pet_spawn

	settings_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), os.pardir) + "/settings"
	sig_progress = pyqtSignal(int)

	def statusbar_show_message(self, message: str, timeout: int = 0):
		"""
		Use to update the status bar in the main window.
		:param message: Message to update with
		:param timeout: How long message is
		:return: None
		"""
		self.sig_status_bar.emit(message)

	def center(self, view):
		"""
		Function to set window to center of screen.

		:param view: A QWidget to be centered
		:return: None
		"""
		q_rect = view.frameGeometry()
		center_point = QDesktopWidget().availableGeometry().center()
		q_rect.moveCenter(center_point)
		view.move(q_rect.topLeft())

	def remove_margins(self, view):
		"""
		Remove margins to make space for status label at the bottom
		:param view:
		:return:
		"""

	# view.centralWidget

	def handle_extraction_started(self, *args):
		self.sig_progress.emit(0)
		self.sig_status_bar.emit(args[1])

	def handle_extraction_changed(self, *args):
		self.sig_progress.emit(args[0])
		self.sig_status_bar.emit(args[1])

	def handle_extraction_ended(self, *args):
		self.sig_progress.emit(100)
		self.sig_status_bar.emit("Extracted Server info.")

	def __init__(self, view):
		super(MainWindowController, self).__init__(view)
		self.progress = QProgressBar(self._view)
		# self.progress.setAlignment(Qt.AlignRight)
		frame_geometry = self._view.statusbar.frameGeometry()
		self.progress.setGeometry(QRect(0, 0, frame_geometry.width() / 2, frame_geometry.height()))
		self._view.statusbar.addPermanentWidget(self.progress)
		self.sig_progress.connect(self.progress.setValue)

		self.center(view)

		self.status = StatusController(self._view)
		self.login = LoginController(self._view)
		self.town = TownController(self._view)
		self.item = ItemController(self._view)
		self.script = ScriptController(self._view)
		self.skill = SkillController(self._view)
		self.script = ScriptController(self._view)
		self.Party = PartyController(self._view)
		self.potions = PotionController(self._view)
		self.item_containers = ItemContainersController(self._view)
		self.map = MapController(self._view)

	# Loader.instance().start_client(LoginSettings.instance().login_client_directory_path, NetworkGlobal.ProxyGatewayPort, LoginSettings.instance().locale)
	# debug
	# Loader.wine_file_path = "/home/pr/.local/share/lutris/runners/wine/lutris-4.20-x86_64/bin/wine64"
	# Loader.k32_file_path = "/home/pr/.local/share/lutris/runners/wine/lutris-4.20-x86_64/lib/wine/kernel32.dll.so"
	# Loader.instance().start_client("/home/pr/Documents/Games/SRO_Asatru/", NetworkGlobal.ProxyGatewayPort, LoginSettings.instance().locale)

	def _connect_signals(self):
		self.sig_status_bar.connect(self._view.statusbar.showMessage)
		PK2Utils.on_extraction_started += self.handle_extraction_started
		PK2Utils.on_extraction_changed += self.handle_extraction_changed
		PK2Utils.on_extraction_ended += self.handle_extraction_ended

		self._view.saveSettingsPushButton.clicked.connect(self.save_settings)
