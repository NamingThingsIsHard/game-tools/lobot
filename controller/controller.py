# This Python file uses the following encoding: utf-8
from abc import abstractmethod

from PyQt5.QtCore import QObject, pyqtSignal, QThreadPool

from lobot_api.globals.DirectoryGlobal import DirectoryGlobal
from lobot_api.globals.settings.Settings import settings


class Controller(QObject):
	"""Abstract base class for QObjects containing logic. In Lobot's case these are the settings tabs"""

	sig_status_bar = pyqtSignal(str)
	"""ThreadPoolExecutor to use as worker threads and prevent GUI freezing."""
	thread_pool = QThreadPool()

	def __init__(self, view):
		"""Controller class for login tab
		:param view: QWidget, mainly QMainWindow
		"""
		super().__init__()
		self._view = view
		# Connect signals and slots
		self._connect_signals()
		self._load_settings()

	def save_settings(self):
		with open(DirectoryGlobal.settings_data(), 'w+') as configfile:
			settings.write(configfile)
			self.sig_status_bar.emit("Settings saved")

	@abstractmethod
	def _load_settings(self):
		"""Try to load settings from ini file."""

	@abstractmethod
	def _connect_signals(self):
		"""Connect signals and slots"""
		pass
