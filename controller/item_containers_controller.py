from PyQt5.QtWidgets import QTableWidgetItem, QAbstractItemView, QHeaderView

from controller.controller import Controller
from lobot_api.Bot import Bot
from lobot_api.packets.char.CharDataHandler import CharDataHandler
from lobot_api.packets.inventory.MoveDrop import MoveDrop
from lobot_api.packets.inventory.MoveEquipAvatarItem import MoveEquipAvatarItem
from lobot_api.packets.inventory.MoveInventoryToInventory import MoveInventoryToInventory
from lobot_api.packets.inventory.MoveInventoryToPet import MoveInventoryToPet
from lobot_api.packets.inventory.MoveInventoryToStorage import MoveInventoryToStorage
from lobot_api.packets.inventory.MovePickup import MovePickup
from lobot_api.packets.inventory.MoveRemoveItem import MoveRemoveItem
from lobot_api.packets.inventory.MoveSell import MoveSell
from lobot_api.packets.inventory.MoveStorageToInventory import MoveStorageToInventory
from lobot_api.packets.inventory.MoveStorageToStorage import MoveStorageToStorage
from lobot_api.packets.inventory.MoveUnequipAvatarItem import MoveUnequipAvatarItem
from lobot_api.packets.storage.StorageInfoDataHandler import StorageInfoDataHandler


class ItemContainersController(Controller):

	def set_table_view(self):
		self._view.itemContainerTableWidget.setEditTriggers(QAbstractItemView.NoEditTriggers)
		self._view.itemContainerTableWidget.setSelectionBehavior(QAbstractItemView.SelectRows)
		self._view.itemContainerTableWidget.clear()
		self._view.itemContainerTableWidget.setColumnCount(4)
		self._view.itemContainerTableWidget.setHorizontalHeaderLabels(['Slot', 'Name', 'Type', 'Stacks'])

		self._view.itemContainerTableWidget.setColumnWidth(0, self._view.itemContainerTableWidget.width() / 15)
		self._view.itemContainerTableWidget.setColumnWidth(1, self._view.itemContainerTableWidget.width() / 2)
		self._view.itemContainerTableWidget.setColumnWidth(2, self._view.itemContainerTableWidget.width() / 4)

		self._view.itemContainerTableWidget.horizontalHeader().setStretchLastSection(True)
		self._view.itemContainerTableWidget.horizontalHeader().setSectionResizeMode(QHeaderView.Fixed)
		self._view.itemContainerTableWidget.verticalHeader().setVisible(False)

	def update_table(self, collection):
		if collection == {}:
			pass
		length = len(collection.items())
		self._view.itemContainerTableWidget.setRowCount(length)
		items = list(collection.values())
		items.sort(key=lambda x: x.slot)
		for i in range(0, length):
			item = items[i]
			self._view.itemContainerTableWidget.setItem(i, 0, QTableWidgetItem(str(item.slot)))
			self._view.itemContainerTableWidget.setItem(i, 1, QTableWidgetItem(item.pk2_item.name))
			self._view.itemContainerTableWidget.setItem(i, 2, QTableWidgetItem(item.type.name))
			self._view.itemContainerTableWidget.setItem(i, 3, QTableWidgetItem(str(item.stack_count)))

	def update_table_with_collection(self, collection, button, previous_button):
		self.update_table(collection)

		# prevent loading same list
		previous_button.setEnabled(True)
		button.setEnabled(False)

	def handle_item_moved(self, collection, is_target):
		if is_target:
			self.update_table(collection)
	# def handle_item_stacked(self, collection, is_target):
	# 	self.update_table(collection)
	# def handle_item_switched(self, collection, is_target):
	# 	self.update_table(collection)
	# def handle_item_removed(self, collection, is_target):
	# 	self.update_table(collection)

	def _load_settings(self):
		"""
		Open inventory view by default.
		:return:
		"""
		self._view.inventoryPushButton.clicked.emit()

	def _connect_signals(self):
		self.set_table_view()
		self._view.inventoryPushButton.clicked.connect(
			lambda x: self.update_table_with_collection(Bot.char_data.inventory.items, self._view.inventoryPushButton, self._view.storagePushButton))
		CharDataHandler.on_packet += lambda: self.handle_item_moved(Bot.char_data.inventory.items, not self._view.inventoryPushButton.isEnabled())
		MoveInventoryToInventory.on_item_moved += lambda x: self.handle_item_moved(Bot.char_data.inventory.items, not self._view.inventoryPushButton.isEnabled())
		MoveInventoryToInventory.on_item_split += lambda x: self.handle_item_moved(Bot.char_data.inventory.items, not self._view.inventoryPushButton.isEnabled())
		MoveInventoryToInventory.on_item_switched += lambda x: self.handle_item_moved(Bot.char_data.inventory.items, not self._view.inventoryPushButton.isEnabled())
		MoveInventoryToInventory.on_item_stacked += lambda x: self.handle_item_moved(Bot.char_data.inventory.items, not self._view.inventoryPushButton.isEnabled())
		MoveInventoryToPet.on_item_moved += lambda x: self.handle_item_moved(Bot.char_data.inventory.items, not self._view.inventoryPushButton.isEnabled())
		MovePickup.on_item_picked_up += lambda x: self.handle_item_moved(Bot.char_data.inventory.items, not self._view.inventoryPushButton.isEnabled())
		MoveDrop.on_dropped += lambda x: self.handle_item_moved(Bot.char_data.inventory.items, not self._view.inventoryPushButton.isEnabled())
		MoveSell.on_sold += lambda x: self.handle_item_moved(Bot.char_data.inventory.items, not self._view.inventoryPushButton.isEnabled())
		MoveRemoveItem.on_item_removed += lambda x: self.handle_item_moved(Bot.char_data.inventory.items, not self._view.inventoryPushButton.isEnabled())
		MoveEquipAvatarItem.on_equipped += lambda x: self.handle_item_moved(Bot.char_data.inventory.items, not self._view.inventoryPushButton.isEnabled())

		MoveUnequipAvatarItem.on_unequipped += lambda x: self.handle_item_moved(Bot.char_data.inventory.items, not self._view.inventoryPushButton.isEnabled())

		self._view.storagePushButton.clicked.connect(
			lambda x: self.update_table_with_collection(Bot.storage, self._view.storagePushButton, self._view.inventoryPushButton))
		StorageInfoDataHandler.on_storage_parsed += lambda: self.handle_item_moved(Bot.storage, not self._view.storagePushButton.isEnabled())
		MoveInventoryToStorage.on_item_stored += lambda x: self.handle_item_moved(Bot.char_data.inventory.items, True)
		MoveStorageToStorage.on_item_moved += lambda x: self.handle_item_moved(Bot.storage, not self._view.storagePushButton.isEnabled())
		MoveStorageToInventory.on_moved += lambda x: self.handle_item_moved(Bot.storage, True)
