from PyQt5.QtWidgets import QListWidgetItem

from lobot_api.structs.CharData import Skill


class QListWidgetSkillItem(QListWidgetItem):
	"""Container for skills to more efficiently transfer information between UI and controller"""

	def __init__(self, skill: Skill, text: str = '', parent=None):
		super(QListWidgetSkillItem, self).__init__(text, parent)
		self.skill = skill

	def __eq__(self, other):
		return other and self.skill.ref_id == other.ref_id or self.text() == other.text()

	def __hash__(self):
		return hash(self.skill.ref_id)
