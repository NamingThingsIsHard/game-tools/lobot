from PyQt5.QtCore import pyqtSlot

from controller.controller import Controller
from lobot_api.globals.settings.LoopSettings import ShoppingSettings


class TownController(Controller):

	@pyqtSlot(int)
	def on_buyHPPotIndex_changed(self, i):
		ShoppingSettings.instance().hp_potion_index = i

	@pyqtSlot(int)
	def on_buyHPPot_changed(self, i):
		ShoppingSettings.instance().hp_potion_quantity = i

	@pyqtSlot(int)
	def on_buyMPPotIndex_changed(self, i):
		ShoppingSettings.instance().mp_potion_index = i

	@pyqtSlot(int)
	def on_buyMPPot_changed(self, i):
		ShoppingSettings.instance().mp_potion_quantity = i

	@pyqtSlot(int)
	def on_buyVigorIndex_changed(self, i):
		ShoppingSettings.instance().vigor_potion_index = i

	@pyqtSlot(int)
	def on_buyVigor_changed(self, i):
		ShoppingSettings.instance().vigor_potion_quantity = i

	@pyqtSlot(int)
	def on_buyUniversalPillIndex_changed(self, i):
		ShoppingSettings.instance().universal_pill_index = i

	@pyqtSlot(int)
	def on_buyUniversalPill_changed(self, i):
		ShoppingSettings.instance().universal_pill_quantity = i

	@pyqtSlot(int)
	def on_buySpecialPillIndex_changed(self, i):
		ShoppingSettings.instance().purification_pill_index = i

	@pyqtSlot(int)
	def on_buySpecialPill_changed(self, i):
		ShoppingSettings.instance().purification_pill_quantity = i

	@pyqtSlot(int)
	def on_buyProjectileIndex_changed(self, i):
		ShoppingSettings.instance().projectile_index = i

	@pyqtSlot(int)
	def on_buyProjectile_changed(self, i):
		ShoppingSettings.instance().projectile_quantity = i

	@pyqtSlot(int)
	def on_buySpeedDrugIndex_changed(self, i):
		ShoppingSettings.instance().speed_pot_index = i

	@pyqtSlot(int)
	def on_buySpeedDrug_changed(self, i):
		ShoppingSettings.instance().speed_pot_quantity = i

	@pyqtSlot(int)
	def on_buyReturnScrollIndex_changed(self, i):
		ShoppingSettings.instance().return_scroll_index = i

	@pyqtSlot(int)
	def on_buyReturnScroll_changed(self, i):
		ShoppingSettings.instance().return_scroll_quantity = i

	@pyqtSlot(int)
	def on_buyTransportIndex_changed(self, i):
		ShoppingSettings.instance().transport_index = i

	@pyqtSlot(int)
	def on_buyTransport_changed(self, i):
		ShoppingSettings.instance().transport_quantity = i

	@pyqtSlot(int)
	def on_buyPetHPPotIndex_changed(self, i):
		ShoppingSettings.instance().pet_hp_potion_index = i

	@pyqtSlot(int)
	def on_buyPetHPPot_changed(self, i):
		ShoppingSettings.instance().pet_hp_potion_quantity = i

	@pyqtSlot(int)
	def on_buyPetPillIndex_changed(self, i):
		ShoppingSettings.instance().pet_pill_index = i

	@pyqtSlot(int)
	def on_buyPetPill_changed(self, i):
		ShoppingSettings.instance().pet_pill_quantity = i

	@pyqtSlot(int)
	def on_buyPetHGP_changed(self, i):
		ShoppingSettings.instance().hgp_potion_quantity = i

	@pyqtSlot(int)
	def on_buyPetGrassOfLife_changed(self, i):
		ShoppingSettings.instance().grass_of_life_quantity = i

	def _load_settings(self):

		ShoppingSettings.instance().sig_HPPotionIndex.emit(ShoppingSettings.instance().hp_potion_index)
		ShoppingSettings.instance().sig_HPPotion.emit(ShoppingSettings.instance().hp_potion_quantity)
		ShoppingSettings.instance().sig_MPPotionIndex.emit(ShoppingSettings.instance().mp_potion_index)
		ShoppingSettings.instance().sig_MPPotion.emit(ShoppingSettings.instance().mp_potion_quantity)
		ShoppingSettings.instance().sig_VigorPotionIndex.emit(ShoppingSettings.instance().vigor_potion_index)
		ShoppingSettings.instance().sig_VigorPotion.emit(ShoppingSettings.instance().vigor_potion_quantity)
		ShoppingSettings.instance().sig_UniversalPillIndex.emit(ShoppingSettings.instance().universal_pill_index)
		ShoppingSettings.instance().sig_UniversalPill.emit(ShoppingSettings.instance().universal_pill_quantity)
		ShoppingSettings.instance().sig_SpecialPillIndex.emit(ShoppingSettings.instance().purification_pill_index)
		ShoppingSettings.instance().sig_SpecialPill.emit(ShoppingSettings.instance().purification_pill_quantity)

		# var
		ShoppingSettings.instance().sig_ProjectileIndex.emit(ShoppingSettings.instance().projectile_index)
		ShoppingSettings.instance().sig_Projectile.emit(ShoppingSettings.instance().projectile_quantity)
		ShoppingSettings.instance().sig_SpeedDrugIndex.emit(ShoppingSettings.instance().speed_pot_index)
		ShoppingSettings.instance().sig_SpeedDrug.emit(ShoppingSettings.instance().speed_pot_quantity)
		ShoppingSettings.instance().sig_ReturnScrollIndex.emit(ShoppingSettings.instance().return_scroll_index)
		ShoppingSettings.instance().sig_ReturnScroll.emit(ShoppingSettings.instance().return_scroll_quantity)
		ShoppingSettings.instance().sig_TransportIndex.emit(ShoppingSettings.instance().transport_index)
		ShoppingSettings.instance().sig_Transport.emit(ShoppingSettings.instance().transport_quantity)

		# pet
		ShoppingSettings.instance().sig_PetHPPotionIndex.emit(ShoppingSettings.instance().pet_hp_potion_index)
		ShoppingSettings.instance().sig_PetHPPotion.emit(ShoppingSettings.instance().pet_hp_potion_quantity)
		ShoppingSettings.instance().sig_PetPillIndex.emit(ShoppingSettings.instance().pet_pill_index)
		ShoppingSettings.instance().sig_PetPill.emit(ShoppingSettings.instance().pet_pill_quantity)
		ShoppingSettings.instance().sig_PetHGP.emit(ShoppingSettings.instance().hgp_potion_quantity)
		ShoppingSettings.instance().sig_PetGrassOfLife.emit(ShoppingSettings.instance().grass_of_life_quantity)

	def _connect_signals(self):
		# Consumables
		# pots
		self._view.buyHPPotComboBox.currentIndexChanged.connect(self.on_buyHPPotIndex_changed)
		ShoppingSettings.instance().sig_HPPotionIndex.connect(self._view.buyHPPotComboBox.setCurrentIndex)
		self._view.buyHPPotSpinBox.valueChanged.connect(self.on_buyHPPot_changed)
		ShoppingSettings.instance().sig_HPPotion.connect(self._view.buyHPPotSpinBox.setValue)

		self._view.buyMPPotComboBox.currentIndexChanged.connect(self.on_buyMPPotIndex_changed)
		ShoppingSettings.instance().sig_MPPotionIndex.connect(self._view.buyMPPotComboBox.setCurrentIndex)
		self._view.buyMPPotSpinBox.valueChanged.connect(self.on_buyMPPot_changed)
		ShoppingSettings.instance().sig_MPPotion.connect(self._view.buyMPPotSpinBox.setValue)

		self._view.buyVigorComboBox.currentIndexChanged.connect(self.on_buyVigorIndex_changed)
		ShoppingSettings.instance().sig_VigorPotionIndex.connect(self._view.buyVigorComboBox.setCurrentIndex)
		self._view.buyVigorSpinBox.valueChanged.connect(self.on_buyVigor_changed)
		ShoppingSettings.instance().sig_VigorPotion.connect(self._view.buyVigorSpinBox.setValue)

		self._view.buyUniPillComboBox.currentIndexChanged.connect(self.on_buyUniversalPillIndex_changed)
		ShoppingSettings.instance().sig_UniversalPillIndex.connect(self._view.buyUniPillComboBox.setCurrentIndex)
		self._view.buyUniPillSpinBox.valueChanged.connect(self.on_buyUniversalPill_changed)
		ShoppingSettings.instance().sig_UniversalPill.connect(self._view.buyUniPillSpinBox.setValue)

		self._view.buySpecialPillComboBox.currentIndexChanged.connect(self.on_buySpecialPillIndex_changed)
		ShoppingSettings.instance().sig_SpecialPillIndex.connect(self._view.buySpecialPillComboBox.setCurrentIndex)
		self._view.buySpecialPillSpinBox.valueChanged.connect(self.on_buySpecialPill_changed)
		ShoppingSettings.instance().sig_SpecialPill.connect(self._view.buySpecialPillSpinBox.setValue)

		# var
		self._view.buyProjectileComboBox.currentIndexChanged.connect(self.on_buyProjectileIndex_changed)
		ShoppingSettings.instance().sig_ProjectileIndex.connect(self._view.buyProjectileComboBox.setCurrentIndex)
		self._view.buyProjectileSpinBox.valueChanged.connect(self.on_buyProjectile_changed)
		ShoppingSettings.instance().sig_Projectile.connect(self._view.buyProjectileSpinBox.setValue)

		self._view.buySpeedDrugComboBox.currentIndexChanged.connect(self.on_buySpeedDrugIndex_changed)
		ShoppingSettings.instance().sig_SpeedDrugIndex.connect(self._view.buySpeedDrugComboBox.setCurrentIndex)
		self._view.buySpeedDrugSpinBox.valueChanged.connect(self.on_buySpeedDrug_changed)
		ShoppingSettings.instance().sig_SpeedDrug.connect(self._view.buySpeedDrugSpinBox.setValue)

		self._view.buyReturnScrollComboBox.currentIndexChanged.connect(self.on_buyReturnScrollIndex_changed)
		ShoppingSettings.instance().sig_ReturnScrollIndex.connect(self._view.buyReturnScrollComboBox.setCurrentIndex)
		self._view.buyReturnScrollSpinBox.valueChanged.connect(self.on_buyReturnScroll_changed)
		ShoppingSettings.instance().sig_ReturnScroll.connect(self._view.buyReturnScrollSpinBox.setValue)

		self._view.buyTransportComboBox.currentIndexChanged.connect(self.on_buyTransportIndex_changed)
		ShoppingSettings.instance().sig_TransportIndex.connect(self._view.buyTransportComboBox.setCurrentIndex)
		self._view.buyTransportSpinBox.valueChanged.connect(self.on_buyTransport_changed)
		ShoppingSettings.instance().sig_Transport.connect(self._view.buyTransportSpinBox.setValue)

		# pet
		self._view.buyPetHPPotComboBox.currentIndexChanged.connect(self.on_buyPetHPPotIndex_changed)
		ShoppingSettings.instance().sig_PetHPPotionIndex.connect(self._view.buyPetHPPotComboBox.setCurrentIndex)
		self._view.buyPetHPPotSpinBox.valueChanged.connect(self.on_buyPetHPPot_changed)
		ShoppingSettings.instance().sig_PetHPPotion.connect(self._view.buyPetHPPotSpinBox.setValue)

		self._view.buyPetPillComboBox.currentIndexChanged.connect(self.on_buyPetPillIndex_changed)
		ShoppingSettings.instance().sig_PetPillIndex.connect(self._view.buyPetPillComboBox.setCurrentIndex)
		self._view.buyPetPillSpinBox.valueChanged.connect(self.on_buyPetPill_changed)
		ShoppingSettings.instance().sig_PetPill.connect(self._view.buyPetPillSpinBox.setValue)

		self._view.buyPetHGPSpinBox.valueChanged.connect(self.on_buyPetHGP_changed)
		ShoppingSettings.instance().sig_PetHGP.connect(self._view.buyPetHGPSpinBox.setValue)

		self._view.buyPetGrassOfLifeSpinBox.valueChanged.connect(self.on_buyPetGrassOfLife_changed)
		ShoppingSettings.instance().sig_PetGrassOfLife.connect(self._view.buyPetGrassOfLifeSpinBox.setValue)
