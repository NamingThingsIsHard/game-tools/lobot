from PyQt5.QtCore import pyqtSignal

from controller.controller import Controller
from lobot_api.Bot import Bot
from lobot_api.packets.char.CharValueUpdateHandler import CharValueUpdateHandler
from lobot_api.packets.char.Movement import Movement
from lobot_api.packets.npc.DespawnArgs import DespawnArgs
from lobot_api.packets.pet.PetUpdateHandler import PetUpdateHandler
from lobot_api.statemachine.AutoPotions import AutoPotions


class StatusController(Controller):
	def _load_settings(self):
		pass

	sig_hp_percent = pyqtSignal(int)
	sig_mp_percent = pyqtSignal(int)
	sig_exp_percent = pyqtSignal(int)
	sig_level = pyqtSignal(str)
	sig_name = pyqtSignal(str)
	sig_guild_name = pyqtSignal(str)
	sig_sp = pyqtSignal(str)
	sig_str = pyqtSignal(str)
	sig_int = pyqtSignal(str)
	sig_positionX = pyqtSignal(str)
	sig_positionY = pyqtSignal(str)

	sig_pet_exp_percent = pyqtSignal(int)
	sig_pet_hp_percent = pyqtSignal(int)
	sig_pet_hgp_percent = pyqtSignal(int)
	sig_pet_name = pyqtSignal(str)
	sig_pet_level = pyqtSignal(str)

	@staticmethod
	def get_percent(x, max):
		return x * 100 / max if max > 0 else 100

	def handle_pet_spawned(self):
		self._view.petStatusWidget.show()

		self.handle_pet_hp_changed(Bot.growth_pet.hp_percent)
		self.handle_pet_hgp_changed(Bot.growth_pet.hgp_percent)
		self.handle_pet_exp_percent_changed(Bot.growth_pet.exp_percent)
		self.handle_pet_name_changed(Bot.growth_pet.name)
		self.handle_pet_level_changed(Bot.growth_pet.level)

	def handle_pet_despawned(self, e: DespawnArgs):
		self._view.petStatusWidget.hide()
		# self.sig_pet_hp_percent.disconnect(self._view.pethpProgressBar.setValue)
		# self.sig_pet_hgp_percent.disconnect(self._view.pethgpProgressBar.setValue)
		# self.sig_pet_exp_percent.disconnect(self._view.pethpProgressBar.setValue)
		# self.sig_pet_name.disconnect(self._view.petNameEditLabel.setText)
		# self.sig_pet_level.disconnect(self._view.petLevelEditLabel.setText)

	def handle_hp_changed(self, *args):
		percent = self.get_percent(Bot.char_data.current_hp, Bot.char_info.max_hp)
		self.sig_hp_percent.emit(percent)

	def handle_pet_hp_changed(self, *args):
		percent = self.get_percent(Bot.growth_pet.current_hp, Bot.growth_pet.max_hp)
		self.sig_pet_hp_percent.emit(percent)

	def handle_mp_changed(self, *args):
		percent = self.get_percent(Bot.char_data.current_mp, Bot.char_info.max_mp)
		self.sig_mp_percent.emit(percent)

	def handle_pet_hgp_changed(self, *args):
		percent = self.get_percent(Bot.growth_pet.hgp, AutoPotions.MAX_HGP)
		self.sig_pet_hgp_percent.emit(percent)

	def handle_exp_percent_changed(self, *args):
		self.sig_exp_percent.emit(args[0])
		self._view.expProgressBar.setFormat("%.02f %%" % args[0])

	def handle_pet_exp_percent_changed(self, *args):
		self.sig_pet_exp_percent.emit(args[0])
		self._view.petexpProgressBar.setFormat("%.02f %%" % args[0])

	def handle_name_changed(self, *args):
		self.sig_name.emit(args[0])

	def handle_pet_name_changed(self, *args):
		self.sig_pet_name.emit(args[0])

	def handle_level_changed(self, *args):
		self.sig_level.emit(str(args[0]))

	def handle_pet_level_changed(self, *args):
		self.sig_pet_level.emit(str(args[0]))

	def handle_guild_name_changed(self, *args):
		self.sig_guild_name.emit(args)

	def handle_str_changed(self, *args):
		self.sig_str.emit(str(args[0]))

	def handle_int_changed(self, *args):
		self.sig_int.emit(str(args[0]))

	def handle_sp_changed(self, *args):
		self.sig_sp.emit(str(args[0]))

	def handle_moved(self, *args):
		self.sig_positionX.emit(str(args[0].origin.x))
		self.sig_positionY.emit(str(args[0].origin.y))

	def _connect_signals(self):
		self._view.expProgressBar.setFormat("%.02f %%" % self._view.expProgressBar.value())
		self._view.petexpProgressBar.setFormat("%.02f %%" % self._view.expProgressBar.value())

		self._view.petStatusWidget.hide()
		# re-enable after pet has spawned
		PetUpdateHandler.on_spawned += self.handle_pet_spawned
		PetUpdateHandler.on_despawned += self.handle_pet_despawned

		Bot.char_data.on_hp_changed += self.handle_hp_changed
		Bot.char_data.on_mp_changed += self.handle_mp_changed
		Bot.char_info.on_max_hp_changed += self.handle_hp_changed
		Bot.char_info.on_max_mp_changed += self.handle_mp_changed
		Bot.char_data.on_exp_percent_changed += self.handle_exp_percent_changed
		Bot.char_data.on_level_changed += self.handle_level_changed
		Bot.char_data.on_name_changed += self.handle_name_changed
		Bot.char_data.on_guild_name_changed += self.handle_guild_name_changed
		Bot.char_data.on_level_changed += self.handle_level_changed
		Bot.char_info.on_str_changed += self.handle_str_changed
		Bot.char_info.on_int_changed += self.handle_int_changed
		Bot.char_data.on_sp_changed += self.handle_sp_changed
		CharValueUpdateHandler.on_sp_updated += self.handle_sp_changed
		Bot.char_data.on_moved += self.handle_moved
		Movement.on_botmovement_registered += self.handle_moved

		self.sig_hp_percent.connect(self._view.hpProgressBar.setValue)
		self.sig_mp_percent.connect(self._view.mpProgressBar.setValue)
		self.sig_exp_percent.connect(self._view.expProgressBar.setValue)
		self.sig_level.connect(self._view.levelEditLabel.setText)
		self.sig_name.connect(self._view.nameEditLabel.setText)
		self.sig_guild_name.connect(self._view.guildEditLabel.setText)
		self.sig_sp.connect(self._view.spEditLabel.setText)
		self.sig_str.connect(self._view.strEditLabel.setText)
		self.sig_int.connect(self._view.intEditLabel.setText)
		self.sig_positionX.connect(lambda x: self._view.xLabel.setText(f"X: {x}"))
		self.sig_positionY.connect(lambda y: self._view.yLabel.setText(f"Y: {y}"))

		Bot.growth_pet.on_hp_changed += self.handle_pet_hp_changed
		Bot.growth_pet.on_hgp_changed += self.handle_pet_hgp_changed
		Bot.growth_pet.on_exp_changed += self.handle_pet_exp_percent_changed
		Bot.growth_pet.on_level_changed += self.handle_pet_level_changed
		Bot.growth_pet.on_name_changed += self.handle_pet_name_changed

		self.sig_pet_hp_percent.connect(self._view.pethpProgressBar.setValue)
		self.sig_pet_hgp_percent.connect(self._view.pethgpProgressBar.setValue)
		self.sig_pet_exp_percent.connect(self._view.petexpProgressBar.setValue)
		self.sig_pet_name.connect(self._view.petNameEditLabel.setText)
		self.sig_pet_level.connect(self._view.petLevelEditLabel.setText)
