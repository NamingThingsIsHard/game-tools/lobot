from controller.controller import Controller
from lobot_api.binding import LobotProperty
from lobot_api.globals.settings.TrainingSettings import PlayerSettings, PetSettings, COSSettings


class PotionController(Controller):
	def set_bool(self, obj, value):
		if isinstance(obj, LobotProperty):
			obj.value = value > 0
		else:
			obj = value > 0

	def _load_settings(self):
		PlayerSettings.hp_use_pot.signal[bool].emit(PlayerSettings.hp_use_pot.value)
		PlayerSettings.hp_pot_percent.signal[int].emit(PlayerSettings.hp_pot_percent.value)
		PlayerSettings.hp_use_vigor.signal[bool].emit(PlayerSettings.hp_use_vigor.value)
		PlayerSettings.hp_vigor_percent.signal[int].emit(PlayerSettings.hp_vigor_percent.value)
		PlayerSettings.mp_use_pot.signal[bool].emit(PlayerSettings.mp_use_pot.value)
		PlayerSettings.mp_pot_percent.signal[int].emit(PlayerSettings.mp_pot_percent.value)
		PlayerSettings.mp_use_vigor.signal[bool].emit(PlayerSettings.mp_use_vigor.value)
		PlayerSettings.mp_vigor_percent.signal[int].emit(PlayerSettings.mp_vigor_percent.value)
		PlayerSettings.prefer_grains.signal[bool].emit(PlayerSettings.prefer_grains.value)

		PlayerSettings.bad_status_pill_or_spell.signal[bool].emit(PlayerSettings.bad_status_pill_or_spell.value)
		PlayerSettings.curse_pill_or_spell.signal[bool].emit(PlayerSettings.curse_pill_or_spell.value)

		PetSettings.hp_use_pot.signal[bool].emit(PetSettings.hp_use_pot.value)
		PetSettings.hp_pot_percent.signal[int].emit(PetSettings.hp_pot_percent.value)
		PetSettings.hgp_use_pot.signal[bool].emit(PetSettings.hgp_use_pot.value)
		PetSettings.hgp_pot_percent.signal[int].emit(PetSettings.hgp_pot_percent.value)
		PetSettings.CureBadStatus.signal[bool].emit(PetSettings.CureBadStatus.value)

		COSSettings.HPUsePot.signal[bool].emit(COSSettings.HPUsePot.value)
		COSSettings.HPPotPercent.signal[int].emit(COSSettings.HPPotPercent.value)
		COSSettings.CureBadStatus.signal[bool].emit(COSSettings.CureBadStatus.value)

	def _connect_signals(self):
		self._view.hpCheckBox.stateChanged.connect(lambda x: self.set_bool(PlayerSettings.hp_use_pot, x))
		PlayerSettings.hp_use_pot.signal[bool].connect(lambda x: self._view.hpCheckBox.setChecked(x))
		self._view.hpSpinBox.valueChanged.connect(PlayerSettings.hp_pot_percent.slot)
		PlayerSettings.hp_pot_percent.signal[int].connect(lambda x: self._view.hpSpinBox.setValue(int(x)))
		self._view.hpVigorCheckBox.stateChanged.connect(lambda x: self.set_bool(PlayerSettings.hp_use_vigor, x))
		PlayerSettings.hp_use_vigor.signal[bool].connect(lambda x: self._view.hpVigorCheckBox.setChecked(x))
		self._view.hpVigorSpinBox.valueChanged.connect(PlayerSettings.hp_vigor_percent.slot)
		PlayerSettings.hp_vigor_percent.signal[int].connect(self._view.hpVigorSpinBox.setValue)
		self._view.mpCheckBox.stateChanged.connect(lambda x: self.set_bool(PlayerSettings.mp_use_pot, x))
		PlayerSettings.mp_use_pot.signal[bool].connect(lambda x: self._view.mpCheckBox.setChecked(x))
		self._view.mpSpinBox.valueChanged.connect(PlayerSettings.mp_pot_percent.slot)
		PlayerSettings.mp_pot_percent.signal[int].connect(self._view.mpSpinBox.setValue)
		self._view.mpVigorCheckBox.stateChanged.connect(lambda x: self.set_bool(PlayerSettings.mp_use_vigor, x))
		PlayerSettings.mp_use_vigor.signal[bool].connect(lambda x: self._view.mpVigorCheckBox.setChecked(x))
		self._view.mpVigorSpinBox.valueChanged.connect(PlayerSettings.mp_vigor_percent.slot)
		PlayerSettings.mp_vigor_percent.signal[int].connect(self._view.mpVigorSpinBox.setValue)
		self._view.preferGrainsCheckBox.stateChanged.connect(lambda x: self.set_bool(PlayerSettings.prefer_grains, x))
		PlayerSettings.prefer_grains.signal[bool].connect(lambda x: self._view.preferGrainsCheckBox.setChecked(x))

		self._view.badStatusCheckBox.stateChanged.connect(lambda x: self.set_bool(PlayerSettings.bad_status_cure, x))
		PlayerSettings.bad_status_cure.signal[bool].connect(lambda x: self._view.badStatusCheckBox.setChecked(x))
		self._view.curseCheckBox.stateChanged.connect(lambda x: self.set_bool(PlayerSettings.curse_cure, x))
		PlayerSettings.curse_cure.signal[bool].connect(lambda x: self._view.curseCheckBox.setChecked(x))

		self._view.growthPetHPCheckBox.stateChanged.connect(lambda x: self.set_bool(PetSettings.hp_use_pot, x))
		PetSettings.hp_use_pot.signal[bool].connect(lambda x: self._view.growthPetHPCheckBox.setChecked(x))
		self._view.growthPetHPSpinBox.valueChanged.connect(PetSettings.hp_pot_percent.slot)
		PetSettings.hp_pot_percent.signal[int].connect(self._view.growthPetHPSpinBox.setValue)
		self._view.growthPetHGPCheckBox.stateChanged.connect(lambda x: self.set_bool(PetSettings.hgp_use_pot, x))
		PetSettings.hgp_use_pot.signal[bool].connect(lambda x: self._view.growthPetHGPCheckBox.setChecked(x))
		self._view.growthPetHGPSpinBox.valueChanged.connect(PetSettings.hgp_pot_percent.slot)
		PetSettings.hgp_pot_percent.signal[int].connect(self._view.growthPetHGPSpinBox.setValue)
		self._view.growthPetBadStatusCheckBox.stateChanged.connect(lambda x: self.set_bool(PetSettings.CureBadStatus.slot, x))
		PetSettings.CureBadStatus.signal[bool].connect(lambda x: self._view.growthPetBadStatusCheckBox.setChecked(x))

		self._view.ridePetHPCheckBox.stateChanged.connect(lambda x: self.set_bool(COSSettings.HPUsePot, x))
		COSSettings.HPUsePot.signal[bool].connect(lambda x: self._view.ridePetHPCheckBox.setChecked(x))
		self._view.ridePetHPSpinBox.valueChanged.connect(COSSettings.HPPotPercent.slot)
		COSSettings.HPPotPercent.signal[int].connect(self._view.ridePetHPSpinBox.setValue)
		self._view.ridePetBadStatusCheckBox.stateChanged.connect(lambda x: self.set_bool(COSSettings.CureBadStatus.slot, x))
		COSSettings.CureBadStatus.signal[bool].connect(lambda x: self._view.ridePetBadStatusCheckBox.setChecked(x))
