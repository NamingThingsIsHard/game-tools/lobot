import atexit
import os
import signal
import sys
from pathlib import Path

from PyQt5 import uic
from PyQt5.QtCore import QFile, QCoreApplication, Qt
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QApplication

from controller.controller import Controller
from controller.mainwindow_controller import MainWindowController
from lobot_api.connection import proxy
from lobot_api.globals.settings import Settings
from lobot_api.logger import Logger


def load_settings():
	"""
	Load settings from file if it exists.
	:return: None
	"""
	Settings.load_settings()


def tear_down(signal_number=0, stackframe=None):
	Logger.clean_up_empty_files()
	proxy.stop()  # Allow long-running while loop to stop in worker thread
	Controller.thread_pool.clear()


"""Logo path to be used"""
logo_path = Path(os.path.dirname(os.path.realpath(__file__))) / 'resources' / 'lobot_logo_v3_black.png'


if __name__ == "__main__":
	# Prepare window
	QCoreApplication.setAttribute(Qt.AA_ShareOpenGLContexts)
	app = QApplication(sys.argv)

	ui_file = QFile("mainwindow.ui")
	ui_file.open(QFile.ReadOnly)

	window = uic.loadUi(ui_file)
	window.setWindowIcon(QIcon(str(logo_path)))
	controller = MainWindowController(window)
	load_settings()

	ui_file.close()
	window.show()

	# remove empty log file when bot shuts down
	# overload pyqt close function
	window.closeEvent = tear_down
	if sys.platform.startswith("win_32"):
		atexit.register(tear_down)
	else:
		signal.signal(signal.SIGINT, tear_down)
		signal.signal(signal.SIGTERM, tear_down)

	sys.exit(app.exec_())
