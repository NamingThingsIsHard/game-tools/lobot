# Changelog

## [0.12.0](https://gitlab.com/NamingThingsIsHard/game-tools/lobot/-/tags/0.12.0) - 2022-02-19
Many bug fixes and quality of life changes to training.

- Autopotions now stop using HP potions when affected by Zombie status.
- Delay between movements now shortened.
- Test version of casting heal, resurrect and return activated.

## [0.11.0](https://gitlab.com/NamingThingsIsHard/game-tools/lobot/-/tags/0.11.0) - 2022-02-12
Enabled walk script to training place.

- Added horse riding and teleporting/waiting in scripts.
- Added WalkScript.
- Allowed recording "wait" command after teleporting.
- Fixed Bot button to show a message based on the Bot status.
- Logging now prints output to the console.

## [0.10.0](https://gitlab.com/NamingThingsIsHard/game-tools/lobot/-/tags/0.10.0) - 2022-02-05
Town script fixed to open npcs and buy all items without warnings

- Fixed equipment type checks errors causing items to not be used.
  - Changed item type ids
  - Changed format of item data in `data/items.txt`
- Town script now opens storage without "User is on other business" warning. 
- Fixed some settings not saving.
- Fixed pets to be correctly parsed.
- Fixed some logging errors and reduced output.

## [0.9.1](https://gitlab.com/NamingThingsIsHard/game-tools/lobot/-/tags/0.9.1) - 2021-12-01
Added network logging for debugging and testing

## [0.9.0](https://gitlab.com/NamingThingsIsHard/game-tools/lobot/-/tags/0.9.0) - 2021-11-27
Added some beta versions of features

- Recording, loading and saving scripts now possible.
- Fixed some pk2 extraction errors for private servers.

### Beta versions of features
- Item transfer now possible.
- Casting Heal on low hp now possible.
- Pet revive now possible.

---

## [0.8.4](https://gitlab.com/NamingThingsIsHard/game-tools/lobot/-/tags/0.8.4) - 2021-01-06
Added auto potion preference for grains.

- Auto potion now checks for potion type based on grain preference.
- Uses different potion type if preferred type is not found.
- Fixed auto potions check for item use type.
- Changed item code to ignore custom types like grains and just use the types from the pk2.

## [0.8.3](https://gitlab.com/NamingThingsIsHard/game-tools/lobot/-/tags/0.8.3) - 2021-01-03
Warlock now switches targets after normal/champion monster has 2 DOTs.

- Fixed inventory expansion scrolls being classified as speed scrolls.
- Bot now keeps track of Consumables.
- Fixed removing warlock DOT and other buffs from monsters.

## [0.8.2](https://gitlab.com/NamingThingsIsHard/game-tools/lobot/-/tags/0.8.2) - 2021-01-03
Universal pills are now used until a bad status is removed.

- Added state for when character has both debuff and bad status.
- Fixed reusable timer to now update ready state before calling callback.

## [0.8.1](https://gitlab.com/NamingThingsIsHard/game-tools/lobot/-/tags/0.8.1) - 2021-01-02
Fixed weapon switching inventory packet.

## [0.8.0](https://gitlab.com/NamingThingsIsHard/game-tools/lobot/-/tags/0.8.0) - 2021-01-01
Added zerk and map functions.

- Fixed buffing.
- Fixed item filtering to update after changes.
- Optimized inventory operations.
- Improved attacking and targeting.

---

## [0.7.2](https://gitlab.com/NamingThingsIsHard/game-tools/lobot/-/tags/0.7.2) - 2020-08-23
Replace old client with new values if a new client is chosen.

- Show progress of pk2 extraction in status bar.
- Fixed PK2 shop extractor problems with edited files.
- Fixed connection errors after refactoring

## [0.7.1](https://gitlab.com/NamingThingsIsHard/game-tools/lobot/-/tags/0.7.1) - 2020-08-20
Updated python requirement to version 3.8

## [0.7.0](https://gitlab.com/NamingThingsIsHard/game-tools/lobot/-/tags/0.7.0) - 2020-08-13
Settings now load from ini file

- Skill list persistence TBA later for training.
- Settings must be saved with the button in the top right corner.

---

## [0.6.1](https://gitlab.com/NamingThingsIsHard/game-tools/lobot/-/tags/0.6.0) - 2020-07-20
Bug fixes and reformatting

- Fixed reading float to not read double.
- Fixed all parsed units to show right coordinates

## [0.6.0](https://gitlab.com/NamingThingsIsHard/game-tools/lobot/-/tags/0.6.0) - 2020-07-20
Players, Monster and item spawns now handled

- PK2 fixes for loading from text files
- Spawn lists now visible in UI.
- Removed unnecessary pip package requirements

---

## [0.5.2](https://gitlab.com/NamingThingsIsHard/game-tools/lobot/-/tags/0.5.2) - 2020-07-18
Item management implemented

- Item lists now visible in UI.

## [0.5.1](https://gitlab.com/NamingThingsIsHard/game-tools/lobot/-/tags/0.5.1) - 2020-07-16
Item filters wired up

- Item pick/store/sell/drop settings configurable.
- Settings not yet persistent

## [0.5.0](https://gitlab.com/NamingThingsIsHard/game-tools/lobot/-/tags/0.5.0) - 2020-07-13
Status indicators now wired up

- Character location now recognized
- HP, MP and Experience now correctly shown
- Setup fixed

---

## [0.4.0](https://gitlab.com/NamingThingsIsHard/game-tools/lobot/-/tags/0.4.0) - 2020-07-10
Party functions implemented

- Clientless functions for party added
- Some connection bugs with client/clientless fixed
- Logging out now available

---

## [0.3.0](https://gitlab.com/NamingThingsIsHard/game-tools/lobot/-/tags/0.3.0) - 2020-07-05
Clientless stub implemented

- Fixed opcodes not being sent properly
- Bot now logs in successfully in clientless mode

---

## [0.2.3](https://gitlab.com/NamingThingsIsHard/game-tools/lobot/-/tags/0.2.3) - 2020-06-30
Skill casting implemented

- Now uses all handlers for server packets
- Optimized loading handlers

## [0.2.2](https://gitlab.com/NamingThingsIsHard/game-tools/lobot/-/tags/0.2.2) - 2020-06-29
Auto potions implemented

- Added logo

## [0.2.1](https://gitlab.com/NamingThingsIsHard/game-tools/lobot/-/tags/0.2.1) - 2020-06-28
Auto potions implemented

- Bot now uses potions based on settings.
- Clean up log files before shutting down.
- Various formatting changes. 


## [0.2.0](https://gitlab.com/NamingThingsIsHard/game-tools/lobot/-/tags/0.2.0) - 2020-06-19
Successfully login using external loader

- Connection now possible with external loader (tested with edxSilkroadLoader.
- Handling now logs errors.
- Packet handling now asynchronous for better responsiveness.
- Some blowfish errors fixed.

---

## [0.1.0](https://gitlab.com/NamingThingsIsHard/game-tools/lobot/-/tags/0.1.0) - 2020-04-08
Working PK2 extraction

- PK2 folder validated based on existence of media.pk2 and sro_client.exe files.
- Basic UI implemented.
- Encrypted pk2 Skills decoded using blowfish.

