import setuptools
from setuptools import setup

with open("README.md", "r") as fh:
	LONG_DESCRIPTION = fh.read()
with open("requirements.txt", "r") as fh:
	REQUIREMENTS = fh.readlines()
with open("VERSION", "r") as fh:
	VERSION = fh.read().strip()

setup(
	name='Lobot',
	version=VERSION,
	packages=setuptools.find_packages(),
	url='https://gitlab.com/NamingThingsIsHard/game-tools/lobot',
	license='GNU GPLv3',
	author='pr',
	author_email='opport@web.de',
	description='A bot for SRO private servers using the VSRO 1.188 files.',
	install_requires=REQUIREMENTS,
	long_description=LONG_DESCRIPTION,
	classifiers=[
		"Programming Language :: Python :: 3",
		"License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
		"Operating System :: POSIX :: Linux",
		"Development Status :: 2 - Pre-Alpha",
		"Intended Audience :: Other Audience",
		"Topic :: Games/Entertainment :: Role-Playing"
	],
	python_requires='~=3.8'
)
