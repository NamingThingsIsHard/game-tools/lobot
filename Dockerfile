FROM debian:bullseye-slim as base

WORKDIR /project

RUN apt-get update -qq \
      && apt-get install -yqq \
# Enable if more Qt dependencies become necessary
#           libqt5gui5 \
           libgl1 \
           libglib2.0-0 \
           python3-pip \
      # Make `python` available
      && update-alternatives --install /usr/bin/python python /usr/bin/python3 0 \
      && rm -rf /var/lib/apt/lists/* \
# Enabled to create Qt environment and to find out which Qt libs are missing
ENV QT_DEBUG_PLUGINS=1
# Disables need for x11 in docker
ENV QT_QPA_PLATFORM=offscreen

COPY requirements*.txt ./
RUN pip install -r requirements.txt

FROM base as prod

COPY . ./
CMD [ "python", "-m", "lobot" ]

FROM base as ci
RUN pip install -r requirements.dev.txt
